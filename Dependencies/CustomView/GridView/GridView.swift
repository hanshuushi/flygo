//
//  GridView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

@objc protocol GridViewDelegate : NSObjectProtocol {
    
}

@objc protocol GridViewDataSource : NSObjectProtocol {
    func scaleForGridView(_ gridView:GridView) -> CGFloat
    
    func gridView(_ gridView:GridView, cellAtIndex:Int) -> UICollectionViewCell
    
    func cellCountForGridView(_ gridView:GridView) -> Int
}

class GridView: UIView {

    enum RegisterType {
        case Nib (nibName:String, identfier:String)
        case Class (cellClass:AnyClass, identfier:String)
        
        var identfier:String {
            get {
                switch self {
                case .Nib(_ , let identfier):
                    return identfier
                case .Class(_, let identfier):
                    return identfier
                }
            }
        }
    }
    
    var register:RegisterType? = nil {
        didSet {
            switch register! {
            case .Nib(let nibName, let identfier):
                let nib = UINib(nibName: nibName, bundle: nil)
                
                self.collectionView.register(nib, forCellWithReuseIdentifier: identfier)
            case .Class(let cellClass, let identfier):
                self.collectionView.register(cellClass, forCellWithReuseIdentifier: identfier)
            }
        }
    }
    
    let collectionView:UICollectionView
    
    fileprivate let layout = GridLayout()
    
    override init(frame: CGRect) {
        collectionView = UICollectionView(frame: CGRect(x:0, y:0, w:frame.size.width, h:frame.size.height),
                                          collectionViewLayout: layout)
        
        super.init(frame: frame)
        
        self.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        collectionView = UICollectionView(frame: CGRect.zero,
                                          collectionViewLayout: layout)
        
        super.init(coder: aDecoder)
        
        self.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func reloadData() {
        let scale = dataSource?.scaleForGridView(self) ?? 1.0
        
        let width = self.w / 2.0
        
        let height = width / scale
        
        layout.cellSize = CGSize(width: width, height: height)
        layout.cellCount = dataSource?.cellCountForGridView(self) ?? 0
        
        collectionView.reloadData()
    }
    
    @IBOutlet weak var dataSource:GridViewDataSource? = nil
    
    @IBOutlet weak var delegate:GridViewDelegate? = nil
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        collectionView.frame = super.bounds
        
        let scale = dataSource?.scaleForGridView(self) ?? 1.0
        
        let width = self.w / 2.0
        
        let height = width / scale
        
        layout.cellSize = CGSize(width: width, height: height)
        layout.cellCount = dataSource?.cellCountForGridView(self) ?? 0
        
        collectionView.reloadData()
        
    }
}

extension GridView : UICollectionViewDelegate {
    
}

extension GridView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource?.cellCountForGridView(self) ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.dataSource!.gridView(self, cellAtIndex: indexPath.row)
    }
    
    func dequeueReusableCell<T:UICollectionViewCell>(for row: Int) -> T {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: self.register?.identfier ?? "GridCell", for: IndexPath(row: row, section: 0)) as! T
        
        return cell
    }
}

class GridLayout : UICollectionViewLayout {
    
    override var collectionViewContentSize:CGSize {
        get {
            let width = cellSize.width * 2.0
            
            let height = cellSize.height * CGFloat(((cellCount - 1) / 2 + 1))
            
            return CGSize(width: width, height: height)
        }
    }
    
    var cellSize:CGSize
    
    var cellCount:Int 
    
    var attrsArray:[UICollectionViewLayoutAttributes] = []
    
    override init (){
        cellSize = CGSize.zero
        
        cellCount = 0
        
        super.init()
    }
    
    override func prepare() {
        
        attrsArray = []
        
        for index in 0..<cellCount {
            
            let indexPath = IndexPath(item: index, section: 0)
            
            let attrs = layoutAttributesForItem(at: indexPath)
            
            attrsArray.append(attrs!)
        }
        
        super.prepare()
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let index = indexPath.row
        
        let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        
        attrs.frame = CGRect(x: index % 2 == 0 ? 0 : cellSize.width,
                             y: CGFloat(Int(index / 2)) * cellSize.height,
                             w: cellSize.width,
                             h: cellSize.height)
        
        return attrs
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        
        return attrsArray
    }
}
