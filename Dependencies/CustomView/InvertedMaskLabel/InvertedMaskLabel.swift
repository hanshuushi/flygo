//
//  InvertedMaskLabel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/2.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class InvertedMaskLabel: UILabel {
    override func drawText(in rect: CGRect) {
        guard let gc = UIGraphicsGetCurrentContext() else { return }
        gc.saveGState()
        gc.setFillColor(UIColor.white.cgColor)
        
        gc.fill(rect)
        gc.setBlendMode(.clear)
        
        super.drawText(in: rect)
        
        gc.restoreGState()
    }
}
