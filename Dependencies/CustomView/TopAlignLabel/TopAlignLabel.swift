//
//  TopAlignLabel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/2.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class TopAlignLabel: UILabel {
    
    override func drawText(in rect: CGRect) {
        
        let size = self.sizeThatFits(rect.size)
        
        var frame = rect
        
        frame.size.height = min(rect.height, size.height)
        
        super.drawText(in: frame)
    }
    
}
