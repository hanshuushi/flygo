//
//  SearchPresentController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/7.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import SnapKit

protocol SearchPresentType : class {
    var viewController:UIViewController { get }
}

extension SearchPresentType where Self: UIViewController{
    var viewController:UIViewController {
        return self
    }
}

protocol SearchPresenting: SearchPresentType {
    var searchTextFieldFrame:CGRect { get }
}

protocol SearchPresented: SearchPresentType {
    weak var searchTextField:UITextField? { get set }
    
    func search(keyword:String)
}

class SearchPresentController: UIPresentationController {
    
    let presented:SearchPresented
    
    let presenting:SearchPresenting
    
    static var sharePresenterVC:SearchPresentController? = nil
    
    func present() {
        
        presenting.viewController.present(presented.viewController,
                                          animated: true,
                                          completion: nil)
    }
    
    init(presented: SearchPresented, presenting: SearchPresenting) {
        
        self.presented = presented
        
        self.presenting = presenting
        
        super.init(presentedViewController: presented.viewController,
                   presenting: presenting.viewController)
        
        presented.viewController.transitioningDelegate = self
        presented.viewController.modalPresentationStyle = .custom
    }
    
    static var tempCachePlaceHolder:String = ""
    
    static func generalTextField(placeHolder:String) -> UITextField {
        let textField = UITextField()
        
        tempCachePlaceHolder = placeHolder
        
        textField.backgroundColor = UIConfig.generalColor.backgroudGray.withAlphaComponent(0.95)
        textField.layer.cornerRadius = 5
        textField.font = UIConfig.generalFont(14)
        textField.textColor = UIConfig.generalColor.selected
        textField.attributedPlaceholder = NSAttributedString(string: placeHolder,//"YSL  口红",//"搜索你想要的",
                                                             attributes:[
                                                                NSForegroundColorAttributeName: UIConfig.generalColor.unselected,
                                                                NSFontAttributeName:UIConfig.generalFont(14)
            ])
        
        let leftView = { () -> UIView in 
            let imageView = UIImageView(named: "icon_search-1")
            
            let leftView = UIView(frame: CGRect(x: 0,
                                                y: 0,
                                                w: imageView.size.width + 27, h: 30))
            
            leftView.addSubview(imageView)
            leftView.backgroundColor = UIColor.clear
            
            imageView.left = 15
            imageView.centerYInSuperView()
            
            return leftView
        }()
        
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.returnKeyType = .search
        
        return textField
    }
    
    var navigationTooBar:UIView?
    
    var maskView = { () -> UIView in
        let view = UIView()
        
        view.backgroundColor = UIConfig.generalColor.white
        
        let lineView = UIView()
        
        lineView.backgroundColor = UIConfig.generalColor.lineColor
        
        view.addSubview(lineView)
        
        return view
    }()
    
    func navigationTooBar(frame:CGRect, target:Any, action:Selector) -> UIView {
        let view = UIView(frame: frame)
        
        let cancelButton = UIButton(type: .custom)
        
        cancelButton.backgroundColor = UIColor.clear
        cancelButton.setTitle("取消",
                              for: .normal)
        cancelButton.titleLabel?.font = UIConfig.generalFont(15)
        cancelButton.setTitleColor(UIConfig.generalColor.selected,
                                   for: .normal)
        cancelButton.addTarget(target,
                               action: action,
                               for: UIControlEvents.touchUpInside)
        
        view.addSubview(cancelButton)
        
        cancelButton.sizeToFit()
        cancelButton.frame.size.width += 30
        cancelButton.right = view.bounds.maxX
        cancelButton.frame.size.height = 44
        cancelButton.frame.origin.y = 20
        
        let textField = SearchPresentController.generalTextField(placeHolder: SearchPresentController.tempCachePlaceHolder)
        
        view.addSubview(textField)
        
        textField.frame = CGRect(x: 15,
                                 y: 0,
                                 w: cancelButton.left - 15,
                                 h: 30)
        textField.delegate = self
        textField.centerY = (view.bounds.height - 20) / 2.0 + 20
        
        return view
    }
}

extension SearchPresentController {
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            navigationTooBar?.removeFromSuperview()
        }
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            navigationTooBar?.removeFromSuperview()
        }
        
        SearchPresentController.sharePresenterVC = nil
    }
    
    override func dismissalTransitionWillBegin() {
        let tran = self.presentingViewController.transitionCoordinator
        
        let presentedView = self.presentedView
        
        if let navigationTooBar = self.navigationTooBar,
            navigationTooBar.subviews.count > 0,
            let textField = navigationTooBar.subviews[1] as? UITextField {
            textField.text = ""
        }
        
        tran?.animate(alongsideTransition: { (_) in
            presentedView?.alpha = 0.0
            self.maskView.alpha = 0.0
            self.navigationTooBar!.subviews[0].transform = CGAffineTransform(translationX: self.navigationTooBar!.subviews[0].w, y: 0)
            self.navigationTooBar!.subviews[1].frame = self.presenting.searchTextFieldFrame
        }, completion: nil)
    }
    
    override func presentationTransitionWillBegin() {
        SearchPresentController.sharePresenterVC = self
        
        guard let view = containerView, let presentedView = self.presentedView else {
            return
        }
        
        if navigationTooBar == nil {
            navigationTooBar = navigationTooBar(frame: CGRect(x:0, y:0, width: view.bounds.width, height:64),
                                                target: self,
                                                action: #selector(SearchPresentController.cancelClick))
            
            presented.searchTextField = navigationTooBar?.subviews[1] as? UITextField
        }
        
        view.addSubview(maskView)
        
        maskView.frame = view.bounds
        maskView.subviews[0].frame = CGRect(x: 0,
                                            y: 63,
                                            w: maskView.bounds.width,
                                            h: 0.5)
        
        view.addSubview(navigationTooBar!)
        
        view.addSubview(presentedView)
        
        maskView.alpha = 0
        
        presentedView.alpha = 0
        
        let frame = navigationTooBar!.subviews[1].frame
        
        navigationTooBar!.subviews[0].transform = CGAffineTransform(translationX: navigationTooBar!.subviews[0].w, y: 0)
        navigationTooBar!.subviews[1].frame = presenting.searchTextFieldFrame
        
        let tran = self.presentingViewController.transitionCoordinator
        
        tran?.animate(alongsideTransition: { (_) in
            self.maskView.alpha = 1
            presentedView.alpha = 1
            self.navigationTooBar!.subviews[1].frame = frame
            self.navigationTooBar!.subviews[0].transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    override var frameOfPresentedViewInContainerView:CGRect {
        return self.containerView.map({
            CGRect(x: 0,
                   y: 64,
                   w: $0.bounds.width,
                   h: $0.bounds.height - 64)
        }) ?? CGRect.zero
    }
}

extension SearchPresentController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let keyword = textField.text, keyword.length > 0, string == "\n" {
            textField.resignFirstResponder()
            
            self.presented.search(keyword: keyword)
            
            return false
        }
        
        return true
    }
}

extension SearchPresentController {
    func cancelClick () {
        presented.viewController.dismiss(animated: true, completion: nil)
    }
}

extension SearchPresentController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return self
    }
}
