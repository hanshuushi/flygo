//
//  PanelPresentController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/22.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

enum PanelPresentControllerLayout {
    case inset(UIEdgeInsets)
    case size(CGSize)
}

class PanelPresentController: UIPresentationController {
    
    var isPresenting:Bool
    
    let panelLayout:PanelPresentControllerLayout
    
    static var sharePresenterVC:PanelPresentController? = nil
    
    init(presentedViewController: UIViewController,
         presenting presentingViewController: UIViewController?,
         panelLayout:PanelPresentControllerLayout,
         panelCornerRadius:CGFloat = 5) {
        
        self.panelLayout = panelLayout
        
        self.isPresenting = true
        
        if panelCornerRadius > 0 {
            presentedViewController.view.layer.cornerRadius = panelCornerRadius
            presentedViewController.view.layer.masksToBounds = true
        }
        
        super.init(presentedViewController: presentedViewController,
                   presenting: presentingViewController)
        
        presentedViewController.modalPresentationStyle = .custom
        presentedViewController.transitioningDelegate = self
    }
    
    func present() {
        isPresenting = true
        
        self.presentingViewController.present(self.presentedViewController,
                                              animated: true,
                                              completion: nil)
    }
    
    func present(completion: @escaping (PanelPresentController) -> Void) {
        isPresenting = true
        
        self
            .presentingViewController
            .present(self.presentedViewController,
                     animated: true) { 
                        [weak self] in
                        guard let `self` = self else {
                            return
                        }
                        
                        completion(self)
        }
    }
    
    let maskView = { () -> UIView in
        let maskView = UIView()
        
        maskView.alpha = 0.0
        
        return maskView
    }()
    
    func dismiss() {
        self.presentingViewController.dismissVC(completion: nil)
    }
}

extension PanelPresentController {
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            maskView.removeFromSuperview()
        }
    }
    
    override func dismissalTransitionWillBegin() {
        let tran = self.presentingViewController.transitionCoordinator
        
        tran?.animate(alongsideTransition: { (_) in
            self.maskView.alpha = 0.0
        }, completion: nil)
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            maskView.removeFromSuperview()
        }
        
        PanelPresentController.sharePresenterVC = nil
    }
    
    override func presentationTransitionWillBegin() {
        
        guard let view = containerView, let presentedView = self.presentedView else {
            return
        }
        
        PanelPresentController.sharePresenterVC = self
        
        view.addSubview(maskView)
        
        maskView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        maskView.frame = view.bounds
        
        view.addSubview(presentedView)
        
        let tran = self.presentingViewController.transitionCoordinator
        
        tran?.animate(alongsideTransition: { (_) in
            self.maskView.alpha = 1
        }, completion: nil)
    }
    
    override var frameOfPresentedViewInContainerView:CGRect {
        let bounds = self.containerView?.bounds ?? UIScreen.main.bounds
        
        switch panelLayout {
        case .inset(let edgeInsets):
            let frame = UIEdgeInsetsInsetRect(bounds, edgeInsets)
            
            return frame
        case .size(let size):
            return CGRect(x: (bounds.width - size.width) / 2.0,
                          y: (bounds.height - size.height) / 2.0,
                          width: size.width,
                          height: size.height)
        }
    }
}

extension PanelPresentController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return self
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.isPresenting ? 0.7 : 0.125
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {
            animatePresentationWithTransitionContext(transitionContext: transitionContext)
        } else {
            animateDismissalWithTransitionContext(transitionContext: transitionContext)
        }
    }
    
    func animatePresentationWithTransitionContext(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let presentedController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.to)
            else {
                return
        }
        
        let containerView = transitionContext.containerView
        
        var animation:((Void) -> Void)! = nil
        
        var completion:((Bool) -> Void) = {
            success in
            transitionContext.completeTransition(success)
        }
        
        let finalFrame = transitionContext.finalFrame(for: presentedController)
        
        presentedControllerView.frame = finalFrame
        
        if let image = presentedControllerView.getScreenShot() {
            
            presentedControllerView.isHidden = true
            
            let imageView = UIImageView(image: image)
            
            containerView.addSubview(imageView)
            
            imageView.frame = presentedControllerView.frame
            imageView.layer.changeAnchorPoint(CGPoint(x: 0.05, y: 0.05))
            imageView.transform = CGAffineTransform.init(rotationAngle: -CGFloat(CGFloat.pi / 4.0 / 6.0))
            imageView.transform.ty = -finalFrame.maxY
            
            animation = {
                imageView.transform = CGAffineTransform.identity
            }
            
            completion = {
                success in
                
                transitionContext.completeTransition(success)
                
                if success {
                    imageView.removeFromSuperview()
                    
                    presentedControllerView.isHidden = false
                }
            }
        } else {
            
            presentedControllerView.transform = CGAffineTransform(translationX: 0,
                                                                  y: -UIScreen.main.bounds.height)
            
            animation = {
                presentedControllerView.transform = CGAffineTransform.identity
            }
        }
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5,
                       options: .curveEaseOut,
                       animations: animation,
                       completion: completion)
    }
    
    func animateDismissalWithTransitionContext(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.from)
            else {
                return
        }
        
        let containerView = transitionContext.containerView
        
        var animation:((Void) -> Void)! = nil
        
        let completion:((Bool) -> Void) = {
            success in
            transitionContext.completeTransition(success)
        }
        
        if let image = presentedControllerView.getScreenShot() {
            
            presentedControllerView.isHidden = true
            
            let imageView = UIImageView(image: image)
            
            containerView.addSubview(imageView)
            
            imageView.frame = presentedControllerView.frame
            imageView.layer.changeAnchorPoint(CGPoint(x: 0.05, y: 0.05))
            
            var transform = CGAffineTransform.init(rotationAngle: CGFloat(CGFloat.pi / 4.0 / 16.0))
            
            transform.ty = UIScreen.main.bounds.height
            
            animation = {
                imageView.transform = transform
            }
        } else {
            animation = {
                presentedControllerView.transform = CGAffineTransform(translationX: 0,
                                                                      y: UIScreen.main.bounds.height)
            }
        }
        
        // Animate the presented view off the bottom of the view
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: animation,
                       completion: completion)
    }
}
