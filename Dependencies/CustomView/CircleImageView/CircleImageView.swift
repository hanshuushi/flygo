//
//  CircleImageView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/1.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class CircleImageView: UIImageView {
    
    var orgImage:UIImage? = nil
    
    override var image:UIImage? {
        get {
            return orgImage
        }
        set {
            orgImage = newValue
            
            super.image = cornerRadiusImage()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        super.image = cornerRadiusImage()
    }
    
    func cornerRadiusImage() -> UIImage? {
        
        guard let _orgImage = orgImage else {
            return nil
        }
        
        let rect = CGRect(origin: CGPoint.zero,
                          size: self.bounds.size)
        
        UIGraphicsBeginImageContextWithOptions(rect.size,
                                               false,
                                               UIScreen.main.scale)
        
        let radius = min(self.bounds.width, self.bounds.height)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            
            UIGraphicsEndImageContext()
            
            return nil
        }
        
        context.addPath(UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: UIRectCorner.allCorners,
                                     cornerRadii: CGSize(width: radius, height: radius)).cgPath)
        
        context.clip()
        
        _orgImage.draw(in: self.bounds)
        
        context.drawPath(using: .fillStroke)
        
        let output = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return output
    }
}
