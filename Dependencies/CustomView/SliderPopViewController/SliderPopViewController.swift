//
//  SliderPopViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/6.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class SliderPopViewController: UIPresentationController {
    
    var isPresenting = true
    
    let contentViewController:UIViewController
    
    static var sharePresenterVC:SliderPopViewController? = nil
    
    enum Mode {
        case top
        case left
        case right
        case bottom
    }
    
    let mode:Mode
    
    enum FillMode {
        case margin
        case size
    }
    
    let fillMode:FillMode
    
    let value:CGFloat
    
    init (_ contentViewController:UIViewController,
          mode:Mode = .right,
          fillMode:FillMode = .margin,
          value:CGFloat = 90) {
        self.contentViewController = contentViewController
        
        self.mode = mode
        
        self.fillMode = fillMode
        
        self.value = value
        
        super.init(presentedViewController: contentViewController,
                   presenting: UIApplication.shared.keyWindow?.rootViewController)
        
        contentViewController.modalPresentationStyle = .custom
        contentViewController.transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func present() {
        isPresenting = true
        
        UIApplication.shared.keyWindow?.rootViewController?.present(self.contentViewController,
                                                                    animated: true,
                                                                    completion: nil)
    }
    
    func present(on viewController:UIViewController) {
        isPresenting = true
        
        viewController.present(self.contentViewController,
                               animated: true,
                               completion: nil)
    }
    
    let maskView = { () -> UIView in
        let maskView = UIView()
        
        maskView.alpha = 0.0
        
        return maskView
    }()
}

extension SliderPopViewController {
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            maskView.removeFromSuperview()
        }
    }
    
    override func dismissalTransitionWillBegin() {
        let tran = self.presentingViewController.transitionCoordinator
        
        tran?.animate(alongsideTransition: { (_) in
                        self.maskView.alpha = 0.0
        }, completion: nil)
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            maskView.removeFromSuperview()
        }
        
        SliderPopViewController.sharePresenterVC = nil
    }
    
    override func presentationTransitionWillBegin() {
        
        guard let view = containerView, let presentedView = self.presentedView else {
            return
        }
        
        SliderPopViewController.sharePresenterVC = self
        
        view.addSubview(maskView)
        
        maskView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        maskView.frame = view.bounds
        
        let gestureCount = maskView.gestureRecognizers?.count ?? 0
        
        if gestureCount <= 0 {
            
            let contentViewController = self.contentViewController
            
            maskView.addTapGesture(action: { (_) in
                contentViewController.dismiss(animated: true,
                                              completion: nil)
            })
        }
        
        view.addSubview(presentedView)
        
        let tran = self.presentingViewController.transitionCoordinator
        
        tran?.animate(alongsideTransition: { (_) in
            self.maskView.alpha = 1
        }, completion: nil)
    }
    
    override var frameOfPresentedViewInContainerView:CGRect {
        return self.containerView.map({
            
            switch self.mode {
            case .top:
                switch self.fillMode {
                case .margin:
                    return CGRect(x: 0,
                                  y: 0,
                                  w: $0.bounds.width,
                                  h: $0.bounds.height - self.value)
                case .size:
                    return CGRect(x: 0,
                                  y: 0,
                                  w: $0.bounds.width,
                                  h: self.value)
                }
            case .bottom:
                switch self.fillMode {
                case .margin:
                    return CGRect(x: 0,
                                  y: self.value,
                                  w: $0.bounds.width,
                                  h: $0.bounds.height - self.value)
                case .size:
                    return CGRect(x: 0,
                                  y: $0.bounds.height - self.value,
                                  w: $0.bounds.width,
                                  h: self.value)
                }
            case .left:
                switch self.fillMode {
                case .margin:
                    return CGRect(x: 0,
                                  y: 0,
                                  w: $0.bounds.width - self.value,
                                  h: $0.bounds.height)
                case .size:
                    return CGRect(x: 0,
                                  y: 0,
                                  w: self.value,
                                  h: $0.bounds.height)
                }
            case .right:
                switch self.fillMode {
                case .margin:
                    return CGRect(x: self.value,
                                  y: 0,
                                  w: $0.bounds.width - self.value,
                                  h: $0.bounds.height)
                case .size:
                    return CGRect(x: $0.bounds.width - self.value,
                                  y: 0,
                                  w: self.value,
                                  h: $0.bounds.height)
                }
            }
            
        }) ?? CGRect.zero
    }
}

extension SliderPopViewController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return self
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.isPresenting ? 0.25 : 0.125
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {
            animatePresentationWithTransitionContext(transitionContext: transitionContext)
        } else {
            animateDismissalWithTransitionContext(transitionContext: transitionContext)
        }
    }
    
    func animatePresentationWithTransitionContext(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let presentedController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.to)
            else {
                return
        }
        
        let containerView = transitionContext.containerView
        
        // Position the presented view off the top of the container view
        let frame = transitionContext.finalFrame(for: presentedController)
        
        presentedControllerView.frame = frame
        
        switch self.mode {
        case .left:
            presentedControllerView.center.x -= presentedControllerView.w
        case .right:
            presentedControllerView.center.x += presentedControllerView.w
        case .bottom:
            presentedControllerView.center.y += presentedControllerView.h
        case .top:
            presentedControllerView.center.y -= presentedControllerView.h
        }
        
        containerView.addSubview(presentedControllerView)
        
        // Animate the presented view to it's final position
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: {
                        presentedControllerView.frame = frame
        },
                       completion: {(completed: Bool) -> Void in
            transitionContext.completeTransition(completed)
        })
    }
    
    func animateDismissalWithTransitionContext(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.from)
            else {
                return
        }
        
        // Animate the presented view off the bottom of the view
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: {
                        switch self.mode {
                        case .left:
                            presentedControllerView.center.x -= presentedControllerView.w
                        case .right:
                            presentedControllerView.center.x += presentedControllerView.w
                        case .bottom:
                            presentedControllerView.center.y += presentedControllerView.h
                        case .top:
                            presentedControllerView.center.y -= presentedControllerView.h
                        }
        },
                       completion: {(completed: Bool) -> Void in
            transitionContext.completeTransition(completed)
        })
    }
}
