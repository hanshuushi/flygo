//
//  ExcursionTableViewCell.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Kingfisher

enum ExcursionMoveType {
    case autoFix
    case fixed(CGFloat)
}

protocol ExcursionView : class {
    var moveType:ExcursionMoveType {get set}
    
    var backgroudImage:UIImage? {get set}
    
    var excursion:CGFloat {get set}
    
    var startPositionY:CGFloat {get set}
    
    var excursionDistance:CGFloat {get set}
    
    var cellFrame:CGRect {get}
    
    var backgroudImageView:UIImageView {get}
}


// MARK: - 布局
extension ExcursionView {
    func setImage(url:URL?) {
        weak var weakSelf = self
        
        
        self.backgroudImageView.kf.cancelDownloadTask()
        self.backgroudImageView.kf.setImage(with:url,
                                            completionHandler: {
                                                (_ image: Image?, _ error: NSError?, _ cacheType: CacheType, _ imageURL: URL?) in
                                                if let strongSelf = weakSelf {
                                                    strongSelf.layoutImageView()
                                                }
        })
        
        self.layoutImageView()
    }
    
    func layoutImageView() {
        let cellFrame = self.cellFrame
        
        if let image = backgroudImage, cellFrame.width > 0, cellFrame.height > 0 {
            backgroudImageView.isHidden = false
            
            let size = image.size
            
            let width =  cellFrame.w
            
            let height =  cellFrame.w / size.width * size.height
            
            var fitSize = CGSize(width: width, height: height)
            
            // fit Size
            switch moveType {
            case .autoFix:
                excursionDistance =  cellFrame.h - height
                
                startPositionY = 0
                
            case .fixed(let offset):
                excursionDistance = offset * 2.0
                
                if fitSize.height < ( cellFrame.h + excursionDistance) {
                    let scale = fitSize.height / ( cellFrame.h + excursionDistance)
                    
                    fitSize = CGSize(width: width / scale, height: height / scale)
                }
                
                startPositionY = ( cellFrame.h - fitSize.height) / 2.0 - offset
            }
            
            print("cellFrame = \(cellFrame)")
            
            backgroudImageView.frame = CGRect(x: (width - fitSize.width) / 2.0,
                                              y: 0,
                                              w: fitSize.width,
                                              h: fitSize.height)
            
            excursionLayout()
        } else {
            backgroudImageView.isHidden = true
        }
    }
}


// MARK: - 偏移布局
extension ExcursionView {
    
    fileprivate func excursionLayout() {
        let excursionValue = min(1.0, max(0.0, excursion))
        
        let offsetY = excursionDistance * excursionValue
        
        var  cellFrame = backgroudImageView.frame
        
         cellFrame.y = offsetY + startPositionY;
        
        backgroudImageView.frame =  cellFrame
    }
    
    func scrollInScrollView(_ scrollView:UIScrollView) {
        
        let offsetY = self.cellFrame.maxY - scrollView.contentOffset.y
        
        let rate = offsetY / (scrollView.h +  cellFrame.h)
        
        self.excursion = rate
        
        excursionLayout ()
    }
}



//fileprivate extension ExcursionTableViewCell {

//}
