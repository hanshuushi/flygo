//
//  ExcursionImageView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/4.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class ExcursionImageView: UIView, ExcursionView {
    
    var moveType:ExcursionMoveType = .autoFix
    
    var backgroudImage:UIImage? {
        set(newVal) {
            backgroudImageView.image = newVal
            
            layoutImageView()
        }
        get {
            return backgroudImageView.image
        }
    }
    
    var excursion:CGFloat = 0
    
    var startPositionY:CGFloat = 0
    
    var excursionDistance:CGFloat = 0
    
    var backgroudImageView:UIImageView {
        get {
            if let imageView = self.view.subviews.last as? UIImageView {
                return imageView
            }
            
            let imageView = UIImageView()
            
            self.addSubview(imageView)
            
            self.clipsToBounds = true
            
            return imageView
        }
    }
    
    var cellFrame: CGRect {
        var superview = self.superview
        
        while superview != nil {
            if let scrollView = superview as? UIScrollView {
                
                return scrollView.convert(self.frame,
                                          from: self.superview!)
            }
            
//            if let cell = superview as? UICollectionViewCell {
//                return cell.frame
//            }
//            
//            if let view = superview as? UITableViewHeaderFooterView {
//                return view.frame
//            }
//            
//            if let view = superview as? UICollectionReusableView {
//                return view.frame
//            }
            
            superview = superview!.superview
        }
        
        
        return self.frame
    }
}
