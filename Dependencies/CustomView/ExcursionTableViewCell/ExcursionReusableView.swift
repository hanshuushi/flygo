//
//  ExcursionTableViewCell.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class ExcursionReusableView: UICollectionReusableView, ExcursionView {
    
    var moveType:ExcursionMoveType = .autoFix
    
    var backgroudImage:UIImage? {
        set(newVal) {
            backgroudImageView.image = newVal
            
            layoutImageView()
        }
        get {
            return backgroudImageView.image
        }
    }
    
    var excursion:CGFloat = 0
    
    var startPositionY:CGFloat = 0
    
    var excursionDistance:CGFloat = 0
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.backgroudImageView.kf.cancelDownloadTask()
    }
    
    var _backgroudImageView:UIImageView! = nil
    
    var backgroudImageView:UIImageView {
        get {
            
            if _backgroudImageView == nil {
                _backgroudImageView = UIImageView()
                
                let contentView = UIView()
                
                contentView.addSubview(_backgroudImageView)
                
                self.insertSubview(contentView,
                                   at: 0)
                
                contentView.clipsToBounds = true
            }
            
            return _backgroudImageView
        }
    }
    
    var cellFrame: CGRect {
        return self.frame
    }
}

// MARK: - 偏移布局
extension ExcursionReusableView {
    func scrollInCollectionView(_ collectionView:UITableView) {
        scrollInScrollView(collectionView)
    }
}

// MARK: - 布局
extension ExcursionReusableView {
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        backgroudImageView.superview?.frame = self.bounds
        
        layoutImageView()
    }
}


