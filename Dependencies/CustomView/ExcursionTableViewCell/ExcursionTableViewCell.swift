//
//  ExcursionTableViewCell.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class ExcursionTableViewCell: UITableViewCell, ExcursionView {
    
    var moveType:ExcursionMoveType = .autoFix
    
    var backgroudImage:UIImage? {
        set(newVal) {
            backgroudImageView.image = newVal
            
            layoutImageView()
        }
        get {
            return backgroudImageView.image
        }
    }
    
    var excursion:CGFloat = 0
    
    var startPositionY:CGFloat = 0
    
    var excursionDistance:CGFloat = 0
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
    }
    
    init(reuseIdentifier: String?) {
        
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.backgroudImageView.kf.cancelDownloadTask()
    }
    
    var backgroudImageView:UIImageView {
        get {
            if self.backgroundView == nil {
                self.backgroundView = UIView()
            }
            
            self.backgroundView!.clipsToBounds = true
            
            if (self.backgroundView?.subviews.count ?? 0) < 1 {
                let layer = UIImageView()
                
                self.backgroundView!.addSubview(layer)
                
                return layer
            }
            
            return self.backgroundView!.subviews[0] as! UIImageView
        }
    }
    
    var cellFrame: CGRect {
        return self.frame
    }
}


// MARK: - 偏移布局
extension ExcursionTableViewCell {
    func scrollInTableView(_ tableView:UITableView) {
        
        scrollInScrollView(tableView)
    }
}

// MARK: - 布局
extension ExcursionTableViewCell {
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        layoutImageView()
    }
}


