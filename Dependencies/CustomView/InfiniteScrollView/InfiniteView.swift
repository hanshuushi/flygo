//
//  InfiniteView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/18.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class InfiniteReactiveProperty <T> {
    
    init(_ val:T) {
        value = val
    }
    
    var value:T {
        didSet {
            for handler in handlers {
                handler (value)
            }
        }
    }
    
    var handlers:[(T) -> Void] = []
    
    func onChange(_ handler:@escaping (T) -> Void) {
        handlers.append(handler)
        
        handler (value)
    }
}


@objc protocol InfiniteViewDataSource: NSObjectProtocol {
    func contentViewForInfiniteView(_ InfiniteView:InfiniteView) -> UIView
}

class InfiniteView: UIScrollView {
    @IBOutlet weak var dataSource:InfiniteViewDataSource? = nil
    
    fileprivate var scrollDelegate:UIScrollViewDelegate? = nil
    
    fileprivate var displayViews:[Int: UIView] = [Int: UIView]()
    
    fileprivate var allDisplayFrame:[Int: CGRect] = [Int: CGRect]()
    
    fileprivate var mirrorViews:[UIView] = []
    
    fileprivate var fitSize:CGSize = CGSize.zero
    
    func reloadData() {
        super.delegate = self
        
        super.showsHorizontalScrollIndicator = false
        
        // remove prev
        for one in displayViews.values {
            one.removeSubviews()
        }
        
        for one in mirrorViews {
            one.removeSubviews()
        }
        
        displayViews = [Int: UIView]()
        
        allDisplayFrame = [Int: CGRect]()
        
        guard let _ = dataSource else {
            
            return
        }
        
        if self.size.equalTo(CGSize.zero) {
            return
        }
        
        self.contentSize = CGSize(width: self.w * 100, height: 0)
        
        mirrorViews.append(fetchMirrorView ())
        
        let size = mirrorViews[0].size
        
        let rate = self.h / size.height
        
        let cellSize = CGSize(width: size.width * rate, height: self.h)
        // setup all displayFrame
        
        // point center
        let centerX = self.contentSize.width / 2.0
        
        let centerFrame = CGRect(x: centerX - cellSize.width / 2.0,
                                 y: 0,
                                 w: cellSize.width,
                                 h: cellSize.height)
        
        var index = 0
        
        allDisplayFrame[index] = centerFrame
        
        while true {
            index += 1
            
            let rightFrame = CGRect(x: centerX - cellSize.width / 2.0 + cellSize.width * CGFloat(index),
                                    y: 0,
                                    w: cellSize.width,
                                    h: cellSize.height)
            
            if rightFrame.x > self.contentSize.width {
                break
            }
            
            allDisplayFrame[index] = rightFrame
            
            let leftFrame = CGRect(x: centerX - cellSize.width / 2.0 + cellSize.width * CGFloat(-index),
                                   y: 0,
                                   w: cellSize.width,
                                   h: cellSize.height)
            
            allDisplayFrame[-index] = leftFrame
        }
        
        self.contentOffset = CGPoint(x: centerX - self.w / 2.0,
                                     y: 0)
        
        mirrorViews.append(fetchMirrorView ())
    }
    
}


// MARK: - UIScrollViewDelegate 桥接
extension InfiniteView: UIScrollViewDelegate {
    override var delegate:UIScrollViewDelegate? {
        get {
            return scrollDelegate
        }
        set {
            scrollDelegate = newValue
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewDidScroll?(self)
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewDidZoom?(self)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        jumpToCenter()
        
        scrollDelegate?.scrollViewWillBeginDragging?(self)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        scrollDelegate?.scrollViewWillEndDragging?(scrollView,
                                                   withVelocity: velocity,
                                                   targetContentOffset: targetContentOffset)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollDelegate?.scrollViewDidEndDragging?(self,
                                                  willDecelerate: decelerate)
    }
    
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewWillBeginDecelerating?(scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewDidEndDecelerating?(scrollView)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewDidEndScrollingAnimation?(scrollView)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollDelegate?.viewForZooming?(in: scrollView)
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollDelegate?.scrollViewWillBeginZooming?(self, with: view)
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        scrollDelegate?.scrollViewDidEndZooming?(self,
                                                 with: view,
                                                 atScale: scale)
    }
    
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        if let result = scrollDelegate?.scrollViewShouldScrollToTop?(self) {
            return result
        }
        
        return false
    }
    
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewDidScrollToTop?(self)
    }
    
}

extension InfiniteView {
    fileprivate func jumpToCenter () {
        guard let width = allDisplayFrame[0]?.size.width, width > 0 else {
            return
        }

        
        let boundsCenterX = self.bounds.midX
        
        let contentCenterX = self.contentSize.width / 2.0
        
        var distance = boundsCenterX - contentCenterX
        
        if distance > 0 {
            while distance > width {
                distance -= width
            }
        } else if distance < 0 {
            while distance < -width {
                distance += width
            }
        }
        
        self.contentOffset = CGPoint(x: contentCenterX + distance - self.w / 2.0, y: 0)
    }
}

// MARK: - Cell Dissplay
extension InfiniteView {
    fileprivate func displayCell () {
        let bounds = self.bounds
        
        for (index, frame) in allDisplayFrame {
            if bounds.intersects(frame) {
                if displayViews[index] == nil {
                    let view = fetchMirrorView()
                    
                    self.addSubview(view)
                    
                    view.frame = frame
                    
                    displayViews[index] = view
                }
            } else if let invisibleView = displayViews[index] {
                invisibleView.removeFromSuperview()
                
                mirrorViews.append(invisibleView)
                
                displayViews[index] = nil
            }
        }
    }
    
    fileprivate func fetchMirrorView () -> UIView {
        
        if (mirrorViews.count > 0) {
            let fetchView = mirrorViews[0]
            
            mirrorViews.removeFirst()
            
            return fetchView
        }
        
        return dataSource!.contentViewForInfiniteView(self)
    }
    
    override var contentOffset:CGPoint {
        didSet {
            displayCell ()
        }
    }
}
