//
//  UIGradientView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/20.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class UIGradientView: UIView {
    
    var startPoint:CGPoint = .zero{
        didSet {
            gradientLayer.startPoint = startPoint
        }
    }
    
    var endPoint:CGPoint = CGPoint(x: 1, y: 1){
        didSet {
            gradientLayer.endPoint = endPoint
        }
    }
    
    var startColor:UIColor = .clear{
        didSet {
            gradientLayer.colors?[0] = endColor.cgColor
        }
    }
    
    var endColor:UIColor = .white {
        didSet {
            gradientLayer.colors?[1] = endColor.cgColor
        }
    }
    
    let gradientLayer:CAGradientLayer = {
        (Void) -> CAGradientLayer in
        let layer = CAGradientLayer()
        
        layer.startPoint = .zero
        layer.endPoint = CGPoint(x: 1, y: 1)
        layer.colors = [UIColor.clear, UIColor.white].map({ $0.cgColor })
        layer.locations = [0.0, 1.0]
        
        return layer
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if gradientLayer.superlayer != self.layer {
            self.layer.addSublayer(gradientLayer)
        }
        
        gradientLayer.frame = self.bounds
    }
}
