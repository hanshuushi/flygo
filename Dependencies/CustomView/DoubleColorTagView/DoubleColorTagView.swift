//
//  DoubleColorTagView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/23.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class DoubleColorTagView: UIView {
    
    enum SeparateType : Int {
        case leftUp, rightUp
    }
    
    var type:SeparateType = .rightUp {
        didSet {
            leftLayer.path = getLeftPath()
            
            rightLayer.path = getRightPath()
        }
    }
    
    var leftColor = UIColor.red {
        didSet {
            leftLayer.fillColor = leftColor.cgColor
        }
    }
    
    var rightColor = UIColor.white {
        didSet {
            rightLayer.fillColor = rightColor.cgColor
        }
    }
    
    fileprivate var leftShapeLayer:CAShapeLayer!
    
    fileprivate var rightShapeLayer:CAShapeLayer!
}

extension DoubleColorTagView {
    
    fileprivate var leftLayer:CAShapeLayer {
        get {
            if leftShapeLayer == nil {
                leftShapeLayer = CAShapeLayer()
                leftShapeLayer.path = getLeftPath()
                leftShapeLayer.fillColor = leftColor.cgColor
                leftShapeLayer.lineWidth = 0
                
                self.layer.addSublayer(leftShapeLayer)
            }
            
            return leftShapeLayer
        }
    }
    
    fileprivate var rightLayer:CAShapeLayer {
        get {
            if rightShapeLayer == nil {
                rightShapeLayer = CAShapeLayer()
                rightShapeLayer.path = getRightPath()
                rightShapeLayer.fillColor = rightColor.cgColor
                rightShapeLayer.lineWidth = 0
                
                self.layer.addSublayer(rightShapeLayer)
            }
            
            return rightShapeLayer
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        leftLayer.path = getLeftPath()
        
        rightLayer.path = getRightPath()
    }
}


fileprivate extension DoubleColorTagView {
    func getLeftPath() -> CGPath {
        let path = UIBezierPath()
        
        switch self.type {
        case .leftUp:
            path.move(to: CGPoint(x: 0, y: self.bounds.maxY))
            path.addLine(to: CGPoint(x: self.bounds.maxX, y: self.bounds.maxY))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: 0, y: self.bounds.maxY))
        case .rightUp:
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: self.bounds.maxX, y: 0))
            path.addLine(to: CGPoint(x: 0, y: self.bounds.maxY))
            path.addLine(to: CGPoint(x: 0, y: 0))
        }
        
        path.close()
        
        return path.cgPath
    }
    
    func getRightPath() -> CGPath {
        let path = UIBezierPath()
        
        switch self.type {
        case .leftUp:
            path.move(to: CGPoint(x: self.bounds.maxX, y: 0))
            path.addLine(to: CGPoint(x: self.bounds.maxX, y: self.bounds.maxY))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: self.bounds.maxX, y: 0))
        case .rightUp:
            path.move(to: CGPoint(x: self.bounds.maxX, y: self.bounds.maxY))
            path.addLine(to: CGPoint(x: self.bounds.maxX, y: 0))
            path.addLine(to: CGPoint(x: 0, y: self.bounds.maxY))
            path.addLine(to: CGPoint(x: self.bounds.maxX, y: self.bounds.maxY))
        }
        
        path.close()
        
        return path.cgPath
    }
}
