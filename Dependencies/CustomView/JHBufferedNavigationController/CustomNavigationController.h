//
//  CustomNavigationController.m
//  FlyGo
//
//  Created by Latte on 12/6/16.
//  Copyright (c) 2016 FlyGo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController
<UINavigationControllerDelegate>

@end
