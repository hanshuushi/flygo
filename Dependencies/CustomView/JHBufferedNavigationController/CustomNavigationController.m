//
//  CustomNavigationController.m
//  FlyGo
//
//  Created by Latte on 12/6/16.
//  Copyright (c) 2016 FlyGo Inc. All rights reserved.
//

#import "CustomNavigationController.h"

@interface CustomNavigationController()
<
UIGestureRecognizerDelegate
>

@end

@implementation CustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.interactivePopGestureRecognizer.delegate = self;
    
    self.delegate = self;
    
//    [self.navigationBar setBackIndicatorImage:[[UIImage imageNamed:@"icon_return"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    [self.navigationBar setBackIndicatorTransitionMaskImage:[[UIImage imageNamed:@"icon_return"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

#pragma mark - ovveride
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    self.interactivePopGestureRecognizer.enabled = NO;
    [super pushViewController:viewController animated:animated];
}

#pragma mark - Delegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    self.interactivePopGestureRecognizer.delegate = viewController;
    self.interactivePopGestureRecognizer.enabled = true;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (navigationController.viewControllers.count > 0 &&
        navigationController.viewControllers[0] != navigationController &&
        [navigationController.viewControllers containsObject:viewController])
    {
        viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    }
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    return self.viewControllers.count > 1;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return self.viewControllers.count > 1;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return [gestureRecognizer isKindOfClass:UIScreenEdgePanGestureRecognizer.class] && self.viewControllers.count > 1;
}


//#pragma mark - MFSideMenuContainerDelegate
//
//- (BOOL)shouldOpenLeftMenu {
//    
//    if (self.viewControllers.count == 1 && [self.topViewController isKindOfClass:[YYCandidateHomePageVC class]]) {
//        
//        YYCandidateHomePageVC *vc = (YYCandidateHomePageVC *)self.topViewController;
//        if (vc.isSearchShowing) {
//            return NO;
//        }
//    }
//    
//    if (self.viewControllers.count == 1) {
//            
//        return YES;
//    }
//    
//    return NO;
//}
//
//- (BOOL)shouldOpenRightMenu {
//    
//    if (self.viewControllers.count == 1 && [self.topViewController isKindOfClass:[YYCandidateHomePageVC class]]) {
//        
//        YYCandidateHomePageVC *vc = (YYCandidateHomePageVC *)self.topViewController;
//        if (vc.isSearchShowing) {
//            return NO;
//        }
//    }
//    
//    if (self.viewControllers.count == 1 && [self.viewControllers.firstObject isKindOfClass:[YYYCandidateCardVC class]]) {
//        return YES;
//    }
//    
//    if (self.viewControllers.count == 1 && [self.viewControllers.firstObject isKindOfClass:[YYCandidateHomePageVC class]]) {
//        return YES;
//    }
//    
//    return NO;
//}

@end
