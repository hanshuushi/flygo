//
//  DeleteLabel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/24.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class DeleteLabel: UILabel {
    
    private var _deleteLayer:CALayer! = nil
    
    private var deleteLayer:CALayer {
        get {
            guard let layer = _deleteLayer, layer.superlayer == self.layer else {
                let layer = CALayer()
                
                self.layer.addSublayer(layer)
                
                return layer
            }
            
            return layer
        }
    }
    
    override var textColor: UIColor! {
        get {
            return super.textColor
        }
        set (newVal){
            super.textColor = newVal
            
            deleteLayer.backgroundColor = newVal.cgColor
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
}
