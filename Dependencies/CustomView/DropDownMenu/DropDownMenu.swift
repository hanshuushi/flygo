//
//  DropDownMenu.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/4.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

enum DropDownMenuItemPopStyle {
    case pop
    case custom(selected:(Void)->Void)
    
    func isCustom () -> Bool {
        switch self {
        case .custom(_):
            return true
        default:
            return false
        }
    }
    
    func selectedHandler() {
        switch self {
        case .custom(let selected):
            selected()
        default:
            return
        }
    }
}

protocol DropDownMenuItemType : class {
    var title:String { get }
    
    var image:UIImage? { get }
    
    var highlightedImage:UIImage? { get }
    
    var allowMultiple:Bool { get }
    
    var itemNames:[String] { get }
    
    var values:[Int] { set get }
    
    var syncHandlers:[(Void) -> Void] { set get }
    
    var itemStyle:DropDownMenuItemPopStyle { get }
}

extension DropDownMenuItemType {
    func toggle(in index:Int) {
        if values.contains(index) {
            values.removeFirst(index)
        } else if allowMultiple {
            values.append(index)
        } else {
            values = [index]
        }
        
        for handler in syncHandlers {
            handler()
        }
    }
    
    func popViewShowTransition(frame:CGRect) -> (ori: CGRect, to:CGRect) {
        
        var ori = frame
        
        ori.size.height = 0
        
        return (ori: ori, to:frame)
    }
    
    func popViewHideTransition(frame:CGRect) -> (ori: CGRect, to:CGRect) {
        var to = frame
        
        to.size.height = 0
        
        return (ori: frame, to:to)
    }
}

protocol DropDownMenuStyle {
    var backgroudColor:UIColor { get }
    
    var barHeight:CGFloat { get }
    
    var titleFont:UIFont { get }
    
    var titleColor:UIColor { get }
    
    var titleHighlyColor:UIColor { get }
    
    var contentMarginTop:CGFloat { get }
    
    var tagOffset:CGFloat { get }
    
    var itemHeight:CGFloat { get }
    
    var itemFont:UIFont { get }
    
    var itemFontColor:UIColor { get }
    
    var itemSelectColor:UIColor { get }
    
    var lineColor:UIColor { get }
    
    var lineEdge:CGFloat { get }
    
    var maskColor:UIColor { get }
    
    var shadowColor:UIColor { get }
    
    var shadowHeight:CGFloat { get }
}

class DropDownMenu: UIView {
    
    fileprivate var menuStyle:DropDownMenuStyle?
    
    fileprivate let contentView = UIView()
    
    fileprivate let headView = UIView()
    
    fileprivate var popView:UIView? = nil
    
    fileprivate var maskButton:UIButton? = nil
    
    fileprivate var selectIndex = -1 {
        didSet (oldValue) {
            popView?.removeFromSuperview()
            
            for sub in headView.subviews {
                if let itemView = sub as? ItemView {
                    itemView.selected = itemView.tag == selectIndex
                }
            }
            
            if selectIndex < 0 {
                maskButton?.removeFromSuperview()
                maskButton = nil
                
                popView = nil
                
                return
            }
            
            if maskButton == nil {
                maskButton =  UIButton(type: .custom)
                
                maskButton!.frame = contentView.frame
                maskButton!.backgroundColor = currentStyle.maskColor
                maskButton!.addTarget(self,
                                      action: #selector(DropDownMenu.removePopClick(sender:)),
                                      for: UIControlEvents.touchUpInside)
                
                self.insertSubview(maskButton!, belowSubview: headView)
            }
            
            let aPopView = PopMenuView(style: currentStyle,
                                       item: items[selectIndex])
            
            popView = aPopView
            
            aPopView.itemClickHandler = {
                [weak self] _ in
                
                self?.removePopView()
                self?.removeMask()
                self?.selectIndex = -1
            }
            
            self.view.addSubview(aPopView)
            
            let popItemHeight = currentStyle.itemHeight
            
            let itemWidth = self.bounds.width /// CGFloat(items.count)
            
            let frame = CGRect(x: 0,//CGFloat(selectIndex) * itemWidth,
                               y: headView.bottom,
                               w: itemWidth,
                               h: min(popItemHeight * CGFloat(items[selectIndex].itemNames.count), self.bounds.height - headView.bottom))
            
            aPopView.frame = frame
            aPopView.tag = selectIndex
        }
    }
    
    let shadowView = UIView()
    
    init(contentView:UIView,
         frame:CGRect = CGRect.zero,
         style:DropDownMenuStyle? = nil) {
        menuStyle = style
        
        self.contentView.addSubview(contentView)
        
        super.init(frame: frame)
        
        self.addSubview(self.contentView)
        
        self.addSubview(headView)
        
        shadowView.backgroundColor = currentStyle.lineColor
        
        self.addSubview(shadowView)
        
        headView.backgroundColor = currentStyle.backgroudColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var items:[DropDownMenuItemType] = []
    
    var values:[String:[String]] = [:]
}

// MARK: - Item Control
extension DropDownMenu {
    func add(item:DropDownMenuItemType) {
        let count = items.count
        
        let itemView = ItemView(style: currentStyle,
                                item: item)
        
        itemView.tag = count
        itemView.itemTapHandler = itemTapHandler
        
        headView.addSubview(itemView)
        
        adjustHeadLayout()
    }
    
    func reload() {
        headView.removeSubviews()
        
        weak var weakSelf = self
        
        let handler:(Int) -> Void = {
            (index) -> Void in
            if let strongSelf = weakSelf {
                strongSelf.itemTapHandler(index:index)
            }
        }
        
        for (index, item) in items.enumerated() {
            let itemView = ItemView(style: currentStyle,
                                    item: item)
            
            itemView.tag = index
            itemView.itemTapHandler = handler
            
            headView.addSubview(itemView)
        }
        
        adjustLayout()
    }
    
    func itemTapHandler(index:Int) {
        
        let buttonAnimation = (maskButton == nil)
        
        removePopView()
        
        if selectIndex == index || items[index].itemStyle.isCustom() {
            removeMask()
            
            selectIndex = -1
            
            if items[index].itemStyle.isCustom() {
                items[index].itemStyle.selectedHandler()
            }
            
            return
        }
        
        selectIndex = index
        
        if buttonAnimation {
            maskButton?.alpha = 0
        }
        
        let item = items[index]
        
        let tran = item.popViewShowTransition(frame: popView!.frame)
        
        popView?.frame = tran.ori
        
        UIView.animate(withDuration: 0.25,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: {
                        self.maskButton?.alpha = 1
                        self.popView?.frame = tran.to
        }, completion: nil)
    }
    
    func removePopView () {
        if let image = popView?.getScreenShot(), selectIndex >= 0 {
            let imageView = UIImageView(image: image)
            
            imageView.frame = popView!.frame
            imageView.contentMode = .top
            imageView.clipsToBounds = true
            
            popView!.superview?.addSubview(imageView)
            popView!.isHidden = true
            
            let tran = items[self.selectIndex].popViewHideTransition(frame: imageView.frame)
            
            UIView.animate(withDuration: 0.125,
                           delay: 0,
                           options: .curveEaseIn,
                           animations: {
                            imageView.frame = tran.to
            },
                           completion: { (success) in
                            imageView.removeFromSuperview()
            })
        }
    }
    
    func removeMask () {
        guard let mask = maskButton else {
            return
        }
        
        mask.isHidden = true
        
        let fakeView = UIView()
        
        fakeView.frame = mask.frame
        fakeView.backgroundColor = maskButton?.backgroundColor
        fakeView.isUserInteractionEnabled = false
        
        mask.superview?.insertSubview(fakeView,
                                      belowSubview: mask)
        
        UIView.animate(withDuration: 0.125,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: {
                        fakeView.alpha = 0.0
        },
                       completion: { (success) in
                        if success {
                            fakeView.removeFromSuperview()
                        }
        })
    }
    
    func removePopClick(sender:Any?) {
        removePopView()
        
        removeMask()
        
        selectIndex = -1
    }
}


// MARK: - Pop
extension DropDownMenu {
    class PopMenuView: UIView {
        
        var labels:[UILabel]
        
        let linePadding:CGFloat
        
        let itemHeight:CGFloat
        
        let item:DropDownMenuItemType
        
        let scrollView:UIScrollView
        
        var itemClickHandler:((Int) -> Void)?
        
        init(style:DropDownMenuStyle,
             item:DropDownMenuItemType) {
            self.item = item
            
            let scrollView = UIScrollView()
            
            self.scrollView = scrollView
            
            scrollView.alwaysBounceVertical = false
            
            labels = item.itemNames.map({ (name) -> UILabel in
                let label = UILabel()
                
                label.text = name
                label.font = style.itemFont
                label.textColor = style.itemFontColor
                label.highlightedTextColor = style.itemSelectColor
                label.backgroundColor = UIColor.clear
                label.sizeToFit()
                label.textAlignment = .center
                
                scrollView.addSubview(label)
                
                return label
            })
            
            linePadding = style.lineEdge
            
            itemHeight = style.itemHeight
            
            super.init(frame: .zero)
            
            self.view.addSubview(scrollView)
            self.clipsToBounds = true
            self.backgroundColor = style.backgroudColor
            
            for label in labels {
                label.isUserInteractionEnabled = true
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(PopMenuView.itemTap(sender:)))
                
                label.addGestureRecognizer(tap)
                
                let lineView = UIView()
                
                lineView.backgroundColor = style.lineColor
                lineView.isUserInteractionEnabled = false
                
                label.addSubview(lineView)
            }
            
            adjustLayout()
            
            weak var weakSelf = self
            
            let sync = {
                if let strongSelf = weakSelf {
                    strongSelf.sync()
                }
            }
            
            item.syncHandlers += [sync]
            
            sync()
        }
        
        func adjustLayout() {
            let lineWidth = self.bounds.width - linePadding * 2.0
            
            for (index, label) in labels.enumerated() {
                label.frame = CGRect(x: 0,
                                     y: CGFloat(index) * itemHeight,
                                     w: self.bounds.width,
                                     h: itemHeight)
                
                if label.subviews.count > 0 {
                    let lineView = label.subviews[0]
                    
                    lineView.frame = CGRect(x: index == 0 ? 0 : linePadding,
                                            y: 0,
                                            width: index == 0 ? self.bounds.width : lineWidth,
                                            height: 0.5)
                }
            }
            
            scrollView.frame = self.bounds
            scrollView.contentSize = CGSize(width: 0,
                                            height: scrollView.subviews.reduce(0, { (height, subview) -> CGFloat in
                                                return max(height, subview.bottom)
                                            }))
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            adjustLayout()
        }
        
        func itemTap(sender:UITapGestureRecognizer) {
            if let label = sender.view as? UILabel, let index = labels.index(of: label), index >= 0, index < item.itemNames.count {
                item.toggle(in: index)
                
                itemClickHandler?(index)
            }
        }
        
        func sync() {
            for (index, _) in labels.enumerated() {
                labels[index].isHighlighted = item.values.contains(index)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

// MARK: - Layout
extension DropDownMenu {
    fileprivate class ItemView: UIView {
        let titleLabel = UILabel()
        
        let tagImageView = UIImageView()
        
        let offset:CGFloat
        
        var itemTapHandler:((Int) -> Void)? = nil
        
        let item:DropDownMenuItemType
        
        var selected:Bool = false {
            didSet {
                titleLabel.isHighlighted = selected
                tagImageView.isHighlighted = selected
            }
        }
        
        func tap(gesture:UITapGestureRecognizer) {
            itemTapHandler?(self.tag)
        }
        
        init(style:DropDownMenuStyle,
             item:DropDownMenuItemType) {
            
            self.item = item
            
            offset = style.tagOffset
            
            super.init(frame: CGRect.zero)
            
            self.addSubview(titleLabel)
            
            titleLabel.font = style.titleFont
            titleLabel.textColor = style.titleColor
            titleLabel.highlightedTextColor = style.titleHighlyColor
            titleLabel.backgroundColor = UIColor.clear
            titleLabel.text = item.title
            titleLabel.lineBreakMode = .byTruncatingMiddle
            titleLabel.sizeToFit()
            
            self.addSubview(tagImageView)
            
            tagImageView.image = item.image
            tagImageView.highlightedImage = item.highlightedImage
            tagImageView.sizeToFit()
            
            let tap = UITapGestureRecognizer(target: self,
                                             action: #selector(ItemView.tap(gesture:)))
            
            self.addGestureRecognizer(tap)
            
            weak var weakSelf = self
            
            item.syncHandlers += [{
                if let strongSelf = weakSelf {
                    strongSelf.titleLabel.text = strongSelf.item.title
                    strongSelf.titleLabel.sizeToFit()
                    strongSelf.adjustLayout()
                }
            }]
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            adjustLayout()
        }
        
        func adjustLayout() {
            let c = self.bounds.center
            
            let maxLabelWidth = self.bounds.width - offset * 3.0 - tagImageView.w
            
            titleLabel.sizeToFit()
            
            if titleLabel.w > maxLabelWidth {
                titleLabel.w = maxLabelWidth
            }
            
            titleLabel.centerY = c.y
            
            tagImageView.centerY = c.y
            
            let fillWidth = titleLabel.w + tagImageView.w + offset
            
            let gap = (self.bounds.width - fillWidth) / 2.0
            
            titleLabel.left = gap
            
            tagImageView.right = self.bounds.width - gap
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    /// adjust root view
    fileprivate func adjustRootLayout() {
        let headFrame = CGRect(x: 0,
                               y: 0,
                               w: self.bounds.width,
                               h: currentStyle.barHeight)
        
        headView.frame = headFrame
        
        let contentFrame = CGRect(x: 0,
                                  y: currentStyle.barHeight + currentStyle.contentMarginTop,
                                  w: self.bounds.width,
                                  h: self.bounds.height - currentStyle.barHeight - currentStyle.contentMarginTop)
        
        contentView.frame = contentFrame
        
        if contentView.subviews.count > 0 {
            contentView.subviews[0].frame = contentView.bounds
        }
    }
    
    /// adjust head 
    fileprivate func adjustHeadLayout() {
        let headFrame = headView.frame
        
        /// adjust head item
        let itemWidth = self.bounds.width / CGFloat(items.count)
        
        for (index, subView) in headView.subviews.enumerated() {
            subView.frame = CGRect(x: CGFloat(index) * itemWidth ,
                                   y: 0,
                                   w: itemWidth,
                                   h: headFrame.height)
        }
        
        shadowView.frame = CGRect(x: headFrame.minX,
                                  y: headFrame.maxY,
                                  width: headFrame.width,
                                  height: currentStyle.shadowHeight)
        
        adjustPopView()
    }
    
    /// adjust pop 
    fileprivate func adjustPopView() {
        let itemWidth = self.bounds.width /// CGFloat(items.count)
        
        if let currentPopView = popView as? PopMenuView {
            
//            let tag = CGFloat(currentPopView.tag)
            
            let popItemHeight = currentStyle.itemHeight
            
            currentPopView.frame = CGRect(x: 0,//tag * itemWidth,
                                          y: headView.bottom,
                                          w: itemWidth,
                                          h: min(popItemHeight * CGFloat(items[currentPopView.tag].itemNames.count), self.bounds.height - headView.bottom))
        }
    }
    
    fileprivate func adjustLayout() {
        adjustRootLayout()
        adjustHeadLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustLayout()
    }
}

extension DropDownMenu: DropDownMenuStyle {
    var shadowHeight: CGFloat {
        return 0.5
    }

    var shadowColor: UIColor {
        return UIColor.black
    }

    
    var currentStyle:DropDownMenuStyle {
        return menuStyle ?? self
    }
    
    var backgroudColor:UIColor {
        return UIColor.white
    }
    
    var barHeight:CGFloat {
        return 40.0
    }
    
    var titleFont:UIFont {
        return UIFont.systemFont(ofSize: 20)
    }
    
    var titleColor:UIColor {
        return UIColor.black
    }
    
    var titleHighlyColor:UIColor {
        return UIColor.blue
    }
    
    var contentMarginTop:CGFloat {
        return 0
    }
    
    var tagOffset:CGFloat {
        return 5
    }
    
    var itemHeight:CGFloat {
        return 40.0
    }
    
    var itemFont:UIFont {
        return UIFont.systemFont(ofSize: 15)
    }
    
    var itemFontColor:UIColor {
        return UIColor.black
    }
    
    var itemSelectColor:UIColor {
        return UIColor.red
    }
    
    var lineColor:UIColor {
        return UIColor.darkGray
    }
    
    var lineEdge:CGFloat {
        return 5
    }
    
    var maskColor: UIColor {
        return UIColor.black
    }

}
