//
//  NavTabView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/15.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import SnapKit
import EZSwiftExtensions

@objc
protocol NavTabViewDataSource : NSObjectProtocol {
    func titleArrayForNavTabView(_ navTabView:NavTabView) -> [String]
    
    func navTabView(_ navTabView:NavTabView, viewAtIndex index:Int) -> UIView
    
    @objc optional func navTabView(_ navTabView:NavTabView, normalAttrStringAtIndex index:Int) -> NSAttributedString?
    
    @objc optional func navTabView(_ navTabView:NavTabView, highlyAttrStringAtIndex index:Int) -> NSAttributedString?
}

protocol NavTabStyle {
    var font:UIFont {get}
    
    var tabItemColor:UIColor {get}
    
    var tabHighlyColor:UIColor {get}
    
    var titleBackgroundColor:UIColor {get}
    
    var contentBackgroundColor:UIColor {get}
    
    var contentMarginTop:CGFloat {get}
    
    var barColor:UIColor {get}
    
    var titleHeight:CGFloat {get}
    
    var titlePadding:CGFloat {get}
    
    var titleInset:CGFloat {get}
    
    var barHeight:CGFloat {get}
    
    var shadowColor:UIColor {get}
    
    var shadowHeigh:CGFloat {get}
}

class NavTabView: UIView {
    
    @IBOutlet
    weak var dataSource:NavTabViewDataSource? = nil
    
    var navTabStyle:NavTabStyle? = nil
    
    let shadowView = UIView()
    
    func reloadData () {
        if let prevTitlesView = titleView {
            prevTitlesView.removeFromSuperview()
        }
        
        titleView = TitleView(self)
        
        titleView!.tabIndex = 0
        
        if let prevContentView = contentView {
            prevContentView.removeFromSuperview()
        }
        
        contentView = ContentView(self)
        
        contentView!.pageIndex = 0
        
        self.addSubview(contentView!)
        
        self.addSubview(titleView!)

        shadowView.backgroundColor = currentNavTabStyle.shadowColor
        
        self.addSubview(shadowView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleView?.frame = CGRect(x: 0,
                                  y: 0,
                                  w: self.frame.width,
                                  h: currentNavTabStyle.titleHeight)
        
        titleView?.adjustOffsetXToCurrentPage(CGFloat(titleView!.tabIndex))
        
        contentView?.frame = CGRect(x: 0,
                                    y: currentNavTabStyle.titleHeight + currentNavTabStyle.contentMarginTop,
                                    w: self.frame.width,
                                    h: self.frame.height - currentNavTabStyle.titleHeight - currentNavTabStyle.contentMarginTop)
        
        contentView?.autoResizeContentSize()
        contentView?.pageIndex = Int(titleView!.tabIndex)
        contentView?.scrollToCurrentPage()
        
        shadowView.frame = CGRect(x: 0,
                                  y: titleView?.frame.maxY ?? 0,
                                  w: self.bounds.width,
                                  h: currentNavTabStyle.shadowHeigh)
    }
    
    private var style: NavTabStyle? = nil
    
    /// Tab 标签View及逻辑
    fileprivate class TitleView: UIScrollView {
        
        var leftLayer:CALayer? = nil, rightLayer:CALayer? = nil
        
        unowned let navTabView:NavTabView
        
        var labelCollection:[UIButton] = []
        
        let tabBar:UIView
        
        static func getLabelFrame (frame:CGRect, padding:CGFloat) -> CGRect {
            var labelFrame = frame
            
            labelFrame.origin.x = frame.x + padding
            
            labelFrame.size.width = frame.width - padding * 2.0
            
            return labelFrame
        }
        
        func selectCurrentLabel(index:Int) {
            for (i, button) in labelCollection.enumerated() {
                button.isEnabled = index != i
            }
        }
        
        var tabIndex:CGFloat = 0.0 {
            didSet {
                
                let tabIndexInt = Int(tabIndex)
                
                if (tabIndex < 0) {
                    selectCurrentLabel(index: 0)
                    
                    let gap = 1.0 - min(0.9, -tabIndex)
                    
                    let labelFrame = TitleView.getLabelFrame(frame: labelCollection[0].frame, padding: navTabView.currentNavTabStyle.titleInset)
                    
                    tabBar.frame = CGRect(x: labelFrame.x,
                                          y: tabBar.y,
                                          w: labelFrame.width * gap,
                                          h: tabBar.h)
                } else if (tabIndexInt >= labelCollection.count - 1) {
                    selectCurrentLabel(index: labelCollection.count - 1)
                    
                    let gap = 1.0 - min(0.9, tabIndex - CGFloat(labelCollection.count - 1))
                    
                    let labelFrame = TitleView.getLabelFrame(frame: labelCollection.last!.frame, padding: navTabView.currentNavTabStyle.titleInset)
                    
                    let width = labelFrame.width * gap
                    
                    tabBar.frame = CGRect(x: labelFrame.maxX - width,
                                          y: tabBar.y,
                                          w: labelFrame.width * gap,
                                          h: tabBar.h)
                } else {
                    
                    let leftFrame = TitleView.getLabelFrame(frame: labelCollection[tabIndexInt].frame,
                                                           padding: navTabView.currentNavTabStyle.titleInset)
                    
                    let gap = tabIndex - CGFloat(tabIndexInt)
                    
                    if (gap < 0.5) {
                        selectCurrentLabel(index: tabIndexInt)
                    } else {
                        selectCurrentLabel(index: tabIndexInt + 1)
                    }
                    
                    let rightFrame = TitleView.getLabelFrame(frame: labelCollection[tabIndexInt + 1].frame,
                                                            padding: navTabView.currentNavTabStyle.titleInset)
                    
                    let left = (rightFrame.minX - leftFrame.minX) * gap + leftFrame.minX
                    
                    let width = (rightFrame.width - leftFrame.width) * gap + leftFrame.width
                    
                    tabBar.frame = CGRect(x: left,
                                          y: tabBar.y,
                                          w: width,
                                          h: tabBar.h)
                }
            }
        }
        
        func adjustOffsetXToCurrentPage(_ targetTabIndex:CGFloat) {
            
            let contentWidth = self.contentSize.width
            
            if (contentWidth <= self.w) {
                self.contentOffset = CGPoint(x: 0, y: 0)
                return
            }
            
            let tabIndexInt = Int(targetTabIndex)
            
            if targetTabIndex < 0 {
                self.contentOffset = CGPoint.zero
            } else if tabIndexInt >= labelCollection.count - 1 {
                self.contentOffset = CGPoint(x: self.contentSize.width - self.w, y: 0)
            } else {
                let leftFrame = TitleView.getLabelFrame(frame: labelCollection[tabIndexInt].frame,
                                                        padding: navTabView.currentNavTabStyle.titleInset)
                
                let gap = targetTabIndex - CGFloat(tabIndexInt)
                
                let rightFrame = TitleView.getLabelFrame(frame: labelCollection[tabIndexInt + 1].frame,
                                                         padding: navTabView.currentNavTabStyle.titleInset)
                
                let pointX = min(self.contentSize.width - self.w, max(0, leftFrame.midX + (rightFrame.minX - leftFrame.minX) * gap - self.w / 2.0))
                
                self.contentOffset = CGPoint(x: pointX, y: 0)
            }
        }
        
        init(_ parent:NavTabView) {
            navTabView = parent
            
            tabBar = UIView(x: 0,
                            y: navTabView.currentNavTabStyle.titleHeight - navTabView.currentNavTabStyle.barHeight,
                            w: 0,
                            h: navTabView.currentNavTabStyle.barHeight)
            
            tabBar.backgroundColor = navTabView.currentNavTabStyle.barColor
            
            super.init(frame: CGRect(x: 0,
                           y: 0,
                           width: navTabView.bounds.width,
                           height: navTabView.currentNavTabStyle.titleHeight)
            )
            
            let backgrouodColor = navTabView.currentNavTabStyle.titleBackgroundColor
            
            self.backgroundColor = backgrouodColor
            
            self.showsHorizontalScrollIndicator = false
            
            // 设置Title Label
            if let titleArray:[String] = navTabView.dataSource?.titleArrayForNavTabView(navTabView), titleArray.count > 0 {
                
                let inset = navTabView.currentNavTabStyle.titleInset
                
                let labelColor = navTabView.currentNavTabStyle.tabItemColor
                
                let disableColor = navTabView.currentNavTabStyle.tabHighlyColor
                
                let cellHeight = navTabView.currentNavTabStyle.titleHeight
                
                var x:CGFloat =  navTabView.currentNavTabStyle.titlePadding - navTabView.currentNavTabStyle.titleInset
                
                labelCollection = []
                
                for (index, one) in titleArray.enumerated() {
                    let button = UIButton(type: .custom)
                    
                    labelCollection.append(button)
                    
                    button.setTitle(one, for: .normal)
                    button.setTitleColor(labelColor, for: .normal)
                    button.setTitleColor(disableColor, for: .disabled)
                    button.titleLabel?.font = navTabView.currentNavTabStyle.font
                    
                    if let normalAsstributedString = navTabView.dataSource?.navTabView?(navTabView,
                                                                                       normalAttrStringAtIndex: index) {
                        button.setAttributedTitle(normalAsstributedString, for: .normal)
                    }
                    
                    if let highlyAsstributedString = navTabView.dataSource?.navTabView?(navTabView,
                                                                                        highlyAttrStringAtIndex: index) {
                        button.setAttributedTitle(highlyAsstributedString, for: .disabled)
                    }
                    
                    button.sizeToFit()
                    
                    button.backgroundColor = UIColor.clear
                    button.frame = CGRect(x: x,
                                         y: 0,
                                         w: button.w + 2.0 * inset,
                                         h: cellHeight)
                    button.titleLabel?.textAlignment = .center
                    button.addTarget(self.navTabView,
                                     action: #selector(tabButtonClick(sender:)),
                                     for: UIControlEvents.touchUpInside)
                    
                    button.layer.setValue(index, forKey: "TitleIndex")
                    
                    self.addSubview(button)
                    
                    x = button.right - inset
                }
                
                self.contentSize = CGSize(width: x + navTabView.currentNavTabStyle.titlePadding, height: 0)
                
                self.addSubview(tabBar)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func tabButtonClick(sender:UIButton) {
        let titleIndex = sender.layer.value(forKey: "TitleIndex") as! Int
        
        select(titleIndex: titleIndex,
               animated: true)
    }
    
    func select(titleIndex:Int, animated:Bool) {
        if animated {
            UIView.animate(withDuration: 0.25, animations: {
                self.titleView?.tabIndex = CGFloat(titleIndex)
                self.titleView?.adjustOffsetXToCurrentPage(CGFloat(titleIndex))
            })
            
            contentView?.pageIndex = titleIndex
            contentView?.scrollToCurrentPage()
        } else {
            self.titleView?.tabIndex = CGFloat(titleIndex)
            self.titleView?.adjustOffsetXToCurrentPage(CGFloat(titleIndex))
            
            contentView?.pageIndex = titleIndex
            contentView?.scrollToCurrentPage()
        }
        
    }
    
    private var titleView:TitleView?
    
    fileprivate class ContentView: UIScrollView, UIScrollViewDelegate {
        
        unowned let navTabView:NavTabView
        
        var currentView:UIView!
        
        var viewCollection:[UIView?]
        
        var pageIndex:Int = 0 {
            didSet {
                for (index, item) in viewCollection.enumerated() {
                    if abs(index - pageIndex) > 1 {
                        if let view = item {
                            view.removeFromSuperview()
                            
                            viewCollection[index] = nil
                        }
                    } else {
                        if item == nil, let view = navTabView.dataSource?.navTabView(navTabView, viewAtIndex: index) {
                            viewCollection[index] = view
                            
                            self.addSubview(view)
                            
                            view.frame = CGRect(x: CGFloat(index) * self.w,
                                                y: 0,
                                                width: self.w,
                                                height: self.h)
                        }
                    }
                }
            }
        }
        
        func scrollToCurrentPage() {
            self.contentOffset = CGPoint(x: CGFloat(self.pageIndex) * self.w,
                                         y: 0)
        }
        
        init(_ parent:NavTabView) {
            navTabView = parent
            
            viewCollection = Array(repeating: nil, count: navTabView.titleView?.labelCollection.count ?? 0)
            
            super.init(frame: CGRect(x: 0,
                                     y: navTabView.currentNavTabStyle.titleHeight + navTabView.currentNavTabStyle.contentMarginTop,
                                     w: navTabView.bounds.width,
                                     h: navTabView.bounds.height - navTabView.currentNavTabStyle.titleHeight))
            
            self.delegate = self
            self.backgroundColor = navTabView.currentNavTabStyle.contentBackgroundColor
            self.showsHorizontalScrollIndicator = false
            self.isPagingEnabled = true
        }
        
        func autoResizeContentSize() {
            self.contentSize = CGSize(width: navTabView.bounds.width * CGFloat(viewCollection.count), height: 0)
            
            for (index, view) in viewCollection.enumerated() {
                view?.frame = CGRect(x: CGFloat(index) * self.w,
                                     y: 0,
                                     width: self.w,
                                     height: self.h)
            }
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if (scrollView != self || self.w == 0) {return}
            
            let offsetX = self.contentOffset.x + self.w / 2.0
            
            pageIndex = Int(offsetX / self.w)
            
            navTabView.titleView?.tabIndex = self.contentOffset.x / self.w
        }
        
        func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            let targetX = targetContentOffset.pointee
            
            UIView.animate(withDuration: 0.125) {
                self.navTabView.titleView?.adjustOffsetXToCurrentPage(targetX.x / self.w)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    private var contentView:ContentView?
}

extension NavTabView: NavTabStyle {
    
    fileprivate var currentNavTabStyle:NavTabStyle {
        get {
            guard let thisStyle = navTabStyle else {
                return self
            }
            
            return thisStyle
        }
    }
    
    var font:UIFont {
        get {
            return UIFont.systemFont(ofSize: 15.0)
        }
    }
    
    var tabItemColor:UIColor {
        get {
            return UIColor.blue
        }
    }
    
    var tabHighlyColor:UIColor {
        get {
            return UIColor.gray
        }
    }
    
    var titleBackgroundColor:UIColor {
        get {
            return UIColor.white
        }
    }
    
    var contentBackgroundColor:UIColor {
        get {
            return UIColor.white
        }
    }
    var contentMarginTop: CGFloat {
        get {
            return 0.0
        }
    }
    
    var barColor:UIColor {
        get {
            return UIColor.blue
        }
    }
    
    var titleHeight:CGFloat {
        get {
            return 50.0
        }
    }
    
    var titlePadding:CGFloat {
        get {
            return 10.0
        }
    }
    
    var titleInset:CGFloat {
        get {
            return 30.0
        }
    }
    
    var barHeight:CGFloat {
        get {
            return 5.0
        }
    }
    
    var shadowColor:UIColor {
        get {
            return UIColor.black
        }
    }
    
    var shadowHeigh:CGFloat {
        get {
            return 0.5
        }
    }
    
}
