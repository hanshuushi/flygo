//
//  UIView+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/29.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import CoreGraphics

extension UIView {
    
    func addSubviews(_ views:UIView ...) {
        for one in views {
            self.addSubview(one)
        }
    }
    
    /// UIView截图
    ///
    /// - Returns: 通过View Render出来的图片
    func getScreenShot() -> UIImage? {
        return getScreenShot(in: self.bounds)
    }
    
    func getScreenShot(in bounds:CGRect) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        let result = self.drawHierarchy(in: bounds, afterScreenUpdates: true)
        
        if !result {
            return nil
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
}
