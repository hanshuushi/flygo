//
//  UIViewController+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension UIViewController {
    func showLoading() -> ToastViewController {
        let toastVC = ToastViewController()
        
        self.presentVC(toastVC)
        
        return toastVC
    }
    
    func showLoadingAndToast(_ text:String) -> ToastViewController {
        let toastVC = ToastViewController(loadingAndText: text)
        
        self.presentVC(toastVC)
        
        return toastVC
    }
    
    func showAlert(message:String, handler:@escaping () -> Void) {
        let alertVC = UIAlertController(title: nil,
                                        message: message,
                                        preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: "否",
                                        style: .cancel,
                                        handler: nil))
        
        alertVC.addAction(UIAlertAction(title: "是",
                                        style: .default,
                                        handler: {
                                            _ in
                                            handler()
        }))
        
        self.presentVC(alertVC)
    }
    
    func showAlert(message:String, responser:UIResponder) {
        
        let alertVC = UIAlertController(title: nil,
                                        message: message,
                                        preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: "好的",
                                        style: .cancel,
                                        handler: {
                                            _ in
                                            responser.becomeFirstResponder()
        }))
        
        self.presentVC(alertVC)
    }
    
    func showAlert(message:String, responserHandler:@escaping () -> Void) {
        
        let alertVC = UIAlertController(title: nil,
                                        message: message,
                                        preferredStyle: .alert)
        
        alertVC.addAction(UIAlertAction(title: "好的",
                                        style: .cancel,
                                        handler: {
                                            _ in
                                            responserHandler()
        }))
        
        self.presentVC(alertVC)
    }
    
    @discardableResult
    func showToast(text:String) -> ToastViewController  {
        let toastVC = ToastViewController(text: text)
        
        self.presentVC(toastVC)
        
        return toastVC
    }
}
