//
//  NSAttributedString+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension NSAttributedString {
    
    convenience init(string:String, font:UIFont, textColor:UIColor) {
        self.init(string: string,
                  attributes: [NSFontAttributeName:font,
                               NSForegroundColorAttributeName:textColor])
    }
    
    convenience init(imageNamed:String) {
        guard let image = UIImage(named: imageNamed) else {
            
            self.init()
            
            return
        }
        
        let attach = NSTextAttachment()
        
        attach.image = image
        
        self.init(attachment: attach)
    }
    
    convenience init(imageNamed:String, baseLineOffset:Int) {
        
        let imageAttributedString = NSAttributedString(imageNamed: imageNamed)
        
        let attributedString = NSMutableAttributedString(attributedString: imageAttributedString)
        
        let length = attributedString.length
        
        attributedString.setAttributes([NSBaselineOffsetAttributeName: baseLineOffset], range: NSMakeRange(0, length))
        
        self.init(attributedString: attributedString)
    }
    
    convenience init(string:String, font:UIFont, textColor:UIColor, lineSpace:CGFloat, alignment:NSTextAlignment = .left) {
        
        let ps = NSMutableParagraphStyle()
        
        ps.lineSpacing = lineSpace
        ps.alignment = alignment
        
        self.init(string: string,
                  attributes: [NSParagraphStyleAttributeName:ps,
                               NSFontAttributeName:font,
                               NSForegroundColorAttributeName:textColor])
    }
    
    convenience init(withUnderLine string:String, font:UIFont, textColor:UIColor) {
        self.init(string: string,
                  attributes: [NSFontAttributeName:font,
                               NSForegroundColorAttributeName:textColor,
                               NSUnderlineStyleAttributeName:NSNumber(integerLiteral: NSUnderlineStyle.styleSingle.rawValue)])
    }
    
    convenience init(withDeleteLine string:String, font:UIFont, textColor:UIColor) {
        self.init(string: string,
                  attributes: [NSFontAttributeName:font,
                               NSForegroundColorAttributeName:textColor,
                               NSStrikethroughStyleAttributeName: NSNumber(integerLiteral: NSUnderlineStyle.styleSingle.rawValue),
                               NSStrikethroughColorAttributeName: textColor,
                               NSBaselineOffsetAttributeName: NSNumber(integerLiteral: NSUnderlineStyle.styleSingle.rawValue)])
    }
}
