//
//  NSObject_NSString_Pinyin.h
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/16.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Pinyin.h"

@implementation NSString (Pinyin)

- (NSString *)pinyin {
    NSMutableString *pinyin = [self mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    return [pinyin uppercaseString];
}

@end
