//
//  UIScrollView+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/23.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension UIScrollView {
    
    func setInset(top:CGFloat, bottom:CGFloat) {
        self.contentInset = UIEdgeInsetsMake(top, 0, bottom, 0)
        
        self.scrollIndicatorInsets = self.contentInset
        
        self.contentOffset = CGPoint(x: 0, y: -top)
    }
    
}
