//
//  NSObject_NSString_Pinyin.h
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/16.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Pinyin)

- (NSString *)pinyin;

@end

