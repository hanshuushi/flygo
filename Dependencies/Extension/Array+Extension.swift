//
//  Array+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension Array {
    func from (start index:Int) -> [Element] {
        if index >= self.count {
            return []
        }
        
        var array = [Element]()
        
        for i in index ..< self.count {
            array.append(self[i])
        }
        
        return array.map({ (one)  in
            return one
        })
    }
    
    func mapEnumerated<T>(_ transform: (Int, Element) -> T) -> [T] {
        var newArray = [T]()
        
        for (index, element) in self.enumerated() {
            let obj = transform(index, element)
            
            newArray.append(obj)
        }
        
        return newArray
    }
}
