//
//  UIImage+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/17.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension UIColor {
    func getImage() -> UIImage? {
        let rect = CGRect(x: 0, y: 0, w: 1, h: 1)
     
        UIGraphicsBeginImageContext(rect.size)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.setFillColor(self.cgColor)
        context.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
}
