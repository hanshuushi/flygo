//
//  String+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension String {
    
    func isNumber() -> Bool {
        let scan = Scanner(string: self)
        
        var val:Int = 0

        return scan.scanInt(&val) && scan.isAtEnd
    }
    
    func intFormat() -> String {
        return components(separatedBy:  CharacterSet(charactersIn: "01234567890").inverted).joined(separator: "")
    }
    
    func floatFormat() -> String {
        
        let str = components(separatedBy:  CharacterSet(charactersIn: "01234567890.").inverted).joined(separator: "")
        
        return str
    }
    
    func htmlDecode() -> String {
        guard let data = self.data(using: .utf8) else {
            return ""
        }
        
        let val:UInt = String.Encoding.utf8.rawValue
        
        let number = NSNumber(value: val)
        
        #if DEBUG
            
            do {
                let attrString = try NSAttributedString(data: data,
                                                        options:[NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,
                                                                 NSCharacterEncodingDocumentAttribute: number],
                                                        documentAttributes: nil)
                
                return attrString.string
            } catch (let exp) {
                
                print("exp is \(exp)")
                
                return self
            }
            
        #endif
        
        guard let attrString = (try? NSAttributedString(data: data,
                                                        options:[NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,
                                                                 NSCharacterEncodingDocumentAttribute: number],
                                                        documentAttributes: nil)) else {
                                                            return ""
        }
        
        return attrString.string
    }
    
    func urlEncode() -> String {
        let encodeString = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? self
        
        return encodeString
    }
    
    func isPassword() -> Bool {
        let string = "^[a-zA-Z\\d_]{6,16}$"
        
        let regextestmobile = NSPredicate(format: "SELF MATCHES %@",string)
        
        if ((regextestmobile.evaluate(with: self) == true))
            //            || (regextestcm.evaluate(with: self)  == true)
            //            || (regextestct.evaluate(with: self) == true)
            //            || (regextestcu.evaluate(with: self) == true))
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func isTel() -> Bool {
        
        let mobile = "\\d{1,14}$"
        
//        let  CM = "(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)"
//        let  CU = "(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)"
//        let  CT = "(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)"
        
        let regextestmobile = NSPredicate(format: "SELF MATCHES %@",mobile)
        
//        let regextestcm = NSPredicate(format: "SELF MATCHES %@",CM )
//        
//        let regextestcu = NSPredicate(format: "SELF MATCHES %@" ,CU)
//        
//        let regextestct = NSPredicate(format: "SELF MATCHES %@" ,CT)
        
        if ((regextestmobile.evaluate(with: self) == true))
//            || (regextestcm.evaluate(with: self)  == true)
//            || (regextestct.evaluate(with: self) == true)
//            || (regextestcu.evaluate(with: self) == true))
        {
            return true
        }
        else
        {
            return false
        }
    }
}
