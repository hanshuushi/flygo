//
//  CALayer+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

extension CALayer {
    func changeAnchorPoint(_ anchorPoint:CGPoint) {
        let frame = self.frame
        
        self.anchorPoint = anchorPoint
        
        self.frame = frame
    }
}
