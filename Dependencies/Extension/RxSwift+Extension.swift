//
//  RxSwift+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/25.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import RxSwift
import RxCocoa

extension ObservableType {
    func bindNext<O : ObserverType>(_ observer: O) -> Disposable where O.E == E{
        return self.bind { (element) in
            observer.onNext(element)
        }
    }
}

extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy {
    func driveNext<O : ObserverType>(_ observer: O) -> Disposable where O.E == E{
        return self.drive(onNext: { (element) in
            observer.onNext(element)
        }, onCompleted: nil, onDisposed: nil)
    }
}
