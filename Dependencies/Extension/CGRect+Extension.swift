//
//  CGRect+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension CGRect {
    var center:CGPoint {
        get {
            return CGPoint(x: x + width / 2.0, y: y + height / 2.0)
        }
    }
}


public func + (lhs: CGRect, rhs: CGRect) -> CGRect {
    return CGRect(x: lhs.origin.x + rhs.origin.x,
                  y: lhs.origin.y + rhs.origin.y,
                  w: lhs.size.width + rhs.size.width,
                  h: lhs.size.height + rhs.size.height)
}

public func + (lhs: CGRect, rhs: CGFloat) -> CGRect {
    return CGRect(x: lhs.origin.x - rhs,
                  y: lhs.origin.y - rhs,
                  w: lhs.size.width + rhs * 2,
                  h: lhs.size.height + rhs * 2)
}

public func - (lhs: CGRect, rhs: CGRect) -> CGRect {
    return CGRect(x: lhs.origin.x - rhs.origin.x,
                  y: lhs.origin.y - rhs.origin.y,
                  w: lhs.size.width - rhs.size.width,
                  h: lhs.size.height - rhs.size.height)
}

public func - (lhs: CGRect, rhs: CGFloat) -> CGRect {
    return CGRect(x: lhs.origin.x + rhs,
                  y: lhs.origin.y + rhs,
                  w: lhs.size.width - rhs * 2,
                  h: lhs.size.height - rhs * 2)
}
