//
//  Reusable+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/2.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

extension UITableView {
    func registerView<V:UITableViewHeaderFooterView>(viewClass:V.Type) {
        self.register(viewClass,
                      forHeaderFooterViewReuseIdentifier: viewClass.className)
    }
    
    
    func registerCell<C:UITableViewCell>(cellClass:C.Type){
        self.register(cellClass,
                      forCellReuseIdentifier: cellClass.className)
    }
    
    func dequeueReusableView<V:UITableViewHeaderFooterView>() -> V {
        let identifier = V.className
        
        guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: identifier) as? V else {
            return V(reuseIdentifier: identifier)
        }
        
        return view
    }
    
    func dequeueReusableCell<C:UITableViewCell>(at indexPath:IndexPath) -> C {
        
        let identifier = C.className
        
        guard let cell = self.dequeueReusableCell(withIdentifier: identifier,
                                                  for: indexPath) as? C else {
                                                    return C(style: .default,
                                                             reuseIdentifier: identifier)
        }
        
        return cell
    }
}

extension UICollectionView {
    func registerView(from dict:[String:AnyClass]) {
        for (kind, viewClass) in dict {
            self.registerView(viewClass: viewClass,
                              kind: kind)
        }
    }
    
    func registerHeaderView(viewClass:AnyClass) {
        self.registerView(viewClass: viewClass,
                          kind: UICollectionElementKindSectionHeader)
    }
    
    func registerView(viewClass:AnyClass, kind:String) {
        self.register(viewClass,
                      forSupplementaryViewOfKind: kind,
                      withReuseIdentifier: String(describing: viewClass))
    }
    
    func registerCell(cellClass:AnyClass){
        self.register(cellClass,
                      forCellWithReuseIdentifier: String(describing: cellClass))
    }
    
    func registerCells(_ cellClassList:AnyClass...) {
        for one in cellClassList {
            registerCell(cellClass: one)
        }
    }
    
    func dequeueHeaderView<V:UICollectionReusableView>(indexPath:IndexPath) -> V {
        let identifier = V.className
        
        guard let view = self.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                            withReuseIdentifier: identifier,
                                                               for: indexPath) as? V else {
                                                                return V(frame: CGRect.zero)
        }
        
        return view
    }
    
    func dequeueReusableView<V:UICollectionReusableView>(kind:String, indexPath:IndexPath) -> V {
        let identifier = V.className
        
        guard let view = self.dequeueReusableSupplementaryView(ofKind: kind,
                                                               withReuseIdentifier: identifier,
                                                               for: indexPath) as? V else {
            return V(frame: CGRect.zero)
        }
        
        return view
    }
    
    func dequeueReusableCell<C:UICollectionViewCell>(at indexPath:IndexPath) -> C {
        
        let identifier = C.className
        
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: identifier,
                                                  for: indexPath) as? C else {
                                                    return C(frame: CGRect.zero)
        }
        
        return cell
    }
}
