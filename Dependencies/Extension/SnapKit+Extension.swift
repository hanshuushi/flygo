//
//  SnapKit+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/1.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import SnapKit

extension ConstraintViewDSL {
    func fillToSuperView() {
        guard let view = self.target as? UIView,
            let superView = view.superview else {
            return
        }
        
        makeConstraints { (maker) in
            maker.edges.equalTo(superView).inset(UIEdgeInsets.zero)
        }
    }
    
    func centerInSuperView() {
        guard let view = self.target as? UIView,
            let superView = view.superview else {
                return
        }
        
        makeConstraints { (maker) in
            maker.center.equalTo(superView)
        }
    }
}
