//
//  UIImageView+Extension.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/30.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

extension UIImageView {
    convenience init(named:String) {
        guard let image = UIImage(named: named) else {
            self.init()
            return
        }
        
        self.init(image: image)
    }
}
