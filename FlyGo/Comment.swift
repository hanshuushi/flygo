//
//  Comment.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/26.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import ObjectMapper

extension API {
    
    struct CommentOrder: Model {
        var productName:String = ""
        var productRecStandard:String   = ""
        var pic:URL?                    = nil
        var nickname:String = ""
        var userAvatar:URL? = nil
        var buyScoreLabels:[String] = []
        var serviceScoreLabels:[String] = []
        var serviceLabels:[String]  = []
        var selfSupport:Int = 0
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            productName                 <-      (map["productName"], API.StringTransform())
            productRecStandard          <-      (map["productRecStandard"], API.StringTransform())
            pic                         <-      (map["pic"], API.PicTransform())
            nickname                    <-      (map["nickname"], API.StringTransform())
            userAvatar                  <-      (map["userAvatar"], API.PicTransform())
            buyScoreLabels              <-      map["buyScoreLabels"]
            serviceScoreLabels          <-      map["serviceScoreLabels"]
            serviceLabels               <-      map["serviceLabels"]
            selfSupport                 <-     (map["selfSupport"], API.IntTransform())
        }
        
        static func getCommentLabel(of orderFormid:String) -> Observable<APIItem<CommentOrder>> {
            return HttpSession.get(urlString: "comments/forwardComment",
                                   params: ["orderFormId": orderFormid])
        }
    }
    
    struct CommentCollection: Model, DataSetPageCollection {
        
        var list:[Comment] {
            return self.comments
        }
        
        var otherInfo: Any? {
            return nil
        }
        
        var hasMore:Bool = false
        
        var comments:[Comment] = []
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            hasMore                 <-  (map["hasMore"], API.BoolTransform())
            comments                <-  map["list"]
        }
        
        static func getList(
            of productId:String,
            page:Int = 1,
            pageSize:Int = 10) -> Observable<APIItem<CommentCollection>> {
            
            var params = [String:Any]()
            
            params["productId"] = productId
            params["page"] = page
            params["pageSize"] = pageSize
            
            return HttpSession.get(urlString: "comments/getProductComments",
                                   params: params)
            
        }
    }
    
    struct Comment: Model {
        struct CommentPic: Model {
            var picSeq:Int = 0
            var pic:URL? = nil
            
            init?(map: Map) {
                
            }
            
            mutating func mapping(map: Map) {
                picSeq      <-      (map["picSeq"], API.IntTransform())
                pic         <-      (map["pic"],   API.PicTransform())
            }
            
            init(picSeq:Int, pic:URL) {
                self.picSeq = picSeq
                
                self.pic = pic
            }
            
            func toJSON() -> [String : Any] {
                return ["picSeq" : picSeq, "pic" : pic?.absoluteString ?? ""]
            }
        }
        
        var nickname:String           = ""
        var userAvatar:URL?           = nil
        var productRecStandard:String = ""
        var buyPrice:Float            = 0
        var commentsDesc:String       = ""
        var buyScore:Int              = 1
        var registerTime:Date?        = nil
        var commentsPic:[CommentPic]  = []
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            nickname                <-      (map["nickname"], API.StringTransform())
            userAvatar              <-      (map["userAvatar"], API.PicTransform())
            productRecStandard      <-      (map["productRecStandard"], API.StringTransform())
            buyPrice                <-      (map["buyPrice"], API.FloatTransform())
            buyScore                <-      (map["buyScore"], API.IntTransform())
            commentsDesc            <-      (map["commentsDesc"], API.StringTransform())
            registerTime            <-      (map["registerTime"], API.DateTransform())
            commentsPic             <-      map["commentsPic"]
        }
        
        static func submitCommentWith(orderformId:String, buyScore:Int, serviceScore:Int, commentsDesc:String, serviceSelectedLabel:[String], commentsPic:[String]) -> HttpSession {
            var dict = API.defaultDict
            
            dict["orderformId"] = orderformId
            dict["buyScore"] = buyScore
            dict["serviceScore"] = serviceScore
            dict["commentsDesc"] = commentsDesc
            dict["serviceSelectedLabel"] = serviceSelectedLabel
            dict["commentsPic"] = commentsPic
                .map({ URL.init(string: $0) })
                .filter({ $0 != nil })
                .mapEnumerated({ CommentPic.init(picSeq: $0, pic: $1!).toJSON() })
            
            return HttpSession.post(urlString: "comments/submitComment",
                                    isJson: true,
                                    params: dict)
        }
        
        static func submitCommentWith(orderformId:String, buyScore:Int, serviceScore:Int, commentsDesc:String, serviceSelectedLabel:[String], commentsPic:[String]) -> Observable<PostItem> {
            var dict = API.defaultDict
            
            dict["orderformId"] = orderformId
            dict["buyScore"] = buyScore
            dict["serviceScore"] = serviceScore
            dict["commentsDesc"] = commentsDesc
            dict["serviceSelectedLabel"] = serviceSelectedLabel
            dict["commentsPic"] = commentsPic
                .map({ URL.init(string: $0) })
                .filter({ $0 != nil })
                .mapEnumerated({ CommentPic.init(picSeq: $0, pic: $1!).toJSON() })
            
            return HttpSession.post(urlString: "comments/submitComment",
                                    isJson: true,
                                    params: dict)
        }
    }
}
