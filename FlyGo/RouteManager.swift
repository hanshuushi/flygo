//
//  RouteManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/21.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift

enum RouteActionItem {
    case home
    case search
    case order
    case flyman
    case product(String)
    
    @available(iOS 9.0, *)
    static func routeActionItem(from shortcutItem:UIApplicationShortcutItem) -> RouteActionItem? {
        switch shortcutItem.type {
        case "com.zhiyou.flygo.home":
            return .home
        case "com.zhiyou.flygo.search":
            return .search
        case "com.zhiyou.flygo.order":
            return .order
        default:
            return nil
        }
    }
}

class RouteManager: NSObject {
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) -> UIWindow {
        var rootViewController:UIViewController!
        
        var handler:((Void) -> Void)?
        
        if #available(iOS 9.0, *) {
            if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
                switch shortcutItem.type {
                case "com.zhiyou.flygo.home":
                    rootViewController = MainVC.rootController()
                case "com.zhiyou.flygo.search":
                    rootViewController = MainVC.rootController()
                    
                    handler = {
                        (rootViewController.childViewControllers[0] as! MainVC).showSearchViewController()
                    }
                case "com.zhiyou.flygo.order":
                    rootViewController = MainVC.rootController()
                    
                    handler = {
                        (rootViewController.childViewControllers[0] as! MainVC).showOrderViewController()
                    }
                default:
                    rootViewController = UIStoryboard(name: "Choose",
                                                      bundle: nil).instantiateInitialViewController()!
                }
            } else if let url = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? URL,
                let vc = RouteManager.viewController(from: url, handler: &handler) {
                rootViewController = vc
            } else {
                
                rootViewController = WelcomeVC.rootController()
            }
            
        } else {
            // Fallback on earlier versions
            rootViewController = WelcomeVC.rootController()
        }
        
        let window = UIWindow(frame: UIScreen.main.bounds)
       
        window.rootViewController =  rootViewController //CommonNavigationVC(rootViewController: TesttttttVC()) //rootViewController
        
        window.makeKeyAndVisible()
        
        handler?()
        
        return window
    }
    
    @available(iOS 9.0, *)
    static func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        if let item = RouteActionItem.routeActionItem(from: shortcutItem) {
            shareInstance.actionItemSubject.onNext(item)
        }
    }
    
    static let shareInstance = RouteManager()
    
    let actionItemSubject:PublishSubject<RouteActionItem>
    
    override init() {
        actionItemSubject = PublishSubject()
        
        super.init()
    }
    
    static func viewController(from url:URL, handler: inout ((Void) -> Void)?) -> UIViewController? {
        if url.scheme != "zhiyouflygo" {
            return nil
        }
        
        guard let host = url.host else {
            return nil
        }
        
        switch host {
        case "home":
            return MainVC.rootController()
        case "flyman":
            return FlygoVC.init()
        case "product":
            guard let query = url.query.map({ RouteManager.toDictionaryFrom(query: $0) }),
                let recId = query["recId"] else {
                    return nil
            }
            
            let rootViewController = MainVC.rootController()
            
            handler = {
                (rootViewController.childViewControllers[0] as! MainVC).showProductDetailViewController(of: recId)
            }
            
            return rootViewController
        default:
            return nil
        }
    }
    
    func urlHandler(url:URL) -> Bool {
        if url.scheme != "zhiyouflygo" {
            return false
        }
        
        guard let host = url.host else {
            return false
        }
        
        switch host {
        case "home":
            actionItemSubject.onNext(.home)
            
            return true
        case "flyman":
            actionItemSubject.onNext(.flyman)
            
            return true
        case "product":
            guard let query = url.query.map({ RouteManager.toDictionaryFrom(query: $0) }),
                let recId = query["recId"]else {
                return false
            }
            
            actionItemSubject.onNext(.product(recId))
            
            return true
        default:
            return false
        }
    }
    
    static func toDictionaryFrom(query queryString:String) -> [String:String] {
        let tempArray = queryString.split("&")
        
        var dict = [String:String]()
        
        for one in tempArray {
            let currentArray = one.split("=")
            
            if currentArray.count < 2 {
                continue
            }
            
            if currentArray.count == 2 {
                dict[currentArray[0]] = currentArray[1]
            } else {
                dict[currentArray[0]] = currentArray.from(start: 1).joined(separator: "=")
            }
        }
        
        return dict
    }
}

extension UIViewController {
    func setupRouteObserver(disposeBag:DisposeBag) {
        RouteManager
            .shareInstance
            .actionItemSubject
            .bind {
                (item) in
                
                switch item {
                case .home:
                    let mainViewController = MainVC.rootController()
                    
                    UIApplication.shared.keyWindow?.rootViewController = mainViewController
                case .search:
                    let mainViewController = MainVC.rootController()
                    
                    UIApplication.shared.keyWindow?.rootViewController = mainViewController
                    
                    (mainViewController.childViewControllers[0] as! MainVC).showSearchViewController()
                case .order:
                    let mainViewController = MainVC.rootController()
                    
                    UIApplication.shared.keyWindow?.rootViewController = mainViewController
                    
                    (mainViewController.childViewControllers[0] as! MainVC).showOrderViewController()
                case .product(let recId):
                    let mainViewController = MainVC.rootController()
                    
                    UIApplication.shared.keyWindow?.rootViewController = mainViewController
                    
                    (mainViewController.childViewControllers[0] as! MainVC).showProductDetailViewController(of: recId)
                case .flyman:
                    UIApplication.shared.keyWindow?.rootViewController = FlygoVC()
                }
            }
            .addDisposableTo(disposeBag)
    }
}
