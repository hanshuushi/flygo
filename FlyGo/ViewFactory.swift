//
//  ViewFactory.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class ViewFactory {
    static func view(color:UIColor) -> UIView {
        let view = UIView()
        
        view.backgroundColor = color
        
        return view
    }
    
    static func line() -> UIView { 
        let lineView = view(color: UIConfig.generalColor.lineColor)
        
        lineView.isUserInteractionEnabled = false
        
        return lineView
    }
    
    static func label(font:UIFont,
                      textColor:UIColor,
                      backgroudColor:UIColor? = UIConfig.generalColor.white) -> UILabel {
        let label = UILabel()
        
        label.font = font
        label.textColor = textColor
        label.backgroundColor = backgroudColor
        
        return label
    }
    
    static func generalLabel(generalSize:CGFloat = 15,
                      textColor:UIColor = UIConfig.generalColor.selected ,
                      backgroudColor:UIColor? = UIConfig.generalColor.white) -> UILabel {
        return label(font: UIConfig.generalFont(generalSize),
                     textColor: textColor,
                     backgroudColor: backgroudColor)
    }
    
    static func label(backgroudColor:UIColor? = UIConfig.generalColor.white) -> UILabel {
        let label = UILabel()
        label.backgroundColor = backgroudColor
        
        return label
    }
    
    static func groupedTableViewEmptyView() -> UIView {
        return UIView(x: 0, y: 0, w: 0, h: CGFloat.leastNormalMagnitude)
    }
}

fileprivate class RedButton: UIButton {
    
    var oriBounds:CGRect?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let oriBounds = self.oriBounds, oriBounds == self.bounds {
            return
        }
        
        oriBounds = self.bounds
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: self.w, height: self.h),
                                               false,
                                               UIScreen.main.scale)
        
        guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            
            return
        }

        let path = CGMutablePath()

        let colorSpace = CGColorSpaceCreateDeviceRGB()

        let array = [UIColor(r: 247, g: 107, b: 98).withAlphaComponent(0.95).cgColor, UIColor(r: 253, g: 60, b: 83).withAlphaComponent(0.95).cgColor] as CFArray

        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: array,
                                  locations: [0.0, 1.0])

        let startPoint = CGPoint(x: self.w / 2.0, y: 0)

        let endPoint = CGPoint(x: self.w / 2.0 + self.h * 0.176, y: self.h)

        path.addRect(self.bounds)
        
        
        ctx.addPath(path)
        ctx.clip()
        ctx.drawLinearGradient(gradient!,
                               start: startPoint,
                               end: endPoint,
                               options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(image,
                                for: .normal)
        self.setBackgroundColor(UIConfig.generalColor.labelGray.withAlphaComponent(0.95),
                                forState: .disabled)
    }
}

fileprivate class YellowButton: UIButton {
    
    var oriBounds:CGRect?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let oriBounds = self.oriBounds, oriBounds == self.bounds {
            return
        }
        
        oriBounds = self.bounds
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: self.w, height: self.h),
                                               false,
                                               UIScreen.main.scale)
        
        guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            
            return
        }
        
        let path = CGMutablePath()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let array = [UIColor(r: 247, g: 154, b: 98).withAlphaComponent(0.95).cgColor, UIColor(r: 248, g: 112, b: 94).withAlphaComponent(0.95).cgColor] as CFArray
//        let array = [UIColor(r: 255, g: 255, b: 255).withAlphaComponent(0.95).cgColor, UIColor(r: 0, g: 0, b: 0).withAlphaComponent(0.95).cgColor] as CFArray
        
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: array,
                                  locations: [0.0, 1.0])
        
        let startPoint = CGPoint(x: self.w / 2.0, y: 0)
        
        let endPoint = CGPoint(x: self.w / 2.0 + self.h * 0.176, y: self.h)
        
        path.addRect(self.bounds)
        
        
        ctx.addPath(path)
        ctx.clip()
        ctx.drawLinearGradient(gradient!,
                               start: startPoint,
                               end: endPoint,
                               options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(image,
                                for: .normal)
        self.setBackgroundColor(UIConfig.generalColor.labelGray.withAlphaComponent(0.95),
                                forState: .disabled)
    }
}

// MARK: - Genaral Buttons
extension ViewFactory {
    
    static func button(imageNamed:String) -> UIButton {
        let button = UIButton(type: .custom)
        
        button.setImage(UIImage(named:imageNamed),
                        for: .normal)
        button.sizeToFit()
        
        return button
    }
    
    static func button(imageNamed:String, highlightedImageNamed:String) -> UIButton {
        let button = UIButton(type: .custom)
        
        button.setImage(UIImage(named:imageNamed),
                        for: .normal)
        button.setImage(UIImage(named:highlightedImageNamed),
                        for: .highlighted)
        button.sizeToFit()
        
        return button
    }
    
    static func button(imageNamed:String, disabledImageNamed:String) -> UIButton {
        let button = UIButton(type: .custom)
        
        button.setImage(UIImage(named:imageNamed),
                        for: .normal)
        button.setImage(UIImage(named:disabledImageNamed),
                        for: .disabled)
        button.sizeToFit()
        
        return button
    }
    
    static func serverButton() -> UIButton {
        let serverButton = UIButton(type: .custom)
        
        serverButton.backgroundColor = UIConfig.generalColor.white.withAlphaComponent(0.95)
        serverButton.setImage(UIImage(named:"icon_contact_customer_service"),
                              for: .normal)
        serverButton.setTitle("联系客服",
                              for: .normal)
        serverButton.setTitleColor(UIConfig.generalColor.selected,
                                   for: .normal)
        serverButton.titleLabel?.font = UIConfig.generalFont(10)
        
        serverButton.titleEdgeInsets = UIEdgeInsetsMake(26, 0, 0, 20)
        serverButton.imageEdgeInsets = UIEdgeInsetsMake(0, 36, 15, 0)
        serverButton.addTarget(FlygoUtil.self,
                               action: #selector(FlygoUtil.callServer),
                               for: UIControlEvents.touchUpInside)
        
        return serverButton
    }
    
    static func redButton(title:String) -> UIButton {
        let redButton = RedButton(type: .custom)
        
        redButton.setTitle(title,
                           for: .normal)
        
        redButton.setTitleColor(UIConfig.generalColor.white,
                                for: .normal)
        
//        redButton.setBackgroundColor(UIConfig.generalColor.red.withAlphaComponent(0.95),
//                                     forState: .normal)
//        redButton.setBackgroundColor(UIConfig.generalColor.highlyRed.withAlphaComponent(0.95),
//                                     forState: .highlighted)
        
        redButton.titleLabel?.font = UIConfig.generalFont(15)
        
        return redButton
    }
    
    static func grayButton(title:String) -> UIButton {
        let grayButton = UIButton(type: .custom)
        
        grayButton.setTitle(title,
                           for: .normal)
        
        grayButton.setTitleColor(UIConfig.generalColor.white,
                                for: .normal)
        
        grayButton.setBackgroundColor(UIConfig.generalColor.labelGray.withAlphaComponent(0.95),
                                     forState: .normal)
        
        grayButton.titleLabel?.font = UIConfig.generalFont(15)
        
        return grayButton
    }
    
    static func yellowButton(title:String) -> UIButton {
        let yellowButton = YellowButton(type: .custom)
        
        yellowButton.setTitle(title,
                           for: .normal)
        
        yellowButton.setTitleColor(UIConfig.generalColor.white,
                                for: .normal)
        
        //        redButton.setBackgroundColor(UIConfig.generalColor.red.withAlphaComponent(0.95),
        //                                     forState: .normal)
        //        redButton.setBackgroundColor(UIConfig.generalColor.highlyRed.withAlphaComponent(0.95),
        //                                     forState: .highlighted)
        
        yellowButton.titleLabel?.font = UIConfig.generalFont(15)
        
        return yellowButton
    }
}


// MARK: - Network
extension ViewFactory {
    static func loadingView() -> UIView {
        return LoadingView(type: .red)
    }
}

extension ViewFactory {
    static func deleteActionSheet(from sender:UIView, title:String? = nil, content:String, response:@escaping (Void) -> Void) -> UIAlertController {
        let alertVC = UIAlertController(title: title,
                                        message: content,
                                        preferredStyle: .actionSheet)
        
        alertVC.addAction(UIAlertAction(title: "删除",
                                        style: .destructive,
                                        handler: { (_) in
                                            response()
        }))
        
        alertVC.addAction(UIAlertAction(title: "不要",
                                        style: .cancel,
                                        handler: nil))
        
        if let popover = alertVC.popoverPresentationController {
            popover.sourceView = sender
            popover.sourceRect = sender.bounds
        }
        
        return alertVC
    }
}


// MARK: - Fake Navigation
extension ViewFactory {
    static func blurView() -> UIView {
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        
        blurView.contentView.addSubview(self.view(color: UIColor(white: 0.97, alpha: 0.8)))
        
        return blurView
    }
    
    static func backButton(on viewController:UIViewController) -> UIButton {
        let button = self.button(imageNamed: "icon_return")
        
        button.contentHorizontalAlignment = .left
        button.imageEdgeInsets = UIEdgeInsetsMake(1.5, 0, -1.5, 0)
        button.frame = CGRect(x: 8, y: 26, width: 46, height: 30)
        button.addTarget(viewController,
                         action: #selector(UIViewController.popVC),
                         for: .touchUpInside)
        
        return button
    }
    
    static func navigationTitleLabel(with title:String) -> UILabel {
        let label = ViewFactory.label(font: UIConfig.generalFont(20.0), textColor: UIConfig.generalColor.selected, backgroudColor: .clear)
        
        label.lineBreakMode = .byTruncatingMiddle
        label.text = title
        
        return label
    }
}

