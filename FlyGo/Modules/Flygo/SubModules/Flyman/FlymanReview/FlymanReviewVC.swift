//
//  FlymanReviewVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/20.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift

class FlymanReviewVC: UIViewController {
    let tableView:UITableView = UITableView(frame: .zero,
                                            style: .plain)
    
    var dataSet:TableViewDataSet!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_switch"),
                                                                style: .plain,
                                                                target: FlygoVC.self,
                                                                action: #selector(FlygoVC.switchMain))
        
        self.view.addSubview(tableView)
        
        self.tableView.snp.fillToSuperView()
        self.tableView.backgroundColor = UIConfig.generalColor.white
        self.tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        
        dataSet = TableViewDataSet(tableView,
                                   delegate: self,
                                   dataSource: self,
                                   finishStyle: .none,
                                   refreshEnable:false)
        
        dataSet.isEmpty.value = true
        dataSet.status.value = .normal
        dataSet.emptyDescriptionHandler = { "飞哥身份解锁中，请耐心等待。" }
        dataSet.emptyImageHandler = { UIImage(named:"icon_identity_auditing")! }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

extension FlymanReviewVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}
