//
//  FlymanVerificationVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/17.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import SnapKit
import RxSwift
import RxCocoa
import EZSwiftExtensions

enum FlymanVerificationType {
    case identification
    case passport
    
    var title:String {
        switch self {
        case .identification:
            return "身份证"
        case .passport:
            return "护照"
        }
    }
    
    var type:Int {
        switch self {
        case .identification:
            return 1
        case .passport:
            return 2
        }
    }
    
    var subTitles:[String] {
        switch self {
        case .passport:
            return ["护照正面","手持证件照"]
        case .identification:
            return ["身份证反面","身份证正面","手持证件照"]
        }
    }
    
    var picCount:Int {
        switch self {
        case .identification:
            return 3
        case .passport:
            return 2
        }
    }
    
    var images:[UIImage] {
        switch self {
        case .passport:
            return ["icon_passporet_front","icon_passporet_front&self"].map({ UIImage(named:$0)! })
        case .identification:
            return ["icon_identity_card_front","icon_identity_card_contrary","icon_identity_card_front&self"].map({ UIImage(named:$0)! })
        }
    }
    
    func check(text:String) -> Bool {
        switch self {
        case .identification:
            let predicateString = "(^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}$)"
            
            return NSPredicate(format: "SELF MATCHES %@", predicateString).evaluate(with: text)
        case .passport:
//            let predicateString = "^1[45][0-9]{7}|([P|p|S|s]\\d{7})|([S|s|G|g|E|e]\\d{8})|([Gg|Tt|Ss|Ll|Qq|Dd|Aa|Ff]\\d{8})|([H|h|M|m]\\d{8，10})$"
            
            return true //NSPredicate(format: "SELF MATCHES %@", predicateString).evaluate(with: text)
        }
    }
}

struct FlymanVerificationPic {
    let image:UIImage
    
    let path:String
}

struct FlymanVerificationInfo {
    var pics:[FlymanVerificationPic?]
    
    var imagePath:String {
        return pics
            .filter({ $0 != nil })
            .map({ $0!.path })
            .joined(separator: ",")
    }
    
    var name:String
    
    var cardNo:String
    
    init(count:Int) {
        pics = Array(repeating: nil, count: count)
        
        name = ""
        
        cardNo = ""
    }
}

class FlymanVerificationVC: UITableViewController {
    let verificationType:FlymanVerificationType
    
    let info:Variable<FlymanVerificationInfo>
    
    let bag:DisposeBag
    
    static var inviteCode:String = ""
    
    var picObsDisposables:[Disposable?]
    var nameObsDisposable:Disposable?
    var idObsDisposable:Disposable?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.verificationType = FlymanVerificationVC.tempVerificationTypeForIOS8
        
        self.info = Variable(FlymanVerificationInfo(count: verificationType.picCount))
        
        self.bag = DisposeBag()
        
        self.picObsDisposables = Array(repeating: nil, count: verificationType.picCount)
        
        super.init(nibName: nibNameOrNil,
                   bundle: nibBundleOrNil)
    }
    
    static var tempVerificationTypeForIOS8 = FlymanVerificationType.identification
    
    init(verificationType:FlymanVerificationType) {
        FlymanVerificationVC.tempVerificationTypeForIOS8 = verificationType
        
        self.verificationType = verificationType
        
        self.info = Variable(FlymanVerificationInfo(count: verificationType.picCount))
        
        self.picObsDisposables = Array(repeating: nil, count: verificationType.picCount)
        
        self.bag = DisposeBag()
        
        super.init(style: .grouped)
        
        NotificationCenter
            .default
            .addObserver(self,
                         selector: #selector(FlymanVerificationVC.applicationDidEnterBackground(notification:)),
                         name: .UIApplicationDidEnterBackground,
                         object: nil)
        
        NotificationCenter
            .default
            .addObserver(self,
                         selector: #selector(FlymanVerificationVC.applicationWillEnterForeground(notification:)),
                         name: .UIApplicationWillEnterForeground,
                         object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "\(verificationType.title)认证"
        
        /// button
        let submitButton = ViewFactory.redButton(title: "确认")
        
        submitButton.layer.cornerRadius = 22
        submitButton.layer.masksToBounds = true
        
        let type = verificationType
        
        Driver.combineLatest(info.asDriver(),
                             LocationManager.shareInstance.authorization)
        { (o1, o2) -> Bool in
            return o1.pics.filter({ $0 != nil }).count > 0
                && o1.name.length > 1
                && o1.cardNo.length > 1
                && o2.enable
        }
            .drive(submitButton
                .rx
                .isEnabled)
            .addDisposableTo(bag)
        
        LocationManager
            .shareInstance
            .authorization
            .distinctUntilChanged()
            .drive(onNext: {
                [weak self] (_) in
                self?.tableView.reloadSections([2],
                                              animationStyle: .automatic)
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(bag)
        
        submitButton
            .rx
            .tap
            .withLatestFrom(info.asObservable())
            .bind {
                [weak self] (info) in
                guard let `self` = self else {
                    return
                }
                
                if !type.check(text: info.cardNo) {
                    
                    self.showToast(text: "身份证格式不正确")
                    
                    return
                }
                
                let toast = self.showLoading()
                
                API
                    .UserInfo
                    .verification(name: info.name,
                                  cardNo: info.cardNo,
                                  type: self.verificationType.type,
                                  imgPath: info.imagePath,
                                  flygoInvitationCode: FlymanVerificationVC.inviteCode)
                    .responseNoModel({
                        ez.runThisInMainThread {
                            toast.dismiss(animated: true,
                                          completion: {
                                            self.navigationController?.setViewControllers([FlymanReviewVC()],
                                                                                          animated: true)
                                            
                            })
                        }
                    },
                                     error: {
                                        error in
                                        ez.runThisInMainThread {
                                            toast.displayLabel(text: error.description)
                                        }
                    })
            }
            .addDisposableTo(bag)
        
        /// tableview
        tableView.tableHeaderView = {
            (Void) -> UIView in
            let view = ViewFactory.view(color: UIConfig.generalColor.white)
            
            view.frame = CGRect(x: 0, y: 0, width: 0, height: 20)
            
            return view
        }()
        tableView.tableFooterView = {
            (Void) -> UIView in
            
            let view = ViewFactory.view(color: UIConfig.generalColor.white)
            
            view.frame = CGRect(x: 0, y: 0, width: 0, height: 77)
            
            view.addSubview(submitButton)
            
            submitButton.snp.makeConstraints({ (maker) in
                maker.left.equalTo(view).offset(25)
                maker.right.equalTo(view).offset(-25)
                maker.top.equalTo(view)
                maker.height.equalTo(44)
            })
            
            return view
        }()
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        tableView.registerView(viewClass: HeaderView.self)
        tableView.registerCell(cellClass: CardTableViewCell.self)
        tableView.registerCell(cellClass: TextFieldViewCell.self)
        tableView.registerCell(cellClass: LabelTableViewCell.self)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.keyboardDismissMode = .onDrag
    }
    
    deinit {
        picObsDisposables.forEachEnumerated { (_, disposable) in
            disposable?.dispose()
        }
        nameObsDisposable?.dispose()
        idObsDisposable?.dispose()
        
        NotificationCenter
            .default
            .removeObserver(self)
        
        LocationManager.shareInstance.stopLocation()
    }
    
    func launchLocation() {
        if !LocationManager.shareInstance.locationEnable, let navigationController = self.navigationController {
            ez.runThisAfterDelay(seconds: 0.35) {
                PanelPresentController(presentedViewController: LocationTipVC(nibName: nil, bundle: nil),
                                       presenting: navigationController,
                                       panelLayout: .size(LocationTipVC.sizeWithScreenFrom(bounds: UIScreen.main.bounds))).present()
            }
        }
    }
    
    private let _onceToken = NSUUID().uuidString
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.once(token: self._onceToken) {
            self.launchLocation()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.endEditing(true)
    }
    
    func applicationDidEnterBackground(notification:Notification) {
        LocationManager.shareInstance.stopLocation()
    }
    
    func applicationWillEnterForeground(notification:Notification) {
        launchLocation()
    }
}

extension FlymanVerificationVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return verificationType.picCount
        case 1:
            return 2
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 194
        case 1:
            return 55
        case 2:
            return 40
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        
        let row = indexPath.row
        
        switch section {
        case 0:
            let cell = tableView.dequeueReusableCell(at: indexPath) as CardTableViewCell
            
            cell.set(image: info.value.pics[row]?.image ?? verificationType.images[row],
                     text: verificationType.subTitles[row])
            cell.parentTableViewController = self
            
            picObsDisposables[row]?.dispose()
            picObsDisposables[row] = cell
                .uploaderImage
                .bind(onNext: {
                    [weak self](pic) in
                    if let `self` = self {
                        self.info.value.pics[row] = pic
                        
                        self.tableView.reloadRows(at: [indexPath],
                                                  with: .automatic)
                    }
                })
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(at: indexPath) as TextFieldViewCell
            
            if row == 0 {
                cell.set(text: info.value.name, placeHolder: "真实姓名")
                cell.textField.keyboardType = .default
                
                nameObsDisposable?.dispose()
                nameObsDisposable = cell
                    .textField
                    .rx
                    .text
                    .withLatestFrom(info.asObservable(),
                                    resultSelector: { (text, oriInfo) -> FlymanVerificationInfo in
                                        var newInfo = oriInfo
                                        
                                        newInfo.name = text ?? ""
                                        
                                        return newInfo
                    })
                    .bind(to: info)
            } else if row == 1 {
                cell.set(text: info.value.cardNo, placeHolder: "\(verificationType.title)号")
                cell.textField.keyboardType = .asciiCapable
                
                idObsDisposable?.dispose()
                idObsDisposable = cell
                    .textField
                    .rx
                    .text
                    .withLatestFrom(info.asObservable(),
                                    resultSelector: { (text, oriInfo) -> FlymanVerificationInfo in
                                        var newInfo = oriInfo
                                        
                                        newInfo.cardNo = text ?? ""
                                        
                                        return newInfo
                    })
                    .bind(to: info)
            }
            
            return cell
        case 2:
            return tableView.dequeueReusableCell(at: indexPath) as LabelTableViewCell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableView() as HeaderView
        
        switch section {
        case 0:
            headerView.set(text: "上传证件照")
        case 1:
            headerView.set(text: "身份和证件信息")
        case 2:
            if LocationManager.shareInstance.locationEnable {
                headerView.set(text: "是否获取定位：已获取")
            } else {
                headerView.set(text: "是否获取定位：", red: "未获取(请打开定位)")
            }
        default:
            break
        }
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return ViewFactory.view(color: UIConfig.generalColor.white)
    }
}

extension FlymanVerificationVC {
    class HeaderView: UITableViewHeaderFooterView {
        let titleLabel:UILabel
        
        static let viewHeight:CGFloat = 35
        
        override init(reuseIdentifier: String?) {
            titleLabel = ViewFactory.label(font:UIConfig.generalFont(15),
                                           textColor: UIConfig.generalColor.unselected)
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(titleLabel)
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.bottom.equalTo(self.contentView)
                maker.left.equalTo(self.contentView).offset(25)
            }
        }
        
        func set(text:String) {
            titleLabel.text = text
        }
        
        func set(text:String, red string:String) {
            let attributed = NSMutableAttributedString(string: text,
                                                       font: UIConfig.generalFont(15),
                                                       textColor: UIConfig.generalColor.unselected)
            
            attributed.append(NSAttributedString(string: string,
                                                 font: UIConfig.generalFont(15),
                                                 textColor: UIConfig.generalColor.red))
            
            titleLabel.attributedText = attributed
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FlymanVerificationVC {
    class CardTableViewCell: UITableViewCell {
        
        let cardImageButton:UIButton
        
        var uploaderImage:Observable<FlymanVerificationPic>!
        
        weak var parentTableViewController:UITableViewController?
        
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            cardImageButton = UIButton(type: .custom)
            
            label = ViewFactory.label(font: UIConfig.generalFont(13),
                                      textColor: UIConfig.generalColor.whiteGray)
            
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            uploaderImage = cardImageButton
                .rx
                .tap
                .flatMapLatest({
                    [weak self]
                        (_) -> Observable<FlymanVerificationPic> in
                    return Observable.create({ (obs) -> Disposable in
                        guard let `self` = self,
                            let viewController = self.parentTableViewController,
                            let picker = ImagePicker
                                .uploader(show: viewController,
                                          response: { (image, _, path) in
                                            obs.onNext(FlymanVerificationPic(image: image, path: path ?? ""))
                                })
                            else {
                                obs.onCompleted()
                                
                                return Disposables.create()
                        }
                        
                        return Disposables.create {
                            picker.hide()
                        }
                    })
                    
                })
            
            self.contentView.addSubviews(cardImageButton, label)
            
            cardImageButton.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.left.equalTo(self.contentView).offset(25)
                maker.right.equalTo(self.contentView).offset(-25)
            }
            
            label.snp.makeConstraints { (maker) in
                maker.left.equalTo(cardImageButton)
                maker.top.equalTo(cardImageButton.snp.bottom).offset(10)
                maker.bottom.equalTo(self.contentView)
            }
        }
        
        func set(image:UIImage, text:String) {
            cardImageButton.setImage(image, for: .normal)
            
            let size = image.size
            
            let radio = size.height / size.width
            
            cardImageButton.snp.remakeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.left.equalTo(self.contentView).offset(25)
                maker.right.equalTo(self.contentView).offset(-25)
                maker.height.equalTo(cardImageButton.snp.width).multipliedBy(radio)
            }
            
            label.text = "* \(text)"
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FlymanVerificationVC {
    class TextFieldViewCell: UITableViewCell {
        let textField:UITextField
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            textField = UITextField(frame: .zero)
            textField.font = UIConfig.generalFont(15)
            textField.textColor = UIConfig.generalColor.selected
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(textField)
            
            textField.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(25)
                maker.top.equalTo(self.contentView).offset(10)
                maker.right.equalTo(self.contentView).offset(-25)
                maker.height.equalTo(45)
                maker.bottom.equalTo(self.contentView)
            }
            
            let line = ViewFactory.line()
            
            self.contentView.addSubview(line)
            
            line.snp.makeConstraints { (maker) in
                maker.left.right.bottom.equalTo(textField)
                maker.height.equalTo(0.5)
            }
        }
        
        func set(text:String, placeHolder:String) {
            textField.attributedPlaceholder = NSAttributedString(string: placeHolder,
                                                                 font: UIConfig.generalFont(15),
                                                                 textColor: UIConfig.generalColor.whiteGray)
            textField.text = text
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FlymanVerificationVC {
    class LabelTableViewCell: UITableViewCell {
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            let label = UILabel()
            
            label.numberOfLines = 0
            label.backgroundColor = UIConfig.generalColor.white
            label.attributedText = NSAttributedString(string: "* 定位未获取将无法使用部分飞哥功能，飞哥将不会实时访问您的位置，也不会透露您的隐私，请放心使用。",
                                                      font: UIConfig.generalFont(15),
                                                      textColor: UIConfig.generalColor.whiteGray,
                                                      lineSpace: 8)
            
            self.contentView.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(10)
                maker.left.equalTo(self.contentView).offset(25)
                maker.right.equalTo(self.contentView).offset(-25)
                maker.bottom.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
