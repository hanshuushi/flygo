//
//  UploadChosenVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/17.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class UploadChosenVC: UIViewController {
    
    let scrollView:UIScrollView
    
    init() {
        scrollView = UIScrollView()
        
        super.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        scrollView = UIScrollView()
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "飞哥认证"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_switch"),
                                                                style: .plain,
                                                                target: FlygoVC.self,
                                                                action: #selector(FlygoVC.switchMain))
        
        self.view.addSubview(scrollView)
        self.view.backgroundColor = UIConfig.generalColor.white
        
        scrollView.snp.fillToSuperView()
        scrollView.backgroundColor = self.view.backgroundColor
        
        /// title
        let title = UIImageView(named: "icon_copywriting")
        
        scrollView.addSubview(title)
        
        title.snp.makeConstraints { (maker) in
            maker.top.equalTo(scrollView).offset(30)
            maker.centerX.equalTo(scrollView)
        }
        
        /// header
        let header = UIImageView(named: "icon_point")
        
        scrollView.addSubview(header)
        
        var rate = header.h / header.w
        
        header.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(25)
            maker.right.equalTo(self.view).offset(-25)
            maker.top.equalTo(title.snp.bottom).offset(30)
            maker.height.equalTo(header.snp.width).multipliedBy(rate)
        }
        
        /// passport
        let passport = ViewFactory.button(imageNamed: "button_passport")
        
        scrollView.addSubview(passport)
        
        rate = passport.h / passport.w
        
        passport.addTarget(self,
                           action: #selector(UploadChosenVC.passportPressed(sender:)),
                           for: .touchUpInside)
        passport.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(45)
            maker.right.equalTo(self.view).offset(-45)
            maker.top.equalTo(header.snp.bottom).offset(9)
            maker.height.equalTo(passport.snp.width).multipliedBy(rate)
        }
        
        /// idcard
        let idcard = ViewFactory.button(imageNamed: "button_identity_card")
        
        scrollView.addSubview(idcard)
        
        rate = idcard.h / idcard.w
        
        idcard.addTarget(self,
                         action: #selector(UploadChosenVC.idcardPressed(sender:)),
                         for: .touchUpInside)
        idcard.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(45)
            maker.right.equalTo(self.view).offset(-45)
            maker.top.equalTo(passport.snp.bottom).offset(20)
            maker.bottom.equalTo(scrollView).offset(-20)
            maker.height.equalTo(idcard.snp.width).multipliedBy(rate)
        }
    }
    
    func passportPressed(sender:Any?) {
        let vc = FlymanVerificationVC(verificationType: .passport)
        
        self.navigationController?.pushViewController(vc,
                                                      animated: true)
    }
    
    func idcardPressed(sender:Any?) {
        let vc = FlymanVerificationVC(verificationType: .identification)
        
        self.navigationController?.pushViewController(vc,
                                                      animated: true)
    }
}
