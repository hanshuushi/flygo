//
//  FlymanActivateVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/12.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import EZSwiftExtensions

class FlymanActivateVC: UITableViewController {
    
    let disposeBag:DisposeBag
    
    let labelCell = LabelCell()
    
    let textFieldCell = TextFieldCell()
    
    let buttonCell = ButtonCell()
    
    init() {
        disposeBag = DisposeBag()
        
        super.init(style: .plain)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        disposeBag = DisposeBag()
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "激活"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_switch"),
                                                                style: .plain,
                                                                target: FlygoVC.self,
                                                                action: #selector(FlygoVC.switchMain))
        
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 10
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.keyboardDismissMode = .onDrag
        
        // rx
        textFieldCell
            .textField
            .rx
            .text
            .map({ $0 ?? "" })
            .map({ $0.length == 6 && $0.isNumber() })
            .bind(to: buttonCell.submitButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        
        buttonCell
            .submitButton
            .rx
            .tap
            .withLatestFrom(textFieldCell.textField.rx.text.map({ $0 ?? "" }))
            .bind {
                [weak self] (text) in
                
                guard let `self` = self else {
                    return
                }
                
                let loadingView = self.showLoading()
                
                UserManager.shareInstance.validate(code: text) {
                     (error) in
                    ez.runThisInMainThread {
                        if let _error = error {
                            if let httperror = _error as? HttpError {
                                loadingView.displayLabel(text: httperror.description)
                            }
                        } else {
                            loadingView.dismiss(animated: false,
                                                completion: {
                                                    self.navigationController?.pushViewController(UploadChosenVC(),
                                                                                                  animated: true)
                            })
                        }
                    }
                }
            }
            .addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
//321 44 215
extension FlymanActivateVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            
            return false
        }
        
        return true
    }
}

extension FlymanActivateVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return labelCell
        case 1:
            return textFieldCell
        default:
            return buttonCell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return 44
        case 2:
            return 215
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 321
        case 1:
            return 44
        case 2:
            return 215
        default:
            return .leastNormalMagnitude
        }
    }
}

extension FlymanActivateVC {
    class LabelCell: UITableViewCell {
        init() {
            let label = UILabel()
            
            label.backgroundColor = UIConfig.generalColor.white
            label.numberOfLines = 0
            label.attributedText = NSAttributedString(string: "亲爱的用户，\n欢迎加入飞哥内测！\n想要获取飞哥资格，\n请联系飞购客服获得邀请码。\n感谢您对飞购的支持！\nwechat：flygo-service",
                                                      font: UIConfig.generalFont(15),
                                                      textColor: UIConfig.generalColor.unselected,
                                                      lineSpace: 10,
                                                      alignment: .center)
            
            super.init(style: .default, reuseIdentifier: "Label")
            
            self.contentView.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(80)
                maker.centerX.equalTo(self.contentView)
                maker.bottom.equalTo(self.contentView).offset(-65)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FlymanActivateVC {
    class TextFieldCell: UITableViewCell {
        let textField = UITextField()
        
        init() {
            super.init(style: .default, reuseIdentifier: "TextField")
            
            self.contentView.addSubview(textField)
            
            textField.attributedPlaceholder = NSAttributedString(string: "请输入6位邀请码",
                                                                 font: UIConfig.generalFont(15),
                                                                 textColor: UIConfig.generalColor.whiteGray)
            textField.font = UIConfig.generalFont(15)
            textField.textColor = UIConfig.generalColor.selected
            textField.backgroundColor = UIConfig.generalColor.white
            textField.textAlignment = .center
            textField.enablesReturnKeyAutomatically = true
            textField.returnKeyType = .done
            textField.keyboardType = .numberPad
            textField.snp.makeConstraints { (maker) in
                maker.top.centerX.equalTo(self.contentView)
                maker.size.equalTo(CGSize(width: 117, height: 44))
            }
            
            let line = ViewFactory.line()
            
            self.contentView.addSubview(line)
            
            line.snp.makeConstraints { (maker) in
                maker.top.equalTo(textField.snp.bottom)
                maker.left.right.equalTo(textField)
                maker.height.equalTo(0.5)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FlymanActivateVC {
    class ButtonCell: UITableViewCell {
        let submitButton = ViewFactory.redButton(title: "确认激活")
        
        init() {
            super.init(style: .default, reuseIdentifier: "TextField")
            
            self.contentView.addSubview(submitButton)
            
            submitButton.layer.cornerRadius = 22
            submitButton.layer.masksToBounds = true
            submitButton.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(25)
                maker.right.equalTo(self.contentView).offset(-25)
                maker.height.equalTo(44)
                maker.top.equalTo(self.contentView).offset(35)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
