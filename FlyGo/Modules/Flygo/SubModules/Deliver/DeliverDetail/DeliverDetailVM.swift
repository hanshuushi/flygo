//
//  DeliverDetailVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/5.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct DeliverItem {
    var content:String
    
    var time:String
    
    init(model:API.DeliverItem) {
        content = model.acceptStation
        time    = model.acceptTime
    }
}

class DeliverDetailVM: ViewModel {
    
    let shipperCode:String
    
    let shippingSn:String
    
    init(shipperCode:String, shippingSn:String) {
        self.shipperCode    =   shipperCode
        self.shippingSn     =   shippingSn
    }
    
    func bind(to view: DeliverDetailVC) -> DisposeBag? {
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .none)
        
        let shipperCode = self.shipperCode
        
        let shippingSn = self.shippingSn
        
        let getRequest:((Void) -> Observable<APIItem<ModelList<API.DeliverItem>>>) = {
            return API.DeliverItem.getDetail(shipperCode: shipperCode,
                                             shippingSn: shippingSn)
        }
        
        let bag = dataSet
            .refrence(getRequest: getRequest) {
                (list) in
                
                view.deliverItems = list.map({ DeliverItem(model:$0) })
        }
        
        return bag
    }
    
}
