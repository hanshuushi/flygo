//
//  DeliverDetailVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/5.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import SnapKit
import RxSwift
import RxCocoa

class DeliverDetailVC: UIViewController, PresenterType {
    
    let viewModel:DeliverDetailVM
    
    let tableView = UITableView(frame: .zero,
                                style: .plain)
    
    var bindDisposeBag: DisposeBag?
    
    init(shipperCode:String = "SF", shippingSn:String) {
        viewModel = DeliverDetailVM(shipperCode: shipperCode, shippingSn: shippingSn)
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "查看物流"
        
        self.view.addSubview(self.tableView)
        
        self.tableView.snp.fillToSuperView()
        self.tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 67
        self.tableView.allowsSelection = false
        self.tableView.allowsMultipleSelection = false
        self.tableView.sectionFooterHeight = 35
        self.tableView.sectionHeaderHeight = 0
        self.tableView.registerCell(cellClass: TimeLineTableViewCell.self)
        
        bind()
    }
    
    var deliverItems:[DeliverItem] = []
}

extension DeliverDetailVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliverItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as TimeLineTableViewCell
        
        cell.set(item: deliverItems[indexPath.row],
                 row: indexPath.row,
                 rowCount: deliverItems.count)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension DeliverDetailVC {
    class TimeLineTableViewCell: UITableViewCell {
        
        let circle:UIView
        
        let line:UIView
        
        let label:UILabel
        
        var circleConstraint:Constraint!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            circle = ViewFactory.view(color: UIConfig.generalColor.selected)
            circle.layer.cornerRadius = 5
            circle.layer.masksToBounds = true
            
            line = ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
            
            label = UILabel()
            
            super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubviews(label)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.unselected
            self.label.highlightedTextColor = UIConfig.generalColor.selected
            self.label.backgroundColor = UIConfig.generalColor.white
            self.label.numberOfLines = 0
            
            self.contentView.insertSubview(line, at: 0)
            self.contentView.addSubview(circle)
            
            circle.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(self.contentView.snp.left).offset(30)
                circleConstraint = maker.width.equalTo(10).constraint
                maker.height.equalTo(circle.snp.width)
                maker.centerY.equalTo(self.contentView.snp.top).offset(30)
            }
            
            self.label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.left.equalTo(circle.snp.centerX).offset(30)
//                maker.centerY.equalTo(circle)
                maker.right.equalTo(self.contentView).offset(-20)
                maker.bottom.equalTo(self.contentView).offset(0)
            })
        }
        
        func set(item:DeliverItem, row:Int, rowCount:Int) {
            let highlighted = row == 0
            
            self.label.isHighlighted = highlighted
            
            self.circle.backgroundColor = highlighted ? UIConfig.generalColor.red : UIConfig.generalColor.whiteGray
            
            self.circleConstraint.update(offset: highlighted ? 20 : 10)
            
            self.circle.layer.cornerRadius = highlighted ? 10 : 5
            
            let attributedString = NSMutableAttributedString(string: "\(item.content)",
                font: UIConfig.generalFont(15),
                textColor: highlighted ? UIConfig.generalColor.selected : UIConfig.generalColor.unselected,
                lineSpace: 2)
            
            attributedString
                .append(NSAttributedString(string: "\n\(item.time)",
                    font: UIConfig.generalFont(13),
                    textColor: highlighted ? UIConfig.generalColor.selected : UIConfig.generalColor.unselected,
                    lineSpace: 10))
            
            self.label.attributedText = attributedString
//            self.label.text = item.
            
//            if let logistics = item.logistics, logistics.count > 0 {
//                
//                let attributedString = NSMutableAttributedString()
//                
//                for (index, one) in logistics.enumerated() {
//                    let info = NSAttributedString(string: one.0 + "\n",
//                                                  font: UIConfig.generalFont(12),
//                                                  textColor: UIConfig.generalColor.unselected,
//                                                  lineSpace: 10)
//                    
//                    attributedString.append(info)
//                    
//                    var timeString = one.1?.generalFormartString ?? ""
//                    
//                    if index < (logistics.count - 1) {
//                        timeString += "\n"
//                    }
//                    
//                    let time = NSAttributedString(string: timeString,
//                                                  font: UIConfig.arialFont(11),
//                                                  textColor: UIConfig.generalColor.whiteGray,
//                                                  lineSpace: 15)
//                    
//                    attributedString.append(time)
//                }
//                
//                labelConstraint.update(offset: 20)
//                
//                self.subLabel.attributedText = attributedString
//            } else if let date = item.date?.generalFormartString {
//                self.subLabel.attributedText = NSAttributedString(string: date,
//                                                                  font: UIConfig.arialFont(12),
//                                                                  textColor: highlighted ? UIConfig.generalColor.selected : UIConfig.generalColor.unselected)
//                labelConstraint.update(offset: 10)
//            } else {
//                self.subLabel.text = ""
//            }
            
            if row == 0 && rowCount == 1 {
                self.line.isHidden = true
            } else if row == 0 {
                self.line.isHidden = false
                self.line.snp.remakeConstraints({ (maker) in
                    maker.width.equalTo(2)
                    maker.centerX.equalTo(circle)
                    maker.top.equalTo(circle.snp.centerY)
                    maker.bottom.equalTo(self.contentView)
                })
            } else if row >= (rowCount - 1) {
                self.line.isHidden = false
                self.line.snp.remakeConstraints({ (maker) in
                    maker.width.equalTo(2)
                    maker.centerX.equalTo(circle)
                    maker.bottom.equalTo(circle.snp.centerY)
                    maker.top.equalTo(self.contentView)
                })
            } else {
                self.line.isHidden = false
                self.line.snp.remakeConstraints({ (maker) in
                    maker.width.equalTo(2)
                    maker.centerX.equalTo(circle)
                    maker.bottom.equalTo(self.contentView)
                    maker.top.equalTo(self.contentView)
                })
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
