//
//  OrderDeliverVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/29.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension TimePickerViewController {
    static func time (showOn parent:UIViewController?) -> Observable<Date> {
        return Observable.create({ [weak parent] (observer) -> Disposable in
            guard let parentController = parent else {
                observer.onCompleted()
                return Disposables.create()
            }
            
            let picker = TimePickerViewController(confirmHandler: {
                (time) in
                observer.onNext(time)
                observer.onCompleted()
            })
            
            SliderPopViewController(picker,
                                    mode: .bottom,
                                    fillMode: .size,
                                    value: 180).present(on: parentController)
            
            return Disposables.create()
        })
    }
}

class OrderDeliverVC: UIViewController {
    
    static func order(showOn parent:UIViewController, vendorOrderInfoId:String, orderformId:String) -> Observable<(Date, AddressItem)> {
        return Observable.create {
            [weak parent](obs) -> Disposable in
            
            guard let `parent` = parent else {
                obs.onCompleted()
                
                return Disposables.create()
            }
            
            let vc = OrderDeliverVC(vendorOrderInfoId: vendorOrderInfoId,
                                    orderformId: orderformId)
            
            vc.successHandler = {
                (date, address)  in
                
                obs.onNext((date, address))
                obs.onCompleted()
            }
            
            parent.navigationController?.pushViewController(vc,
                                                            animated: true)
            
            return Disposables.create ()
        }
    }
    
    let tableView:UITableView = UITableView(frame: .zero,
                                            style: .grouped)
    
    let submitButton = {
        (Void) -> UIButton in
        let button = ViewFactory.redButton(title: "预约取件")
        
        button.layer.cornerRadius = 22
        button.layer.masksToBounds = true
        
        return button
    }()
    
    func checkButton() {
        submitButton.isEnabled = orderTime != nil && addressItem != nil
    }
    
    var orderTime:Date? {
        didSet {
            if let time = orderTime {
                
                let endTime = Date(timeIntervalSince1970: time.timeIntervalSince1970 + 60 * 60)
                
                let string = "\(time.toString(format: "YYYY-MM-dd HH")):00 ~ \(endTime.toString(format: "HH")):00"
                
                timeCell.set(title: "预约取件时间",
                             info: string,
                             showAccess: true)
            } else {
                timeCell.set(title: "预约取件时间",
                             info: "",
                             showAccess: true)
            }
            
            self.tableView.reloadData()
            
            checkButton()
        }
    }
    
    let timeCell:InfoTableViewCell = {
       (Void) -> InfoTableViewCell in
        let cell = InfoTableViewCell(style: .default,
                                     reuseIdentifier: "InfoTableViewCell")
        
        cell.label.snp.remakeConstraints({ (maker) in
            maker.top.equalTo(cell.contentView).offset(20)
            maker.bottom.equalTo(cell.contentView).offset(-20)
            maker.left.equalTo(cell.contentView).offset(15)
        })
        
        cell.infoLabel.snp.remakeConstraints({ (maker) in
            maker.top.equalTo(cell.label)
            maker.bottom.equalTo(cell.label)
            maker.right.equalTo(cell.contentView).offset(-15)
        })
        
        cell.set(title: "预约取件时间",
                 info: "",
                 showAccess: true)
        
        return cell
    }()
    
    var addressItem:AddressItem? {
        didSet {
            addressCell.item = addressItem
            
            self.tableView.reloadData()
            
            checkButton()
        }
    }
    
    let addressCell = AddressTableViewCell()
    
    let vendorOrderInfoId:String
    
    let orderformId:String
    
    init(vendorOrderInfoId:String, orderformId:String) {
        self.vendorOrderInfoId = vendorOrderInfoId
        self.orderformId       = orderformId
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "预约取件"
        
        /// tableview
        self.view.addSubview(tableView)
        
        tableView.snp.fillToSuperView()
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.separatorStyle = .none
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerView(viewClass: FooterView.self)
        
        /// button
        self.view.addSubview(submitButton)
        
        submitButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(self.view).offset(-15)
            maker.left.equalTo(self.view).offset(15)
            maker.bottom.equalTo(self.view).offset(-20)
            maker.height.equalTo(44)
        }
        submitButton.addTarget(self,
                               action: #selector(OrderDeliverVC.submit),
                               for: .touchUpInside)
        
        /// obsever
        bindAllOberserver()
        
        checkButton()
    }
 
    let disposeBag = DisposeBag()
    
    var successHandler:((Date, AddressItem) -> Void)?
    
    func submit() {
        guard let addressId = addressItem?.id, let startDate = orderTime else {
            
            self.showToast(text: "输入不完整")
            
            return
        }
        
        let toast = self.showLoading()
        
        API.FlightmentTicket.onlineOrder(vendorOrderInfoId: vendorOrderInfoId,
                                customerAddressId: addressId,
                                orderformId: orderformId,
                                startDate: startDate.toString(format: "yyyy-MM-dd HH:mm:ss"),
                                endDate: Date(timeIntervalSince1970: startDate
                                    .timeIntervalSince1970 + 60 * 60)
                                    .toString(format: "yyyy-MM-dd HH:mm:ss"))
            .responseNoModel({
                toast.dismiss(animated: true,
                              completion: {
                                
                                let alertVC = UIAlertController(title: nil,
                                                                message: "已预约取件，快递小哥取件后请进行”扫码发货”操作",
                                                                preferredStyle: .alert)
                                
                                let alertAction = UIAlertAction(title: "了解",
                                                                style: .default,
                                                                handler: { (_) in
                                                                    self.successHandler?(self.orderTime!,
                                                                                         self.addressItem!)
                                                                    
                                                                    self.navigationController?.popViewController(animated: true)
                                })
                                
                                alertVC.addAction(alertAction)
                                
                                self.present(alertVC,
                                             animated: true,
                                             completion: nil)
                                
                })
            }) { (error) in
                toast.displayLabel(text: error.description)
        }
    }
}

extension OrderDeliverVC {
    func bindAllOberserver() {
        /// adress
        let defaultAddress = API
            .Address
            .getList()
            .filter{$0.isSuccess}
            .map{$0.model?.list ?? []}.map { (items) -> API.Address? in
                for one in items {
                    if one.isDefault {
                        return one
                    }
                }
                
                return items.first
            }
            .map({ $0.map({ AddressItem(model:$0) })})
            .startWith(nil)
            .shareReplay(1)
        
        let addressSelected = tableView
            .rx
            .itemSelected
            .filter{ $0.section == 0 && $0.row == 0 }
            .map({[weak self] _ in AddressSelectorVM
                .addressItem(showOn: self)
                .take(1).map({ (item) -> AddressItem? in return item}) })
            .startWith(defaultAddress)
            .switchLatest()
            .shareReplay(1)
        
        addressSelected
            .bind {
                [weak self] (item) in
                self?.addressItem = item
            }
            .addDisposableTo(disposeBag)
        
        /// time
        tableView
            .rx
            .itemSelected
            .filter{ $0.section == 1 && $0.row == 0 }
            .map({[weak self] _ in
                TimePickerViewController
                    .time(showOn: self)
                .take(1)
            })
            .switchLatest()
            .bind {
                [weak self] (time) in
                self?.orderTime = time
            }
            .addDisposableTo(disposeBag)
    }
}

extension OrderDeliverVC {
    class FooterView: UITableViewHeaderFooterView {
        
        let label:UILabel
        
        override init(reuseIdentifier: String?) {
            
            label = ViewFactory.generalLabel(generalSize: 13,
                                             textColor: UIConfig.generalColor.labelGray,
                                             backgroudColor: UIConfig.generalColor.backgroudWhite)
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(label)
            
            label.numberOfLines = 0
            label.text = "注：请您在航班到达后的48小时内发货，发货时间每推迟24小时，收益将减少30%。"
            label.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(15, 15, 0, 15))
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension OrderDeliverVC {
    typealias InfoTableViewCell = UserAccountVC.InfoTableViewCell
    
    typealias AddressTableViewCell = CartConfirmVC.AddressTableViewCell
}

extension OrderDeliverVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 1 {
            return tableView.dequeueReusableView() as FooterView
        }
        return UITableViewHeaderFooterView(reuseIdentifier: "Space")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0, indexPath.row == 0 {
            return addressCell
        } else if indexPath.section == 1, indexPath.row == 0 {
            return timeCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0, indexPath.row == 0 {
            return AddressTableViewCell.height(with: tableView.w,
                                               and: addressItem)
        } else if indexPath.section == 1, indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0, indexPath.row == 0 {
            return 100
        } else if indexPath.section == 1, indexPath.row == 0 {
            return 35
        }
        
        return 0
    }
    
}
