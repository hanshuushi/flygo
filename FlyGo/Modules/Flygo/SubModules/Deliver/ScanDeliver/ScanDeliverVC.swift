//
//  ScanDeliverVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/5.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import EZSwiftExtensions

class ScanDeliverVC: UIViewController, PresenterType {
    
    let viewModel:ScanDeliverVM
    
    var bindDisposeBag: DisposeBag?
    
    let scanEvent:PublishSubject<Void> = PublishSubject()
    
    let arriveDate:Date
    
    init(vendorOrderInfoId:String, orderFormId:String, arriveDate:TimeInterval) {
        self.viewModel = ScanDeliverVM(vendorOrderInfoId:vendorOrderInfoId,
                                       orderFormId: orderFormId)
        
        self.arriveDate = Date(timeIntervalSince1970: arriveDate)
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "扫描运单条形码"
        
        self.view.addSubview(tableView)
        
        tableView.snp.fillToSuperView()
        tableView.separatorStyle = .none
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        
        orderCell.scanButton.addTarget(self,
                                       action: #selector(ScanDeliverVC.scanButtonPressed(sender:)),
                                       for: UIControlEvents.touchUpInside)
        
        bind()
    }
    
    let tableView = UITableView(frame: .zero,
                                style: .grouped)
    
    let adressCell:AddressTableViewCell = AddressTableViewCell(style: .default,
                                                               reuseIdentifier: "AddressTableViewCell")
    
    let orderCell:OrderInfoTableViewCell = OrderInfoTableViewCell(style: .default,
                                                                  reuseIdentifier: "OrderInfoTableViewCell")
}

extension ScanDeliverVC {
    func scanButtonPressed(sender:Any?) {
        let scanVC = ScanViewController()
        
        scanVC.resultCallBack = {
            codeContent in
            
            self.navigationController?.popViewController(animated: false)
            
            let toast = self.showLoading()
            
            API
                .DeliverOrder
                .bindLogisticsOrder(from: codeContent,
                                    vendorOrderInfoId: self.viewModel.vendorOrderInfoId,
                                    orderformId: self.viewModel.orderFormId,
                                    arrivedTime: self.arriveDate.toString(format: "yyyy-MM-dd hh:mm:ss"))
                .responseNoModel({
                    ez.runThisInMainThread {
                        
                        toast.dismiss(animated: true,
                                      completion: {
                                        if let root = self.navigationController?.viewControllers.first as? WKWebViewController {
                                            let openudid = OpenUDID.value() ?? ""
                                            
                                            let urlString = URLConfig.prefix + "flyapp/fly/orderindex.html?customerId=\(UserManager.shareInstance.currentId.urlEncode())&token=\(UserManager.shareInstance.currentToken.urlEncode())&nickname=\(UserManager.shareInstance.currentNickName.urlEncode())&imei=\(openudid.urlEncode())"
                                            
                                            let webURL = URL(string: urlString)!
                                            
                                            let request = URLRequest(url: webURL)
                                            
                                            root.webView.load(request)
                                            
                                            self.navigationController?.setViewControllers([root, DeliverDetailVC(shippingSn: codeContent)],
                                                                                          animated: true)
                                        }
                        })
                    }
                },
                                 error: { (error) in
                                    ez.runThisInMainThread {
                                        toast.displayLabel(text: error.description)
                                    }
                })
        }
        
        self.navigationController?.pushViewController(scanVC,
                                                      animated: true)
    }
}

extension ScanDeliverVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return adressCell
        case 1:
            return orderCell
        default:
            return ReorderTableViewCell(style: .default,
                                        reuseIdentifier: "ReorderTableViewCell")
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 120
        case 1:
            return 290
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return AddressTableViewCell.height(with: tableView.w,
                                               and: adressCell.item)
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        scanEvent.onNext()
    }
}


extension ScanDeliverVC {
    class AddressTableViewCell: UITableViewCell {
        static func height(with width:CGFloat, and address:AddressItem?) -> CGFloat {
            guard let addressItem            = address else {
                return 104
            }
            
            let nameHeight                   = (addressItem.receiver as NSString).size(attributes: [
                NSFontAttributeName: UIConfig.generalFont(13)
                ]).height
            
            let top                          = 35 + nameHeight
            
            let ps                           = NSMutableParagraphStyle()
            
            ps.lineSpacing                   = 5.0
            
            let fontWidth = width - 65
            
            let size = CGSize(width: fontWidth,
                              height: 10000)
            
            let addressHeight = addressItem.addressAttributedString.boundingRect(with: size,
                                                                                 options: .usesLineFragmentOrigin,
                                                                                 context: nil).height
            
            return addressHeight + 25 + top
        }
        
        static func label() -> UILabel {
            let label                        = UILabel()
            
            label.textColor                  = UIConfig.generalColor.selected
            label.font                       = UIConfig.generalFont(13)
            label.backgroundColor            = UIConfig.generalColor.white
            
            return label
        }
        
        func adjustLayout () {
            lineImageView.bottom = self.view.bounds.height
            lineImageView.size.width = self.view.bounds.width
            
            self.view.bringSubview(toFront: lineImageView)
            
            if item == nil {
                detailLabel.sizeToFit()
                detailLabel.centerInSuperView()
                
                return
            }
            
            nameLabel.origin = CGPoint(x: 50, y: 20)
            nameLabel.sizeToFit()
            
            telLabel.sizeToFit()
            telLabel.right = self.bounds.width - 15
            telLabel.bottom = nameLabel.bottom
            
            let fontWidth = self.bounds.width - 65
            
            detailLabel.w = fontWidth
            detailLabel.fitHeight()
            detailLabel.origin = CGPoint(x: 50, y: telLabel.bottom + 15)
            
            tagImageView.centerYInSuperView()
            tagImageView.right = 35
        }
        
        let nameLabel                        = AddressTableViewCell.label()
        
        let telLabel                         = AddressTableViewCell.label()
        
        let detailLabel                      = AddressTableViewCell.label()
        
        let tagImageView                     = UIImageView(named: "icon_address")
        
        let lineImageView                    = UIImageView(named: "background_rules")
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            self.selectionStyle = .none
            
            tagImageView.backgroundColor     = UIConfig.generalColor.white
            
            detailLabel.numberOfLines = 0
            
            self
                .contentView
                .addSubviews([tagImageView, detailLabel, nameLabel, telLabel])
            self.addSubview(lineImageView)
        }
        
        var item:AddressItem? {
            didSet {
                guard let addressItem        = item else {
                    detailLabel.text         = "不添加收货地址，快递会迷路哒~"
                    
                    telLabel.isHidden        = true
                    nameLabel.isHidden       = true
                    tagImageView.isHidden    = true
                    
                    adjustLayout()
                    
                    return
                }
                
                telLabel.isHidden            = false
                nameLabel.isHidden           = false
                tagImageView.isHidden        = false
                
                telLabel.text = addressItem.phone
                nameLabel.text = addressItem.receiver
                detailLabel.attributedText = addressItem.addressAttributedString
                
                adjustLayout()
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            adjustLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ScanDeliverVC {
    class OrderInfoTableViewCell: UITableViewCell {
        let timeLabel:UILabel
        
        let coverImageView:UIImageView
        
        let titleLabel:UILabel
        
        let scanButton:UIButton
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            coverImageView = UIImageView()
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
            coverImageView.layer.borderWidth = 1
            
            titleLabel = UILabel()
            titleLabel.numberOfLines = 2
            titleLabel.backgroundColor = UIConfig.generalColor.backgroudWhite
            
            timeLabel = ViewFactory.label(font: UIConfig.generalFont(12),
                                             textColor: UIConfig.generalColor.labelGray,
                                             backgroudColor: UIConfig.generalColor.backgroudWhite)
            
            timeLabel.numberOfLines = 2
            scanButton = ViewFactory.button(imageNamed: "icon_barcode")
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            let contentZone = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            contentZone.addSubviews(coverImageView, titleLabel, timeLabel)
            
            coverImageView.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(contentZone).offset(10)
                maker.size.equalTo(CGSize(width:75, height:65))
            }
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(coverImageView)
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.right.equalTo(contentZone).offset(-10)
                maker.bottom.lessThanOrEqualTo(contentZone).offset(-10)
            }
            
            timeLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel.snp.bottom).offset(1)
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.right.equalTo(contentZone).offset(-10)
                maker.bottom.lessThanOrEqualTo(contentZone).offset(-10)
            }
            
            self.contentView.addSubviews(contentZone, scanButton)
            
            contentZone.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.height.greaterThanOrEqualTo(85)
            }
            
            let w = scanButton.w
            
            let h = scanButton.h
            
            let radio = h / w
            
            scanButton.snp.makeConstraints { (maker) in
                maker.top.equalTo(contentZone.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
                maker.centerX.equalTo(self.contentView)
                maker.width.lessThanOrEqualTo(w)
                maker.height.equalTo(scanButton.snp.width).multipliedBy(radio)
                maker.left.greaterThanOrEqualTo(self.contentView).offset(35)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-35)
            }
        }
        
        func set(item:ScanDeliverOrderInfo) {
            timeLabel.text = "预约取件时间：\(item.orderTime)"
            
            
            coverImageView.kf.setImage(with: item.coverImage)
            
            titleLabel.attributedText = NSAttributedString(string: item.productName,
                                                           font: UIConfig.generalFont(12),
                                                           textColor: UIConfig.generalColor.selected,
                                                           lineSpace: 5)
            titleLabel.lineBreakMode = .byTruncatingTail
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ScanDeliverVC {
    class ReorderTableViewCell: UITableViewCell {
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            let leftPaddingLabel = ViewFactory.generalLabel(generalSize: 13,
                                                            textColor: UIConfig.generalColor.labelGray,
                                                            backgroudColor: UIConfig.generalColor.backgroudWhite)
            
            leftPaddingLabel.text = "注："
            
            self.contentView.addSubview(leftPaddingLabel)
            self.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            leftPaddingLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(0)
            }
            
            let tip = UILabel()// ViewFactory.generalLabel(generalSize: 13,
                                 //              textColor: UIConfig.generalColor.labelGray)
            
            tip.numberOfLines = 0
            
            let attributedString = NSMutableAttributedString(string: "快递小哥会在您预约的时间上门取货请准时在您指定的地点等候小哥。\n不小心约错了？  ",
                                                             font: UIConfig.generalFont(13),
                                                             textColor: UIConfig.generalColor.labelGray,
                                                             lineSpace: 3)
            
            attributedString.append(NSAttributedString(string: "重新预约>>",
                                                       font: UIConfig.generalFont(13),
                                                       textColor:  UIColor(hexString: "#3977cb")!))
            
            tip.attributedText = attributedString
            
            self.contentView.addSubview(tip)
            
            tip.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(45)
                maker.top.equalTo(leftPaddingLabel)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.bottom.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
}
