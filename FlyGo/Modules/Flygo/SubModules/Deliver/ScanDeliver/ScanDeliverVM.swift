//
//  ScanDeliverVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/5.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ScanDeliverOrderInfo {
    let productName:String
    
    let coverImage:URL?
    
    var date:Date?
    
    var orderTime:String {
        return date?.toString(format: "yyyy-MM-dd HH:mm:ss") ?? ""
    }
    
    init(model:API.DeliverOrder) {
        productName = model.productName
        coverImage  = model.pic
        date   = model.startDate
    }
}

class ScanDeliverVM: ViewModel {
    
    let orderFormId:String
    
    let vendorOrderInfoId:String
    
    init(vendorOrderInfoId:String, orderFormId:String) {
        self.vendorOrderInfoId = vendorOrderInfoId
        
        self.orderFormId = orderFormId
    }
    
    func bind(to view: ScanDeliverVC) -> DisposeBag? {
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .none)
        
        let bag = DisposeBag()
        
        let orderFormId = self.orderFormId
        
        let vendorOrderInfoId = self.vendorOrderInfoId
        
        let getRequest:(Void) -> Observable<APIItem<API.DeliverOrder>> = {
            (Void) -> Observable<APIItem<API.DeliverOrder>> in
            
            return API.DeliverOrder.getOrderInfo(from: orderFormId)
        }
        
        let request = dataSet
            .refrence(getRequest: getRequest,
                      isEmpty: { _ in return false },
                      in: bag)
        
        request
            .flatMapLatest {
            [weak view]
            (model) -> Observable<(AddressItem, ScanDeliverOrderInfo)> in
            
            let address = AddressItem(model: model)
            
            let orderInfo = ScanDeliverOrderInfo(model: model)
            
            guard let `view` = view else {
                return Observable.just((address, orderInfo))
            }
            
            return view
                .scanEvent
                .flatMapLatest({
                    (Void) -> Observable<(AddressItem, ScanDeliverOrderInfo)> in
                    return OrderDeliverVC
                        .order(showOn: view,
                               vendorOrderInfoId: vendorOrderInfoId,
                               orderformId: orderFormId)
                        .map({ (date, address) -> (AddressItem, ScanDeliverOrderInfo) in
                            var newInfo = orderInfo
                            
                            newInfo.date = date
                            
                            return (address, newInfo)
                        })
                })
                .startWith((address, orderInfo))
            }
            .bind { (collection) in
                view.orderCell.set(item: collection.1)
                view.adressCell.item = collection.0
            }
            .addDisposableTo(bag)
        
        return bag
    }
    
}
