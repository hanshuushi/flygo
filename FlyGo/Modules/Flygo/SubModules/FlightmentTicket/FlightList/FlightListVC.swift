//
//  FlightListVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/31.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import EZSwiftExtensions

class FlightListVC: UITableViewController {
    
    var itemArray:[API.FlymentItemInArray] = []
    
    init(item:API.FlymentItemInArray) {
        super.init(style: .grouped)
        
        itemArray.append(item)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var submitItem:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Navigation
        self.title = "添加归国航班"
        
        submitItem = CommonNavigationVC.item(with: "下一步",
                                             target: self,
                                             action: #selector(FlightListVC.nextStepHandler(sender:)))
        
        self.navigationItem.rightBarButtonItem = submitItem
        
        /// TableView
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 226.5
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 80
        self.tableView.registerCell(cellClass: ItemCardTableViewCell.self)
        self.tableView.registerCell(cellClass: NewItemTableViewCell.self)
        self.tableView.separatorStyle = .none
        self.tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        self.tableView.reloadData()
    }
    
    func nextStepHandler(sender:Any?) {
        
        let toast = self.showLoading()
        
        API.FlightmentTicket.addFlightment(for: self.itemArray).responseModel({
            (ticket:API.FlightmentTicket) in
            ez.runThisInMainThread {
                
                toast.dismiss(animated: true,
                              completion: {
                                self.navigationController?.pushViewController(VerifyTicketVC(vendorFlightInfoId:ticket.vendorFlightInfoId),
                                                                              animated: true)
                })
            }
            
        }, error: { (error) in
            ez.runThisInMainThread {
                toast.displayLabel(text:error.description)
            }
        })
        
    }
    
    func deleteItemHandler(sender:UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: sender) else {
            return
        }
        
        itemArray.remove(at: indexPath.row)
        
        self.tableView.beginUpdates()
        
        self.tableView.deleteRows(at: [indexPath],
                                  with: .automatic)
        
        if itemArray.count == 2 {
            self.tableView.insertRows(at: [IndexPath(row: 2, section: 0)],
                                      with: .automatic)
        } else if itemArray.count == 0 {
            submitItem.isEnabled = false
        }
        
        self.tableView.endUpdates()
    }
}

extension FlightListVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return min(itemArray.count + 1, 3)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row >= itemArray.count {
            return tableView.dequeueReusableCell(at: indexPath) as NewItemTableViewCell
        }
        
        let cell = tableView.dequeueReusableCell(at: indexPath) as ItemCardTableViewCell
        
        cell.set(item: itemArray[indexPath.row])
        cell.parentViewController = self
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "icon_add_flight_information") {
            return headerView
        }
        
        let imageView = UIImageView(named: "icon_add_flight_information")
        
        let headerView = UITableViewHeaderFooterView(reuseIdentifier: "icon_add_flight_information")
        
        headerView.contentView.addSubview(imageView)
        
        imageView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(headerView.contentView)
            maker.top.equalTo(headerView.contentView).offset(20)
            maker.bottom.equalTo(headerView.contentView).offset(-10)
        }
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= itemArray.count {
            return 241.5
        }
        
        return 230
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= itemArray.count {
            return 241.5
        }
        
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= itemArray.count {
            AddFlightVC.getFlightmentItem(showOn: self.navigationController ?? self,
                                          callBack: { (item) in
                                            self.itemArray.append(item)
                                            
                                            self.submitItem.isEnabled = true
                                            
                                            if self.itemArray.count >= 3 {
                                                let indexPath = IndexPath(row: 2,
                                                                          section: 0)
                                                
                                                self.tableView.reloadRows(at: [indexPath],
                                                                          with: .automatic)
                                            } else {
                                                let indexPath = IndexPath(row: self.itemArray.count - 1,
                                                                          section: 0)
                                                
                                                self.tableView.insertRows(at: [indexPath],
                                                                          with: .automatic)
                                            }
            })
        }
    }
}


extension FlightListVC {
    class NewItemTableViewCell: UITableViewCell {
        
        fileprivate class BackgroudView: UIView {
            
            let backgroudLayer:CAShapeLayer
            
            override init(frame: CGRect) {
                
                var bounds = frame
                
                bounds.origin = .zero
                
                let path = UIBezierPath(roundedRect: bounds,
                                        cornerRadius: 5)
                
                backgroudLayer = CAShapeLayer()
                
                backgroudLayer.shadowPath = path.cgPath
                backgroudLayer.fillColor = UIConfig.generalColor.white.cgColor
                backgroudLayer.strokeColor = UIColor(hexString: "#eeeeee")?.cgColor
                backgroudLayer.lineCap = kCALineCapRound
                backgroudLayer.lineJoin = kCALineJoinRound
                backgroudLayer.lineWidth = 2
                backgroudLayer.lineDashPattern = [15, 12]
                backgroudLayer.lineDashPhase = -3
                backgroudLayer.frame = bounds
                
                super.init(frame: frame)
                
                self.layer.addSublayer(backgroudLayer)
            }
            
            override func layoutSubviews() {
                super.layoutSubviews()
                
                backgroudLayer.path = UIBezierPath(roundedRect: self.bounds,
                                                   cornerRadius: 5).cgPath
                
                backgroudLayer.frame = self.bounds
                
                NSLog("frame %@", backgroudLayer)
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            let backgroudCard = BackgroudView()
            
            self.contentView.addSubview(backgroudCard)
            
            backgroudCard.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(15)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.bottom.equalTo(self.contentView)
            }
            
            let label = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                          textColor: UIConfig.generalColor.whiteGray)
            
            backgroudCard.addSubview(label)
            
            label.text = "点击添加转机航班"
            label.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(backgroudCard).offset(15)
                maker.centerY.equalTo(backgroudCard)
            }
            
            let icon = UIImageView(named: "icon_add_flight")
            
            backgroudCard.addSubview(icon)
            
            icon.snp.makeConstraints { (maker) in
                maker.right.equalTo(label.snp.left).offset(-10)
                maker.centerY.equalTo(label)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FlightListVC {
    class ItemCardTableViewCell: UITableViewCell {
        
        let titleLabel:UILabel
        
        let contentLabel:UILabel
        
        weak var parentViewController:FlightListVC?
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            //// card view
            let cardView = ViewFactory.view(color: .clear)
            
            let cardContentView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            cardView.addSubview(cardContentView)
            
            cardContentView.snp.fillToSuperView()
            cardContentView.layer.cornerRadius = 5
            cardContentView.layer.masksToBounds = true
            
            cardView.layer.shadowColor = UIColor.black.cgColor
            cardView.layer.shadowOpacity = 0.1
            cardView.layer.shadowOffset = .zero
            cardView.layer.shadowRadius = 5
            
            /// content
            titleLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                           textColor: UIConfig.generalColor.selected)
            
            contentLabel = UILabel()
            contentLabel.numberOfLines = 0
            contentLabel.adjustsFontSizeToFitWidth = true
            contentLabel.lineBreakMode = .byTruncatingMiddle
            
            let line = ViewFactory.line()
            
            let deleteButton = ViewFactory.button(imageNamed: "icon_delete_flight")
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(cardView)
            
            cardView.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(25, 15, 5, 15))
            }
            
            cardContentView.addSubviews(titleLabel, deleteButton, line, contentLabel)
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(cardContentView).offset(20)
                maker.left.equalTo(cardContentView).offset(15)
                maker.height.equalTo(21)
            }
            
            deleteButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(titleLabel)
                maker.right.equalTo(cardContentView).offset(-15)
            }
            deleteButton.addTarget(self,
                                   action: #selector(ItemCardTableViewCell.deletePressed(sender:)),
                                   for: UIControlEvents.touchUpInside)
            
            line.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel.snp.bottom).offset(20)
                maker.left.equalTo(cardContentView).offset(15)
                maker.right.equalTo(cardContentView).offset(-15)
                maker.height.equalTo(0.5)
            }
            
            contentLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(cardContentView).offset(15)
                maker.right.equalTo(cardContentView).offset(-15)
                maker.bottom.equalTo(cardContentView).offset(-20)
                maker.top.equalTo(line.snp.bottom).offset(20)
            }
        }
        
        func set(item:API.FlymentItemInArray) {
            titleLabel.text = "\(item.startCountryName) - \(item.endCountryName)"
            
            var content = "航班号：\(item.flightNumber)"

            content += "\n"
            content += "起飞机场：\(item.startAirport)"
            content += "\n"
            content += "起飞时间：\(item.orgTimezone),\(item.flightDeptimePlanDate)"
            content += "\n"
            content += "到达机场：\(item.endAirport)"
            content += "\n"
            content += "到达时间：\(item.dstTimezone),\(item.flightArrtimePlanDate)"
            
            contentLabel.attributedText = NSAttributedString(string: content,
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.selected,
                                                             lineSpace: 5)
            
            contentLabel.numberOfLines = 0
            contentLabel.adjustsFontSizeToFitWidth = true
            contentLabel.lineBreakMode = .byTruncatingMiddle
        }
        
        func deletePressed(sender:UIButton) {
            let alertVC = UIAlertController(title: "提醒",
                                            message: "您是否要删除该行程",
                                            preferredStyle: .actionSheet)
            
            alertVC.addAction(UIAlertAction(title: "删除",
                                            style: .destructive,
                                            handler: { (_) in
                                                self.parentViewController?.deleteItemHandler(sender: self)
            }))
            
            alertVC.addAction(UIAlertAction(title: "不要",
                                            style: .cancel,
                                            handler: nil))
            
            if let popover = alertVC.popoverPresentationController {
                popover.sourceView = sender
                popover.sourceRect = sender.bounds
            }
            
            self.parentViewController?.present(alertVC,
                                               animated: true,
                                               completion: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
