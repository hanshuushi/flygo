//
//  TicketReviewVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/4.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let SwitchTripManagerNotification = Notification.Name("SwitchTripManagerNotification")
}

class TicketReviewVC: UIViewController, UIGestureRecognizerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// title
        self.title = "航班审核"
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        /// scroll view
        let scrollView = UIScrollView(frame: .zero)
        
        self.view.addSubview(scrollView)
        
        scrollView.backgroundColor = UIConfig.generalColor.white
        scrollView.snp.fillToSuperView()
        
        //// header
        let imageView = UIImageView(named: "icon_upload_succeed")
        
        scrollView.addSubview(imageView)
        scrollView.alwaysBounceVertical = true
        
        imageView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(scrollView)
            maker.top.equalTo(scrollView).offset(20)
        }
        
        /// label
        let label = ViewFactory.generalLabel(generalSize: 13,
                                             textColor: UIConfig.generalColor.labelGray)
        
        label.text = "您的航班已上传！请耐心等待审核结果"
        
        scrollView.addSubview(label)
        
        label.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(scrollView)
            maker.top.equalTo(imageView.snp.bottom).offset(100)
        }
        
        /// button
        let finishButton = ViewFactory.redButton(title: "完成")
        
        scrollView.addSubview(finishButton)
        
        finishButton.layer.cornerRadius = 22
        finishButton.layer.masksToBounds = true
        finishButton.addTarget(self,
                               action: #selector(TicketReviewVC.finishPressed(sender:)),
                               for: .touchUpInside)
        finishButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(30)
            maker.right.equalTo(self.view).offset(-30)
            maker.top.equalTo(label.snp.bottom).offset(100)
            maker.height.equalTo(44)
        }
        
        /// journey managerlet 
        let journeyButton = UIButton(type: .custom)
        journeyButton.addTarget(self,
                            action: #selector(TicketReviewVC.tripManagerPressed(sender:)),
                            for: UIControlEvents.touchUpInside)
        journeyButton.setAttributedTitle(NSAttributedString(string: "查看行程管理",
                                                        font: UIConfig.generalFont(13),
                                                        textColor: UIColor(hexString: "#3977cb")!),
                                     for: .normal)
        journeyButton.contentEdgeInsets = UIEdgeInsetsMake(20, 15, 20, 15)
        
        scrollView.addSubview(journeyButton)
        
        journeyButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(finishButton.snp.bottom)
            maker.centerX.equalTo(scrollView)
            maker.bottom.equalTo(scrollView).offset(-30)
        }
    }
    
    func finishPressed(sender:Any?) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func tripManagerPressed(sender:Any?) {
        NotificationCenter
            .default
            .post(name: .SwitchTripManagerNotification,
                  object: nil)
        
        self.navigationController?.popToRootViewController(animated: true)
    }
}
