//
//  VerifyTicketVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/31.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import EZSwiftExtensions
import ImageViewer

extension Notification.Name {
    static let TicketAddSuccessNotification = Notification.Name("TicketReviewVCAddSuccessNotification")
}

class VerifyTicketVC: UIViewController {
    
    let vendorFlightInfoId:String
    
    fileprivate let stackView:PicStackView
    
    var selectedImages:[UIImage] = [] {
        didSet {
            var stackChildViews = selectedImages.map { (image) -> UIView in
                let imageView = UIImageView(image: image)
                
                imageView.contentMode = .scaleAspectFill
                imageView.backgroundColor = UIConfig.generalColor.white
                imageView.layer.cornerRadius = 5
                imageView.layer.masksToBounds = true
                imageView.clipsToBounds = true
                imageView.isUserInteractionEnabled = true
                imageView.addTapGesture(target: self,
                                        action: #selector(VerifyTicketVC.imagePreviewGesture(sender:)))
                
                let button = ViewFactory.button(imageNamed: "icon_delete_images")
                
                imageView.addSubview(button)
                
                button.snp.makeConstraints({ (maker) in
                    maker.width.height.equalTo(35)
                    maker.right.top.equalTo(imageView)
                })
                button.addTarget(self,
                                 action: #selector(VerifyTicketVC.deleteImagePressed(sender:)),
                                 for: UIControlEvents.touchUpInside)
                
                return imageView
            }
            
            if stackChildViews.count < 9 {
                let button = UIButton(type: .custom)
                
                button.setImage(VerifyTicketVC.addImage(text: "\(stackChildViews.count)/9"),
                                for: .normal)
                button.addTarget(self,
                                 action: #selector(VerifyTicketVC.addImagePressed(sender:)),
                                 for: UIControlEvents.touchUpInside)
                
                stackChildViews.append(button)
            }
            
            stackChildViews.forEachEnumerated { (tag, subView) in
                subView.tag = tag
            }
            
            stackView.childViews = stackChildViews
            
            self.submitItem.isEnabled = self.selectedImages.count > 0
        }
    }
    
    func imagePreviewGesture(sender:UITapGestureRecognizer) {
        guard let index = sender.view?.tag else {
            return
        }
        
        let galleryVC = GalleryViewController(startIndex: index,
                                              itemsDataSource: self,
                                              displacedViewsDataSource: self,
                                              configuration: ProductDetailVC.galleryConfiguration())
        
        galleryVC.headerView = nil
        
        self.present(galleryVC, animated: false, completion: nil)
    }
    
    static func addImage(text:String) -> UIImage? {
        let length = (UIScreen.main.bounds.width - 60) / 3.0
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: length, height: length),
                                               false,
                                               UIScreen.main.scale)
        
        guard let ct:CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            
            return nil
        }
        
        let path = CGPath(roundedRect: CGRect(x: 2, y: 2, width: length - 4, height: length - 4),
                          cornerWidth: 5,
                          cornerHeight: 5,
                          transform: nil)
        
        ct.addPath(path)
        ct.setLineCap(.round)
        ct.setLineJoin(.round)
        ct.setLineWidth(2)
        ct.setStrokeColor(UIColor(hexString: "#eeeeee")!.cgColor)
        ct.setLineDash(phase: 0, lengths: [8,7])
        ct.strokePath()
        
        let image = UIImage(named: "icon_add_photo-1")!
        
        let imageFrame = CGRect(x: (length - image.size.width) / 2.0,
                                y: length / 2.0 - image.size.height - 5,
                                width: image.size.width,
                                height: image.size.height)
        
        image.draw(in: imageFrame)
        
        let attributed = NSAttributedString(string: text,
                                            font: UIConfig.generalFont(13),
                                            textColor: UIConfig.generalColor.whiteGray)
        
        let labelSize = attributed
            .boundingRect(with: CGSize.init(width: length,
                                            height: CGFloat.greatestFiniteMagnitude),
                          options: .usesLineFragmentOrigin,
                          context: nil)
            .size
        
        attributed.draw(at: CGPoint(x: length / 2.0 - labelSize.width / 2.0,
                                    y: imageFrame.maxY + 10))
        
        guard let outputImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            
            return nil
        }
        
        UIGraphicsEndImageContext()
        
        return outputImage
    }
    
    init(vendorFlightInfoId:String) {
        self.stackView = PicStackView()
        
        self.vendorFlightInfoId = vendorFlightInfoId
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var submitItem:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// title
        self.title = "上传机票凭证"
        
        submitItem = CommonNavigationVC.item(with: "下一步",
                                             target: self,
                                             action: #selector(VerifyTicketVC.uploadImagePressed(sender:)))
        
        self.navigationItem.rightBarButtonItem = submitItem
        
        submitItem.isEnabled = false
        
        /// scroll view
        let scrollView = UIScrollView(frame: .zero)
        
        self.view.addSubview(scrollView)
        
        scrollView.backgroundColor = UIConfig.generalColor.white
        scrollView.snp.fillToSuperView()
        
        //// header
        let imageView = UIImageView(named: "icon_add_flight_information")
        
        scrollView.addSubview(imageView)
        scrollView.alwaysBounceVertical = true
        
        imageView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(scrollView)
            maker.top.equalTo(scrollView).offset(20)
        }
        
        //// stack
        scrollView.addSubview(stackView)
        
        stackView.childSideLength = (UIScreen.main.bounds.width - 60) / 3.0
        stackView.snp.makeConstraints { (maker) in
            maker.left.equalTo(scrollView).offset(15)
            maker.right.equalTo(scrollView).offset(-15)
            maker.top.equalTo(imageView.snp.bottom).offset(30)
        }
        
        selectedImages = []
        
        /// tip
        let leftPaddingLabel = ViewFactory.generalLabel(generalSize: 13, textColor: UIConfig.generalColor.labelGray)
        
        leftPaddingLabel.text = "注："
        
        scrollView.addSubview(leftPaddingLabel)
        
        leftPaddingLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(15)
            maker.top.equalTo(stackView.snp.bottom).offset(15)
        }
        
        let tip = ViewFactory.generalLabel(generalSize: 13,
                                           textColor: UIConfig.generalColor.labelGray)
        
        tip.numberOfLines = 0
        tip.text = "图片中必须包含真实的姓名、航班号和行程日期。\n上传图片的数量为：至少1张、至多9张。\n上传的机票凭证包括：航空公司或各大售票平台提供的购买回执、短信或邮件。"
    
        scrollView.addSubview(tip)
        
        tip.snp.makeConstraints { (maker) in
            maker.left.equalTo(leftPaddingLabel.snp.right)
            maker.top.equalTo(leftPaddingLabel)
            maker.right.equalTo(self.view).offset(-15)
            maker.bottom.equalTo(scrollView).offset(-30)
        }
        
        /// example
        let tipButton = UIButton(type: .custom)
        tipButton.addTarget(self,
                            action: #selector(VerifyTicketVC.showTip),
                            for: UIControlEvents.touchUpInside)
        tipButton.setAttributedTitle(NSAttributedString(string: "查看范例>>",
                                                        font: UIConfig.generalFont(13),
                                                        textColor: UIColor(hexString: "#3977cb")!),
                                     for: .normal)
        tipButton.contentEdgeInsets = UIEdgeInsetsMake(10, 15, 10, 15)
        
        scrollView.addSubview(tipButton)
        
        tipButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(tip.snp.bottom)
            maker.right.equalTo(self.view)
//            maker.size.equalTo(CGSize(width: 98, height: 50))
        }
    }
    
    func showTip() {
        let vc = UploadTipVC()
        
        PanelPresentController(presentedViewController: vc,
                               presenting: self.navigationController ?? self,
                               panelLayout: .size(UploadTipVC.windowSize),
                               panelCornerRadius: 0)
            .present(completion: { (pc) in
                let frame = pc.frameOfPresentedViewInContainerView
                
                let button = ViewFactory.button(imageNamed: "icon_close-1")
                
                button.right = frame.maxX - 15
                button.top = frame.maxY
                button.alpha = 0
                
                UIView.animate(withDuration: 0.25,
                               animations: {
                                button.alpha = 1.0
                })
                
                button.addTarget(pc,
                                 action: #selector(PanelPresentController.dismiss),
                                 for: .touchUpInside)
                
                pc.containerView?.addSubview(button)
            })

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UploadTipVC.shouldShowTip() {
            ez.runThisAfterDelay(seconds: 0.3,
                                 after: {
                                    self.showTip()
            })
        }
    }
    
    var uploadedPaths:[String] = []
}

extension UIImageView: DisplaceableView {
    
}

extension VerifyTicketVC: GalleryDisplacedViewsDataSource {
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        return (stackView.childViews[index] as? UIImageView).flatMap({ $0 })  //self.picsCell.listView.buttons[index]
    }
}

extension VerifyTicketVC: GalleryItemsDataSource {
    func itemCount() -> Int {
        return selectedImages.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return .image(fetchImageBlock: { (fetchImageBlock) in
            fetchImageBlock(self.selectedImages[index])
        })
    }
}

extension VerifyTicketVC: TZImagePickerControllerDelegate {
    func deleteImagePressed(sender:UIButton) {
        let index = sender.tag
        
        let alert = ViewFactory.deleteActionSheet(from: sender,
                                      content: "是否要删除这张照片") {
                                        self.selectedImages.remove(at: index)
        }
        
        self.navigationController?.present(alert,
                                           animated: true,
                                           completion: nil)
    }
    
    func addImagePressed(sender:Any?) {
        guard let imagePickerController = TZImagePickerController(maxImagesCount: 9 - selectedImages.count,
                                                                  delegate: self) else {
                                                                    return
        }
        
        imagePickerController.allowCrop = false
        imagePickerController.allowPickingVideo = false
        imagePickerController.allowPickingGif = false
        imagePickerController.allowPickingOriginalPhoto = false
        
        self.navigationController?.presentVC(imagePickerController)
    }
    
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        self.selectedImages += photos ?? []
    }
}

extension VerifyTicketVC {
    func uploadImagePressed(sender:Any?) {
        
        uploadedPaths = []
        
        uploadImage(at: 0, toast: nil)
    }
    
    func uploadImage(at index:Int, toast:ToastViewController?) {
        /// upload ticket
        if index >= selectedImages.count {
            
            toast?.displayLoadingAndLabel(text: "更新航班信息中")
            
            /// upload pic url into ticket
            API.FlightmentTicket.updateTicketPics(for: self.vendorFlightInfoId,
                                                  pics: self.uploadedPaths).responseNoModel({
                                                    
                                                    ez.runThisInMainThread {
                                                        NotificationCenter
                                                            .default
                                                            .post(name: .TicketAddSuccessNotification,
                                                                  object: nil)
                                                        
                                                        toast?.dismiss(animated: true,
                                                                       completion: {
                                                                        let reviewVC = TicketReviewVC()
                                                                        
                                                                        self
                                                                            .navigationController?
                                                                            .pushViewController(reviewVC,
                                                                                                animated: true)
                                                        })
                                                    }
                                                    
                                                  }, error: { (error) in
                                                    ez.runThisInMainThread {
                                                        toast?.displayLabel(text:error.description)
                                                    }
                                                  })
            
            return
        }
        
        /// upload image
        let image = selectedImages[index]
        
        guard let imageData = UIImageJPEGRepresentation(image, 0.7) else {
            
            toast?.displayLabel(text: "")
            
            return
        }
        
        let toastLabel = "正在上传第\(index + 1)张图片，请稍等"
        
        toast?.displayLoadingAndLabel(text: toastLabel)
        
        let `toast` = toast ?? self.showLoadingAndToast(toastLabel)
        
        ImagePicker
            .httpManager
            .post(URLConfig.imageUploader + "files/uploadProcesser",
                  parameters: ["type":"x99",
                               "module":"member"],
                  constructingBodyWith: { (formData) in
                    formData.appendPart(withFileData: imageData,
                                        name: "file",
                                        fileName: NSUUID().uuidString + ".jpg",
                                        mimeType: "image/jpeg")
            },
                  progress: nil,
                  success: {
                    (task, result) in
                    
                    do {
                        let model:API.UploaderImage = try HttpSession.model(from: result ?? [String:Any]())
                        
                        self.uploadedPaths.append(model.urlString)
                        
                        ez.runThisInMainThread {
                            self.uploadImage(at: index + 1, toast: toast)
                        }
                        
                    } catch HttpError.businessError(_, let description) {
                        ez.runThisInMainThread {
                            toast.displayLabel(text: "第\(index + 1)张图片上传失败，原因为\(description)")
                        }
                    } catch {
                        ez.runThisInMainThread {
                            toast.displayLabel(text: "第\(index + 1)张图片上传失败")
                        }
                    }
                    
            }) { (task, error) in
                ez.runThisInMainThread {
                    toast.displayLabel(text: "第\(index + 1)张图片上传失败，原因为\(error.localizedDescription)")
                }
        }
    }
}

extension VerifyTicketVC {
    class PicStackView: UIView {
        var childViews:[UIView] = [] {
            didSet {
                self.removeSubviews()
                self.addSubviews(childViews)
                self.adjustChildViewLayout()
                self.invalidateIntrinsicContentSize()
            }
        }
        
        var childSideLength:CGFloat = 100 {
            didSet {
                self.adjustChildViewLayout()
                self.invalidateIntrinsicContentSize()
            }
        }
        
        override var intrinsicContentSize: CGSize {
            get {
                if childViews.count <= 0 {
                    return .zero
                }
                
                let rowCount = (childViews.count - 1) / 3 + 1
                
                let colCount = min(childViews.count, 3)
                
                let width = CGFloat(colCount) * childSideLength + 15 * CGFloat(colCount - 1)
                
                let height = CGFloat(rowCount) * childSideLength + 15 * CGFloat(rowCount - 1)
                
                return CGSize(width: width, height: height)
            }
        }
        
        func adjustChildViewLayout() {
            let childSideLength = self.childSideLength
            
            childViews.forEachEnumerated { (index, childView) in
                let row = index / 3
                
                let col = index % 3
                
                childView.frame = CGRect(x: CGFloat(col) * childSideLength + 15 * CGFloat(col),
                                         y: CGFloat(row) * childSideLength + 15 * CGFloat(row),
                                         width: childSideLength,
                                         height: childSideLength)
            }
        }
    }
}
