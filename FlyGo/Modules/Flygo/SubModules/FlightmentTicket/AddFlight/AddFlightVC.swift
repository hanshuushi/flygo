//
//  AddFlightVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/30.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import EZSwiftExtensions

fileprivate extension UITableViewCell {
    func addLine() {
        let line = ViewFactory.line()
        
        self.contentView.addSubview(line)
        
        line.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.bottom.equalTo(self.contentView)
            maker.left.equalTo(self.contentView).offset(30)
            maker.right.equalTo(self.contentView).offset(-30)
        }
    }
}

class AddFlightVC: UITableViewController {
    
    static func getFlightmentItem(showOn viewController:UIViewController,
                                  callBack:((API.FlymentItemInArray) -> Void)? = nil) {
        let vc = AddFlightVC(style: .grouped)
        
        vc.callBack = callBack
        
        let navigationVC = PresentNavigationVC(rootViewController: vc)
        
        viewController.presentVC(navigationVC)
    }
    
    let flyghtNoCell = TextFieldTableViewCell(title: "航班号",
                                              placeHolder: "请输入航班号")
    
    let flyghtDateCell = LabelTableViewCell(title: "起飞时间(当地)")
    
    let listHeader = ListheaderView()
    
    let searchHeader = SearchHeaderView()
    
    let searchFooter = SearchFooterView()
    
    var callBack:((API.FlymentItemInArray) -> Void)?
    
    var flymentItems:[API.FlymentItemInArray] = [] {
        didSet {
            self.tableView.reloadSections([1],
                                          animationStyle: .automatic)
        }
    }
    
    var flyghtNo:String = "" {
        didSet {
            checkButtonEnable()
        }
    }
    
    var selectedDate:Date? {
        didSet {
            flyghtDateCell.contentLabel.text = selectedDate?.toString(format: "YYYY-MM-dd") ?? ""
            
            let reload = self.flymentItems.count > 0
            
            if reload {
                self.flymentItems = []
            }
            
            checkButtonEnable()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// title
        self.title = "添加归国航班"
        
        /// textfield
        flyghtNoCell.textField.returnKeyType = .done
        flyghtNoCell.textField.keyboardType = .asciiCapable
        flyghtNoCell.textField.enablesReturnKeyAutomatically = true
        flyghtNoCell.textField.textAlignment = .right
        flyghtNoCell.textField.clearButtonMode = .whileEditing
        flyghtNoCell.textField.addTarget(self,
                                         action: #selector(AddFlightVC.changeFlyghtNo(sender:)),
                                         for: UIControlEvents.editingChanged)
        
        /// table view
        self.tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 55
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.registerCell(cellClass: LabelTableViewCell.self)
        
        /// button
        checkButtonEnable()
        
        searchHeader
            .button
            .addTarget(self,
                       action: #selector(AddFlightVC.searchFlyghtmentArray),
                       for: UIControlEvents.touchUpInside)
    }
}

extension AddFlightVC {
    
    func changeFlyghtNo(sender:UITextField) {
        let reload = self.flymentItems.count > 0
        
        if reload {
            self.flymentItems = []
        }
        
        flyghtNo = sender.text ?? ""
    }
    
    func checkButtonEnable() {
        searchHeader.button.isEnabled = flyghtNo.length > 0 && selectedDate != nil
    }
    
    func searchFlyghtmentArray() {
        self.tableView.endEditing(true)
        
        let predicateString = "^[a-zA-Z0-9][a-zA-Z0-9][0-9]{2,4}$"
        
        if !NSPredicate(format: "SELF MATCHES %@", predicateString).evaluate(with: flyghtNo) {
            self.showToast(text: "请输入正确的航班号")
            
            return
        }
        
        guard let date = selectedDate else {
            self.showToast(text: "未输入航班时间")
            
            return
        }
        
        let toast = self.navigationController!.showLoading()
        
        let toManualInput = {
            let actionSheet = UIAlertController(title: "未查找到航班",
                                                message: "是否需要手动输入航班",
                                                preferredStyle: .actionSheet)
            
            let confirmAction = UIAlertAction(title: "手动输入",
                                              style: .default,
                                              handler: { (_) in
                                                let vc = ManualInputVC()
                                                
                                                vc.callBack = {
                                                    item in
                                                    
                                                    let callBack = self.callBack
                                                    
                                                    self.dismiss(animated: true,
                                                                 completion: {
                                                                    callBack?(item)
                                                    })
                                                }
                                                
                                                self.navigationController!.pushViewController(vc,
                                                                                              animated: true)
            })
            
            actionSheet.addAction(confirmAction)
            
            let reinputAction = UIAlertAction(title: "重新输入",
                                              style: .cancel,
                                              handler: { (_) in
                                                self.flyghtNoCell.textField.becomeFirstResponder()
            })
            
            actionSheet.addAction(reinputAction)
            
            self
                .navigationController!
                .present(actionSheet,
                         animated: true,
                         completion: nil)
        }
        
        API.FlymentItemInArray.getFlyghtArray(flightNo: flyghtNo,
                                              startTime: date).responseModelList({
                                                (items:[API.FlymentItemInArray]) in
                                                
                                                ez.runThisInMainThread {
                                                    if items.count <= 0 {
                                                        toast.dismiss(animated: true,
                                                                      completion: toManualInput)
                                                        
                                                        return
                                                    }
                                                    
                                                    toast.dismiss(animated: true,
                                                                  completion: {
                                                                    self.flymentItems = items
                                                    })
                                                }
                                                
                                              }) { (error) in
                                                switch error {
                                                case .businessError(let code, _):
                                                    if code == 1503 {
                                                        
                                                        toManualInput()
                                                        
                                                        return
                                                    }
                                                default:
                                                    break
                                                }
                                                
                                                ez.runThisInMainThread {
                                                    toast.displayLabel(text: error.description)
                                                }
        }
    }
}

extension AddFlightVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return flymentItems.count
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return flyghtNoCell
            case 1:
                return flyghtDateCell
            default:
                break
            }
        case 1:
            let cell = tableView.dequeueReusableCell(at: indexPath) as LabelTableViewCell
            
            let item = flymentItems[indexPath.row]
            
            cell.titleLabel.text = "\(item.startAirport) ~ \(item.endAirport)"
            
            return cell
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 && flymentItems.count <= 0 {
            return searchHeader
        } else if section == 1 {
            return listHeader
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 && flymentItems.count <= 0 {
            return 79
        } else if section == 1 {
            return 30
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 && flymentItems.count <= 0 {
            return 79
        } else if section == 1 {
            return UITableViewAutomaticDimension
        }
        
        return .leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            return ViewFactory.view(color: UIConfig.generalColor.white)
        } else if section == 1 && flymentItems.count <= 0 {
            return searchFooter
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        if section == 0 && flymentItems.count <= 0 {
            return 35
        } else if section == 0 {
            return 30
        } else if section == 1 && flymentItems.count <= 0 {
            return 200
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 && flymentItems.count <= 0 {
            return UITableViewAutomaticDimension
        } else if section == 0 {
            return 30
        } else if section == 1 && flymentItems.count <= 0 {
            return UITableViewAutomaticDimension
        }
        
        return .leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 1 {
            let vc = UIDatePickerController(mode: .date,
                                            callBack: {
                                                [weak self] (selectedDate) in
                                                
                                                guard let `self` = self else {
                                                    return
                                                }
                                                
                                                self.selectedDate = selectedDate
            })
            
            SliderPopViewController(vc,
                                    mode: .bottom,
                                    fillMode: .size,
                                    value: 250).present(on: self)
        } else if indexPath.section == 1 {
            let item = flymentItems[indexPath.row]
            
            let callBack = self.callBack
            
            self.dismiss(animated: true,
                         completion: {
                            callBack?(item)
            })
        }
    }
}

extension AddFlightVC {
    class ListheaderView: UITableViewHeaderFooterView {
        init() {
            super.init(reuseIdentifier: "ListheaderView")
            
            
            self.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            let label = ViewFactory.generalLabel(generalSize: 13,
                                                 textColor: UIConfig.generalColor.unselected)
            
            label.text = "请选择航班"
            
            self.contentView.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(30)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
            
            let line = ViewFactory.line()
            
            self.contentView.addSubview(line)
            
            line.snp.makeConstraints { (maker) in
                maker.height.equalTo(0.5)
                maker.left.right.equalTo(label)
                maker.bottom.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class SearchHeaderView: UITableViewHeaderFooterView {
        
        let button:UIButton
        
        init() {
            button = ViewFactory.redButton(title: "查找航班")
            button.layer.cornerRadius = 22
            button.layer.masksToBounds = true
            
            super.init(reuseIdentifier: "SearchHeader")
            
            
            self.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            self.contentView.addSubview(button)
            
            button.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0, 30, 35, 30))
                maker.height.equalTo(44)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class SearchFooterView: UITableViewHeaderFooterView {
        init() {
            super.init(reuseIdentifier: "SearchFooter")
            
            
            self.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            let label = UILabel()
            
            let attributedString = NSMutableAttributedString(string: "添加航班前，请仔细阅读以下说明。\n",
                                                             font: UIConfig.generalFont(13),
                                                             textColor: UIConfig.generalColor.unselected,
                                                             lineSpace: 20)
            
            attributedString.append(NSAttributedString(string: "1.航班最多可添加五十条，必须为一年内归国航班，飞购会根据您添加的航班信息推送订单。\n2.航班必须真实可靠，乘机人必须为认证飞哥本人。\n3.飞购按照您上传的航班信息推送订单，请按照每个订单中提供的渠道购买商品，如价格超出飞购指定的商品价格，产生的额外费用自付。\n4.请保留完整的免税店商品外包装和小票，若损坏或丢失，责任自负。",
                                                       font: UIConfig.generalFont(12),
                                                       textColor: UIConfig.generalColor.labelGray,
                                                       lineSpace: 2))
            
            label.numberOfLines = 0
            label.attributedText = attributedString
            
            self.contentView.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0, 30, 0, 30))
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension AddFlightVC {
    class TextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {
    
        let textField:UITextField
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if string == "\n" {
                
                textField.resignFirstResponder()
                
                return false
            }
            
            return true
        }
        
        init(title:String, placeHolder:String) {
            let titleLabel = ViewFactory.generalLabel()
            
            titleLabel.text = title
            
            textField = UITextField()
            
            textField.font = titleLabel.font
            textField.textColor = titleLabel.textColor
            textField.attributedPlaceholder = NSAttributedString(string: placeHolder,
                                                                 font: titleLabel.font!,
                                                                 textColor: UIConfig.generalColor.unselected)
            
            super.init(style: .default,
                       reuseIdentifier: "TextField")
            
            
            textField.delegate = self
            
            self.selectionStyle = .none
            
            self.contentView.addSubviews(titleLabel, textField)
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(30)
                maker.top.equalTo(self.contentView).offset(20)
                maker.bottom.equalTo(self.contentView).offset(-20)
            }
            
            textField.snp.makeConstraints { (maker) in
                maker.top.bottom.equalTo(self.contentView)
                maker.left.equalTo(titleLabel.snp.right).offset(8)
                maker.right.equalTo(self.contentView).offset(-30)
            }
            
            addLine()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension AddFlightVC {
    class LabelTableViewCell: UITableViewCell {
        
        let contentLabel:UILabel
        
        let titleLabel:UILabel
        
        convenience init(title:String) {
            self.init(style: .default,
                      reuseIdentifier: "LabelTableViewCell_\(title)")
            
            
            titleLabel.text = title
        }
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            titleLabel = ViewFactory.generalLabel()
            
            contentLabel = ViewFactory.generalLabel()
            
            let accessImageView = UIImageView(named: "icon_more-brand")
            
            super.init(style: .default,
                       reuseIdentifier: "TextField")
            
            self.selectionStyle = .none
            
            self.contentView.addSubviews(titleLabel, contentLabel, accessImageView)
            
            titleLabel.lineBreakMode = .byTruncatingMiddle
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(30)
                maker.top.equalTo(self.contentView).offset(20)
                maker.bottom.equalTo(self.contentView).offset(-20)
                maker.right.lessThanOrEqualTo(accessImageView.snp.left).offset(-10)
            }
            
            contentLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(accessImageView.snp.left).offset(-10)
                maker.centerY.equalTo(titleLabel)
//                maker.left.greaterThanOrEqualTo(titleLabel.snp.right).offset(8)
            }
            
            accessImageView.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(self.contentView).offset(-30)
            }
            
            addLine()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
