//
//  ManualInputVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/6.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class ManualInputVC: UITableViewController {
    
    typealias TextFieldTableViewCell = AddFlightVC.TextFieldTableViewCell
    
    typealias LabelTableViewCell = AddFlightVC.LabelTableViewCell
    
    typealias SearchHeaderView = AddFlightVC.SearchHeaderView
    
    var callBack:((API.FlymentItemInArray) -> Void)?
    
    var flyghtNo:String = "" {
        didSet {
            checkButtonEnable()
        }
    }
    
    let flyghtNoCell = TextFieldTableViewCell(title: "归国航班号",
                                              placeHolder: "请输入归国航班号")
    
    var startFlyghtDate:Date? {
        didSet {
            startFlyghtDateCell.contentLabel.text = startFlyghtDate?.toString(format: "YYYY-MM-dd") ?? ""
            
            checkButtonEnable()
        }
    }
    
    let startFlyghtDateCell = LabelTableViewCell(title: "起飞时间(当地)")
    
    var startAirportItem:AirportItem? {
        didSet {
            startAirportCell.contentLabel.text = startAirportItem?.name ?? ""
            
            checkButtonEnable()
        }
    }
    
    let startAirportCell = LabelTableViewCell(title: "起飞机场")
    
    var endFlyghtDate:Date? {
        didSet {
            endFlyghtDateCell.contentLabel.text = endFlyghtDate?.toString(format: "YYYY-MM-dd") ?? ""
            
            checkButtonEnable()
        }
    }
    
    let endFlyghtDateCell = LabelTableViewCell(title: "降落时间(当地)")
    
    var endAirportItem:AirportItem? {
        didSet {
            endAirportCell.contentLabel.text = endAirportItem?.name ?? ""
            
            checkButtonEnable()
        }
    }
    
    let endAirportCell = LabelTableViewCell(title: "降落机场")
    
    let searchHeader = SearchHeaderView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "添加归国行程"
        
        /// textfield
        flyghtNoCell.textField.returnKeyType = .done
        flyghtNoCell.textField.keyboardType = .asciiCapable
        flyghtNoCell.textField.enablesReturnKeyAutomatically = true
        flyghtNoCell.textField.textAlignment = .right
        flyghtNoCell.textField.clearButtonMode = .whileEditing
        flyghtNoCell.textField.addTarget(self,
                                         action: #selector(ManualInputVC.changeFlyghtNo(sender:)),
                                         for: UIControlEvents.editingChanged)
        
        self.tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 55
        self.tableView.keyboardDismissMode = .onDrag
        
        searchHeader.button.addTarget(self,
                                      action: #selector(ManualInputVC.selectTicketItem),
                                      for: UIControlEvents.touchUpInside)
        searchHeader.button.setTitle("添加航班",
                                     for: .normal)
        
        checkButtonEnable()
    }
    
    func selectTicketItem() {
        let predicateString = "^[a-zA-Z0-9][a-zA-Z0-9][0-9]{2,4}$"
        
        if !NSPredicate(format: "SELF MATCHES %@", predicateString).evaluate(with: flyghtNo) {
            self.showToast(text: "航班格式输入不正确")
            
            return
        }
        
        if (startAirportItem?.code ?? "") == (endAirportItem?.code ?? "") {
            self.showToast(text: "不能输入相同的机场")
            
            return
        }
        
        let item = API.FlymentItemInArray(flightNo: flyghtNo,
                                          startAirportItem: startAirportItem!,
                                          endAirportItem: endAirportItem!,
                                          startDate: startFlyghtDate!,
                                          endDate: endFlyghtDate!)
        
        callBack?(item)
    }
    
    func changeFlyghtNo(sender:UITextField) {
        flyghtNo = sender.text ?? ""
    }
    
    func checkButtonEnable() {
        searchHeader.button.isEnabled = flyghtNo.length > 0 && startAirportItem != nil && endAirportItem != nil && startFlyghtDate != nil && endFlyghtDate != nil
    }
    
}

extension ManualInputVC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 1 {
            tableView.endEditing(true)
            
            let vc = UIDatePickerController(mode: .dateAndTime,
                                            miniDate: Date(),
                                            callBack: {
                                                [weak self] (selectedDate) in
                                                
                                                guard let `self` = self else {
                                                    return
                                                }
                                                
                                                self.startFlyghtDate = selectedDate
            })
            
            SliderPopViewController(vc,
                                    mode: .bottom,
                                    fillMode: .size,
                                    value: 250).present(on: self)
        } else if indexPath.section == 0 && indexPath.row == 2 {
            tableView.endEditing(true)
            
            let vc = AirportListVC(type: .departure)
            
            vc.callBack = {
                (item) in
                
                self.startAirportItem = item
                self.navigationController?.popViewController(animated: true)
            }
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        } else if indexPath.section == 0 && indexPath.row == 3 {
            tableView.endEditing(true)
            
            let vc = UIDatePickerController(mode: .dateAndTime,
                                            miniDate: Date(),
                                            callBack: {
                                                [weak self] (selectedDate) in
                                                
                                                guard let `self` = self else {
                                                    return
                                                }
                                                
                                                self.endFlyghtDate = selectedDate
            })
            
            SliderPopViewController(vc,
                                    mode: .bottom,
                                    fillMode: .size,
                                    value: 250).present(on: self)
        } else if indexPath.section == 0 && indexPath.row == 4 {
            tableView.endEditing(true)
            
            let vc = AirportListVC(type: .land)
            
            vc.callBack = {
                (item) in
                
                self.endAirportItem = item
                self.navigationController?.popViewController(animated: true)
            }
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 5
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return flyghtNoCell
        case 1:
            return startFlyghtDateCell
        case 2:
            return startAirportCell
        case 3:
            return endFlyghtDateCell
        case 4:
            return endAirportCell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            return ViewFactory.view(color: UIConfig.generalColor.white)
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 35
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 35
        }
        
        return .leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return searchHeader
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 79
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return UITableViewAutomaticDimension
        }
        
        return .leastNormalMagnitude
    }
}
