//
//  SearchAirportVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct SearchAirportSection {
    var city:String
    
    var items:[AirportItem]
    
    static func sections(from models:[API.AirportItem]) -> [SearchAirportSection] {
        var dict:[String:[AirportItem]] = [:]
        
        for model in models {
            let city = model.city
            
            if var array = dict[city] {
                array.append(AirportItem(model: model))
                
                dict[city] = array
            } else {
                dict[city] = [AirportItem(model: model)]
            }
        }
        
        return dict.keys.elements.sorted(by: <).map({ (city) -> SearchAirportSection in
            return SearchAirportSection(city: city, items: dict[city]!)
        })
    }
}

class SearchAirportVM: ViewModel {
    
    let searchAirports:Driver<[SearchAirportSection]>
    
    init(textInput:Observable<String>) {
        let obs = textInput
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
        
        let scheduler = SerialDispatchQueueScheduler(queue: .global(),
                                                     internalSerialQueueName: "Search")
    
        searchAirports = obs
            .observeOn(scheduler)
            .flatMapLatest({ (searchText) -> Observable<[SearchAirportSection]> in
                
                if searchText.length < 1 {
                    return Observable.just([])
                }
                
                return API
                    .AirportItem
                    .getItems(by: searchText)
                    .asModelsObservable()
                    .map({ SearchAirportSection.sections(from: $0) })
            }).asDriver(onErrorJustReturn: [])
    }
    
    func bind(to view: SearchAirportVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        searchAirports
            .drive(onNext: { (sections) in
                view.sections = sections
                view.tableView.reloadData()
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(bag)
        
        return bag
    }
    
}
