//
//  SearchAirportVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SearchAirportVC: UIViewController, SearchPresented, PresenterType {
    func search(keyword: String) {
        keywordSearchEvent.onNext(keyword)
    }
    
    let viewModel:SearchAirportVM
    
    fileprivate let keywordSearchEvent = PublishSubject<String>()
    
    var sections:[SearchAirportSection] = []
    
    let tableView:UITableView
    
    let textPublish:PublishSubject<String>
    
    let disposeBag = DisposeBag()
    
    var bindDisposeBag: DisposeBag?
    
    init() {
        tableView = UITableView(frame: .zero,
                                style: .grouped)
        
        textPublish = PublishSubject()
        
        viewModel = SearchAirportVM(textInput: textPublish.asObservable())
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    weak var searchTextField:UITextField? {
        didSet {
            guard let textField = searchTextField else { return }
            
            textField.returnKeyType = .done
            
            textField
                .rx
                .text
                .map({ $0 ?? "" })
                .distinctUntilChanged()
                .bind(to: textPublish)
                .addDisposableTo(disposeBag)
        }
    }
    
    var callBack:((AirportItem) -> Void)?
}

extension SearchAirportVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(tableView)
        
        tableView.snp.fillToSuperView()
        tableView.estimatedRowHeight = 35
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 45
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.sectionFooterHeight = .leastNormalMagnitude
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.registerView(viewClass: TableViewHeaderView.self)
        tableView.registerCell(cellClass: TableViewCell.self)
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        tableView.separatorStyle = .none
        
        bind()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchTextField?.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchTextField?.resignFirstResponder()
    }
}

extension SearchAirportVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as TableViewCell
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        cell.label.text = item.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableView() as TableViewHeaderView
        
        view.label.text = sections[section].city
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        if let callBack = self.callBack {
            self.dismiss(animated: true,
                         completion: {
                            callBack(item)
            })
        }
    }
}

extension SearchAirportVC {
    class TableViewHeaderView : UITableViewHeaderFooterView {
        let label:UILabel
        
        override init(reuseIdentifier: String?) {
            label = ViewFactory.generalLabel(generalSize: 15,
                                             textColor: UIConfig.generalColor.unselected)
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(label)
            
            label.lineBreakMode = .byTruncatingMiddle
            label.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(20, 15, 10, 15))
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension SearchAirportVC {
    class TableViewCell: UITableViewCell {
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            label = ViewFactory.generalLabel(generalSize: 15,
                                             textColor: UIConfig.generalColor.selected)
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(label)
            
            self.selectionStyle = .none
            
            label.lineBreakMode = .byTruncatingMiddle
            label.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(10, 30, 10, 30))
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
