//
//  AirportListVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/6.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift

class AirportListVC: UIViewController, PresenterType {
    
    let viewModel:AirportListVM
    
    var bindDisposeBag: DisposeBag?
    
    var sections:[AirportSection] = []
    
    let tableView:TableView
    
    let searchTextField = SearchPresentController.generalTextField(placeHolder: "上海/浦东机场/纽约")
    
    init(type:AirportType) {
        viewModel = AirportListVM(type: type)
        
        tableView = TableView(frame: .zero,
                              style: .plain)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildSearch()
        
        /// table view
        self.view.addSubview(tableView)
        
        self.tableView.parentViewController = self
        self.tableView.snp.fillToSuperView()
        self.tableView.rowHeight = 43
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        self.tableView.separatorColor = UIConfig.generalColor.lineColor
        self.tableView.sectionHeaderHeight = 20
        self.tableView.registerCell(cellClass: TableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.snp.makeConstraints { (maker) in
            maker.center.width.height.equalTo(self.view)
        }
        
        bind()
    }
    
    var callBack:((AirportItem) -> Void)?
}

extension AirportListVC: UITextFieldDelegate, SearchPresenting {
    // MARK: - Search Refer
    var searchTextFieldFrame:CGRect {
        return searchTextField.superview?.convert(searchTextField.frame,
                                                  to: self.view) ?? .zero
    }
    
    fileprivate func buildSearch() {
        
        searchTextField.frame = CGRect(x: 0,
                                       y: 0,
                                       width: UIScreen.main.bounds.size.width - 30,
                                       height: 30)
        
        self.navigationItem.titleView = searchTextField
        
        searchTextField.delegate = self
    }
    
    func showSearchViewController() {
        let vc = SearchAirportVC()
        
        vc.callBack = {
            item in
            
            self.select(item: item)
        }
        
        SearchPresentController(presented: vc,
                                presenting: self).present()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showSearchViewController()
        
        return false
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        
//    }
}

extension AirportListVC {
    
    class TableView: UITableView {
        var history:[AirportItem]   = []
        var hot:[AirportItem]       = []
        
        weak var parentViewController:AirportListVC?
        
        func historyButtonPressed(sender:UIButton) {
            let item = history[sender.tag]
            
            parentViewController?.select(item: item)
        }
        
        func hotButtonPressed(sender:UIButton) {
            let item = hot[sender.tag]
            
            parentViewController?.select(item: item)
        }
        
        override func reloadData() {
            
            if history.count == 0 && hot.count == 0 {
                self.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
            } else {
                let collectionView = UIView()
                
                var x:CGFloat = 15
                
                var y:CGFloat = 20
                
                let rightSide:CGFloat = self.w - 15
                
                var bottomSide:CGFloat = 20
                
                let getButton = {
                    (text:String) -> UIButton in
                    
                    let button = UIButton(type: .custom)
                    
                    button.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                    button.setAttributedTitle(NSAttributedString(string: text,
                                                                 font: UIConfig.generalFont(12),
                                                                 textColor: UIConfig.generalColor.labelGray),
                                              for: .normal)
                    button.sizeToFit()
                    button.titleLabel?.lineBreakMode = .byTruncatingMiddle
                    button.layer.cornerRadius = 10
                    button.layer.masksToBounds = true
                    button.layer.borderWidth = 0.5
                    button.layer.borderColor = UIConfig.generalColor.labelGray.cgColor
                    
                    return button
                }
                
                let addItemToCollectionView = {
                    (title:String, items:[AirportItem], action:Selector) -> Void in
                    
                    if items.count <= 0 {
                        return
                    }
                    
                    x = 15
                    
                    y = bottomSide
                    
                    /// add title
                    let titleLabel = ViewFactory.generalLabel(generalSize: 15,
                                                              textColor: UIConfig.generalColor.unselected)
                    
                    collectionView.addSubview(titleLabel)
                    
                    titleLabel.left = x
                    titleLabel.top = y
                    titleLabel.text = title
                    titleLabel.sizeToFit()
                    
                    y = titleLabel.bottom + 15
                    
                    /// add history
                    for (index, one) in items.enumerated() {
                        let button = getButton(one.name)
                        
                        collectionView.addSubview(button)
                        
                        let size = button.size
                        
                        if x + size.width > rightSide {
                            x = 15
                            
                            y = bottomSide
                            
                            if x + size.width > rightSide {
                                button.w = rightSide - 15
                            }
                        }
                        
                        button.left = x
                        button.top = y
                        button.tag = index
                        button.addTarget(self,
                                         action: action,
                                         for: UIControlEvents.touchUpInside)
                        
                        x = button.right + 10
                        
                        bottomSide = max(button.bottom + 10, bottomSide)
                    }
                    
                    bottomSide += 20
                }
                
                addItemToCollectionView("历史记录", history, #selector(TableView.historyButtonPressed(sender:)))
                addItemToCollectionView("热门机场", hot, #selector(TableView.hotButtonPressed(sender:)))
                
                
                /// add title
                x = 15
                
                y = bottomSide
                
                let titleLabel = ViewFactory.generalLabel(generalSize: 15,
                                                          textColor: UIConfig.generalColor.unselected)
                
                collectionView.addSubview(titleLabel)
                
                titleLabel.left = x
                titleLabel.top = y
                titleLabel.text = "全部机场"
                titleLabel.sizeToFit()
                
                bottomSide = titleLabel.bottom + 5
                
                collectionView.frame = CGRect(x: 0,
                                              y: 0,
                                              width: self.w,
                                              height: bottomSide + 10)
                
                self.tableHeaderView = collectionView
            }
            
            super.reloadData()
        }
    }
}

extension AirportListVC: UITableViewDelegate, UITableViewDataSource {
    
    typealias TableViewCell = CategoryVCBrandView.TableViewCell
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].index
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sections.map({ $0.index })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView, let label = header.textLabel {
            label.font = UIConfig.arialFont(13)
            label.textColor = UIColor(r: 175, g: 177, b: 179)
            label.backgroundColor = UIConfig.generalColor.backgroudGray
        }
        
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
        } else {
            view.backgroundColor = UIConfig.generalColor.backgroudGray
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.white
        } else {
            view.backgroundColor = UIConfig.generalColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as TableViewCell
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        cell.title.text = "\(item.name)(\(item.city))"
        
        return cell
    }
    
    func select(item:AirportItem) {
        DispatchQueue.global().async {
            AirportItem.addHistory(of: item)
        }
        
        callBack?(item)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = sections[indexPath.section].items[indexPath.row]
        
        select(item: item)
    }
}
