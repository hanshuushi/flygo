//
//  AirportListVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/6.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum AirportType: String {
    case departure, land
    
    var hotAirports:[AirportItem] {
        guard let dict = NSDictionary(contentsOfFile: Bundle.main.bundlePath + "/Airport.plist"),
        let array = dict[self.rawValue] as? [[String:Any]] else {
                return []
        }
        
        return array.map({ AirportItem(json: $0) })
    }
}

struct AirportSection {
    var index:String
    
    var items:[AirportItem]
}

struct AirportItem {
    var name:String
    
    var code:String
    
    var city:String
    
    var utc:Int
    
    var countryCode:String
    
    var countryName:String
    
    init(model:API.AirportItem) {
        name        = model.name
        code        = model.fs
        city        = model.city
        utc         = model.utcOffsetHours
        countryCode = model.countryCode
        countryName = model.countryName
    }
    
    init(json:[String:Any]) {
        name        = (json["name"] as? String) ?? ""
        code        = (json["code"] as? String) ?? ""
        city        = (json["city"] as? String) ?? ""
        utc         = (json["utc"] as? Int) ?? 0
        countryCode = (json["countryCode"] as? String) ?? ""
        countryName = (json["countryName"] as? String) ?? ""
    }
    
    var resultDictionary:[String:Any] {
        var dict = [String:Any]()
        
        dict["name"]        = name
        dict["code"]        = code
        dict["city"]        = city
        dict["utc"]         = utc
        dict["countryCode"] = countryCode
        dict["countryName"] = countryName
        
        return dict
    }
    
    private static func getHistoryItems() -> [AirportItem] {
        guard let data = UserDefaults.standard.value(forKey: "Airport_History") as? Data,
        let array = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String:Any]] else {
            return []
        }
        
        return array?.map({ (json) -> AirportItem in
            return AirportItem(json: json)
        }).takeMax(10) ?? []
    }
    
    static var historyItems:[AirportItem] = getHistoryItems()
    
    static func addHistory(of item:AirportItem) {
        historyItems = [item] + historyItems.filter({ item.name != $0.name })
        
        let array = historyItems.map({ $0.resultDictionary })
        
        guard let data = try? JSONSerialization.data(withJSONObject: array,
                                                     options: .prettyPrinted) else {
                                                        return
        }
        
        UserDefaults.standard.setValue(data, forKey: "Airport_History")
        UserDefaults.standard.synchronize()
    }
    
    #if DEBUG
    
    static let landAirportNames = ["南苑机场","首都机场","济南机场","青岛机场","广州白云机场","浦东机场","杭州机场","天津机场","香港国际机场","厦门机场"]
    
    static func setupAirportList() {
        
        let dict = NSMutableDictionary()
        
        var array = landAirportNames.map { (name) -> [String:Any] in
            let model = API.AirportItem.getItem(from: name)!
            
            let dict = AirportItem(model: model).resultDictionary
            
            return dict
        } as NSArray
        
        dict["land"] = array
        
//        var dbDocPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
//                                                            .userDomainMask,
//                                                            true)[0] + "/landAirport.plist"
//        
//        array.write(toFile: dbDocPath, atomically: true)
        
        array = departureAirportNames.map { (name) -> [String:Any] in
            let model = API.AirportItem.getItem(from: name)!
            
            let dict = AirportItem(model: model).resultDictionary
            
            return dict
            } as NSArray
        
        dict["departure"] = array
        
        let dbDocPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                        .userDomainMask,
                                                        true)[0] + "/Airport.plist"
        
        assert(!dict.write(toFile: dbDocPath, atomically: true))
    }
    
    static let departureAirportNames = ["东京成田机场","东京羽田机场","大阪关西国际机场","那霸机场","仁川国际机场","济洲机场","香港国际机场","澳门机场","台湾桃园国际机场","洛杉矶国际机场"]
    
    #endif
}

class AirportListVM: ViewModel {
    
    let type:AirportType
    
    init(type:AirportType) {
        self.type = type
    }
    
    func bind(to view: AirportListVC) -> DisposeBag? {
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .none,
                                       refreshEnable: false)
        
        let disposeBag = DisposeBag()
        
        let type = self.type
        
        dataSet.status.value = .loading
        dataSet.isEmpty.value = true
        
        DispatchQueue.global().async {
            let request = API
                .AirportItem
                .getAllItems()
                .shareReplay(1)
            
            request
                .observeOn(MainScheduler.instance)
                .bind { (item) in
                    switch item {
                    case .failed(let error):
                        dataSet.status.value = .error(error)
                    default:
                        return
                    }
                }
                .addDisposableTo(disposeBag)
            
            request
                .asModelsObservable()
                .observeOn(SerialDispatchQueueScheduler(queue: DispatchQueue.global(),
                                                        internalSerialQueueName: "Change"))
                .map {
                    (items) -> [AirportSection] in
                    
                    var dict = [String:[AirportItem]]()
                    
                    var cache = [String:String]()
                    
                    for item in items {
                        if item.name.length < 1 {
                            continue
                        }
                        
                        var index = (item.name as NSString).substring(to: 1)
                        
                        if NSPredicate(format: "SELF matches %@", "(^[\\u4e00-\\u9fa5]+$)").evaluate(with: index) {
                            if let _index = cache[index] {
                                index = _index
                            } else {
                                let _index = ((index as NSString).pinyin() as NSString).substring(to: 1)
                                
                                cache[index] = _index
                                
                                index = _index
                            }
                        }
                        
                        if var array = dict[index] {
                            array.append(AirportItem(model:item))
                            
                            dict[index] = array
                        } else {
                            dict[index] = [AirportItem(model:item)]
                        }
                    }
                    
                    let indexArray = dict.keys.elements.sorted(by: <)
                    
                    return indexArray.map({ AirportSection(index: $0, items: dict[$0]!) })
                }
                .observeOn(MainScheduler.instance)
                .bind {
                    [weak view] (items) in
                    
                    dataSet.isEmpty.value = items.count <= 0
                    dataSet.status.value = .normal
                    
                    view?.tableView.history = AirportItem.historyItems
                    view?.tableView.hot = type.hotAirports
                    view?.sections = items
                }
                .addDisposableTo(disposeBag)
        }
        
        disposeBag.insert(dataSet)
        
        return disposeBag
    }
}
