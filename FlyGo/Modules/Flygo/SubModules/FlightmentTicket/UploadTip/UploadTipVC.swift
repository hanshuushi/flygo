//
//  UploadTipVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/20.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class UploadTipVC: UIViewController {
    
    typealias PageControl = FlygoInfoVC.UserInfoCard.PageControl
    
    let pageControl:PageControl
    
    let scrollView:UIScrollView
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        scrollView = UIScrollView()
        
        pageControl = PageControl()
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let titles = ["短信截屏","购买回执","机票照片"]
    
    static var windowSize:CGSize {
        let radio:CGFloat = 930 / 600
        
        let screenSize = UIScreen.main.bounds.size
        
        let width = screenSize.width - 80
        
        let height = width * radio
        
        return CGSize(width: width, height: height)
    }
    
    var subViews:[UIView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // scroll
        self.view.addSubview(scrollView)
        
        scrollView.backgroundColor = UIConfig.generalColor.white
        scrollView.snp.fillToSuperView()
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.layer.cornerRadius = 5
        scrollView.layer.masksToBounds = true
        
        var prevView:UIView?
        
        subViews = []
        
        showed()
        
        for index in 0..<3 {
            // sub
            let subView = ViewFactory.view(color:UIConfig.generalColor.white)
            
            subViews.append(subView)
            
            scrollView.addSubview(subView)
            
            subView.snp.makeConstraints({ (maker) in
                maker.width.top.equalTo(scrollView)
                maker.height.equalTo(scrollView).offset(-50)
            })
            
            if let prevSubView = prevView {
                subView.snp.makeConstraints({ (maker) in
                    maker.left.equalTo(prevSubView.snp.right)
                })
            } else {
                subView.snp.makeConstraints({ (maker) in
                    maker.left.equalTo(scrollView)
                })
            }
            
            prevView = subView
            
            /// label
            let label = ViewFactory.generalLabel()
            
            subView.addSubview(label)
            
            label.text = UploadTipVC.titles[index]
            label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(subView).offset(30)
                maker.centerX.equalTo(subView)
            })
            
            /// imageview
            let imageView = UIImageView(image: UIImage(contentsOfFile: Bundle.main.bundlePath + "/UploadTipImages/\(index).jpg"))
            
            subView.addSubview(imageView)
            
            imageView.snp.makeConstraints({ (maker) in
                maker.left.equalTo(subView).offset(50)
                maker.right.equalTo(subView).offset(-50)
                maker.bottom.equalTo(subView)
                maker.height.equalTo(imageView.snp.width).multipliedBy(700.0 / 394.0)
            })
        }
        
        if let prevSubView = prevView {
            prevSubView.snp.makeConstraints({ (maker) in
                maker.right.equalTo(scrollView)
            })
        }
        
        // page 
        self.view.addSubview(pageControl)
        
        pageControl.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.centerY.equalTo(self.view.snp.bottom).offset(-30)
        }
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
    }
    
    func closePressed(sender:Any?) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UploadTipVC {
    
    fileprivate static let userDefaultKey = "UploadTipVC__2Show_Key"
    
    static func shouldShowTip() -> Bool {
        return !(UserDefaults.standard.value(forKey: userDefaultKey) as? Bool ?? false)
    }
    
    fileprivate func showed() {
        UserDefaults.standard.setValue(true, forKey: UploadTipVC.userDefaultKey)
        UserDefaults.standard.synchronize()
    }
}

extension UploadTipVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.w + 0.5)
        
        if scrollView.w >  0{
            for one in subViews {
                let opacity = 1.0 - fabs(one.left - scrollView.contentOffset.x) / scrollView.w
                
                one.alpha = max(0, min(1.0, opacity))
            }
        }
    }
}
