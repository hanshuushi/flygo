//
//  LocationTipVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/22.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import SnapKit

class LocationTipVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        let imageView = UIImageView(named: "icon_location_tip")
        
        self.view.addSubview(imageView)
        
        imageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.view).offset(70)
            maker.left.equalTo(self.view).offset(45)
            maker.right.equalTo(self.view).offset(-55)
            maker.height.equalTo(imageView.snp.width).multipliedBy(264.0 / 380.0)
        }
        
        let label = UILabel()
        
        label.backgroundColor = UIConfig.generalColor.white
        
        self.view.addSubview(label)
        
        label.numberOfLines = 0
        label.attributedText = LocationTipVC.attributedText
        label.snp.makeConstraints { (maker) in
            maker.top.equalTo(imageView.snp.bottom).offset(35)
            maker.width.equalTo(imageView)
            maker.centerX.equalTo(imageView)
        }
        
        let button = ViewFactory.redButton(title: "为了一个亿！")
        
        self.view.addSubview(button)
        
        button.layer.cornerRadius = 17
        button.layer.masksToBounds = true
        button.addTarget(self,
                         action: #selector(LocationTipVC.enableLocationClick(sender:)),
                         for: .touchUpInside)
        button.snp.makeConstraints { (maker) in
            maker.size.equalTo(CGSize(width:150, height:34))
            maker.centerX.equalTo(self.view)
            maker.top.equalTo(label.snp.bottom).offset(50)
        }
    }
    
    func enableLocationClick(sender:Any?) {
        LocationManager.shareInstance.startLocation()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    static var attributedText:NSAttributedString {
        return NSAttributedString(string: "定位未打开将无法使用部分飞哥功能，飞购不会实时访问您的位置，也不会透露您的隐私，请放心使用。",
                           font: UIConfig.generalFont(12),
                           textColor: UIConfig.generalColor.unselected,
                           lineSpace: 5)
    }
    
    static func sizeWithScreenFrom(bounds:CGRect) -> CGSize {
        let viewWidth = bounds.width - (42 * 2)
        
        let imageWidth = viewWidth - 45 - 55
        
        let imageHeight = imageWidth * 264 / 380
        
        let fontHeight = attributedText.boundingRect(with: CGSize(width:imageWidth, height:CGFloat.greatestFiniteMagnitude),
                                                     options: .usesLineFragmentOrigin,
                                                     context: nil).height
        
        return CGSize(width: viewWidth,
                      height: fontHeight + imageHeight + 70 + 35 + 50 + 34 + 35)
    }
}
