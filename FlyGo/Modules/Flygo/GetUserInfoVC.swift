//
//  GetUserInfoVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/12.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import EZSwiftExtensions
import RxSwift

class GetUserInfoVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        let loadingView = ViewFactory.loadingView()
        
        self.view.addSubview(loadingView)
        
        loadingView.snp.makeConstraints { (maker) in
            maker.center.equalTo(self.view)
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_switch"),
                                                                style: .plain,
                                                                target: FlygoVC.self,
                                                                action: #selector(FlygoVC.switchMain))
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UserManager.shareInstance.getUserInfo {[weak self] (success) in
            
            ez.runThisInMainThread {
                if !success {
                    self?.changeError()
                    
                    return
                }
                
                switch UserManager.shareInstance.flymanAuthState {
                case .authing:
                    self?.changeAuthing()
                case .normal:
                    self?.changeActivate()
                default:
                    self?.switchFlygoVC()
                }
            }
        }
    }
    
    func changeError() {
        self.view.removeSubviews()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.whiteGray)
        
        label.text = "请求信息失败"
        
        view.addSubview(label)
        
        let imageView = UIImageView(named: "icon_interface_error")
        
        view.addSubview(imageView)
        
        imageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(self.view.snp.centerY).offset(64)
            maker.centerX.equalTo(self.view)
        }
        
        label.snp.makeConstraints { (maker) in
            maker.top.equalTo(imageView.snp.bottom).offset(30)
            maker.centerX.equalTo(self.view)
        }
        
        //self.addFakeShadow()
    }
    
    func changeAuthing() {
        self.navigationController?.setViewControllers([FlymanReviewVC()],
                                                      animated: false)
    }
    
    func changeActivate() {
        self.navigationController?.setViewControllers([UploadChosenVC()],
                                                      animated: false)
    }
    
    func switchFlygoVC() {
        self.navigationController?.setViewControllers([FlygoVC.WebViewController()],
                                                      animated: false)
    }
    
}
