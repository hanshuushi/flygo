//
//  FlygoVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import WebKit
import RxSwift
import RxCocoa
import EZSwiftExtensions

class FlygoVC: CommonNavigationVC {
    
    let disposeBag:DisposeBag = DisposeBag()
    
    init() {
        super.init(rootViewController: GetUserInfoVC())
        
        setupRouteObserver(disposeBag: disposeBag)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static func switchMain() {
        let tration = CATransition()
        
        tration.duration = 0.35
        tration.type = "oglFlip"
        tration.subtype = kCATransitionFromLeft
        tration.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        UIApplication.shared.keyWindow?.layer.add(tration, forKey: "tration")
        UIApplication.shared.keyWindow?.rootViewController = MainVC.rootController()
    }
}

extension FlygoVC {
    class WebViewController: WKWebViewController {
        
        let disposeBag = DisposeBag()
        
        let uploadLocation = PublishSubject<Void>()
        
        init() {
            let openudid = OpenUDID.value() ?? ""
            
            let urlString = URLConfig.prefix + "flyapp/fly/orderindex.html?customerId=\(UserManager.shareInstance.currentId.urlEncode())&token=\(UserManager.shareInstance.currentToken.urlEncode())&nickname=\(UserManager.shareInstance.currentNickName.urlEncode())&imei=\(openudid.urlEncode())"
        
            let webURL = URL(string: urlString)!
            
            super.init(url: webURL)
            
            webView.configuration.userContentController.add(hybridContainer, name: "saveLocation")
            
            Driver.of(UserManager.shareInstance.nickName.map({ _ -> Void in () }),
                      UserManager.shareInstance.avatar.map({ _  -> Void in () }))
                .merge()
                .drive(onNext: {
                    [weak self](_) in
                    let _ = self?.webView.reload()
                    },
                       onCompleted: nil,
                       onDisposed: nil)
                .addDisposableTo(disposeBag)
            
            uploadLocation
                .withLatestFrom(LocationManager
                    .shareInstance
                    .locationItem
                    .asObservable())
                .bind { (item) in
                    API
                        .LocationTrack
                        .save(longitude: item.longitude,
                              latitude: item.latitude,
                              locationName: item.locationName,
                              countryName: item.countryName,
                              provinceName: item.provinceName,
                              cityName: item.cityName,
                              districtName: item.districtName,
                              streetName: item.streetName)
                        .responseNoModel({
                            print("上传成功")
                        },
                                         error: { (_) in
                                            print("坐标上传失败")
                        })
            }
                .addDisposableTo(disposeBag)
            
            LocationManager
                .shareInstance
                .authorization
                .distinctUntilChanged()
                .drive(onNext: {
                    [weak self] (_) in
                    let _ = self?.webView.reload()
                },
                       onCompleted: nil,
                       onDisposed: nil)
                .addDisposableTo(disposeBag)
            
            NotificationCenter
                .default
                .addObserver(self,
                             selector: #selector(WebViewController.applicationDidEnterBackground(notification:)),
                             name: .UIApplicationDidEnterBackground,
                             object: nil)
            
            NotificationCenter
                .default
                .addObserver(self,
                             selector: #selector(WebViewController.applicationWillEnterForeground(notification:)),
                             name: .UIApplicationWillEnterForeground,
                             object: nil)
            
            NotificationCenter
                .default
                .addObserver(self,
                             selector: #selector(WebViewController.ticketAddSuccess(notification:)),
                             name: .TicketAddSuccessNotification,
                             object: nil)
            
            NotificationCenter
                .default
                .addObserver(self,
                             selector: #selector(WebViewController.switchTripManager(notification:)),
                             name: .SwitchTripManagerNotification,
                             object: nil)
        }
        
        deinit {
            NotificationCenter
                .default
                .removeObserver(self)
            
            LocationManager.shareInstance.stopLocation()
        }
        
        func ticketAddSuccess(notification:Notification) {
            let openudid = OpenUDID.value() ?? ""
            
            let urlString = URLConfig.prefix + "flyapp/fly/orderindex.html?customerId=\(UserManager.shareInstance.currentId.urlEncode())&token=\(UserManager.shareInstance.currentToken.urlEncode())&nickname=\(UserManager.shareInstance.currentNickName.urlEncode())&imei=\(openudid.urlEncode())"
            
            let webURL = URL(string: urlString)!
            
            let request = URLRequest(url: webURL)
            
            webView.load(request)
        }
        
        func applicationDidEnterBackground(notification:Notification) {
            LocationManager.shareInstance.stopLocation()
        }
        
        func switchTripManager(notification:Notification) {
            let urlString = URLConfig.prefix + "flyapp/fly/unfinished.html#!/managementtrip.html?customerId=\(UserManager.shareInstance.currentId.urlEncode())&token=\(UserManager.shareInstance.currentToken.urlEncode()))"
            
            let webURL = URL(string: urlString)!
            
            let request = URLRequest(url: webURL)
            
            webView.load(request)
        }
        
        func applicationWillEnterForeground(notification:Notification) {
            launchLocation()
        }
        
        func launchLocation() {
            if LocationManager.shareInstance.locationEnable {
                LocationManager.shareInstance.startLocation()
                
                uploadLocation.onNext()
                
            } else {
                ez.runThisAfterDelay(seconds: 0.35) {
                    
                    self.webView.reload()
                    
                    PanelPresentController(presentedViewController: LocationTipVC(nibName: nil, bundle: nil),
                                           presenting: self.navigationController!,
                                           panelLayout: .size(LocationTipVC.sizeWithScreenFrom(bounds: UIScreen.main.bounds))).present()
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private let _onceToken = NSUUID().uuidString
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            
            if !UserManager.shareInstance.isLogined() {
                FlygoVC.switchMain()
            }
            
            DispatchQueue.once(token: self._onceToken) {
                self.launchLocation()
            }
        }
        
        override var coverView: UIView? {
            set {
                super.coverView = newValue
                
                if let cv = newValue {
                    cv.snp.remakeConstraints ({ (maker) in
                        maker.centerX.width.equalTo(view)
                        maker.size.equalTo(view)
                        maker.centerY.equalTo(view)
                    })
                }
            }
            get {
                return super.coverView
            }
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.automaticallyAdjustsScrollViewInsets = false
            
            self.title = "飞哥"
            
            webView.scrollView.setInset(top: 0, bottom: 0)
            webView.scrollView.bounces = false
            webView.snp.remakeConstraints { (maker) in
                maker.edges.equalTo(self.view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
            }
            
            progressView.snp.remakeConstraints { (maker) in
                maker.left.right.equalTo(self.view)
                maker.top.equalTo(self.view).offset(20)
                maker.height.equalTo(3)
            }
        }
        
        override var showNavigationBar: Bool {
            return false
        }
    }
}


extension FlygoVC.WebViewController {
    
    fileprivate func hybridPosition() {
        
        let evaString = "positions({'location': '\(LocationManager.shareInstance.locationEnable ? "true" : "false")'})"
        
        webView.evaluateJavaScript(evaString,
            completionHandler: { (_, _) in
        })
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        super.webView(webView, didFinish: navigation)
    }
}

extension FlygoVC.WebViewController {
    typealias UserInfo = API.UserInfo
    
    override func userContentController(_ userContentController: WKUserContentController,
                               didReceive message: WKScriptMessage) {
        
        super.userContentController(userContentController, didReceive: message)
        
        if message.name == "saveLocation" {
            hybridPosition()
            
            uploadLocation.onNext()
        }
    }
}

extension FlygoVC.WebViewController {
    class StoreWebViewController: WKWebViewController {
        
        let currentURL:URL
        
        override init(url: URL) {
            currentURL = url
            
            super.init(url: url)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            
            self.automaticallyAdjustsScrollViewInsets = false
            
            super.viewDidLoad()
            
            webView.scrollView.setInset(top: 64, bottom: 0)
            
            self.title = "免税店"
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "在Safari中打开",
                                                                     style: .plain,
                                                                     target: self,
                                                                     action: #selector(StoreWebViewController.openInSafary))
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            
            self.navigationController?.setToolbarHidden(false,
                                                        animated: animated)
        }
        
        func openInSafary() {
            UIApplication.shared.openURL(currentURL)
        }
    }
}
