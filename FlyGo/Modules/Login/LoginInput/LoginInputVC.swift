//
//  LoginInput.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/4.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import EZSwiftExtensions

class LoginInputVC: UIViewController {
    
    let disposeBag = DisposeBag()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "登录"
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        // add gesture
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(LoginInputVC.backgroudTap(gesture:)))
        
        self.view.addGestureRecognizer(tapGesture)
        
        let swipeGesture = UISwipeGestureRecognizer(target: self,
                                                    action: #selector(LoginInputVC.backgroudTap(gesture:)))
        
        swipeGesture.direction = [.up, .down]
        
        self.view.addGestureRecognizer(swipeGesture)
        
        // 账号
        let userLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                          textColor: UIConfig.generalColor.selected)
        
        userLabel.text = "账号"
        
        let userInput = UITextField()
        
        userInput.font = UIConfig.generalFont(15)
        userInput.textColor = UIConfig.generalColor.selected
        userInput.attributedPlaceholder = NSAttributedString(string: "请输入手机号",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.whiteGray)
        userInput.backgroundColor = UIConfig.generalColor.white
        userInput.returnKeyType = .next
        userInput.keyboardType = .phonePad
        userInput.delegate = self
        
        let areaCodeButton = ViewFactory.redButton(title: "")
        
        areaCodeButton.frame = CGRect(x: 0, y: 0, width: 40, height: 22)
        areaCodeButton.layer.cornerRadius = 4
        areaCodeButton.layer.masksToBounds = true
        
        let leftView = ViewFactory.view(color: UIConfig.generalColor.white)
        
        leftView.frame = CGRect(x: 0, y: 0, width: 50, height: 22)
        leftView.addSubview(areaCodeButton)
        
        userInput.leftView = leftView
        userInput.leftViewMode = .always
        userInput.tag = 1
        
        
        let userBottomLine = ViewFactory.line()
        
        // 密码
        let pswLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                          textColor: UIConfig.generalColor.selected)
        
        pswLabel.text = "密码"
        
        let pswInput = UITextField()
        
        pswInput.font = UIConfig.generalFont(15)
        pswInput.textColor = UIConfig.generalColor.selected
        pswInput.attributedPlaceholder = NSAttributedString(string: "请输入6-16位密码",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.whiteGray)
        pswInput.backgroundColor = UIConfig.generalColor.white
        pswInput.returnKeyType = .done
        pswInput.tag = 2
        pswInput.isSecureTextEntry = true
        pswInput.delegate = self
        
        let pswBottomLine = ViewFactory.line()
        
        // 忘记密码
        let fgtButton = UIButton(type: .custom)
        
        fgtButton.setAttributedTitle(NSAttributedString(string: "忘记密码",
                                                        font: UIConfig.generalFont(13),
                                                        textColor: UIConfig.generalColor.unselected),
                                     for: .normal)
        fgtButton.backgroundColor = UIConfig.generalColor.white
        fgtButton.contentHorizontalAlignment = .right
        fgtButton.addTarget(self,
                            action: #selector(LoginInputVC.showForgetPressed(sender:)),
                            for: UIControlEvents.touchUpInside)
        
        // 登录
        let lgiButton = ViewFactory.redButton(title: "登录")
        
        lgiButton.layer.cornerRadius = 17.5
        lgiButton.layer.masksToBounds = true
        lgiButton.addTarget(self,
                            action: #selector(LoginInputVC.buttonTap(sender:)),
                            for: UIControlEvents.touchUpInside)
        
        // 注册
        let rgsButton = UIButton(type: .custom)
        
        let rgsAttributedString = NSMutableAttributedString(string: "还没有账号？去",
                                                            font: UIConfig.generalFont(15),
                                                            textColor: UIConfig.generalColor.unselected)
        
        rgsAttributedString.append(NSAttributedString(withUnderLine: "注册",
                                                      font: UIConfig.generalFont(15),
                                                      textColor: UIConfig.generalColor.red))
        
        rgsButton.setAttributedTitle(rgsAttributedString,
                                     for: .normal)
        rgsButton.backgroundColor = UIConfig.generalColor.white
        rgsButton.contentHorizontalAlignment = .right
        rgsButton.addTarget(self,
                            action: #selector(LoginInputVC.showRegisterPressed(sender:)),
                            for: UIControlEvents.touchUpInside)
        // Layout
        self.view.addSubviews(userLabel, userInput, userBottomLine, pswLabel, pswInput, pswBottomLine, fgtButton, lgiButton, rgsButton)
        
        pswBottomLine.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(63)
            maker.right.equalTo(self.view).offset(-63)
            maker.height.equalTo(0.5)
            maker.centerY.equalTo(self.view).offset(6)
        }
        
        pswInput.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(pswBottomLine.snp.top).offset(-13)
            maker.left.right.equalTo(pswBottomLine)
        }
        
        pswLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(pswInput)
            maker.bottom.equalTo(pswInput.snp.top).offset(-17)
        }
        
        userBottomLine.snp.makeConstraints { (maker) in
            maker.left.right.height.equalTo(pswBottomLine)
            maker.bottom.equalTo(pswLabel.snp.top).offset(-23)
        }
        
        userInput.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(userBottomLine.snp.top).offset(-13)
            maker.left.right.equalTo(pswBottomLine)
        }
        
        userLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(userInput)
            maker.bottom.equalTo(userInput.snp.top).offset(-17)
        }
        
        fgtButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(pswBottomLine)
            maker.size.equalTo(CGSize(width: 60,
                                      height: 43))
            maker.top.equalTo(pswBottomLine.snp.bottom)
        }
        
        lgiButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.left.right.equalTo(pswBottomLine)
            maker.height.equalTo(35)
            maker.top.equalTo(pswBottomLine.snp.bottom).offset(52)
        }
        
        rgsButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.top.equalTo(lgiButton.snp.bottom).offset(20)
        }
        
        //self.addFakeShadow()
        
        // bind all observer
        
        /// code
        let code = areaCodeButton
            .rx
            .tap
            .flatMapLatest({[weak self] in AreaCodeVC.selectedArea(on:self) })
            .startWith(86)
            .shareReplay(1)
        
        code
            .observeOn(MainScheduler.instance)
            .bind {
                (codeValue) in
                
                areaCodeButton.setAttributedTitle(NSAttributedString(string: "+\(codeValue)",
                    font: UIConfig.generalFont(10),
                    textColor: UIConfig.generalColor.white),
                                                  for: .normal)
            }.addDisposableTo(disposeBag)
        
        /// buttonEnable
        let loginInfo = Observable.combineLatest(userInput.rx.text,
                                                 pswInput.rx.text,
                                                 code.asObservable()) {
                                                    (user, psw, code) -> (user:String, password:String, code:Int) in
                                                    return (user:user ?? "", password:psw ?? "", code:code)
            }
            .shareReplay(1)
        
        let buttonEnable = loginInfo.map { (info) -> Bool in
            return info.user.isTel() && info.password.length >= 6 && info.password.length <= 16
            }
            .startWith(false)
        
        buttonEnable
            .bind(to: lgiButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        /// login action
        lgiButton.rx.tap.withLatestFrom(loginInfo).bind {
            [weak self] (info) in
            
            guard let strongSelf = self else {
                return
            }
            
            if !info.password.isPassword() {
                strongSelf.showToast(text: "密码仅可以为数字，字母和下划线，区分大小写")
                
                pswInput.becomeFirstResponder()
                
                return
            }
            
            let toast = strongSelf.showLoading()
            
            API.UserInfo.login(areaCode: info.code,
                               mobile: info.user,
                               passWord: info.password,
                               imei: OpenUDID.value()!).responseModel({ (userInfo:API.UserInfo) in
                                
                                UserManager.shareInstance.login(with: userInfo)
                                
                                CartVM.refreshNotification()
                                
                                ez.runThisInMainThread {
                                    toast.dismiss(animated: true,
                                                  completion: {
                                                    if let loginVC = strongSelf.navigationController as? LoginVC {
                                                        loginVC.loginSuccess()
                                                    }
                                    })
                                }
                                
                               }).error({ (error) in
                                ez.runThisInMainThread {
                                    toast.displayLabel(text: error.localizedDescription)
                                }
                               })
            
        }.addDisposableTo(disposeBag)
    }
}

extension LoginInputVC {
    func showRegisterPressed(sender:Any?) {
        let registerVC = MobileSMSInputVC()
        
        self.navigationController?.pushViewController(registerVC,
                                                      animated: true)
    }
    
    func showForgetPressed(sender:Any?) {
        let registerVC = MobileSMSInputVC()
        
        registerVC.isRegister = false
        
        self.navigationController?.pushViewController(registerVC,
                                                      animated: true)
    }
}

// MARK: - TextField Delegate
extension LoginInputVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            let tag = textField.tag
            
            if textField.returnKeyType == .next {
                if let nextTextField = self.view.viewWithTag(tag + 1) as? UITextField {
                    nextTextField.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.resignFirstResponder()
            }
            
            return false
        }
        
        return true
    }
}

// MARK: - End Edit Gesture
extension LoginInputVC {
    func backgroudTap(gesture:UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func buttonTap(sender:Any?) {
        self.view.endEditing(true)
    }
}
