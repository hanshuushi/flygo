//
//  SupplementInfoVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/5.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SupplementInfoVC: UIViewController, PresenterType {
    
    let viewModel:SupplementInfoVM
    
    var bindDisposeBag: DisposeBag?
    
    let disposeBag:DisposeBag
    
    let tableView:UITableView
    
    let infoCells:[UITableViewCell]
    
    init() {
        self.viewModel = SupplementInfoVM()
        
        /// init table view
        tableView = UITableView(frame: .zero,
                                style: .plain)
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        //        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        //        tableView.estimatedSectionHeaderHeight = 110
        
        /// init disposebag
        disposeBag = DisposeBag()
        
        /// init variable
        nickName = Variable("")
        
        telephone = Variable("")
        
        avatar = Variable(nil)
        
        /// init cell
        var tempArray = [UITableViewCell]()
        
        let avatarCell = ImageTableViewCell(style: .default,
                                            reuseIdentifier: "avatar")
        
        avatarCell.label.text = "头像"
        
        avatar
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { (picUrl) in
                avatarCell.infoImageView.setAvatar(url: picUrl)
            }
            .addDisposableTo(disposeBag)
        
        tempArray.append(avatarCell)
        
        let phoneCell = InfoTableViewCell(style: .default,
                                          reuseIdentifier: "phoneCell")
        
        phoneCell.set(title: "手机号",
                      info: telephone.value,
                      showAccess: false)
        
        tempArray.append(phoneCell)
        
        telephone
            .asObservable()
            .bind(to: phoneCell.infoLabel.rx.text)
            .addDisposableTo(disposeBag)
        
        let nameCell = InfoTableViewCell(style: .default,
                                         reuseIdentifier: "nameCell")
        
        nameCell.set(title: "更改昵称",
                     info: nickName.value,
                     showAccess: true)
        
        tempArray.append(nameCell)
        
        nickName
            .asObservable().bind(onNext: { (nickName) in
                nameCell.set(title: "更改昵称",
                             info: nickName,
                             showAccess: true)
            })
            .addDisposableTo(disposeBag)
        
        let passwordCell = InfoTableViewCell(style: .default,
                                             reuseIdentifier: "passwordCell")
        
        passwordCell.set(title: "设置密码",
                         info: "",
                         showAccess: true)
        
        tempArray.append(passwordCell)
        
        infoCells = tempArray
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nickName:Variable<String>
    
    let telephone:Variable<String>
    
    let avatar:Variable<URL?>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self
            .navigationItem
            .leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon_close"),
                                                 style: .plain,
                                                 target: self,
                                                 action: #selector(SupplementInfoVC.cancelSupplementPressed(sender:)))
        
        self.title = "补充资料"
        
        /// add table view
        self.view.addSubview(tableView)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        
        // add Fake Shadow()
        Observable
            .of(nickName.asObservable(), telephone.asObservable())
            .merge()
            .bind { [weak self] _ in
                self?.tableView.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        /// login out button
        bind()
    }
    
    func cancelSupplementPressed(sender:Any) {
        if let loginVC = self.navigationController as? LoginVC {
            loginVC.loginSuccess()
        }
    }
}

extension SupplementInfoVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return infoCells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 2:
            let viewController = NicknameInputVC()
            
            self.pushVC(viewController)
        case 0:
            ImagePicker.updateAvatar(show: self,
                                     response: { (_, _, _) in
                                        
            })
        case 3:
            let viewController = PasswordSettingVC()
            
            viewController.mobile = UserManager.shareInstance.currentTelephone
            viewController.areaCode = "\(UserManager.shareInstance.areaCode)"
            viewController.successHandler = {
                [weak self] in
                
                guard let `self` = self else {
                    return
                }
                
                if let loginVC = self.navigationController as? LoginVC {
                    loginVC.loginSuccess()
                }
            }
            
            self.pushVC(viewController)
        default:
            return
        }
    }
}

extension SupplementInfoVC {
    typealias InfoTableViewCell = UserAccountVC.InfoTableViewCell
    
    typealias ImageTableViewCell = UserAccountVC.ImageTableViewCell
}
