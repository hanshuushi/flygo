//
//  AreaCodeVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/7.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class AreaCodeVC: UIViewController {
    static var itemCollection:[String:[AreaItem]] = {
        let filePath = Bundle.main.bundlePath + "/AreaCode.plist"
        
        guard let itemList = NSArray(contentsOfFile: filePath) as? [[String:Any]] else {
            return [String:[AreaItem]]()
        }
        
        var itemCollection = [String:[AreaItem]]()
        
        for dict in itemList {
            guard let type = dict["type"] as? String,
                let cieies = dict["cieies"] as? [String] else {
                continue
            }
            
            itemCollection[type] = cieies
                .map{AreaItem(index:type, text:$0)}
                .filter{ $0 != nil}
                .map{ $0! }
        }
        
        return itemCollection
    }()
    
    let disposeBag = DisposeBag()
    
    let tableView:UITableView = UITableView(frame: .zero,
                                            style: .plain)
}

extension AreaCodeVC {
    static func selectedArea(on parent:UIViewController?) -> Observable<Int> {
        return Observable<Int>.create({
            [weak parent] (observer) -> Disposable in
            let vc = AreaCodeVC()
            
            guard let navigationController = parent?.navigationController else {
                observer.onCompleted()
                
                return Disposables.create()
            }
            
            navigationController.pushViewController(vc,
                                                    animated: true)
            
            let p1 = vc
                .tableView
                .rx
                .modelSelected(AreaItem.self)
                .take(1)
                .map{ $0.code }
                .bind(to: observer)
            
            return Disposables.create(p1,
                                      Disposables.create {
                                        let _ = vc.navigationController?.popViewController(animated: true)
            })
        })
    }
}

extension AreaCodeVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// table view
        self.view.addSubview(self.tableView)
        
        self.tableView.rowHeight = 43
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        self.tableView.separatorColor = UIConfig.generalColor.lineColor
        self.tableView.sectionHeaderHeight = 20
        self.tableView.registerCell(cellClass: TableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.snp.makeConstraints { (maker) in
            maker.center.width.height.equalTo(self.view)
        }
        self.tableView.tableHeaderView = {
            let tableHeaderView = UIView(frame: CGRect(x: 0,
                                                       y: 0,
                                                       width: UIScreen.main.bounds.width,
                                                       height: 55))
            
            tableHeaderView.backgroundColor = UIConfig.generalColor.white
            
            let label = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                          textColor: UIConfig.generalColor.selected)
            
            tableHeaderView.addSubview(label)
            
            label.text = "选择您的国家和地区"
            label.sizeToFit()
            label.centerYInSuperView()
            label.left = 15
            
            return tableHeaderView
        }()
        
        var models = [SectionModel<String, AreaItem>]()
        
        for (key, val) in AreaCodeVC.itemCollection {
            models.append(SectionModel(model: key, items: val.sorted(by: { (left, right) -> Bool in
                return left.name < right.name
            })))
        }
        
        Observable
            .just(models.sorted(by: { (left, right) -> Bool in
                return left.identity < right.identity
            }))
            .observeOn(MainScheduler.instance)
            .bind(to: self.tableView.rx.items(dataSource: AreaCodeVC.configureDataSource()))
            .addDisposableTo(disposeBag)
    }
}

extension AreaCodeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView, let label = header.textLabel {
            label.font = UIConfig.arialFont(13)
            label.textColor = UIColor(r: 175, g: 177, b: 179)
            label.backgroundColor = UIConfig.generalColor.backgroudGray
        }
        
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
        } else {
            view.backgroundColor = UIConfig.generalColor.backgroudGray
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.white
        } else {
            view.backgroundColor = UIConfig.generalColor.white
        }
    }
}

extension AreaCodeVC {
    struct AreaItem {
        let index:String
        
        let name:String
        
        let code:Int
        
        var displayName:String {
            return "\(name) (+\(code))"
        }
        
        init?(index:String, text:String) {
            self.index = index
            
            let array = text.split(",")
            
            if array.count < 2 {
                return nil
            }
            
            guard let code = array[0].toInt(), code > 0 else {
                return nil
            }
            
            self.code = code
            
            self.name = array[1]
        }
    }
    
    typealias TableViewCell = CategoryVCBrandView.TableViewCell
    
    static func configureDataSource() -> RxTableViewSectionedReloadDataSource<SectionModel<String, AreaItem>> {
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, AreaItem>>()
        
        dataSource.configureCell = { (_, tv, ip, item: AreaItem) in
            let cell:TableViewCell = tv.dequeueReusableCell(at: ip)
            
            cell.title.text = item.displayName
            
            return cell
        }
        
        dataSource.titleForHeaderInSection = { return $0[$1].model }
        dataSource.sectionForSectionIndexTitle = { return $2 }
        dataSource.sectionIndexTitles = { return $0.sectionModels.map({ $0.model }) }
        
        return dataSource
    }
}
