//
//  LoginVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import WebKit
import RxSwift

fileprivate let openudid = OpenUDID.value() ?? ""

fileprivate let webURL = URL(string: (URLConfig.prefix + "flyapp/login.html?imei=\(openudid)").urlEncode())!

class LoginVC: PresentNavigationVC {
    
    var loginedAction:(() -> Void)?
    
    static func doActionIfNeedLogin(action:@escaping () -> Void) {
        if UserManager.shareInstance.isLogined() {
            action()
            
            return
        }
        
        let sessionVC = LoginVC(nibName: nil,
                                bundle: nil)
        
        sessionVC.loginedAction = action
        
        sessionVC.setViewControllers([MobileLoginVC(nibName: nil,
                                                   bundle: nil)],
                                     animated: false)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(sessionVC,
                                                                    animated: true,
                                                                    completion: nil)
    }
    
    static func showLoginSession(on viewController:UIViewController) -> UIViewController {
        
        print("showLoginSession")
        
        let sessionVC = LoginVC(nibName: nil,
                                bundle: nil)
        
        sessionVC.setViewControllers([MobileLoginVC(nibName: nil,
                                                   bundle: nil)],
                                     animated: false)
        
        viewController.present(sessionVC,
                               animated: false,
                               completion: nil)
        
        return sessionVC
    }
    
    func loginSuccess() {
        self.dismiss(animated: true,
                     completion: loginedAction)
    }
}
