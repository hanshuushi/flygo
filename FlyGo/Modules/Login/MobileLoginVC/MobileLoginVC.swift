//
//  MobileLoginVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/4.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

import Foundation
import EZSwiftExtensions
import RxSwift
import RxCocoa

class MobileLoginVC: UIViewController {
    
    let disposeBag = DisposeBag()
    
    var isRegister = true
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        
        let isRegister = self.isRegister
        
        super.viewDidLoad()
        
        self.title = "登录"
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        // add gesture
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(LoginInputVC.backgroudTap(gesture:)))
        
        self.view.addGestureRecognizer(tapGesture)
        
        let swipeGesture = UISwipeGestureRecognizer(target: self,
                                                    action: #selector(LoginInputVC.backgroudTap(gesture:)))
        
        swipeGesture.direction = [.up, .down]
        
        self.view.addGestureRecognizer(swipeGesture)
        
        // 账号
        let userLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                          textColor: UIConfig.generalColor.selected)
        
        userLabel.text = "账号"
        
        let userInput = UITextField()
        
        userInput.font = UIConfig.generalFont(15)
        userInput.textColor = UIConfig.generalColor.selected
        userInput.attributedPlaceholder = NSAttributedString(string: "请输入手机号",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.whiteGray)
        userInput.backgroundColor = UIConfig.generalColor.white
        userInput.returnKeyType = .next
        userInput.keyboardType = .phonePad
        userInput.delegate = self
        
        let areaCodeButton = ViewFactory.redButton(title: "")
        
        areaCodeButton.frame = CGRect(x: 0, y: 0, width: 40, height: 22)
        areaCodeButton.layer.cornerRadius = 4
        areaCodeButton.layer.masksToBounds = true
        
        let leftView = ViewFactory.view(color: UIConfig.generalColor.white)
        
        leftView.frame = CGRect(x: 0, y: 0, width: 50, height: 22)
        leftView.addSubview(areaCodeButton)
        
        userInput.leftView = leftView
        userInput.leftViewMode = .always
        userInput.tag = 1
        
        
        let userBottomLine = ViewFactory.line()
        
        // 验证码
        let chkLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                         textColor: UIConfig.generalColor.selected)
        
        chkLabel.text = "验证码"
        
        let chkInput = UITextField()
        
        chkInput.font = UIConfig.generalFont(15)
        chkInput.textColor = UIConfig.generalColor.selected
        chkInput.attributedPlaceholder = NSAttributedString(string: "请输入验证码",
                                                            font: UIConfig.generalFont(15),
                                                            textColor: UIConfig.generalColor.whiteGray)
        chkInput.backgroundColor = UIConfig.generalColor.white
        chkInput.returnKeyType = .done
        chkInput.keyboardType = .numberPad
        chkInput.delegate = self
        
        let chkCodeButton = ViewFactory.redButton(title: "")
        
        chkCodeButton.frame = CGRect(x: 10, y: 0, width: 73, height: 22)
        chkCodeButton.layer.cornerRadius = 4
        chkCodeButton.layer.masksToBounds = true
        chkCodeButton.addTarget(self,
                                action: #selector(MobileSMSInputVC.buttonTap(sender:)),
                                for: UIControlEvents.touchUpInside)
        
        let rightView = ViewFactory.view(color: UIConfig.generalColor.white)
        
        rightView.frame = CGRect(x: 0, y: 0, width: 83
            , height: 22)
        rightView.addSubview(chkCodeButton)
        
        chkInput.rightView = rightView
        chkInput.rightViewMode = .always
        chkInput.tag = 2
        
        let chkBottomLine = ViewFactory.line()
        
        // 提交
        let sbmButton = ViewFactory.redButton(title: "提交")
        
        sbmButton.layer.cornerRadius = 17.5
        sbmButton.layer.masksToBounds = true
        sbmButton.addTarget(self,
                            action: #selector(MobileSMSInputVC.buttonTap(sender:)),
                            for: UIControlEvents.touchUpInside)
        
        // 注册条款
        let clauseButton = UIButton(type: .custom)
        
        let clauseAttributedString = NSMutableAttributedString(string: "登录即代表同意飞购",
                                                               font: UIConfig.generalFont(12),
                                                               textColor: UIConfig.generalColor.unselected)
        
        clauseAttributedString.append(NSAttributedString(withUnderLine: "飞购用户协议",
                                                         font: UIConfig.generalFont(12),
                                                         textColor: UIConfig.generalColor.unselected))
        
        clauseButton.setAttributedTitle(clauseAttributedString,
                                        for: .normal)
        clauseButton.backgroundColor = UIConfig.generalColor.white
        clauseButton.contentHorizontalAlignment = .right
        clauseButton.addTarget(self,
                               action: #selector(MobileSMSInputVC.showAgreeMent(sender:)),
                               for: UIControlEvents.touchUpInside)
        
        // 登录
        let lgButton = UIButton(type: .custom)
        
        let lgAttributedString = NSMutableAttributedString(string: "用密码登录",
                                                            font: UIConfig.generalFont(13),
                                                            textColor: UIConfig.generalColor.labelGray)
        
        lgButton.setAttributedTitle(lgAttributedString,
                                     for: .normal)
        lgButton.backgroundColor = UIConfig.generalColor.white
        lgButton.contentHorizontalAlignment = .right
        lgButton.addTarget(self,
                            action: #selector(MobileLoginVC.switchLoginFromPassword(sender:)),
                            for: UIControlEvents.touchUpInside)
        
        // Layout
        self.view.addSubviews(userLabel, userInput, userBottomLine, chkLabel, chkInput, chkBottomLine, sbmButton, clauseButton, lgButton)
        
        chkBottomLine.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(63)
            maker.right.equalTo(self.view).offset(-63)
            maker.height.equalTo(0.5)
            maker.centerY.equalTo(self.view).offset(6)
        }
        
        chkInput.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(chkBottomLine.snp.top).offset(-12)
            maker.left.right.equalTo(chkBottomLine)
        }
        
        chkLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(chkInput)
            maker.bottom.equalTo(chkInput.snp.top).offset(-13)
        }
        
        userBottomLine.snp.makeConstraints { (maker) in
            maker.left.right.height.equalTo(chkBottomLine)
            maker.bottom.equalTo(chkLabel.snp.top).offset(-22)
        }
        
        userInput.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(userBottomLine.snp.top).offset(-11)
            maker.left.right.equalTo(chkBottomLine)
        }
        
        userLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(userInput)
            maker.bottom.equalTo(userInput.snp.top).offset(-17)
        }
        
        lgButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(userInput)
            maker.top.equalTo(chkBottomLine.snp.bottom).offset(7.5)
        }
        
        sbmButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.left.right.equalTo(chkBottomLine)
            maker.height.equalTo(35)
            maker.top.equalTo(lgButton.snp.bottom).offset(16.5)
        }
        
        clauseButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.top.equalTo(sbmButton.snp.bottom).offset(3)
        }
        
        if !isRegister {
            clauseButton.isHidden = true
        }
        
        //self.addFakeShadow()
        
        // bind all observer
        
        /// code
        
        //// areacode
        
        let areacode = areaCodeButton
            .rx
            .tap
            .flatMapLatest({[weak self] in AreaCodeVC.selectedArea(on:self) })
            .startWith(86)
            .shareReplay(1)
        
        areacode
            .observeOn(MainScheduler.instance)
            .bind {
                (codeValue) in
                
                areaCodeButton.setAttributedTitle(NSAttributedString(string: "+\(codeValue)",
                    font: UIConfig.generalFont(10),
                    textColor: UIConfig.generalColor.white),
                                                  for: .normal)
            }.addDisposableTo(disposeBag)
        //// SMS Code
        let collection = Observable.combineLatest(areacode.asObservable(),
                                                  userInput.rx.text) {
                                                    (chkCode, user) -> (code:Int, mobile:String) in
                                                    return (code:chkCode, mobile: user ?? "")
            }
            .startWith((code: 0, mobile: ""))
            .shareReplay(1)
        
        let smsType:API.SendSMSType = self.isRegister ? .register : .resetPassword
        
        let allSecound = 60
        
        let requestCode = chkCodeButton
            .rx
            .tap
            .throttle(0.3,
                      scheduler: MainScheduler.instance)
            .withLatestFrom(collection)
            .map {
                [weak self] (collection) -> Observable<Int> in
                
                return smsType
                    .sendSMS(mobile: collection.mobile, areaCode: "\(collection.code)")
                    .flatMapLatest({ (postItem) -> Observable<Int> in
                        switch postItem {
                        case .failed(let error):
                            self?.showToast(text: error)
                            
                            return Observable.just(0)
                        case .requesting:
                            return Observable.just(-1)
                        case .success:
                            return Observable<Int>.timer(0.0,
                                                         period: 1.0,
                                                         scheduler: MainScheduler.instance)
                                .take(allSecound + 1)
                                .map{ allSecound - $0 }
                        }
                    }).shareReplay(1)
            }.shareReplay(1)
        
        let inputChangeCode = userInput
            .rx
            .text
            .map({ $0 ?? "" })
            .distinctUntilChanged()
            .map({ _ in return Observable.just(0) })
        
        let chkCode = Observable
            .of(requestCode, inputChangeCode)
            .merge()
            .switchLatest()
            .startWith(0)
        
        chkCode
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { (chkCode) in
                if chkCode < 0 {
                    chkCodeButton.setAttributedTitle(NSAttributedString(string: "发送中",
                                                                        font: UIConfig.generalFont(10),
                                                                        textColor: UIConfig.generalColor.white),
                                                     for: .normal)
                } else if chkCode > 0 {
                    chkCodeButton.setAttributedTitle(NSAttributedString(string: "\(chkCode)",
                        font: UIConfig.generalFont(10),
                        textColor: UIConfig.generalColor.white),
                                                     for: .normal)
                } else {
                    chkCodeButton.setAttributedTitle(NSAttributedString(string: "发送验证码",
                                                                        font: UIConfig.generalFont(10),
                                                                        textColor: UIConfig.generalColor.white),
                                                     for: .normal)
                }
                
            }.addDisposableTo(disposeBag)
        
        Observable.combineLatest(chkCode,
                                 userInput.rx.text.map{ $0 ?? "" }) { (code, mobile) -> Bool in
                                    return code == 0 && mobile.isTel()
            }
            .bindNext(chkCodeButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        /// buttonEnable
        let validInfo = Observable.combineLatest(userInput.rx.text,
                                                 chkInput.rx.text,
                                                 areacode.asObservable()) {
                                                    (user, chk, code) -> (user:String, checkCode:String, code:Int) in
                                                    return (user:user ?? "", checkCode:chk ?? "", code:code)
            }
            .shareReplay(1)
        
        let buttonEnable = validInfo.map { (info) -> Bool in
            return info.user.isTel() && info.checkCode.length == 6 && info.checkCode.isNumber()
            }
            .startWith(false)
        
        buttonEnable
            .bind(to: sbmButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        /// login action
        sbmButton.rx.tap.withLatestFrom(validInfo).bind {
            [weak self] (info) in
            
            guard let strongSelf = self else {
                return
            }
            
            let toast = strongSelf.showLoading()
            
            API
                .UserInfo
                .login(areaCode: info.code,
                               mobile: info.user,
                               passWord: info.checkCode,
                               imei: OpenUDID.value()!,
                               loginWay: 1)
                .responseModel({ (user:API.UserInfo) in
                    UserManager.shareInstance.login(with: user)
                    
                    ez.runThisInMainThread {
                        toast.dismiss(animated: true,
                                      completion: {
                                        if user.infoFlag <= 0 {
                                            self?.navigationController?.pushViewController(SupplementInfoVC(),
                                                                                           animated: true)
                                            
                                            return
                                        }
                                        
                                        if let loginVC = strongSelf.navigationController as? LoginVC {
                                            loginVC.loginSuccess()
                                        }
                        })
                    }
                },
                               error: { (error) in
                                ez.runThisInMainThread {
                                    toast.displayLabel(text: error.localizedDescription)
                                }
                })
        
//            smsType.validate(mobile: info.user,
//                             areaCode: "\(info.code)",
//                smsCode: info.checkCode)
//                .responseNoModel({
//                    
//                    ez.runThisInMainThread {
//                        toast.dismiss(animated: true,
//                                      completion: {
//                                        let pswSettingVC = PasswordSettingVC()
//                                        
//                                        pswSettingVC.mobile = info.user
//                                        pswSettingVC.areaCode = "\(info.code)"
//                                        
//                                        strongSelf.navigationController?.pushViewController(pswSettingVC,
//                                                                                            animated: true)
//                        })
//                    }
//                },
//                                 error: { (error) in
//                                    ez.runThisInMainThread {
//                                        toast.displayLabel(text: error.localizedDescription)
//                                    }
//                })
            }
            .addDisposableTo(disposeBag)
    }
    
}

extension MobileLoginVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            let tag = textField.tag
            
            if textField.returnKeyType == .next {
                if let nextTextField = self.view.viewWithTag(tag + 1) as? UITextField {
                    nextTextField.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.resignFirstResponder()
            }
            
            return false
        }
        
        return true
    }
}

// MARK: - End Edit Gesture
extension MobileLoginVC {
    func switchLoginFromPassword(sender:Any) {
        self.navigationController?.pushViewController(LoginInputVC(),
                                                      animated: true)
    }
    
    func backgroudTap(gesture:UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func buttonTap(sender:Any?) {
        self.view.endEditing(true)
    }
    
    func showAgreeMent(sender:Any?) {
        guard let url = URL(string: URLConfig.prefix + "flyapp/login.html#!/agreement.html") else {
            return
        }
        
        let webViewController = WKWebViewController(url: url)
        
        self.navigationController?.pushViewController(webViewController,
                                                      animated: true)
    }
}
