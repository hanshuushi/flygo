//
//  PasswordSettingVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/7.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import EZSwiftExtensions

class PasswordSettingVC: UIViewController {
    
    let disposeBag = DisposeBag()
    
    var mobile:String = ""
    
    var areaCode:String = ""
    
    var successHandler:((Void) -> Void)?
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let vcs = self.navigationController?.viewControllers {
            let newVcs = vcs.filter({ type(of: $0) != MobileSMSInputVC.classForCoder() })
            
            if newVcs.count != vcs.count {
                self.navigationController?.setViewControllers(newVcs,
                                                              animated: false)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "设置密码"
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        // add gesture
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(LoginInputVC.backgroudTap(gesture:)))
        
        self.view.addGestureRecognizer(tapGesture)
        
        let swipeGesture = UISwipeGestureRecognizer(target: self,
                                                    action: #selector(LoginInputVC.backgroudTap(gesture:)))
        
        swipeGesture.direction = [.up, .down]
        
        self.view.addGestureRecognizer(swipeGesture)
        
        // 密码
        let pswLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                          textColor: UIConfig.generalColor.selected)
        
        pswLabel.text = "密码"
        
        let pswInput = UITextField()
        
        pswInput.font = UIConfig.generalFont(15)
        pswInput.textColor = UIConfig.generalColor.selected
        pswInput.attributedPlaceholder = NSAttributedString(string: "请输入6-16位密码",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.whiteGray)
        pswInput.backgroundColor = UIConfig.generalColor.white
        pswInput.returnKeyType = .next
        pswInput.isSecureTextEntry = true
        pswInput.delegate = self
        pswInput.tag = 1
        
        let pswBottomLine = ViewFactory.line()
        
        // 重复密码
        let rptLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                         textColor: UIConfig.generalColor.selected)
        
        rptLabel.text = "重复密码"
        
        let rptInput = UITextField()
        
        rptInput.font = UIConfig.generalFont(15)
        rptInput.textColor = UIConfig.generalColor.selected
        rptInput.attributedPlaceholder = NSAttributedString(string: "请重复输入密码",
                                                            font: UIConfig.generalFont(15),
                                                            textColor: UIConfig.generalColor.whiteGray)
        rptInput.backgroundColor = UIConfig.generalColor.white
        rptInput.returnKeyType = .done
        rptInput.isSecureTextEntry = true
        rptInput.delegate = self
        rptInput.tag = 2
        
        let rptBottomLine = ViewFactory.line()
        
        // 提交
        let sbmButton = ViewFactory.redButton(title: "确认")
        
        sbmButton.layer.cornerRadius = 17.5
        sbmButton.layer.masksToBounds = true
        sbmButton.addTarget(self,
                            action: #selector(PasswordSettingVC.buttonTap(sender:)),
                            for: UIControlEvents.touchUpInside)
        
        // Layout
        self.view.addSubviews(pswLabel, pswInput, pswBottomLine, rptLabel, rptInput, rptBottomLine, sbmButton)
        
        rptBottomLine.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(63)
            maker.right.equalTo(self.view).offset(-63)
            maker.height.equalTo(0.5)
            maker.centerY.equalTo(self.view).offset(6)
        }
        
        rptInput.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(rptBottomLine.snp.top).offset(-12)
            maker.left.right.equalTo(rptBottomLine)
        }
        
        rptLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(rptInput)
            maker.bottom.equalTo(rptInput.snp.top).offset(-13)
        }
        
        pswBottomLine.snp.makeConstraints { (maker) in
            maker.left.right.height.equalTo(rptBottomLine)
            maker.bottom.equalTo(rptLabel.snp.top).offset(-22)
        }
        
        pswInput.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(pswBottomLine.snp.top).offset(-11)
            maker.left.right.equalTo(pswBottomLine)
        }
        
        pswLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(pswInput)
            maker.bottom.equalTo(pswInput.snp.top).offset(-17)
        }
        
        sbmButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.left.right.equalTo(rptBottomLine)
            maker.height.equalTo(35)
            maker.top.equalTo(rptBottomLine.snp.bottom).offset(24)
        }
        
        // bind all observer
        //self.addFakeShadow()
        
        /// buttonEnable
        let buttonEnable = Observable.combineLatest(rptInput.rx.text,
                                                    pswInput.rx.text) { (rpt, psw) -> Bool in
                                                        guard let psw1 = psw, let psw2 = rpt, psw1.length >= 6, psw1.length <= 16, psw1 == psw2 else {
                                                            return false
                                                        }
                                                        
                                                        return true
            }
            .startWith(false)
        
        buttonEnable
            .bind(to: sbmButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        /// login action
        sbmButton.rx.tap.withLatestFrom(pswInput.rx.text).bind {
            [weak self] (password) in
            
            guard let strongSelf = self else {
                return
            }
            
            if !(password ?? "").isPassword() {
                strongSelf.showToast(text: "密码仅可以为数字，字母和下划线，区分大小写")
                
                pswInput.becomeFirstResponder()
                
                return
            }
            
            
            let toast = strongSelf.showLoading()
            
            let errorHandler:(HttpError) -> Void = {
                (error) in
                ez.runThisInMainThread {
                    toast.displayLabel(text: error.localizedDescription)
                }
            }
            
            API
                .UserInfo
                .settingPassword(areaCode: strongSelf.areaCode,
                                 mobile: strongSelf.mobile,
                                 passWord: password ?? "")
                .responseNoModel({
                    API
                        .UserInfo
                        .login(areaCode: strongSelf.areaCode.toInt() ?? 86,
                               mobile: strongSelf.mobile,
                               passWord: password ?? "",
                               imei: OpenUDID.value() ?? "")
                        .responseModel({
                            (userInfo:API.UserInfo) in
                            
                            UserManager.shareInstance.login(with: userInfo)
                            
                            ez.runThisInMainThread {
                                toast.dismiss(animated: true,
                                              completion: {
                                                
                                                CartVM.refreshNotification()
                                                
                                                if let handler = strongSelf.successHandler {
                                                    handler()
                                                } else if let loginVC = strongSelf.navigationController as? LoginVC {
                                                    loginVC.loginSuccess()
                                                } else {
                                                    let _ = strongSelf.navigationController?.popViewController(animated: true)
                                                }
                                })
                            }
                            
                        }, error: errorHandler)
                },
                                 error: errorHandler)
            }.addDisposableTo(disposeBag)
    }
}

extension PasswordSettingVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            let tag = textField.tag
            
            if textField.returnKeyType == .next {
                if let nextTextField = self.view.viewWithTag(tag + 1) as? UITextField {
                    nextTextField.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.resignFirstResponder()
            }
            
            return false
        }
        
        return true
    }
}

// MARK: - End Edit Gesture
extension PasswordSettingVC {
    func backgroudTap(gesture:UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func buttonTap(sender:Any?) {
        self.view.endEditing(true)
    }
}
