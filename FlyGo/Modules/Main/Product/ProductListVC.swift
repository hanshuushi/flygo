//
//  ProductListVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ProductListVC: UIViewController, PresenterType {
    
    var bindDisposeBag: DisposeBag?
    
    let viewModel:ProductListVM
    
    let tableView = UITableView(frame: .zero,
                                style: .plain)
    
    let dropDownMenu:DropDownMenu
    
    init(keyword:String) {
        viewModel = ProductListVM(keyword: keyword)
        
        dropDownMenu = DropDownMenu(contentView: tableView,
                                    style: CommonDropDownMenuStyle())
        
        super.init(nibName: nil,
                   bundle: nil)
        
        self.title = keyword
    }
    
    init(brandId:String, brandName:String) {
        viewModel = ProductListVM(brandId: brandId)
        
        dropDownMenu = DropDownMenu(contentView: tableView,
                                    style: CommonDropDownMenuStyle())
        
        super.init(nibName: nil,
                   bundle: nil)
        
        self.title = brandName
    }
    
    init(type:ProductTypeItem) {
        viewModel = ProductListVM(productType: type)
        
        dropDownMenu = DropDownMenu(contentView: tableView,
                                    style: CommonDropDownMenuStyle())
        
        super.init(nibName: nil,
                   bundle: nil)
        
        self.title = type.name
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        /// table view
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.rowHeight = 135
        tableView.registerCell(cellClass: ProductListCell.self)
        tableView.setInset(top: 64 + 43.5, bottom: 0)
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        
        /// drop down
        self.view.addSubview(dropDownMenu)
        
        dropDownMenu.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsetsMake(64, 0, 0, 0))
        }
        
        /// force touch
        if #available(iOS 9.0, *) {
            if self.traitCollection.forceTouchCapability == .available {
                self.registerForPreviewing(with: self,
                                           sourceView: tableView)
            }
        } else {
            // Fallback on earlier versions
        }
        
        bind()
    }
    
    var productList:[ProductItem] = []
    
    var brandItem:ProductBrandItem?
}


// MARK: - Force Touch Peek & Pop
@available(iOS 9.0, *)
extension ProductListVC: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = self.tableView.indexPathForRow(at: location),
            let selectedFrame = self.tableView.cellForRow(at: indexPath)?.frame else {
            return nil
        }
        
        previewingContext.sourceRect = selectedFrame
        
        let item = productList[indexPath.row]
        
        let vm = ProductDetailVM(productId: item.recId)
        
        let vc = ProductDetailVC(viewModel: vm, showCart: true, canBuy: true)
        
        vc.peekViewController = self
        
        return vc
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        self.navigationController?.pushViewController(viewControllerToCommit, animated: true)
    }
}

class ProductListCell: UITableViewCell {
    
    let coverImageView = UIImageView()
    
    let nameLabel = UILabel()
    
    let oriLabel = UILabel()
    
    let rmbLabel = UILabel()
    
    let priceLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        // add cover
        self.contentView.addSubview(coverImageView)
        
        coverImageView.contentMode = .scaleAspectFit
        coverImageView.backgroundColor = UIConfig.generalColor.white
        coverImageView.clipsToBounds = true
        coverImageView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.contentView).offset(15)
            maker.centerY.equalTo(self.contentView)
            maker.size.equalTo(CGSize(width:115, height:95))
        }
        
        // add title
        self.contentView.addSubview(nameLabel)
        
        nameLabel.font = UIConfig.generalFont(13)
        nameLabel.numberOfLines = 2
        nameLabel.textColor = UIConfig.generalColor.selected
        nameLabel.backgroundColor = coverImageView.backgroundColor
        nameLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(coverImageView.snp.right).offset(15)
            maker.top.equalTo(coverImageView)
            maker.right.equalTo(self.contentView).offset(-15)
        }
        
        // add price
        self.contentView.addSubview(priceLabel)
        
        priceLabel.font = UIConfig.generalFont(13)
        priceLabel.textColor = UIConfig.generalColor.red
        priceLabel.backgroundColor = coverImageView.backgroundColor
        priceLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(nameLabel)
            maker.bottom.equalTo(coverImageView)
        }
    }
    
    var item:ProductItem? {
        didSet {
            coverImageView.kf.setImage(with:item?.imageUrl)
            
            nameLabel.text = item?.name
            
            /// price
            priceLabel.attributedText = getPriceAttributedString(rmb: item?.rmb ?? Currency(unit: "￥",
                                                                                            value: 0),
                                                                 ori: item?.price ?? Currency(unit: "$",
                                                                                              value: 0))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension ProductListVC {
    class BrandHeaderView: UITableViewHeaderFooterView {
        
        let brandAvatar:UIImageView
        
        let brandLabel:UILabel
        
        override init(reuseIdentifier: String?) {
            brandLabel = ViewFactory.label(font: UIConfig.arialBoldFont(20),
                                           textColor: UIConfig.generalColor.titleColor,
                                           backgroudColor:.clear)
            
            let coverImageView = UIImageView(named: "background_without-picture")
            
            coverImageView.contentMode = .scaleAspectFill
            
            let avatarBoardView = UIView(frame: CGRect(x: 15,
                                                       y: 30,
                                                       width: 90,
                                                       height: 90))
            avatarBoardView.backgroundColor = UIConfig.generalColor.white
            avatarBoardView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
            avatarBoardView.layer.borderWidth = 1
            avatarBoardView.layer.masksToBounds = true
            
            brandAvatar = UIImageView(frame: CGRect(x: 5,
                                                    y: 5,
                                                    width: 80,
                                                    height: 80))
            brandAvatar.backgroundColor = UIConfig.generalColor.white
            brandAvatar.contentMode = .scaleAspectFit
            
            avatarBoardView.addSubview(brandAvatar)
            
            brandAvatar.snp.makeConstraints { (maker) in
                maker.edges.equalTo(avatarBoardView).inset(UIEdgeInsetsMake(5, 5, 5, 5))
            }
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            self.backgroundView = ViewFactory.view(color: .clear)
            
            self.addSubviews(coverImageView, avatarBoardView, brandLabel)
            
            coverImageView.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0, 0, 20, 0))
            }
            
            avatarBoardView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(30)
                maker.width.height.equalTo(90)
            }
            
            brandLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarBoardView.snp.right).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-30)
            }
        }
        
        func set(brandItem: ProductBrandItem) {
            brandAvatar.kf.setImage(with: brandItem.imageUrl)
            
            brandLabel.text = brandItem.name
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ProductListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as ProductListCell
        
        cell.item = productList[indexPath.row]
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return brandItem == nil ? 0 : 120
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return brandItem == nil ? 0 : 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let item = brandItem else {
            return nil
        }
        
        let brandView:BrandHeaderView = tableView.dequeueReusableView()
        
        brandView.set(brandItem: item)
        
        return brandView
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = productList[indexPath.row]
        
        let vm = ProductDetailVM(productId: item.recId)
        
        let vc = ProductDetailVC(viewModel: vm, showCart: true, canBuy: true)
        
        self.navigationController?.pushViewController(vc,
                                                      animated: true)
    }
}

