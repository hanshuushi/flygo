//
//  ProductDetailVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import EZSwiftExtensions

struct ProductDetailStandar {
    var productRecId:String
    
    var name:String
    
    var productId:String
    
    init(model:API.ProductDetail.Standard) {
        productId = model.productId
        
        productRecId = model.productRecid
        
        name = model.productRecStandard
    }
}

struct ProductDetailStore {
    var storeId:String
    
    var rmb:Currency
    
    var price:Currency
    
    var storeName:String
    
    var isValid:Bool
    
    init(model:API.ProductDetail.Stores) {
        storeId = model.belongStoreId
        
        rmb = model.rmb
        
        price = model.sellPrice
        
        storeName = model.freeStoreName
        
        isValid = model.state == 1
    }
}

struct ProductDetailComment {
    
    var id:String
    
    var commentCount:Int
    
    typealias CommentItem = (proposeValue:Float, avatar:URL?, nickName:String, star:Int, time:String, standar:String, price:Currency, content:String, images:[URL])
    
    var item:CommentItem?
    
    init(model:API.ProductDetail.BestComment) {
        id           = model.productId
        
        commentCount = model.commentsNumber
        
        item         = model.comment.map({ (comment) -> CommentItem in
            return (proposeValue:model.buyIndex,
                    avatar:comment.userAvatar,
                    nickName:comment.nickname,
                    star:comment.buyScore,
                    time:comment.registerTime?.generalFormartString ?? "",
                    standar:comment.productRecStandard,
                    price:Currency(unit: "￥",
                                   value: comment.buyPrice),
                    content:comment.commentsDesc,
                    images: comment.commentsPic
                        .filter({ $0.pic != nil })
                        .sorted(by: { (left, right) -> Bool in
                            return left.picSeq > right.picSeq
                        })
                        .map({ $0.pic! }))
        })
    }
}

struct ProductDetailItem {
    
    let recId:String
    
    var pics:[URL?]
    
    var title:String
    
    var typeName:String

    var brandName:String
    
    var brandId:String

    var price:Currency
    
    var inStock:Bool
    
    var displayPrice:(price:Currency, rmb:Currency) {
        
        var store:ProductDetailStore?
        
        for one in stores {
            if one.rmb.value > (store?.rmb.value ?? rmb.value) {
                store = one
            }
        }

        guard let currentStore = store else {
            return (price:price, rmb:rmb)
        }
        
        return (price:currentStore.price, rmb:currentStore.rmb)
    }

    var deliver:Float
    
    var arriveTime:String
    
    var rmb:Currency

    var rate:Float
    
    var standar:String {
        var array = [String]()
        
        for index in selectedStandars {
            array.append(standars[index].name)
        }
        
        return array.joined(separator: ",")
    }
    
    var standars:[ProductDetailStandar]
    
    var selectedStandars:[Int]
    
    var belongStoreId:String {
        var array = [String]()
        
        for one in stores {
            array.append(one.storeId)
        }
        
        return array.joined(separator: ",")
    }
    
    var stores:[ProductDetailStore]
    
//    var selectedStores:[Int]
    
    var info:[String:String]
    
    var detailPics:[URL?]
    
    var comment:ProductDetailComment?
    
    init?(model:API.ProductDetail) {
        pics = model.pics.map({ $0.pic })
        
        if model.detail.count <= 0 {
            return nil
        }
        
        let detail = model.detail[0]
        
        recId = detail.productRecid
        
        title = detail.productName
        
        typeName = detail.productTypeName
        
        brandName = detail.displayName
        
        brandId = detail.brandId
        
        inStock = model.productStock == 1
        
//        if let firstStore = model.stores.first {
//            price = firstStore.sellPrice
//            
//            rmb = firstStore.rmb
//        } else {
//            price = Currency(unit: "$", value: 0)
//            
//            rmb = Currency(unit: "￥", value: 0)
//        }
        
        deliver = model.postage
        
        arriveTime = model.estimatedArrivalTime
        
        price = Currency(unit: "￥", value: model.sellPrice)
        
        rmb = Currency(unit: "￥", value: model.cnyPrice)
        
        rate = model.serviceRatio
        
        standars = model.standard.map({ ProductDetailStandar(model:$0) })
        
        selectedStandars = []
        
        for (index, one) in standars.enumerated() {
            if one.productRecId == recId {
                selectedStandars.append(index)
            }
        }
        
        stores = model.stores.map({ ProductDetailStore(model:$0) })
        
//        selectedStores = []
        
        info = [String:String]()
        
        for one in model.paras {
            info[one.productParaName] = one.content.replacingOccurrences(of: "<br/>", with: "\n")
        }
        
        detailPics = model.productPics.count > 0 ? model.productPics.map({ $0.pic }) : pics
        
        comment = model.bestComment.map({ ProductDetailComment(model: $0) })
    }
}

class ProductDetailVM: ViewModel {
    
    let productId:String
    
    init(productId:String) {
        self.productId = productId
    }
    
    func bind(to view: ProductDetailVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        let dataset = TableViewDataSet(view.tableView, delegate: view, dataSource: view, finishStyle: .gray, refreshEnable: false)
        
        let request = API.ProductDetail.detail(recId: productId).shareReplay(1)
        
        bag.insert(dataset)
        
        let loadingView = view.tableView.dequeueReusableView() as ProductDetailVC.FullLoadingHeaderView
        
        dataset.loadingViewHandler = { loadingView }
        
        request
            .asRequestStatusObservable()
            .bind(to: dataset.status)
            .addDisposableTo(bag)
        
        let item = Variable<ProductDetailItem?>(nil)
        
        request.asModelObservable()
            .map({
                ProductDetailItem(model:$0)
            })
            .bind(to: item)
            .addDisposableTo(bag)
        
        item
            .asObservable()
            .map({ $0 == nil })
            .observeOn(MainScheduler.instance)
            .bind(to: dataset.isEmpty)
            .addDisposableTo(bag)
        
        item
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { [weak view] (item) in
                view?.item = item
                
                if let `item` = item {
                    AnalyzeManager.view(product: item)
                }
        }
            .addDisposableTo(bag)
        
        
        /// number
        let numberVariable = Variable(1)
        
        view.numberChangeEvent
            .withLatestFrom(numberVariable.asObservable()) { $0 + $1 }
            .bind(to: numberVariable)
            .addDisposableTo(bag)
        
        numberVariable
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { [weak view] (number) in
            view?.number = number
        }.addDisposableTo(bag)
        
        // switch
        let switchItem = SwitchItem(rootController: view)
        
        bag.insert(switchItem)
        
        let stardarSelected = view.stardarSelected.withLatestFrom(item.asObservable()) {
            (index, item) -> String? in
            guard let currentItem = item else {
                return nil
            }
            
            return currentItem.standars[index].productRecId
        }
            .filter{ $0 != nil }
            .map{ $0! }
        
        let switchRequest = stardarSelected
            .flatMapLatest{ API
                .ProductDetail
                .detail(recId: $0) }
            .shareReplay(1)
        
        switchRequest
            .map({ $0.postItem })
            .bind(to: switchItem)
            .addDisposableTo(bag)
        
        switchRequest
            .asModelObservable()
            .map({ ProductDetailItem(model: $0) })
            .bind(to: item)
            .addDisposableTo(bag)
        
//        view.storeSelected.withLatestFrom(item.asObservable()) { (array, item) -> ProductDetailItem? in
//            var currentItem = item
//            
//            currentItem?.selectedStores = array
//            
//            return currentItem
//        }.bind(to: item).addDisposableTo(bag)
        
        // add to cart
        let addCartControl = RequestItem(rootController: view,
                                         successTip: "已添加至购物车",
                                         successHandler: {
                                            CartVM.refreshNotification()
        })
        
        bag.insert(addCartControl)
        
        let addCartRequest = view
            .addCartEvent
            .flatMapLatest({ API.Cart.addToCart(belongStoreId: $0.belongStoreId,
                                                productRecId: $0.recId,
                                                productName: $0.title,
                                                orderNumber: numberVariable.value) })
            .shareReplay(1)
        
        addCartRequest
            .bind(to: addCartControl)
            .addDisposableTo(bag)
        
        // order
        view
            .addOrderEvent
            .withLatestFrom(numberVariable.asObservable()) {
                (productDetailItem, number) -> (item:ProductDetailItem, number:Int) in
                
                return (item:productDetailItem, number:number)
            }
            .flatMapLatest {
                (collection) -> Observable<[CartItem]> in
                
                return Observable.create({
                    [weak view]
                    (obs) -> Disposable in
                    guard let `view` = view else {
                        obs.onCompleted()
                        return Disposables.create()
                    }
                    
                    let toast = view.showLoading()
                    
                    let price = collection.item.rmb.value * Float(collection.number)
                    
                    let session = API.CartPrice.getPrice(of: price)
                    
                    session
                        .responseModel({ (model:API.CartPrice) in
                            ez.runThisInMainThread {
                                toast.dismiss(animated: true,
                                              completion: {
                                                let cartItem = CartItem(detailItem: collection.item,
                                                                        number: collection.number,
                                                                        model: model)
                                                
                                                obs.onNext([cartItem])
                                                obs.onCompleted()
                                })
                            }
                        },
                                       error: { (error) in
                                        ez.runThisInMainThread {
                                            toast.displayLabel(text: error.description)
                                        }
                        })
                    
                    return Disposables.create(session,
                                              Disposables.create {
                                                ez.runThisInMainThread {
                                                    toast.dismiss(animated: true,
                                                                  completion: nil)
                                                }
                    })
                })
            }
            .observeOn(MainScheduler.instance)
            .bind(onNext: {
                [weak view]
                (items) in
                let vm = CartConfirmVM(cartItems: items)
                
                let vc = CartConfirmVC(viewModel: vm)
                
                let _ = view?.navigationController?.pushViewController(vc,
                                                                       animated: true)
            })
            .addDisposableTo(bag)
        
//        view
//            .addOrderEvent
//            .withLatestFrom(numberVariable.asObservable()) { (item, number) -> (CartItem, Float) in
//                return (CartItem(detailItem: item,
//                                number: number), item.rate)
//            }
//            .map({ CartConfirmVC(viewModel:CartConfirmVM(cartItems: [$0.0])) })
//            .bind {
//                [weak view] (vc) in
//                let _ = view?.navigationController?.pushViewController(vc,
//                                                                       animated: true)
//        }.addDisposableTo(bag)
        
        /// brand
        view
            .brandSelected
            .withLatestFrom(item.asObservable())
            .filter({ $0 != nil })
            .map({ $0! })
            .bind {
                [weak view]
                (item) in
                
                let vc = ProductListVC(brandId: item.brandId, brandName: item.brandName)
                
                let _ = view?.navigationController?.pushViewController(vc,
                                                                       animated: true)
        }.addDisposableTo(bag)
        
        
        /// comment
        view
            .commentCell
            .toAllButton
            .rx
            .tap.withLatestFrom(item.asObservable().filter({ $0?.comment?.id != nil }).map({ $0!.comment!.id }))
            .bind {
                [weak view] (productId) in
                
                let vc = CommentVC(productId: productId)
                
                view?.navigationController?.pushViewController(vc,
                                                               animated: true)
        }
            .addDisposableTo(bag)
        
        return bag
    }
    
}
