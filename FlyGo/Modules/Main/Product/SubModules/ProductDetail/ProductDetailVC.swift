//
//  ProductDetailVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import EZSwiftExtensions
import ObjectMapper
import SnapKit
import ImageViewer
import Kingfisher

class ProductDetailVC: UIViewController, PresenterType {
    
    let viewModel:ProductDetailVM
    
    var bindDisposeBag: DisposeBag?
    
    let numberChangeEvent: PublishSubject<Int>
    
    let stardarSelected: PublishSubject<Int>
    
//    let storeSelected: PublishSubject<[Int]>
    
    let brandSelected: PublishSubject<Void>
    
    let addCartEvent: PublishSubject<ProductDetailItem>
    
    let addOrderEvent: PublishSubject<ProductDetailItem>
    
    let cartButton = ViewFactory.yellowButton(title: "加入购物车")
    
    let flygoButton = ViewFactory.redButton(title: "立即购买")
    
    let disableZone = ViewFactory.view(color: .clear)
    
    var fakeNavigationBar:FakeNavigationBar!
    
    init(viewModel:ProductDetailVM,
         showCart:Bool = true,
         canBuy:Bool = true) {
        self.viewModel = viewModel
        
        let numberChangeEvent = PublishSubject<Int>()
        
        self.numberChangeEvent = numberChangeEvent
        
        stardarSelected = PublishSubject()
        
//        storeSelected = PublishSubject()
        
        addCartEvent = PublishSubject()
        
        addOrderEvent = PublishSubject()
        
        brandSelected = PublishSubject()
        
        coverCell = CoverTableViewCell(contentMode: .scaleAspectFill)
        
        detailCell = DetailTableViewCell()
        
        standarCell = StandarTableViewCell()
        
//        storeCell = StoreTableViewCell()
        
        numberCell = NumberTableViewCell()
        numberCell.changeHandler = {
            (change:Int) -> Void in
            
            numberChangeEvent.onNext(change)
        }
        
        infoCell = InfoTableViewCell()
        
        picsCell = PicsTableViewCell()
        
        tipCell = TipTableViewCell()
        
        commentCell = CommentTableViewCell()
        
        self.tableView = UITableView(frame: .zero,
                                     style: .grouped)
        
        bottomView = ViewFactory
            .view(color: UIConfig.generalColor.white)
        
        let serverButton = ViewFactory.serverButton()
        
        bottomView.addSubviews(serverButton, cartButton, flygoButton, disableZone)
        bottomView.isHidden = !canBuy
        
        super.init(nibName: nil,
                   bundle: nil)
        
        
        disableZone.addTapGesture(action: {
            [weak self] (_) -> Void in
            
            self?.showToast(text: "当前规格暂时无货")
        })
        
        fakeNavigationBar = FakeNavigationBar(target: self, showCart: showCart)
//        detailCell.priceButton.addTarget(self,
//                                         action: #selector(ProductDetailVC.toPriceIntroduce),
//                                         for: UIControlEvents.touchUpInside)
        detailCell.subTitleButton.addTarget(self,
                                            action: #selector(ProductDetailVC.brandButtonPressed(sender:)),
                                            for: UIControlEvents.touchUpInside)
        
        flygoButton.addTarget(self,
                              action: #selector(ProductDetailVC.flygoButtonPressed(sender:)),
                              for: UIControlEvents.touchUpInside)
        
        
        cartButton.addTarget(self,
                             action: #selector(ProductDetailVC.cartButtonPressed(sender:)),
                             for: UIControlEvents.touchUpInside)
        
        serverButton.snp.makeConstraints { (maker) in
            maker.left.top.bottom.equalTo(bottomView)
            maker.width.equalTo(105)
        }
        
        cartButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(serverButton.snp.right)
            maker.bottom.top.equalTo(bottomView)
        }
        
        flygoButton.snp.makeConstraints { (maker) in
            maker.width.equalTo(cartButton)
            maker.left.equalTo(cartButton.snp.right).offset(-0.5)
            maker.bottom.top.right.equalTo(bottomView)
        }
        
        disableZone.snp.makeConstraints { (maker) in
            maker.top.bottom.equalTo(bottomView)
            maker.left.equalTo(cartButton)
            maker.right.equalTo(flygoButton)
        }
        
        picsCell.listView.delegate = self
        
        standarCell.collectionView.delegate = self
        
        commentCell.viewController = self
        
//        storeCell.collectionView.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        /// insert navigation
        fakeNavigationBar.darkValue = 0
        
        self.view.addSubview(fakeNavigationBar)
        
        fakeNavigationBar.snp.makeConstraints { (maker) in
            maker.left.top.right.equalTo(self.view)
            maker.height.equalTo(64)
        }
        
        /// insert scroll view
        self.view.insertSubview(tableView,
                                belowSubview: fakeNavigationBar)
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        tableView.separatorStyle = .none
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.allowsSelection = false
        tableView.setInset(top: 0, bottom: bottomView.isHidden ? 0 : 49)
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.registerView(viewClass: FullLoadingHeaderView.self)
        
        /// bottom
        self.view.insertSubview(bottomView,
                                belowSubview: fakeNavigationBar)
        
        bottomView.snp.makeConstraints { (maker) in
            maker.bottom.left.right.equalTo(self.view)
            maker.height.equalTo(49)
        }
        
        bind()
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.reloadData()
    }
    
    var item:ProductDetailItem? = nil {
        didSet {
            if let currentItem = item {
                coverCell.urlList = currentItem.pics.takeMax(5)
                
                detailCell.set(item: currentItem)
                
                standarCell.standars = currentItem.standars
                standarCell.collectionView.setSelected(selectedList: currentItem.selectedStandars)
                standarCell.inStock = currentItem.inStock
                
                if standarCell.inStock {
                    flygoButton.isEnabled = true
                    
                    cartButton.isEnabled = true
                    
                    disableZone.isUserInteractionEnabled = false
                } else {
                    
                    flygoButton.isEnabled = false
                    
                    cartButton.isEnabled = false
                    
                    disableZone.isUserInteractionEnabled = true
                }
                
//                storeCell.disable = false
//                storeCell.stores = currentItem.stores
//                storeCell.collectionView.setSelected(selectedList: currentItem.selectedStores)
                
                infoCell.set(info: currentItem.info)
                
                picsCell.urls = currentItem.detailPics
                
                commentCell.item = currentItem.comment
                
                self.tableView.reloadData()
            }
//            else {
//                storeCell.disable = true
//                storeCell.collectionView.reloadData()
//            }
            
//            self.tableView.reloadData()
        }
    }
    
    var number:Int = 1 {
        didSet {
            numberCell.number = number
        }
    }
    
    let tableView:UITableView
    
    let coverCell:CoverTableViewCell
    
    let detailCell:DetailTableViewCell
    
    let standarCell:StandarTableViewCell
    
//    let storeCell:StoreTableViewCell
    
    let numberCell:NumberTableViewCell

    let infoCell:InfoTableViewCell
    
    let picsCell:PicsTableViewCell
    
    let tipCell:TipTableViewCell
    
    let commentCell:CommentTableViewCell
    
    let bottomView:UIView
    
    static let coverHeight = UIScreen.main.bounds.width * 4 / 5
    
    weak var peekViewController:UIViewController?
}


// MARK: - Force Touch Actions
@available(iOS 9.0, *)
extension ProductDetailVC {
    override var previewActionItems: [UIPreviewActionItem] {
        get {
            var items = super.previewActionItems
            
            let share = UIPreviewAction(title: "分享",
                                        style: .default) { [weak self]
                                            (action, previewViewController) in
                                            
                                            guard let strongSelf = self,
                                                let peekViewController = strongSelf.peekViewController  else {
                                                return
                                            }
                                            
                                            ProductDetailVC.share(from: strongSelf.item,
                                                                  on: peekViewController,
                                                                  sender:strongSelf.view)
            }
            
            items.append(share)
            
            return items
        }
    }
}

extension ProductDetailVC {
    class FullLoadingHeaderView: UITableViewHeaderFooterView {
        override init(reuseIdentifier: String?) {
            super.init(reuseIdentifier: reuseIdentifier)
            
            let loadingView = ActivityIndicatorView()
            
            self.contentView.addSubview(loadingView)
            
            loadingView.snp.makeConstraints { (maker) in
                maker.center.equalTo(self.contentView)
//                maker.top.equalTo(self.contentView).offset(20)
//                maker.bottom.equalTo(self.contentView).offset(-20)
            }
            
            self.contentView.snp.makeConstraints { (maker) in
                maker.size.equalTo(UIScreen.main.bounds.size)
            }
            
            loadingView.play()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ProductDetailVC {
    
    func brandButtonPressed(sender:UIButton) {
        brandSelected.onNext()
    }
    
    static func share(from item:ProductDetailItem?, on viewController:UIViewController, sender:UIView) {
        guard let currentItem = item else {
            viewController.showToast(text: "数据正在请求中...")
            
            return
        }
        
        WechatManager.shareInstance.showShareProductActionSheet(on: viewController,
                                                                productItem: (recId:currentItem.recId,
                                                                              picURL:currentItem.pics.filter{ $0 != nil }.map{ $0! }.first,
                                                                              productName:currentItem.title),
                                                                sender:sender)
    }
    
    func shareButtonPressed(sender:UIButton) {
        self.dismiss(animated: true) {
            ProductDetailVC.share(from: self.item, on: self, sender:self.fakeNavigationBar.darkMoreBarButton)
        }
    }
    
    func cartButtonPressed(sender:UIButton) {
        guard let currentItem = item else {
            self.showToast(text: "数据正在请求中...")
            
            return
        }
        
        if !standarCell.inStock {
            self.showToast(text: "当前规格暂时无货")
            
            return
        }
//        if currentItem.selectedStores.count <= 0 || currentItem.selectedStandars.count <= 0 {
//            self.showToast(text: "请选择购买渠道")
//            
//            return
//        }
//        
        print("cartButtonPressed In Detail")
        
        LoginVC.doActionIfNeedLogin {
            self.addCartEvent.onNext(currentItem)
        }
    }
    
    func flygoButtonPressed(sender:UIButton) {
        guard let currentItem = item else {
            self.showToast(text: "数据正在请求中...")
            
            return
        }
        
        if !standarCell.inStock {
            self.showToast(text: "当前规格暂时无货")
            
            return
        }
//        if currentItem.selectedStores.count <= 0 || currentItem.selectedStandars.count <= 0 {
//            self.showToast(text: "请选择购买渠道")
//            
//            return
//        }
        
        print("flygoButtonPressed In Detail")
        
        LoginVC.doActionIfNeedLogin {
            self.addOrderEvent.onNext(currentItem)
        }
    }
}

extension ProductDetailVC: SelectionButtonCollectionViewDelegate {
    func collectionView(_ collectionView: SelectionButtonCollectionView, didSelectedIndexs: [Int]) {
        if collectionView == standarCell.collectionView {
            stardarSelected.onNext(didSelectedIndexs[0])
        }
//        else if collectionView == storeCell.collectionView {
//            storeSelected.onNext(didSelectedIndexs)
//        }
    }
}

extension ProductDetailVC: CoverListViewDelegate {
    func listViewChangeSize(_ listView: CoverListView) {
        self.tableView.reloadData()
    }
    
    func listView(_ listView: CoverListView, didClickFromButtonIndex index: Int) {
        let galleryVC = GalleryViewController(startIndex: index,
                                              itemsDataSource: self,
                                              displacedViewsDataSource: self,
                                              configuration: ProductDetailVC.galleryConfiguration())
        
        galleryVC.headerView = nil
        
        self.present(galleryVC, animated: false, completion: nil)
    }
}

extension UIButton: DisplaceableView {
    public var image: UIImage? {
        return self.imageView?.image
    }
}

extension ProductDetailVC: GalleryDisplacedViewsDataSource {
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        return self.picsCell.listView.buttons[index]
    }
}

extension ProductDetailVC: GalleryItemsDataSource {
    func itemCount() -> Int {
        return self.picsCell.urls.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let url = self.picsCell.urls[index]
        
        return .image(fetchImageBlock: { (completion) in
            
            if let imageUrl = url {
                KingfisherManager.shared.retrieveImage(with: imageUrl,
                                                       options: nil,
                                                       progressBlock: nil,
                                                       completionHandler: { (image, _, _, _) in
                                                        completion(image)
                })
            }
        })
    }
    
    static func galleryConfiguration() -> GalleryConfiguration {
        return [
            GalleryConfigurationItem.deleteButtonMode(.none),
            GalleryConfigurationItem.closeButtonMode(.custom({
                () -> UIButton in
                
                let closeButton = UIButton(type: .custom)
                
                closeButton.setAttributedTitle(NSAttributedString(string: "关闭",
                                                                  font: UIConfig.generalFont(15),
                                                                  textColor: UIConfig.generalColor.white),
                                               for: .normal)
                closeButton.contentVerticalAlignment = .top
                closeButton.sizeToFit()
                
                return closeButton
            }())),
            GalleryConfigurationItem.closeLayout(ButtonLayout.pinLeft(9, 15)),
            GalleryConfigurationItem.thumbnailsButtonMode(.custom({
                () -> UIButton in
                
                let allButton = UIButton(type: .custom)
                
                allButton.setAttributedTitle(NSAttributedString(string: "全部",
                                                                  font: UIConfig.generalFont(15),
                                                                  textColor: UIConfig.generalColor.white),
                                               for: .normal)
                allButton.contentVerticalAlignment = .top
                allButton.sizeToFit()
                
                return allButton
                }())),
            GalleryConfigurationItem.thumbnailsLayout(ButtonLayout.pinRight(9, 15)),
            GalleryConfigurationItem.pagingMode(.standard),
            GalleryConfigurationItem.presentationStyle(.displacement),
            GalleryConfigurationItem.hideDecorationViewsOnLaunch(false),
            
            GalleryConfigurationItem.overlayColor(UIColor(white: 0.035, alpha: 1)),
            GalleryConfigurationItem.overlayColorOpacity(1),
            GalleryConfigurationItem.overlayBlurOpacity(1),
            GalleryConfigurationItem.overlayBlurStyle(UIBlurEffectStyle.light),
            
            GalleryConfigurationItem.maximumZoomScale(8),
            GalleryConfigurationItem.swipeToDismissThresholdVelocity(500),
            
            GalleryConfigurationItem.doubleTapToZoomDuration(0.15),
            
            GalleryConfigurationItem.blurPresentDuration(0.5),
            GalleryConfigurationItem.blurPresentDelay(0),
            GalleryConfigurationItem.colorPresentDuration(0.25),
            GalleryConfigurationItem.colorPresentDelay(0),
            
            GalleryConfigurationItem.blurDismissDuration(0.1),
            GalleryConfigurationItem.blurDismissDelay(0.4),
            GalleryConfigurationItem.colorDismissDuration(0.45),
            GalleryConfigurationItem.colorDismissDelay(0),
            
            GalleryConfigurationItem.itemFadeDuration(0.3),
            GalleryConfigurationItem.decorationViewsFadeDuration(0.15),
            GalleryConfigurationItem.rotationDuration(0.15),
            
            GalleryConfigurationItem.displacementDuration(0.55),
            GalleryConfigurationItem.reverseDisplacementDuration(0.25),
            GalleryConfigurationItem.displacementTransitionStyle(.springBounce(0.7)),
            GalleryConfigurationItem.displacementTimingCurve(.linear),
            
            GalleryConfigurationItem.statusBarHidden(true),
            GalleryConfigurationItem.displacementKeepOriginalInPlace(false),
            GalleryConfigurationItem.displacementInsetMargin(50)
        ]
    }
}

extension ProductDetailVC: UITableViewDataSource, UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        coverCell.scrollInTableView(scrollView)
        
        let coverHeight = ProductDetailVC.coverHeight
        
        let distance = coverHeight - 44
        
        let offsetY = scrollView.contentOffset.y
        
        fakeNavigationBar.darkValue = offsetY / distance
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 2:
            return 2
        case 1,4,3,5:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return coverCell
            case 1:
                return detailCell
            default:
                fatalError()
            }
        case 1:
            return tipCell
        case 2:
            switch indexPath.row {
            case 0:
                return standarCell
            case 1:
                return numberCell
            default:
                fatalError()
            }
        case 3:
            return commentCell
        case 4:
            return infoCell
        case 5:
            return picsCell
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return ProductDetailVC.coverHeight
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return ProductDetailVC.coverHeight
            case 1:
                return 146.5
            default:
                fatalError()
            }
        case 1:
            return 123.5
        case 2:
            switch indexPath.row {
            case 0:
                return 80
            case 1:
                return 94
            case 2:
                return 92
            default:
                fatalError()
            }
        case 3:
            return 210.5
        case 4:
            return 90
        case 5:
            return 50 + (UIScreen.main.bounds.w / 2.0 * CGFloat(picsCell.urls.count))
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == 5 {
            return CGFloat.leastNormalMagnitude
        }
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}

extension ProductDetailVC {
    class TipTableViewCell: UITableViewCell {
        init() {
            
            super.init(style: .default,
                       reuseIdentifier: "TipTableViewCell")
            
            let titleLabel = ViewFactory.generalLabel(generalSize: 13,
                                                      textColor: UIConfig.generalColor.titleColor)
            
            self.contentView.addSubview(titleLabel)
            
            titleLabel.text = "购买流程"
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(15)
            }
            
            let displayZone = ViewFactory.view(color: UIConfig.generalColor.white)
            
            self.contentView.addSubview(displayZone)
            
            displayZone.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel.snp.bottom).offset(25)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.height.equalTo(65)
                maker.bottom.equalTo(self.contentView)
            }
            
            var blockView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            self.contentView.addSubview(blockView)
            
            blockView.snp.makeConstraints { (maker) in
                maker.width.equalTo(displayZone).multipliedBy(0.25)
                maker.top.bottom.left.equalTo(displayZone)
            }
            
            /// step 1
            var arrow = UIImageView(named: "icon_more-brand")
            
            blockView.addSubview(arrow)
            
            arrow.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(blockView.snp.right)
            }
            
            var step = UIImageView(named: "icon_ordering")
            
            blockView.addSubview(step)
            
            step.snp.makeConstraints { (maker) in
                maker.top.centerX.equalTo(blockView)
                maker.bottom.equalTo(arrow)
            }
            
            var label = ViewFactory.generalLabel(generalSize: 12,
                                                 textColor: UIConfig.generalColor.titleColor)
            
            blockView.addSubview(label)
            
            label.text = "买家下单"
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(step.snp.bottom).offset(7)
                maker.centerX.equalTo(step)
            }
            
            /// step 2
            var left = blockView
             
            blockView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            self.contentView.insertSubview(blockView, belowSubview: left)
            
            blockView.snp.makeConstraints { (maker) in
                maker.width.equalTo(displayZone).multipliedBy(0.25)
                maker.top.bottom.equalTo(displayZone)
                maker.left.equalTo(left.snp.right)
            }
            
            arrow = UIImageView(named: "icon_more-brand")
            
            blockView.addSubview(arrow)
            
            arrow.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(blockView.snp.right)
            }
            
            step = UIImageView(named: "icon_Purchasing")
            
            blockView.addSubview(step)
            
            step.snp.makeConstraints { (maker) in
                maker.top.centerX.equalTo(blockView)
                maker.bottom.equalTo(arrow)
            }
            
            label = ViewFactory.generalLabel(generalSize: 12,
                                             textColor: UIConfig.generalColor.titleColor)
            
            blockView.addSubview(label)
            
            label.text = "飞哥采购"
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(step.snp.bottom).offset(7)
                maker.centerX.equalTo(step)
            }
            
            /// step 3
            left = blockView
            
            blockView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            self.contentView.insertSubview(blockView, belowSubview: left)
            
            blockView.snp.makeConstraints { (maker) in
                maker.width.equalTo(displayZone).multipliedBy(0.25)
                maker.top.bottom.equalTo(displayZone)
                maker.left.equalTo(left.snp.right)
            }
            
            arrow = UIImageView(named: "icon_more-brand")
            
            blockView.addSubview(arrow)
            
            arrow.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(blockView.snp.right)
            }
            
            step = UIImageView(named: "icon_back_home")
            
            blockView.addSubview(step)
            
            step.snp.makeConstraints { (maker) in
                maker.top.centerX.equalTo(blockView)
                maker.bottom.equalTo(arrow)
            }
            
            label = ViewFactory.generalLabel(generalSize: 12,
                                             textColor: UIConfig.generalColor.titleColor)
            
            blockView.addSubview(label)
            
            label.text = "带货回国"
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(step.snp.bottom).offset(7)
                maker.centerX.equalTo(step)
            }
            
            /// step 4
            left = blockView
            
            blockView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            self.contentView.insertSubview(blockView, belowSubview: left)
            
            blockView.snp.makeConstraints { (maker) in
                maker.width.equalTo(displayZone).multipliedBy(0.25)
                maker.top.bottom.equalTo(displayZone)
                maker.left.equalTo(left.snp.right)
            }
            
            step = UIImageView(named: "icon_delivery")
            
            blockView.addSubview(step)
            
            step.snp.makeConstraints { (maker) in
                maker.top.centerX.equalTo(blockView)
            }
            
            label = ViewFactory.generalLabel(generalSize: 12,
                                             textColor: UIConfig.generalColor.titleColor)
            
            blockView.addSubview(label)
            
            label.text = "国内发货"
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(step.snp.bottom).offset(7)
                maker.centerX.equalTo(step)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ProductDetailVC {
    class PicsTableViewCell: UITableViewCell {
        let listView:CoverListView
        
        weak var ownedViewController:UIViewController?
        
        init() {
            let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.titleColor)
            
            label.text = "商品详情"
            
            listView = CoverListView(frame: CGRect(x:0,
                                                   y:0,
                                                   width:UIScreen.main.bounds.width - 30,
                                                   height:0))
            listView.space = 10
            
            super.init(style: .default,
                       reuseIdentifier: "PicsTableViewCell")
            
            self.contentView.addSubviews(label, listView)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(0)
                maker.left.equalTo(self.contentView).offset(15)
                maker.height.equalTo(50)
            }
            
            listView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(label.snp.bottom)
                maker.bottom.equalTo(self.contentView.snp.bottom).offset(-20)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var urls:[URL?] = [] {
            didSet {
                listView.urlList = urls
            }
        }
    }
}

extension ProductDetailVC {
    class InfoTableViewCell: UITableViewCell {
        
        let infoLabel:UILabel
        
        init() {
            let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.titleColor)
            
            label.text = "商品信息"
            
            infoLabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.selected)
            
            infoLabel.numberOfLines = 0
            
            super.init(style: .default,
                       reuseIdentifier: "StoreTableViewCell")
            
            self.contentView.addSubviews(label, infoLabel)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(0)
                maker.left.equalTo(self.contentView).offset(15)
                maker.height.equalTo(50)
            }
            
            infoLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(label.snp.bottom).offset(0)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.bottom.equalTo(self.contentView).offset(-20)
            }
        }
        
        func set(info:[String:String]) {
            if info.keys.count <= 0 {
                infoLabel.text = "无"
                
                return
            }
            
            var infoString = ""
            
            for (key, val) in info {
                if infoString.length > 0 {
                    infoString += "\n"
                }
                
                infoString += "\(key)：\(val)"
            }
            
            infoLabel.attributedText = NSAttributedString(string: infoString,
                                                          font: infoLabel.font,
                                                          textColor: infoLabel.textColor,
                                                          lineSpace: 5)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ProductDetailVC {
    class NumberTableViewCell: UITableViewCell {
        
        let prevButton:UIButton
        
        let nextButton:UIButton
        
        let numberLabel:UILabel
        
        var changeHandler:((Int) -> Void)?
        
        var number:Int = 1{
            didSet {
                numberLabel.text = "\(number)"
                
                prevButton.isEnabled = number > 1
            }
        }
        
        init() {
            let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.titleColor)
            
            label.text = "数量"
            
            prevButton = UIButton(type: .custom)
            prevButton.setImage(UIImage(named:"icon_minus_available"),
                                for: .normal)
            prevButton.setImage(UIImage(named:"icon_minus_not_available"),
                                for: .disabled)
            prevButton.tag = 0
            
            nextButton = UIButton(type: .custom)
            nextButton.setImage(UIImage(named:"icon_plus_available"),
                                for: .normal)
            nextButton.setImage(UIImage(named:"icon_plus_not_available"),
                                for: .disabled)
            nextButton.tag = 2
            
            numberLabel = ViewFactory.label(font: UIConfig.arialBoldFont(15),
                                            textColor: UIConfig.generalColor.titleColor)
            
            numberLabel.text = "1"
            
            super.init(style: .default,
                       reuseIdentifier: "StoreTableViewCell")
            
            self.contentView.addSubviews(label, prevButton, nextButton, numberLabel)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(0)
                maker.left.equalTo(self.contentView).offset(15)
                maker.height.equalTo(50)
            }
            
            prevButton.addTarget(self,
                                 action: #selector(NumberTableViewCell.numberChangeClick(sender:)),
                                 for: UIControlEvents.touchUpInside)
            prevButton.snp.makeConstraints { (maker) in
                maker.top.equalTo(label.snp.bottom)
                maker.left.equalTo(label)
                maker.bottom.equalTo(self.contentView).offset(-20)
            }
            
            numberLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(prevButton.snp.right).offset(40)
                maker.centerY.equalTo(prevButton)
            }
            
            nextButton.addTarget(self,
                                 action: #selector(NumberTableViewCell.numberChangeClick(sender:)),
                                 for: UIControlEvents.touchUpInside)
            nextButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(numberLabel)
                maker.left.equalTo(numberLabel.snp.right).offset(40)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func numberChangeClick(sender:UIButton) {
            let change = sender.tag - 1
            
            changeHandler?(change)
        }
    }
}

extension ProductDetailVC {
    class StandarTableViewCell: UITableViewCell, SelectionButtonCollectionViewDataSource {
        
        let collectionView:SelectionButtonCollectionView
        
        var constraint:Constraint!
        
        let stockTag:UIImageView = UIImageView(named: "icon_failure_notice")
        
        let stocklabel = ViewFactory.generalLabel(generalSize: 13, textColor: UIConfig.generalColor.unselected)
        
        init() {
            let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.titleColor)
            
            label.text = "规格"
            
            collectionView = SelectionButtonCollectionView(frame: CGRect(x:0,
                                                                         y:0,
                                                                         width:UIScreen.main.bounds.width - 30,
                                                                         height:0))
            
            collectionView.horizonSpace = 5
            collectionView.virticalSpace = 5
            
            super.init(style: .default,
                       reuseIdentifier: "StandarTableViewCell")
            
            self.contentView.addSubviews(label, collectionView, stockTag, stocklabel)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(0)
                maker.left.equalTo(self.contentView).offset(15)
                maker.height.equalTo(50)
            }
            
            collectionView.dataSource = self
            collectionView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(label.snp.bottom)
                maker.bottom.equalTo(self.contentView).priority(998)
            }
            
            stockTag.snp.makeConstraints { (maker) in
                maker.left.equalTo(collectionView)
                maker.top.equalTo(collectionView.snp.bottom).offset(10)
                maker.size.equalTo(stockTag.image!.size)
                constraint = maker.bottom.equalTo(self.contentView).priority(1000).constraint
            }
            
            stocklabel.text = "当前规格暂时无货"
            stocklabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(stockTag)
                maker.left.equalTo(stockTag.snp.right).offset(10)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var standars:[ProductDetailStandar] = [] {
            didSet {
                collectionView.reloadData()
            }
        }
        
        var inStock:Bool = true {
            didSet {
                stockTag.isHidden = inStock
                stocklabel.isHidden = inStock
                
                if inStock {
                    constraint.deactivate()
                } else {
                    constraint.activate()
                }
            }
        }
        
        func numberOfButtonInCollectionView(_ collectionView: SelectionButtonCollectionView) -> Int {
            return standars.count
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, sizeFromIndex index: Int) -> CGSize {
            let fontSize = NSAttributedString(string: standars[index].name,
                                              font: UIConfig.generalFont(11),
                                              textColor: UIConfig.generalColor.white).size()
            
            return CGSize(width: fontSize.width + 20,
                          height: 30)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, attributedStrongFromIndex index: Int) -> NSAttributedString {
            return NSAttributedString(string: standars[index].name,
                                      font: UIConfig.generalFont(11),
                                      textColor: UIConfig.generalColor.selected)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, disableAttributedStrongFromIndex index: Int) -> NSAttributedString {
            return NSAttributedString(string: standars[index].name,
                                      font: UIConfig.generalFont(11),
                                      textColor: UIConfig.generalColor.selected.withAlphaComponent(0.3))
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, selectedAttributedStrongFromIndex index: Int) -> NSAttributedString {
            return NSAttributedString(string: standars[index].name,
                                      font: UIConfig.generalFont(11),
                                      textColor: UIConfig.generalColor.white)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, highlightedAttributedStrongFromIndex index: Int) -> NSAttributedString {
            return NSAttributedString(string: standars[index].name,
                                      font: UIConfig.generalFont(11),
                                      textColor: UIConfig.generalColor.white)
        }
    }
}

/*
extension ProductDetailVC {
    class StoreTableViewCell: UITableViewCell, SelectionButtonCollectionViewDataSource {
        
        let collectionView:SelectionButtonCollectionView
        
        var disable:Bool = false
        
        init() {
            let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.titleColor)
            
            label.text = "接受购买渠道"
            
            collectionView = SelectionButtonCollectionView(frame: CGRect(x:0,
                                                                         y:0,
                                                                         width:UIScreen.main.bounds.width - 30,
                                                                         height:0))
            
            collectionView.horizonSpace = 15
            collectionView.virticalSpace = 5
            
            super.init(style: .default,
                       reuseIdentifier: "StoreTableViewCell")
            
            self.contentView.addSubviews(label, collectionView)
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(0)
                maker.left.equalTo(self.contentView).offset(15)
                maker.height.equalTo(50)
            }
            
            collectionView.dataSource = self
            collectionView.allowsMultipleSelection = true
            collectionView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(label.snp.bottom)
                maker.bottom.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var stores:[ProductDetailStore] = [] {
            didSet {
                collectionView.reloadData()
            }
        }
        
        func numberOfButtonInCollectionView(_ collectionView: SelectionButtonCollectionView) -> Int {
            return stores.count
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, sizeFromIndex index: Int) -> CGSize {
            let width = (collectionView.w - collectionView.horizonSpace) / 2.0
            
            return CGSize(width: width,
                          height: 44)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, buttonEnableFromIndex index: Int) -> Bool {
            return disable ? false : stores[index].isValid
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, attributedStrongFromIndex index: Int) -> NSAttributedString {
            let item = stores[index]
            
            let attributedString = NSMutableAttributedString(string: item.storeName + "\n",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.selected,
                                                             lineSpace: 2.5,
                                                             alignment: .center)
            
            attributedString.append(item.rmb.attributedString(color: UIConfig.generalColor.red,
                                                              intSize: 12,
                                                              floatSize: 10))
            
            attributedString.append(NSAttributedString(string: "   有货",
                                                       font: UIConfig.generalFont(11),
                                                       textColor: UIConfig.generalColor.selected))
            
            return NSAttributedString(attributedString: attributedString)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, disableAttributedStrongFromIndex index: Int) -> NSAttributedString {
            let item = stores[index]
            
            let attributedString = NSMutableAttributedString(string: item.storeName + "\n",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.selected.withAlphaComponent(0.3),
                                                             lineSpace: 2.5,
                                                             alignment: .center)
            
            attributedString.append(item.rmb.attributedString(color: UIConfig.generalColor.red.withAlphaComponent(0.3),
                                                              intSize: 12,
                                                              floatSize: 10))
            
            attributedString.append(NSAttributedString(string: "   无货",
                                                       font: UIConfig.generalFont(11),
                                                       textColor: UIConfig.generalColor.selected.withAlphaComponent(0.3)))
            
            return NSAttributedString(attributedString: attributedString)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, selectedAttributedStrongFromIndex index: Int) -> NSAttributedString {
            let item = stores[index]
            
            let attributedString = NSMutableAttributedString(string: item.storeName + "\n",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.white,
                                                             lineSpace: 2.5,
                                                             alignment: .center)
            
            attributedString.append(item.rmb.attributedString(color: UIConfig.generalColor.white,
                                                              intSize: 12,
                                                              floatSize: 10))
            
            attributedString.append(NSAttributedString(string: "   有货",
                                                       font: UIConfig.generalFont(11),
                                                       textColor: UIConfig.generalColor.white))
            
            return NSAttributedString(attributedString: attributedString)
        }
        
        func collectionView(_ collectionView: SelectionButtonCollectionView, highlightedAttributedStrongFromIndex index: Int) -> NSAttributedString {
            let item = stores[index]
            
            let attributedString = NSMutableAttributedString(string: item.storeName + "\n",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.white,
                                                             lineSpace: 2.5,
                                                             alignment: .center)
            
            attributedString.append(item.rmb.attributedString(color: UIConfig.generalColor.white,
                                                              intSize: 12,
                                                              floatSize: 10))
            
            attributedString.append(NSAttributedString(string: "   有货",
                                                       font: UIConfig.generalFont(11),
                                                       textColor: UIConfig.generalColor.white))
            
            return NSAttributedString(attributedString: attributedString)
        }
    }
}
 */

extension ProductDetailVC {
    class CommentTableViewCell: UITableViewCell, GalleryDisplacedViewsDataSource, GalleryItemsDataSource {
        let titleLabel:UILabel
        
        let progressImageView:UIImageView
        
        let numberLabel:UILabel
        
        let avatarImaegView:UIImageView
        
        let nickNameLabel:UILabel
        
        let dateLabel:UILabel
        
        let starView:UIView
        
        let contentLabel:UILabel
        
        let subTitleLabel:UILabel
        
        let toAllButton = UIButton(type: .custom)
        
        let stackView = PicsView()
        
        var constraint:Constraint!
        
        typealias PicsView = CommentVC.PicsView
        
        static func progressImage(of progress:Float) -> UIImage? {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 150, height: 6),
                                                   false,
                                                   UIScreen.main.scale)
            
            guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
                UIGraphicsEndImageContext()
                
                return nil
            }
            
            /// backgroud 
            let backgroudPath = CGPath(roundedRect: CGRect(x: 0, y: 0, width: 150, height: 6), cornerWidth: 3, cornerHeight: 3, transform: nil)
            
            ctx.addPath(backgroudPath)
            ctx.setFillColor(UIConfig.generalColor.backgroudGray.cgColor)
            ctx.fillPath()
            
            /// progress
            let width = 150 * CGFloat(progress)
            
            let path = CGPath(roundedRect: CGRect(x: 0, y: 0, width: width, height: 6), cornerWidth: 3, cornerHeight: 3, transform: nil)
            
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            
            let array = [UIColor(r: 247, g: 107, b: 98).withAlphaComponent(0.95).cgColor, UIColor(r: 253, g: 60, b: 83).withAlphaComponent(0.95).cgColor] as CFArray
            
            let gradient = CGGradient(colorsSpace: colorSpace,
                                      colors: array,
                                      locations: [0.0, 1.0])
            
            
            let startPoint = CGPoint(x: width / 2.0, y: 0)
            
            let endPoint = CGPoint(x: width / 2.0 + 6 * 0.176, y: 6)
            
            ctx.addPath(path)
            ctx.clip()
            ctx.drawLinearGradient(gradient!,
                                   start: startPoint,
                                   end: endPoint,
                                   options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            
            return image
        }
        
        init() {
            titleLabel = ViewFactory.generalLabel()
            titleLabel.text = "宝贝评价 （共0条）"
            titleLabel.tag = 1
            
            toAllButton.tag = 1
            toAllButton.setAttributedTitle(NSAttributedString(string: "查看全部 >",
                                                              font: UIConfig.generalFont(11),
                                                              textColor: UIConfig.generalColor.labelGray),
                                           for: .normal)
            
            let proposeLabel = ViewFactory.generalLabel(generalSize: 13,
                                                        textColor: UIConfig.generalColor.unselected)
            
            proposeLabel.text = "值得买指数"
            
            progressImageView = UIImageView()
            
            numberLabel = ViewFactory.generalLabel(generalSize: 13,
                                                   textColor: UIConfig.generalColor.red)
            
            numberLabel.text = "0.0"
            
            avatarImaegView = UIImageView()
            
            nickNameLabel = ViewFactory.generalLabel()
            nickNameLabel.text = "昵称"
            
            dateLabel = ViewFactory.generalLabel(generalSize: 12,
                                                 textColor: UIConfig.generalColor.labelGray)
            dateLabel.text = "日期"
            
            starView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            let starImage = UIImage(named: "icon_score")!
            
            for i in 0..<5 {
                let starImageView = UIImageView(image: starImage)
                
                let x = (starImage.size.width + 3) * CGFloat(i)
                
                starImageView.frame = CGRect(x: x,
                                             y: 0,
                                             width: starImage.size.width,
                                             height: starImage.size.height)
                
                starView.insertSubview(starImageView, at: 0)
            }
            
            contentLabel = ViewFactory.generalLabel()

            contentLabel.numberOfLines = 0
            contentLabel.text = "评价"
            
            subTitleLabel = ViewFactory.generalLabel(generalSize: 12,
                                                     textColor: UIConfig.generalColor.unselected)
            subTitleLabel.numberOfLines = 0
            
            super.init(style: .default,
                       reuseIdentifier: "CommentTableViewCell")
            
            self.contentView.clipsToBounds = true
            self.contentView.addSubviews(titleLabel, toAllButton, proposeLabel, progressImageView, numberLabel, avatarImaegView, nickNameLabel, dateLabel, starView, contentLabel, stackView, subTitleLabel)
        
            titleLabel.snp.makeConstraints { (maker) in
                maker.top.left.equalTo(self.contentView).offset(15)
                noneBottomConstraint = maker.bottom.equalTo(self.contentView).offset(-15).priority(999).constraint
            }
            
            toAllButton.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.centerY.equalTo(titleLabel)
            }
            
            proposeLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel.snp.bottom).offset(15)
                maker.left.equalTo(self.contentView).offset(15)
            }
            
            progressImageView.snp.makeConstraints { (maker) in
                maker.left.equalTo(proposeLabel.snp.right).offset(15)
                maker.centerY.equalTo(proposeLabel)
            }
            
            numberLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(progressImageView)
                maker.left.equalTo(progressImageView.snp.right).offset(5)
            }
            
            avatarImaegView.snp.makeConstraints { (maker) in
                maker.width.height.equalTo(30)
                maker.left.equalTo(titleLabel)
                maker.top.equalTo(proposeLabel.snp.bottom).offset(15)
            }
            
            nickNameLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView.snp.right).offset(10)
                maker.top.equalTo(avatarImaegView).offset(-3)
                maker.right.equalTo(starView.snp.left).offset(-5)
            }
            
            dateLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView.snp.right).offset(10)
                maker.bottom.equalTo(avatarImaegView).offset(3)
                maker.right.equalTo(starView.snp.left).offset(-5)
            }
            
            starView.snp.makeConstraints { (maker) in
                maker.right.equalTo(toAllButton)
                maker.top.equalTo(nickNameLabel)
                maker.width.equalTo(starImage.size.width * 5 + 12)
                maker.height.equalTo(starImage.size.height)
            }
            
            contentLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView)
                maker.top.equalTo(avatarImaegView.snp.bottom).offset(10)
                maker.right.equalTo(starView)
            }
            
            stackView.imageTap = {
                [weak self]
                (index) in
                
                self?.imagePreview(of: index)
                
            }
            stackView.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView)
                maker.top.equalTo(contentLabel.snp.bottom).offset(15)
                maker.right.equalTo(starView)
            }
            
            subTitleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(contentLabel)
                maker.right.equalTo(contentLabel)
                maker.bottom.equalTo(self.contentView).offset(-12).priority(998)
                maker.top.equalTo(stackView.snp.bottom).offset(12).priority(998)
                constraint = maker.top.equalTo(contentLabel.snp.bottom).offset(12).priority(999).constraint
            }
        }
        
        var item:ProductDetailComment? {
            didSet {
                guard let `item` = self.item, let comment = item.item else {
                    
                    titleLabel.text = "宝贝评价 （共0条）"
                    
                    noneBottomConstraint.activate()
                    
                    for subview in self.contentView.subviews {
                        if subview.tag == 0 {
                            subview.isHidden = true
                        }
                    }
                    
                    picUrls = []
                    
                    return
                }
                
                for subview in self.contentView.subviews {
                    if subview.tag == 0 {
                        subview.isHidden = false
                    }
                }
                
                titleLabel.text = "宝贝评价 （共\(item.commentCount)条）"
                
                noneBottomConstraint.deactivate()
                
                let progress = min(5.0, comment.proposeValue / 5.0)
                
                progressImageView.image = CommentTableViewCell.progressImage(of: progress)
                
                numberLabel.text = String(format:"%.1f", comment.proposeValue)
                
                avatarImaegView.setAvatar(url: comment.avatar)
                
                dateLabel.text = comment.time
                
                starView.isHidden = false
                for (index, starImageView) in starView.subviews.enumerated() {
                    starImageView.isHidden = (index + 1) > comment.star
                }
                
                contentLabel.attributedText = NSAttributedString(string: comment.content,
                                                                 font: UIConfig.generalFont(15),
                                                                 textColor: UIConfig.generalColor.selected,
                                                                 lineSpace: 2)
                
                subTitleLabel.text = "购入规格：\(comment.standar) 入手价格：\(comment.price.description)"
                
                picUrls = comment.images
                
                stackView.set(imageUrls: picUrls, width: UIScreen.main.bounds.width - 30)
                
                nickNameLabel.text = comment.nickName
                
                if picUrls.count > 0 {
                    constraint.deactivate()
                } else {
                    constraint.activate()
                }
            }
        }
        
        var picUrls:[URL] = []
        
        var noneBottomConstraint:Constraint!
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
            return (stackView.subviews[index] as? UIImageView).flatMap({ $0 })
        }
        
        func itemCount() -> Int {
            return picUrls.count
        }
        
        func provideGalleryItem(_ index: Int) -> GalleryItem {
            let url = picUrls[index]
            
            return .image(fetchImageBlock: { (fetchImageBlock) in
                KingfisherManager.shared.retrieveImage(with: url,
                                                       options: nil,
                                                       progressBlock: nil,
                                                       completionHandler: { (image, _, _, _) in
                                                        fetchImageBlock(image)
                })
                
            })
        }
        
        weak var viewController:UIViewController?
        
        func imagePreview(of index:UInt) {
            guard let viewController = self.viewController else {
                return
            }
            
            let galleryVC = GalleryViewController(startIndex: Int(index),
                                                  itemsDataSource: self,
                                                  displacedViewsDataSource: self,
                                                  configuration: ProductDetailVC.galleryConfiguration())
            
            galleryVC.headerView = nil
            
            viewController.present(galleryVC,
                                   animated: false,
                                   completion: nil)
        }
    }
}

extension ProductDetailVC {
    class DetailTableViewCell: UITableViewCell {
        
        let titleLabel:UILabel
        
        let subTitleButton:UIButton
        
        let priceLabel:UILabel
        
        let oriLabel:UILabel
        
        let tipLabel:UILabel
        
//        let priceButton:UIButton
        
        var constraint:Constraint!
        
        init() {
            titleLabel = ViewFactory.label(font: UIConfig.generalFont(15),
                                           textColor: UIConfig.generalColor.selected)
            
            titleLabel.numberOfLines = 0
            
            subTitleButton = UIButton(type: .custom)
            subTitleButton.setBackgroundColor(UIConfig.generalColor.white,
                                              forState: .normal)
            subTitleButton.contentHorizontalAlignment = .left
            subTitleButton.titleLabel?.numberOfLines = 0
            subTitleButton.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            
            priceLabel = ViewFactory.label(font: UIConfig.arialFont(13),
                                           textColor: UIConfig.generalColor.red)
            priceLabel.numberOfLines = 0
            priceLabel.adjustsFontSizeToFitWidth = true
            
            oriLabel = DeleteLabel()
            
            oriLabel.font = UIConfig.generalFont(11)
            oriLabel.textColor = UIConfig.generalColor.unselected
            oriLabel.backgroundColor = UIConfig.generalColor.white
            
            tipLabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                         textColor: UIConfig.generalColor.titleColor)
            tipLabel.numberOfLines = 0
            tipLabel.adjustsFontSizeToFitWidth = true
            
//            priceButton = UIButton(type: .custom)
//            
//            priceButton.setImage(UIImage(named:"icon_price_state"),
//                                 for: .normal)
            
            super.init(style: .default,
                       reuseIdentifier: "DetailTableViewCell")
            
            self.selectionStyle = .none
            
            self.contentView.addSubviews(titleLabel, subTitleButton, priceLabel, oriLabel, tipLabel)
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.top.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            subTitleButton.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.top.equalTo(titleLabel.snp.bottom).offset(3)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-15)
            }
            
            priceLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.top.equalTo(subTitleButton.snp.bottom).offset(10)
            }
            
            oriLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(priceLabel.snp.right).offset(15)
                maker.bottom.equalTo(priceLabel)
            }
            
            tipLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(priceLabel.snp.bottom).offset(10)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-15)
                maker.bottom.equalTo(self.contentView).offset(-10)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func set(item:ProductDetailItem) {
            self.titleLabel.text = item.title
            
            self.subTitleButton.setAttributedTitle({
                (type:String, brand:String) -> NSAttributedString? in
                
                if type.length <= 0 && brand.length <= 0 {
                    return nil
                }
                
                let attr = NSMutableAttributedString()
                
                if type.length > 0 {
                    attr.append(NSAttributedString(string: "# \(type)    ",
                        font: UIConfig.generalFont(12),
                        textColor: UIConfig.generalColor.labelGray))
                }
                
                if brand.length > 0 {
                    attr.append(NSAttributedString(string: "# \(brand)",
                        font: UIConfig.generalFont(12),
                        textColor: UIColor(hexString: "#3977cb")!))
                }
                
                return NSAttributedString(attributedString: attr)
            }(item.typeName, item.brandName),
                                                   for: .normal)
            
            
            
            let attributedString = NSMutableAttributedString(attributedString: item.rmb.attributedString(intSize: 17, floatSize: 13))
            
            attributedString.append(NSAttributedString(string: "   "))
            
            if Int(item.price.value) > 0 {
                attributedString.append(NSAttributedString.init(withDeleteLine: "市场价：\(item.price.description)",
                    font: UIConfig.generalFont(11),
                    textColor: UIConfig.generalColor.unselected))
            }
            
            self.priceLabel.attributedText = attributedString
            
            if let subAttrString = self.subTitleButton.titleLabel?.attributedText, subAttrString.string.length > 0 {
                priceLabel.snp.remakeConstraints { (maker) in
                    maker.left.equalTo(titleLabel)
                    maker.top.equalTo(subTitleButton.snp.bottom).offset(10)
                }
            } else {
                priceLabel.snp.remakeConstraints { (maker) in
                    maker.left.equalTo(titleLabel)
                    maker.top.equalTo(titleLabel.snp.bottom).offset(10)
                }
            }
            
            self.tipLabel.text = "运费:\(item.deliver <= 0.0 ? "免费" : "￥\(item.deliver.priceString)")   \(item.arriveTime)"
        }
    }
}

extension ProductDetailVC {
    func popClick(sender:Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showMore(sender:UIButton) {
        let popoverViewController = UIViewController()
        
        popoverViewController.preferredContentSize = CGSize(width: 100, height: 151)
        popoverViewController.modalPresentationStyle = .popover
        popoverViewController.popoverPresentationController?.permittedArrowDirections = .up
        popoverViewController.popoverPresentationController?.sourceView = self.view
        popoverViewController.popoverPresentationController?.sourceRect = sender.frame
        popoverViewController.popoverPresentationController?.delegate = self
        popoverViewController.popoverPresentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        let homeButton = UIButton(type: .custom)
        
        homeButton.setImage(UIImage(named:"icon_home_line"),
                            for: .normal)
        homeButton.setAttributedTitle(NSAttributedString(string: "首页",
                                                         font: UIConfig.generalFont(15),
                                                         textColor: UIConfig.generalColor.white),
                                      for: .normal)
        homeButton.frame = CGRect(x: 0,
                                  y: 0,
                                  width: 100,
                                  height: 50)
        homeButton.addTarget(self,
                             action: #selector(ProductDetailVC.popToHome),
                             for: UIControlEvents.touchUpInside)
        homeButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5)
        homeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5)
        
        let lineView = UIView(frame: CGRect(x: 0, y:50, width:100, height:0.5))
        
        lineView.backgroundColor = UIConfig.generalColor.white
        
        let messageButton = UIButton(type: .custom)
        
        messageButton.setImage(UIImage(named:"icon_news_line"),
                               for: .normal)
        messageButton.setAttributedTitle(NSAttributedString(string: "消息",
                                                            font: UIConfig.generalFont(15),
                                                            textColor: UIConfig.generalColor.white),
                                         for: .normal)
        messageButton.frame = CGRect(x: 0,
                                     y: 50.5,
                                     width: 100,
                                     height: 50)
        messageButton.addTarget(self,
                                action: #selector(ProductDetailVC.showMessageVC),
                                for: UIControlEvents.touchUpInside)
        messageButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5)
        messageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5)
        
        let lineView2 = UIView(frame: CGRect(x: 0, y:100.5, width:100, height:0.5))
        
        lineView2.backgroundColor = UIConfig.generalColor.white
        
        let shareButton = UIButton(type: .custom)
        
        shareButton.setImage(UIImage(named:"icon_share_line-1"),
                             for: .normal)
        shareButton.setAttributedTitle(NSAttributedString(string: "分享",
                                                          font: UIConfig.generalFont(15),
                                                          textColor: UIConfig.generalColor.white),
                                       for: .normal)
        shareButton.frame = CGRect(x: 0,
                                   y: 101,
                                   width: 100,
                                   height: 50)
        shareButton.addTarget(self,
                              action: #selector(ProductDetailVC.shareButtonPressed(sender:)),
                              for: UIControlEvents.touchUpInside)
        shareButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5)
        shareButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5)
        
        popoverViewController.view.addSubviews(homeButton, lineView, messageButton, lineView2, shareButton)
        
        self.present(popoverViewController,
                     animated: true,
                     completion: nil)
    }
    
    func showCartVC(sender:UIButton) {
        let cartVC = CartVC()
        
        cartVC.showToolBar = false
        
        self.navigationController?.pushViewController(cartVC,
                                                      animated: true)
    }
    
    func popToHome() {
        self.dismiss(animated: true,
                     completion: nil)
        
        let _ = self.navigationController?.popToRootViewController(animated: true)
        
        if let mainVC = self.navigationController?.viewControllers.first as? MainVC {
            mainVC.selectedIndex = 0
        }
    }
    
    func showMessageVC() {
        self.dismiss(animated: true,
                     completion: nil)
        
        let messageVC = MessageEntryVC()
        
        self.navigationController?.pushViewController(messageVC,
                                                      animated: true)
    }
    
    func toPriceIntroduce() {
        guard let url = URL(string: URLConfig.prefix + "flyapp/iospicdetail.html") else {
            return
        }
        
        let webVC = WKWebViewController(url: url)
        
        webVC.title = "价格说明"
        
        self.navigationController?.pushViewController(webVC,
                                                      animated: true)
    }
}

extension ProductDetailVC: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension ProductDetailVC {
    class FakeNavigationBar: UIView {
        let lightBackBarButton: UIButton
        
        let lightCartBarButton: UIButton?
        
        let lightMoreBarButton: UIButton
        
        let darkBackBarButton: UIButton
        
        let darkCartBarButton: UIButton?
        
        let darkMoreBarButton: UIButton
        
        let lineView: UIView
        
        init(target:ProductDetailVC, showCart:Bool) {
            /// back
            lightBackBarButton = UIButton(type: .custom)
            
            lightBackBarButton.setImage(UIImage(named:"icon_return_white"),
                                        for: .normal)
            
            lightBackBarButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            lightBackBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2)
            lightBackBarButton.layer.cornerRadius = 15
            lightBackBarButton.layer.masksToBounds = true
            lightBackBarButton.addTarget(target,
                                         action: #selector(ProductDetailVC.popClick(sender:)),
                                         for: UIControlEvents.touchUpInside)
            
            darkBackBarButton = UIButton(type: .custom)
            
            darkBackBarButton.setImage(UIImage(named:"icon_return_gray"),
                                       for: .normal)
            darkBackBarButton.backgroundColor = UIColor.clear
            darkBackBarButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2)
            darkBackBarButton.addTarget(target,
                                        action: #selector(ProductDetailVC.popClick(sender:)),
                                        for: UIControlEvents.touchUpInside)
            
            /// more
            lightMoreBarButton = UIButton(type: .custom)
            
            lightMoreBarButton.setImage(UIImage(named:"icon_more"),
                                        for: .normal)
            
            lightMoreBarButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            lightMoreBarButton.layer.cornerRadius = 15
            lightMoreBarButton.layer.masksToBounds = true
            lightMoreBarButton.addTarget(target,
                                         action: #selector(ProductDetailVC.showMore(sender:)),
                                         for: UIControlEvents.touchUpInside)
            
            darkMoreBarButton = UIButton(type: .custom)
            
            darkMoreBarButton.setImage(UIImage(named:"icon_more_gray"),
                                       for: .normal)
            darkMoreBarButton.backgroundColor = UIColor.clear
            darkMoreBarButton.addTarget(target,
                                        action: #selector(ProductDetailVC.showMore(sender:)),
                                        for: UIControlEvents.touchUpInside)
            
            /// cart
            if showCart {
                lightCartBarButton = UIButton(type: .custom)
                
                lightCartBarButton!.setImage(UIImage(named:"icon_shopping-cart_line"),
                                            for: .normal)
                
                lightCartBarButton!.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                lightCartBarButton!.layer.cornerRadius = 15
                lightCartBarButton!.layer.masksToBounds = true
                lightCartBarButton!.addTarget(target,
                                             action: #selector(ProductDetailVC.showCartVC(sender:)),
                                             for: UIControlEvents.touchUpInside)
                
                darkCartBarButton = UIButton(type: .custom)
                
                darkCartBarButton!.setImage(UIImage(named:"icon_shopping-cart_line_gray"),
                                           for: .normal)
                darkCartBarButton!.backgroundColor = UIColor.clear
                darkCartBarButton!.addTarget(target,
                                            action: #selector(ProductDetailVC.showCartVC(sender:)),
                                            for: UIControlEvents.touchUpInside)
            } else {
                lightCartBarButton = nil
                
                darkCartBarButton = nil
            }
            
            lineView = ViewFactory.view(color: UIConfig.generalColor.lineColor)
            
            super.init(frame: .zero)
            
            self.addSubviews(lightBackBarButton, lightMoreBarButton, darkBackBarButton, darkMoreBarButton)
            
            if showCart {
                self.addSubviews(lightCartBarButton!, darkCartBarButton!)
            }
            
            self.addSubview(lineView)
            
            lightBackBarButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self).offset(10)
                maker.left.equalTo(self).offset(14)
                maker.size.equalTo(CGSize(width:30, height:30))
            }
            
            darkBackBarButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self).offset(10)
                maker.left.equalTo(self).offset(14)
                maker.size.equalTo(CGSize(width:30, height:30))
            }
            
            lightMoreBarButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self).offset(10)
                maker.right.equalTo(self).offset(-10)
                maker.size.equalTo(CGSize(width:30, height:30))
            }
            
            darkMoreBarButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self).offset(10)
                maker.right.equalTo(self).offset(-10)
                maker.size.equalTo(CGSize(width:30, height:30))
            }
            
            lightCartBarButton?.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self).offset(10)
                maker.right.equalTo(lightMoreBarButton.snp.left).offset(-10)
                maker.size.equalTo(CGSize(width:30, height:30))
            }
            
            darkCartBarButton?.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self).offset(10)
                maker.right.equalTo(lightMoreBarButton.snp.left).offset(-10)
                maker.size.equalTo(CGSize(width:30, height:30))
            }
            
            lineView.snp.makeConstraints { (maker) in
                maker.height.equalTo(0.5)
                maker.left.bottom.right.equalTo(self)
            }
        }
        
        var darkValue:CGFloat = 0.0 {
            didSet {
                let value = max(0, min(1.0, darkValue))
                
                if value > 0.5 {
                    lightBackBarButton.isHidden = true
                    lightMoreBarButton.isHidden = true
                    lightCartBarButton?.isHidden = true
                    
                    darkBackBarButton.isHidden = false
                    darkMoreBarButton.isHidden = false
                    darkCartBarButton?.isHidden = false
                    
                    darkBackBarButton.alpha = ((value - 0.5) / 0.5)
                    darkMoreBarButton.alpha = ((value - 0.5) / 0.5)
                    darkCartBarButton?.alpha = ((value - 0.5) / 0.5)
                } else {
                    lightBackBarButton.isHidden = false
                    lightMoreBarButton.isHidden = false
                    lightCartBarButton?.isHidden = false
                    
                    darkBackBarButton.isHidden = true
                    darkMoreBarButton.isHidden = true
                    darkCartBarButton?.isHidden = true
                    
                    lightBackBarButton.alpha = ((0.5 - value) / 0.5)
                    lightMoreBarButton.alpha = ((0.5 - value) / 0.5)
                    lightCartBarButton?.alpha = ((0.5 - value) / 0.5)
                }
                
                self.backgroundColor = UIConfig.generalColor.white.withAlphaComponent(value * 0.95)
                
                lineView.alpha = value
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
