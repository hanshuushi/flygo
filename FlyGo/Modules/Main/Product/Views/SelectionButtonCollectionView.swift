//
//  SelectionButtonCollectionView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/17.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

@objc
protocol SelectionButtonCollectionViewDelegate {
    func collectionView(_ collectionView:SelectionButtonCollectionView, didSelectedIndexs:[Int])
}

@objc
protocol SelectionButtonCollectionViewDataSource {
    func numberOfButtonInCollectionView(_ collectionView:SelectionButtonCollectionView) -> Int
    
    func collectionView(_ collectionView:SelectionButtonCollectionView, sizeFromIndex index:Int) -> CGSize
    
    func collectionView(_ collectionView:SelectionButtonCollectionView, attributedStrongFromIndex index:Int) -> NSAttributedString
    
    @objc
    optional func collectionView(_ collectionView:SelectionButtonCollectionView, buttonEnableFromIndex index:Int) -> Bool
    
    @objc
    optional func collectionView(_ collectionView:SelectionButtonCollectionView, selectedAttributedStrongFromIndex index:Int) -> NSAttributedString

    @objc
    optional func collectionView(_ collectionView:SelectionButtonCollectionView, disableAttributedStrongFromIndex index:Int) -> NSAttributedString

    @objc
    optional func collectionView(_ collectionView:SelectionButtonCollectionView, highlightedAttributedStrongFromIndex index:Int) -> NSAttributedString

}

class SelectionButtonCollectionView: UIView {
    
    fileprivate class RedButton: UIButton {
        
        var oriBounds:CGRect?
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            if let oriBounds = self.oriBounds, oriBounds == self.bounds {
                return
            }
            
            oriBounds = self.bounds
            
            UIGraphicsBeginImageContextWithOptions(CGSize(width: self.w, height: self.h),
                                                   false,
                                                   UIScreen.main.scale)
            
            guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
                UIGraphicsEndImageContext()
                
                return
            }
            
            let path = CGMutablePath()
            
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            
            let array = [UIColor(r: 247, g: 107, b: 98).withAlphaComponent(0.95).cgColor, UIColor(r: 253, g: 60, b: 83).withAlphaComponent(0.95).cgColor] as CFArray
            
            let gradient = CGGradient(colorsSpace: colorSpace,
                                      colors: array,
                                      locations: [0.0, 1.0])
            
            let startPoint = CGPoint(x: self.w / 2.0, y: 0)
            
            let endPoint = CGPoint(x: self.w / 2.0 + self.h * 0.176, y: self.h)
            
            path.addRect(self.bounds)
            
            
            ctx.addPath(path)
            ctx.clip()
            ctx.drawLinearGradient(gradient!,
                                   start: startPoint,
                                   end: endPoint,
                                   options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            
            self.setBackgroundImage(image,
                                    for: .selected)
        }
    }
    
    static func createButton() -> UIButton {
        let button = RedButton(type: .custom)
        
        button.setBackgroundColor(UIConfig.generalColor.backgroudGray,
                                  forState: .normal)
        button.setBackgroundColor(UIConfig.generalColor.highlyRed,
                                  forState: .highlighted)
        button.setBackgroundColor(UIConfig.generalColor.backgroudGray.withAlphaComponent(0.3),
                                  forState: .disabled)
        
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.textAlignment = .center
        
        return button
    }
    
    var horizonSpace:CGFloat = 10
    
    var virticalSpace:CGFloat = 10
    
    private var contentHeight:CGFloat = 0
    
    override var intrinsicContentSize:CGSize {
        get {
            return CGSize(width: self.w,
                          height: contentHeight)
        }
    }
    
    private var buttons:[UIButton] = [] {
        didSet {
            self.removeSubviews()
            
            self.view.addSubviews(buttons)
        }
    }
    
    func reloadData() {
        guard let _dataSource = dataSource else {
            return
        }
        
        var buttons = [UIButton]()
        
        var sizeArray = [CGSize]()
        
        let count = _dataSource.numberOfButtonInCollectionView(self)
        
        for i in 0..<count {
            let size = _dataSource.collectionView(self,
                                                  sizeFromIndex: i)
            
            sizeArray.append(size)
            
            let attributedString = _dataSource.collectionView(self,
                                                              attributedStrongFromIndex: i)
            
            let button = SelectionButtonCollectionView.createButton()
            
            button.setAttributedTitle(attributedString,
                                      for: .normal)
            button.addTarget(self,
                             action: #selector(SelectionButtonCollectionView.buttonClick(sender:)),
                             for: UIControlEvents.touchUpInside)
            button.isEnabled = _dataSource.collectionView?(self,
                                                          buttonEnableFromIndex: i) ?? true
            
            buttons.append(button)
            
            if let selectedAttributedString = _dataSource.collectionView?(self,
                                                                          selectedAttributedStrongFromIndex: i) {
                button.setAttributedTitle(selectedAttributedString,
                                          for: .selected)
            }
            
            if let disableAttributedString = _dataSource.collectionView?(self,
                                                                         disableAttributedStrongFromIndex: i) {
                button.setAttributedTitle(disableAttributedString,
                                          for: .disabled)
            }
            
            if let highlightedAttributedString = _dataSource.collectionView?(self,
                                                                        highlightedAttributedStrongFromIndex: i) {
                button.setAttributedTitle(highlightedAttributedString,
                                          for: .highlighted)
            }
            
            button.titleLabel?.textAlignment = .center
        }
        
        self.buttons = buttons
        self.sizeArray = sizeArray
        
        self.adjustLayout()
    }
    
    var sizeArray:[CGSize] = []
    
    func adjustLayout() {
        let maxWidth = self.w
        
        var x:CGFloat = 0
        
        var y:CGFloat = 0
        
        var bottom:CGFloat = 0
        
        for (index, button) in buttons.enumerated() {
            let size = sizeArray[index]
            
            button.frame = CGRect(x: x,
                                  y: y,
                                  width: size.width,
                                  height: size.height)
            
            while button.right > maxWidth {
                if button.left == 0 {
                    button.w = maxWidth
                } else {
                    y = bottom + virticalSpace
                    
                    button.origin = CGPoint(x: 0,
                                            y: y)
                }
            }
            
            x = button.right + horizonSpace
            
            if button.bottom > bottom {
                bottom = button.bottom
            }
        }
        
        contentHeight = bottom
        
        self.invalidateIntrinsicContentSize()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustLayout()
    }
    
    weak var dataSource:SelectionButtonCollectionViewDataSource?
    
    weak var delegate:SelectionButtonCollectionViewDelegate?
    
    func setSelected(selectedList:[Int]) {
        for (index, button) in buttons.enumerated() {
            button.isSelected = selectedList.contains(index)
        }
    }
    
    var allowsMultipleSelection:Bool = false
    
    func buttonClick(sender:UIButton) {
        let isSelected = sender.isSelected
        
        if allowsMultipleSelection {
            sender.isSelected = !isSelected
        } else {
            if !isSelected {
                for one in buttons {
                    one.isSelected = (one == sender)
                }
            } else {
                return
            }
        }
        
        var selected = [Int]()
        
        for (index, one) in buttons.enumerated() {
            if one.isSelected {
                selected.append(index)
            }
        }
        
        delegate?.collectionView(self, didSelectedIndexs: selected)
    }
}
