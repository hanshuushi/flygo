//
//  CoverListView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/18.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

@objc
protocol CoverListViewDelegate {
    func listViewChangeSize(_ listView:CoverListView)
    
    @objc
    optional func listView(_ listView:CoverListView,
                           didClickFromButtonIndex index:Int)
}

class CoverListView: UIView {
    
    var imageRate:CGFloat = 0.5
    
    var urlList:[URL?] = [] {
        didSet {
            rateList = urlList.map({_ in nil})
            
            buttons = urlList.map({ (_) -> UIButton in
                let button = UIButton(type: .custom)
                
                button.adjustsImageWhenHighlighted = false
                button.setBackgroundColor(UIConfig.generalColor.white,
                                          forState: .normal)
                button.addTarget(self,
                                 action: #selector(CoverListView.buttonPressed(sender:)),
                                 for: UIControlEvents.touchUpInside)
                
                return button
            })
            
            for (index, url) in urlList.enumerated() {
                buttons[index].kf.setImage(with: url,
                                           for: .normal,
                                           placeholder: nil,
                                           options: nil,
                                           progressBlock: nil,
                                           completionHandler: {
                                            [weak self] (image, _, _, _) in
                                            
                                            if let strongSelf = self, let currentImage = image {
                                                strongSelf.rateList[index] = currentImage.size.height / currentImage.size.width
                                                strongSelf.adjustLayout()
                                                strongSelf.delegate?.listViewChangeSize(strongSelf)
                                            }
                })
            }
            
            adjustLayout()
        }
    }
    
    var rateList:[CGFloat?] = []
    
    var space:CGFloat = 10
    
    var buttons:[UIButton] = [] {
        didSet(oldVal) {
            for one in oldVal {
                one.removeFromSuperview()
                one.kf.cancelImageDownloadTask()
            }
            
            self.addSubviews(buttons)
        }
    }
    
    func adjustLayout() {
        var y:CGFloat = 0
        
        let width = self.w
        
        for (index, button) in buttons.enumerated() {
            if index > 0 {
                y += space
            }
            
            let currentRate:CGFloat = rateList[index] ?? imageRate
            
            let height = width * currentRate
            
            button.frame = CGRect(x: 0,
                                  y: y,
                                  width: width,
                                  height: height)
            
            y = button.frame.maxY
        }
        
        self.invalidateIntrinsicContentSize()
    }
    
    weak var delegate:CoverListViewDelegate?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustLayout()
    }
    
    override var intrinsicContentSize: CGSize {
        let size = CGSize(width: self.w,
                          height: self.buttons.reduce(0,
                                                      { (result, button) -> CGFloat in
                                                        return max(result, button.bottom)
                          }))
        
        return size
    }
    
    func buttonPressed(sender:UIButton) {
        if let index = buttons.index(of: sender) {
            delegate?.listView?(self,
                                didClickFromButtonIndex: index)
        }
    }
}
