//
//  ProductListVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


/// Product
struct ProductItem {
    var rank:String = ""
    
    var recId:String = ""
    
    var id:String = ""
    
    var name:String = ""
    
    var rmb:Currency = Currency(unit: "￥", value: 0)
    
    var price:Currency = Currency(unit: "$", value: 0)
    
    var imageUrl:URL?
    
    init() {
        
    }
    
    init(model:API.Product) {
        id = model.productId
        
        recId = model.productRecId
        
        name = model.productName
        
        rmb = model.cnyPrice
        
        price = model.sellPrice
        
        imageUrl = model.productPics
    }
    
    init(model:API.HomeMenuItem.Promotion.Item) {
        id = model.productId
        
        recId = model.productRecId
        
        name = model.productName
        
        price = model.sellPrice
        
        rmb = model.cnyPrice
        
        imageUrl = model.pic
    }
    
    init(model:API.HomeMenuItem.Top.Item) {
        id = model.productId
        
        recId = model.productRecId
        
        name = model.productName
        
        rmb = model.cnyPrice
        
        price = model.sellPrice
        
        rank = model.useId
        
        imageUrl = model.pic
    }
}

struct ProductBrandItem {
    var id:String = ""
    
    var imageUrl:URL?
    
    var name:String = ""
    
    var nameCN:String = ""
    
    var nameEN:String = ""
    
    var indexLetter:String = ""
    
    init() {
        
    }
    
    init (model:API.HomeMenuItem.MenuBrand.Item) {
        self.id = model.brandId
        
        self.imageUrl = model.pic
        
        self.nameCN = model.menuBrandName
        
        self.name = self.nameCN
    }
    
    init (model:API.ProductBrand) {
        self.id = model.brandId
        
        self.imageUrl = model.pic
        
        self.nameCN = model.chineseName
        
        self.nameEN = model.englishName
        
        self.indexLetter = model.firstLetter
        
        self.name = model.displayName
    }
}

struct ProductTypeItem {
    var id:String
    
    var name:String
    
    var imageUrl:URL?
    
    var level:Int
    
    var parentId:String?
    
    init(model:API.ProductType) {
        id = model.productTypeId
        
        name = model.productTypeName
        
        imageUrl = model.pic
        
        level = model.layers
        
        parentId = model.fatherId
    }
}

enum ProductListVMType {
    case keyword(String)
    case brand(String)
    case type(ProductTypeItem)
    
    var showBrand:Bool {
        switch self {
        case .brand(_):
            return true
        default:
            return false
        }
    }
    
    func requestWithPage(_ page:Int, dict:[String:Any]) -> Observable<APIItem<API.ProductCollection>> {
        let sort = dict["sort"] as? String ?? "2"
        
        switch self {
        case .type(let currentType):
            
            var typeId = currentType.id
            
            let layer = currentType.level
            
            if let pductTypeId = dict["subPductTypeId"] as? String {
                typeId = pductTypeId
                
                return DictionaryManager
                    .shareInstance
                    .getType(by: pductTypeId)
                    .map({ (item) -> Observable<APIItem<API.ProductCollection>> in
                    guard let `item` = item else {
                        return Observable.just(.failed(error:"无效的搜索条件"))
                    }
                    
                    return API.ProductCollection.search(brandId: dict["brandId"] as? String,
                                                        pductTypeId:typeId,
                                                        level: item.layers,
                                                        sort:sort,
                                                        page: page)
                })
                    .startWith(Observable.just(.validating))
                    .switchLatest()
            } else if let pductTypeId = dict["pductTypeId"] as? String {
                typeId = pductTypeId
                
                return DictionaryManager
                    .shareInstance
                    .getType(by: pductTypeId)
                    .map({ (item) -> Observable<APIItem<API.ProductCollection>> in
                        guard let `item` = item else {
                            return Observable.just(.failed(error:"无效的搜索条件"))
                        }
                        
                        return API.ProductCollection.search(brandId: dict["brandId"] as? String,
                                                            pductTypeId:typeId,
                                                            level: item.layers,
                                                            sort:sort,
                                                            page: page)
                    })
                    .startWith(Observable.just(.validating))
                    .switchLatest()
            }
            
            return API.ProductCollection.search(brandId: dict["brandId"] as? String,
                                                pductTypeId:typeId,
                                                level: layer,
                                                sort:sort,
                                                page: page)
        case .keyword(let keyword):
            let brandId = dict["brandId"] as? String
            
//            if let pductTypeId = dict["subPductTypeId"] as? String {
//                typeId = pductTypeId
//                
//                return DictionaryManager
//                    .shareInstance
//                    .getType(by: pductTypeId)
//                    .map({ (item) -> Observable<APIItem<API.ProductCollection>> in
//                        guard let `item` = item else {
//                            return Observable.just(.failed(error:"无效的搜索条件"))
//                        }
//                        
//                        return API.ProductCollection.search(brandId: dict["brandId"] as? String,
//                                                            keyWord: keyword,
//                                                            pductTypeId:typeId,
//                                                            level: item.layers,
//                                                            sort:sort,
//                                                            page: page)
//                    })
//                    .startWith(Observable.just(.validating))
//                    .switchLatest()
//            }
            
            return API.ProductCollection.search(brandId: brandId,
                                                keyWord: keyword,
                                                sort:sort,
                                                page: page)
        case .brand(let brandId):
            
            let useProType = dict["useProType"] as? Bool ?? true
            
            if let pductTypeId = dict["pductTypeId"] as? String {
                
                return DictionaryManager
                    .shareInstance
                    .getType(by: pductTypeId)
                    .map({ (item) -> Observable<APIItem<API.ProductCollection>> in
                        guard let `item` = item else {
                            return Observable.just(.failed(error:"无效的搜索条件"))
                        }
                        
                        return API.ProductCollection.search(brandId: brandId,
                                                            pductTypeId:pductTypeId,
                                                            level: item.layers,
                                                            sort:sort,
                                                            useProType: useProType,
                                                            page: page)
                    })
                    .startWith(Observable.just(.validating))
                    .switchLatest()
            }
            
            return API.ProductCollection.search(brandId: brandId,
                                                sort:sort,
                                                useProType: useProType,
                                                page: page)
        }
    }
    
    var items:[ProductFilterItemType] {
        var array = [ProductFilterItemSort.sort]
        
        switch self {
        case .brand(let brandId):
            array.append(BrandFilterType(brandId: brandId))
        case .type(let currentType):
            if currentType.level > 1,
                let category = ProductFilterItemSort.category(currentType) {
                array.insert(category, at: 0)
            }
            
            array.append(CategoryFilterType(currentType: currentType))
        case .keyword(let string):
            array.append(CategoryKeywordType(keyword: string))
        }
        
        return array
    }
    
}

class ProductListVM: ViewModel {
    
    let type:ProductListVMType
    
    var value = Variable<[String:Any]>([:])
    
    init(keyword:String) {
        type = .keyword(keyword)
    }
    
    init(brandId:String) {
        type = .brand(brandId)
    }
    
    init(productType:ProductTypeItem) {
        type = .type(productType)
    }
    
    func bind(to view: ProductListVC) -> DisposeBag? {
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .white)
        
        let type = self.type
        
        let filterItems = type.items
        
        let value = self.value
        
        let typeFilter = filterItems.map({ $0 as? CategoryFilterType }).filter({ $0 != nil }).map({ $0! }).first
        
        let brotherFilter = filterItems.map({ $0 as? ProductFilterCategorySort }).filter({ $0 != nil }).map({ $0! }).first
        
        let bag = dataSet.refrence(getRequestWithPage: {
            [weak view]
            
            (page) -> Observable<APIItem<API.ProductCollection>> in
            
            var dict = value.value
            
            if let strongView = view {
                if type.showBrand {
                    dict["useProType"] = (strongView.brandItem == nil)
                }
            }
            
            return type.requestWithPage(page, dict:dict)
        },
                                   otherRequest: value.asObservable().skip(1).map({ _ -> Void in () }),
                                   listCallBack: { (list) in
                                    view.productList = list.map({ ProductItem(model: $0) })
        }) { (otherInfo) in
            guard let brand = otherInfo as? API.ProductBrand else {
                return
            }
            
            if !type.showBrand {
                return
            }
            
            let item = ProductBrandItem(model: brand)
            
            view.brandItem = item
        }
        
        Observable<[String:Any]>.create({
            [weak view] (observer) -> Disposable in
            let sync = {
                var dict = [String:Any]()
                
                for one in filterItems {
                    if let value = one.dataValue as? String {
                        dict[one.key] = value
                    } else if let value = one.dataValue as? Int {
                        dict[one.key] = "\(value)"
                    } else if let value = one.dataValue as? [String:Any] {
                        for (k, v) in value {
                            dict[k] = v
                        }
                    }
                }
                
                if let categoryFilterItem = typeFilter,
                    let brotherFilterItem = brotherFilter,
                    let pductTypeId = dict["pductTypeId"] as? String,
                    categoryFilterItem.currentType.id != pductTypeId,
                    let thisType = brotherFilterItem.dict[pductTypeId] {
                    
                    categoryFilterItem.currentType = thisType
                    
                    dict["subPductTypeId"] = nil
                    dict["brandId"] = nil
                    
                    view?.title = thisType.name
                }
                
#if DEBUG
                print("dict is \(dict)")
#endif
                observer.onNext(dict)
            }
            
            for one in filterItems {
                one.syncHandlers += [sync]
            }
            
            return Disposables.create()
        })
            .bind(to: value)
            .addDisposableTo(bag)
        
        view.dropDownMenu.items = filterItems
        view.dropDownMenu.reload()
        
        if let `brotherFilter` = brotherFilter {
            brotherFilter.sync = {
                [weak view] in
                
                DispatchQueue.main.async {
                    view?.dropDownMenu.reload()
                }
                
            }
        }
        
        return bag
    }
}
