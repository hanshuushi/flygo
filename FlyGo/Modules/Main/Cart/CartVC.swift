//
//  CartVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/8.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class CartVC: UIViewController, PresenterType {
    
    let tableView:CartTableView = CartTableView(frame: .zero,
                                                    style: .plain)
    
    let items:Variable<[SelectableItem<CartItem>]> = Variable([])

    var _items:[SelectableItem<CartItem>] = [] {
        didSet{
            items.value = _items
        }
    }
    
    let showFooter:Variable<Bool> = Variable(false)
    
    let disposeBag = DisposeBag()
    
    let selectedCarts:Observable<[CartItem]>
    
    let deletedCarts:Observable<[CartItem]>
    
    let deletedCart:PublishSubject<String> = PublishSubject()
    
    let changeCarts:PublishSubject<[CartItem]> = PublishSubject()
    
    let detailSelected:PublishSubject<(id:String, title:String)> = PublishSubject()
    
    let restoreHandler = PublishSubject<Void>()
    
    let confirmButton:UIButton = ViewFactory.redButton(title: "立刻结算")
    
    let deleteButton:UIButton = ViewFactory.redButton(title: "删除")
    
    let viewModel:CartVM = CartVM()
    
    var bindDisposeBag:DisposeBag?
    
    var editorButtonItem:UIBarButtonItem?
    
    var isChanged:Bool = false
    
    override var isEditing: Bool {
        set {
            super.isEditing = newValue
        }
        get {
            return super.isEditing
        }
    }
    
//    #if DEBUG
    deinit {
        print("Kill Cart")
    }
//    #endif
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        let cells = self.tableView.visibleCells
        
        for cell in cells {
            cell.setEditing(editing, animated: animated)
        }
        
        
        if animated {
            let tration = CATransition()
            
            tration.duration = editing ? 0.125 : 0.25
            tration.type = "kCATransitionFade"
            
            deleteButton.superview!.layer.add(tration, forKey: "tration")
        }
        
        deleteButton.isHidden = !editing
        
        confirmButton.isHidden = editing
        
        sunLabel.isHidden = editing
        
        if editing {
            countUpDictionary = [:]
            
            for one in _items {
                countUpDictionary[one.item.id] = one.item.number
            }
        } else {
            countUpDictionary = [:]
        }
        
//        self.tableView.reloadData()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let selectedItem = items
            .asObservable()
            .map { array in array
                .filter{ $0.isSelected && $0.item.isValid }
                .map{ $0.item }
            }
            .shareReplay(1)
        
        selectedCarts = confirmButton
            .rx
            .tap
            .withLatestFrom(selectedItem)
        
        deletedCarts = deleteButton
            .rx
            .tap
            .withLatestFrom(selectedItem)
        
        selectedItem
            .map{$0.count}
            .map{$0>0}
            .bind(to: confirmButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        super.init(nibName: nil,
                   bundle: nil)
        
        tableView.loginEvent.bind {
            LoginVC.doActionIfNeedLogin {
            }
        }.addDisposableTo(disposeBag)
    }
    
    var showToolBar:Bool = true
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let footView = UIView()
    
    let sunLabel = UILabel()
    
    var countUpDictionary = [String:Int]()
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.isEditing {
            restoreHandler.onNext()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "购物车"
        
        /// Table View
        
        self.view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        tableView.registerCell(cellClass: TableViewCell.self)
//        tableView.registerCell(cellClass: EditorTableViewCell.self)
        tableView.setInset(top: 64, bottom: (showToolBar ? 100 : 50))
        tableView.rowHeight = 135
        tableView.sectionFooterHeight = 10
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.delegate = self
        tableView.dataSource = self
        
        weak var weakSelf = self
        
        /// Add Foot View
        showFooter.asDriver().map({ !$0 }).drive(footView.rx.isHidden).addDisposableTo(disposeBag)
        
        footView.backgroundColor = UIColor.clear
        
        self.view.addSubview(footView)
        
        footView.snp.remakeConstraints { (maker) in
            maker.left.right.equalTo(self.view)
            maker.height.equalTo(50.0)
            maker.bottom.equalTo(self.view).offset(showToolBar ? -49 : 0)
        }
        
        /// Add Button
        footView.addSubview(confirmButton)
        
        confirmButton.snp.makeConstraints { (maker) in
            maker.right.top.bottom.equalTo(footView)
            maker.width.equalTo(100)
        }
        
        footView.addSubview(deleteButton)
        
        deleteButton.snp.makeConstraints { (maker) in
            maker.right.top.bottom.equalTo(footView)
            maker.width.equalTo(100)
        }
        
        /// Add Padding 
        let paddingView = UIView()
        
        footView.addSubview(paddingView)
        
        paddingView.backgroundColor = UIConfig.generalColor.white.withAlphaComponent(0.95)
        paddingView.snp.makeConstraints { (maker) in
            maker.left.top.bottom.equalTo(footView)
            maker.right.equalTo(confirmButton.snp.left)
        }
        
        /// Add All Selected Button
        let selButton = UIButton(type: .custom)
        
        footView.addSubview(selButton)
        
        selButton.setImage(UIImage(named:"icon_choice_normal"),
                           for: .normal)
        selButton.setImage(UIImage(named:"icon_choice_selected"),
                           for: .selected)
        selButton.sizeToFit()
        selButton.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(footView)
            maker.left.equalTo(footView).offset(15)
            maker.size.equalTo(selButton.size)
        }
        
        selButton.backgroundColor = UIColor.clear
        selButton.rx.tap.bind {
            if let strongSelf = weakSelf {
                let selected = !selButton.isSelected
                
                strongSelf._items = strongSelf._items.map({ one in
                    var item = one
                    
                    item.isSelected = selected;
                    
                    return item
                })
                strongSelf.tableView.reloadData()
            }
        }.addDisposableTo(disposeBag)
        
        items.asDriver().map { array in
            
            let validArray = array.filter{$0.item.isValid}
            
            return validArray.count > 0 && validArray.reduce(true, { $0 && $1.isSelected })
        }.drive(selButton.rx.isSelected).addDisposableTo(disposeBag)
        
        /// Add Label
        
        //// Select All
        let selectAllLabel = UILabel()
        
        footView.addSubview(selectAllLabel)
        
        selectAllLabel.text = "全选"
        selectAllLabel.font = UIConfig.generalFont(15)
        selectAllLabel.textColor = UIConfig.generalColor.selected
        selectAllLabel.backgroundColor = UIColor.clear
        selectAllLabel.sizeToFit()
        selectAllLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(selButton)
            maker.left.equalTo(selButton.snp.right).offset(5)
            maker.size.equalTo(selectAllLabel.size)
        }
        
        //// Sum
        
        footView.addSubview(sunLabel)
        
        sunLabel.adjustsFontSizeToFitWidth = true
        sunLabel.text = ""
        sunLabel.backgroundColor = UIColor.clear
        sunLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(footView)
            maker.right.equalTo(confirmButton.snp.left).offset(-8)
        }
        sunLabel.numberOfLines = 2
        
        items
            .asObservable()
            .map { (items) -> NSAttributedString in
                
                let ps = NSMutableParagraphStyle()
                
                ps.lineSpacing = 2
                ps.alignment = .right
                
                let attr = NSMutableAttributedString(string: "合计：", attributes: [NSParagraphStyleAttributeName:ps,
                                                                                    NSFontAttributeName:UIConfig.generalSemiboldFont(12),
                                                                                    NSForegroundColorAttributeName:UIConfig.generalColor.selected])
                
                attr.append(NSAttributedString(string: "￥", attributes: [NSParagraphStyleAttributeName:ps,
                                                                           NSFontAttributeName:UIConfig.generalSemiboldFont(11),
                                                                           NSForegroundColorAttributeName:UIConfig.generalColor.red]))
                
                let sum = items
                    .filter{$0.isSelected && $0.item.isValid}
                    .reduce(0){ $0 + ($1.item.totalPrice)}
                
                let tip = items
                    .filter{$0.isSelected && $0.item.isValid}
                    .reduce(0){ $0 + ($1.item.tipPrice)}
                
                let sumIntValue = Int(sum)
                
                let middle = "\(sumIntValue)"
                
                attr.append(NSAttributedString(string: middle,
                                               attributes: [NSParagraphStyleAttributeName:ps,
                                                            NSFontAttributeName:UIConfig.generalSemiboldFont(15),
                                                            NSForegroundColorAttributeName:UIConfig.generalColor.red]))
                
                let tail = ((sum - Float(sumIntValue)).priceString as NSString).substring(from: 1)
                
                attr.append(NSAttributedString(string: tail,
                                               attributes: [NSParagraphStyleAttributeName:ps,
                                                            NSFontAttributeName:UIConfig.generalSemiboldFont(11),
                                                            NSForegroundColorAttributeName:UIConfig.generalColor.red]))
                
                attr.append(NSAttributedString(string: "\n含服务费(￥\(tip.priceString))",
                                               attributes: [NSParagraphStyleAttributeName:ps,
                                                            NSFontAttributeName:UIConfig.generalSemiboldFont(10),
                                                            NSForegroundColorAttributeName:UIConfig.generalColor.unselected]))
                
                return attr
            }
            .bind(to: sunLabel.rx.attributedText)
            .addDisposableTo(disposeBag)
        
        let line = UIView()
        
        footView.addSubview(line)
        
        line.backgroundColor = UIConfig.generalColor.lineColor
        line.snp.makeConstraints { (maker) in
            maker.left.top.right.equalTo(footView)
            maker.height.equalTo(0.5)
        }
        
        self.setEditing(false, animated: false)
        
        bind()
        //addFakeShadow()
    }
    
    func delete (cell:UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        deletedCart.onNext(_items[indexPath.row].item.id)
        
        _items.remove(at: indexPath.row)
        
        tableView.reloadData()
    }
    
    func change (number:Int, of cell:UITableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        var thisItem = _items[indexPath.row].item
        
        thisItem.number = number
        
        _items[indexPath.row].item = thisItem
    }
}

extension CartVC: EditorRootViewController {
    func switchEditorStatus(sender: UIBarButtonItem) {
        
        if _items.count < 1 {
            self.showToast(text: "购物车已经没有商品可供编辑了。")
            
            return
        }
        
        if self.isEditing {
            
            for one in _items {
                guard let number = countUpDictionary[one.item.id], number == one.item.number else {
                    
                    changeCarts.onNext(_items.map({ $0.item }))
                    
                    return
                }
            }
            
            self.setEditing(!self.isEditing,
                            animated: true)
            
            sender.title = "编辑"
            
            return
        }
        
        sender.title = "完成"
        
        self.setEditing(!self.isEditing,
                        animated: true)
    }
}

extension CartVC {
    class CartTableView: UITableView {
        
        let loginEvent:PublishSubject<Void> = PublishSubject()
        
        var timer:Timer?
        
        let asyncDisposeBag = DisposeBag()
        
        deinit {
            if let currentTimer = timer {
                currentTimer.invalidate()
            }
        }
        
        override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
            
            for one in self.visibleCells {
                if let cell = one as? TableViewCell, cell.panProgress > 0.0, !cell.isEditing {
                    
                    let p = self.convert(point, to: cell)
                    
                    if cell.bounds.contains(p) {
                        return cell.hitTest(p, with: event)
                    }
                    
                    if let currentTimer = timer {
                        currentTimer.invalidate()
                    }
                    
                    timer = Timer.scheduledTimer(timeInterval: 0.1,
                                                 target: cell,
                                                 selector: #selector(TableViewCell.endEdit),
                                                 userInfo: nil,
                                                 repeats: false)
                    
                    return nil
                }
            }
            
            return super.hitTest(point, with: event)
        }
        
        func createEmptyView() -> UIView {
            let view = UIView()
            
            view.backgroundColor = UIConfig.generalColor.white
            
            if UserManager.shareInstance.isLogined() {
                let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                              textColor: UIConfig.generalColor.whiteGray)
                
                label.text = "快去选几件东西吧~"
                
                view.addSubview(label)
                
                let imageView = UIImageView(named: "icon_nothing_in_cart")
                
                view.addSubview(imageView)
                
                imageView.snp.makeConstraints { (maker) in
                    maker.top.equalTo(view).offset(100)
                    maker.centerX.equalTo(view)
                }
                
                label.snp.makeConstraints { (maker) in
                    maker.top.equalTo(imageView.snp.bottom).offset(30)
                    maker.centerX.equalTo(view)
                    maker.bottom.bottom.equalTo(view).offset(0)
                }
            } else {
                let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                              textColor: UIConfig.generalColor.whiteGray)
                
                label.text = "登录了看看里面都有啥~"
                
                view.addSubview(label)
                
                let imageView = UIImageView(named: "icon_nothing_in_cart")
                
                view.addSubview(imageView)
                
                imageView.snp.makeConstraints { (maker) in
                    maker.top.equalTo(view).offset(70)
                    maker.centerX.equalTo(view)
                }
                
                label.snp.makeConstraints { (maker) in
                    maker.top.equalTo(imageView.snp.bottom).offset(30)
                    maker.centerX.equalTo(view)
                }
                
                let button = ViewFactory.redButton(title: "立刻登录")
                
                button.layer.cornerRadius = 22
                button.layer.masksToBounds = true
                button.rx
                    .tap
                    .throttle(1.0, scheduler: MainScheduler.instance)
                    .bindNext(loginEvent).addDisposableTo(asyncDisposeBag)
                
                view.addSubview(button)
                
                button.snp.makeConstraints { (maker) in
                    maker.centerX.equalTo(view)
                    maker.top.equalTo(label.snp.bottom).offset(20)
                    maker.size.equalTo(CGSize(width:200, height:44))
                    maker.bottom.bottom.equalTo(view).offset(0)
                }
            }
            
            return view
        }
    }
}

extension CartVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = _items[indexPath.row]
        
//        if self.isEditing {
//            let cell:EditorTableViewCell = tableView.dequeueReusableCell(at: indexPath)
//            
//            
//            cell.setItem(item: item)
////            cell.parentVC = self
//            
//            return cell
//            
//        }
        
        let cell:TableViewCell = tableView.dequeueReusableCell(at: indexPath)
        
        cell.setItem(item: item)
        cell.parentVC = self
        cell.isEditing = self.isEditing
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _items[indexPath.row].isSelected = !_items[indexPath.row].isSelected
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

/*
extension CartVC {
    class EditorTableViewCell: UITableViewCell {
        
        let selectImageView = UIImageView(image: UIImage(named:"icon_choice_normal"),
                                          highlightedImage: UIImage(named:"icon_choice_selected"))
        
        let coverImageView = UIImageView()
        
        let titleLabel = UILabel()
        
        let storeLabel = UILabel()
        
        let numLabel = UILabel()
        
        let reduceButton = ViewFactory.button(imageNamed: "icon_minus_available")
        
        let addButton = ViewFactory.button(imageNamed: "icon_minus_available",
                                           disabledImageNamed: "icon_minus_not_available")
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(selectImageView)
            
            selectImageView.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.left.equalTo(self.contentView).offset(20)
                maker.size.equalTo(selectImageView.image!.size)
            }
            
            // add cover
            self.contentView.addSubview(coverImageView)
            
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.selectImageView.snp.right)
                maker.centerY.equalTo(self.contentView)
                maker.size.equalTo(CGSize(width:115, height:95))
            }
            
            // add rmb
            self.contentView.addSubview(titleLabel)
            
            titleLabel.textColor = UIConfig.generalColor.whiteGray
            titleLabel.font = UIConfig.arialFont(13)
            titleLabel.backgroundColor = UIConfig.generalColor.backgroudWhite
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.top.equalTo(coverImageView).offset(15)
            }
            
            self.contentView.addSubview(storeLabel)
            
            storeLabel.textColor = UIConfig.generalColor.whiteGray
            storeLabel.font = UIConfig.arialFont(13)
            storeLabel.backgroundColor = UIConfig.generalColor.backgroudWhite
            storeLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.top.equalTo(titleLabel.snp.bottom)
                maker.right.equalTo(titleLabel)
            }
            
            // add backgroud
            let backgroudView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            self.contentView.insertSubview(backgroudView, belowSubview: titleLabel)
            
            backgroudView.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel).offset(-10)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.left.equalTo(titleLabel).offset(-5)
                maker.bottom.equalTo(storeLabel).offset(10)
            }
            
            let pullDownImageView = UIImageView(named: "icon_pulldown_normal")
            
            self.contentView.addSubview(pullDownImageView)
            
            pullDownImageView.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(backgroudView)
                maker.right.equalTo(backgroudView).offset(-5)
                maker.left.equalTo(titleLabel.snp.right).offset(5)
                maker.size.equalTo(pullDownImageView.size)
            }
            
            // add num
            self.contentView.addSubviews(reduceButton, numLabel, addButton)
            
            addButton.snp.makeConstraints { (maker) in
                maker.right.equalTo(backgroudView)
                maker.bottom.equalTo(coverImageView).offset(5)
            }
            
            numLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(addButton.snp.left).offset(-45)
                maker.centerY.equalTo(addButton)
            }
            
            reduceButton.snp.makeConstraints { (maker) in
                maker.right.equalTo(numLabel.snp.left).offset(-45)
                maker.centerY.equalTo(addButton)
            }
            
//            numLabel.textColor = UIConfig.generalColor.selected
//            numLabel.backgroundColor = UIConfig.generalColor.white
//            numLabel.font = UIConfig.arialFont(13)
//            numLabel.snp.makeConstraints { (maker) in
//                maker.right.equalTo(titleLabel)
//                maker.centerY.equalTo(priceLabel)
//            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var currentItem:CartItem?
        
        func setItem(item:SelectableItem<CartItem>) {
            currentItem = item.item
            
            selectImageView.isHighlighted = item.isSelected
            selectImageView.isHidden = !item.item.isValid
            
            coverImageView.kf.setImage(with:item.item.imageUrl)
            
            titleLabel.text = "规格：\(item.item.title)"
            
            storeLabel.text = "渠道：\(item.item.title)"
            
            numLabel.text = "\(item.item.number)"
        }

    }
}*/

extension CartVC {
    
    fileprivate class RedBlockView: UIView {
        
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        
        override func layoutSubviews() {
            
            super.layoutSubviews()
            
            if gradientLayer.superlayer != self.layer {
                self.layer.insertSublayer(gradientLayer, at: 0)
            }
            
            if gradientLayer.frame == self.bounds {
                return
            }
            
            CATransaction.begin()
            CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
            
            gradientLayer.frame = self.bounds
            gradientLayer.locations = [0, 1]
            gradientLayer.colors = [UIColor(r: 247, g: 107, b: 98).cgColor, UIColor(r: 253, g: 60, b: 83).cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.676, y: 1.0)
            
            CATransaction.commit()
        }
    }
    
    class TableViewCell: UITableViewCell, UITextFieldDelegate {
        
        let selectImageView = UIImageView(image: UIImage(named:"icon_choice_normal"),
                                          highlightedImage: UIImage(named:"icon_choice_selected"))
        
        let unvalidLabel = UILabel()
        
        let coverImageView = UIImageView()
        
        let titleLabel = UILabel()
        
        let subTitle = UILabel()
        
        let priceLabel = UILabel()
        
        let numLabel = UILabel()
        
        let deleteButton = UIButton(type: .custom)
        
        var distance:CGFloat = 0
        
        let modelView = UIView()
        
        let numTextField = UITextField()
        
        let reduceButton = ViewFactory.button(imageNamed: "icon_minus_available")
        
        let addButton = ViewFactory.button(imageNamed: "icon_plus_available",
                                           disabledImageNamed: "icon_plus_not_available")
        
        weak var cachedParentTable:UITableView?
        
        override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
            if self.panProgress > 0.0, !self.isEditing {
                let views = [deleteButton]
                
                for one in views {
                    let p = self.convert(point, to: one)
                    
                    if one.bounds.contains(p) {
                        return one.hitTest(p, with: event)
                    }
                }
            }
            
            return super.hitTest(point, with: event)
        }
        
        var parentTable:UITableView? {
            
            if let cachedParentTable = self.cachedParentTable {
                return cachedParentTable
            }
            
            var _superview:UIView? = self.superview
            
            while let superview = _superview {
                if let tableView = superview as? UITableView {
                    self.cachedParentTable = tableView
                    
                    return tableView
                }
                
                _superview = superview.superview
            }
            
            return nil
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            distance = self.view.bounds.width - 5
            
            let panProgress = self.panProgress
            
            self.panProgress = panProgress
        }
        
        override func prepareForReuse() {
            super.prepareForReuse()
            
            timer?.invalidate()
            timer = nil
            
            isPanning = false
            
            self.panProgress = 0.0
        }
        
        var _panProgress:CGFloat = 0.0
        
        var panProgress:CGFloat {
            set {
                _panProgress = newValue
                
                let offset = -panProgress * distance
                
                modelView.transform = CGAffineTransform(translationX: offset - 5,
                                                               y: 0)
                
                let buttonLeft = distance + offset
                
                let buttonRight = distance + 20
                
                let width = buttonRight - buttonLeft
                
                deleteButton.superview!.frame = CGRect(x: buttonLeft,
                                            y: 1,
                                            w: width,
                                            h: self.view.bounds.height - 2)
                deleteButton.frame = deleteButton.superview!.bounds
                deleteButton.alpha = panProgress <= 0 ? 0 : panProgress / 0.2
            }
            
            get {
                return _panProgress
            }
        }
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(modelView)
            
            modelView.view.backgroundColor = UIConfig.generalColor.white
            modelView.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsets.zero)
            }
            
            modelView.addSubview(selectImageView)
            
            selectImageView.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(modelView)
                maker.left.equalTo(modelView).offset(20)
                maker.size.equalTo(selectImageView.image!.size)
            }
            
            modelView.addSubview(unvalidLabel)
            
            unvalidLabel.text = "失效"
            unvalidLabel.textColor = UIConfig.generalColor.white
            unvalidLabel.backgroundColor = UIConfig.generalColor.whiteGray
            unvalidLabel.font = UIConfig.generalFont(11)
            unvalidLabel.textAlignment = .center
            unvalidLabel.layer.cornerRadius = 11
            unvalidLabel.layer.masksToBounds = true
            unvalidLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(selectImageView)
                maker.centerY.equalTo(selectImageView)
                maker.size.equalTo(CGSize(width:32, height:22))
            }
            
            // add cover
            modelView.addSubview(coverImageView)
            
            coverImageView.addTapGesture(target: self,
                                         action: #selector(TableViewCell.toDetail))
            coverImageView.isUserInteractionEnabled = true
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.selectImageView.snp.right)
                maker.centerY.equalTo(modelView)
                maker.size.equalTo(CGSize(width:115, height:95))
            }
            
            // add title
            modelView.addSubview(titleLabel)
            
            titleLabel.font = UIConfig.generalFont(13)
            titleLabel.numberOfLines = 2
            titleLabel.backgroundColor = coverImageView.backgroundColor
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(coverImageView.snp.right).offset(15)
                maker.top.equalTo(coverImageView)
                maker.right.equalTo(modelView).offset(-10)
            }
            
            // add subtitle
            modelView.addSubview(subTitle)
            
//            subTitle.textColor = UIConfig.generalColor.titleColor
//            subTitle.font = UIConfig.generalFont(12)
            subTitle.backgroundColor = titleLabel.backgroundColor
            subTitle.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.right.equalTo(titleLabel)
                maker.top.equalTo(titleLabel.snp.bottom).offset(7)
            }
            
            // add price
            modelView.addSubview(priceLabel)
            
            priceLabel.font = UIConfig.generalFont(13)
            priceLabel.textColor = UIConfig.generalColor.red
            priceLabel.backgroundColor = coverImageView.backgroundColor
            priceLabel.numberOfLines = 2
            priceLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.top.equalTo(subTitle.snp.bottom).offset(7)
            }
            
            
            // add num
            modelView.addSubview(numLabel)
            
            numLabel.textColor = UIConfig.generalColor.selected
            numLabel.backgroundColor = UIConfig.generalColor.white
            numLabel.font = UIConfig.arialFont(13)
            numLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(titleLabel)
                maker.top.equalTo(priceLabel)
            }
            
            // add delete
            let deleteBlock = RedBlockView()
            
            deleteBlock.addSubview(deleteButton)
            
            self.contentView.insertSubview(deleteBlock,
                                           belowSubview: modelView)
            
            deleteButton.backgroundColor = .clear
            deleteButton.setImage(UIImage(named:"icon_delete"),
                                  for: .normal)
            deleteButton.addTarget(self,
                                   action: #selector(TableViewCell.deleteHandler),
                                   for: .touchUpInside)
            deleteButton.imageEdgeInsets = UIEdgeInsetsMake(0,
                                                            0,
                                                            22.5,
                                                            10)
            
            let label = UILabel()
            
            label.backgroundColor = .clear
            label.attributedText = NSAttributedString(string: "删除",
                                                      font: UIConfig.generalSemiboldFont(15),
                                                      textColor: UIConfig.generalColor.white)
            
            deleteButton.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(deleteButton).offset(-5)
                maker.centerY.equalTo(deleteButton).offset(10)
            }
            
            let panGesture = UIPanGestureRecognizer(target: self,
                                                    action: #selector(TableViewCell.pan(gesture:)))
            
            panGesture.delegate = self
            
            self.contentView.addGestureRecognizer(panGesture)
            
            // add edit block
            modelView.addSubviews(numTextField, reduceButton, addButton)
            
        
            addButton.addTarget(self,
                                action: #selector(TableViewCell.addPressed(sender:)),
                                for: UIControlEvents.touchUpInside)
            addButton.snp.makeConstraints { (maker) in
                maker.right.equalTo(titleLabel)
                maker.bottom.equalTo(coverImageView).offset(5)
                maker.size.equalTo(addButton.size)
            }
            
            numTextField.font = UIConfig.generalFont(15)
            numTextField.textColor = UIConfig.generalColor.titleColor
            numTextField.textAlignment = .center
            numTextField.delegate = self
            numTextField.keyboardType = .numberPad
            numTextField.isEnabled = false
            numTextField.snp.makeConstraints { (maker) in
                maker.right.equalTo(addButton.snp.left)
                maker.left.equalTo(reduceButton.snp.right)
                maker.top.bottom.centerY.equalTo(addButton)
            }
            
            reduceButton.addTarget(self,
                                   action: #selector(TableViewCell.reducePressed(sender:)),
                                   for: UIControlEvents.touchUpInside)
            reduceButton.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.centerY.equalTo(addButton)
                maker.size.equalTo(reduceButton.size)
            }
            
            self.panProgress = 0.0
        }
        
        var isPanning = false
        
        var targetProgress:CGFloat = 0.0
        
        var animationStartTime:CFTimeInterval?
        
        var startProgress:CGFloat = 0.0
        
        func pan(gesture:UIPanGestureRecognizer) {
            
            let current = gesture.translation(in: self)
            
            switch gesture.state {
            case .began:
                
                timer?.invalidate()
                timer = nil
                
                isPanning = true
                
            case .cancelled, .ended, .failed:
                timer?.invalidate()
                timer = nil
                
                isPanning = false
                
                let velocity = gesture.velocity(in: self).x
                
                let inertiaThreshold:CGFloat = 100.0
                
                if velocity > inertiaThreshold {
                    targetProgress = 0.0
                } else if velocity < -inertiaThreshold {
                    targetProgress = 0.2
                } else {
                    targetProgress = self.panProgress < 0.1 ? 0.0 : 0.2
                }
                
                if targetProgress != self.panProgress {
                    
                    startProgress = self.panProgress
                    
                    animationStartTime = nil
                    
                    timer = CADisplayLink(target: self,
                                          selector: #selector(TableViewCell.animationTick))
                    
                    timer!.add(to: .main,
                               forMode: .commonModes)
                }
                
                return
            default:
                break
            }
            
            let changeRate = current.x / self.size.width
            
            let newProgress = self.panProgress - changeRate
            
            if newProgress < 0.0 {
                self.panProgress = 0.0
            } else if newProgress > 0.4 {
                let rate = max(0, 1.0 - newProgress)
                
                self.panProgress -= rate * changeRate
            } else {
                self.panProgress = newProgress
            }
            
            gesture.setTranslation(.zero,
                                   in: self)
        }
        
        func animationTick() {
            guard let currentTimer = timer else {
                return
            }
            
            if animationStartTime == nil {
                animationStartTime = currentTimer.timestamp
            }
            
            let elapsed = currentTimer.timestamp - animationStartTime!
            
            self.panProgress = TableViewCell.value(of: CGFloat(elapsed),
                                                   duration: 0.25,
                                                   from: startProgress,
                                                   to: targetProgress)
            
            if elapsed >= 0.25 {
                currentTimer.invalidate()
                
                timer = nil
            }
        }
        
        static func value(of elapsed:CGFloat, duration:CGFloat, from:CGFloat, to:CGFloat) -> CGFloat {
            let t = min(1.0, elapsed / duration)
            
            if t == 1.0 {
                return to
            }
            
            return mgEaseOutQuad(t: t, b: from, c: to - from)
        }
        
        static func mgEaseOutQuad(t:CGFloat, b:CGFloat, c:CGFloat) -> CGFloat {
            return -c * t * (t-2) + b
        }
        
        var timer:CADisplayLink?
        
        override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
            guard let panGesture = gestureRecognizer as? UIPanGestureRecognizer else {
                return true
            }
            
            let translation = panGesture.translation(in: self)
            
            if fabs(translation.y) > fabs(translation.x) {
                
                if self.panProgress > 0.0 {
                    self.endEdit()
                }
                
                return false
            }
            
            if translation.x > 0 && self.panProgress <= 0.0 {
                return false
            }
            
            if isPanning {
                return false
            }
            
            return true
        }
        
        func toDetail() {
            if let item = currentItem {
                parentVC?.detailSelected.onNext((id:item.recId,
                                                 title:item.name))
            }
        }
        
        func endEdit() {
            if self.panProgress <= 0.0 {
                return
            }
            
            timer?.invalidate()
            timer = nil
            
            isPanning = false
            
            UIView.animate(withDuration: 0.25) { 
                self.panProgress = 0.0
            }
//            setEditing(false, animated: true)
        }
        
        weak var parentVC:CartVC? = nil
        
        var currentItem:CartItem?
        
        func setItem(item:SelectableItem<CartItem>) {
            currentItem = item.item
            
            selectImageView.isHighlighted = item.isSelected
            selectImageView.isHidden = !item.item.isValid
            
            unvalidLabel.isHidden = item.item.isValid
            
            coverImageView.kf.setImage(with:item.item.imageUrl)
            
            titleLabel.text = item.item.title
            
            if item.item.isValid {
                titleLabel.textColor = UIConfig.generalColor.selected
                subTitle.attributedText = NSAttributedString(string: "规格：" + item.item.subTitle,
                                                             font: UIConfig.generalFont(12),
                                                             textColor: UIConfig.generalColor.titleColor)
                
                numLabel.isHidden = false
                numLabel.attributedText = FlygoUtil.attributedString(number: item.item.number)
                
                numTextField.isHidden = false
                numTextField.text = "\(item.item.number)"
                
                addButton.isHidden = false
                
                reduceButton.isHidden = false
                
                priceLabel.isHidden = false
                
                coverImageView.isUserInteractionEnabled = true
            } else {
                titleLabel.textColor = UIConfig.generalColor.whiteGray
                subTitle.attributedText = NSAttributedString(string: item.item.state.description,
                                                             font: UIConfig.generalFont(12),
                                                             textColor: UIConfig.generalColor.whiteGray)
                
                numLabel.isHidden = true
                
                numTextField.isHidden = true
                
                addButton.isHidden = true
                
                reduceButton.isHidden = true
                
                priceLabel.isHidden = true
                
                coverImageView.isUserInteractionEnabled = false
            }
            
            /// price
            let priceAttributedString = NSMutableAttributedString(string: item.item.rmb.description,
                                                                  font: UIConfig.generalFont(13),
                                                                  textColor: UIConfig.generalColor.red)
            
            priceAttributedString.append(NSAttributedString(string: "\n服务费：￥\(item.item.tipPrice.priceString)",
                font: UIConfig.generalFont(10),
                textColor: UIConfig.generalColor.labelGray))
            
            priceLabel.attributedText = priceAttributedString
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func deleteHandler() {
            
            parentVC?.delete(cell: self)
            
            if let cartId = currentItem?.subId {
                
                parentVC?.deletedCart.onNext(cartId)
            }
        }
        
        override var isEditing: Bool {
            set {
                super.isEditing = newValue
                
                self.panProgress = newValue ? (-5 / distance) : 0
                
                priceLabel.alpha = !isEditing ? 1.0 : 0.0
                
                numLabel.alpha = !isEditing ? 1.0 : 0.0
                
                numTextField.alpha = isEditing ? 1.0 : 0.0
                
                reduceButton.alpha = isEditing ? 1.0 : 0.0
                
                addButton.alpha = isEditing ? 1.0 : 0.0
                
                let gestures = self.contentView.gestureRecognizers ?? []
                
                for gesture in gestures {
                    gesture.isEnabled = !isEditing
                }
            }
            get {
                return super.isEditing
            }
        }
        
        override func setEditing(_ editing: Bool, animated: Bool) {
            super.setEditing(editing, animated: animated)
            
            if !animated {
                return
            }
            
            UIView.animate(withDuration: editing ? 0.125 : 0.25,
                           delay: 0.0,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: {
                            self.isEditing = editing
            },
                           completion: nil)
        }
        
        func reducePressed(sender:Any?) {
            guard var item = currentItem else {
                return
            }
            
            if item.number <= 1 {
                
                let actionSheetVC = UIAlertController(title: nil,
                                                      message: "最后一件了，是否要删除该商品？",
                                                      preferredStyle: .actionSheet)
                
                actionSheetVC.addAction(UIAlertAction(title: "是",
                                                      style: .destructive,
                                                      handler: {
                                                        _ in
                                                        self.deleteHandler()
                }))
                actionSheetVC.addAction(UIAlertAction(title: "否",
                                                      style: .cancel,
                                                      handler: nil))
                
                parentVC?.present(actionSheetVC,
                                  animated: true,
                                  completion: nil)
                
                return
            }
            
            item.number -= 1
            
            currentItem = item
            
            numLabel.attributedText = FlygoUtil.attributedString(number: item.number)
            
            numTextField.text = "\(item.number)"
            
            parentVC?.change(number: item.number,
                             of: self)
        }
        
        func addPressed(sender:Any?) {
            guard var item = currentItem else {
                return
            }
            
            item.number += 1
            
            currentItem = item
            
            addButton.isEnabled = item.number < 999
            
            numLabel.attributedText = FlygoUtil.attributedString(number: item.number)
            
            numTextField.text = "\(item.number)"
            
            parentVC?.change(number: item.number,
                             of: self)
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            guard var item = currentItem else {
                return
            }
            
            guard let value = (textField.text ?? "").toInt(), value > 0, value <= 999 else {
                parentVC?.showToast(text: "商品数量必须大于0，小于1000个")
                
                numLabel.attributedText = FlygoUtil.attributedString(number: item.number)
                
                numTextField.text = "\(item.number)"
                
                addButton.isEnabled = item.number < 999
                
                return
            }
            
            item.number = value
            
            currentItem = item
            
            addButton.isEnabled = item.number < 999
            
            numLabel.attributedText = FlygoUtil.attributedString(number: item.number)
            
            numTextField.text = "\(item.number)"
            
            parentVC?.change(number: item.number,
                             of: self)
        }
    }
}
