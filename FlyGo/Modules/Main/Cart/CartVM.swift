//
//  CartVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import EZSwiftExtensions

extension NSNotification.Name {
    static var CartRefreshNotification:NSNotification.Name {
        return NSNotification.Name("CartRefreshNotification")
    }
}

enum CartState: Int {
    case normal        = 1
    case outStock      = 2
    case shelved       = 3
    
    var description:String {
        switch self{
        case .outStock:
            return "该商品暂时缺货"
        case .shelved:
            return "该商品已下架"
        default:
            return ""
        }
    }
}

struct CartItem {
    var name:String = ""
    
    var id:String {
        return self.subId
    }
    
    var imageUrl:URL?
    
    var price:Currency = Currency(unit: "$", value: 0)
    
    var rmb:Currency = Currency(unit: "￥", value: 0)
    
    var title:String { return name }
    
    var subTitle:String = ""
    
    var isValid:Bool {
        switch state {
        case .normal:
            return true
        default:
            return false
        }
    }
    
    var subId:String = ""
    
    var freeStoreId:String = ""
    
    var belongStoreId:String = ""
    
    var recId:String = ""
    
    var pdcId:String = ""
    
    var number:Int = 0
    
    var floatNumber:Float {
        return Float(number)
    }
    
    var tipPrice:Float = 0.0
    
    var deliver:Float = 0.0
    
    var sumPrice:Float {
        return floatNumber * rmb.value
    }
    
    var totalPrice:Float = 0.0
    
    var state:CartState = .normal
    
    init(model:API.Cart) {
        name = model.productName
        
        subTitle = model.productRecStandard
        
        imageUrl = model.pic
        
        price = model.sellPrice
        
        rmb = model.cnyPrice
        
        subId = model.cartSubId
        
        freeStoreId = model.selectedBelongStore
        
        belongStoreId = model.belongStoreId
        
        recId = model.productRecId
        
        pdcId = model.productId
        
        number = model.orderNumber
        
        tipPrice = model.serviceCharge
        
        deliver = model.shippingCharge
        
        totalPrice = model.sumPrice
        
        state = CartState.init(rawValue: model.state.rawValue) ?? .shelved
    }
    
    init(detailItem:ProductDetailItem, number:Int, model:API.CartPrice) {
        
        pdcId = detailItem.standars.first?.productId ?? ""
        
        name = detailItem.title
        
        subTitle = detailItem.standar
        
        imageUrl = detailItem.pics.filter({ $0 != nil }).map({ $0! }).first
        
//        let priceItem = detailItem.displayPrice
        
        price = detailItem.price
        
        rmb = detailItem.rmb
        
        belongStoreId = detailItem.belongStoreId
        
        recId = detailItem.recId
        
        self.number = number
        
        tipPrice = model.serviceCharge
        
        deliver = model.shippingCharge
        
        totalPrice = model.sumPrice
        
        state = .normal
    }
    
    mutating func fill(price:API.CartPrice) {
        self.tipPrice = price.serviceCharge
        
        self.totalPrice = price.sumPrice
        
        self.deliver = price.shippingCharge
    }
    
    var requestParams:API.CartPrice.SubmitCartItem {
        return (cartSubId:self.subId,
                belongStoreId:self.belongStoreId,
                productRecId:self.recId,
                selectedBelongStore:self.freeStoreId,
                productName:self.name,
                num:self.number,
                sellPrice:self.sumPrice)
    }
}


class CartVM: ViewModel {
    
    private let refreshEvent:PublishSubject<Void>
    
    static func refreshNotification() {
        NotificationCenter.default.post(name: .CartRefreshNotification,
                                        object: nil)
    }
    
    @objc
    func getRefresh(notification:Notification) {
        refreshEvent.onNext()
    }
    
    init () {
        refreshEvent = PublishSubject()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CartVM.getRefresh(notification:)),
                                               name: .CartRefreshNotification,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func bind(to view: CartVC) -> DisposeBag? {
        
        let bag = DisposeBag()
        
        weak var weakView = view
        
        let tableView = view.tableView
        
        let dataset = TableViewDataSet.init(view.tableView,
                                            delegate: view,
                                            dataSource: view,
                                            finishStyle: .none)
        
        
        dataset.emptyViewHandler = {
            tableView.createEmptyView()
        }
        
        let cartItemList = UserManager
            .shareInstance
            .isLogin
            .asObservable()
            .map {
                (isLogin) -> Observable<[CartItem]> in
                
                if !isLogin {
                    
                    dataset.status.value = .normal
                    weakView?._items = []
                    
                    return Observable.just([])
                }
                
                /// FirstRequest
                return Observable.create({ (obs) -> Disposable in
                    
                    let request = API.CartCollection.findCarts().shareReplay(1)
                    
                    let otherRefreshEvent = self.refreshEvent
                        .map({ API
                            .CartCollection
                            .findCarts()
                            .shareReplay(1) })
                        .shareReplay(1)
                    
                    let status = otherRefreshEvent
                        .startWith(request)
                        .switchLatest()
                        .asRequestStatusObservable()
                        .shareReplay(1)
                    
                    let d1 = status
                        .bind(to: dataset.status)
                    
                    
                
                    let refreshArray = dataset
                        .refreshAction
                        .map({ API
                            .CartCollection
                            .findCarts()
                            .asItemObservable()
                            .retry()
                            .shareReplay(1)
                        })
                    
                    let otherRefreshArray = otherRefreshEvent
                        .map({ $0.asModelObservable().shareReplay(1) })
                    
                    /// Refresh
                    let refreshRequest = Observable
                        .of(refreshArray, otherRefreshArray)
                        .merge()
                        .startWith(request
                            .asModelObservable())
                        .switchLatest()
                        .shareReplay(1)
                    
                    let list =  Observable.of(refreshRequest
                        .map({ $0
                            .cartList
                            .map({ CartItem(model:$0) })}),
                                              weakView!
                                                .changeCarts
                                                .flatMapLatest({ (submitItems) -> Observable<[CartItem]> in
                                                    Observable.create({ (obs) -> Disposable in
                                                        let toast = weakView!.showLoading()
                                                        
                                                        let session = API
                                                            .CartPrice
                                                            .getPrice(of: submitItems.map({ $0.requestParams }))
                                                        
                                                        session
                                                            .responseNoModel({
                                                                ez.runThisInMainThread {
                                                                    toast.dismiss(animated: true,
                                                                                  completion: {
                                                                                    CartVM.refreshNotification()
                                                                    })
                                                                }
                                                            },
                                                                             error: { (error) in
                                                                                ez.runThisInMainThread {
                                                                                    toast.displayLabel(text: error.description)
                                                                                }
                                                            })
                                                        
                                                        return session
                                                        
                                                    })
                                                })
                        )
                        .merge()
                        .shareReplay(1)
                    
                    let d2 = Observable.of(weakView!
                        .restoreHandler
                        .withLatestFrom(list),
                                           list)
                        .merge()
                        .map{$0.map{SelectableItem<CartItem>(item:$0)}}
                        .bind(onNext: { (items) in
                            weakView!._items = items
                            
                            if weakView!.isEditing {
                                weakView!.setEditing(false, animated: false)
                                
                                weakView!.editorButtonItem?.title = "编辑"
                                
                                weakView!.tableView.reloadData()
                            }
                        })
                    
                    let d3 = list.bind(to: obs)
                    
                    let d4 = weakView!
                        .deletedCarts
                        .bind { (items) in
                            let toast = weakView!.showLoading()
                            
                            let ids = items.map({ $0.id })
                            
                            API
                                .Cart
                                .delete(cardIds: ids)
                                .responseNoModel({
                                    ez.runThisInMainThread {
                                        toast.dismiss(animated: true,
                                                      completion: nil)
                                        
//                                        weakView!.items.value = weakView!._items.filter({ (item) -> Bool in
//                                            return !ids.contains(item.item.id)
//                                        })
                                        
                                        CartVM.refreshNotification()
                                    }
                                }, error: { (error) in
                                    ez.runThisInMainThread {
                                        toast.displayLabel(text: error.description)
                                    }
                                })
                    }
                    
                    
                    let d5 = Observable
                        .combineLatest(status,
                                       list,
                                       resultSelector: { (status, list) -> Bool in
                                        return list.count > 0 && status.rawValue == 1
                        })
                        .bind(to: weakView!.showFooter)
                    
                    let d6 = list
                        .map{$0.map{SelectableItem<CartItem>(item:$0)}}
                        .bind(onNext: { (items) in
                            weakView!._items = items
                        })
                    
                    let confirmVC = weakView!
                        .selectedCarts
                        .map({ (items) -> UIViewController in
                            let vm = CartConfirmVM(cartItems: items)
                            
                            return CartConfirmVC(viewModel: vm)
                        })
                    
                    let d7 = confirmVC.bind(onNext: { (vc) in
                        weakView!.navigationController?.pushViewController(vc,
                                                                            animated: true)
                    })
                    
                    return Disposables.create(d1,
                                              d2,
                                              d3,
                                              d4,
                                              d5,
                                              d6,
                                              d7)
 
                })
                    .shareReplay(1)
        }
            .switchLatest()
            .shareReplay(1)
        
        cartItemList.bind { (items) in
            print(items.count)
        }.addDisposableTo(bag)
        
        cartItemList
            .map({ $0.count <= 0 })
            .bind(to: dataset.isEmpty)
            .addDisposableTo(bag)

        view.deletedCart.subscribe(onNext: { (id) in
            API.Cart.delete(cartId: id).responseNoModel({
                ez.runThisInMainThread {
                    CartVM.refreshNotification()
                }
            }, error: { (error) in
                ez.runThisInMainThread {
                weakView?.showToast(text: error.description)
                }
            })
        }, onError: nil, onCompleted: nil, onDisposed: nil).addDisposableTo(bag)
        
        
        view.detailSelected.bind { (item) in
            let detaiVM = ProductDetailVM(productId: item.id)
            
            let detailVC = ProductDetailVC(viewModel: detaiVM, showCart:false)
            
            
            weakView?.navigationController?.pushViewController(detailVC,
                                                               animated: true)
        }.addDisposableTo(bag)
        
        return bag
        
    }
    
}
