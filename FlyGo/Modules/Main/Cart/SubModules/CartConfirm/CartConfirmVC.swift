//
//  CartConfirmVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CartConfirmVC: UIViewController, PresenterType {
    
    let selectAddressEvent:PublishSubject<Void>
    
    let viewModel:CartConfirmVM
    
    let selectedAddress:Variable<AddressItem?>
    
    let items:Variable<[CartConfirmItem]>
    
    let deliver:Variable<String>
    
    let charge:Variable<String>
    
    let sumPrice:PublishSubject<Float>
    
    let couponPickerEvent:PublishSubject<(Int, Float)>
    
    var bindDisposeBag:DisposeBag?
    
    let tableView:UITableView
    
    let confirmButton:UIButton
    
    let disposeBag:DisposeBag
    
    let sumLabel:UILabel
    
    let sumCell = UITableViewCell(style: .default,
                                  reuseIdentifier: "Sum")
    
    let priceSumLabel                        = {
        () -> UILabel in
        
        let label                            = UILabel()
        
        label.adjustsFontSizeToFitWidth      = true
        label.text                           = ""
        label.backgroundColor                = UIColor.clear
        
        return label
    }
    
    init(viewModel:CartConfirmVM) {
        disposeBag = DisposeBag()
        
        deliver = Variable("免费")
        
        charge = Variable("0%")
        
        /// init event publish
        selectAddressEvent = PublishSubject()
        
        couponPickerEvent = PublishSubject()
        
        /// view model
        self.viewModel                       = viewModel
        
        /// init variable
        selectedAddress                      = Variable(nil)
        
        items = Variable([])
        
        /// init ui
        let tableView = UITableView(frame: .zero,
                                    style: .grouped)
        
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
        
        self.tableView = tableView
        
        confirmButton = ViewFactory.redButton(title: "立刻结算")
        
        selectedAddress
            .asObservable()
            .map({ $0 != nil })
            .bind(to: confirmButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        let footLabel = UILabel()
        
        footLabel.backgroundColor = UIColor.clear
        footLabel.numberOfLines = 0
        footLabel.textAlignment = .right
        footLabel.tag = 1
        
        let _sumCell = sumCell
        
        sumCell.selectionStyle = .none
        sumCell.contentView.backgroundColor = UIConfig.generalColor.white
        sumCell.contentView.addSubview(footLabel)
        
        footLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(_sumCell.contentView)
            maker.right.equalTo(_sumCell.contentView).offset(-15)
        }
        
        let sumLabel = UILabel()
        
        sumLabel.backgroundColor = UIColor.clear
        
        self.sumLabel = sumLabel
        
        /// set attr observer
        
        let countNumber = items
            .asDriver()
            .map{$0.reduce(0, {$0 + $1.cartItem.number})}
        
        let collection = Driver.combineLatest(countNumber, charge.asDriver(), deliver.asDriver()) { (count:$0, charge:$1, deliver:$2) }
        
        let countAttr = collection
            .map { (collection) -> NSAttributedString in
                
                let count = collection.count
                
                let charge = collection.charge
                
                let attrString = NSAttributedString(string: "运费:\(collection.deliver)\n服务费:\(charge)\n共计 \(count) 件商品  小计：",
                    font: UIConfig.generalFont(11),
                    textColor: UIConfig.generalColor.unselected,
                    lineSpace: 10)
                
                return attrString
        }
        
        self.sumPrice = PublishSubject()
        
        let sumAttr = sumPrice
            .map { (sum) -> NSAttributedString in
                let attr = NSMutableAttributedString(string: "￥",
                                                     font:UIConfig.generalSemiboldFont(11),
                                                     textColor: UIConfig.generalColor.red)
                
                let sumIntValue = Int(sum)
                
                let middle = "\(sumIntValue)"
                
                attr.append(NSAttributedString(string: middle,
                                               attributes: [NSFontAttributeName:UIConfig.generalSemiboldFont(15),
                                                            NSForegroundColorAttributeName:UIConfig.generalColor.red]))
                
                let tail = ((sum - Float(sumIntValue)).priceString as NSString).substring(from: 1)
                
                attr.append(NSAttributedString(string: tail,
                                               attributes: [NSFontAttributeName:UIConfig.generalSemiboldFont(11),
                                                            NSForegroundColorAttributeName:UIConfig.generalColor.red]))
                
                return attr
            }
            .asDriver(onErrorJustReturn: NSAttributedString(string: "计算中",
                                                            attributes: [NSFontAttributeName:UIConfig.generalFont(12),
                                                                         NSForegroundColorAttributeName:UIConfig.generalColor.selected]))
    
        sumAttr.drive(onNext: { (attrString) in
            let currentAttr = NSMutableAttributedString()
            
            currentAttr.append(NSAttributedString(string: "合计：",
                                                  attributes: [NSFontAttributeName:UIConfig.generalFont(12),
                                                               NSForegroundColorAttributeName:UIConfig.generalColor.selected]))
            currentAttr.append(attrString)
            
            sumLabel.attributedText = currentAttr
        },
                      onCompleted: nil,
                      onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        Driver.combineLatest(sumAttr,
                             countAttr,
                             resultSelector: { (sum, count) -> NSAttributedString in
                                let attr = NSMutableAttributedString(attributedString: count)
                                
                                attr.append(sum)
                                
                                return attr
        })
            .drive(onNext: { (attr) in
                footLabel.attributedText = attr
                footLabel.textAlignment = .right
                footLabel.sizeToFit()
                
                tableView.reloadData()
            },
                   onCompleted: nil,
                   onDisposed: nil).addDisposableTo(disposeBag)
        
        /// super init
        super.init(nibName: nil,
                   bundle: nil)
        
        /// test data
        bind()
    }
    
    let bottomView = UIView()
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        /// set title
        self.title = "确认订单"
        
        /// set table view
        self.view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(cellClass: AddressTableViewCell.self)
        tableView.registerCell(cellClass: AlipayCell.self)
        tableView.registerCell(cellClass: OrderTableViewCell.self)
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.contentInset = UIEdgeInsets(top: 64,
                                              left: 0,
                                              bottom: 50,
                                              right: 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
        tableView.contentOffset = CGPoint(x: 0, y: -64)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.tableFooterView = UIView()
        
        /// add bottom
        
        self.view.addSubview(bottomView)
        
        bottomView.snp.makeConstraints { (maker) in
            maker.left.bottom.right.equalTo(self.view)
            maker.height.equalTo(50)
        }
        bottomView.backgroundColor = UIColor.clear
        bottomView.addSubview(confirmButton)
        
        confirmButton.snp.makeConstraints { (maker) in
            maker.right.top.bottom.equalTo(bottomView)
            maker.width.equalTo(100)
        }
        
        let fitView = UIView()
        
        bottomView.addSubview(fitView)
        
        fitView.backgroundColor = UIConfig.generalColor.white.withAlphaComponent(9.5)
        fitView.snp.makeConstraints { (maker) in
            maker.left.top.bottom.equalTo(bottomView)
            maker.right.equalTo(confirmButton.snp.left)
        }
        
        bottomView.addSubview(sumLabel)
        
        sumLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(bottomView)
            maker.right.equalTo(confirmButton.snp.left).offset(-25)
        }
        
        /// bind data and reload
        weak var weakSelf = self
        
        selectedAddress.asObservable().bind { (_) in
            if let strongSelf = weakSelf {
                strongSelf.tableView.reloadData()
            }
            }.addDisposableTo(disposeBag)
        
        //addFakeShadow()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.tableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func couponPickerPressed(cell:UITableViewCell) {
        guard let index = tableView.indexPath(for: cell) else {
            return
        }
        
        let i = index.section - 1
        
        let price = items.value[i].cartItem.sumPrice
        
        couponPickerEvent.onNext((i, price))
    }
}

extension CartConfirmVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            selectAddressEvent.onNext(Void())
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2 + items.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
        } else {
            view.backgroundColor = UIConfig.generalColor.backgroudGray
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return AddressTableViewCell.height(with: self.view.bounds.width,
                                               and: selectedAddress.value)
        }
        
        if indexPath.section > items.value.count {
            return 45
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        
        if indexPath.section > items.value.count {
            return 45
        }
        
        return 385.5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell:AddressTableViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.item = selectedAddress.value
            
            return cell
        }
        
        if indexPath.section > items.value.count {
            
            let cell:AlipayCell = tableView.dequeueReusableCell(at: indexPath)
            
            return cell
        }
        
        let item = items.value[indexPath.section - 1]
        
        let cell:OrderTableViewCell = tableView.dequeueReusableCell(at: indexPath)
        
        cell.setItem(item: item)
        cell.parentViewController = self
        
        return cell
    }
}

extension CartConfirmVC {
    class OrderTableViewCell: UITableViewCell {
        let coverImageView = UIImageView()
        
        let titleLabel = UILabel()
        
        let standardLabel = UILabel()
        
        let storeLabel = UILabel()
        
        let priceLabel = UILabel()
        
        let numberLabel = UILabel()
        
        let couponLabel = ViewFactory.generalLabel()
        
        let costLabel = UILabel()
        
        let totalLabel = UILabel()
        
        weak var parentViewController:CartConfirmVC?
        
        func couponItemPressed(sender:Any) {
            parentViewController?.couponPickerPressed(cell: self)
        }
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            // add header label
            let headerLabel = ViewFactory.generalLabel(generalSize: 14)
            
            self.contentView.addSubview(headerLabel)
            
            headerLabel.text = "订单信息"
            
            headerLabel.snp.makeConstraints { (maker) in
                maker.top.left.equalTo(self.contentView).offset(15)
            }
            
            // add card view
            let cardView = ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
            
            self.contentView.addSubview(cardView)
            
            cardView.snp.makeConstraints { (maker) in
                maker.top.equalTo(headerLabel.snp.bottom).offset(15)
                maker.left.equalTo(headerLabel)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            // add cover
            cardView.addSubview(coverImageView)
            
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(cardView).offset(10)
                maker.bottom.equalTo(cardView).offset(-10)
                maker.size.equalTo(CGSize(width:115, height:95))
            }
            
            // add title
            cardView.addSubview(titleLabel)
            
            titleLabel.font = UIConfig.generalFont(13)
            titleLabel.numberOfLines = 2
            titleLabel.textColor = UIConfig.generalColor.selected
            titleLabel.backgroundColor = cardView.backgroundColor
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.top.equalTo(coverImageView)
                maker.right.equalTo(cardView).offset(-10)
            }
            
            // add standar
            cardView.addSubview(standardLabel)
            
            standardLabel.textColor = UIConfig.generalColor.unselected
            standardLabel.font = UIConfig.generalFont(11)
            standardLabel.backgroundColor = titleLabel.backgroundColor
            standardLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.right.equalTo(titleLabel)
                maker.top.equalTo(titleLabel.snp.bottom).offset(2)
            }
            
            // add store
            cardView.addSubview(storeLabel)
            
            storeLabel.textColor = UIConfig.generalColor.unselected
            storeLabel.font = UIConfig.generalFont(11)
            storeLabel.backgroundColor = titleLabel.backgroundColor
            storeLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.right.equalTo(titleLabel)
                maker.top.equalTo(standardLabel.snp.bottom).offset(1)
            }
            
            // add price
            cardView.addSubview(priceLabel)
            
            priceLabel.font = UIConfig.generalFont(12)
            priceLabel.textColor = UIConfig.generalColor.red
            priceLabel.backgroundColor = cardView.backgroundColor
            priceLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.bottom.equalTo(coverImageView)
            }
            
            // add number
            cardView.addSubview(numberLabel)
            
            numberLabel.textColor = UIConfig.generalColor.unselected
            numberLabel.backgroundColor = cardView.backgroundColor
            numberLabel.font = UIConfig.generalFont(12)
            numberLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(titleLabel)
                maker.bottom.equalTo(coverImageView)
            }
            
            // add line
            let upLine = ViewFactory.line()
            
            self.contentView.addSubview(upLine)
            
            upLine.snp.makeConstraints { (maker) in
                maker.top.equalTo(cardView.snp.bottom).offset(20)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.height.equalTo(0.5)
            }
            
            var bottomLine = ViewFactory.line()
            
            self.contentView.addSubview(bottomLine)
            
            bottomLine.snp.makeConstraints { (maker) in
                maker.top.equalTo(upLine.snp.bottom).offset(55)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.height.equalTo(0.5)
            }
            
            // coupon
            let couponTitleLabel = ViewFactory.generalLabel()
            
            self.contentView.addSubview(couponTitleLabel)
            
            couponTitleLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(upLine.snp.bottom).offset(27.25)
                maker.left.equalTo(upLine)
            }
            
            couponTitleLabel.text = "优惠券"
            
            self.contentView.addSubview(couponLabel)
            
            couponLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(couponTitleLabel)
                maker.right.equalTo(upLine)
            }
            
            let couponButton = UIButton(type: .custom)
            
            self.contentView.addSubview(couponButton)
            
            couponButton.addTarget(self,
                                   action: #selector(OrderTableViewCell.couponItemPressed(sender:)),
                                   for: UIControlEvents.touchUpInside)
            couponButton.backgroundColor = .clear
            couponButton.snp.makeConstraints { (maker) in
                maker.left.right.top.equalTo(upLine)
                maker.bottom.equalTo(bottomLine)
            }
            
            // cast 
            costLabel.numberOfLines = 3
            
            self.contentView.addSubview(costLabel)
            
            costLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(bottomLine.snp.right)
                maker.top.equalTo(bottomLine.snp.bottom).offset(15)
            }
            
            bottomLine = ViewFactory.line()
            
            self.contentView.addSubview(bottomLine)
            
            bottomLine.snp.makeConstraints { (maker) in
                maker.top.equalTo(costLabel.snp.bottom).offset(15)
                maker.left.equalTo(costLabel)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.height.equalTo(0.5)
            }
            
            // total
            self.contentView.addSubview(totalLabel)
            
            totalLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(bottomLine.snp.right)
                maker.top.equalTo(bottomLine.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
        }
        
        override func prepareForReuse() {
            super.prepareForReuse()
        }
        
        func setItem(item:CartConfirmItem) {
            coverImageView.kf.setImage(with:item.cartItem.imageUrl)
            
            titleLabel.text = item.cartItem.title
            
            standardLabel.text = "规格：\(item.cartItem.subTitle)"
            
            storeLabel.text = ""//"渠道：\(item.cartItem.freeStoreId)"
            
            /// price
            priceLabel.text = item.cartItem.rmb.description
            
            numberLabel.text = item.cartItem.number.description
            
            /// coupon
            
            if let `couponItem` = item.couponItem {
                let couponAttributedString = NSMutableAttributedString(string: couponItem.title + "  ",
                                                                       font: UIConfig.generalFont(15),
                                                                       textColor: UIConfig.generalColor.selected)
                
                couponAttributedString.append(NSAttributedString(imageNamed: "icon_more-brand"))
                
                couponLabel.attributedText = couponAttributedString
            } else if item.selableList.count > 0 {
                let couponAttributedString = NSMutableAttributedString(string: "无   ",
                                                                       font: UIConfig.generalFont(15),
                                                                       textColor: UIConfig.generalColor.selected)
                
                couponAttributedString.append(NSAttributedString(imageNamed: "icon_more-brand"))
                
                couponLabel.attributedText = couponAttributedString
            } else {
                couponLabel.attributedText = NSAttributedString(string: "无可用的优惠券",
                                                                font: UIConfig.generalFont(15),
                                                                textColor: UIConfig.generalColor.whiteGray)
            }
            
            /// cast
            let castAttributedString = NSMutableAttributedString(string: "运费：\(item.cartItem.deliver <= 0 ? "免费" : "￥" + item.cartItem.deliver.description)\n",
                font: UIConfig.generalFont(12),
                textColor: UIConfig.generalColor.titleColor,
                lineSpace: 5,
                alignment: .right)
            
            castAttributedString.append(NSAttributedString(string: "服务费：\(item.cartItem.tipPrice <= 0 ? "免费" : "￥" + item.cartItem.tipPrice.priceString)\n",
                font: UIConfig.generalFont(12),
                textColor: UIConfig.generalColor.titleColor,
                lineSpace: 5,
                alignment: .right))
            
            castAttributedString.append(NSAttributedString(string: "优惠折扣：",
                                                           font: UIConfig.generalFont(12),
                                                           textColor: UIConfig.generalColor.titleColor,
                                                           lineSpace: 5,
                                                           alignment: .right))
            
            castAttributedString
                .append(NSAttributedString(string: "￥\(item.couponItem.map({ $0.favorablePrice(item.cartItem.sumPrice) }) ?? 0)\n",
                    font: UIConfig.generalFont(12),
                    textColor: UIConfig.generalColor.red,
                    lineSpace: 5,
                    alignment: .right))
            
            costLabel.attributedText = castAttributedString
            
            /// total
            let totalPrice = item.totalPrice
            
            let totalAttributedString = NSMutableAttributedString(string: "共计 \(item.cartItem.number) 件商品 小计:",
                font: UIConfig.generalFont(12),
                textColor: UIConfig.generalColor.titleColor,
                lineSpace: 5,
                alignment: .right)
            
            let currency = Currency(unit: "￥",
                                    value: totalPrice)
            
            totalAttributedString.append(currency.attributedString(color: UIConfig.generalColor.red,
                                                                   intSize: 15,
                                                                   floatSize: 12))
            
            totalLabel.attributedText = totalAttributedString
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension CartConfirmVC {
    class TableViewCell: UITableViewCell {
        
        let coverImageView = UIImageView()
        
        let titleLabel = UILabel()
        
        let subTitle = UILabel()
        
        let priceLabel = UILabel()
        
        let numberLabel = UILabel()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            // add cover
            self.contentView.addSubview(coverImageView)
            
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(30)
                maker.centerY.equalTo(self.contentView)
                maker.size.equalTo(CGSize(width:115, height:95))
            }
            
            // add title
            self.contentView.addSubview(titleLabel)
            
            titleLabel.font = UIConfig.generalFont(13)
            titleLabel.numberOfLines = 2
            titleLabel.textColor = UIConfig.generalColor.selected
            titleLabel.backgroundColor = coverImageView.backgroundColor
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(coverImageView.snp.right).offset(15)
                maker.top.equalTo(coverImageView)
                maker.right.equalTo(self.contentView).offset(-10)
            }
            
            // add price
            self.contentView.addSubview(priceLabel)
            
            priceLabel.font = UIConfig.generalFont(13)
            priceLabel.textColor = UIConfig.generalColor.red
            priceLabel.backgroundColor = coverImageView.backgroundColor
            priceLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.bottom.equalTo(coverImageView)
            }
            
            // add rmb
            self.contentView.addSubview(subTitle)
            
            subTitle.textColor = UIConfig.generalColor.whiteGray
            subTitle.font = UIConfig.arialFont(13)
            subTitle.backgroundColor = titleLabel.backgroundColor
            subTitle.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.right.equalTo(titleLabel)
                maker.top.equalTo(titleLabel.snp.bottom)
            }
            
            // add number
            self.contentView.addSubview(numberLabel)
            
            numberLabel.textColor = UIConfig.generalColor.selected
            numberLabel.backgroundColor = UIConfig.generalColor.white
            numberLabel.font = UIConfig.arialFont(13)
            numberLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(titleLabel)
                maker.centerY.equalTo(priceLabel)
            }
        }
        
        func setItem(item:CartItem) {
            coverImageView.kf.setImage(with:item.imageUrl)
            
            titleLabel.text = item.title
            
            subTitle.text = item.subTitle
            
            /// price
            priceLabel.attributedText = getPriceAttributedString(rmb: item.rmb,
                                                                 ori: item.price)
            
            numberLabel.attributedText = FlygoUtil.attributedString(number: item.number)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}

extension CartConfirmVC {
    class AlipayCell: UITableViewCell {
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            let textLabel = UILabel()
            
            self.contentView.addSubview(textLabel)
            
            textLabel.text = "支付宝付款"
            textLabel.font = UIConfig.generalFont(15)
            textLabel.textColor = UIConfig.generalColor.selected
            textLabel.sizeToFit()
            textLabel.origin.x = 15
            
            textLabel.snp.makeConstraints({ (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.centerY.equalTo(self.contentView)
            })
            
            let imageView = UIImageView()
            
            self.contentView.addSubview(imageView)
            
            imageView.image = UIImage(named:"icon_selected")
            imageView.sizeToFit()
            
            imageView.snp.makeConstraints({ (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.centerY.equalTo(self.contentView)
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class AddressTableViewCell: UITableViewCell {
        
        static func height(with width:CGFloat, and address:AddressItem?) -> CGFloat {
            guard let addressItem            = address else {
                return 104
            }
            
            let nameHeight                   = (addressItem.receiver as NSString).size(attributes: [
                NSFontAttributeName: UIConfig.generalFont(13)
                ]).height
            
            let top                          = 35 + nameHeight
            
            let ps                           = NSMutableParagraphStyle()
            
            ps.lineSpacing                   = 5.0
            
            let fontWidth = width - 100
            
            let size = CGSize(width: fontWidth,
                              height: 10000)
            
            let addressHeight = addressItem.addressAttributedString.boundingRect(with: size,
                                                                                 options: .usesLineFragmentOrigin,
                                                                                 context: nil).height
            
            return addressHeight + 25 + top
        }
        
        static func label() -> UILabel {
            let label                        = UILabel()
            
            label.textColor                  = UIConfig.generalColor.selected
            label.font                       = UIConfig.generalFont(13)
            label.backgroundColor            = UIConfig.generalColor.white
            
            return label
        }
        
        func adjustLayout () {
            accessImageView.centerYInSuperView()
            accessImageView.left = self.view.bounds.width - 21
            
            lineImageView.bottom = self.view.bounds.height
            lineImageView.size.width = self.view.bounds.width
            
            self.view.bringSubview(toFront: lineImageView)
            
            if item == nil {
                detailLabel.sizeToFit()
                detailLabel.centerInSuperView()
                
                return
            }
            
            nameLabel.origin = CGPoint(x: 50, y: 20)
            nameLabel.sizeToFit()
            
            telLabel.sizeToFit()
            telLabel.right = self.bounds.width - 50
            telLabel.bottom = nameLabel.bottom
            
            let fontWidth = self.bounds.width - 100
            
            detailLabel.w = fontWidth
            detailLabel.fitHeight()
            detailLabel.origin = CGPoint(x: 50, y: telLabel.bottom + 15)
            
            tagImageView.centerYInSuperView()
            tagImageView.right = 35
        }
        
        let nameLabel                        = AddressTableViewCell.label()
        
        let telLabel                         = AddressTableViewCell.label()
        
        let detailLabel                      = AddressTableViewCell.label()
        
        let tagImageView                     = UIImageView(named: "icon_address")
        
        let accessImageView                  = UIImageView(named: "icon_more-brand")
        
        let lineImageView                    = UIImageView(named: "background_rules")
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            self.selectionStyle = .none
            
            tagImageView.backgroundColor     = UIConfig.generalColor.white
            
            accessImageView.backgroundColor  = UIConfig.generalColor.white
            
            detailLabel.numberOfLines = 0
            
            self
                .contentView
                .addSubviews([tagImageView, accessImageView, detailLabel, nameLabel, telLabel])
            self.addSubview(lineImageView)
        }
        
        var item:AddressItem? {
            didSet {
                guard let addressItem        = item else {
                    detailLabel.text         = "不添加收货地址，快递会迷路哒~"
                    
                    telLabel.isHidden        = true
                    nameLabel.isHidden       = true
                    tagImageView.isHidden    = true
                    
                    adjustLayout()
                    
                    return
                }
                
                telLabel.isHidden            = false
                nameLabel.isHidden           = false
                tagImageView.isHidden        = false
                
                telLabel.text = addressItem.phone
                nameLabel.text = addressItem.receiver
                detailLabel.attributedText = addressItem.addressAttributedString
                
                adjustLayout()
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            adjustLayout()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
}
