//
//  CartConfirmVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct CartConfirmItem {
    let cartItem:CartItem
    
    var couponItem:CouponItem?
    
    init(cartItem:CartItem) {
        self.cartItem = cartItem
        
        self.couponItem = nil
    }
    
    var selableList:[CouponItem] = []
    
    var totalPrice:Float {
        guard let couponItem = self.couponItem else {
            return cartItem.totalPrice
        }
        
        return cartItem.totalPrice - couponItem.favorablePrice(cartItem.sumPrice)
    }
    
    typealias OrderSubmit = API.OrderPaymentInfo.OrderSubmit
    
    var orderSubmit:OrderSubmit {
        return (
            productRecId:cartItem.recId,
            num:cartItem.number,
            belongStoreIds:cartItem.belongStoreId,
            freeStores:cartItem.freeStoreId,
            cartSubId:cartItem.subId,
            couponDetailsId:couponItem?.detailId ?? ""
        )
    }
}

class CartConfirmVM: ViewModel {
    let cartItems:[CartItem]
    
    var selectedCoupon:CouponItem?
    
    init(cartItems:[CartItem]) {
        self.cartItems = cartItems
    }
    
    func bind(to view: CartConfirmVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .none,
                                       refreshEnable: false)
        
        bag.insert(dataSet)
        
        let addressRequest = API
            .Address
            .getList()
            .shareReplay(1)
        
        let couponRequest = API
            .ProductsCoupes
            .getCoupons(from: self.cartItems.map({ $0.pdcId }))
            .shareReplay(1)
        
        let _cartItems = self.cartItems
        
        let couponDictionary = couponRequest.asModelObservable().map { (coupes) -> [String:[CouponItem]] in
            var dict = [String:[CouponItem]]()
            
            let globles = coupes.globalCoupons.map({ CouponItem(model: $0,
                                                                standardTime: coupes.serverTime) })
            
            for (index, item) in _cartItems.enumerated() {
                var array = globles
                
                if index < coupes.productCoupons.count {
                    let currentList = coupes
                        .productCoupons[index]
                        .map({ CouponItem(model: $0,
                                          standardTime: Date()) })
                    
                    array.append(contentsOf: currentList)
                }
                
                dict[item.id] = array
            }
            
            return dict
        }
            .shareReplay(1)
        
        let cartItems = couponDictionary
            .flatMapLatest {
                (dict) -> Observable<[CartConfirmItem]> in
                
                var items = [CartConfirmItem]()
                
                let currentList = _cartItems
                
                /// 自动匹配最实惠的优惠券
                for item in currentList {
                    let recId = item.id
                    
                    let price = item.sumPrice
                    
                    let selectedCoupons = items
                        .map({ $0.couponItem?.id })
                        .filter({ $0 != nil })
                        .map({ $0! })
                    
                    let currentCoupous = dict[recId]?.filter({ (one) -> Bool in
                        return !selectedCoupons.contains(one.id) && one.limit <= price
                    })
                        .sorted(by: { (ori, new) -> Bool in
                            let oriPrice = ori.favorablePrice(price)
                            
                            let newPrice = new.favorablePrice(price)
                            
                            return newPrice < oriPrice
                        }) ?? []
                    
                    var confirmItem = CartConfirmItem(cartItem: item)
                    
                    confirmItem.couponItem = currentCoupous.first
                    
                    items.append(confirmItem)
                }
                
                let selectedCoupons = items
                    .map({ $0.couponItem?.id })
                    .filter({ $0 != nil })
                    .map({ $0! })
                
                for (index, one) in items.enumerated() {
                    var newItem = one
                    
                    let price = one.cartItem.sumPrice
                    
                    newItem.selableList = dict[one.cartItem.id]?.filter({ (one) -> Bool in
                        return !selectedCoupons.contains(one.id) || one.id == newItem.couponItem?.id
                    })
                        .sorted(by: { (ori, new) -> Bool in
                            let oriPrice = ori.favorablePrice(price)
                            
                            let newPrice = new.favorablePrice(price)
                            
                            return newPrice < oriPrice
                        }) ?? []
                    
                    items[index] = newItem
                }
                
                return Observable.create({ (obs) -> Disposable in
                    obs.onNext(items)
                    
                    return view
                        .couponPickerEvent
                        .flatMapLatest({
                            [weak view] (collection) -> Observable<(index:Int, item:CouponItem?)> in
                            
                            let item = items[collection.0]
                            
                            if item.selableList.count <= 0 {
                                return Observable.empty()
                            }
                            
                            return CouponPickerVC
                                .picker(in: view,
                                        price: collection.1,
                                        with: item.selableList)
                                .take(1)
                                .map({ (index:collection.0, item:$0) })
                        })
                        .bind(onNext: { (collection) in
                            let index = collection.index
                            
                            let item = collection.item
                            
                            items[index].couponItem = item
                            
                            let selectedCoupons = items
                                .map({ $0.couponItem?.id })
                                .filter({ $0 != nil })
                                .map({ $0! })
                            
                            for (index, one) in items.enumerated() {
                                var newItem = one
                                
                                let price = one.cartItem.sumPrice
                                
                                newItem.selableList = dict[one.cartItem.id]?.filter({ (one) -> Bool in
                                    return !selectedCoupons.contains(one.id) || one.id == newItem.couponItem?.id
                                })
                                    .sorted(by: { (ori, new) -> Bool in
                                        let oriPrice = ori.favorablePrice(price)
                                        
                                        let newPrice = new.favorablePrice(price)
                                        
                                        return newPrice < oriPrice
                                    }) ?? []
                                
                                items[index] = newItem
                            }
                            
                            obs.onNext(items)
                        })
                })
            }
            .shareReplay(1)
        
        cartItems.bind(to: view.items)
            .addDisposableTo(bag)
        
        [addressRequest.asRequestStatusObservable(), couponRequest.asRequestStatusObservable()]
            .merge()
            .bind(to: dataSet.status)
            .addDisposableTo(bag)
        
        cartItems
            .map({ $0.count <= 0 })
            .bind(to: dataSet.isEmpty)
            .addDisposableTo(bag)
        
        let sumPrice = cartItems
            .map({ $0.reduce(0,
                             { (result, item) -> Float in
                                return result + item.totalPrice
            })
            })
            .shareReplay(1)
        
        sumPrice
            .bind(to: view.sumPrice)
            .addDisposableTo(bag)
        
        weak var weakView = view
        
        let addressSelected = view.selectAddressEvent
            .withLatestFrom(view.selectedAddress.asObservable())
            .flatMapLatest({currentAddress -> Observable<AddressItem> in
                if currentAddress != nil {
                    return  AddressSelectorVM
                        .addressItem(showOn: weakView)
                        .take(1)
                }
                
                return AddressEditVC.create(in: weakView).take(1)
            })
            .map({ (item) -> AddressItem? in
                return item
            })
            .shareReplay(1)
        
        let defaultAddress = addressRequest
            .asModelsObservable()
            .map { (items) -> API.Address? in
                for one in items {
                    if one.isDefault {
                        return one
                    }
                }
                
                return items.first
            }
            .map({ $0.map({ AddressItem(model:$0) })})
            .shareReplay(1)
        
        let addressObserver = Observable
            .of(defaultAddress, addressSelected).merge().shareReplay(1)
        
        addressObserver.bind(to: view.selectedAddress)
            .addDisposableTo(bag)
        
        addressSelected.map({ $0 != nil })
            .bindNext(view.confirmButton.rx.isEnabled)
            .addDisposableTo(bag)
        
        dataSet
            .status
            .asDriver()
            .map({ $0.rawValue != 1 })
            .drive(view.bottomView.rx.isHidden)
            .addDisposableTo(bag)
        
        let paymentInfo = PublishSubject<API.OrderPaymentInfo>()
        
        let requestItem = SubmitItem(rootController: view) {
            (payment:API.OrderPaymentInfo) -> Void in
            
            paymentInfo.onNext(payment)
        }
        
        bag.insert(requestItem)
        
        let successItem = NoResSubmitItem(rootController: view) { 
                                        [weak view] in
                                        let vc = OrderVC(defaultPage: 2)
                                        
                                        view?.navigationController?.pushViewController(vc,
                                                                                       animated: true)
        }
        
        let submitOrders = cartItems.map({ $0.map({ $0.orderSubmit }) })
        
        let submitInfo = Observable.combineLatest(submitOrders,
                                                  addressObserver.filter({ $0 != nil }).map({ $0! }).asObservable(),
                                                  sumPrice) {
                                                    (orders, address, totalPrice) -> (orders:[CartConfirmItem.OrderSubmit], addressId:String, price:Float) in
                                                    
                                                    return (orders:orders, addressId:address.id, price:totalPrice)
        }
            .shareReplay(1)
        
        let submitRequest = view.confirmButton.rx.tap.withLatestFrom(submitInfo).flatMapLatest {
            (info) in
            
            return API.OrderPaymentInfo.submit(orders: info.orders,
                                               addressId: info.addressId,
                                               payTotalMoney: info.price)
        }.shareReplay(1)
        
        submitRequest.bindNext(requestItem).addDisposableTo(bag)
        
        paymentInfo.map({_ in }).bind(onNext: {CartVM.refreshNotification()}).addDisposableTo(bag)
        
        let alipayRequest = paymentInfo
            .flatMapLatest{AlipayManager.payEvent(order: $0.applyPayContent, userInfo:$0.outTradeNo )}
            .shareReplay(1)
        
        alipayRequest
            .observeOn(MainScheduler.instance)
            .bind {
                [weak view] (result) in
                switch result {
                case .success(_):
                    return
                default:
                    let vc = OrderVC(defaultPage: 1)
                    
                    view?.navigationController?.pushViewController(vc,
                                                                   animated: true)
                }
        }.addDisposableTo(bag)
        
        let paySuccess = alipayRequest.filter({$0.isSuccess}).flatMapLatest { (result) -> Observable<PostItem> in
            return API.OrderPaymentInfo.paySuccess(outTradeNo: result.userInfo ?? "")
        }.shareReplay(1)
        
        paySuccess.bindNext(successItem).addDisposableTo(bag)
        
        return bag
    }
    
}

