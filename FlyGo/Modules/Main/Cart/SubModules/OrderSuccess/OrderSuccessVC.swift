//
//  OrderSuccessVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class OrderSuccessVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = ViewFactory.label(font: UIConfig.generalSemiboldFont(20),
                                      textColor: UIConfig.generalColor.red)
        
        label.text = "支付成功"
        
        let imageView = UIImageView(named: "icon_pay_success")
        
        func createButton (title:String, target:Any, action:Selector) -> UIButton {
            let button = UIButton(type: .custom)
            
            button.backgroundColor = UIConfig.generalColor.white
            button.setAttributedTitle(NSAttributedString(string: title,
                                                         font: UIConfig.generalFont(15),
                                                         textColor: UIConfig.generalColor.selected),
                                      for: .normal)
            button.layer.borderWidth = 2
            button.layer.cornerRadius = 10
            button.layer.borderColor = UIConfig.generalColor.unselected.cgColor
            button.layer.masksToBounds = true
            button.addTarget(target,
                             action: action,
                             for: .touchUpInside)
            
            return button
        }
        
        let homeButton = createButton(title: "返回首页",
                                      target: self,
                                      action: #selector(OrderSuccessVC.toHomeClick(sender:)))
        
        let orderButton = createButton(title: "查看订单",
                                       target: self,
                                       action: #selector(OrderSuccessVC.toOrderClick(sender:)))
    
        self.view.addSubviews(label, imageView, homeButton, orderButton)
        
        label.snp.makeConstraints { (maker) in
            maker.center.equalTo(self.view)
        }
        
        imageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(label.snp.top).offset(-35)
            maker.centerX.equalTo(label)
        }
        
        homeButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(label)
            maker.top.equalTo(label.snp.bottom).offset(50)
            maker.size.equalTo(CGSize(width:250, height:44))
        }
        
        orderButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(label)
            maker.top.equalTo(homeButton.snp.bottom).offset(20)
            maker.size.equalTo(CGSize(width:250, height:44))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let vcs = self.navigationController?.viewControllers, vcs.count > 0 {
            var array = [UIViewController]()
            
            for one in array {
                if type(of:one) != CartConfirmVC.classForCoder() {
                    array.append(one)
                }
            }
            
            if array.count != vcs.count {
                self.navigationController?.setViewControllers(array,
                                                              animated: false)
            }
        }
    }
    
    func toOrderClick(sender:Any?) {
        
    }
    
    func toHomeClick(sender:Any?) {
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
}
