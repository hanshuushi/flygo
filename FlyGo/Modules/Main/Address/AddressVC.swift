//
//  AddressVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class AddressVC: UIViewController, PresenterType {
    
    var items:[AddressItem] = []
    
    let itemUpdated:PublishSubject<AddressItem> = PublishSubject()
    
    let itemDeleted:PublishSubject<AddressItem> = PublishSubject()
    
    let itemSelectedDefault:PublishSubject<AddressItem> = PublishSubject()
    
    let tableView = UITableView(frame: .zero,
                                style: .grouped)
    
    let viewModel:AddressVM
    
    var bindDisposeBag: DisposeBag?
    
    init(viewModel:AddressVM) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        bind()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let createButton = {
        () -> UIButton in
        
        let createButton = ViewFactory.redButton(title: "新建收货地址")
        
        return createButton
    }()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "收货地址管理"
        
        self.view.addSubview(tableView)

        tableView.backgroundColor = UIConfig.generalColor.backgroudGray
        tableView.estimatedRowHeight = 157
        tableView.setInset(top: 64, bottom: 49)
        tableView.snp.fillToSuperView()
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(cellClass: AddressCell.self)
        tableView.registerCell(cellClass: EditorCell.self)
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        
        self.view.addSubview(createButton)
        
        createButton.snp.makeConstraints { (maker) in
            maker.left.bottom.right.equalTo(self.view)
            maker.height.equalTo(49)
        }
        
        //addFakeShadow()
    }
}

extension AddressVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
        } else {
            view.backgroundColor = UIConfig.generalColor.backgroudGray
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        
        return 46
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            let cell:EditorCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.item = items[indexPath.section]
            cell.parentVC = self
            
            return cell
        }
        
        let cell:AddressCell = tableView.dequeueReusableCell(at: indexPath)
        
        cell.set(item: items[indexPath.section])
        
        return cell
    }
}

extension AddressVC {
    
    class AddressCell: UITableViewCell {
        
        let nameLabel:UILabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                                  textColor: UIConfig.generalColor.selected)
        
        let telLabel:UILabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                                 textColor: UIConfig.generalColor.selected)
        
        let addressLabel:UILabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                                     textColor: UIConfig.generalColor.selected)
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(nameLabel)
            
            self.selectionStyle = .none
            
            nameLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.left.equalTo(self.contentView).offset(15)
            }
            
            self.contentView.addSubview(telLabel)
            
            telLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            self.contentView.addSubview(addressLabel)
            
            addressLabel.numberOfLines = 3
            addressLabel.lineBreakMode = .byTruncatingTail
            addressLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(nameLabel)
                maker.right.equalTo(telLabel)
                maker.top.equalTo(nameLabel.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
        }
        
        func set(item:AddressItem) {
            nameLabel.text              = item.receiver
            telLabel.text               = item.phone
            addressLabel.attributedText = item.addressAttributedString
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension AddressVC {
    class EditorCell: UITableViewCell {
        
        weak var parentVC:AddressVC? = nil
        
        let checkBox = UIButton(type: .custom)
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            checkBox.setImage(UIImage(named:"icon_isnt_default_address"),
                              for: .normal)
            checkBox.setTitleColor(UIConfig.generalColor.unselected,
                                   for: .normal)
            checkBox.setTitle("设为默认地址", for: .normal)
            checkBox.setImage(UIImage(named:"icon_default_address"),
                              for: .disabled)
            checkBox.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
            checkBox.setTitle("默认地址", for: .disabled)
            checkBox.setTitleColor(UIConfig.generalColor.red,
                                   for: .disabled)
            checkBox.backgroundColor = UIConfig.generalColor.white
            checkBox.titleLabel?.font = UIConfig.generalFont(12)
            checkBox.sizeToFit()
            checkBox.contentHorizontalAlignment = .left
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            checkBox.addTarget(self,
                               action: #selector(EditorCell.defaultHandler),
                               for: UIControlEvents.touchUpInside)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(checkBox)
            
            checkBox.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.left.equalTo(self.contentView).offset(15)
                maker.size.equalTo(CGSize(width:100, height:30))
            }
            
            let deleteButton = UIButton(type: .custom)
            
            self.contentView.addSubview(deleteButton)
            
            deleteButton.setTitle("删除", for: .normal)
            deleteButton.setImage(UIImage(named:"icon_delete_address"), for: .normal)
            deleteButton.backgroundColor = UIConfig.generalColor.white
            deleteButton.layer.masksToBounds = true
            deleteButton.layer.borderColor = UIConfig.generalColor.unselected.cgColor
            deleteButton.layer.borderWidth = 1
            deleteButton.layer.cornerRadius = 4
            deleteButton.titleLabel?.font = UIConfig.generalFont(11)
            deleteButton.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0)
            deleteButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5)
            deleteButton.setTitleColor(UIConfig.generalColor.unselected, for: .normal)
            deleteButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.size.equalTo(CGSize(width:80, height:25))
            }
            deleteButton.addTarget(self,
                                   action: #selector(EditorCell.deleteHandler(sender:)),
                                   for: UIControlEvents.touchUpInside)
            
            let editButton = UIButton(type: .custom)
            
            self.contentView.addSubview(editButton)
            
            editButton.setTitle("编辑", for: .normal)
            editButton.setImage(UIImage(named:"icon_edit"), for: .normal)
            editButton.backgroundColor = UIConfig.generalColor.white
            editButton.layer.masksToBounds = true
            editButton.layer.borderColor = UIConfig.generalColor.unselected.cgColor
            editButton.layer.borderWidth = 1
            editButton.layer.cornerRadius = 4
            editButton.titleLabel?.font = UIConfig.generalFont(11)
            editButton.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0)
            editButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5)
            editButton.setTitleColor(UIConfig.generalColor.unselected, for: .normal)
            editButton.addTarget(self,
                                 action: #selector(EditorCell.editHandler),
                                 for: UIControlEvents.touchUpInside)
            editButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(deleteButton.snp.left).offset(-15)
                maker.size.equalTo(CGSize(width:80, height:25))
            }
        }
        
        var item:AddressItem? {
            didSet {
                checkBox.isEnabled = !(item?.isDefault ?? false)
            }
        }
        
        func defaultHandler() {
            if let currentItem = item {
                parentVC?.itemSelectedDefault.onNext(currentItem)
            }
        }
        
        func editHandler() {
            if let currentItem = item {
                parentVC?.itemUpdated.onNext(currentItem)
            }
        }
        
        func deleteHandler(sender:UIView) {
            if let currentItem = item, let vc = parentVC {
                
                if let count = parentVC?.items.count, count > 1 {
                    let alertVC = UIAlertController(title: nil,
                                                    message: "是否要删除该地址？",
                                                    preferredStyle: .actionSheet)
                    
                    alertVC.addAction(UIAlertAction(title: "是",
                                                    style: .destructive,
                                                    handler: { [weak vc] (_) in
                                                        vc?.itemDeleted.onNext(currentItem)
                    }))
                    
                    alertVC.addAction(UIAlertAction(title: "否",
                                                    style: .cancel,
                                                    handler: nil))
                    if let popover = alertVC.popoverPresentationController {
                        popover.sourceView = sender
                        popover.sourceRect = sender.bounds
                    }
                    
                    vc.presentVC(alertVC)
                } else {
                    parentVC?.showToast(text: "剩下最后一条，不能再删了！")
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
