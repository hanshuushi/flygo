//
//  AddressVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import EZSwiftExtensions

struct AddressItem {
    var id:String
    
    var receiver:String
    
    var phone:String
    
    var province:String
    
    var city:String
    
    var district:String
    
    var detail:String
    
    var isDefault:Bool
    
    var area:String {
        return [province, city, district].filter({ $0.length > 0 }).joined(separator: " ")
    }
    
    var detailAddress:String {
        return [province, city, district, detail].filter({ $0.length > 0 }).joined(separator: " ")
    }
    
    var addressAttributedString:NSAttributedString {
        var attriString = NSMutableAttributedString()
        
        if isDefault {
            attriString = NSMutableAttributedString(string: "[默认地址]  ",
                                                    font: UIConfig.generalFont(13),
                                                    textColor: UIConfig.generalColor.red)
        }
        
        attriString.append(NSAttributedString(string: detailAddress,
                                              font: UIConfig.generalFont(13),
                                              textColor: UIConfig.generalColor.selected))
        
        return attriString
    }
    
    init(id:String,
         collection:(name:String,
        telephone:String,
        province:String,
        city:String,
        district:String,
        address:String),
         isDefault:Bool) {
        self.id = id
        self.receiver  = collection.name
        self.province  = collection.province
        self.city      = collection.city
        self.district  = collection.district
        self.phone     = collection.telephone
        self.detail    = collection.address
        self.isDefault = isDefault
    }
    
    init(model:API.Address) {
        id          = model.customerAddressId
        receiver    = model.contactName
        phone       = model.contactTel
        detail      = model.address
        province    = model.province
        city        = model.city
        district    = model.district
        isDefault   = model.isDefault
    }
    
    init(model:API.DeliverOrder) {
        id          = ""
        receiver    = model.contactName
        phone       = model.contactTel
        detail      = model.address
        province    = ""
        city        = ""
        district    = ""
        isDefault   = false
    }
}

class AddressVM: ViewModel {
    
    private let changedItemsEvent:PublishSubject<[AddressItem]>
    
    let changedItems:Driver<[AddressItem]>
    
    init() {
        changedItemsEvent = PublishSubject()
        
        changedItems = changedItemsEvent.asDriver(onErrorJustReturn: [])
    }
    
    deinit {
        print("kill AddressVM")
    }
    
    func bind(to view: AddressVC) -> DisposeBag? {
        
        let bag = DisposeBag()
        
        let request = API.Address.getList().shareReplay(1)
        
        let dataSet = TableViewDataSet.init(view.tableView,
                                            delegate: view,
                                            dataSource: view,
                                            finishStyle: .none,
                                            refreshEnable: false)
        
        dataSet.emptyDescriptionHandler = { "不添加收货地址快递会迷路哦~" }
        dataSet.emptyImageHandler = { UIImage(named:"icon_no_address")! }
        
        request.asRequestStatusObservable()
            .bind(to: dataSet.status)
            .addDisposableTo(bag)
        
        let requestItems = request
            .asModelsObservable()
            .map({$0.map({ AddressItem(model:$0)}) })
            .shareReplay(1)

        let items = requestItems.flatMap {
            [weak view](oriItems) -> Observable<[AddressItem]> in
            
            guard let strongView = view else {
                return Observable.empty()
            }
            
            return Observable.create({
                [weak strongView] (obs) -> Disposable in
                
                guard let `strongView` = strongView else {
                    return Disposables.create()
                }
                
                var currentItems = oriItems
                
                obs.onNext(currentItems)
                
                let p1 = strongView.createButton.rx.tap.flatMapLatest { () -> Observable<AddressItem> in
                    return AddressEditVC.create(in: view).take(1)
                }.bind(onNext: { (item) in
                    currentItems += [item]
                    
                    obs.onNext(currentItems)
                })
                
                let p2 = strongView.itemUpdated.flatMapLatest { (item) -> Observable<AddressItem> in
                    return AddressEditVC.update(item: item,
                                                in: view).take(1)
                }.bind(onNext: { (item) in
                    for (index, one) in currentItems.enumerated() {
                        if one.id == item.id {
                            currentItems[index] = item
                            
                            obs.onNext(currentItems)
                        }
                    }
                })
                
                let p3 = strongView
                    .itemDeleted
                    .bind(onNext: { (deletedItem) in
                        currentItems = currentItems.filter({ $0.id != deletedItem.id })
                        
                        obs.onNext(currentItems)
                    })
                
                let p4 = strongView
                    .itemSelectedDefault
                    .bind(onNext: { (updatedItem) in
                        currentItems = currentItems.map({ (oriItem) -> AddressItem in
                            var newItem = oriItem
                            
                            newItem.isDefault = (oriItem.id == updatedItem.id)
                            
                            return newItem
                        })
                        
                        obs.onNext(currentItems)
                    })
                
                return Disposables.create(p1, p2, p3, p4)
            })
        }.shareReplay(1)

        items
            .bind { [weak view] (items) in
                view?.items = items
            }
            .addDisposableTo(bag)
        
        items
            .bindNext(changedItemsEvent)
            .addDisposableTo(bag)

        items
            .map({ $0.count <= 0 })
            .bind(to: dataSet.isEmpty)
            .addDisposableTo(bag)

        view.itemDeleted.bind { [weak view] (item) in
            API.Address.delete(id: item.id).responseNoModel({
                
            }, error: {  (error) in
                ez.runThisInMainThread {
                    view?.showToast(text: error.description)
                }
            })
            }
            .addDisposableTo(bag)

        view.itemSelectedDefault.bind { [weak view] (item) in
            API.Address.selectDefault(id: item.id).responseNoModel({
                
            }, error: { (error) in
                ez.runThisInMainThread {
                    view?.showToast(text: error.description)
                }
            })
            }
            .addDisposableTo(bag)
        
        bag.insert(dataSet)
        /*
        let items = Variable<[AddressItem]>([])
        
        requestItems.bind(to: items).addDisposableTo(bag)
        
        changedItems.drive(items).addDisposableTo(bag)
        
        let list = request
            .filter({$0.isSuccess})
            .map({$0.map(transform: {AddressItem(model:$0)})})
        
        list.bind(to: items).addDisposableTo(bag)
        
        view.createButton.rx.tap.flatMapLatest { [weak view]() -> Observable<AddressItem> in
            return AddressEditVC.create(in: view).take(1)
        }.withLatestFrom(items.asObservable()) { $1 + [$0] }
            .bind(to: items)
            .addDisposableTo(bag)
        
        view.itemUpdated.flatMapLatest { [weak view](item) -> Observable<AddressItem> in
            return AddressEditVC.update(item: item,
                                        in: view).take(1)
            }.withLatestFrom(items.asObservable()) { (updatedItem, oriArray) -> [AddressItem] in
                var newArray = oriArray
                
                for (index, item) in newArray.enumerated() {
                    if item.id == updatedItem.id {
                        newArray[index] = updatedItem
                        
                        break
                    }
                }
                
                return newArray
            }
            .bindNext(changedItemsEvent)
            .addDisposableTo(bag)
        
        view.itemDeleted.withLatestFrom(items.asObservable()) { (updatedItem, oriArray) -> [AddressItem] in
            var newArray = oriArray
            
            for (index, item) in newArray.enumerated() {
                if item.id == updatedItem.id {
                    newArray.remove(at: index)
                    
                    break
                }
            }
            
            return newArray
        }
            .bindNext(changedItemsEvent)
            .addDisposableTo(bag)
        
        view.itemDeleted.bind { (item) in
            API.Address.delete(id: item.id).responseNoModel({
                
            }, error: { [weak view] (error) in
                ez.runThisInMainThread {
                    view?.showToast(text: error.description)
                }
            })
            }
            .addDisposableTo(bag)
        
        view.itemSelectedDefault.withLatestFrom(items.asObservable()) { (updatedItem, oriArray) -> [AddressItem] in
            return oriArray.map({ (oriItem) -> AddressItem in
                var newItem = oriItem
                
                newItem.isDefault = (oriItem.id == updatedItem.id)
                
                return newItem
            })
            }
            .bindNext(changedItemsEvent)
            .addDisposableTo(bag)
        
        view.itemSelectedDefault.bind { (item) in
            API.Address.selectDefault(id: item.id).responseNoModel({
                
            }, error: { [weak view] (error) in
                ez.runThisInMainThread {
                    view?.showToast(text: error.description)
                }
            })
            }
            .addDisposableTo(bag)
        
        */
        return bag
    }
    
}
