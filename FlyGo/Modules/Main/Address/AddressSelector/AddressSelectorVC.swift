//
//  AddressSelectorVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/26.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class AddressSelectorVC: UIViewController, PresenterType {
    
    let addressSelected:PublishSubject<AddressItem>
    
    var items:[AddressItem]
    
    let tableView:UITableView
    
    let viewModel:AddressSelectorVM
    
    var bindDisposeBag: DisposeBag?
    
    let disposeBag = DisposeBag()
    
    let rightBarItem:UIBarButtonItem
    
    init(viewModel:AddressSelectorVM) {
        self.viewModel = viewModel
        
        addressSelected = PublishSubject()
        
        tableView = UITableView(frame: .zero,
                                style: .plain)
        
        tableView.registerCell(cellClass: AddressCell.self)
        
        items = []
        
        rightBarItem = UIBarButtonItem(title: "管理",
                                       style: .plain,
                                       target: nil,
                                       action: nil)
        
        super.init(nibName: nil, bundle: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "请选择收货地址"
        
        self.navigationItem.rightBarButtonItem = rightBarItem
        
        self.view.addSubview(tableView)
        
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.allowsMultipleSelection = false
        tableView.allowsSelection = true
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.estimatedRowHeight = 87
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        
        bind()
        //addFakeShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AddressSelectorVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addressSelected.onNext(items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as AddressCell
        
        cell.set(item: items[indexPath.row])
        
        return cell
    }
}

extension AddressSelectorVC {
    class AddressCell: UITableViewCell {
        
        let nameLabel:UILabel
        
        let telLabel:UILabel
        
        let addressLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            nameLabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                          textColor: UIConfig.generalColor.selected)
            
            telLabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                         textColor: UIConfig.generalColor.selected)
            
            addressLabel = UILabel()
            addressLabel.numberOfLines = 0
            addressLabel.backgroundColor = UIConfig.generalColor.white
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            self.contentView.addSubviews(nameLabel, telLabel, addressLabel)
            
            nameLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(20)
            }
            
            telLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.centerY.equalTo(nameLabel)
            }
            
            addressLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(nameLabel)
                maker.right.equalTo(telLabel)
                maker.top.equalTo(nameLabel.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func set(item:AddressItem) {
            nameLabel.text = item.receiver
            
            telLabel.text = item.phone
            
            addressLabel.attributedText = item.addressAttributedString
        }
    }
}

