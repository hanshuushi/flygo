//
//  AddressSelectorVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/26.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class AddressSelectorVM: ViewModel {
    
    let addressSelected:PublishSubject<AddressItem>
    
    init() {
        addressSelected = PublishSubject()
    }
    
    func bind(to view: AddressSelectorVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        let request = API
            .Address
            .getList()
            .shareReplay(1)
        
        let dataSet = TableViewDataSet.init(view.tableView,
                                            delegate: view,
                                            dataSource: view,
                                            finishStyle: .none,
                                            refreshEnable: false)
        
        dataSet.emptyDescriptionHandler = { "不添加收货地址快递会迷路哦~" }
        dataSet.emptyImageHandler = { UIImage(named:"icon_no_address")! }
        
        let list =
            view
                .rightBarItem
                .rx
                .tap
                .map({[weak view] _ in AddressVM.items(showOn: view) })
                .startWith(request.asModelsObservable()
                    .map({$0.map({ AddressItem(model:$0) })}))
                .switchLatest()
                .shareReplay(1)
        
        list.bind { [weak view] (items) in
            view?.items = items
            }
            .addDisposableTo(bag)
        
        list
            .map({ $0.count <= 0 })
            .bind(to: dataSet.isEmpty)
            .addDisposableTo(bag)
        
        request
            .asRequestStatusObservable()
            .bind(to: dataSet.status)
            .addDisposableTo(bag)
        
        view
            .addressSelected
            .bindNext(addressSelected)
            .addDisposableTo(bag)
        
        bag.insert(dataSet)
        
        return bag
    }
}

extension AddressVM {
    static func items (showOn parent:UIViewController?) -> Observable<[AddressItem]> {
        return Observable.create({
            [weak parent] (observer) -> Disposable in
            guard let vc = parent else {
                observer.onCompleted()
                
                return Disposables.create()
            }
            
            let viewModel = AddressVM()
            
            let viewController = AddressVC(viewModel: viewModel)
            
            let p1 = viewModel.changedItems.drive(observer)
            
            vc.pushVC(viewController)
            
            return Disposables.create(p1, Disposables.create {
                viewController.popVC()
            })
        })
    }
}

extension AddressSelectorVM {
    static func addressItem (showOn parent:UIViewController?) -> Observable<AddressItem> {
        return Observable.create({
            [weak parent] (observer) -> Disposable in
            guard let vc = parent else {
                observer.onCompleted()
                
                return Disposables.create()
            }
            
            let viewModel = AddressSelectorVM()
            
            let viewController = AddressSelectorVC(viewModel: viewModel)
            
            let p1 = viewModel.addressSelected.bind(to: observer)
            
            vc.pushVC(viewController)
            
            return Disposables.create(p1, Disposables.create {
                viewController.popVC()
            })
        })
    }
}
