//
//  AddressEditVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import ObjectMapper
import EZSwiftExtensions

extension CityPickerViewController {
    static func area (showOn parent:UIViewController?,
                      defaultValue:(String, String, String)) -> Observable<AddressEditVC.Area> {
        return Observable.create({ [weak parent] (observer) -> Disposable in
            guard let parentController = parent else {
                observer.onCompleted()
                return Disposables.create()
            }
            
            let cityPickerView = CityPickerViewController(default: defaultValue,
                                                          confirmHandler:{
                (province, city, district) in
                
                observer.onNext((province: province, city: city, district: district))
                observer.onCompleted()
            })
            
            SliderPopViewController(cityPickerView,
                                    mode: .bottom,
                                    fillMode: .size,
                                    value: 300).present(on: parentController)
            
            return Disposables.create()
        })
    }
}

class AddressEditVC: UIViewController {
    
    typealias Area = (province:String, city:String, district:String)
    
    let disposeBag = DisposeBag()
    
    fileprivate let address:Driver<String>
    
    fileprivate let showCityPicker:PublishSubject<Void>
    
    fileprivate let name:Driver<String>
    
    fileprivate let telephone:Driver<String>
    
    fileprivate var area:Driver<Area>!
    
    let tableView:UITableView
    
    fileprivate let cellArray:[TextFieldCell]
    
    fileprivate let textViewCell:TextViewCell
    
    fileprivate let addressItem:AddressItem?
    
    var successAction:((AddressItem) -> Void)?
    
    fileprivate let saveButton = ViewFactory.redButton(title: "保存")
    
    init (item:AddressItem?) {
        self.addressItem = item
        
        /// init base property
        
        /// init cells
        var array = [TextFieldCell]()
        
        //// name
        let nameCell = TextFieldCell(title: "收货人：",
                                     placeHolderAttrString: NSAttributedString(string: "请输入您的姓名",
                                                                               font: UIConfig.generalFont(15),
                                                                               textColor: UIConfig.generalColor.whiteGray))

        
        nameCell.textField.tag = 1
        nameCell.textField.text = item?.receiver ?? ""
        
        name = nameCell.textField.rx.text.asDriver().startWith(nameCell.textField.text).map({ $0 ?? ""})
        
        array.append(nameCell)
        
        //// phone
        let phoneCell = TextFieldCell(title: "联系电话：",
                                      placeHolderAttrString: NSAttributedString(string: "请输入您的联系电话",
                                                                                font: UIConfig.generalFont(15),
                                                                                textColor: UIConfig.generalColor.whiteGray))

        phoneCell.textField.keyboardType = .phonePad
        phoneCell.textField.tag = 2
        phoneCell.textField.text = item?.phone ?? ""
        
        telephone = phoneCell.textField.rx.text.asDriver().startWith(phoneCell.textField.text).map({ $0 ?? "" })
        
        array.append(phoneCell)
        
        //// area
        showCityPicker = PublishSubject()
        
        let cityCell = TextFieldCell(title: "所在地区：",
                                     placeHolderAttrString: NSAttributedString(string: "请选择您所在的城市",
                                                                               font: UIConfig.generalFont(15),
                                                                               textColor: UIConfig.generalColor.whiteGray))
        
        cityCell.textField.tag = 0
        
        cityCell.textField.text = item?.area ?? ""
        
        array.append(cityCell)
        
        
        cellArray = array
        
        //// address
        textViewCell = TextViewCell()
        textViewCell.textView.text = item?.detail ?? ""
        
        address = textViewCell.textView.rx.text.asDriver().map({ $0 ?? "" })
        
        tableView = UITableView()
        
        /// super init
        super.init(nibName: nil, bundle: nil)
        
        let areaVar = Variable<Area>((province:item?.province ?? "", city:item?.city ?? "", district:item?.district ?? ""))
        
        showCityPicker.withLatestFrom(areaVar.asObservable())
            .flatMapLatest({[weak self] area in CityPickerViewController.area(showOn: self, defaultValue: (area.province, area.city, area.district)) })
            .observeOn(MainScheduler.instance).bind(to: areaVar).addDisposableTo(disposeBag)
        
        area = areaVar.asDriver()
        
        area.drive(onNext: { [weak cityCell](areaItem) in
            cityCell?.textField.text = [areaItem.province, areaItem.city, areaItem.district].filter({ $0.length > 0 }).joined(separator: " ")
        }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        /// set delegate
        nameCell.textField.delegate = self
        phoneCell.textField.delegate = self
        cityCell.textField.delegate = self
        
        setupSaveButtonObserverble()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = addressItem == nil ? "新建收货地址" : "编辑收货地址"
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.view.addSubview(tableView)
        
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.contentInset = UIEdgeInsetsMake(64, 0, 49, 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.sectionFooterHeight = 50
        tableView.keyboardDismissMode = .onDrag
        
        self.view.addSubview(saveButton)
        
        saveButton.snp.makeConstraints { (maker) in
            maker.height.equalTo(49)
            maker.right.bottom.left.equalTo(self.view)
        }
        
        //self.addFakeShadow()
    }
}

extension AddressEditVC {
    
    func setupSaveButtonObserverble () {

        let collection = Driver.combineLatest(name,
                                              telephone,
                                              area,
                                              address) {
                                                (_name, _telephone, _area, _address)
                                                -> (name:String,
                                                telephone:String,
                                                province:String,
                                                city:String,
                                                district:String,
                                                address:String)
                                                in
                                                
                                                return (name:_name,
                                                        telephone:_telephone,
                                                        province:_area.province,
                                                        city:_area.city,
                                                        district:_area.district,
                                                        address:_address)
        }
        
        saveButton
            .rx
            .tap
            .withLatestFrom(collection.asObservable())
            .bind { [weak self] (collection) in
                
                guard let strongSelf = self else {
                    return
                }
                
                /// condition
                if collection.name.length <= 0 {
                    strongSelf.showAlert(message: "请填写收货人的名字",
                                         responser: strongSelf
                                            .cellArray[0]
                                            .textField)
                    
                    return
                }
                
                if collection.name.length > 10 {
                    strongSelf.showAlert(message: "收货人名字不宜过长",
                                         responser: strongSelf
                                            .cellArray[0]
                                            .textField)
                    
                    return
                }
                
                if collection.telephone.length <= 0 {
                    strongSelf.showAlert(message: "请填写联系电话",
                                         responser: strongSelf
                                            .cellArray[1]
                                            .textField)
                    
                    return
                }
                
                if !collection.telephone.isTel() {
                    strongSelf.showAlert(message: "联系电话不符合规范",
                                         responser: strongSelf
                                            .cellArray[1]
                                            .textField)
                    
                    return
                }
                
                if !(collection.province.length > 0 || collection.city.length > 0 || collection.district.length > 0) {
                    strongSelf.showAlert(message: "请选择所在地区",
                                         responserHandler: {
                                            strongSelf.showCityPicker.onNext()
                    })
                    
                    return
                }
                
                if collection.address.length <= 0 {
                    strongSelf.showAlert(message: "请填写您的详细地址",
                                         responser: strongSelf
                                            .textViewCell
                                            .textView)
                    
                    return
                }
                
                if collection.address.length > 70 {
                    strongSelf.showAlert(message: "地址长度不能长于70个字",
                                         responser: strongSelf
                                            .textViewCell
                                            .textView)
                    
                    return
                }
                
                /// request
                let loadingView = strongSelf.showLoading()
                
                let errorHandler = {
                    (error:HttpError) -> Void in
                    
                    ez.runThisInMainThread {
                        loadingView.displayLabel(text: error.description)
                    }
                }
                
                if let item = strongSelf.addressItem {
                    API.Address.update(id: item.id,
                                       province:collection.province,
                                       city:collection.city,
                                       district:collection.district,
                                       address: collection.address,
                                       contactName: collection.name,
                                       contactTel: collection.telephone,
                                       isDefault: item.isDefault).responseNoModel({
                                        ez.runThisInMainThread {
                                            loadingView.dismiss(animated: true,
                                                                completion: {
                                                                    let item = AddressItem(id: item.id,
                                                                                           collection: collection,
                                                                                           isDefault: item.isDefault)
                                                                    
                                                                    strongSelf.successAction?(item)
                                            })
                                        }
                                        
                                       }).error(errorHandler)
                } else {
                    API.Address.create(province:collection.province,
                                       city:collection.city,
                                       district:collection.district,
                                       address: collection.address,
                                       contactName: collection.name,
                                       contactTel: collection.telephone).responseModel({ (model:API.Address) in
                                        loadingView.dismiss(animated: true,
                                                            completion: {
                                            let item = AddressItem(model: model)
                                            
                                            strongSelf.successAction?(item)
                                        })
                                       }, error: errorHandler)
                }
        }.addDisposableTo(disposeBag)
    }
}

extension AddressEditVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            showCityPicker.onNext(())
            
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            
            return false
        }
        
        return true
    }
}

extension AddressEditVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == cellArray.count {
            return 130
        }
        
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.backgroundView?.backgroundColor = UIColor.clear
            
            headerFooterView.contentView.backgroundColor = UIColor.clear
            
        }
        
        view.backgroundColor = UIConfig.generalColor.white
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == cellArray.count {
            return textViewCell
        }
        
        return cellArray[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension AddressEditVC {
    class CheckBoxFooterView: UITableViewCell {
        
        let checkBox = {
            () -> UIButton in
            
            let checkBox = UIButton(type: .custom)
            
            checkBox.setImage(UIImage(named:"icon_isnt_default_address"),
                              for: .normal)
            checkBox.setTitleColor(UIConfig.generalColor.unselected,
                                   for: .normal)
            checkBox.setTitle("设为默认地址", for: .normal)
            checkBox.setImage(UIImage(named:"icon_default_address"),
                              for: .selected)
            checkBox.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
            checkBox.setTitle("默认地址", for: .selected)
            checkBox.setTitleColor(UIConfig.generalColor.red,
                                   for: .selected)
            checkBox.backgroundColor = UIConfig.generalColor.white
            checkBox.titleLabel?.font = UIConfig.generalFont(12)
            checkBox.sizeToFit()
            checkBox.contentHorizontalAlignment = .left
            
            return checkBox
        }()
        
        init () {
            super.init(style: .default, reuseIdentifier: "TextView")
            
            self.contentView.addSubview(checkBox)
            
            let backgroudView = UIView()
            
            backgroudView.backgroundColor = UIColor.blue
            
            self.backgroundView = backgroundView
            
            checkBox.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(25)
                maker.size.equalTo(CGSize(width:100, height:30))
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class TextViewCell: UITableViewCell, UITextViewDelegate {
        let textView:KMPlaceholderTextView
        
        let disposeBag:DisposeBag
        
        init () {
            disposeBag = DisposeBag()
            
            textView = KMPlaceholderTextView(frame: .zero)
            
            super.init(style: .default, reuseIdentifier: "TextView")
            
            textView.font = UIConfig.generalFont(15)
            textView.textColor = UIConfig.generalColor.selected
            textView.placeholder = "请输入您的详细地址，最多70字"
            textView.placeholderFont = UIConfig.generalFont(15)
            textView.placeholderColor = UIConfig.generalColor.whiteGray
            textView.textContainerInset = UIEdgeInsets.zero
            textView.returnKeyType = .done
            textView.delegate = self
            
            self.contentView.addSubview(textView)
            
            textView.snp.makeConstraints { (maker) in
                
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(15, 10, 25, 10))
            }
            
            let tip = UILabel()
            
            tip.backgroundColor = textView.backgroundColor
            tip.font = UIConfig.generalFont(10)
            
            let count = textView
                .rx.text.map{70 - ($0?.length ?? 0)}
            
            let color = count.map{ $0 >= 0 ? UIConfig.generalColor.unselected : UIConfig.generalColor.red }
         
            color.bind { (color) in
                tip.textColor = color
            }.addDisposableTo(disposeBag)
            
            count.map{String($0)}.bind(to: tip.rx.text).addDisposableTo(disposeBag)
            
            self.contentView.addSubview(tip)
            
            tip.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.bottom.equalTo(self.contentView).offset(-5)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
                
                return false
            }
            
            return true
        }
    }
    
    class TextFieldCell: UITableViewCell {
        let textField:UITextField
        
        let label:UILabel
        
        init(title: String, placeHolderAttrString: NSAttributedString) {
            
            textField = UITextField()
            
            textField.backgroundColor = UIConfig.generalColor.white
            textField.font = UIConfig.generalFont(15)
            textField.textColor = UIConfig.generalColor.selected
            textField.attributedPlaceholder = placeHolderAttrString
            textField.returnKeyType = .done
            
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "TextField")
            
            self.contentView.addSubview(label)
            
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.backgroundColor = UIConfig.generalColor.white
            self.label.text = title
            self.label.sizeToFit()
            self.label.snp.makeConstraints({ (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.centerY.equalTo(self.contentView)
                maker.size.equalTo(self.label.size)
            })
            
            self.contentView.addSubview(textField)
            
            textField.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.label.snp.right)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.centerY.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension AddressEditVC {
    static func create(in parentController:UIViewController?,
                animated:Bool = true) -> Observable<AddressItem> {
        return Observable.create({ [weak parentController] observer -> Disposable in
            let viewController = AddressEditVC(item: nil)
            
            guard let navigationController = parentController?.navigationController else {
                observer.onCompleted()
                
                return Disposables.create()
            }
            
            navigationController.pushViewController(viewController, animated: true)
            
            viewController.successAction = {
                item in
                
                observer.onNext(item)
                observer.onCompleted()
            }
            
            return Disposables.create {
                [weak viewController] () in
                
                let _ = viewController?.popVC()
            }
        })
    }
    
    static func update(item:AddressItem,
                       in parentController:UIViewController?,
                       animated:Bool = true) -> Observable<AddressItem> {
        return Observable.create({ [weak parentController] observer -> Disposable in
            let viewController = AddressEditVC(item: item)
            
            guard let navigationController = parentController?.navigationController else {
                observer.onCompleted()
                
                return Disposables.create()
            }
            
            navigationController.pushViewController(viewController, animated: true)
            
            viewController.successAction = {
                item in
                
                observer.onNext(item)
                observer.onCompleted()
            }
            
            return Disposables.create {
                [weak viewController] () in
                
                let _ = viewController?.popVC()
            }
        })
    }
}
