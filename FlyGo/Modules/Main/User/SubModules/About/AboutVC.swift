//
//  AboutVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/10.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import React

class AboutVC: UITableViewController {
    
    typealias InfoTableViewCell = SettingVC.InfoTableViewCell
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "飞购"
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.registerCell(cellClass: InfoTableViewCell.self)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as InfoTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.set(title: "关于知游",
                     info: "",
                     showAccess: false)
        case 1:
            cell.set(title: "关于飞购",
                     info: "",
                     showAccess: false)
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
//            let url = URL(string: URLConfig.prefix + "flyapp/fly/aboutzy.html")!
//            
//            let viewController = WKWebViewController(url: url)
//            
//            viewController.title = "关于知游"
//            
//            self.navigationController?.pushViewController(viewController, animated: true)
            let vc = UIViewController(nibName: nil,
                                      bundle: nil)
            
            vc.automaticallyAdjustsScrollViewInsets = false
            
            let url = URL(string: Bundle.main.bundlePath + "/about/aboutZhiyou.jsbundle")!
            
            let bridge = RCTBridge(bundleURL: url,
                                   moduleProvider: { () -> [RCTBridgeModule]? in
                                    return nil
            },
                                   launchOptions: nil)
            
            let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "1.0.0"
            
            let root:RCTRootView = RCTRootView(bridge: bridge,
                                               moduleName: "AboutZhiyou",
                                               initialProperties: ["version":version])
            vc.title = "关于知游"
            vc.view.addSubview(root)
            
            root.snp.fillToSuperView()
            
            //            let url = URL(string: URLConfig.prefix + "flyapp/fly/aboutfly.html")!
            //
            //            let viewController = WKWebViewController(url: url)
            //
            //            viewController.title = "关于飞购"
            //
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = UIViewController(nibName: nil,
                                      bundle: nil)
            
            let url = URL(string: Bundle.main.bundlePath + "/about/aboutFlygo.jsbundle")!
            
            let bridge = RCTBridge(bundleURL: url,
                                   moduleProvider: { () -> [RCTBridgeModule]? in
                                    return nil
            },
                                   launchOptions: nil)
            
            let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "1.0.0"
            
            let root:RCTRootView = RCTRootView(bridge: bridge,
                                               moduleName: "AboutFlygo",
                                               initialProperties: ["version":version])
            vc.title = "关于飞购"
            vc.view.addSubview(root)
            
            root.snp.fillToSuperView()
            
//            let url = URL(string: URLConfig.prefix + "flyapp/fly/aboutfly.html")!
//            
//            let viewController = WKWebViewController(url: url)
//            
//            viewController.title = "关于飞购"
//            
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return ViewFactory.view(color: UIColor.white)
    }
}
