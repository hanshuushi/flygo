//
//  CallServerVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/14.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import WebKit

class CallServerVC: WKWebViewController {
    init() {
        let urlString = URLConfig.prefix + "flyapp/fly/user.html?flag=1#!/customerService.html"
        
        super.init(url:URL(string:urlString)!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "联系客服"
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        webView.scrollView.setInset(top: 0, bottom: 0)
        webView.scrollView.bounces = false
        webView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsetsMake(20, 0, 0, 0))
        }
        
        progressView.snp.remakeConstraints { (maker) in
            maker.left.right.equalTo(self.view)
            maker.top.equalTo(self.view).offset(20)
            maker.height.equalTo(3)
        }
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override var coverView: UIView? {
        set {
            super.coverView = newValue
            
            if let cv = newValue {
                cv.snp.remakeConstraints ({ (maker) in
                    maker.centerX.width.equalTo(self.view)
                    maker.size.equalTo(self.view)
                    maker.centerY.equalTo(self.view)
                })
            }
        }
        get {
            return super.coverView
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
