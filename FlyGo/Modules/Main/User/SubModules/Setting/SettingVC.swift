//
//  SettingVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/26.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import Kingfisher
import EZSwiftExtensions

class SettingVC: UITableViewController {
    
    typealias InfoTableViewCell = UserAccountVC.InfoTableViewCell
    
    init() {
        super.init(style: .plain)
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        tableView.tableFooterView = versionLabel
        tableView.registerCell(cellClass: InfoTableViewCell.self)
    }
    
    override init(style: UITableViewStyle) {
        super.init(style: .plain)
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        tableView.tableFooterView = versionLabel
        tableView.registerCell(cellClass: InfoTableViewCell.self)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        tableView.tableFooterView = versionLabel
        tableView.registerCell(cellClass: InfoTableViewCell.self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "设置"
    }
    
    let versionLabel:UILabel = {
        let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                      textColor: UIConfig.generalColor.unselected)
        
        label.text = "当前版本号:\(VersionManager.version) (Build \(VersionManager.build))"
        label.textAlignment = .center
        label.sizeToFit()
        label.h = 50
        
        return label
    }()
}

extension SettingVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
#if AdHoc || DEBUG
        return 6
#else
        return 5
#endif
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as InfoTableViewCell
        
        switch indexPath.row {
        case 0:
            let info = Bundle.main.infoDictionary
            
            let version = info?["CFBundleShortVersionString"].flatMap({ $0 as? String }) ?? "1.0.0"
            
            cell.set(title: "版本信息",
                     info: version,
                     showAccess: false)
        case 1:
            cell.set(title: "清理图片缓存",
                     info: "\(String(format:"%.2f", SettingVC.folderSizeAtPath(path: KingfisherManager.shared.cache.diskCachePath)))M",
                     showAccess: true)
        case 2:
            cell.set(title: "用户协议",
                     info: "",
                     showAccess: true)
        case 3:
            cell.set(title: "关于",
                     info: "",
                     showAccess: true)
        case 4:
            cell.set(title: "意见反馈",
                     info: "",
                     showAccess: true)
        case 5:
            
            var serverName = "开发"
            
            if let value = UserDefaults.standard.value(forKey: "AdHoc-URLConfig") as? String, value == "test" {
                serverName = "测试"
            } else if let value = UserDefaults.standard.value(forKey: "AdHoc-URLConfig") as? String, value == "release" {
                serverName = "线上"
            }
            
            cell.set(title: "当前:\(serverName)服务器，点击切换",
                     info: "",
                     showAccess: true)
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            
            let alertController = UIAlertController(title: nil,
                                                    message: "是否要清理缓存",
                                                    preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "否",
                                                    style: .cancel,
                                                    handler: nil))
            alertController.addAction(UIAlertAction(title: "是",
                                                    style: .default,
                                                    handler: { [weak self](_) in
                                                        KingfisherManager.shared.cache.clearDiskCache(completion: {
                                                            self?.tableView.reloadRows(at: [indexPath],
                                                                                       with: .automatic)
                                                        })
                                                        KingfisherManager.shared.cache.clearMemoryCache()
            }))
            
            self.presentVC(alertController)
        case 2:
            let urlString = URLConfig.prefix + "flyapp/login.html#!/agreement.html"
            
            guard let url = URL(string: urlString) else {
                return
            }
            
            let webViewController = WKWebViewController(url: url)
            
            webViewController.title = "用户协议"
            
            self.navigationController?.pushViewController(webViewController, animated: true)
        case 3:
            let aboutViewController = AboutVC(style: .plain)
            
            self.navigationController?.pushViewController(aboutViewController, animated: true)
        case 4:
            LoginVC.doActionIfNeedLogin {
                self
                    .navigationController?
                    .pushViewController(FeedbackVC(),
                                        animated: true)
            }
        case 5:
            
            let sheetVC = UIAlertController(title: "切换服务器",
                                            message: "切换后，会自动退出App，请重新打开",
                                            preferredStyle: .actionSheet)
            
            sheetVC.addAction(UIAlertAction(title: "测试",
                                            style: .default,
                                            handler: { (_) in
                                                UserDefaults.standard.set("test", forKey: "AdHoc-URLConfig")
                                                UserDefaults.standard.synchronize()
                                                
                                                ez.runThisAfterDelay(seconds: 1.0,
                                                                     after: {
                                                                        exit(0)
                                                })
                                                
            }))
            sheetVC.addAction(UIAlertAction(title: "线上",
                                            style: .default,
                                            handler: { (_) in
                                                UserDefaults.standard.set("release", forKey: "AdHoc-URLConfig")
                                                UserDefaults.standard.synchronize()
                                                
                                                ez.runThisAfterDelay(seconds: 1.0,
                                                                     after: {
                                                                        exit(0)
                                                })
            }))
            sheetVC.addAction(UIAlertAction(title: "开发",
                                            style: .default,
                                            handler: { (_) in
                                                UserDefaults.standard.removeObject(forKey: "AdHoc-URLConfig")
                                                UserDefaults.standard.synchronize()
                                                
                                                ez.runThisAfterDelay(seconds: 1.0,
                                                                     after: {
                                                                        exit(0)
                                                })
            }))
            
            if let popover = sheetVC.popoverPresentationController, let cell = tableView.cellForRow(at: indexPath) {
                popover.sourceView = cell
                popover.sourceRect = cell.bounds
            }
            
            self.navigationController?.presentVC(sheetVC)
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return ViewFactory.view(color: UIColor.white)
    }
}

extension SettingVC {
    static func folderSizeAtPath(path:String) -> Double {
        let manager = FileManager.default
        
        var size = 0.0
        
        if !manager.fileExists(atPath: path) {
            return size
        }
        
        guard let subpaths = manager.subpaths(atPath: path) else {
            return size
        }
        
        for fileName in subpaths {
            let path = path + "/\(fileName)"
            
            let currentSize = (try? manager.attributesOfItem(atPath: path)[FileAttributeKey.size] as? Double)?.flatMap{$0} ?? 0
            
            size += currentSize
        }
        
        return size / 1024.0 / 1024.0
    }
    
    static func cleanCaches(path:String) {
        let manager = FileManager.default
        
        if !manager.fileExists(atPath: path) {
            return
        }
        
        guard let subpaths = manager.subpaths(atPath: path) else {
            return
        }
        
        for fileName in subpaths {
            let path = path + "/\(fileName)"
            
            try? manager.removeItem(atPath: path)
        }
    }
}
