//
//  UserVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/19.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class UserVM: ViewModel {
    
    let nickName:Driver<String>
    
    let isLogined:Driver<Bool>
    
    let avatar:Driver<URL?>
    
    init() {
        isLogined = UserManager.shareInstance.isLogin.asDriver()
        
        nickName = Driver.combineLatest(UserManager.shareInstance.nickName.asDriver(),
                                        isLogined,
                                        resultSelector: { (name, logined) -> String in
                                            if !logined {
                                                return "点击登录"
                                            }
                                            
                                            if name.length <= 0 {
                                                return "请选择您的昵称"
                                            }
                                            
                                            return name
        })
        
        avatar = UserManager.shareInstance.avatar.asDriver()
    }
    
    func bind(to view: UserVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        nickName
            .drive(view.nickName)
            .addDisposableTo(bag)
        
        isLogined
            .drive(view.isLogined)
            .addDisposableTo(bag)
        
        avatar
            .drive(view.avatar)
            .addDisposableTo(bag)
        
        view
            .orderCell
            .linkSelected
            .bind {
                [weak view] (index) in
                print("linkSelected In UserVM")
                
                LoginVC.doActionIfNeedLogin {
                    let vc = OrderVC(defaultPage: index)
                    
                    view?.navigationController?.pushViewController(vc,
                                                                   animated: true)
                }
        }.addDisposableTo(bag)
        
        view
            .tableView
            .rx
            .itemSelected
            .bind {
                [weak view] (indexPath) in
                
                if indexPath.row == 7 {
                    FlygoUtil.callServer()
                    
                    return
                } else if indexPath.row == 6 {
                    view?
                        .navigationController?
                        .pushViewController(SettingVC(),
                                            animated: true)
                    
                    return
                }
                
                let lognedAction = {
                    switch indexPath.row {
                    case 0:
                        let vm = UserAccountVM()
                        
                        view?
                            .navigationController?
                            .pushViewController(UserAccountVC(viewModel: vm),
                                                animated: true)
                    case 1:
                        view?.navigationController?
                            .pushViewController(OrderVC(),
                                                animated: true)
                    case 2:
                        MainVC.switchRoot()
                    case 3:
                        view?.navigationController?
                            .pushViewController(CouponListVC(),
                                                animated: true)
                    case 4:
                        let urlString = "\(URLConfig.prefix)activity/coupon/share.html?customerId=\(UserManager.shareInstance.currentId.urlEncode())&nickname=\(UserManager.shareInstance.currentNickName.urlEncode())&avatar=\(UserManager.shareInstance.avatarURL?.absoluteString.urlEncode() ?? "")"
                        
                        guard let url = URL(string: urlString) else {
                            return
                        }
                        
                        let vc = WKWebViewController(url: url)
                        
                        vc.title = "分享领劵"
                        
                        view?
                            .navigationController?
                            .pushViewController(vc,
                                                animated: true)
                    case 5:
                        let vm = AddressVM()
                        
                        view?
                            .navigationController?
                            .pushViewController(AddressVC(viewModel: vm),
                                                animated: true)
                    case 8:
                        view?
                            .navigationController?
                            .pushViewController(FeedbackVC(),
                                                animated: true)
                        
                    default:
                        return
                    }
                }
                
                print("linkSelected In itemSelected")
                
                LoginVC.doActionIfNeedLogin(action: lognedAction)
                
            }
            .addDisposableTo(bag)
        
        return bag
    }
    
}
