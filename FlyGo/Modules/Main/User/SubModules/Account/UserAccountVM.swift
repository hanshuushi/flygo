//
//  UserAccountVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/19.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class UserAccountVM: ViewModel {
    
    let nickName:Driver<String>
    
    let telephone:Driver<String>
    
    let avatar:Driver<URL?>
    
    init() {
        nickName = UserManager.shareInstance.nickName.asDriver().map({ (name) -> String in
            if name.length <= 0 {
                return "请选择您的昵称"
            }
            
            return name
        })
        
        telephone = UserManager.shareInstance.telephone.asDriver()
        
        avatar = UserManager.shareInstance.avatar.asDriver()
    }
    
    func bind(to view: UserAccountVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        nickName
            .drive(view.nickName)
            .addDisposableTo(bag)
        
        telephone
            .drive(view.telephone)
            .addDisposableTo(bag)
        
        avatar
            .drive(view.avatar)
            .addDisposableTo(bag)
        
        return bag
    }
    
}
