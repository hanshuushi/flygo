//
//  UserAccountVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/19.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class UserAccountVC: UIViewController, PresenterType {
    
    let viewModel:UserAccountVM
    
    var bindDisposeBag: DisposeBag?
    
    let disposeBag:DisposeBag
    
    let tableView:UITableView
    
    let loginOutButton:UIButton
    
    let infoCells:[UITableViewCell]
    
    init(viewModel:UserAccountVM) {
        loginOutButton = ViewFactory.redButton(title: "退出登录")
        loginOutButton.layer.cornerRadius = 22
        loginOutButton.layer.masksToBounds = true
        
        let footerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: UIScreen.main.bounds.width,
                                              height: 80))
        
        footerView.addSubview(loginOutButton)
        
        let line = ViewFactory.view(color: UIConfig.generalColor.white)
        
        footerView.addSubview(line)
        
        line.frame = CGRect(x: 0, y: -2, width: UIScreen.main.bounds.width, height: 2)
        line.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin]
        
        loginOutButton.frame = CGRect(x: 30,
                                      y: 36,
                                      width: footerView.w - 60,
                                      height: 44)
        
        loginOutButton.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        
        self.viewModel = viewModel
        
        /// init table view
        tableView = UITableView(frame: .zero,
                                style: .plain)
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
//        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
//        tableView.estimatedSectionHeaderHeight = 110
        tableView.tableFooterView = footerView
        
        /// init disposebag
        disposeBag = DisposeBag()
        
        /// init variable
        nickName = Variable("")
        
        telephone = Variable("")
        
        avatar = Variable(nil)
        
        /// init cell
        var tempArray = [UITableViewCell]()
        
        let avatarCell = ImageTableViewCell(style: .default,
                                            reuseIdentifier: "avatar")
        
        avatarCell.label.text = "头像"
        
        avatar
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { (picUrl) in
                avatarCell.infoImageView.setAvatar(url: picUrl)
            }
            .addDisposableTo(disposeBag)
        
        tempArray.append(avatarCell)
        
        let phoneCell = InfoTableViewCell(style: .default,
                                          reuseIdentifier: "phoneCell")
        
        phoneCell.set(title: "手机号",
                      info: telephone.value,
                      showAccess: false)
        
        tempArray.append(phoneCell)
        
        telephone
            .asObservable()
            .bind(to: phoneCell.infoLabel.rx.text)
            .addDisposableTo(disposeBag)
        
        let nameCell = InfoTableViewCell(style: .default,
                                         reuseIdentifier: "nameCell")
        
        nameCell.set(title: "更改昵称",
                     info: nickName.value,
                     showAccess: true)
        
        tempArray.append(nameCell)
        
        nickName
            .asObservable().bind(onNext: { (nickName) in
                nameCell.set(title: "更改昵称",
                             info: nickName,
                             showAccess: true)
            })
            .addDisposableTo(disposeBag)
        
        let passwordCell = InfoTableViewCell(style: .default,
                                             reuseIdentifier: "passwordCell")
        
        passwordCell.set(title: "重置密码",
                         info: "",
                         showAccess: true)
        
        tempArray.append(passwordCell)
        
        infoCells = tempArray
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nickName:Variable<String>
    
    let telephone:Variable<String>
    
    let avatar:Variable<URL?>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "账户管理"
        
        loginOutButton.addTarget(self,
                                 action: #selector(UserAccountVC.logoutHandler),
                                 for: UIControlEvents.touchUpInside)
        
        /// add table view
        self.view.addSubview(tableView)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        
        //addFakeShadow()
        
        Observable
            .of(nickName.asObservable(), telephone.asObservable())
            .merge()
            .bind { [weak self] _ in
                self?.tableView.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        /// login out button
        bind()
    }
}

extension UserAccountVC {
    func logoutHandler () {
        let alert = UIAlertController(title: "",
                                      message: "您是否要登出该账户？",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "否",
                                      style: .cancel,
                                      handler: nil))
        alert.addAction(UIAlertAction(title: "是",
                                      style: .default){
                                        [weak self] (_) -> Void in
                                        let _ = self?.navigationController?.popToRootViewController(animated: true)
                                        
                                        API.UserInfo.loginOut(imei: OpenUDID.value()).responseNoModel({
                                            print("已登出")
                                        })
                                        
                                        UserManager.shareInstance.logout()
                                        
                                        CartVM.refreshNotification()
        })
        
        self.presentVC(alert)
    }
}

extension UserAccountVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return infoCells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 2:
            let viewController = NicknameInputVC()
            
            self.pushVC(viewController)
        case 0:
            ImagePicker.updateAvatar(show: self,
                                     response: { (_, _, _) in
                                        
            })
        case 3:
            let viewController = MobileSMSInputVC()
            
            viewController.isRegister = false
            
            
            self.pushVC(viewController)
        default:
            return
        }
    }
}

extension UserAccountVC {
    class InfoTableViewCell: UITableViewCell {
        
        let infoLabel:UILabel
        
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            infoLabel = UILabel()
            
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "InfoTableViewCell")
            
            self.contentView.addSubviews(infoLabel, label)
            
            self.selectionStyle = .none
            
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.backgroundColor = UIColor.clear
            self.label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.bottom.equalTo(self.contentView).offset(-20)
                maker.left.equalTo(self.contentView).offset(30)
            })
            
            
            infoLabel.font = UIConfig.arialFont(15)
            infoLabel.textColor = UIConfig.generalColor.unselected
            infoLabel.backgroundColor = self.label.backgroundColor
            infoLabel.snp.makeConstraints({ (maker) in
                maker.top.equalTo(self.label)
                maker.bottom.equalTo(self.label)
                maker.right.equalTo(self.contentView).offset(-30)
            })
        }
        
        func set(title:String, info:String, showAccess:Bool) {
            
            self.label.text = title
            
            let attr = NSMutableAttributedString(string: info + (showAccess ? " " : ""),
                                                 font: infoLabel.font,
                                                 textColor: infoLabel.textColor)
            
            if showAccess {
                
                attr.addAttributes([NSBaselineOffsetAttributeName:1],
                                   range: NSMakeRange(0, info.length))
                
                let attach = NSTextAttachment()
                
                attach.image = UIImage(named:"icon_more-brand")
                
                attr.append(NSAttributedString(attachment: attach))
            }
            
            infoLabel.attributedText = attr
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}


extension UserAccountVC {
    class ImageTableViewCell: UITableViewCell {
        
        let infoImageView:UIImageView
        
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            infoImageView = UIImageView()
            
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "ImageTableViewCell")
            
            self.contentView.addSubviews(infoImageView, label)
            
            self.selectionStyle = .none
            
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.backgroundColor = UIColor.clear
            self.label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.bottom.equalTo(self.contentView).offset(-20)
                maker.left.equalTo(self.contentView).offset(30)
            })
            
            let access = UIImageView(named: "icon_more-brand")
            
            self.contentView.addSubview(access)
            
            access.snp.makeConstraints({ (maker) in
                maker.right.equalTo(self.contentView).offset(-30)
                maker.centerY.equalTo(self.contentView)
//                access.size.equalTo(access.size)
            })
            
            infoImageView.snp.makeConstraints({ (maker) in
                maker.height.width.equalTo(35)
                maker.right.equalTo(access.snp.left).offset(-20)
                maker.top.equalTo(self.contentView).offset(10)
                maker.bottom.equalTo(self.contentView).offset(-10)
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
