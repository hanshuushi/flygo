//
//  FeedbackVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/26.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class FeedbackVC: UIViewController {
    
    let tableView:UITableView
    
    let addressCell:AddressTableViewCell
    
    var addressItem:AddressItem? {
        didSet {
            addressCell.item = addressItem
            
            self.tableView.reloadData()
        }
    }
    
    let feedbackCell:TextViewTableViewCell
    
    let disposeBag = DisposeBag()
    
    let submitButton:UIButton
    
    init() {
        submitButton = ViewFactory.redButton(title: "确认提交")
        
        tableView = UITableView(frame: .zero,
                                style: .grouped)
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.separatorStyle = .none
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        tableView.keyboardDismissMode = .onDrag
        
        addressCell = AddressTableViewCell(style: .default,
                                           reuseIdentifier: "AddressTableViewCell")
        
        feedbackCell = TextViewTableViewCell(style: .default,
                                             reuseIdentifier: "TextViewTableViewCell")
        
        super.init(nibName: nil, bundle: nil)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        bindAllOberserver()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        self.title = "意见反馈"
        
        self.view.addSubview(tableView)
        
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.setInset(top: 64, bottom: 49)
        
        self.view.addSubview(submitButton)
        
        submitButton.snp.makeConstraints { (maker) in
            maker.left.right.bottom.equalTo(self.view).offset(0)
            maker.height.equalTo(49)
        }
        submitButton.addTarget(self,
                               action: #selector(FeedbackVC.submit),
                               for: UIControlEvents.touchUpInside)
    }
}

extension FeedbackVC {
    func submit() {
        guard let currentAddress = self.addressItem else {
            self.showAlert(message: "请选择您的收货地址",
                           responserHandler: {
                            
            })
            
            return
        }
        
        guard let content = self.feedbackCell.textView.text, content.length > 0 else {
            self.showAlert(message: "请告诉我们您的意见",
                           responser: self.feedbackCell.textView)
            
            return
        }
        
        let submit = API.UserInfo.feedBack(addressId: currentAddress.id,
                                           content: content)
        
        let toast = self.showLoading()
        
        submit.bind { (item) in
            switch item {
            case .failed(let error):
                toast.displayLabel(text: error)
            case .success:
                toast.dismiss(animated: false,
                              completion: {
                                let _ = self.navigationController?.popViewController(animated: true)
                                self.navigationController?.showToast(text: "感谢您提供给我们的意见^_^")
                })
            default:
                return
            }
        }.addDisposableTo(disposeBag)
    }
}

extension FeedbackVC {
    func bindAllOberserver() {
        /// adress
        let defaultAddress = API
            .Address
            .getList()
            .filter{$0.isSuccess}
            .map{$0.model?.list ?? []}.map { (items) -> API.Address? in
                for one in items {
                    if one.isDefault {
                        return one
                    }
                }
                
                return items.first
            }
            .map({ $0.map({ AddressItem(model:$0) })})
            .startWith(nil)
            .shareReplay(1)
        
        let addressSelected = tableView
            .rx
            .itemSelected
            .filter{ $0.section == 0 && $0.row == 0 }
            .map({[weak self] _ in AddressSelectorVM
                .addressItem(showOn: self)
                .take(1).map({ (item) -> AddressItem? in return item}) })
            .startWith(defaultAddress)
            .switchLatest()
            .shareReplay(1)
        
        addressSelected.bind { [weak self] (item) in
            self?.addressItem = item
        }.addDisposableTo(disposeBag)
    }
}

extension FeedbackVC {
    class TextViewTableViewCell: UITableViewCell, UITextViewDelegate {
        let textView:KMPlaceholderTextView
        
        static func height(with width:CGFloat, text:String) -> CGFloat {
            
            let textWidth = width - 30
            
            let size = CGSize(width: textWidth, height: 1000000)
            
            let placeHolderAttr = NSAttributedString(string: "请在此留下您的意见，我们将不断进步~为您做的更好",
                                                     font: UIConfig.generalFont(12),
                                                     textColor: UIConfig.generalColor.whiteGray)
            
            let placeHolderHeight = placeHolderAttr.boundingRect(with: size,
                                                                 options: .usesLineFragmentOrigin,
                                                                 context: nil).height
            
            let textAttr = NSAttributedString(string: text,
                                              font: UIConfig.generalFont(12),
                                              textColor: UIConfig.generalColor.selected)
            
            let height = textAttr.boundingRect(with: size,
                                               options: .usesLineFragmentOrigin,
                                               context: nil).height
            
            return max(height, placeHolderHeight) + 40
        }
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            textView = KMPlaceholderTextView()
            textView.font = UIConfig.generalFont(12)
            textView.textColor = UIConfig.generalColor.selected
            textView.placeholder = "请在此留下您的意见，我们将不断进步~为您做的更好"
            textView.placeholderFont = textView.font
            textView.placeholderColor = UIConfig.generalColor.whiteGray
            textView.keyboardDismissMode = .onDrag
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(textView)
            
            textView.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(20, 15, 20, 15))
                maker.height.greaterThanOrEqualTo(150)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension FeedbackVC:UITableViewDelegate, UITableViewDataSource {
    
    typealias AddressTableViewCell = CartConfirmVC.AddressTableViewCell
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section != 0 { return CGFloat.leastNormalMagnitude }
        
        return 47
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section != 0 { return nil }
        
        let view = UIView(x: 0, y: 0, w: 1, h: 40)
        
        view.backgroundColor = UIConfig.generalColor.white
        
        let lineImageView = UIImageView(named: "background_rules")
        
        let label = ViewFactory.label(font: UIConfig.generalFont(12),
                                      textColor: UIConfig.generalColor.labelGray)
        
        label.text = "请选择收货地址，我们有小礼品相送哦~"
        
        view.addSubviews(label, lineImageView)
        
        label.snp.makeConstraints { (maker) in
            maker.left.equalTo(view).offset(15)
            maker.centerY.equalTo(view).offset(-lineImageView.h)
        }
        
        lineImageView.snp.makeConstraints { (maker) in
            maker.left.right.bottom.equalTo(view)
            maker.height.equalTo(lineImageView.h)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != 0 { return CGFloat.leastNormalMagnitude }
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section != 0 { return nil }
        
        return ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return AddressTableViewCell.height(with: tableView.w,
                                               and: addressItem)
        } else if indexPath.section == 1 && indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == 0 {
            return 70
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return addressCell
        } else if indexPath.section == 1 {
            return feedbackCell
        }
        
        fatalError()
    }
    
    
}
