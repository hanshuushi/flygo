//
//  InputVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class InputVC: UIViewController {
    
    let tableView = {
        () -> UITableView in
        
        let tableView = UITableView()
        
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.allowsMultipleSelection = false
        tableView.allowsSelection = false
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.rowHeight = 55
        tableView.backgroundColor = UIConfig.generalColor.white
        
        return tableView
    }()
    
    var cellList:[InputTableViewCell] = []
    
    let saveButton = ViewFactory.redButton(title: "保存")
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = true
        
        super.viewDidLoad()
        
        /// set button
        saveButton.layer.cornerRadius = 10
        saveButton.layer.masksToBounds = true
        
        let footerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: UIScreen.main.bounds.width,
                                              height: 80))
        
        footerView.addSubview(saveButton)
        
        saveButton.frame = CGRect(x: 30,
                                  y: 36,
                                  width: footerView.w - 60,
                                  height: 44)
        
        saveButton.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        saveButton.addTarget(self,
                             action: #selector(InputVC.submitHandler),
                             for: UIControlEvents.touchUpInside)
        
        /// set tableview
        self.view.addSubview(tableView)
        
        tableView.tableFooterView = footerView
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        
        //addFakeShadow()
        
        tableView.reloadData()
    }
    
    func submitHandler() {
        
    }
}

extension InputVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellList[indexPath.row]
    }
}

extension InputVC {
    class InputTableViewCell: UITableViewCell, UITextFieldDelegate {
        
        let textField:UITextField
        
        init (text:String, placeholder:String,
              keyboardType:UIKeyboardType = .default) {
            textField = UITextField()
            
            textField.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                                 font: UIConfig.generalFont(15),
                                                                 textColor: UIConfig.generalColor.whiteGray)
            textField.backgroundColor = UIConfig.generalColor.white
            textField.returnKeyType = .done
            
            super.init(style: .default,
                       reuseIdentifier: "InputTableViewCell")
            
            self.contentView.addSubview(textField)
            textField.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0,
                                                                             30,
                                                                             0,
                                                                             30))
            }
            textField.delegate = self
            
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            
            return false
        }
    }
}
