//
//  NicknameInputVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import EZSwiftExtensions

class NicknameInputVC: InputVC {
    
    let disposeBag:DisposeBag
    
    init() {
        disposeBag = DisposeBag()
        
        let nickNameCell = InputTableViewCell(text: "",
                                              placeholder: "请输入4-25字符用户昵称")
        
        let word = nickNameCell.textField.rx.text.asObservable().startWith("")
        
        let count = word.map{$0?.lengthOfBytes(using: .utf8) ?? 0}
        
        let enable = count.map{$0 > 4 && $0 <= 25}
        
        super.init(nibName: nil,
                   bundle: nil)
        
        enable.bind(to: saveButton.rx.isEnabled).addDisposableTo(disposeBag)
        
        cellList.append(nickNameCell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "更改昵称"
    }
    
    override func submitHandler() {
        let loadingView = self.showLoading()
        
        UserManager.shareInstance.update(nickName: cellList[0].textField.text ?? "") { [weak self](error) in
            ez.runThisInMainThread {
                if let httperror = error {
                    loadingView.displayLabel(text: httperror.description)
                } else {
                    loadingView.dismiss(animated: false,
                                        completion: {
                                            self?.popVC()
                    })
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
