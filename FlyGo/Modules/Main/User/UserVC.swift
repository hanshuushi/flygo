//
//  UserVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/18.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UserVC: UIViewController, PresenterType {

    let viewModel = UserVM()
    
    var bindDisposeBag: DisposeBag?
    
    let disposeBag = DisposeBag()
    
    let tableView = UITableView(frame: .zero,
                                style: .plain)
    
    let nickName = Variable("我的昵称")
    
    let isLogined = Variable(false)
    
    let avatar:Variable<URL?> = Variable(nil)
    
    let userInfoCell = UserInfoTableViewCell()
    
    let orderCell = OrderTableViewCell()
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        /// bind cell
        nickName
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: userInfoCell.label.rx.text)
            .addDisposableTo(disposeBag)
        
        avatar
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { [weak self](url) in
                self?.userInfoCell.avatarImageView.setAvatar(url: url)
        }
            .addDisposableTo(disposeBag)
        
        /// add table view
        self.view.addSubview(tableView)
        
        tableView.contentInset = UIEdgeInsetsMake(64, 0, 49, 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 30, 0, 30)
        tableView.backgroundColor = UIConfig.generalColor.white
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        tableView.registerCell(cellClass: TitleViewCell.self)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        //addFakeShadow()
        bind()
    }

}

extension UserVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return userInfoCell
        case 1:
            return orderCell
        case 2:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "切换为飞哥模式"
            
            return cell
        case 3:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "我的优惠券"
            
            return cell
        case 4:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "分享领劵"
            
            return cell
        case 5:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "收货地址管理"
            
            return cell
        case 6:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "设置"
            
            return cell
        case 7:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "联系客服"
            
            return cell
        case 8:
            let cell:TitleViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.label.text = "意见反馈"
            
            return cell
        default:
            fatalError()
        }
    }
}

extension UserVC {
    
    class UserInfoTableViewCell: UITableViewCell {
        let avatarImageView = UIImageView()
        
        let label = UILabel()
        
        let whiteView = ViewFactory.view(color: UIConfig.generalColor.white)
        
        init() {
            super.init(style: .default,
                       reuseIdentifier: "UserInfoTableViewCell")
            
            self.contentView.addSubviews(avatarImageView, label)
            
            self.selectionStyle = .none
            
            avatarImageView.image = UIImage(named: "icon_default_avatar")
            avatarImageView.backgroundColor = UIConfig.generalColor.white
            avatarImageView.snp.makeConstraints({ (maker) in
                maker.size.equalTo(CGSize(width:60, height:60))
                maker.top.equalTo(self.contentView).offset(25)
                maker.bottom.equalTo(self.contentView).offset(-25)
                maker.left.equalTo(self.contentView).offset(30)
            })
            
            label.textAlignment = .left
            label.adjustsFontSizeToFitWidth = true
            label.backgroundColor = avatarImageView.backgroundColor
            label.font = UIConfig.generalSemiboldFont(20)
            label.textColor = UIConfig.generalColor.selected
            label.snp.makeConstraints({ (maker) in
                maker.left.equalTo(avatarImageView.snp.right).offset(30)
                maker.centerY.equalTo(avatarImageView)
            })
            
            let accessImageView = UIImageView(named: "icon_more-brand")
            
            accessImageView.backgroundColor = UIConfig.generalColor.white
            
            self.contentView.addSubview(accessImageView)
            
            accessImageView.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(self.contentView).offset(-30)
                maker.left.greaterThanOrEqualTo(label.snp.right).offset(20)
            })
            
            /// mask line
            
            self.addSubview(whiteView)
            
            whiteView.snp.makeConstraints { (maker) in
                maker.left.right.equalTo(self)
                maker.top.equalTo(self.contentView.snp.bottom)
                maker.height.equalTo(3)
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            self.bringSubview(toFront: whiteView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension UserVC {
    class TitleViewCell: UITableViewCell {
        
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "TitleViewCell")
            
            self.contentView.addSubviews(label)
            
            self.selectionStyle = .none
            
            self.label.backgroundColor = UIConfig.generalColor.white
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(self.contentView).offset(20)
                maker.bottom.equalTo(self.contentView).offset(-20)
                maker.left.equalTo(self.contentView).offset(30)
            })
            
            let accessImageView = UIImageView(named: "icon_more-brand")
            
            accessImageView.backgroundColor = UIConfig.generalColor.white
            
            self.contentView.addSubview(accessImageView)
            
            accessImageView.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(self.contentView).offset(-30)
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension UserVC {
    class OrderTableViewCell: UITableViewCell {
        
        let linkSelected:Observable<Int>
        
        let label:UILabel
        
        init() {
            let linkButtons = [OrderTableViewCell.createButton(imageName: "icon_waiting_for_payment",
                                                               title: "待付款",
                                                               tag:1),
                               OrderTableViewCell.createButton(imageName: "icon_waiting_for_order_receiving",
                                                               title: "待接单",
                                                               tag:2),
                               OrderTableViewCell.createButton(imageName: "icon_waiting_for_receipt",
                                                               title: "待收货",
                                                               tag:3),
                               OrderTableViewCell.createButton(imageName: "icon_waiting_for_evaluation",
                                                               title: "待评价",
                                                               tag:4),
                               OrderTableViewCell.createButton(imageName: "icon_refund",
                                                               title: "退款",
                                                               tag:5)]
            
            linkSelected = Observable.from(linkButtons.map({ (button:UIButton) -> Observable<Int> in
                let tag = button.tag
                
                return button.rx.tap.map({_ in tag})
            })).merge()
            
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "OrderTableViewCell")
            
            self.contentView.addSubviews(label)
            
            self.selectionStyle = .none
            
            /// add text label
            self.label.text = "我的订单"
            self.label.backgroundColor = UIConfig.generalColor.white
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.snp.makeConstraints({ (maker) in
                maker.left.equalTo(self.contentView).offset(30)
                maker.top.equalTo(self.contentView).offset(20)
            })
            
            /// add line
            
            let line = ViewFactory.view(color: UIConfig.generalColor.lineColor)
            
            self.contentView.addSubview(line)
            
            line.snp.makeConstraints { (maker) in
                maker.left.right.equalTo(label)
                maker.top.equalTo(label.snp.bottom).offset(10)
                maker.height.equalTo(0.5)
            }
            
            /// set accessory
            let accessImageView = UIImageView(named: "icon_more-brand")
            
            accessImageView.backgroundColor = UIConfig.generalColor.white
            
            self.contentView.addSubview(accessImageView)
            
            accessImageView.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(self.label)
                maker.right.equalTo(self.contentView).offset(-30)
            })
            
            /// 代付款
            var fixed:UIView? = nil
            
            for (index, button) in linkButtons.enumerated() {
                self.contentView.addSubview(button)
                
                button.tag = index
                
                if index == 0 {
                    button.snp.makeConstraints({ (maker) in
                        maker.left.equalTo(self.contentView).offset(30)
                        maker.top.equalTo(self.label.snp.bottom).offset(30)
                        maker.size.equalTo(CGSize(width:35, height:45))
                        maker.bottom.equalTo(self.contentView).offset(-20)
                    })
                } else  {
                    button.snp.makeConstraints({ (maker) in
                        maker.left.equalTo(fixed!.snp.right)
                        maker.centerY.equalTo(linkButtons[0])
                        maker.size.equalTo(linkButtons[0])
                    })
                }
                
                if index >= (linkButtons.count - 1) {
                    button.snp.makeConstraints({ (maker) in
                        maker.right.equalTo(self.contentView).offset(-30)
                    })
                    
                    break
                }
                
                let currentFixex = UIView()
                
                self.contentView.addSubview(currentFixex)
                
                currentFixex.snp.makeConstraints({ (maker) in
                    maker.height.equalTo(1)
                    maker.left.equalTo(button.snp.right)
                    maker.centerY.equalTo(button)
                })
                
                if let fixedView = fixed {
                    currentFixex.snp.makeConstraints({ (maker) in
                        maker.width.equalTo(fixedView)
                    })
                }
                
                fixed = currentFixex
            }
        }
        
        /// add icon
        static func createButton(imageName:String, title:String, tag:Int) -> UIButton {
            
            let attrString = NSAttributedString(string: title,
                                                font: UIConfig.generalFont(11),
                                                textColor: UIConfig.generalColor.unselected)
            
            let button = UIButton(type: .custom)
            
            button.setImage(UIImage(named:imageName),
                            for: .normal)
            button.backgroundColor = UIConfig.generalColor.white
            button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 12.5, 0)
            button.tag = tag
            
            let label = UILabel()
            
            label.backgroundColor = button.backgroundColor
            label.attributedText = attrString
            
            button.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.bottom.equalTo(button)
                maker.centerX.equalTo(button)
            }
            
            return button
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}
