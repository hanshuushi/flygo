//
//  MainViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/15.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import SnapKit
import EZSwiftExtensions
import RxSwift
import RxCocoa

typealias RootControllerConfig = (title:String, iconName:String)

@objc
protocol EditorRootViewController: NSObjectProtocol {
    func switchEditorStatus(sender:UIBarButtonItem)
    
    var editorButtonItem:UIBarButtonItem? {get set}
}

class MainVC: UITabBarController {
    
    static func rootController() -> UINavigationController {
        return CommonNavigationVC(rootViewController: MainVC())
    }
    
    override var selectedViewController: UIViewController? {
        set {
            super.selectedViewController = newValue
            
            setupNavigationItems()
        }
        
        get {
            return super.selectedViewController
        }
    }

    let searchController = HomeSearchVC()
    
    static let config:[RootControllerConfig] = [
        (title:"首页", iconName:"home"),
        (title:"分类", iconName:"classify"),
        (title:"购物车", iconName:"shop_cart"),
        (title:"我", iconName:"mine")]
    
    #if DEBUG
    deinit {
        print("Kill MainVC")
    }
    #endif
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        for (index, one) in MainVC.config.enumerated() {
            let viewController = {
                () -> UIViewController in
                switch index {
                case 0:
                    return HomeVC(nibName: nil, bundle: nil)
                case 1:
                    return CategoryVC(nibName: nil, bundle: nil)
                case 2:
                    return CartVC(nibName: nil, bundle: nil)
                case 3:
                    return UserVC(nibName: nil, bundle: nil)
                default:
                    fatalError()
                }
            }()
            
            viewController.title = one.title
            
            viewController.tabBarItem.image = UIImage(named: "icon_\(one.iconName)")?.withRenderingMode(.alwaysOriginal)
            viewController.tabBarItem.selectedImage = UIImage(named: "icon_\(one.iconName)")
            
            addChildViewController(viewController)
        }
        
        self.tabBar.tintColor = UIConfig.generalColor.selected
        self.tabBar.shadowImage = UIImage()
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.backgroundColor = UIConfig.generalColor.barColor
        
        let line = ViewFactory.line()
        
        self.tabBar.addSubview(line)
        
        line.snp.makeConstraints { (maker) in
            maker.left.right.top.equalTo(self.tabBar)
            maker.height.equalTo(0.5)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        setupRouteObserver()
        
        setupNavigationItems()
    }
    
    var tipTimer:Timer?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let timer = tipTimer {
            timer.invalidate()
        }
        
        if !shouldShowTipView {
            return
        }
        
        tipTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                        target: self,
                                        selector: #selector(MainVC.showTipView),
                                        userInfo: nil,
                                        repeats: false)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let timer = tipTimer {
            timer.invalidate()
            
            tipTimer = nil
        }
    }
    
    private let _oneceTokenInLayoutSubviews = NSUUID().uuidString
    
    fileprivate let disposeBag = DisposeBag()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.once(token: _oneceTokenInLayoutSubviews) {
            if let navigationBar = self.navigationController?.navigationBar {
                if let rightButton = navigationBar
                    .subviews.map({ $0 as? UIButton })
                    .filter({ $0 != nil })
                    .map({ $0! })
                    .sorted(by: { $0.left >= $1.left }).first {
                    
                    if let imageView = rightButton.imageView {
                        rightButton.bageOffset = CGSize(width: (rightButton.w - imageView.w) / 2.0,
                                                        height: (rightButton.h - imageView.h) / 2.0)
                    }
                    
                    NotificationManager
                        .shareInstance
                        .notificationUnread
                        .drive(onNext: { (bage) in
                            
                            rightButton.bage = bage
                        },
                               onCompleted: nil,
                               onDisposed: nil).addDisposableTo(disposeBag)
                }
            }
        }
    }
    
    let searchTextField = SearchPresentController.generalTextField(placeHolder: "YSL  口红")
    
}


// MARK: - Tip 相关
extension MainVC {
    var shouldShowTipView:Bool {
        get {
            return (UserDefaults.standard.value(forKey: "ShouldShowTipView") as? Bool) ?? true
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "ShouldShowTipView")
            UserDefaults.standard.synchronize()
        }
    }
    
    func showTipView() {
        if !shouldShowTipView {
            return
        }
        
        let backgroundView = ViewFactory.view(color: UIColor.black.withAlphaComponent(0.6))
        
        backgroundView.tag = 666
        
        let tration = CATransition()
        
        tration.duration = 0.125
        tration.type = "fade"
        tration.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        self.navigationController?.view.layer.add(tration, forKey: "tration")
        self.navigationController?.view.addSubview(backgroundView)
        
        backgroundView.snp.fillToSuperView()
        
        let switchIcon = UIImageView(named: "icon_switch-1")
        
        let arrowIcon = UIImageView(named: "icon_arrow")
        
        let confirmIcon = ViewFactory.button(imageNamed: "button_confirm")
        
        confirmIcon.addTarget(self,
                              action: #selector(MainVC.removeTipView),
                              for: UIControlEvents.touchUpInside)
        
        let tipLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(15),
                                         textColor: UIConfig.generalColor.white,
                                         backgroudColor: .clear)
        
        tipLabel.numberOfLines = 0
        tipLabel.text = "在飞购，\n不止买买买！\n点击这里，\n切换到飞哥模式，\n开始赚赚赚！"
        tipLabel.textAlignment = .center
        
        backgroundView.addSubviews(switchIcon, arrowIcon, tipLabel, confirmIcon)
        
        switchIcon.snp.makeConstraints { (maker) in
            maker.left.equalTo(backgroundView).offset(5)
            maker.top.equalTo(backgroundView).offset(20)
        }
        
        arrowIcon.snp.makeConstraints { (maker) in
            maker.left.equalTo(backgroundView).offset(45)
            maker.top.equalTo(switchIcon.snp.bottom).offset(15)
        }
        
        tipLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(backgroundView).offset(65)
            maker.top.equalTo(arrowIcon).offset(35)
        }
        
        confirmIcon.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(backgroundView)
            maker.bottom.equalTo(backgroundView).offset(-40)
        }
    }
    
    func removeTipView() {
        guard let backgroundView = self.navigationController?.view.viewWithTag(666) else {
            return
        }
        
        shouldShowTipView = false
        
        let tration = CATransition()
        
        tration.duration = 0.125
        tration.type = "fade"
        tration.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        self.navigationController?.view.layer.add(tration, forKey: "tration")
        self.navigationController?.view.addSubview(backgroundView)
        
        backgroundView.removeFromSuperview()
    }
}

// MARK: - Navigation Item的相关
extension MainVC {
    fileprivate func setupNavigationItems () {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_switch"),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(MainVC.switchToFlygo(sender:)))
        
        var rights = [UIBarButtonItem]()
        
        rights.append(UIBarButtonItem(image: UIImage(named: "icon_news"),
                                      style: .plain,
                                      target: self,
                                      action: #selector(MainVC.showMessage(sender:))))
        
        if let editorController = self.selectedViewController as? EditorRootViewController {
            let item = UIBarButtonItem(title: "编辑",
                                       style: .plain,
                                       target: editorController,
                                       action: #selector(EditorRootViewController.switchEditorStatus(sender:)))
            
            rights.append(item)
            
            editorController.editorButtonItem = item
        }
        
        if let _ = self.selectedViewController as? CartVC {
            AnalyzeManager.shareInstance.viewCart()
        }
        
        self.navigationItem.rightBarButtonItems = rights
        
        buildSearch()
        
        
    }
    
    func switchToFlygo(sender:Any?) {
        LoginVC.doActionIfNeedLogin {
            MainVC.switchRoot()
        }
    }
    
    static func switchRoot() {
        let tration = CATransition()
        
        tration.duration = 0.35
        tration.type = "oglFlip"
        tration.subtype = kCATransitionFromRight
        tration.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        UIApplication.shared.keyWindow?.layer.add(tration, forKey: "tration")
        UIApplication.shared.keyWindow?.rootViewController = FlygoVC()
    }
    
    func showMessage(sender:Any?) {
        print("switchToFlygo In showMessage")
        LoginVC.doActionIfNeedLogin {
            let vc = MessageEntryVC()
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        }
    }
}

// MARK: - Search Refer
extension MainVC: UITextFieldDelegate, SearchPresenting {
    var searchTextFieldFrame:CGRect {
        
        return searchTextField.superview?.convert(searchTextField.frame,
                                                  to: self.view) ?? .zero
        
    }
    
    fileprivate func buildSearch() {
        searchTextField.frame = CGRect(x: 0,
                                       y: 0,
                                       width: UIScreen.main.bounds.size.width - 50,
                                       height: 30)
        
        self.navigationItem.titleView = searchTextField
        
        searchTextField.delegate = self
    }
    
    func showSearchViewController() {
        let vc = HomeSearchVC(nibName: nil, bundle: nil)
        
        SearchPresentController(presented: vc,
                                presenting: self).present()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        showSearchViewController()
        
        return false
    }
}


// MARK: - Route
extension MainVC {
    func showOrderViewController() {
        print("switchToFlygo In showOrderViewController")
        LoginVC.doActionIfNeedLogin {
            let orderViewController = OrderVC(defaultPage: 0)
            
            self.navigationController?.pushViewController(orderViewController,
                                                          animated: true)
        }
    }
    
    func showProductDetailViewController(of recId:String) {
        let vm = ProductDetailVM(productId: recId)
        
        self.navigationController?.pushViewController(ProductDetailVC(viewModel: vm),
                                                      animated: true)
    }
    
    func setupRouteObserver() {
        RouteManager
            .shareInstance
            .actionItemSubject
            .bind {
                [weak self] (item) in
                
                guard let `self` = self else {
                    return
                }
                
                switch item {
                case .home:
                    self.selectedIndex = 0
                case .search:
                    self.showSearchViewController()
                case .order:
                    self.showOrderViewController()
                case .product(let recId):
                    self.showProductDetailViewController(of: recId)
                case .flyman:
                    UIApplication.shared.keyWindow?.rootViewController = FlygoVC()
                }
        }
            .addDisposableTo(disposeBag)
    }
}
