//
//  CouponPickerVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/18.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CouponPickerVC: UIViewController {
    var validItemArray:[CouponItem] = []
    
    var invalidItemArray:[CouponItem] = []
    
    let selectedItem:PublishSubject<CouponItem?> = PublishSubject()
    
    func cancelCouponPressed(sender:Any?) {
        selectedItem.onNext(nil)
    }
    
    let tableView:UITableView
    
    let price:Float
    
    init(itemArray:[CouponItem], price:Float) {
        self.validItemArray = itemArray.filter({ $0.limit <= price })
        
        self.invalidItemArray = itemArray.filter({ $0.limit > price })
        
        self.price = price
        
        self.tableView = UITableView(frame: .zero,
                                     style: .grouped)
        self.tableView.registerCell(cellClass: CouponListTableViewCell.self)
        self.tableView.rowHeight = 115
        self.tableView.separatorStyle = .none
        self.tableView.backgroundView = ViewFactory.view(color: .white)
        
        super.init(nibName: nil,
                   bundle: nil)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "选择优惠券"
        
        self.view.addSubview(tableView)
        
        tableView.snp.fillToSuperView()
        
        tableView.setInset(top: 64, bottom: 44)
        
        let button = UIButton(type: .custom)
        
        button.setAttributedTitle(NSAttributedString.init(string: "不使用优惠券",
                                                          font: UIConfig.generalFont(15),
                                                          textColor: UIConfig.generalColor.labelGray),
                                  for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIConfig.generalColor.whiteGray.cgColor
        button.layer.masksToBounds = true
        button.backgroundColor = UIConfig.generalColor.white
        button.addTarget(self,
                         action: #selector(CouponPickerVC.cancelCouponPressed(sender:)),
                         for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(button)
        
        button.snp.makeConstraints({ (maker) in
            maker.left.right.bottom.equalTo(self.view)
            maker.height.equalTo(44)
        })
    }
    
    static func picker(in parentController:UIViewController?,
                       price:Float,
                       with item:[CouponItem]) -> Observable<CouponItem?> {
        return Observable.create({
            [weak parentController] observer -> Disposable in
            let viewController = CouponPickerVC(itemArray: item, price: price)
            
            guard let navigationController = parentController?.navigationController else {
                observer.onCompleted()
                
                return Disposables.create()
            }
            
            navigationController.pushViewController(viewController, animated: true)
            
            let p = viewController.selectedItem.bind(to: observer)
            
            return Disposables.create(p,
                                      Disposables.create {
                                        [weak viewController] () in
                                        
                                        let _ = viewController?.popVC()
            })
        })
    }
}

extension CouponPickerVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0, validItemArray.count > 0 {
            return validItemArray.count
        }
        
        return invalidItemArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (validItemArray.count > 0 ? 1 : 0) + (invalidItemArray.count > 0 ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as CouponListTableViewCell
        
        if indexPath.section == 0, validItemArray.count > 0 {
            cell.set(item: validItemArray[indexPath.row], price: price)
        } else {
            cell.set(item: invalidItemArray[indexPath.row], price: price)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if section != numberOfSections(in: tableView) - 1 {
            return nil
        }
        
        let view = UITableViewHeaderFooterView()
        
        let label = ViewFactory.generalLabel(generalSize: 12,
                                             textColor: UIConfig.generalColor.whiteGray)
        
        view.backgroundView = ViewFactory.view(color: .white)
        view.contentView.addSubview(label)
        
        label.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(view.contentView)
            maker.centerY.equalTo(view.contentView).offset(-5)
        }
        
        label.text = "以上是全部有效的优惠券"
        
        let leftLine = ViewFactory.line()
        
        view.contentView.addSubview(leftLine)
        
        leftLine.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.left.equalTo(view.contentView).offset(15)
            maker.right.equalTo(label.snp.left).offset(-15)
            maker.centerY.equalTo(label)
        }
        
        let rightLine = ViewFactory.line()
        
        view.contentView.addSubview(rightLine)
        
        rightLine.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.right.equalTo(view.contentView).offset(-15)
            maker.left.equalTo(label.snp.right).offset(15)
            maker.centerY.equalTo(label)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != numberOfSections(in: tableView) - 1 {
            return .leastNormalMagnitude
        }
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0, validItemArray.count > 0 {
            selectedItem.onNext(validItemArray[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView()
        
        let label = ViewFactory.generalLabel(generalSize: 12,
                                             textColor: UIConfig.generalColor.whiteGray)
        
        view.backgroundView = ViewFactory.view(color: .white)
        view.contentView.addSubview(label)
        
        label.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(view.contentView)
            maker.centerY.equalTo(view.contentView).offset(-5)
        }
        
        if section == 0, validItemArray.count > 0 {
            label.text = "可使用的优惠券"
        } else {
            label.text = "不可使用的优惠券"
        }
        
        let leftLine = ViewFactory.line()
        
        view.contentView.addSubview(leftLine)
        
        leftLine.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.left.equalTo(view.contentView).offset(15)
            maker.right.equalTo(label.snp.left).offset(-15)
            maker.centerY.equalTo(label)
        }
        
        let rightLine = ViewFactory.line()
        
        view.contentView.addSubview(rightLine)
        
        rightLine.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.right.equalTo(view.contentView).offset(-15)
            maker.left.equalTo(label.snp.right).offset(15)
            maker.centerY.equalTo(label)
        }
        
//        let view = ViewFactory.view(color: UIConfig.generalColor.white)
//        
        
        
        return view
    }

}
