//
//  CouponListVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/18.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum CouponDiscountType {
    case value(Double)
    case percent(Float)
    
    var attributedString:NSAttributedString {
        switch self {
        case .value(let value):
            let attributedString = NSMutableAttributedString(string: "￥",
                                                             font: UIConfig.generalFont(13),
                                                             textColor: UIConfig.generalColor.white,
                                                             lineSpace: 0,
                                                             alignment: .center)
            
            let intVal = Int(value)
            
            let fontSize:CGFloat = {
                if intVal < 10 {
                    return 50
                }
                
                if intVal < 100 {
                    return 45
                }
                
                if intVal < 1000 {
                    return 40
                }
                
                if intVal < 10000 {
                    return 35
                }
                
                if intVal < 100000 {
                    return 30
                }
                
                return 25
            }()
            
            attributedString.append(NSMutableAttributedString(string: "\(Int(value))",
                font: UIConfig.generalFont(fontSize),
                textColor: UIConfig.generalColor.white,
                lineSpace: 0,
                alignment: .center))
            
            return attributedString
        case .percent(let per):
            let intVal = Int(per * 100)
            
            let str = intVal % 10 == 0 ? "\(intVal / 10)" : "\(intVal / 10).\(intVal % 10)"
            
            let fontSize:CGFloat = intVal % 10 == 0 ? 50 : 40
            
            let attributedString = NSMutableAttributedString(string: str ,
                font: UIConfig.generalFont(fontSize),
                textColor: UIConfig.generalColor.white,
                lineSpace: 0,
                alignment: .center)
            
            attributedString.append(NSMutableAttributedString(string: " 折",
                                                              font: UIConfig.generalFont(13),
                                                              textColor: UIConfig.generalColor.white,
                                                              lineSpace: 0,
                                                              alignment: .center))
            
            return attributedString
        }
    }
}

struct CouponItem {
    
    var id:String
    
    var detailId:String
    
    var discount:CouponDiscountType
    
    var condition:String
    
    var title:String
    
    var introduce:String
    
    var isValid:Bool
    
    var expireDay:Int?
    
    var startTime:String
    
    var endTime:String
    
    var limit:Float
    
    var attributedString:NSAttributedString {
        let attributedString = NSMutableAttributedString()
        
        attributedString.append(discount.attributedString)
        attributedString.append(NSAttributedString(string: "\n\(condition)",
            font: UIConfig.generalFont(13),
            textColor: UIConfig.generalColor.white,
            lineSpace: 5,
            alignment: .center))
        
        return attributedString
    }
    
    func favorablePrice(_ price:Float) -> Float {
        switch discount {
        case .percent(let val):
            return price * (1 - val)
        case .value(let val):
            return (Float(val))
        }
    }
    
    init(model:API.Coupon, standardTime:Date) {
        id = model.couponId
        
        detailId = model.couponDetailsId
        
        switch model.couponType {
        case .cash:
            discount = .value(Double(model.discountAmt))
        case .discount:
            discount = .percent(model.discountPercent)
        }
        
//        switch model.useType {
//        case .all:
//            condition = "通用型"
//        case .exclusive:
//            condition = "专用型"
//        case .special:
//            condition = "特殊型"
//        case .vip:
//            condition = "VIP型"
//        }
        
        limit = model.useLimitAmt
        
        condition = "满\(Int(limit))可用"
        
        title = model.couponName
        
        introduce = model.couponDesc
        
        let timeOffset = model.endTime.timeIntervalSince(standardTime)
        
        isValid = timeOffset > 0
        
        if isValid {
            expireDay = Int(timeOffset / 60 / 60 / 24)
        } else {
            expireDay = nil
        }
        
        startTime = model.startTime.toString(format: "YYYY.MM.dd")
        
        endTime = model.endTime.toString(format: "YYYY.MM.dd")
    }
}

class CouponListVM: ViewModel {
    
    let isValid:Bool
    
    init(isValid:Bool) {
        self.isValid = isValid
    }
    
    func bind(to view:CouponListPresenter) -> DisposeBag? {
        
        view.expireCount = 10
        
        view.tableView.reloadData()
        
        let bag = DisposeBag()
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .none)
        
        let state:API.CouponRequestState = self.isValid ? .valid : .invalid
        
        let isValid = self.isValid
        
        let request = dataSet.refrence(getRequest: {
            API.Coupon.getCoupon(of: state)
        },
                         isEmpty: { (collection) -> Bool in
                            
                            print("collection is \(collection)")
                            
                            return false
        },
                         in: bag)
        
        request.map { (collection) -> [CouponItem] in
            if isValid {
                return collection.validList.map({ CouponItem(model: $0,
                                                             standardTime: collection.serverTime) })
            } else {
                return collection.invalidList.map({ CouponItem(model: $0,
                                                               standardTime: collection.serverTime) })
            }
        }.bind { [weak view] (items) in
            view?.itemArray = items
            view?.expireCount = items.filter({ (item) -> Bool in
                if let day = item.expireDay, day >= 0 && day <= 10 {
                    return true
                }
                
                return false
            }).count
        }.addDisposableTo(bag)
        
        let getAdvertisementRequest = DictionaryManager
            .shareInstance
            .advertisementList
            .map({ $0
                .filter({ $0.type == 3 })
                .first })
            .map { (model) -> AdvertisementTableViewCell? in
                
                guard let `model` = model else {
                    return nil
                }
                
                let item = AdvertisementItem(model: model)
                
                let cell = AdvertisementTableViewCell(style: .default,
                                                      reuseIdentifier: "AdvertisementTableViewCell")
                
                cell.set(item: item)
                
                return cell
        }
        
        getAdvertisementRequest
            .drive(onNext: {
                [weak view] (cell) in
                view?.advertisementCell = cell
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(bag)
        
        getAdvertisementRequest
            .withLatestFrom(dataSet.isEmpty.asDriver()) { (item, isEmpty) -> Bool in
                return false
            }
            .drive(dataSet.isEmpty)
            .addDisposableTo(bag)
        
        return bag
    }
}
