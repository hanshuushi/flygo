//
//  CouponVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/18.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SnapKit

class CouponListTableViewCell: UITableViewCell {
    
    private static func makeBackgroudImage(startColor:UIColor, endColor:UIColor) -> UIImage? {
        let width:CGFloat = UIScreen.main.bounds.width - 30
        
        let height:CGFloat = 105
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width + 4, height: height + 4),
                                               false,
                                               UIScreen.main.scale)
        
        guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            
            return nil
        }
        
        /// draw backgroud
        let backgroudpath = CGMutablePath()
        
        backgroudpath.addRoundedRect(in: CGRect(x: 0, y: 0, width: width, height: height),
                                     cornerWidth: 5,
                                     cornerHeight: 5)
        
        ctx.saveGState()
        
        ctx.addPath(backgroudpath)
        ctx.setFillColor(UIColor.white.cgColor)
        ctx.setShadow(offset: CGSize(width: 2, height: 2),
                      blur: 4,
                      color: UIColor.black.withAlphaComponent(0.04).cgColor)
        ctx.fillPath()
        
        ctx.restoreGState()
        
        ctx.saveGState()
        
        ctx.addPath(backgroudpath)
        ctx.setStrokeColor(UIColor(hexString: "#eeeeee")!.cgColor)
        ctx.strokePath()
        
        ctx.restoreGState()
        
        /// draww left red square
        ctx.saveGState()
        
        let squareWidth:CGFloat = 120
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let array = [startColor.cgColor, endColor.cgColor] as CFArray
        
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: array,
                                  locations: [0.0, 1.0])
        
        let startPoint = CGPoint(x: width / 2.0, y: 0)
        
        let endPoint = CGPoint(x: width / 2.0 + height * 0.176, y: height)
        
        let leftPath = CGMutablePath()
        
        leftPath.move(to: CGPoint(x: 0, y: 5))
        leftPath.addArc(tangent1End: .zero,
                        tangent2End: CGPoint(x: squareWidth, y: 0),
                        radius: 5)
        leftPath.addLine(to: CGPoint(x: squareWidth, y: 0))
        leftPath.addLine(to: CGPoint(x: squareWidth, y: height))
        leftPath.addArc(tangent1End: CGPoint(x: 0, y: height),
                        tangent2End: CGPoint(x: 0, y: 5),
                        radius: 5)
        
        
        ctx.addPath(leftPath)
        ctx.clip()
        ctx.drawLinearGradient(gradient!,
                               start: startPoint,
                               end: endPoint,
                               options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
        ctx.restoreGState()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
    static var validCouponBackgroudImage:UIImage? = makeBackgroudImage(startColor: UIColor(r: 247, g: 107, b: 98),
    endColor: UIColor(r: 253, g: 60, b: 83))
    
    static var unvalidCouponBackgroudImage:UIImage? = makeBackgroudImage(startColor: UIColor(r: 195, g: 206, b: 211),
    endColor: UIColor(r: 136, g: 150, b: 173))
    
    static func makeTagImage(startColor:UIColor, endColor:UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 40, height: 40),
                                               false,
                                               UIScreen.main.scale)
        
        guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            
            return nil
        }
        
        let path = CGMutablePath()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let array = [startColor.cgColor, endColor.cgColor] as CFArray
        
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: array,
                                  locations: [0.0, 1.0])
        
        let startPoint = CGPoint(x: 20, y: 0)
        
        let endPoint = CGPoint(x: 20 + 20 * 0.176, y: 20)
        
        path.move(to: .zero)
        path.addLine(to: CGPoint(x: 35, y: 0))
        path.addArc(tangent1End: CGPoint(x: 40, y: 0), tangent2End: CGPoint(x: 40, y: 40), radius: 5)
        path.addLine(to: CGPoint(x: 40, y: 40))
        path.addLine(to: .zero)
        
        ctx.addPath(path)
        ctx.clip()
        ctx.drawLinearGradient(gradient!,
                               start: startPoint,
                               end: endPoint,
                               options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
    static var validTagImage:UIImage? = makeTagImage(startColor: UIColor(r: 247, g: 107, b: 98), endColor: UIColor(r: 253, g: 60, b: 83))
    
    static var unvalidTagImage:UIImage? = makeTagImage(startColor: UIColor(r: 195, g: 206, b: 211), endColor: UIColor(r: 136, g: 150, b: 173))
    
    let backgroudImageView:UIImageView
    
    let discountLabel:UILabel
    
    let titleLabel:UILabel
    
    let expireLabel:UILabel
    
    let introduceLabel:UILabel
    
    let tagImageView:UIImageView
    
    var rightMarginConstraint:Constraint!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        backgroudImageView = UIImageView(image: CouponListTableViewCell.validCouponBackgroudImage,
                                         highlightedImage: CouponListTableViewCell.unvalidCouponBackgroudImage)
        
        tagImageView = UIImageView(image: CouponListTableViewCell.validTagImage,
                                   highlightedImage: CouponListTableViewCell.unvalidTagImage)
        
        discountLabel = UILabel()
        
        discountLabel.numberOfLines = 2
        discountLabel.adjustsFontSizeToFitWidth = true
        
        titleLabel = ViewFactory.generalLabel(generalSize: 16,
                                              textColor: UIConfig.generalColor.selected)
        titleLabel.numberOfLines = 2
        titleLabel.adjustsFontSizeToFitWidth = true
        
        expireLabel = ViewFactory.generalLabel(generalSize: 11,
                                               textColor: UIConfig.generalColor.white,
                                               backgroudColor: .clear)
        
//        expireLabel.layer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        
        introduceLabel = ViewFactory.generalLabel(generalSize: 12,
                                                  textColor: UIConfig.generalColor.unselected)
        
        introduceLabel.numberOfLines = 2
        introduceLabel.adjustsFontSizeToFitWidth = true
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        self.contentView.addSubviews(backgroudImageView, discountLabel, titleLabel, introduceLabel, tagImageView, expireLabel)
        
        backgroudImageView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.contentView).offset(15)
            maker.top.equalTo(self.contentView)
        }
        
        discountLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.contentView).offset(15)
            maker.top.equalTo(self.contentView)
            maker.width.equalTo(120)
            maker.height.equalTo(105)
        }
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.contentView).offset(150)
            maker.top.equalTo(self.contentView).offset(10)
            rightMarginConstraint = maker.right.equalTo(self.contentView).offset(-49).constraint
        }
        
        introduceLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(titleLabel)
            maker.bottom.equalTo(backgroudImageView).offset(-15)
            maker.right.equalTo(self.contentView).offset(-30)
        }
        
        tagImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(backgroudImageView)
            maker.right.equalTo(self.contentView).offset(-15)
        }
        
        expireLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(tagImageView.snp.top).offset(13)
            maker.centerX.equalTo(tagImageView.snp.right).offset(-13)
        }
        
        expireLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 4))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(item:CouponItem) {
        discountLabel.attributedText = item.attributedString
        
        titleLabel.text = item.title
        
        introduceLabel.text = "说明：\(item.introduce)\n时间：\(item.startTime)-\(item.endTime)"
        
        if !item.isValid {
            tagImageView.isHighlighted = true
            
            backgroudImageView.isHighlighted = true
            
            expireLabel.isHidden = false
            expireLabel.text = "失效"
            
            return
        }
        
        tagImageView.isHighlighted = false
        
        backgroudImageView.isHighlighted = false
        
        if let expireDay = item.expireDay, expireDay <= 10, expireDay >= 0  {
            tagImageView.isHidden = false
            
            expireLabel.isHidden = false
            
            if expireDay > 0 {
                expireLabel.text = "\(expireDay)天"
            } else {
                expireLabel.text = "即将"
            }
            
            rightMarginConstraint.update(offset: -49)
        } else {
            tagImageView.isHidden = true
            
            expireLabel.isHidden = true
            
            rightMarginConstraint.update(offset: -30)
        }
    }
    
    func set(item:CouponItem, price:Float) {
        discountLabel.attributedText = item.attributedString
        
        titleLabel.text = item.title
        
        introduceLabel.text = "说明：\(item.introduce)\n时间：\(item.startTime)-\(item.endTime)"
        
        if !item.isValid {
            tagImageView.isHighlighted = true
            
            backgroudImageView.isHighlighted = true
            
            expireLabel.isHidden = false
            expireLabel.text = "失效"
            
            return
        }
        
        tagImageView.isHighlighted = price < item.limit
        
        backgroudImageView.isHighlighted = price < item.limit
        
        if let expireDay = item.expireDay, expireDay <= 10, expireDay >= 0  {
            tagImageView.isHidden = false
            
            expireLabel.isHidden = false
            
            if expireDay > 0 {
                expireLabel.text = "\(expireDay)天"
            } else {
                expireLabel.text = "即将"
            }
            
            rightMarginConstraint.update(offset: -49)
        } else {
            tagImageView.isHidden = true
            
            expireLabel.isHidden = true
            
            rightMarginConstraint.update(offset: -30)
        }
    }
}

class CouponListPresenter: NSObject, PresenterType, View {
    
    let isValid:Bool
    
    let viewModel:CouponListVM
    
    init(isValid:Bool) {
        self.isValid = isValid
        
        viewModel = CouponListVM(isValid: isValid)
        
        tableView = UITableView(frame: .zero,
                                style: .grouped)
        
        tableView.registerCell(cellClass: CouponListTableViewCell.self)
        tableView.rowHeight = 115
        tableView.separatorStyle = .none
        tableView.backgroundView = ViewFactory.view(color: .white)
//        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
//        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        
        itemArray = []
        
        super.init()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    let tableView:UITableView
    
    var containView:CouponListPresenter {
        return self
    }
    
    var view:UIView! {
        return tableView
    }
    
    var bindDisposeBag: DisposeBag?
    
    var itemArray:[CouponItem]
    
    var expireCount:Int = 0
    
    var advertisementCell:AdvertisementTableViewCell? = nil
    
    weak var navigationController:UINavigationController?
}

extension CouponListPresenter: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = advertisementCell, section == 0 {
            return 1
        }
        
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = advertisementCell?.url, indexPath.section == 0 {
            let urlVC = OpenURLWebVC(title: "",
                                     url: url)
            
            navigationController?.pushViewController(urlVC,
                                                     animated: true)
            
            if let id = advertisementCell?.id {
                API.Advertisement.clickBy(advertisementId: id)
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = advertisementCell {
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = advertisementCell, indexPath.section == 0 {
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(at: indexPath) as CouponListTableViewCell
        
        cell.set(item: itemArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let _ = advertisementCell, section == 0 {
            return nil
        }
        
        let view = UITableViewHeaderFooterView()
        
        if itemArray.count < 1 {
            let imageView = UIImageView(named: "icon_without_coupon")
            
            view.addSubview(imageView)
            
            imageView.snp.makeConstraints({ (maker) in
                maker.top.equalTo(view).offset(100)
                maker.centerX.equalTo(view)
            })
            
            let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.whiteGray)
            
            view.addSubview(label)
            
            label.text = self.isValid ? "您还没有可使用的优惠券" : "您并没有过期的优惠券"
            
            label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(imageView.snp.bottom).offset(30)
                maker.centerX.equalTo(imageView)
            })
            
            return view
        }
        
        let label = ViewFactory.generalLabel(generalSize: 12,
                                             textColor: UIConfig.generalColor.whiteGray)
        
        view.backgroundView = ViewFactory.view(color: .white)
        view.contentView.addSubview(label)
        
        label.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(view.contentView)
            maker.centerY.equalTo(view.contentView).offset(-5)
        }
        
        label.text = "以上是所有\(self.isValid ? "可使用" : "已过期")的优惠券"
        
        let leftLine = ViewFactory.line()
        
        view.contentView.addSubview(leftLine)
        
        leftLine.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.left.equalTo(view.contentView).offset(15)
            maker.right.equalTo(label.snp.left).offset(-15)
            maker.centerY.equalTo(label)
        }
        
        let rightLine = ViewFactory.line()
        
        view.contentView.addSubview(rightLine)
        
        rightLine.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.right.equalTo(view.contentView).offset(-15)
            maker.left.equalTo(label.snp.right).offset(15)
            maker.centerY.equalTo(label)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if let _ = advertisementCell, section == 0 {
            return CGFloat.leastNormalMagnitude
        }
        
        return itemArray.count < 1 ? 400 : 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = advertisementCell, section == 0 {
            return CGFloat.leastNormalMagnitude
        }
        
        return expireCount > 0 && self.isValid ? 55 : 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let cell = advertisementCell, indexPath.section == 0 {
            return tableView.w * cell.radio
        }
        
        return 115
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let _ = advertisementCell, section == 0 {
            return nil
        }
        
        if expireCount > 0 && self.isValid  {
            let label = ViewFactory.generalLabel(generalSize: 13,
                                                 textColor: UIConfig.generalColor.unselected)
            
            label.text = "您有\(expireCount)张优惠券将要过期"
            
            let imageView = UIImageView(named: "icon_remind")
            
            let view = UITableViewHeaderFooterView()
            
            view.contentView.addSubviews(imageView, label)
            
            imageView.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(view.contentView)
                maker.left.equalTo(view.contentView).offset(15)
            })
            
            label.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(view.contentView)
                maker.left.equalTo(imageView.snp.right).offset(10)
            })
            
            return view
        }
        
        return nil
    }
}

class CouponListVC: UIViewController {
    
    var bindDisposeBag: DisposeBag?
    
    var navTabView:NavTabView!
    
    var style = CommonTabViewStyle()
    
    let disposeBag = DisposeBag()
    
    let validPresenter = CouponListPresenter(isValid: true)
    
    let unvalidPresenter = CouponListPresenter(isValid: false)
    
    override func viewDidLoad() {
        
        self.title = "我的优惠券"
        
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        navTabView = NavTabView()
        
        self.view.addSubview(navTabView)
        
        navTabView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0))
        }
        navTabView.navTabStyle = style
        navTabView.dataSource = self
    }
    
    var oriFrame:CGRect?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let oriFrame = self.oriFrame, oriFrame == self.view.bounds {
            return
        }
        
        oriFrame = self.view.bounds
        
        let leftWith = ("可使用" as NSString).size(attributes: [
            NSFontAttributeName: style.font
            ]).width
        
        let rightWith = ("已过期" as NSString).size(attributes: [
            NSFontAttributeName: style.font
            ]).width
        
        
        let padding = (self.view.bounds.width - (leftWith + rightWith)) / 4.0
        
        style.titlePadding = padding
        style.titleInset = padding * 2.0
        
        navTabView.navTabStyle = style
        navTabView.reloadData()
    }
}

extension CouponListVC: NavTabViewDataSource {
    func navTabView(_ navTabView: NavTabView, viewAtIndex index: Int) -> UIView {
        if index == 0 {
            validPresenter.tableView.setInset(top: 106.5, bottom: 0)
            validPresenter.navigationController = self.navigationController
            validPresenter.bind()
            
            return validPresenter.view
        }
        
        if index == 1 {
            unvalidPresenter.tableView.setInset(top: 106.5, bottom: 0)
            unvalidPresenter.navigationController = self.navigationController
            unvalidPresenter.bind()
            
            return unvalidPresenter.view
        }
        
        return UIView()
    }
    
    func titleArrayForNavTabView(_ navTabView:NavTabView) -> [String] {
        return ["可使用", "已过期"]
    }
}
