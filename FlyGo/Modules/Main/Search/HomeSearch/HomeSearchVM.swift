//
//  HomeSearchVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/7.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

extension ProductBrandItem: GeneralSectionRow {}

protocol HomeSearchVMSelectionItem: GeneralSectionItem{
    func viewController(from row:Int) -> UIViewController
}

struct HomeSearchVMBrandSection : HomeSearchVMSelectionItem {
    
    init (items:[ProductBrandItem]) {
        self.items = items
    }
    
    let title:String = "搜索到的品牌"
    
    var items:[GeneralSectionRow]
    
    func viewController(from row:Int) -> UIViewController {
        let item = items[row]
        
        return ProductListVC(brandId: item.id, brandName: item.name)
    }
}

extension ProductTypeItem: GeneralSectionRow {}

struct HomeSearchVMTypeSection : HomeSearchVMSelectionItem {
    
    init (items:[ProductTypeItem]) {
        self.items = items
    }
    
    let title:String = "搜索到的分类"
    
    var items:[GeneralSectionRow]
    
    func viewController(from row:Int) -> UIViewController {
        let item = items[row]
        
        return ProductListVC(type: item as! ProductTypeItem)
    }
}

class HomeSearchVM: ViewModel {
    
    let textInput:Driver<String>
    
    let searchedBrands:Driver<[ProductBrandItem]>
    
    let searchedTypes:Driver<[ProductTypeItem]>
    
    init(textInput:Observable<String>) {
        self.textInput = textInput.asDriver(onErrorJustReturn: "")
        
        let obs = self.textInput.throttle(0.3).distinctUntilChanged()
        
        searchedTypes = obs.flatMapLatest {DictionaryManager
            .shareInstance
            .searchType(by: $0)
            .asDriver(onErrorJustReturn: [])
            .map{$0.map{ ProductTypeItem(model:$0)}}
        }
        
        searchedBrands = obs.flatMapLatest {DictionaryManager
            .shareInstance
            .searchBrand(by: $0)
            .asDriver(onErrorJustReturn: [])
            .map{$0.map{ ProductBrandItem(model:$0)}}}
    }
    
    func bind(to view: HomeSearchVC) -> DisposeBag? {
        let bindBag = DisposeBag()
        
        DictionaryManager
            .shareInstance
            .searchList
            .map({ $0.map({ $0.keyword }) })
            .drive(view.hotList)
            .addDisposableTo(bindBag)
        
        let resultList = Driver.combineLatest(searchedBrands, searchedTypes) {
            (d1, d2) -> [HomeSearchVMSelectionItem] in
            
            var items = [HomeSearchVMSelectionItem]()
            
            items.append(HomeSearchVMTypeSection(items: d2))
            items.append(HomeSearchVMBrandSection(items: d1))
            
            return items
        }
        
        resultList.map({ $0.map({ $0 as GeneralSectionItem  }) }).drive(view.resultList).addDisposableTo(bindBag)
        
        view
            .selectedObserable
            .withLatestFrom(resultList) { (indexPath, list) -> UIViewController? in
            let row = indexPath.row

            let section = indexPath.section
            
            return list[section].viewController(from: row)
        }.bind {
            (viewController) in
            
            view.dismiss(animated: true,
                         completion: {
                            if let vc = viewController,
                                let nc = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                                nc.pushViewController(vc,
                                                      animated: true)
                            }
            })
            
        }.addDisposableTo(bindBag)
        
        view.keywordSearch.bind { (keyword) in
            view.dismiss(animated: true,
                         completion: {
                            if let nc = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                                
                                let vc = ProductListVC(keyword:keyword)
                                
                                nc.pushViewController(vc,
                                                      animated: true)
                            }
            })
        }.addDisposableTo(bindBag)
        
        return bindBag
    }
}
