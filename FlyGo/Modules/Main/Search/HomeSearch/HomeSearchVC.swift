//
//  HomeSearchController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/7.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HomeSearchVC: UIViewController, SearchPresented {
    func search(keyword: String) {
        keywordSearchEvent.onNext(keyword)
    }
    
    fileprivate let selectedEvent = PublishSubject<IndexPath>()
    
    fileprivate let keywordSearchEvent = PublishSubject<String>()
    
    let selectedObserable:Observable<IndexPath>
    
    let keywordSearch:Observable<String>
    
    var presenter:Presenter<HomeSearchVM>? = nil
    
    weak var searchTextField:UITextField? {
        didSet {
            guard let textField = searchTextField else { return }
            
            let tableView = self.tableView
            
            let collectionView = self.collectionView
            
            let hotList = self.hotList
            
            let selected = collectionView!.rx.itemSelected.map({ $0.row })
                .map({ hotList.value[$0] })
                .shareReplay(1)
                .distinctUntilChanged()
            
            selected.bind(to: textField.rx.text)
                .addDisposableTo(disposeBag)
            
            
            let text = Observable
                .of(textField.rx.text.map({ $0 ?? "" }).distinctUntilChanged(), selected)
                .merge()
                .distinctUntilChanged()
            
            presenter = Presenter(viewModel: HomeSearchVM(textInput:text),
                                  view: self)
            
            presenter!.bind()
            weak var weakSelf = self
            
            text.map({ $0.length > 0 })
                .distinctUntilChanged()
                .map({ $0 ? tableView : collectionView })
                .bind { (view) in
                    if let storngSelf = weakSelf {
                        storngSelf.contentView = view
                    }
            }.addDisposableTo(presenter!.bindDisposeBag!)
        }
    }
    
    var tableView:UITableView!
    
    weak var flowLayout:UICollectionViewFlowLayout!
    
    var collectionView:UICollectionView!
    
    var hotList:Variable<[String]> = Variable([])
    
    var resultList:Variable<[GeneralSectionItem]> = Variable([])
    
    var items:[GeneralSectionItem] = []
    
    let disposeBag = DisposeBag()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        selectedObserable = selectedEvent.asObserver()
        
        keywordSearch = keywordSearchEvent.asObserver()
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HomeSearchVC {
    
    var contentView:UIView? {
        get {
            return self.view.subviews.count > 0 ? self.view.subviews[0] : nil
        }
        set (newValue) {
            self.view.removeSubviews()
            
            if let cv = newValue {
                self.view.addSubview(cv)
                
                cv.frame = self.view.bounds
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        let flowLayout = UICollectionViewFlowLayout()
        
        flowLayout.sectionInset = UIEdgeInsets(top: 0,
                                               left: 15,
                                               bottom: 0,
                                               right: 15)
        
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 10
        
        self.flowLayout = flowLayout
        
        let collectionView = UICollectionView(frame: self.view.bounds,
                                              collectionViewLayout: flowLayout)
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.backgroundColor = UIConfig.generalColor.white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerCell(cellClass: CollectionCell.self)
        collectionView.registerHeaderView(viewClass: TitleView.self)
        
        self.collectionView = collectionView
    
        hotList.asDriver().drive(onNext: { (_) in
            collectionView.reloadData()
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        let tableView = UITableView(frame: .zero,
                                  style: .plain)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        tableView.sectionHeaderHeight = 43
        tableView.registerCell(cellClass: TableViewCell.self)
        tableView.registerView(viewClass: TitleHeaderView.self)
        tableView.registerCell(cellClass: EmptyCell.self)
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.tableFooterView = UIView()
        
        resultList.asDriver().drive(onNext: { [weak self](value) in
            self?.items = value
            tableView.reloadData()
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        self.tableView = tableView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchTextField?.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchTextField?.resignFirstResponder()
    }
}

extension HomeSearchVC: UITableViewDelegate, UITableViewDataSource {
    class TitleHeaderView: UITableViewHeaderFooterView {
        
        let label:UILabel
        
        override init(reuseIdentifier: String?) {
            label = UILabel()
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            label.textColor = UIConfig.generalColor.unselected
            label.font = UIConfig.generalFont(13)
            label.backgroundColor = UIConfig.generalColor.white
            label.sizeToFit()
            
            self.addSubview(label)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            label.left = 15
            label.centerYInSuperView()
        }
        
        func setTitle(_ title:String) {
            label.text = title
            label.sizeToFit()
            label.left = 15
            label.centerYInSuperView()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class TableViewCell : UITableViewCell {
        var title:String = "" {
            didSet {
                label.text = title
                label.sizeToFit()
                label.centerYInSuperView()
                label.left = 15
            }
        }
        
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            label = UILabel()
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            label.font = UIConfig.generalFont(15)
            label.textColor = UIConfig.generalColor.selected
            label.backgroundColor = UIConfig.generalColor.white
            
            self.selectionStyle = .none
            self.contentView.addSubview(label)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            label.centerYInSuperView()
            label.left = 15
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchTextField?.resignFirstResponder()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(1, items[section].items.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row >= items[indexPath.section].items.count {
            let cell:EmptyCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.set(name: items[indexPath.section].title)
            
            return cell
        }
        
        let cell:TableViewCell = tableView.dequeueReusableCell(at: indexPath)
        
        cell.title = items[indexPath.section].items[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= items[indexPath.section].items.count {
            return 75
        }
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view:TitleHeaderView = tableView.dequeueReusableView()
        
        view.setTitle(items[section].title)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedEvent.onNext(indexPath)
    }
}

extension HomeSearchVC {
    class EmptyCell: UITableViewCell {
        
        let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.unselected)
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.center.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func set(name:String) {
            label.text = "未\(name)"
        }
    }
}

extension HomeSearchVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    class TitleView: UICollectionReusableView {
        
        let label:UILabel
        
        override init (frame: CGRect) {
            label = UILabel()
            
            super.init(frame: frame)
            
            label.backgroundColor = UIConfig.generalColor.unselected
            label.font = UIConfig.generalFont(13)
            label.backgroundColor = UIConfig.generalColor.white
            label.text = "热门搜索"
            label.sizeToFit()
            
            self.addSubview(label)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            label.left = 15
            label.centerYInSuperView()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class CollectionCell : UICollectionViewCell {
        var title:String = "" {
            didSet {
                label.text = title
                label.sizeToFit()
                label.centerInSuperView()
            }
        }
        
        let label:UILabel
        
        override init(frame: CGRect) {
            label = UILabel()
            
            super.init(frame: frame)
            
            label.font = CollectionCell.labelFont
            label.textColor = UIConfig.generalColor.selected
            label.backgroundColor = UIColor.clear
            
            self.contentView.addSubview(label)
            self.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
            self.contentView.layer.cornerRadius = 5
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            label.centerInSuperView()
        }
        
        static func size(with string:String) -> CGSize {
            let nsstring = string as NSString
            
            let size = nsstring.size(attributes: [
                NSFontAttributeName: labelFont
                ])
            
            return CGSize(width: 15 + size.width,
                          height: 12 + size.height)
        }
        
        static let labelFont = UIConfig.generalFont(12)
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hotList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CollectionCell = collectionView.dequeueReusableCell(at: indexPath)
        
        cell.title = hotList.value[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell:TitleView = collectionView.dequeueHeaderView(indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CollectionCell.size(with: hotList.value[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width,
                      height: 43)
    }
    
}
