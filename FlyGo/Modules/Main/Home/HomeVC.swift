//
//  HomeVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/18.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit


//fileprivate class
class HomeVC: UIViewController, NavTabViewDataSource {
    
    var mainViewModel:HomeMainVM?
    
    var categoryViewModels:[HomeCategoryVM]
    
    let navTabView:NavTabView
    
    let disposeBag = DisposeBag()
    
    var bindDisposeBag:DisposeBag? = nil
    
    var disposable:Disposable?
    
    deinit {
        disposable?.dispose()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        mainViewModel = nil
        
        categoryViewModels = []
        
        navTabView = NavTabView()
        navTabView.navTabStyle = CommonTabViewStyle()
        
        super.init(nibName: nibNameOrNil,
                   bundle: nibBundleOrNil)
        
        navTabView.dataSource = self
        
        DictionaryManager
            .shareInstance
            .menuList
            .distinctUntilChanged { (left, right) -> Bool in
                left.count == right.count
            }
            .drive(onNext: {
                [weak self] (menuList) in
                
                guard let `self` = self else {
                    return
                }
                
                self.disposable?.dispose()
                
                if menuList.count > 0 {
                    let mainMenuId = menuList[0].homeMenuId
                    
                    let mainTitle = menuList[0].homeMenuName
                    
                    self.mainViewModel = HomeMainVM(title:mainTitle,
                                                    menuId: mainMenuId!)
                    
                    if menuList.count > 1 {
                        self.categoryViewModels = menuList
                            .from(start: 1)
                            .map({ HomeCategoryVM(title:$0.homeMenuName,
                                                  menuId:$0.homeMenuId)
                            })
                    } else {
                        self.categoryViewModels = []
                    }
                } else {
                    self.mainViewModel = nil
                    
                    self.categoryViewModels = []
                }
                
                let typeVC = Observable
                    .from(self.categoryViewModels.map({ $0.typePushed }))
                    .merge()
                    .map { (type) -> UIViewController in
                        return ProductListVC(type: type)
                }
                
                // product push
                let mainpushed = self.mainViewModel != nil ? [self.mainViewModel!.productPushed] : []
                
                let proVC = Observable
                    .from(mainpushed + self.categoryViewModels.map{ $0.productPushed })
                    .merge()
                    .map { (item) -> UIViewController in
                        let vm = ProductDetailVM(productId: item.productId)
                        
                        let vc = ProductDetailVC(viewModel: vm)
                        
                        return vc
                }
                
                // url push
                let urlpushed = self.mainViewModel != nil ? [self.mainViewModel!.urlPushed] : []
                
                let urlVC = Observable
                    .from(urlpushed + self.categoryViewModels.map{ $0.urlPushed })
                    .merge()
                    .filter({ $0.url != nil })
                    .map { (item) -> UIViewController in
                        return OpenURLWebVC(title: item.title,
                                            url: item.url!)
                    }.asObservable()
                
                // brand push
                let brandpushed = self.mainViewModel != nil ? [self.mainViewModel!.brandPushed] : []
                
                let brandVC = Observable
                    .from(brandpushed + self.categoryViewModels.map{ $0.brandPushed })
                    .merge()
                    .map { (brandRef) -> UIViewController in
                        return ProductListVC(brandId: brandRef.brandId, brandName: brandRef.brandName)
                }
                
                // subject push
                let subjectpushed = self.mainViewModel != nil ? [self.mainViewModel!.subjectPushed] : []
                
                let subjectVC = Observable
                    .from(subjectpushed + self.categoryViewModels.map{ $0.subjectPushed })
                    .merge()
                    .map {
                        (collection) -> UIViewController in
                        return SubjectVC(subjectId: collection.subjectId,
                                         subjectTitle: collection.subjectName)
                }
                
                self.disposable = Observable<Observable<UIViewController>>
                    .of(typeVC, urlVC, brandVC, proVC, subjectVC)
                    .merge()
                    .throttle(0.3, scheduler: MainScheduler.instance)
                    .observeOn(MainScheduler.instance)
                    .bind { [weak self](viewController) in
                        self?.navigationController?.pushViewController(viewController,
                                                                       animated: true)
                    }
                
                self.navTabView.reloadData()
                
                },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
            }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func titleArrayForNavTabView(_ navTabView:NavTabView) -> [String] {
        
        let array = (mainViewModel != nil ? [mainViewModel!.title] : [String]()) + categoryViewModels.map({ $0.title })
        
        return array.count < 1 ? ["载入中"] : array
    }
    
    func navTabView(_ navTabView:NavTabView, viewAtIndex index:Int) -> UIView {
        if mainViewModel == nil && categoryViewModels.count < 1 {
            
            let loadingBackgroudView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            let loading = ActivityIndicatorView()
            
            loadingBackgroudView.addSubview(loading)
            
            loading.snp.makeConstraints({ (maker) in
                maker.centerX.equalTo(loadingBackgroudView)
                maker.top.equalTo(loadingBackgroudView).offset(126.5)
            })
            loading.play()
            
            return loadingBackgroudView
        }
        
        if index == 0 {
            let apiView = HomeVCMainView(viewModel: mainViewModel!)
            
            DispatchQueue.main.async {
                apiView.bind()
            }
            
            return apiView
        }
        
        let viewModel = categoryViewModels[index - 1]
        
        let apiView = HomeVCCategoryView(viewModel: viewModel)
        
        DispatchQueue.main.async {
            apiView.bind()
        }
        
        return apiView
    }
}

// MARK: - View Building
extension HomeVC {
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        // Buid Nav Tab
        
        self.view.addSubview(navTabView)
        
        navTabView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0))
        }
        
        navTabView.reloadData()
    }
    
    
}


