//
//  HomeVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
    
    
/// cover item
struct HomeMainVMCoverItem {
    var imageUrl:URL?
    
    var id:String = ""
    
    var url:URL?
    
    init(model:API.HomeMenuItem.Banner.Item) {
        imageUrl = model.pic
        
        id = model.carouselId
        
        url = model.linkAddress
    }
}


/// Theme Item
struct HomeMainVMThemeItem {
    var imageUrl:URL?
    
    var title:String = ""
    
    var subTitle:String = ""
    
    var url:URL?
    
    var id:String = ""
    
    var number:Int = 1000
    
    init(model:API.HomeMenuItem.Active.Item) {
        id = model.activityId
        
        imageUrl = model.pic
        
        title = model.title
        
        subTitle = model.useId
        
        url = model.linkAddress
        
        number = model.pageViews + model.basePageViews
    }
}
    
struct HomeMainProductGroupItem {
    var coverUrl:URL?
    
    var title:String = ""
    
    var products:[ProductItem] = []
    
    var id:String
    
    init(model:API.HomeMenuItem.Modular) {
        id = model.modularId
        
        coverUrl = model.pic
        
        title = model.modularName
        
        products = model.productList
            .map{ProductItem(model:$0)}
    }
}
    
struct HomeMainMenuItem {
    var title:String
        
    var id:String
        
    init( model:API.HomeMenu) {
        title = model.homeMenuName
            
        id = model.homeMenuId
    }
}
    
/// Command View Model
protocol HomeVM: ViewModel {
    var title:String { get }
    
    init(title:String, menuId:String)
}
