//
//  HomeVCMainView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/23.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

/// 全球免税的View Build
final class HomeVCMainView: UITableView, PresenterType {
    
    let viewModel:HomeMainVM
    
    var bindDisposeBag: DisposeBag?
    
    let urlSelected:PublishSubject<(url:URL?, title:String)> = PublishSubject()
    
    let subjectSelected:PublishSubject<(subjectId:String, subjectName:String)> = PublishSubject()
    
    let disponseBag = DisposeBag()
    
    let coverCell:HomeVCMainViewCoverCell
    
    let topCell:HomeVCTopCell = HomeVCTopCell()
    
    let brandCell:HomeVCBrandCell = HomeVCBrandCell()
    
    let preferentialCell:HomeVCTopCell = HomeVCTopCell()
    
    let advertisementTableViewCell:AdvertisementTableViewCell = AdvertisementTableViewCell()
    
    typealias ThemeItem = HomeMainVMThemeItem
    
    typealias BrandItem = ProductBrandItem
    
    typealias CoverItem = HomeMainVMCoverItem
    
    let themeItemList:Variable<[ThemeItem]> = Variable([])
    
    let advertisementItem:Variable<AdvertisementItem?> = Variable(nil)
    
    let titles: Variable<[String]> = Variable(["全球热卖TOP10", "品牌推荐", "免税特惠", "平台精选"])

    init(viewModel:HomeMainVM) {
        self.viewModel = viewModel
        
        coverCell = HomeVCMainViewCoverCell(urlSelected)
        coverCell.autoScroll = true
        
        super.init(frame: UIScreen.main.bounds, style: .grouped)
        
        self.backgroundColor = UIConfig.generalColor.backgroudGray
        self.registerCell(cellClass: HomeVCEmptyTableViewCell.self)
        self.registerCell(cellClass: HomeVCActivityCell.self)
        self.registerView(viewClass: HomeVCMainViewTitleSessionView.self)
        self.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        self.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        self.setInset(top: 106.5, bottom: 49)
        self.separatorStyle = .none
        self.scrollIndicatorInsets = self.contentInset
        
        weak var weakSelf = self
        
//        themeItemList
//            .asObservable()
//            .observeOn(MainScheduler.instance)
//            .bind { (_) in
//                if let strongSelf = weakSelf {
////                    strongSelf.reloadData()
//                }
//            }.addDisposableTo(disponseBag)
//        
//        titles
//            .asObservable()
//            .observeOn(MainScheduler.instance)
//            .bind { (_) in
//                if let strongSelf = weakSelf {
////                    strongSelf.reloadData()
//                }
//            }.addDisposableTo(disponseBag)
//        
        advertisementItem
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind { (item) in
                if let `item` = item {
                    if let strongSelf = weakSelf {
                        strongSelf.advertisementTableViewCell.set(item: item)
//                        strongSelf.reloadData()
                    }
                }
        }
            .addDisposableTo(disponseBag)
        
//        bind()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


/// 每个Section的标题View
class HomeVCMainViewTitleSessionView: UITableViewHeaderFooterView {
    
    static let viewHeight:CGFloat = 45.0
    
    override var textLabel:UILabel {
        get {
            guard let label = self.viewWithTag(1) as? UILabel else {
                let label = UILabel()
                
                self.addSubview(label)
                
                label.textColor = UIConfig.generalColor.selected
                label.font = UIConfig.generalSemiboldFont(15)
                label.snp.makeConstraints({ (maker) in
                    maker.centerX.equalTo(self)
                    maker.top.equalTo(self).offset(15)
                })
                label.tag = 1
                
                let leftLine = ViewFactory.view(color: UIConfig.generalColor.selected)
                
                self.addSubview(leftLine)
                
                leftLine.snp.makeConstraints({ (maker) in
                    maker.size.equalTo(CGSize(width:25, height:0.5))
                    maker.centerY.equalTo(label)
                    maker.right.equalTo(label.snp.left).offset(-15)
                })
                
                let rightLine = ViewFactory.view(color: UIConfig.generalColor.selected)
                
                self.addSubview(rightLine)
                
                rightLine.snp.makeConstraints({ (maker) in
                    maker.size.equalTo(CGSize(width:25, height:0.5))
                    maker.centerY.equalTo(label)
                    maker.left.equalTo(label.snp.right).offset(15)
                })
                
                return label
            }
            
            return label
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.backgroundView = UIView()
        
        self.backgroundView?.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// CoverCell
class HomeVCMainViewCoverCell: CoverTableViewCell {
    
    static var cellHeight:CGFloat = 8 * UIScreen.main.bounds.w / 15
    
    let itemList = Variable<[HomeVCMainView.CoverItem]>([])
    
    let disposeBag = DisposeBag()
    
    weak var urlPush:PublishSubject<(url:URL?, title:String)>?
    
    init(_ urlPush:PublishSubject<(url:URL?, title:String)>) {
        
        super.init()
        
        self.urlPush = urlPush
        
        itemList
            .asObservable()
            .bind { (items) in
                self.urlList = items.map({ $0.imageUrl })
        }.addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func coverTapHandler(with tag: Int) {
        let item = itemList.value[tag]
        
        urlPush?.onNext((url: item.url,
                         title: ""))
    }
}

/// 全球Top10
class HomeVCTopCell: UITableViewCell {
    let disposeBag = DisposeBag()
    
    let itemList:Variable<[ProductItem]> = Variable([])
    
    static let cellHeight:CGFloat = 179.5
    
    static let cellHeightWithOutRank:CGFloat = 161
    
    let scrollView:UIScrollView
    
    var itemViews:[UIView] = [] {
        didSet (oldVal) {
            for one in oldVal {
                one.removeFromSuperview()
            }
            
            var left:CGFloat = 0
            
            for (_, one) in itemViews.enumerated() {
                left += 15
                
                scrollView.addSubview(one)
                
                one.left = left
                
                left += (one.w + 15)
            }
            
            scrollView.contentSize = CGSize(width: left, height: 0)
        }
    }
    
    let productSelected:Driver<(productId:String, productName:String)>
    
    static func viewWithItem (item:ProductItem) -> UIView {
        let boundsWidth:CGFloat = 150
        
        let view = UIView(frame: CGRect(x: 0,
                                        y: 0,
                                        w: boundsWidth,
                                        h: HomeVCTopCell.cellHeight))
        
        // set image
        let imageView = UIImageView(frame: CGRect(x: 10,
                                                  y: 10,
                                                  w: boundsWidth - 20,
                                                  h: 90))
        
        imageView.backgroundColor = UIConfig.generalColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.kf.setImage(with:item.imageUrl, options:[.scaleFactor(UIScreen.main.scale)])
        imageView.clipsToBounds = true
        
        view.addSubview(imageView)
        
        // add block
        let redBlock = UIView(frame: CGRect(x: 0,
                                            y: 105,
                                            w: 5,
                                            h: 10))
        
        redBlock.backgroundColor = UIConfig.generalColor.selected
        
        view.addSubview(redBlock)
        
        // add top title
        let topTitleLabel = UILabel()
        
        topTitleLabel.text = item.rank
        topTitleLabel.textColor = UIConfig.generalColor.selected
        topTitleLabel.font = UIConfig.arialBoldFont(12)
        topTitleLabel.sizeToFit()
        topTitleLabel.left = redBlock.right + 5
        topTitleLabel.centerY = redBlock.centerY
        
        view.addSubview(topTitleLabel)
        
        // add title
        let titleLabel = UILabel()
        
        titleLabel.text = item.name
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.numberOfLines = 1
        titleLabel.textColor = UIConfig.generalColor.selected
        titleLabel.font = UIConfig.generalFont(12)
        titleLabel.w = view.w
        titleLabel.fitHeight()
        titleLabel.origin = CGPoint(x: 0, y: topTitleLabel.bottom + 6.5)
        titleLabel.backgroundColor = UIConfig.generalColor.white
        
        view.addSubview(titleLabel)
        
        if item.rank.length <= 0 {
            redBlock.isHidden = true
            
            topTitleLabel.isHidden = true
            
            titleLabel.top = 105
        }
        
        // add price label
        
        let priceLabel = UILabel()
        
        priceLabel.attributedText = getPriceAttributedString(rmb: item.rmb,
                                                             ori: item.price)
        priceLabel.backgroundColor = UIConfig.generalColor.white
        priceLabel.top = titleLabel.bottom + 6.5
        priceLabel.sizeToFit()
        priceLabel.w = view.w
        
        view.addSubview(priceLabel)
        
        return view
    }
    
    init() {
        scrollView = UIScrollView()
        
        let itemAndViews = itemList
            .asObservable()
            .map({ $0.map({
                (one) -> (view:UIView, item:ProductItem) in
                return (view:HomeVCTopCell.viewWithItem(item: one), item:one)
                
            })
            }).asDriver(onErrorJustReturn: [])
        
        productSelected = itemAndViews.flatMapLatest({
            (array) -> Driver<Driver<(productId:String, productName:String)>> in
            
            let driverArray = array.map({
                (collection:(view:UIView, item:ProductItem)) -> Driver<(productId:String, productName:String)> in
                let view = collection.view
                
                let item = collection.item
                
                let tap = UITapGestureRecognizer()
                
                view.addGestureRecognizer(tap)
                
                return tap.rx.event.asDriver(onErrorJustReturn: tap).map{_ in (productId:item.recId, productName:item.name)}
            })
            
            return Observable
                .from(driverArray).asDriver(onErrorJustReturn: Driver<(productId:String, productName:String)>.empty())
        }).merge()
        
        super.init(style: .default, reuseIdentifier: "HomeVCTopCell")
        
        self.selectionStyle = .none
        
        scrollView.frame = self.bounds
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.contentView.addSubview(scrollView)
        
        itemAndViews.map{$0.map{$0.view}}.drive(onNext: {[weak self] (views) in
            self?.itemViews = views
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


/// Brand
class HomeVCBrandCell: UITableViewCell {
    let disposeBag = DisposeBag()
    
    static var cellHeight:CGFloat {
        get {
            return (UIScreen.main.bounds.width - 30) / 1.5 + 20
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustSubviewFrames()
    }

    private func adjustSubviewFrames() {
        let itemWidth = (self.w - 30) / 3
        
        for (index, view) in self.contentView.subviews.enumerated() {
            switch index {
            case 0:
                view.frame = CGRect(x: 15.0, y: 0, w: itemWidth, h: itemWidth)
            case 1:
                view.frame = CGRect(x: 15.0, y: itemWidth, w: itemWidth, h: itemWidth)
            case 2:
                view.frame = CGRect(x: 15 + itemWidth, y: 0, w: itemWidth, h: itemWidth)
            case 3:
                view.frame = CGRect(x: 15 + itemWidth, y: itemWidth, w: itemWidth, h: itemWidth)
            case 4:
                view.frame = CGRect(x: 15 + itemWidth + itemWidth, y: 0, w: itemWidth, h: itemWidth)
            case 5:
                view.frame = CGRect(x: 15 + itemWidth + itemWidth, y: itemWidth, w: itemWidth, h: itemWidth)
            default:
                break
            }
        }
    }
    
    let itemList:Variable<[HomeVCMainView.BrandItem]> = Variable([])
    
    let brandSelected:Observable<(brandId:String, brandName:String)>
    
    init() {
        let tempObserver = itemList.asObservable().map({ itemList in
            return itemList.map({
                one -> (view:UIView, item:HomeVCMainView.BrandItem) in
                
                let view = ViewFactory.view(color: UIConfig.generalColor.white)
                
                let imageView = UIImageView(frame: .zero)
                
                imageView.kf.setImage(with:one.imageUrl,
                                      options:[.scaleFactor(UIScreen.main.scale)])
                imageView.backgroundColor = UIConfig.generalColor.white
                imageView.contentMode = .scaleAspectFit
                imageView.isUserInteractionEnabled = true
                imageView.clipsToBounds = true
                
                view.layer.borderWidth = 0.5
                view.layer.borderColor = UIConfig.generalColor.backgroudWhite.cgColor
                view.layer.masksToBounds = true
                
                view.addSubview(imageView)
                
                imageView.snp.makeConstraints({ (maker) in
                    maker.center.equalTo(view)
                    maker.edges.equalTo(view).inset(UIEdgeInsetsMake(10, 10, 10, 10))
                })
                
                return (view:view, item:one)
            })
        }).shareReplay(1)
        
        brandSelected = tempObserver.flatMapLatest{ (array) in
            return Observable.from(array.map{
                (one) -> Observable<(brandId:String, brandName:String)> in
                
                let tap = UITapGestureRecognizer()
                
                one.view.addGestureRecognizer(tap)
                
                return tap.rx.event.map{_ in (brandId:one.item.id, brandName:one.item.name)}
            }).merge()
        }
        
        super.init(style: .default, reuseIdentifier: "HomeVCBrandCell")
        
        self.selectionStyle = .none
        
        weak var weakSelf = self

        tempObserver.map{$0
            .map{$0
                .view}}
            .asObservable().bind { (views) in
                
                if let strongSelf = weakSelf {
                    
                    strongSelf.contentView.removeSubviews()
                    strongSelf.contentView.addSubviews(views)
                    strongSelf.adjustSubviewFrames()
                }
        }.addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class HomeVCEmptyTableViewCell: UITableViewCell {
    
    let label:UILabel
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        label = UILabel()
        
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(label)
        
        self.label.text = "敬请期待^_^"
        self.label.font = UIConfig.generalFont(20)
        self.label.textColor = UIConfig.generalColor.unselected
        self.label.backgroundColor = UIConfig.generalColor.white
        self.label.textAlignment = .center
        self.label.snp.makeConstraints({ (maker) in
            maker.edges.equalTo(self.contentView).inset(UIEdgeInsets.zero)
        })
    }
    
    static var cellHeight:CGFloat = 50
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class HomeVCActivityCell: UITableViewCell {
    var title:String? {
        didSet {
            titleLabel.text = title
            
            layoutLabel()
        }
    }
    
    var watchNumber:Int = 0 {
        didSet {
            let attributedString = NSMutableAttributedString(imageNamed: "icon_page_view")
            
            attributedString.append(NSAttributedString(string: " \(watchNumber)",
                font: UIConfig.generalFont(14),
                textColor: UIConfig.generalColor.labelGray))
            
            subTitleLabel.attributedText = attributedString
            
            layoutLabel()
        }
    }
    
    let backgroudImageView:ExcursionImageView = ExcursionImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        titleLabel = ViewFactory.generalLabel(generalSize: 20,
                                              textColor: UIConfig.generalColor.selected)
        
        subTitleLabel = ViewFactory.generalLabel(generalSize: 14,
                                                 textColor: UIConfig.generalColor.labelGray)
        
        
        backgroudImageView.moveType = .fixed(50)
        
        let width = UIScreen.main.bounds.size.width - 30
        
        let height = width * 35 / 69
        
        backgroudImageView.frame = CGRect(x: 15,
                                          y: 0.5,
                                          width: width,
                                          height: height)
        
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        self.contentView.addSubview(backgroudImageView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(subTitleLabel)
        
        let line = ViewFactory.line()
        
        self.contentView.addSubview(line)
        
        line.frame = CGRect(x: 15,
                            y: 0,
                            width: 53,
                            height: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func layoutLabel() {
        // 布局中间标题
        
        /// backgroud
        let width = self.bounds.width - 30
        
        let height = width * 35 / 69
        
        backgroudImageView.frame = CGRect(x: 15,
                                          y: 0,
                                          width: width,
                                          height: height)
        
        titleLabel.sizeToFit()
        titleLabel.left = 15
        titleLabel.top = height + 16
        
        subTitleLabel.sizeToFit()
        subTitleLabel.left = 15
        subTitleLabel.top = titleLabel.frame.maxY + 2
    }
    
    static func height(of width:CGFloat) -> CGFloat {
        return (width - 30) * 35 / 69 + 80.5
    }
    
    fileprivate let titleLabel:UILabel
    
    fileprivate let subTitleLabel:UILabel
    
    static func createLabel (fontSize:CGFloat) -> UILabel {
        let label = UILabel()
        
        label.font = UIConfig.generalFont(fontSize)
        label.backgroundColor = UIColor.clear
        label.textColor = UIConfig.generalColor.white
        
        return label
    }
}

extension HomeVCMainView: UITableViewDataSource, UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for one in self.visibleCells {
            if let cell = one as? ExcursionTableViewCell {
                cell.scrollInTableView(self)
            } else if let cell = one as? HomeVCMainViewCoverCell {
                cell.scrollInTableView(self)
            } else if let cell = one as? HomeVCActivityCell {
                cell.backgroudImageView.scrollInScrollView(self)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5 + ((advertisementItem.value != nil) ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if let item = advertisementItem.value{
            if indexPath.section == 2 {
                urlSelected.onNext((url: item.addressUrl,
                                    title: ""))
                
                API.Advertisement.clickBy(advertisementId: item.id)
                
                return
            }
            
            if indexPath.section == 5 {
                subjectSelected.onNext((subjectId: themeItemList.value[indexPath.row].id,
                                        subjectName: themeItemList.value[indexPath.row].title))
                
                return
            }
        }
        
        if indexPath.section == 4 {
            subjectSelected.onNext((subjectId: themeItemList.value[indexPath.row].id,
                                    subjectName: themeItemList.value[indexPath.row].title))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let _ = self.advertisementItem.value {
            switch section {
            case 0:
                return coverCell.itemList.value.count > 0 ? 1 : 0
            case 1:
                return topCell.itemList.value.count > 0 ? 1 : 0
            case 3:
                return brandCell.itemList.value.count > 0 ? 1 : 0
            case 4:
                return preferentialCell.itemList.value.count > 0 ? 1 : 0
            case 5:
                return themeItemList.value.count
            default:
                return 1
            }
        }
        
        switch section {
        case 0:
            return coverCell.itemList.value.count > 0 ? 1 : 0
        case 1:
            return topCell.itemList.value.count > 0 ? 1 : 0
        case 2:
            return brandCell.itemList.value.count > 0 ? 1 : 0
        case 3:
            return preferentialCell.itemList.value.count > 0 ? 1 : 0
        case 4:
            return themeItemList.value.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let _ = self.advertisementItem.value {
            if indexPath.section == 0 {
                return coverCell
            } else if indexPath.section == 1 {
                return topCell
            } else if indexPath.section == 2 {
                return advertisementTableViewCell
            } else if indexPath.section == 3 {
                return brandCell
            } else if indexPath.section == 4 {
                return preferentialCell
            } else if indexPath.section == 5 {
                let item = themeItemList.value[indexPath.row]
                
                let cell:HomeVCActivityCell = tableView.dequeueReusableCell(at: indexPath)
                
                cell.backgroudImageView.setImage(url: item.imageUrl)
                cell.title = item.title
                cell.watchNumber = item.number
                
                return cell
            }
        }
        
        if indexPath.section == 0 {
            return coverCell
        } else if indexPath.section == 1 {
            return topCell
        } else if indexPath.section == 2 {
            return brandCell
        } else if indexPath.section == 3 {
            return preferentialCell
        } else if indexPath.section == 4 {
            let item = themeItemList.value[indexPath.row]
            
            let cell:HomeVCActivityCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.backgroudImageView.setImage(url: item.imageUrl)
            cell.title = item.title
            cell.watchNumber = item.number
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = self.advertisementItem.value {
            switch section {
            case 1:
                return topCell.itemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
            case 3:
                return brandCell.itemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
            case 4:
                return preferentialCell.itemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
            case 5:
                return themeItemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
            default:
                return CGFloat.leastNormalMagnitude
            }
        }
        
        switch section {
        case 1:
            return topCell.itemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
        case 2:
            return brandCell.itemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
        case 3:
            return preferentialCell.itemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
        case 4:
            return themeItemList.value.count > 0 ? 50 : CGFloat.leastNormalMagnitude
        default:
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = self.advertisementItem.value {
            switch indexPath.section {
            case 0:
                return coverCell.itemList.value.count > 0 ? HomeVCMainViewCoverCell.cellHeight : CGFloat.leastNormalMagnitude
            case 1:
                return topCell.itemList.value.count > 0 ? HomeVCTopCell.cellHeight : CGFloat.leastNormalMagnitude
            case 2:
                return tableView.w * advertisementTableViewCell.radio
            case 3:
                return brandCell.itemList.value.count > 0 ? HomeVCBrandCell.cellHeight : CGFloat.leastNormalMagnitude
            case 4:
                return preferentialCell.itemList.value.count > 0 ? HomeVCTopCell.cellHeightWithOutRank : CGFloat.leastNormalMagnitude
            case 5:
                return HomeVCActivityCell.height(of: UIScreen.main.bounds.width)
            default:
                return 100.0
            }
        }
        
        switch indexPath.section {
        case 0:
            return coverCell.itemList.value.count > 0 ? HomeVCMainViewCoverCell.cellHeight : CGFloat.leastNormalMagnitude
        case 1:
            return topCell.itemList.value.count > 0 ? HomeVCTopCell.cellHeight : CGFloat.leastNormalMagnitude
        case 2:
            return brandCell.itemList.value.count > 0 ? HomeVCBrandCell.cellHeight : CGFloat.leastNormalMagnitude
        case 3:
            return preferentialCell.itemList.value.count > 0 ? HomeVCTopCell.cellHeightWithOutRank : CGFloat.leastNormalMagnitude
        case 4:
            return HomeVCActivityCell.height(of: UIScreen.main.bounds.width)
        default:
            return 100.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if let _ = self.advertisementItem.value {
            switch section {
            case 0:
                return coverCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
            case 1:
                return topCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
            case 3:
                return brandCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
            case 4:
                return preferentialCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
            case 5:
                return self.themeItemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
            default:
                return 10.0
            }
        }
        
        switch section {
        case 0:
            return coverCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
        case 1:
            return topCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
        case 2:
            return brandCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
        case 3:
            return preferentialCell.itemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
        case 4:
            return self.themeItemList.value.count > 0 ? 10.0 : CGFloat.leastNormalMagnitude
        default:
            return 10.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        
        view.backgroundColor = UIConfig.generalColor.backgroudGray
        
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }
        
        
        let titleView:HomeVCMainViewTitleSessionView = tableView.dequeueReusableView()
        
        if let _ = self.advertisementItem.value {
            switch section {
            case 1:
                titleView.textLabel.text = titles.value[0]
            case 2:
                return nil
            case 3:
                titleView.textLabel.text = titles.value[1]
            case 4:
                titleView.textLabel.text = titles.value[2]
            case 5:
                titleView.textLabel.text = titles.value[3]
            default:
                break
            }
            
            return titleView
        }
        
        switch section {
        case 1:
            titleView.textLabel.text = titles.value[0]
        case 2:
            titleView.textLabel.text = titles.value[1]
        case 3:
            titleView.textLabel.text = titles.value[2]
        case 4:
            titleView.textLabel.text = titles.value[3]
        default:
            break
        }
        
        return titleView
    }
}
