//
//  HomeVCCategoryView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/1.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreGraphics

/// 首页分类布局
class HomeVCCategoryLayout : UICollectionViewLayout {
    
    typealias SubjectLayout = (left:CGFloat, width:CGFloat)
    
    var subjectLeftPoint:[CGFloat] = []
    
    var subjectSize:CGSize = CGSize.zero
    
    var sectionTop:[CGFloat] = []
    
    var layoutAttributesArray:[UICollectionViewLayoutAttributes] = []
    
    var contentWidth:CGFloat = 0
    
    var productWidth:CGFloat = 0
    
    var brandWidth:CGFloat = 0
    
    var recommends:Variable<[HomeMainProductGroupItem]> = Variable([])
    
    override var collectionViewContentSize:CGSize {
        
        let height = sectionTop.reduce(0.0, {
            (result:CGFloat, item:CGFloat) -> CGFloat in
            
            return max(result, item)
        })
        
        return CGSize(width: 0, height: height)
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let currentCollectionView = collectionView, currentCollectionView.collectionViewLayout == self else {
            return
        }
        
        let width = self.collectionView?.w ?? 0
        
        contentWidth = width - 30
        
        productWidth = (width - 45) / 2.0
        
        brandWidth = (width - 30) / 3.0
        
        layoutAttributesArray = []
        
        let sectionNumber = currentCollectionView.numberOfSections
        
        sectionTop = Array(repeating: 0, count: sectionNumber + 1)
        
        func fillToEnd (index:Int) {
            let count = sectionTop.count
            
            var currentIndex = index + 1
            
            if index >= count {
                return
            }
            
            let val = sectionTop[index]
            
            while currentIndex < count {
                sectionTop[currentIndex] = val
                
                currentIndex += 1
            }
        }
        
        // get first line size
        let subjectCount = currentCollectionView.numberOfItems(inSection: 0)
        
        if subjectCount > 0 {
            subjectLeftPoint = Array(repeating: 17.5, count: 4)
            
            let subjectDiameter = (width - 175) / 4.0
            
            for index in 0...3 {
                subjectLeftPoint[index] = 17.5 + (35 + subjectDiameter) * CGFloat(index)
            }
            
            subjectSize = CGSize(width: subjectDiameter + 35,
                                 height: 15 + subjectDiameter + 10 + 12)
            
            for index in 0..<subjectCount {
                let indexPath = IndexPath(row: index,
                                          section: 0)
                
                if let attrs = layoutAttributesForItem(at: indexPath) {
                    
                    sectionTop[1] = max(sectionTop[1], attrs.frame.maxY)
                    
                    layoutAttributesArray.append(attrs)
                }
            }
            
            sectionTop[1] += 30
            
            fillToEnd(index: 1)
            
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Foot",
                                                                              at: IndexPath(row: 0,
                                                                                            section: 0))!)
        }
        
        // two line
        
        /// add title
        
        /// add product
        let hotCount = currentCollectionView.numberOfItems(inSection: 1)
        if hotCount > 0 {
            
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Head",
                                                                              at: IndexPath(row: 0,
                                                                                            section: 1))!)
            for index in 0..<hotCount {
                let indexPath = IndexPath(row: index,
                                          section: 1)
                
                if let attrs = layoutAttributesForItem(at: indexPath) {
                    
                    sectionTop[2] = max(sectionTop[2], attrs.frame.maxY)
                    
                    layoutAttributesArray.append(attrs)
                }
            }
            
            sectionTop[2] += 15
            
            fillToEnd(index: 2)
            
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Foot",
                                                                              at: IndexPath(row: 0,
                                                                                            section: 1))!)

        }
        
        // brand
        
        let brandCount = currentCollectionView.numberOfItems(inSection: 2)
        
        if brandCount > 0 {
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Head",
                                                                              at: IndexPath(row: 0,
                                                                                            section: 2))!)
            
            for index in 0..<brandCount {
                let indexPath = IndexPath(row: index,
                                          section: 2)
                
                if let attrs = layoutAttributesForItem(at: indexPath) {
                    
                    sectionTop[3] = max(sectionTop[3], attrs.frame.maxY)
                    
                    layoutAttributesArray.append(attrs)
                }
            }
            
            sectionTop[3] += 30
            
            fillToEnd(index: 3)
            
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Foot",
                                                                              at: IndexPath(row: 0,
                                                                                            section: 2))!)
        }
        // recomand
        
        if sectionNumber > 3 {
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Head",
                                                                              at: IndexPath(row: 0,
                                                                                            section: 3))!)
            
            for index in 3 ..< sectionNumber {
                
                let recommandCount = self.collectionView?.numberOfItems(inSection: index) ?? 0
                
                if recommandCount <= 0 {
                    continue
                }
                
                layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Recommand",
                                                                                  at: IndexPath(row: 0,
                                                                                                section: index))!)
                
                for itemIndex in 0 ..< recommandCount {
                    let indexPath = IndexPath(row: itemIndex,
                                              section: index)
                    
                    if let attrs = layoutAttributesForItem(at: indexPath) {
                        
                        sectionTop[index + 1] = max(sectionTop[index + 1], attrs.frame.maxY)
                        
                        
                        layoutAttributesArray.append(attrs)
                    }
                }
                
                fillToEnd(index: index + 1)
            }
            
            sectionTop[sectionNumber] += 15
            
            layoutAttributesArray.append(layoutAttributesForSupplementaryView(ofKind: "Foot",
                                                                              at: IndexPath(row: 0,
                                                                                            section: sectionNumber - 1))!)
        }
    }
    
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attrs = super.layoutAttributesForItem(at: indexPath) ?? UICollectionViewLayoutAttributes(forCellWith: indexPath)
        
        switch indexPath.section {
        case 0:
            let line:Int = (indexPath.row) / 4
            
            let y:CGFloat = CGFloat(line) * subjectSize.height
            
            let index = indexPath.row % 4
            
            attrs.frame.origin = CGPoint(x: subjectLeftPoint[index],
                                         y: y)
            attrs.frame.size = subjectSize
        case 1:
            let row = indexPath.row
            
            if row == 0 {
                attrs.frame = CGRect(x: 15,
                                     y: sectionTop[1] + 50,
                                     w: contentWidth,
                                     h: HomeVCCategoryViewTopImageCell.cellHeight)
            } else if row == 1 {
                attrs.frame = CGRect(x: 15,
                                     y: sectionTop[1] + 50 + HomeVCCategoryViewTopImageCell.cellHeight,
                                     w: contentWidth,
                                     h: HomeVCCategoryViewTopLabelCell.cellHeight)
            } else if row > 1 {
                let index = row - 2
                
                let height = ProductCollectionViewCell.cellHeight(with: productWidth,
                                                                  and: true)
                
                let x = index % 2 == 0 ? 15 : (30 + productWidth)
                
                let y = CGFloat((Int)(index / 2)) * height + sectionTop[1] + 50 + HomeVCCategoryViewTopImageCell.cellHeight + HomeVCCategoryViewTopLabelCell.cellHeight
            
                attrs.frame = CGRect(x: x,
                                     y: y,
                                     w: productWidth,
                                     h: height)
            }
        case 2:
            let index = indexPath.row
            
            let top = sectionTop[2] + 50
            
            let x = 15 + brandWidth * CGFloat(index % 3)
            
            let y = CGFloat((Int)(index / 3)) * brandWidth + top
            
            attrs.frame = CGRect(x: x,
                                 y: y,
                                 w: brandWidth,
                                 h: brandWidth)
        default:
            if indexPath.section > 2 {
                let index = indexPath.row
                
                let height = ProductCollectionViewCell.cellHeight(with: productWidth,
                                                                  and: false)
                
                let x = index % 2 == 0 ? 15 : (30 + productWidth)
                
                let top = sectionTop[indexPath.section] + HomeVCCategoryViewRecommandCell.cellHeight + (indexPath.section == 3 ?
                    HomeVCCategoryViewTitleView.viewHeight : 0) + 5
                
                let y = CGFloat((Int)(index / 2)) * height + top
                
                attrs.frame = CGRect(x: x,
                                     y: y,
                                     w: productWidth,
                                     h: height)
            }
            
        }
        
        return attrs
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return layoutAttributesArray
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attrs = super.layoutAttributesForSupplementaryView(ofKind: elementKind,
                                                               at: indexPath) ?? UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind,
                                                                                                                  with: indexPath)
        
        if elementKind == "Foot" {
            attrs.frame = CGRect(x: 0,
                                 y: sectionTop[indexPath.section + 1] - 10,
                                 w: collectionView?.w ?? 0,
                                 h: 10)
        } else if elementKind == "Head" && indexPath.section > 0 {
            attrs.frame = CGRect(x: 0,
                                 y: sectionTop[indexPath.section],
                                 w: collectionView?.w ?? 0,
                                 h: 50)
        } else if elementKind == "Recommand" {
            attrs.frame = CGRect(x: 15,
                                 y: (indexPath.section == 3 ?
                                    HomeVCCategoryViewTitleView.viewHeight : 0) + 5
                                    + sectionTop[indexPath.section],
                                 w: contentWidth,
                                 h: HomeVCCategoryViewRecommandCell.cellHeight)
        }
        
        return attrs
    }
}

class HomeVCCategoryView : UICollectionView, PresenterType {
    
    let viewModel:HomeCategoryVM
    
    weak var parentViewController:UIViewController? = nil
    
    var categorys:Variable<[ProductTypeItem]> = Variable([])
    
    var title:String = ""
    
    var hotTitle:String = "热卖推荐"
    
    var brandTitle:String = "品牌推荐"
    
    var recommendTitle:String = "主题推荐"
    
    var hotProducts:Variable<[ProductItem]> = Variable([])
    
    var brands:Variable<[ProductBrandItem]> = Variable([])
    
    var recommends:Variable<[HomeMainProductGroupItem]> = Variable([])
    
    var brandSelected = PublishSubject<(brandId:String, brandName:String)>()
    
    let productSelected = PublishSubject<(productId:String, productName:String)>()
    
    let subjectSelected = PublishSubject<(subjectId:String, subjectName:String)>()
    
    let disposeBag = DisposeBag()
    
    var bindDisposeBag: DisposeBag?
    
    let typeSelected = PublishSubject<ProductTypeItem>()
    
    init (viewModel:HomeCategoryVM){
        self.viewModel = viewModel
        
        super.init(frame: UIScreen.main.bounds,
                   collectionViewLayout: HomeVCCategoryLayout())
        
        self.setInset(top: 106.5, bottom: 49)
        self.allowsSelection = true
        self.allowsMultipleSelection = false
        
        self.registerCells(HomeVCCategoryViewSubjectCell.self,
                           HomeVCCategoryViewTopImageCell.self,
                           HomeVCCategoryViewTopLabelCell.self,
                           ProductCollectionViewCell.self,
                           HomeVCCategoryViewBrandCell.self)
        
        self.registerView(from: ["Foot": HomeVCCategoryViewFootView.self,
                                 "Head": HomeVCCategoryViewTitleView.self,
                                 "Recommand": HomeVCCategoryViewRecommandCell.self
                                 ])
        
        self.backgroundColor = UIColor.white
        
        self.delegate = self
        self.dataSource = self
        
        weak var weakSelf = self
        
        categorys.asDriver().drive(onNext: { _ in
            if let strongSelf = weakSelf {
                strongSelf.reloadData()
            }
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        hotProducts.asDriver().drive(onNext: { _ in
            if let strongSelf = weakSelf {
                strongSelf.reloadData()
            }
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        recommends.asDriver().drive(onNext: { _ in
            if let strongSelf = weakSelf {
                strongSelf.reloadData()
                strongSelf.collectionViewLayout.invalidateLayout()
            }
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
//        bind()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for one in self.visibleCells {
            if let cell = one as? ExcursionView {
                cell.scrollInScrollView(self)
            }
        }
        
        if #available(iOS 9.0, *) {
            let views = self.visibleSupplementaryViews(ofKind: "Recommand")
            
            for view in views {
                if let excursionView = view as? ExcursionView {
                    excursionView.scrollInScrollView(self)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
}


extension HomeVCCategoryView: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            let item = categorys.value[indexPath.row]
            
            typeSelected.onNext(item)
        case 1:
            if indexPath.row == 1 {
                return
            }
            
            let item = indexPath.row == 0 ? hotProducts.value[0] : hotProducts.value[indexPath.row - 1]
            
            productSelected.onNext((productId: item.recId, productName: item.name))
        case 2:
            let item = brands.value[indexPath.row]
            
            brandSelected.onNext((brandId: item.id, brandName: item.name))
        default:
            if indexPath.section > 2 {
                let product = recommends.value[indexPath.section - 3].products[indexPath.row]
                
                productSelected.onNext((productId: product.recId, productName: product.name))
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        collectionView.collectionViewLayout.invalidateLayout()
        return 3 + recommends.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return categorys.value.count
        case 1:
            return hotProducts.value.count <= 0 ? 0 : hotProducts.value.count + 1
        case 2:
            return brands.value.count
        default:
            if section > 2 {
                return recommends.value[section - 3].products.count
            }
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell:HomeVCCategoryViewSubjectCell = collectionView.dequeueReusableCell(at: indexPath)
            
            cell.item = categorys.value[indexPath.row]
            
            return cell
        case 1:
            if indexPath.row == 0 {
                let cell:HomeVCCategoryViewTopImageCell = collectionView.dequeueReusableCell(at: indexPath)
                
                cell.item = hotProducts.value[0]
                
                return cell
            } else if indexPath.row == 1 {
                let cell:HomeVCCategoryViewTopLabelCell = collectionView.dequeueReusableCell(at: indexPath)
                
                cell.item = hotProducts.value[0]
                
                return cell
            } else if indexPath.row > 1 {
                let cell:ProductCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
                
                cell.item = hotProducts.value[indexPath.row - 1]
                
                return cell
            }
        case 2:
            
            let cell:HomeVCCategoryViewBrandCell = collectionView.dequeueReusableCell(at: indexPath)
            
            cell.item = brands.value[indexPath.row]
            
            return cell
        default:
            if indexPath.section > 2 {
                let cell:ProductCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
                
                cell.item = recommends.value[indexPath.section - 3].products[indexPath.row]
                
                return cell
            }
            
            fatalError("Error Code")
        }
        
        fatalError("Error Code")
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case "Head":
            let view:HomeVCCategoryViewTitleView = collectionView.dequeueReusableView(kind: kind, indexPath: indexPath)
            
            switch indexPath.section {
            case 1:
                view.textLabel.text = hotTitle
            case 2:
                view.textLabel.text = brandTitle
            case 3:
                view.textLabel.text = recommendTitle
            default:
                break
            }
            
            return view
        case "Foot":
            let view:HomeVCCategoryViewFootView = collectionView.dequeueReusableView(kind: kind,
                                                                                     indexPath: indexPath)
            
            return view
        case "Recommand":
            let view:HomeVCCategoryViewRecommandCell = collectionView.dequeueReusableView(kind: kind,
                                                                                          indexPath: indexPath)
            
            let index = indexPath.section - 3
            
            view.item = recommends.value[index]
            view.subjectSelected = subjectSelected
            
            return view
        default:
            break
        }
        
        return UICollectionReusableView()
    }
}


/// 每个Section的标题View
class HomeVCCategoryViewTitleView: UICollectionReusableView {
    
    static let viewHeight:CGFloat = 45.0
    
    let textLabel:UILabel
    
    override init(frame: CGRect) {
        textLabel = UILabel()
        
        super.init(frame: frame)
        
        self.backgroundColor = UIConfig.generalColor.white
        
        self.addSubview(textLabel)
        
        textLabel.textColor = UIConfig.generalColor.selected
        textLabel.font = UIConfig.generalSemiboldFont(15)
        textLabel.snp.makeConstraints({ (maker) in
            maker.centerX.equalTo(self)
            maker.top.equalTo(self).offset(15)
        })
        textLabel.tag = 1
        
        let leftLine = ViewFactory.view(color: UIConfig.generalColor.selected)
        
        self.addSubview(leftLine)
        
        leftLine.snp.makeConstraints({ (maker) in
            maker.size.equalTo(CGSize(width:25, height:0.5))
            maker.centerY.equalTo(textLabel)
            maker.right.equalTo(textLabel.snp.left).offset(-15)
        })
        
        let rightLine = ViewFactory.view(color: UIConfig.generalColor.selected)
        
        self.addSubview(rightLine)
        
        rightLine.snp.makeConstraints({ (maker) in
            maker.size.equalTo(CGSize(width:25, height:0.5))
            maker.centerY.equalTo(textLabel)
            maker.left.equalTo(textLabel.snp.right).offset(15)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// 每个Section的Foot
class HomeVCCategoryViewFootView: UICollectionReusableView {
    
    static let viewHeight:CGFloat = 10.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIConfig.generalColor.backgroudGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// 分类主题
class HomeVCCategoryViewSubjectCell: UICollectionViewCell {
    
    static let nullImage:UIImage? = {
        let bounds = CGRect(x: 0, y: 0, w: 100, h: 100)
        
        UIGraphicsBeginImageContextWithOptions(bounds.size,
                                               false,
                                               UIScreen.main.scale)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            
            return nil
        }
        
        
        context.setFillColor(UIConfig.generalColor.selected.cgColor)
        context.addArc(center: bounds.center,
                       radius: 50,
                       startAngle: 0,
                       endAngle: CGFloat(2.0 * CGFloat.pi),
                       clockwise: true)
        context.fillPath()
        
        let output = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return output
        
    }()
    
    let imageView:UIImageView = {
        let imageView = UIImageView()
        
        imageView.backgroundColor = UIColor.clear
        imageView.image = HomeVCCategoryViewSubjectCell.nullImage
        
        return imageView
    }()
    
    let label:UILabel = {
        let label = TopAlignLabel()
        
        label.textAlignment = .center
        label.font = UIConfig.generalFont(12)
        label.textColor = UIConfig.generalColor.unselected
        label.backgroundColor = UIConfig.generalColor.white
        label.numberOfLines = 0
        
        return label
    }()
    
    var item:ProductTypeItem? {
        didSet {
            /// set image
            if imageView.superview != self.contentView {
                self.contentView.addSubview(imageView)
            }
            
            imageView.kf.setImage(with:item?.imageUrl,
                                  placeholder: HomeVCCategoryViewSubjectCell.nullImage)
            
            /// set title
            if label.superview != self.contentView {
                self.contentView.addSubview(label)
            }
            
            label.text = item?.name
            
            adjustLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustLayout()
    }
    
    private func adjustLayout() {
        let width = self.contentView.bounds.width - 40
        
        imageView.frame = CGRect(x: 20,
                                 y: 15,
                                 w: width,
                                 h: width)
        
        let labelWidth = self.contentView.bounds.width - 10
        
        label.w = labelWidth
        label.top = imageView.bottom + 5
        label.centerX = imageView.centerX
        label.fitHeight()
    }
}

/// Hot
class HomeVCCategoryViewTopImageCell: ExcursionCollectionViewCell {
    
    static let cellHeight:CGFloat = 150
    
    let labelImageView = UIImageView(named: "icon_top1")
    
    let shadowView = ViewFactory.view(color: UIColor.black.withAlphaComponent(0.3))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.moveType = .fixed(50)
        
        // add view
        self.contentView.addSubview(shadowView)
        
        shadowView.snp.makeConstraints { (maker) in
            maker.center.equalTo(self.contentView)
            maker.size.equalTo(self.contentView)
        }
        
        self.contentView.addSubview(labelImageView)
        
        labelImageView.snp.makeConstraints { (maker) in
            maker.center.equalTo(self.contentView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var item:ProductItem? {
        didSet {
            setImage(url: item?.imageUrl)
        }
    }
}

/// Top One
class HomeVCCategoryViewTopLabelCell: UICollectionViewCell {
    static let cellHeight:CGFloat = 74.5
    
    let titleLabel = UILabel()
    
    let priceLabel = UILabel()
    
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // add title
        self.contentView.addSubview(titleLabel)

        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.numberOfLines = 1
        titleLabel.textColor = UIConfig.generalColor.selected
        titleLabel.font = UIConfig.generalFont(12)
        titleLabel.backgroundColor = UIConfig.generalColor.white
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.contentView).offset(15)
            maker.left.right.equalTo(self.contentView)
        }
        
        // add price label
        self.contentView.addSubview(priceLabel)
        
        priceLabel.backgroundColor = UIConfig.generalColor.white
        priceLabel.font = UIConfig.arialFont(13)
        priceLabel.textColor = UIConfig.generalColor.red
        priceLabel.snp.makeConstraints { (maker) in
            maker.left.right.equalTo(self.contentView)
            maker.top.equalTo(titleLabel.snp.bottom).offset(6.5)
        }
    }
    
    var item:ProductItem? {
        didSet {
            imageView.kf.setImage(with: item?.imageUrl)
            
            titleLabel.text = item?.name
            
            priceLabel.attributedText = getPriceAttributedString(rmb: item?.rmb ?? Currency(unit: "￥", value: 0),
                                                                 ori: item?.price ?? Currency(unit: "$", value: 0))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// Brand
class HomeVCCategoryViewBrandCell: UICollectionViewCell {
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // add image
        self.contentView.addSubview(imageView)
        
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.clear
        imageView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(10, 10, 10, 10))
        }
        
        self.view.layer.borderWidth = 0.5
        self.view.layer.borderColor = UIConfig.generalColor.backgroudWhite.cgColor
        self.view.layer.masksToBounds = true
    }
    
    var item:ProductBrandItem? {
        didSet {
            imageView.kf.setImage(with:item?.imageUrl, options:[.scaleFactor(UIScreen.main.scale)])
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


/// Recommand
class HomeVCCategoryViewRecommandCell: ExcursionReusableView {
    
    static let cellHeight:CGFloat = 175.0
    
    let label = InvertedMaskLabel()
    
    let squareView = UIView()
    
    let shapeLayer = CAShapeLayer()
    
    let shadowView = ViewFactory.view(color: UIColor.black.withAlphaComponent(0.3))
    
    weak var subjectSelected:PublishSubject<(subjectId:String, subjectName:String)>? = nil
    
    static let path:CGPath = {
        let bezierPath = UIBezierPath()
        
        let rect = CGRect(x: 0,
                          y: 0,
                          w: 32,
                          h: 15)
        
        bezierPath.move(to: CGPoint(x: 0, y: 15))
        bezierPath.addLine(to: CGPoint(x: 32, y: 15))
        bezierPath.addLine(to: CGPoint(x: 16, y: 0))
        bezierPath.close()
        
        return bezierPath.cgPath
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.moveType = .fixed(50)
        
        self.addSubview(shadowView)
        
        // add square
        self.addSubview(squareView)
        
        squareView.backgroundColor = UIConfig.generalColor.white
        
        // add label
        label.font = UIFont(name: "TrebuchetMS-Bold",
                            size: 20)
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        
        squareView.mask = label
        
        shapeLayer.fillColor = UIConfig.generalColor.white.cgColor
        shapeLayer.frame = CGRect(x: 0, y: 0, w: 64, h: 30)
        shapeLayer.backgroundColor = UIColor.clear.cgColor
        shapeLayer.path = HomeVCCategoryViewRecommandCell.path
        
        self.layer.addSublayer(shapeLayer)
        
        self.addTapGesture { [weak self] (_) in
            if let item = self?.item, let subject = self?.subjectSelected {
                subject.onNext((subjectId: item.id,
                                subjectName: item.title))
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func adjustLayout () {
        label.sizeToFit()
        
        shadowView.frame = self.bounds
        
        squareView.size = CGSize(width: label.w + 32,
                                 height: label.h + 7)
        
        label.frame = squareView.bounds
        
        label.centerInSuperView()
        
        squareView.centerInSuperView()
        
        shapeLayer.frame = CGRect(x: 35,
                                  y: self.bounds.height - 14,
                                  w: 32,
                                  h: 15)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustLayout()
    }
    
    var item:HomeMainProductGroupItem? {
        didSet {
            self.setImage(url: item?.coverUrl)
            
            label.text = item?.title
            
            adjustLayout()
        }
    }
}
