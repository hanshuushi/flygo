//
//  SubjectVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class SubjectVC: UIViewController, PresenterType {
    
    let viewModel: SubjectVM
    
    var bindDisposeBag: DisposeBag?
    
    let collectionView:UICollectionView
    
    let productSelected:PublishSubject<(productId:String, productName:String)>
    
    var subjectCollection:SubjectCollection? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    init(subjectId:String, subjectTitle:String) {
        viewModel = SubjectVM(subjectId: subjectId)
        
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .vertical
        
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: layout)
        
        productSelected = PublishSubject()
        
        super.init(nibName: nil,
                   bundle: nil)
        
        self.title = subjectTitle
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // right item
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(named: "icon_share_gray"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(SubjectVC.shareClick(sender:)))
        
        // collection view
        self.view.addSubviews(collectionView)
        
        collectionView.backgroundColor = UIConfig.generalColor.white
        collectionView.registerHeaderView(viewClass: CoverHeaderView.classForCoder())
        collectionView.registerHeaderView(viewClass: TitleHeaderView.classForCoder())
        collectionView.registerCell(cellClass: ProductCollectionViewCell.classForCoder())
        collectionView.snp.makeConstraints { (maker) in
            maker.center.size.equalTo(self.view)
        }
        
        bind()
    }
    
    var shareItem:WechatManager.ShareModuleItem?
}

extension SubjectVC {
    func shareClick(sender:UIButton) {
        guard let item = shareItem else {
            
            self.showToast(text: "数据载入中...")
            
            return
        }
        
        WechatManager
            .shareInstance
            .showShareProductActionSheet(on: self,
                                         moduleItem: item,
                                         sender: sender)
    }
}

extension SubjectVC {
    typealias SubjectCollection = (cover:URL?, title:String, products:[ProductItem])
}

extension SubjectVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        guard let subjectCollection = self.subjectCollection else {
            return 0
        }
        
        if section == 1 {
            return subjectCollection.products.count
        }
        
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 9.0, *) {
            for one in collectionView.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader) {
                if let coverHeaderView = one as? CoverHeaderView {
                    coverHeaderView.scrollInTableView(scrollView)
                }
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return subjectCollection == nil ? 0 : 2
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = subjectCollection!.products[indexPath.row]
        
        productSelected.onNext((productId: item.recId,
                                productName: item.name))
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section != 1 || subjectCollection == nil {
            fatalError("What's the fuck!")
        }
        
        let cell:ProductCollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
        
        cell.item = subjectCollection!.products[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 0 {
            let view:CoverHeaderView = collectionView.dequeueHeaderView(indexPath: indexPath)
            
            view
                .coverImageView
                .kf
                .setImage(with: subjectCollection?.cover.flatMap({ $0 }))
            
            return view
        } else if indexPath.section == 1 {
            let view:TitleHeaderView = collectionView.dequeueHeaderView(indexPath: indexPath)
            
            view
                .titleLabel
                .attributedText = TitleHeaderView
                    .titleAttributedString(from: subjectCollection!.title)
            
            return view
        }
        
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case 0:
            return CGSize(width: collectionView.w,
                          height: CoverHeaderView.cellHeight(with: collectionView.w))
        case 1:
            return CGSize(width: collectionView.w,
                          height: TitleHeaderView.cellHeight(with: subjectCollection?.title ?? "",
                                                             and: collectionView.w))
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let productWidth = (collectionView.w - 45) / 2.0
        
        return CGSize(width: productWidth,
                      height: ProductCollectionViewCell.cellHeight(with: productWidth,
                                                                   and: false))
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension SubjectVC {
    class CoverHeaderView: UICollectionReusableView {
        
        let coverImageView:UIImageView
        
        override init(frame: CGRect) {
            coverImageView = UIImageView()
            
            super.init(frame: frame)
            
            self.addSubview(coverImageView)
            
            coverImageView.contentMode = .scaleAspectFill
            coverImageView.backgroundColor = UIConfig.generalColor.backgroudGray
            coverImageView.clipsToBounds = true
            coverImageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
            
            let boardView = UIImageView(named: "icon_mask")
            
            coverImageView.addSubview(boardView)
            
            self.translatesAutoresizingMaskIntoConstraints = false
            
            coverImageView.frame = self.bounds
            
            boardView.frame = CGRect(x: 0,
                                     y: self.h - boardView.h,
                                     width: self.w,
                                     height: boardView.h)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        static func cellHeight(with width:CGFloat) -> CGFloat {
            if width <= 0 {
                return 0
            }
            
            return width * 7 / 15
        }
        
        func scrollInTableView(_ sv:UIScrollView) {
            let offsetX = sv.contentOffset.y + sv.contentInset.top
            
            let scale:CGFloat = {
                if (offsetX < 0) {
                    return (self.h - offsetX) / self.h
                } else {
                    return 1.0
                }
            }()
            
            coverImageView.layer.transform = CATransform3DMakeScale(scale, scale, 1)
        }
    }
}

extension SubjectVC {
    class TitleHeaderView: UICollectionReusableView {
        let titleLabel:UILabel
        
        override init(frame: CGRect) {
            titleLabel = ViewFactory.label(font: UIConfig.generalFont(15),
                                           textColor: UIConfig.generalColor.selected)
            titleLabel.numberOfLines = 0
            
            super.init(frame: frame)
            
            let left = UIImageView(named: "icon_quotes_left")
            
            let right = UIImageView(named: "icon_quotes_right")
            
            self.addSubviews(titleLabel, left, right)
            
            left.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(self).offset(30)
                maker.size.equalTo(left.size)
            }
            
            right.snp.makeConstraints { (maker) in
                maker.right.equalTo(self).offset(-30)
                maker.bottom.equalTo(self).offset(-35)
                maker.size.equalTo(right.size)
            }
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.center.equalTo(self)
                maker.left.greaterThanOrEqualTo(left.snp.right).offset(15)
                maker.right.lessThanOrEqualTo(right.snp.left).offset(-15)
            }
        }
        
        static func cellHeight(with text:String,
                                and width:CGFloat) -> CGFloat {
            
            if width <= 0 {
                return 135
            }
            
            let attributedString = titleAttributedString(from: text)
            
            let labelWidth = width - 136
            
            let size = attributedString.boundingRect(with: CGSize(width:labelWidth, height: CGFloat.greatestFiniteMagnitude),
                                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                                     context: nil)
            let height = size.height + 130
            
            return height
        }
        
        static func titleAttributedString(from title:String) -> NSAttributedString {
            return NSAttributedString(string: title,
                                      font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.selected,
                                      lineSpace: 5)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

