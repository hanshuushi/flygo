//
//  SubjectVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SubjectVM: ViewModel {
    
    let subjectId:String
    
    init(subjectId:String) {
        self.subjectId = subjectId
    }
    
    
    func bind(to view: SubjectVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        let dataset = CollectionDataSet.init(view.collectionView,
                                             delegate: view,
                                             dataSource: view,
                                             refreshEnable: false)
        
        let subjectId = self.subjectId
        
        API.Advertisement.clickBy(modularId: self.subjectId)
        
        let request = API
            .SubjectInfo
            .getSubjectInfo(modularId: subjectId)
            .shareReplay(1)
        
        request
            .asRequestStatusObservable()
            .bind(to: dataset.status)
            .addDisposableTo(bag)
        
        request
            .map({ _ in false })
            .bind(to: dataset.isEmpty)
            .addDisposableTo(bag)
        
        let model = request
            .asModelObservable()
            .shareReplay(1)
        
        model
            .map { (info) -> (mdlId:String, picURL:URL?, mdlName:String) in
            return (mdlId:info.modularInfo.modularId,
                    picURL:info.modularInfo.pic,
                    mdlName:info.modularInfo.title)
        }
            .bind { [weak view] (shareItem) in
            view?.shareItem = shareItem
        }
            .addDisposableTo(bag)
        
        model
            .map { (info) -> SubjectVC.SubjectCollection in
            return (cover:info.modularInfo.pic,
                    title:info.modularInfo.title,
                    products:info.modularInfo.productList.map({ ProductItem(model: $0) }))
            }
            .observeOn(MainScheduler.instance)
            .bind { [weak view] (collection) in
                view?.subjectCollection = collection
        }.addDisposableTo(bag)
        
        view.productSelected.bind {
            
            [weak view] (collection) in
            let vm = ProductDetailVM(productId: collection.productId)
            
            let vc = ProductDetailVC(viewModel: vm)
            
            view?.navigationController?.pushViewController(vc,
                                                           animated: true)
        }.addDisposableTo(bag)
        
        bag.insert(dataset)
        
        return bag
    }
}
