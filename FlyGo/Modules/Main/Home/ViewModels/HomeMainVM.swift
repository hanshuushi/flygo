//
//  HomeVMMain.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/1.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

/// 全球免税的ViewModel
class HomeMainVM: HomeVM {
    
    let title:String
    
    let menuId:String
    
//    let coverItemList:Driver<[HomeMainVMCoverItem]>
//    
//    let topItemList:Driver<[ProductItem]>
//    
//    let brandItemList:Driver<[ProductBrandItem]>
//    
//    let themeItemList:Driver<[HomeMainVMThemeItem]>
//    
//    let preferentialItemList:Driver<[ProductItem]>
//    
//    let sectionTitles:Driver<[String]>
    
    let urlPushed:PublishSubject<(url:URL?, title:String)> = PublishSubject()
    
    let brandPushed:PublishSubject<(brandId:String, brandName:String)> = PublishSubject()
    
    let productPushed:PublishSubject<(productId:String, productName:String)> = PublishSubject()
    
    let subjectPushed:PublishSubject<(subjectId:String, subjectName:String)> = PublishSubject()
    
    let disposeBag = DisposeBag()
    
    func bind(to view: HomeVCMainView) -> DisposeBag? {
        
        let request = API.HomeMenuItem.getMenuModular(homeMenuId: menuId,
                                                      state: 0).shareReplay(1)
        
        let model = request.asModelObservable().shareReplay(1)
    
        
        let sectionTitles = model.map({ (item) -> [String] in
            return [item.top?.topName,
                    item.menuBrand?.menuBrandName,
                    item.promotion?.promotionName,
                    item.active?.activeName].map{$0 ?? ""}
        }).asDriver(onErrorJustReturn: ["","","",""]).startWith(["","","",""])
        
        /// Country
        let coverItemList = model
            .map({ $0.banner?.bannerList.map{HomeMainVMCoverItem(model: $0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        /// Top Ten
        let topItemList = model
            .map({ $0.top?.topList.map{ProductItem(model: $0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        /// Brand
        let brandItemList = model
            .map({ $0.menuBrand?.menuBrandList.map{ProductBrandItem(model: $0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        /// Top Ten
        let preferentialItemList = model
            .map({ $0.promotion?.promotionList.map{ProductItem(model: $0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        /// Theme
        let themeItemList = model
            .map({ $0.active?.activeList.map{HomeMainVMThemeItem(model: $0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        let dataset = TableViewDataSet.init(view,
                                            delegate: view,
                                            dataSource: view,
                                            finishStyle: .none,
                                            refreshEnable: false)
        
//        dataset.isEmpty.value = false
        
        let bindDisposeBag = DisposeBag()

        view
            .urlSelected
            .map{(url:$0, title:$1)}
            .bindNext(urlPushed)
            .addDisposableTo(bindDisposeBag)
    
        Driver.of(view
            .topCell
            .productSelected,
                      view
                        .preferentialCell
                        .productSelected
            ).merge()
            .driveNext(productPushed).addDisposableTo(bindDisposeBag)
        
        sectionTitles.asObservable().bind(to: view.titles).addDisposableTo(bindDisposeBag)
        
        coverItemList
            .map({ array in array
                .map({ $0 as HomeVCMainView.CoverItem } )})
            .drive(view.coverCell.itemList)
            .addDisposableTo(bindDisposeBag)
        
        topItemList
            .drive(view.topCell.itemList)
            .addDisposableTo(bindDisposeBag)
        
        brandItemList
            .drive(view.brandCell.itemList)
            .addDisposableTo(bindDisposeBag)
        
        view
            .brandCell
            .brandSelected
            .bindNext(brandPushed)
            .addDisposableTo(bindDisposeBag)
        
        view
            .subjectSelected
            .bindNext(subjectPushed)
            .addDisposableTo(bindDisposeBag)
        
        themeItemList
            .map{ array in array
                .map{ $0 as HomeVCMainView.ThemeItem }
            }
            .drive(view.themeItemList)
            .addDisposableTo(bindDisposeBag)
        
        preferentialItemList
            .drive(view.preferentialCell.itemList)
            .addDisposableTo(bindDisposeBag)
        
        let getAdvertisementRequest = DictionaryManager
            .shareInstance
            .advertisementList
            .map({ $0
                .filter({ $0.type == 1 })
                .first })
            .filter({ $0 != nil }).map { (model) -> AdvertisementItem in
                return AdvertisementItem(model: model!)
        }
        
        getAdvertisementRequest
            .drive(view.advertisementItem)
            .addDisposableTo(bindDisposeBag)
        
        let isEmpty = model.map {
            (item) -> Bool in
            let count1 = item.banner?.bannerList.count ?? 0
            
            let count2 = item.top?.topList.count ?? 0
            
            let count3 = item.menuBrand?.menuBrandList.count ?? 0
            
            let count4 = item.promotion?.promotionList.count ?? 0
            
            return count1 == 0 && count2 == 0 && count3 == 0 && count4 == 0
        }
            .asDriver(onErrorJustReturn: true)
        
        isEmpty
            .drive(dataset.isEmpty)
            .addDisposableTo(bindDisposeBag)
        
        getAdvertisementRequest
            .withLatestFrom(isEmpty).filter({ !$0 })
            .drive(dataset.isEmpty)
            .addDisposableTo(bindDisposeBag)
        
        
//            .bind(to: dataset.isEmpty)
//            .addDisposableTo(bindDisposeBag)
        
//        Driver.combineLatest(coverItemList.map({ $0.count }),
//                             topItemList.map({ $0.count }),
//                             brandItemList.map({ $0.count }),
//                             preferentialItemList.map({ $0.count }),
//                             themeItemList.map({ $0.count })) { (counts) -> Bool in
//                                if counts.0 == 0, counts.1 == 0, counts.2 == 0, counts.3 == 0, counts.4 == 0 {
//                                    return true
//                                }
//                                
//                                return false
//        }
//            .drive(dataset.isEmpty)
//            .addDisposableTo(bindDisposeBag)
        
//        getAdvertisementRequest
//            .withLatestFrom(dataset.isEmpty.asDriver()) { (item, isEmpty) -> Bool in
//                return false
//            }
        
        request
            .asRequestStatusObservable()
            .bind(to: dataset.status)
            .addDisposableTo(bindDisposeBag)
        
        bindDisposeBag.insert(dataset)
        
        return bindDisposeBag
    }
    
    required init(title:String, menuId:String) {
        
        self.title = title
        
        self.menuId = menuId
    }
}
