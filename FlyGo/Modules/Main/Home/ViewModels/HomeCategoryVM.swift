//
//  HomeVMCategory.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/1.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class HomeCategoryVM: HomeVM {
    
    var title:String
    
    let urlPushed:PublishSubject<(url:URL?, title:String)> = PublishSubject()
    
//    let productFilterPublish = PublishSubject<ProductFilterViewModel>()
    
    let brandPushed = PublishSubject<(brandId:String, brandName:String)>()
    
    let productPushed = PublishSubject<(productId:String, productName:String)>()
    
    let subjectPushed = PublishSubject<(subjectId:String, subjectName:String)>()
    
    let typePushed = PublishSubject<ProductTypeItem>()
    
    let disposeBag = DisposeBag()
    
    let menuId:String
    
    required init(title:String, menuId:String) {
        self.title = title
        
        self.menuId = menuId
    }
    
    func bind(to view: HomeVCCategoryView) -> DisposeBag? {
        
        let bindDisposeBag = DisposeBag()
        
        let dataSet = CollectionDataSet.init(view,
                                             delegate: view,
                                             dataSource: view,
                                             refreshEnable: false)
        
        let request = API.HomeMenuItem.getMenuModular(homeMenuId: menuId,
                                                      state: 1).shareReplay(1)
        
        let model = request.asModelObservable().shareReplay(1)
        
        request
            .asRequestStatusObservable()
            .bind(to: dataSet.status)
            .addDisposableTo(bindDisposeBag)
        
        let subjects = model
            .map({ $0.menuTypeList.map{ProductTypeItem(model:$0)}})
            .asDriver(onErrorJustReturn: [])
        
        let hotProducts = model
            .map({ $0.top?.topList.map{ProductItem(model:$0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        let brands = model
            .map({ $0.menuBrand?.menuBrandList.map{ProductBrandItem(model:$0)} ?? [] })
            .asDriver(onErrorJustReturn: [])
        
        let recommends = model
            .map({ $0.modularList.map{HomeMainProductGroupItem(model:$0)} })
            .asDriver(onErrorJustReturn: [])
        
        model.bind { (item) in
            if let name = item.top?.topName {
                view.hotTitle = name
            }
            
            if let name = item.menuBrand?.menuBrandName {
                view.brandTitle = name
            }
        }.addDisposableTo(disposeBag)
        
        Driver.combineLatest(subjects,
                             hotProducts,
                             brands,
                             recommends) { (subjects, hotProducts, brands, recommends) -> Bool in
                                return subjects.count <= 0 && hotProducts.count <= 0 && brands.count <= 0 && recommends.count <= 0
        }
            .drive(dataSet.isEmpty)
            .addDisposableTo(bindDisposeBag)
        
        view.brandSelected.bindNext(brandPushed).addDisposableTo(bindDisposeBag)
        
        view.productSelected.bindNext(productPushed).addDisposableTo(bindDisposeBag)
        
        view.typeSelected.bindNext(typePushed).addDisposableTo(bindDisposeBag)
        
        view.subjectSelected.bindNext(subjectPushed).addDisposableTo(bindDisposeBag)
        
        subjects.drive(view.categorys).addDisposableTo(bindDisposeBag)
        
        hotProducts.drive(view.hotProducts).addDisposableTo(bindDisposeBag)
        
        brands.drive(view.brands).addDisposableTo(bindDisposeBag)
        
        recommends.drive(view.recommends).addDisposableTo(bindDisposeBag)
        
        bindDisposeBag.insert(dataSet)
        
        return bindDisposeBag
    }
}
