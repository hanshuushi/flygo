//
//  MessageEntry.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/15.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


fileprivate let labelBageLayerAssociatedKey = NSData(data: "labelBageLayerAssociatedKey".data(using: .utf8)!).bytes
fileprivate let labelBageOffsetAssociatedKey = NSData(data: "labelBageOffsetAssociatedKey".data(using: .utf8)!).bytes
fileprivate let labelBageAssociatedKey = NSData(data: "labelBageAssociatedKey".data(using: .utf8)!).bytes

extension UIView {
    
    private var bageLayer:CALayer? {
        set {
            if let oldLayer = bageLayer {
                oldLayer.removeFromSuperlayer()
            }
            
            objc_setAssociatedObject(self,
                                     labelBageLayerAssociatedKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            if let newLayer = newValue {
                self.layer.addSublayer(newLayer)
                
                adjustBageLayout()
            }
        }
        get {
            guard let val = objc_getAssociatedObject(self, labelBageLayerAssociatedKey) as? CALayer else {
                return nil
            }
            
            return val
        }
    }
    
    static let methodSwizzlingToken = NSUUID().uuidString
    
    var bage:Bool {
        set(newValue) {
            if bage == newValue {
                return
            }
            
            objc_setAssociatedObject(self,
                                     labelBageAssociatedKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN)
            
            if newValue {
                let shapeLayer = CAShapeLayer()
                
                shapeLayer.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
                
                let path = UIBezierPath(arcCenter: shapeLayer.bounds.center,
                                        radius: 4.5,
                                        startAngle: 0,
                                        endAngle: CGFloat(CGFloat.pi * 2),
                                        clockwise: true)
                
                path.lineWidth = 1
                
                shapeLayer.fillColor = UIConfig.generalColor.red.cgColor
                shapeLayer.strokeColor = UIConfig.generalColor.white.cgColor
                shapeLayer.path = path.cgPath
                
                bageLayer = shapeLayer
            } else {
                bageLayer = nil
            }
        }
        get {
            guard let val = objc_getAssociatedObject(self, labelBageAssociatedKey) as? Bool else {
                return false
            }
            
            return val
        }
    }
    
    private func adjustBageLayout() {
        if let bageLayer = self.bageLayer {
            let offset = bageOffset
            
            bageLayer.position = CGPoint(x: self.bounds.maxX - offset.width,
                                         y: offset.height)
        }
    }
    
    var bageOffset:CGSize {
        get {
            guard let val = objc_getAssociatedObject(self, labelBageOffsetAssociatedKey) as? NSValue else {
                return CGSize.zero
            }
            
            return val.cgSizeValue
        }
        set {
            objc_setAssociatedObject(self,
                                     labelBageOffsetAssociatedKey,
                                     NSValue(cgSize:newValue),
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
            adjustBageLayout()
        }
    }
}

class MessageEntryVC: UIViewController, PresenterType {
    
    var tableView:UITableView!
    
    let viewModel:MessageEntryVM
    
    var bindDisposeBag: DisposeBag?
    
    let infoCells:[InfoTableViewCell]
    
    let disposeBag:DisposeBag
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        viewModel = MessageEntryVM()
        
        disposeBag = DisposeBag()
        
        infoCells = [InfoTableViewCell(title: "系统消息"),
                     InfoTableViewCell(title: "订单消息"),
                     InfoTableViewCell(title: "接单消息")]
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "联系客服",
                                                                 style: .plain,
                                                                 target: FlygoUtil.self,
                                                                 action: #selector(FlygoUtil.callServer))
        
        self.title = "消息"
        
        tableView = UITableView(frame: .zero,
                                style: .plain)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.rowHeight = 55
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.sectionFooterHeight = 10
        tableView.sectionHeaderHeight = CGFloat.leastNormalMagnitude
        tableView.registerCell(cellClass: ChatRecordTableViewCell.self)
        tableView.registerCell(cellClass: InfoTableViewCell.self)
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        
        self.view.addSubviews(tableView)
        
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        
        bind()
    }
    
    var chatRecords:[AVIMConversation] = []
}

extension MessageEntryVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return chatRecords.count > 0 ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let footerView = view as? UITableViewHeaderFooterView {
            footerView.backgroundView = ViewFactory.view(color: UIColor.clear)
            footerView.contentView.backgroundColor = UIColor.clear
        } else {
            view.backgroundColor = UIColor.clear
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return infoCells.count
        case 1:
            return chatRecords.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 10 : CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return infoCells[indexPath.row]
        } else if indexPath.section == 1 {
            let cell:ChatRecordTableViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.set(record: chatRecords[indexPath.row])
            
            return cell
        }
        
        fatalError()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let type = MessageRequestType(rawValue: indexPath.row + 1)!
            
            let vc = MessageVC(type: type)
            
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.section == 1 {
            let record = chatRecords[indexPath.row]
            
            let vc = ChatRoomVC(conversation: record)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension MessageEntryVC {
    class ChatRecordTableViewCell: UITableViewCell {
        let avatar:UIImageView
        
        let nickName:UILabel
        
        let message:UILabel
        
        let time:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            avatar = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            avatar.bageOffset = CGSize(width: 4.5, height: 4.5)
            
            nickName = ViewFactory.label(font: UIConfig.generalFont(15),
                                         textColor: UIConfig.generalColor.selected)
            
            message = ViewFactory.label(font: UIConfig.generalFont(12),
                                         textColor: UIConfig.generalColor.unselected)
            
            time = ViewFactory.label(font: UIConfig.generalFont(12),
                                         textColor: UIConfig.generalColor.labelGray)
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubviews(avatar, nickName, message, time)
            
            self.selectionStyle = .none
            
            avatar.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.left.equalTo(self.contentView).offset(15)
                maker.width.height.equalTo(40)
            }
            
            nickName.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatar.snp.right).offset(15)
                maker.top.equalTo(avatar)
                maker.right.lessThanOrEqualTo(time.snp.left).offset(-15)
            }
            
            message.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatar.snp.right).offset(15)
                maker.bottom.equalTo(avatar)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-15)
            }
            
            time.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(nickName)
                maker.right.equalTo(self.contentView).offset(-15)
            }
        }
        
        func set(record:AVIMConversation) {
            avatar.setAvatar(url: record.avatarPath)
            avatar.bage = record.unread
            
            nickName.text = record.nickName
            
            message.text = record.latestMessage
            
            if let date = record.latestMessageDate {
                time.text = AVIMMessage.Time.getTitle(from: date)
            } else {
                time.text = ""
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension MessageEntryVC {
    class InfoTableViewCell: UITableViewCell {
        
        let accessImageView:UIImageView
        
        let label:UILabel
        
        init(title: String) {
            accessImageView = UIImageView(named:"icon_more-brand")
            
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "InfoTableViewCell")
            
            self.contentView.addSubviews(accessImageView, label)
            
            self.selectionStyle = .none
            
            self.label.text = title
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.backgroundColor = UIColor.clear
            self.label.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.left.equalTo(self.contentView).offset(15)
            })
            self.label.sizeToFit()
            
            accessImageView.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(self.contentView).offset(-15)
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
