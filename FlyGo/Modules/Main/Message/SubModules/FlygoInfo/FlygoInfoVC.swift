//
//  FlygoInfoVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/22.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MapKit

class FlygoInfoVC: UIViewController, PresenterType {
    
    var bindDisposeBag: DisposeBag?
    
    let viewModel:FlygoInfoVM
    
    let mapView:MKMapView
    
    let userInfoView:UserInfoCard
    
    init(userId:String, nickName:String, avatar:URL?, orders:[String]) {
        viewModel = FlygoInfoVM(userId: userId,
                                orders: orders)
        
        
        mapView = MKMapView(frame: UIScreen.main.bounds)
        
        userInfoView = UserInfoCard(nickName: nickName, avatar: avatar)
        
        super.init(nibName: nil, bundle: nil)
        
        self.title = nickName
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var myLocation:CLLocation?
    
    func mapViewTapGesture(sender:UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.25,
                       delay: 0,
                       options: self.userInfoView.isOut ? UIViewAnimationOptions.curveEaseOut : UIViewAnimationOptions.curveEaseIn,
                       animations: {
                        self.userInfoView.switchInOrOut()
        },
                       completion: nil)
    }
    
    func userCardTapGesture(sender:UITapGestureRecognizer) {
        if let annotation = mapView.annotations.filter({ ($0 as? Annotation) != nil }).first {
            setMapCenter(to: annotation.coordinate,
                         cardViewHeight: userInfoView.h,
                         isAnimat: true)
        }
    }
    
    func popPressed(sender:Any?) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        /// inser map
        self.view.addSubview(mapView)
        
        mapView.isRotateEnabled = false
        mapView.isPitchEnabled = false
        mapView.delegate = self
        mapView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        mapView.showsUserLocation = true
        mapView.addTapGesture(target: self,
                              action: #selector(FlygoInfoVC.mapViewTapGesture(sender:)))
        
        if let userLocation = mapView.userLocation.location {
            myLocation = userLocation
            
            setMapCenter(to: userLocation.coordinate,
                         cardViewHeight: userInfoView.systemLayoutSizeFitting(CGSize(width:self.view.bounds.size.width - 30,
                                                                                     height: 1)).height,
                         isAnimat: false)
        } else {
            ////默认移动到北京天安门
            setMapCenter(to: CLLocationCoordinate2D(latitude: 39.915168,
                                                    longitude: 116.403875),
                         cardViewHeight: userInfoView.systemLayoutSizeFitting(CGSize(width:self.view.bounds.size.width - 30,
                                                                                     height: 1)).height,
                         isAnimat: false)
            
            mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 39.915168,
                                                                                longitude: 116.403875),
                                                 span: FlygoInfoVC.defaultSpan),
                              animated: false)
        }
        
        /// add pop button
        let popButton = ViewFactory.button(imageNamed: "icon_return")
        
        self.view.addSubview(popButton)
        
        popButton.addTarget(self,
                            action: #selector(FlygoInfoVC.popPressed(sender:)),
                            for: .touchUpInside)
        popButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(8)
            maker.top.equalTo(self.view).offset(26)
            maker.size.equalTo(CGSize(width:46, height:30))
        }
        popButton.contentHorizontalAlignment = .left
        
        /// inser userInfo
        self.view.addSubview(userInfoView)
        
        userInfoView.avatarParentView.addTapGesture(target: self,
                                                    action: #selector(FlygoInfoVC.userCardTapGesture(sender:)))
        userInfoView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(15)
            maker.bottom.right.equalTo(self.view).offset(-15)
        }
        
        bind()
    }
    
    func set(infoItem:FlygoInfoItem) {
        UIView.animate(withDuration: 0.3) {
            self.userInfoView.set(infoItem: infoItem)
            self.userInfoView.layoutIfNeeded()
        }
        
        mapView.removeAnnotations(mapView.annotations.filter({ ($0 as? Annotation) != nil }))
        
        if let locationItem = infoItem.location {
            
            let annotation = Annotation(item: locationItem)
            
            mapView.addAnnotation(annotation)
            
            let cardheight = userInfoView.systemLayoutSizeFitting(CGSize(width:self.view.bounds.size.width - 30,
                                                                         height: 1)).height
            
            setMapCenter(to: annotation.coordinate,
                         cardViewHeight: cardheight,
                         isAnimat: true)
        }
    }
    
    static let defaultSpan = MKCoordinateSpan(latitudeDelta: 0.009310, longitudeDelta: 0.007812)
    
    func setMapCenter(to coordinate:CLLocationCoordinate2D,
                      cardViewHeight height:CGFloat,
                      isAnimat animate:Bool) {
        
        var frame = mapView.convertRegion(MKCoordinateRegion(center: coordinate, span: FlygoInfoVC.defaultSpan),
                                          toRectTo: UIApplication.shared.keyWindow!)
    
        let size = UIScreen.main.bounds.size
        
        let oriLong = mapView.region.span.longitudeDelta
        
        let newLong = FlygoInfoVC.defaultSpan.longitudeDelta
        
        let rate = CGFloat(oriLong / newLong)
        
        frame.y -= (size.height / 2.0 - (size.height - (height + 15) / 2.0)) / rate
        
        let region = mapView.convert(frame, toRegionFrom: UIApplication.shared.keyWindow!)
        
        mapView.setRegion(region,
                          animated: animate)
    }
}

extension FlygoInfoVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let flygoAnnotation = annotation as? Annotation {
            if let currentView = mapView.dequeueReusableAnnotationView(withIdentifier: "Flyman") {
                currentView.image = UIImage(named: "icon_feige_location")
                
                return currentView
            }
            
            let view = MKAnnotationView(annotation: flygoAnnotation,
                                        reuseIdentifier: "Flyman")
            
            view.image = UIImage(named: "icon_feige_location")
            
            return view
        } else if let myAnnotation = annotation as? MKUserLocation {
            if let currentView = mapView.dequeueReusableAnnotationView(withIdentifier: "Me") {
                currentView.image = UIImage(named: "icon_my_location")
                
                return currentView
            }
            
            let view = MKAnnotationView(annotation: myAnnotation,
                                        reuseIdentifier: "Me")
            
            view.image = UIImage(named: "icon_my_location")
            
            return view
        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if let location = userLocation.location {
            myLocation = location
        }
    }
}

extension FlygoInfoVC {
    class Annotation: NSObject, MKAnnotation {
        
        var coordinate: CLLocationCoordinate2D
        
        var title: String?

        var subtitle: String?
        
        init(item:(String, Date?, Double, Double)) {
            coordinate = CLLocationCoordinate2D(latitude: item.2,
                                                longitude: item.3)

            title = item.0
            
            if let date = item.1 {
                subtitle = date.generalFormartString
            }
            
            super.init()
        }
    }
}

extension FlygoInfoVC {
    class UserInfoCard: UIView, ObserverType, UIScrollViewDelegate {
        
        let scrollView:UIScrollView
        
        let locationLabel:UILabel
        
        let pageControl:PageControl
        
        let nickNameLabel:UILabel
        
        let avatarParentView:UIView
        
        init(nickName:String, avatar:URL?) {
            
            scrollView = UIScrollView()
            
            locationLabel = ViewFactory.label()
            
            pageControl = PageControl()
            
            nickNameLabel = ViewFactory.label(font: UIConfig.generalFont(15),
                                              textColor: UIConfig.generalColor.selected)
            
            avatarParentView = ViewFactory.view(color: .clear)
            
            super.init(frame: .zero)
            
            /// card
            let cardView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            let cardViewShadowView = ViewFactory.view(color: .clear)
            
            cardViewShadowView.addSubview(cardView)
            
            cardView.snp.fillToSuperView()
            
            cardView.layer.cornerRadius = 5
            cardView.layer.masksToBounds = true
            
            cardViewShadowView.layer.shadowColor = UIColor.black.cgColor
            cardViewShadowView.layer.shadowOffset = .zero
            cardViewShadowView.layer.shadowRadius = 5
            cardViewShadowView.layer.shadowOpacity = 0.06
            
            self.addSubview(cardViewShadowView)
            
            cardViewShadowView.snp.makeConstraints { (maker) in
                maker.top.equalTo(self).offset(30)
                maker.left.bottom.right.equalTo(self)
            }

            /// add avatar
            let avatarImageView = UIImageView()
            
            avatarImageView.backgroundColor = UIConfig.generalColor.white
            avatarImageView.layer.cornerRadius = 30
            avatarImageView.layer.borderWidth = 1
            avatarImageView.layer.borderColor = UIConfig.generalColor.white.cgColor
            
            
            avatarParentView.addSubview(avatarImageView)
            
            avatarImageView.snp.fillToSuperView()
            
            avatarParentView.layer.shadowColor = UIColor.black.cgColor
            avatarParentView.layer.shadowOffset = .zero
            avatarParentView.layer.shadowRadius = 5
            avatarParentView.layer.shadowOpacity = 0.2
            
            avatarImageView.setAvatar(url: avatar)
            
            self.addSubview(avatarParentView)
            
            avatarParentView.snp.makeConstraints { (maker) in
                maker.top.centerX.equalTo(self)
                maker.width.height.equalTo(60)
            }
            
            /// nickName
            
            nickNameLabel.text = nickName
            
            self.addSubview(nickNameLabel)
            
            nickNameLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(avatarParentView.snp.bottom).offset(10)
                maker.centerX.equalTo(avatarParentView)
            }
            
            /// location
            self.addSubview(locationLabel)
            
            locationLabel.snp.makeConstraints { (maker) in
                maker.left.greaterThanOrEqualTo(self.view).offset(15)
                maker.right.lessThanOrEqualTo(self.view).offset(-15)
                maker.centerX.equalTo(avatarParentView)
                maker.top.equalTo(nickNameLabel.snp.bottom).offset(10)
            }
    
            /// flyments
            
            pageControl.currentPage = 0
            pageControl.numberOfPages = 0
            
            self.addSubviews(scrollView, pageControl)
            
            scrollView.delegate = self
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.isPagingEnabled = true
            scrollView.snp.makeConstraints { (maker) in
                maker.top.equalTo(nickNameLabel.snp.bottom).offset(25).priority(999)
                maker.left.equalTo(self).offset(15)
                maker.right.equalTo(self).offset(-15)
                maker.top.equalTo(locationLabel.snp.bottom).offset(25)
            }
            
            pageControl.snp.makeConstraints({ (maker) in
                maker.top.equalTo(scrollView.snp.bottom).offset(25)
                maker.bottom.equalTo(self).offset(-15)
                maker.centerX.equalTo(self)
            })
        }
        
        func on(_ event: Event<DataSetRequestStatus>) {
            switch event {
            case .next(let status):
                switch status {
                case .loading:
                    isLoading = true
                    
                    let coverView = ViewFactory.view(color: UIConfig.generalColor.white)
                    
                    let loadingView = ActivityIndicatorView()
                    
                    coverView.addSubview(loadingView)
                    
                    loadingView.snp.makeConstraints({ (maker) in
                        maker.center.equalTo(coverView)
                    })
                    
                    self.coverView = coverView
                case .error(let message):
                    
                    isLoading = false
                    
                    let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                                  textColor: UIConfig.generalColor.labelGray)
                    
                    label.text = message
                    label.textAlignment = .center
                    
                    self.coverView = label
                case .normal:
                    
                    isLoading = true
                    
                    self.coverView = nil
                }
                
            default:
                return
            }
        }
        
        var coverView:UIView? {
            willSet {
                if let coverView = self.coverView {
                    coverView.removeFromSuperview()
                }
            }
            didSet {
                if let coverView = self.coverView {
                    self.addSubview(coverView)
                    
                    coverView.snp.makeConstraints({ (maker) in
                        maker.left.bottom.right.equalTo(self)
                        maker.top.equalTo(nickNameLabel.snp.bottom)
                    })
                }
            }
        }
        
        var labels = [UILabel]()
        
        var isLoading:Bool = false
        
        func set(infoItem:FlygoInfoItem) {
            for label in labels {
                label.removeFromSuperview()
            }
            
            if infoItem.flyments.count > 0 {
                
                let labels = infoItem.flyments.map({ (one) -> UILabel in
                    let label = ViewFactory.label()
                    
                    label.numberOfLines = 0
                    label.attributedText = NSAttributedString(string: "航班号：\(one.flightNo)\n起飞机场：\(one.startAirportName)\n起飞时间：\(one.startTime)\n到达机场：\(one.endAirportName)\n到达时间：\(one.endTime)",
                        font: UIConfig.generalFont(15),
                        textColor: UIConfig.generalColor.selected,
                        lineSpace: 5)
                    
                    return label
                })
                
                for (index, label) in labels.enumerated() {
                    scrollView.addSubview(label)
                    
                    label.snp.makeConstraints({ (maker) in
                        maker.width.top.equalTo(scrollView)
                        maker.height.lessThanOrEqualTo(scrollView)
                        
                        if index == 0 {
                            maker.left.equalTo(scrollView)
                        } else {
                            maker.left.equalTo(labels[index - 1].snp.right)
                        }
                        
                        if index == labels.count - 1 {
                            maker.right.equalTo(scrollView)
                        }
                    })
                }
            }
            
            pageControl.currentPage = 0
            pageControl.numberOfPages = infoItem.flyments.count
            pageControl.invalidateIntrinsicContentSize()
            
            if let locationItem = infoItem.location {
                let attrString = NSMutableAttributedString(imageNamed: "icon_location-1")
                
                attrString.append(NSAttributedString(string: locationItem.0,
                                                     font: UIConfig.generalFont(13),
                                                     textColor: UIConfig.generalColor.titleColor))
                
                if let date = locationItem.1 {
                    attrString.append(NSAttributedString(string: date.toString(format: "YYYY-mm-dd"),
                                                         font: UIConfig.generalFont(11),
                                                         textColor: UIConfig.generalColor.labelGray))
                }
                
                locationLabel.attributedText = attrString
            } else {
                locationLabel.text = ""
            }
            
            self.transform = .identity
            
            isOut = false
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.w + 0.5)
        }
        
        var isOut = false
        
        func switchInOrOut() {
            if isOut {
                self.transform = .identity
            } else {
                self.transform = CGAffineTransform(translationX: 0,
                                                   y: UIScreen.main.bounds.height)
            }
            
            self.isOut = !isOut
        }
        
        class PageControl : UIPageControl {
            override var currentPage: Int {
                //willSet {
                didSet { //so updates will take place after page changed
                    self.updateDots()
                }
            }
            
            override func layoutSubviews() {
                super.layoutSubviews()
                
                self.updateDots()
            }
            
            override var intrinsicContentSize: CGSize {
                if numberOfPages == 0 {
                    return .zero
                }
                
                return CGSize(width: (CGFloat(numberOfPages - 1) * CoverTableViewCell.gap + CGFloat(numberOfPages) * CoverTableViewCell.squareSize.width),
                              height: CoverTableViewCell.squareSize.height)
            }
            
            func updateDots() {
                
                if subviews.count <= 0 { return }
                
                self.w = (CGFloat(subviews.count - 1) * CoverTableViewCell.gap + CGFloat(subviews.count) * CoverTableViewCell.squareSize.width)
                
                var left:CGFloat = 0
                
                for (i, view) in subviews.enumerated() {
                    view.frame = CGRect(x: left,
                                        y: -CoverTableViewCell.squareSize.height / 2.0,
                                        w: CoverTableViewCell.squareSize.width,
                                        h: CoverTableViewCell.squareSize.height)
                    
                    view.backgroundColor = i == currentPage ? UIConfig.generalColor.red : UIConfig.generalColor.red.withAlphaComponent(0.3)
                    
                    left = view.right + CoverTableViewCell.gap
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

