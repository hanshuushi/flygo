//
//  FlygoInfoVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/23.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct FlygoInfoFlymentItem {
    var flightNo:String
    
    var startAirportName:String
    var startTime:String
    
    var endAirportName:String
    var endTime:String
    
    init(item:API.Flyment) {
        
        flightNo = item.flightNo
        
        startAirportName = item.startAirport
        
        startTime = [item.startAirportTimezone, item.takeoffTimeString]
            .filter({ $0.length > 0 })
            .joined(separator: ",")
        
        endAirportName = item.endAirport
        
        endTime = [item.endAirportTimezone, item.faillTimeString]
            .filter({ $0.length > 0 })
            .joined(separator: ",")
    }
}

struct FlygoInfoItem {
    var avatar:URL?
    
    var location:(String, Date?, Double, Double)?
    
    var flyments:[FlygoInfoFlymentItem]
    
    init(item:API.FlygoInfo, items:[FlygoInfoFlymentItem]) {
        avatar = item.userAvatar
        
        if let locationTrack = item.locationTrack {
            location = (locationTrack.locationName,
                        locationTrack.createTime,
                        locationTrack.latitude,
                        locationTrack.longitude)
        }
        
        flyments = items
    }
}

class FlygoInfoVM: ViewModel {
    
    var orders:[String]
    
    var userId:String
    
    init(userId:String, orders:[String]) {
        self.orders = orders
        
        self.userId = userId
    }
    
    func bind(to view:FlygoInfoVC) -> DisposeBag? {
        let bag = DisposeBag()
        
        let requstLocation = API.FlygoInfo.get(from: userId).shareReplay(1)
        
        let requestFlyments = API.Flyment.get(orders: orders).shareReplay(1)
        
        let status = Observable
            .combineLatest(requstLocation.asRequestStatusObservable(),
                           requestFlyments.asRequestStatusObservable()) { (s1, s2) -> DataSetRequestStatus in
                            if s1.rawValue == 0 || s2.rawValue == 0 {
                                return .loading
                            }
                            
                            if let error = (s1.errorString ?? s2.errorString) {
                                return .error(error)
                            }
                            
                            return .normal
            }
            .shareReplay(1)
        
        status
            .bindNext(view.userInfoView)
            .addDisposableTo(bag)
        
        let model = Observable
            .combineLatest(requstLocation.asModelObservable(),
                           requestFlyments.asModelsObservable())
            { (location, flyments) in
                return FlygoInfoItem(item: location, items: flyments.map{ FlygoInfoFlymentItem(item: $0) })
        }
        
        model
            .observeOn(MainScheduler.instance)
            .bind {
            [weak view] (item) in
                view?.set(infoItem: item)
        }.addDisposableTo(bag)
        
        return bag
    }
    
}
