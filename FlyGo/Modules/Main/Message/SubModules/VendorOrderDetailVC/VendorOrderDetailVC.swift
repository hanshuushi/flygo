//
//  VendorOrderDetailVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/2.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import WebKit

class VendorOrderDetailVC: WKWebViewController {
    
    init(vendorOrderInfoId:String) {
        let url = URLConfig.prefix + "flyapp/fly/unfinished.html?flag=1&token=\(UserManager.shareInstance.currentToken.urlEncode())&customerId=\(UserManager.shareInstance.currentId)&vendorOrderInfoId=\(vendorOrderInfoId)#!/unorderdetail.html"
        
        super.init(url: URL(string: url)!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
}
