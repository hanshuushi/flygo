//
//  ChatRoomVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/13.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import EZSwiftExtensions

class ChatRoomVC: UIViewController {
    
    let conversation:ChatManager.Conversation
    
    init(conversation:AVIMConversation) {
        
        self.conversation = ChatManager.currentSession!.conversation(for: conversation)
        
        super.init(nibName: nil,
                   bundle: nil)
        
        self.conversation.delegate = self
        
        self.title = self.conversation.avconversation.nickName
        
        MessageTableViewCell.targetAvatarURL = self.conversation.avconversation.avatarPath
        
        addKeyboardNotification()
        addUpdatedCoversationNotification()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var tableView:ChatTableView = ChatTableView(frame: .zero,
                                                style: .plain)
    
    var textField:UITextField!
    
    typealias Conversation = ChatManager.Conversation
    
    
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        /// set title
        
        /// set tableview
        self.view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(UIEdgeInsets.zero)
        }
        tableView.separatorStyle = .none
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(cellClass: MessageTableViewCell.self)
        tableView.registerCell(cellClass: TitleTableViewCell.self)
        tableView.setInset(top: 64, bottom: 45)
        tableView.keyboardDismissMode = .onDrag
        tableView.requestMoreHandler = {
            [weak self] in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf
                .conversation
                .queryMoreMessages(callback: { (_) in
                    
                    guard let strongSelf = self else {
                        return
                    }
                    
                    let tableView = strongSelf.tableView
                    
                    let offsetY = tableView.contentOffset.y
                    
                    let count = strongSelf.rowItems.count
                    
                    strongSelf.setupRowItemArray()
                    
                    if strongSelf.rowItems.count <= count {
                        
                        tableView.canGetMore = false
                        
                        return
                    }
                    
                    let lastIndex = IndexPath(row: strongSelf.rowItems.count - count - 1,
                                              section: 0)
                    
                    let insertOffsetY = tableView.rectForRow(at: lastIndex).maxY
                    
                    tableView.setContentOffset(CGPoint(x:0,
                                                       y:insertOffsetY + offsetY),
                                               animated: false)
                    
                    tableView.canGetMore = true
                })
        }
        
        /// add text field
        textField = UITextField(frame: .zero)
        textField.attributedPlaceholder = NSAttributedString(string: "快来跟飞哥说两句吧...",
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.whiteGray)
        
        self.view.addSubview(textField)
        
        orderView = nil
        
        textField.snp.makeConstraints { (maker) in
            maker.height.equalTo(45)
            maker.bottom.left.right.equalTo(self.view)
        }
        textField.isHidden = true
        textField.delegate = self
        textField.backgroundColor = UIConfig.generalColor.barColor
        textField.font = UIConfig.generalFont(15)
        textField.textColor = UIConfig.generalColor.selected
        textField.leftView = {
            (Void) -> UIView in
            
            let leftView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            leftView.frame = CGRect(x: 0, y: 0, width: 15, height: 24.5)
            
            return leftView
        }()
        textField.leftViewMode = .always
        textField.rightView = {
            (target:Any) -> UIView in
            
            let imagePickerButton = UIButton(type: .custom)
            
            let image = UIImage(named:"icon_send_pic")
            
            imagePickerButton.setImage(image,
                                       for: .normal)
            imagePickerButton.frame = CGRect(x: 0,
                                             y: 0,
                                             width: image!.size.width + 30,
                                             height: 24.5)
            imagePickerButton.addTarget(target,
                                        action: #selector(ChatRoomVC.pickerImageClick(_:)),
                                        for: .touchUpInside)
            
            return imagePickerButton
        }(self)
        textField.rightViewMode = .always
        textField.enablesReturnKeyAutomatically = true
        textField.returnKeyType = .send
        
        /// request
        
        let targetId = self.conversation.avconversation.userId
        
        API
            .VendorOrder
            .getOrder(to: targetId)
            .responseModel({ (collection:API.VendorOrderCollection) in
                
                let list = collection.orderList
                
                ez.runThisInMainThread {
                    self.orderView = VendorOrdersView(flymanId: targetId,
                                                      items: list)
                    
                    if let _orderView = self.orderView {
                        _orderView.delegate = self
                        _orderView.top = 64
                        _orderView.w = UIScreen.main.bounds.w
                        _orderView.maxHeight = self.view.h - 207
                        _orderView.sizeToFit()
                        
                        self.view.addSubview(_orderView)
                    }
                    
                    self.tableView.reloadData()
                }
            }) { (error) in
        }
        
//        tableView.onNext(.loading)
        
        let callBack = {
            [weak self]
            (result:ChatManager.Conversation.MessageQueryResult) -> Void in
            
            guard let strongSelf = self else {
                return
            }
            
            switch result {
            case .failed(_):
                break
//                strongSelf.tableView.onNext(.error(message:error.localizedDescription))
            case .success(let messages):
//                strongSelf.tableView.onNext(.normal)
                
                strongSelf.setupRowItemArray()
                
                /// scroll to bottom
                
                strongSelf.tableView.canGetMore = messages.count > 0
                
                let allNumber = strongSelf.tableView(strongSelf.tableView,
                                               numberOfRowsInSection: 0)
                
                if allNumber > 0 {
                    let indexPath = IndexPath(row: allNumber - 1,
                                              section: 0)
                    
                    strongSelf.tableView.scrollToRow(at: indexPath,
                                                     at: .bottom,
                                                     animated: false)
                }
                
                strongSelf.textField.isHidden = false
            }
        }
        
        conversation.beginQueryMessage(callback: callBack)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        
        #if DEBUG
                print("chatRoomVC killed")
        #endif
        
        ChatManager.currentSession?.closeConversation(conversation)
    }
    
    var rowItems:[RowItem] = []
    
    var orderView:VendorOrdersView? = nil
}

extension ChatRoomVC: VendorOrdersHeaderViewDelegate {
    func headerView(_ view: VendorOrdersView, slideChange isDown: Bool) {
    }
    
    func headerView(_ view: VendorOrdersView, showFlygoInfoFrom orders: [String]) {
        let infoVC = FlygoInfoVC(userId: self.conversation.avconversation.userId,
                                 nickName: self.conversation.avconversation.nickName,
                                 avatar: self.conversation.avconversation.avatarPath,
                                 orders: orders)
        
        
        
        self.navigationController?.pushViewController(infoVC,
                                                      animated: true)
    }
    
    func headerViewShowFlygoInfo(_ view: VendorOrdersView) {
    }
    
    func headerView(_ view: VendorOrdersView, didOrderClick order: VendorOrderItem) {
        if order.userId == UserManager.shareInstance.currentId {
            let vc = OrderDetailVC(orderId: order.orderformId)
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        } else {
            let vc = VendorOrderDetailVC(vendorOrderInfoId: order.vendorOrderInfoId)
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        }
    }
}

extension ChatRoomVC {
    enum RowItem {
        case title(String)
        case message(AVIMMessage)
        
        var message:AVIMMessage? {
            switch self {
            case .title(_):
                return nil
            case .message(let message):
                return message
            }
        }
    }
    
    func setupRowItemArray() {
        let messages = conversation.messages
        
        var currentTime:AVIMMessage.Time?
        
        var rowItems:[RowItem] = []
        
        for one in messages {
            if let time = currentTime, time.isCloseTo(other: one.time) {
                rowItems.append(.message(one))
                
                currentTime = one.time
                
                continue
            }
            
            currentTime = one.time
            
            rowItems.append(.title(currentTime!.title))
            rowItems.append(.message(one))
        }
        
        self.rowItems = rowItems
        
        self.tableView.reloadData()
    }
}

extension ChatRoomVC {
    class ChatTableView: UITableView {
        
        var canGetMore:Bool = false
        
        var requestMoreHandler:((Void) -> Void)?
        
        func scrollAtPosition(_ pointY:CGFloat) {
            
            if pointY <= -(self.contentInset.top) && canGetMore {
                canGetMore = false
                
                requestMoreHandler?()
            }
        }
    }
}

extension ChatRoomVC {
    func pickerImageClick(_ sender:Any?) {
        ImagePicker.picker(show: self) { (image, _, _) in
            
            let message = self.conversation.sendImage(image: image)
            
            let indexPath = self.add(message: message)

            
            self.tableView.scrollToRow(at: indexPath,
                                       at: .bottom,
                                       animated: true)
//            ez.runThisAfterDelay(seconds: 0.1,
//                                 after: {
//            })
            
        }
    }
}

extension ChatRoomVC {
    
    @discardableResult
    func add(message:AVIMMessage) -> IndexPath {
        tableView.beginUpdates()
        
        var indexPaths = [IndexPath]()
        
        if let lastMessage = rowItems.filter({ $0.message != nil }).map({ $0.message! }).last,
            message.isCloseTo(other: lastMessage) {
            
            rowItems.append(.message(message))
            
            indexPaths.append(IndexPath(row: rowItems.count - 1,
                                        section: 0))
        } else {
            rowItems.append(.title(message.time.title))
            
            indexPaths.append(IndexPath(row: rowItems.count - 1,
                                        section: 0))
            
            rowItems.append(.message(message))
            
            indexPaths.append(IndexPath(row: rowItems.count - 1,
                                        section: 0))
        }
        
        tableView.insertRows(at: indexPaths,
                             with: .automatic)
        
        tableView.endUpdates()
        
        return indexPaths.last!
    }
}

extension ChatRoomVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        orderView?.isUserInteractionEnabled = false
        
        if let ov = orderView,
            ov.isDown {
            orderView?.switchClick(textField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        orderView?.isUserInteractionEnabled = true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            
            if (textField.text?.length ?? 0) > 0 {
                let message = conversation.sendText(text: textField.text ?? "")
                
                let indexPath = add(message: message)
                
                self.tableView.scrollToRow(at: indexPath,
                                           at: .bottom,
                                           animated: true)
                
                textField.text = ""
            }
            
            return false
        }
        
        return true
    }
    
}

extension ChatRoomVC {
    func addUpdatedCoversationNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatRoomVC.updateConversation(notification:)),
                                               name: Notification.Name.ChatManagerUpdateConversationNotification,
                                               object: nil)
    }
    
    func updateConversation(notification:Notification) {
        self.title = self.conversation.avconversation.nickName
        
        MessageTableViewCell.targetAvatarURL = self.conversation.avconversation.avatarPath
        
        self.tableView.reloadData()
    }
}

extension ChatRoomVC {
    func addKeyboardNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatRoomVC.keyboardWillChangeFrame(notification:)),
                                               name: Notification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
    }
    
    func keyboardWillChangeFrame(notification:Notification) {
        guard let endFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect,
            let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let option = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? UInt else {
                return
        }
        
        let keyboardFrame = CGRect(x: endFrame.minX,
                                   y: endFrame.minY - 40,
                                   width: endFrame.width,
                                   height: 40)
        
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: UIViewAnimationOptions(rawValue:option),
                       animations: {
                        self.textField.frame = keyboardFrame
                        
        }) { (success) in
            
        }
    }
}

extension ChatRoomVC {
    class TitleTableViewCell: UITableViewCell {
        let titleLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            titleLabel = ViewFactory.label(font: UIConfig.generalFont(11),
                                           textColor: UIConfig.generalColor.labelGray,
                                           backgroudColor: UIConfig.generalColor.backgroudWhite)
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.backgroundColor = UIConfig.generalColor.backgroudWhite
            self.contentView.addSubview(titleLabel)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            titleLabel.centerXInSuperView()
        }
        
        func set(title:String) {
            titleLabel.text = title
            titleLabel.sizeToFit()
            titleLabel.centerXInSuperView()
        }
    }
}

extension ChatRoomVC: UITableViewDelegate, UITableViewDataSource {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tableView.scrollAtPosition(scrollView.contentOffset.y)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch rowItems[indexPath.row] {
        case .title(_):
            return 35
        case .message(let message):
            if message.contentType.isImage {
                return MessageTableViewCell.heightFromImage(width: message.contentType.imageSize.width,
                                                            height: message.contentType.imageSize.height,
                                                            maxWidth: tableView.w - 170) + 25
            }
            
            return MessageTableViewCell.height(from: message.contentType.text ?? "",
                                        and: tableView.w - 170) + 25
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch rowItems[indexPath.row] {
        case .title(let title):
            let cell:TitleTableViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.set(title: title)
            
            return cell
        case .message(let message):
            
            let cell:MessageTableViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            cell.message = message
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = orderView {
            return 179
        }
        
        return 25
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.backgroundView = ViewFactory.view(color: UIColor.clear)
        }
    }
}

extension ChatRoomVC {
    typealias VendorOrderItem = API.VendorOrder
}

extension ChatRoomVC {
    
    class MessageTableViewCell: UITableViewCell {
        
        let avatar:UIImageView
        
        let chartView:ChatPopView
        
        var contentLabel:UILabel
        
        var statusView:UIView?
        
        let localTimeLabel:UILabel
        
        let chatImageView:ImageView
        
        let disposeBag = DisposeBag()
        
        static var targetAvatarURL:URL? = nil
        
        static let rate:CGFloat = {
            let width = UIScreen.main.bounds.width
            
            let height = UIScreen.main.bounds.height
            
            return width / height
        }()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            avatar = UIImageView(frame: CGRect(x:0, y:0, width:40, height:40))
        
            chartView = ChatPopView(isMine: true)
            chartView.layer.shadowOffset = CGSize(width: 2, height: 2)
            chartView.layer.shadowColor = UIColor(hexString: "#000")!.cgColor
            chartView.layer.shadowRadius = 2
            chartView.layer.shadowOpacity = 0.04
            
            contentLabel = UILabel()
            
            contentLabel.numberOfLines = 0
            
            localTimeLabel = ViewFactory.label(font: UIConfig.generalFont(11),
                                               textColor: UIConfig.generalColor.whiteGray,
                                               backgroudColor: UIConfig.generalColor.backgroudWhite)
            localTimeLabel.adjustsFontSizeToFitWidth = true
            
            chatImageView = ImageView()
            chatImageView.layer.shadowOffset = CGSize(width: 2, height: 2)
            chatImageView.layer.shadowColor = UIColor(hexString: "#000")!.cgColor
            chatImageView.layer.shadowRadius = 2
            chatImageView.layer.shadowOpacity = 0.04
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.backgroundColor = UIConfig.generalColor.backgroudWhite
            self.contentView.addSubviews(avatar, localTimeLabel, chartView, contentLabel, chatImageView)
        }
        
        override func prepareForReuse() {
            super.prepareForReuse()
            
            message = nil
        }
        
        var message:AVIMMessage? {
            willSet {
                chatImageView.kf.cancelDownloadTask()
            }
            didSet {
                guard let currentMessage = message else {
                    localTimeLabel.isHidden = true
                    
                    return
                }
                
                if currentMessage.contentType.isImage {
                    contentLabel.isHidden = true
                    chartView.isHidden = true
                    
                    chatImageView.isHidden = false
                    chatImageView.isMine = currentMessage.isMine
                    
                    if currentMessage.contentType.imageFile.isDataAvailable {
                        chatImageView.image = currentMessage.contentType.imageFile.getData().flatMap{ UIImage(data: $0) }
                    } else {
                        chatImageView.kf.setImage(with: currentMessage.contentType.imageFile.url.flatMap{ URL(string: $0) })
                    }
                    
                    chatImageView.layer.setValue(currentMessage.contentType.imageSize,
                                                 forKey: "currentImageSize")
                } else {
                    chatImageView.isHidden = true
                    
                    contentLabel.isHidden = false
                    contentLabel.attributedText = MessageTableViewCell.attributedString(with:currentMessage.contentType.text ?? "",
                                                                                        and: currentMessage.isMine)
                    
                    chartView.isHidden = false
                    chartView.isMine = currentMessage.isMine
                }
                
                
                if currentMessage.isMine {
                    localTimeLabel.isHidden = true
                } else {
                    localTimeLabel.isHidden = false
                    
                    localTimeLabel.text = currentMessage.localTimeString
                    localTimeLabel.sizeToFit()
                }
                
                avatar.setAvatar(url: currentMessage.isMine ? UserManager.shareInstance.avatarURL :  MessageTableViewCell.targetAvatarURL)
                
                switch currentMessage.status {
                case .sending:
                    let loadingView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                    
                    loadingView.startAnimating()
                    
                    statusView = loadingView
                    
                    self.contentView.addSubview(statusView!)
                case .failed:
                    statusView = UIImageView(named: "icon_failure_notice")
                    
                    self.contentView.addSubview(statusView!)
                default:
                    statusView = nil
                }
                
                statusView?.centerY = avatar.centerY
                
                if currentMessage.isMine {
                    statusView?.right = chartView.left - 10
                } else {
                    statusView?.left = chartView.right + 10
                }
                
                
                adjustLayout()
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            adjustLayout()
        }
        
        func adjustLayout() {
            guard let currentMessage = message else {
                return
            }
            
            let chatWidth = self.w - 170
            
            if currentMessage.contentType.isImage {
                let imageSize = (chatImageView
                    .layer
                    .value(forKey: "currentImageSize") as? NSValue)
                    .flatMap({ $0.cgSizeValue }) ?? CGSize.zero
                
                chatImageView.size = MessageTableViewCell.sizeFromImage(width: imageSize.width,
                                                                        height: imageSize.height,
                                                                        maxWidth: chatWidth)
                
                statusView?.centerY = avatar.centerY
                
                if currentMessage.isMine {
                    avatar.right = self.w - 15
                    
                    chatImageView.right = self.w - 65
                    
                    statusView?.right = chartView.left - 10
                } else {
                    avatar.left = 15
                    
                    chatImageView.left = 65
                    
                    statusView?.left = chartView.right + 10
                }
            } else {
                let fontSize = MessageTableViewCell.size(from: currentMessage.contentType.text ?? "",
                                                         and: chatWidth)
                
                chartView.size = CGSize(width:fontSize.width + 25,
                                        height:fontSize.height + 20)
                
                contentLabel.size = fontSize
                
                statusView?.centerY = avatar.centerY
                
                if currentMessage.isMine {
                    avatar.right = self.w - 15
                    
                    chartView.right = self.w - 65
                    
                    contentLabel.center = CGPoint(x: chartView.center.x - 2.5,
                                                  y: chartView.center.y)
                    
                    statusView?.right = chartView.left - 10
                } else {
                    avatar.left = 15
                    
                    chartView.left = 65
                    
                    contentLabel.center = CGPoint(x: chartView.center.x + 2.5,
                                                  y: chartView.center.y)
                    
                    statusView?.left = chartView.right + 10
                }
            }
            
            if !currentMessage.isMine {
                localTimeLabel.sizeToFit()
                localTimeLabel.centerX = avatar.centerX
                localTimeLabel.top = avatar.bottom + 1
            }
        }
        
        static func sizeFromImage(width imageWidth:CGFloat,
                                  height imageHeight:CGFloat,
                                  maxWidth chatWidth:CGFloat) -> CGSize {
            let rate = imageWidth / imageHeight
            
            var width = imageWidth
            
            var height = imageHeight
            
            let screenRate = self.rate
            
            var maxWidth = chatWidth
            
            var maxHeight = maxWidth / screenRate
            
            if imageHeight < 40 {
                height = 40
                
                width = height * rate
                
                maxHeight = 40
                
                maxWidth = maxHeight * screenRate
            }
            
            if imageWidth <= maxWidth && imageHeight <= maxHeight {
                return CGSize(width: width, height: height)
            }
            
            if rate > screenRate {
                let newWidth = maxWidth
                
                let newHeight = newWidth / rate
                
                return CGSize(width: newWidth, height: newHeight)
            } else if rate < screenRate {
                let newHeight = maxHeight
                
                let newWidth = newHeight * rate
                
                return CGSize(width: newWidth, height: newHeight)
            }
            
            return CGSize(width: maxWidth, height: maxHeight)
        }
        
        static func size(from string:String,
                         and fontWidth:CGFloat) -> CGSize {
            if fontWidth <= 0 {
                return .zero
            }
            
            let size = attributedString(with: string)
                .boundingRect(with: CGSize(width:fontWidth,
                                           height:CGFloat.greatestFiniteMagnitude),
                              options: .usesLineFragmentOrigin,
                              context: nil).size
            
            return size
        }
        
        static func height(from string:String,
                           and fontWidth:CGFloat) -> CGFloat {
            if fontWidth <= 0 {
                return 0
            }
            
            return self.size(from: string, and: fontWidth).height + 30
        }
        
        static func heightFromImage(width:CGFloat,
                                    height:CGFloat,
                                    maxWidth:CGFloat) -> CGFloat {
            if maxWidth <= 0 {
                return 0
            }
            
            return self.sizeFromImage(width: width,
                                      height: height,
                                      maxWidth: maxWidth).height + 30
        }
        
        static func attributedString(with string:String,
                                     and isMine:Bool = true) -> NSAttributedString {
            return NSAttributedString(string: string,
                                      font: UIConfig.generalFont(14),
                                      textColor: isMine ? UIConfig.generalColor.unselected : UIConfig.generalColor.white,
                                      lineSpace: 3)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ChatRoomVC {
    class ImageView: UIImageView {
        
        let chatMaskLayer:CAShapeLayer = CAShapeLayer()
        
        var isMine:Bool = false
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            chatMaskLayer.frame = self.bounds
            chatMaskLayer.path = ChatPopView.path(from: self.bounds,
                                                  and: isMine)
            
            if self.layer.mask == nil {
                chatMaskLayer.fillColor = UIColor.white.cgColor
                
                self.layer.mask = chatMaskLayer
            }
        }
    }
}

extension ChatRoomVC: ChatManagerConversationDelegate {
    func conversation(_ conversation:ChatManager.Conversation, receiveNewMessage:AVIMMessage) {
        let _ = add(message: receiveNewMessage)
    }
    
    func conversation(_ conversation:ChatManager.Conversation, messageDidDeliverd:AVIMMessage) {
        for indexPath in tableView.indexPathsForVisibleRows ?? [] {
            guard let message = rowItems[indexPath.row].message else {
                continue
            }
            
            if message.messageId == messageDidDeliverd.messageId,
                let cell =  tableView.cellForRow(at: indexPath) as? MessageTableViewCell {
                
                cell.message = messageDidDeliverd
                
                break
            }
        }
    }
    
    func conversation(_ conversation:ChatManager.Conversation, message:AVIMMessage, deliveFailed:NSError) {
        self.showToast(text: "消息发送失败，原因是\(deliveFailed.localizedDescription)")
    }
}

extension ChatRoomVC {
    
    class ChatPopView: UIImageView {
        
        var isMine:Bool
        
        init(isMine:Bool) {
            self.isMine = isMine
            
            super.init(image: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            updatePopImage()
        }
        
        static func path(from rect:CGRect, and isMine:Bool) -> CGMutablePath {
            let path = CGMutablePath()
            
            if isMine {
                path.move(to: CGPoint(x:0, y:5))
                path.addArc(tangent1End: CGPoint(x:0  , y:0),
                            tangent2End: CGPoint(x:rect.width - 10, y:0),
                            radius: 5)
                path.addArc(tangent1End: CGPoint(x:rect.width - 5, y:0),
                            tangent2End: CGPoint(x:rect.width - 5, y:5),
                            radius: 5)
                path.addLine(to: CGPoint(x:rect.width - 5, y:15))
                path.addLine(to: CGPoint(x:rect.width, y:20))
                path.addLine(to: CGPoint(x:rect.width - 5, y:25))
                path.addArc(tangent1End: CGPoint(x:rect.width - 5, y:rect.height),
                            tangent2End: CGPoint(x:5, y:rect.height),
                            radius: 5)
                path.addArc(tangent1End: CGPoint(x:0, y:rect.height),
                            tangent2End: CGPoint(x:0, y:rect.height - 5),
                            radius: 5)
                path.addLine(to: CGPoint(x:0, y:5))
            } else {
                path.move(to: CGPoint(x:0, y:20))
                path.addLine(to: CGPoint(x:5, y:15))
                path.addArc(tangent1End: CGPoint(x:5, y:0),
                            tangent2End: CGPoint(x:rect.width - 5, y:0),
                            radius: 5)
                path.addArc(tangent1End: CGPoint(x:rect.width, y:0),
                            tangent2End: CGPoint(x:rect.width, y:rect.height - 5),
                            radius: 5)
                path.addArc(tangent1End: CGPoint(x:rect.width, y:rect.height),
                            tangent2End: CGPoint(x:10, y:rect.height),
                            radius: 5)
                path.addArc(tangent1End: CGPoint(x:5, y:rect.height),
                            tangent2End: CGPoint(x:5, y:25),
                            radius: 5)
                path.addLine(to: CGPoint(x:5, y:25))
                path.addLine(to: CGPoint(x:0, y:20))
            }
            return path
        }
        
        func updatePopImage() {
            /// get image
            UIGraphicsBeginImageContext(self.bounds.size)
            
            guard let context = UIGraphicsGetCurrentContext() else {
                UIGraphicsEndImageContext()
                
                return
            }
            
            let popRect = CGRect(x: 0, y: 0,
                                 width: max(25, self.bounds.width),
                                 height: max(20, self.bounds.height))
            
            //// gradient draw
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            
            let array:CFArray = isMine ? [UIConfig.generalColor.white.cgColor, UIConfig.generalColor.white.cgColor] as CFArray
: [UIColor(r: 247, g: 107, b: 98).cgColor, UIColor(r: 253, g: 60, b: 83).cgColor] as CFArray
            
            let gradient = CGGradient(colorsSpace: colorSpace,
                                      colors: array,
                                      locations: [0.0, 1.0])
            
            let startPoint = CGPoint(x: self.bounds.width / 2.0, y: 0)
            
            let endPoint = CGPoint(x: self.bounds.width / 2.0, y: self.bounds.height)
            
            context.saveGState()
            
            context.addPath(ChatPopView.path(from: popRect, and: isMine))
            context.clip()
            context.drawLinearGradient(gradient!,
                                       start: startPoint,
                                       end: endPoint,
                                       options: .init(rawValue: 0))
            context.restoreGState()
            
            //// get image
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            
            self.image = image
        }
    }
}

