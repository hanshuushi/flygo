//
//  MessageVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


@objc class MessageVC: UIViewController, PresenterType {
    
    var messageItems:[MessageItem]
    
    let viewModel:MessageVM
    
    let tableView:UITableView
    
    var bindDisposeBag: DisposeBag?
    
    let disposeBag:DisposeBag
    
    init(type:MessageRequestType) {
        disposeBag = DisposeBag()
        
        viewModel = MessageVM(type: type)
        
        messageItems = []
        
        tableView = UITableView(frame: .zero,
                                style: .plain)
//        tableView.backgroundColor = UIConfig.generalColor.backgroudWhite
        tableView.separatorStyle = .none
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 107
        tableView.registerCell(cellClass: SystomMessageTableViewCell.self)
        tableView.registerCell(cellClass: OrderMessageTableViewCell.self)
        tableView.sectionFooterHeight = 10
        
        super.init(nibName: nil, bundle: nil)
        
        bind()
        
        NotificationManager.shareInstance.clearNotification(type: type)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch viewModel.type {
        case .systom:
            self.title = "系统消息"
        case .order:
            self.title = "订单消息"
        case .flygo:
            self.title = "接单消息"
        }
        
        self.view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        
        //addFakeShadow()
    }
}

extension MessageVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return messageItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = messageItems[indexPath.section]
        
        switch item.type {
        case .order(_, _, _, _, let id, let vid):
            switch viewModel.type {
            case .flygo:
                let vc = VendorOrderDetailVC(vendorOrderInfoId: vid)
                
                self.navigationController?.pushViewController(vc,
                                                              animated: true)
            case .order:
                let vc = OrderDetailVC(orderId: id)
                
                self.navigationController?.pushViewController(vc,
                                                              animated: true)
            default:
                return
            }
            
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = messageItems[indexPath.section]
        
        switch item.type {
        case .systom(_):
            let cell = tableView.dequeueReusableCell(at: indexPath) as SystomMessageTableViewCell
            
            cell.set(item: item)
            
            return cell
        case .order(_, _, _, _, _, _):
            let cell = tableView.dequeueReusableCell(at: indexPath) as OrderMessageTableViewCell
            
            cell.set(item: item)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension MessageVC {
    class SystomMessageTableViewCell: UITableViewCell {
        let timeLabel:UILabel
        
        let contentLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            timeLabel = ViewFactory.label(font: UIConfig.arialFont(12),
                                          textColor: UIConfig.generalColor.labelGray)
            
            contentLabel = ViewFactory.label(font: UIConfig.generalFont(13),
                                             textColor: UIConfig.generalColor.unselected,
                                             backgroudColor: UIConfig.generalColor.backgroudWhite)
            contentLabel.numberOfLines = 0
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            let contentZone = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            contentZone.addSubview(contentLabel)
            
            let titleLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(14),
                                               textColor: UIConfig.generalColor.selected)
            
            titleLabel.text = "系统消息"
            
            self.contentView.addSubviews(timeLabel, contentZone, titleLabel)
            
            contentLabel.snp.makeConstraints { (maker) in
                maker.edges.equalTo(contentZone).inset(UIEdgeInsetsMake(10, 10, 10, 10))
            }
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(self.contentView).offset(15)
            }
            
            timeLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(titleLabel)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            contentZone.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(titleLabel.snp.bottom).offset(15)
                maker.bottom.right.equalTo(self.contentView).offset(-15)
            }
        }
        
        func set(item:MessageItem) {
            timeLabel.text = item.time?.generalFormartString ?? ""
            
            switch item.type {
            case .systom(let content):
                contentLabel.text = content
            default:
                return
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension MessageVC {
    class OrderMessageTableViewCell: UITableViewCell {
        
        let ordernumLabel:UILabel
        
        let timeLabel:UILabel
        
        let coverImageView:UIImageView
        
        let titleLabel:UILabel
        
        let messageLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            ordernumLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(14),
                                              textColor: UIConfig.generalColor.selected)
            
            timeLabel = ViewFactory.label(font: UIConfig.arialFont(12),
                                          textColor: UIConfig.generalColor.labelGray)
            
            coverImageView = UIImageView()
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
            coverImageView.layer.borderWidth = 1
            
            titleLabel = UILabel()
            titleLabel.numberOfLines = 0
            titleLabel.backgroundColor = UIConfig.generalColor.backgroudWhite
            
            messageLabel = ViewFactory.label(font: UIConfig.generalFont(11),
                                             textColor: UIConfig.generalColor.labelGray,
                                             backgroudColor: UIConfig.generalColor.backgroudWhite)
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            let contentZone = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            contentZone.addSubviews(coverImageView, titleLabel, messageLabel)
            
            coverImageView.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(contentZone).offset(10)
                maker.size.equalTo(CGSize(width:75, height:65))
            }
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(coverImageView)
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.right.equalTo(contentZone).offset(-10)
                maker.bottom.lessThanOrEqualTo(contentZone).offset(-10)
            }
            
            messageLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel.snp.bottom).offset(1)
                maker.left.equalTo(coverImageView.snp.right).offset(10)
                maker.right.equalTo(contentZone).offset(-10)
                maker.bottom.lessThanOrEqualTo(contentZone).offset(-10)
            }
            
            self.contentView.addSubviews(ordernumLabel, timeLabel, contentZone)
            
            ordernumLabel.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(self.contentView).offset(15)
            }
            
            timeLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(ordernumLabel)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            contentZone.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(ordernumLabel.snp.bottom).offset(15)
                maker.bottom.right.equalTo(self.contentView).offset(-15)
                maker.height.greaterThanOrEqualTo(85)
            }
        }
        
        func set(item:MessageItem) {
            timeLabel.text = item.time?.generalFormartString ?? ""
            
            switch item.type {
            case .order(let orderNumber, let cover, let title, let message, _, _):
                ordernumLabel.text = "订单编号:\(orderNumber)"
                
                coverImageView.kf.setImage(with: cover)
                
                messageLabel.text = message
                
                titleLabel.attributedText = NSAttributedString(string: title,
                                                               font: UIConfig.generalFont(13),
                                                               textColor: UIConfig.generalColor.unselected,
                                                               lineSpace: 0)
                titleLabel.lineBreakMode = .byTruncatingTail
            default:
                return
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
