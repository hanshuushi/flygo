//
//  MessageVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/26.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum MessageItemType {
    case systom(content:String)
    case order(orderNumber:String, cover:URL?, title:String, message:String?, id:String, vid:String)
}

struct MessageItem {
    var time:Date?
    
    var type:MessageItemType
    
    init(model:API.Message) {
        
        print(model)
        
        time = model.sendTime
        
        switch model.messageType {
        case .systom:
            type = .systom(content: model.content)
        default:
            type = .order(orderNumber: model.orderSn,
                          cover: model.pic,
                          title: model.content,
                          message: model.productName,
                          id:model.orderformId,
                          vid: model.vendorOrderInfoId)
        }
    }
}

enum MessageRequestType: Int {
    case systom = 1
    
    case order = 2
    
    case flygo = 3
    
    var apiType:API.MessageType {
        return API.MessageType(rawValue: self.rawValue)!
    }
    
    var key:String {
        switch self {
        case .systom:
            return "systom"
        case .order:
            return "order"
        case .flygo:
            return "flygo"
        }
    }
}


class MessageVM: ViewModel {
    
    let type:MessageRequestType
    
    init(type:MessageRequestType) {
        self.type = type
    }
    
    typealias Collection = API.MessageCollection
    
    func bind(to view: MessageVC) -> DisposeBag? {
        
        let type = self.type
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .gray)
        
        dataSet.emptyDescriptionHandler = { "您还没有新的消息" }
        
        let request = {
            (page:Int) -> Observable<APIItem<Collection>> in
            
            return Collection.getList(from: type.apiType,
                                      page: page)
        }
        
        let bag = dataSet.refrence(getRequestWithPage: request,
                         listCallBack: {
                           [weak view] (items) in
                            view?.messageItems = items.map({ MessageItem(model:$0) })
        })
        
        return bag
    }
    
}
