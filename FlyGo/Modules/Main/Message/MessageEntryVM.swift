//
//  MessageEntryVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/15.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MessageEntryVM: ViewModel {
    
    let records:Driver<[AVIMConversation]>
    
    let requestRecordEvent:PublishSubject<Void>
    
    init() {
        requestRecordEvent = PublishSubject()
        
        records = requestRecordEvent
            .map({
                (ChatManager.currentSession?.findAllConversation() ?? [])
            })
            .asDriver(onErrorJustReturn: [])
        
        NotificationCenter
            .default
            .addObserver(self,
                         selector: #selector(MessageEntryVM.chatManagerReceiveMessageNotification(_:)),
                         name: .ChatManagerReceiveMessageNotification,
                         object: nil)
        
        NotificationCenter
            .default
            .addObserver(self,
                         selector: #selector(MessageEntryVM.chatManagerUnreadCountChangeNotification(_:)),
                         name: .ChatManagerUnreadCountChangeNotification,
                         object: nil)
        
        NotificationCenter
            .default
            .addObserver(self,
                         selector: #selector(MessageEntryVM.chatManagerUpdateConversationNotification(_:)),
                         name: .ChatManagerUpdateConversationNotification,
                         object: nil)
    }
    
    @objc func chatManagerReceiveMessageNotification(_ notification:Notification) {
        requestRecordEvent.onNext()
    }
    
    @objc func chatManagerUnreadCountChangeNotification(_ notification:Notification) {
        requestRecordEvent.onNext()
    }
    
    @objc func chatManagerUpdateConversationNotification(_ notification:Notification) {
        requestRecordEvent.onNext()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: .ChatManagerReceiveMessageNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: .ChatManagerUnreadCountChangeNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: .ChatManagerUpdateConversationNotification,
                                                  object: nil)
    }
    
    func bind(to view: MessageEntryVC) -> DisposeBag? {
        let disposeBag = DisposeBag()
        
        records.drive(onNext: {
            [weak view] (array) in
            view?.chatRecords = array
            view?.tableView.reloadData()
        },
                      onCompleted: nil,
                      onDisposed: nil).addDisposableTo(disposeBag)
        
        NotificationManager
            .shareInstance
            .systomUnread
            .drive(onNext: {
                [weak view] (bage) in
                view?.infoCells[0].label.bage = bage
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        NotificationManager
            .shareInstance
            .orderUnread
            .drive(onNext: {
                [weak view] (bage) in
                view?.infoCells[1].label.bage = bage
                },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        NotificationManager
            .shareInstance
            .flygoUnread
            .drive(onNext: {
                [weak view] (bage) in
                view?.infoCells[2].label.bage = bage
                },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        requestRecordEvent.onNext()
        
        return disposeBag
    }
    
}
