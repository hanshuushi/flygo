//
//  VendorOrdersHeaderView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/21.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation


protocol VendorOrdersHeaderViewDelegate: NSObjectProtocol {
    func headerView(_ view:VendorOrdersView, slideChange isDown:Bool)
    func headerView(_ view:VendorOrdersView, showFlygoInfoFrom orders:[String])
    func headerView(_ view:VendorOrdersView, didOrderClick order:API.VendorOrder)
}

class VendorOrdersView: UIView {
    
    typealias VendorOrderItem = API.VendorOrder
    
    let items:[VendorOrderItem]
    
    var isDown:Bool
    
    weak var delegate:VendorOrdersHeaderViewDelegate?
    
    var maxHeight:CGFloat?
    
    var flygoItems:[VendorOrderItem]
    
    init?(flymanId:String,
          items:[VendorOrderItem]) {
        
        self.items = items
        
        self.tableView = UITableView(frame: .zero,
                                     style: .plain)
        
        if items.count < 1 {
            return nil
        }
        
        headerView = UITableViewHeaderFooterView(reuseIdentifier: "VendorOrdersHeaderViewTitle")
        
        footerView = UIView()
        
        isDown = false
        
        flygoItems = items.filter({ $0.customerId == flymanId })
        
        super.init(frame: .zero)
        
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        
        /// table view
        self.addSubview(tableView)
        
        tableView.backgroundColor = UIConfig.generalColor.barColor
        tableView.separatorStyle = .none
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 60, 0))
        }
        tableView.registerCell(cellClass: VendorTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        tableView.backgroundView = ViewFactory.view(color: .clear)
        tableView.isScrollEnabled = false
        
        /// header
        let leftLabel = ViewFactory.label(font: UIConfig.generalFont(14),
                                          textColor: UIConfig.generalColor.unselected,
                                          backgroudColor:.clear)
        
        headerView.contentView.addSubview(leftLabel)
        headerView.contentView.backgroundColor = .clear
        headerView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.barColor)
        headerView.backgroundView?.alpha = 0
        
        leftLabel.text = "相关订单"
        leftLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(headerView.contentView).offset(15)
            maker.bottom.equalTo(headerView.contentView)
        }
        
        if flygoItems.count > 0 {
            let rightButton = UIButton(type: .custom)
            
            rightButton.setAttributedTitle(NSAttributedString(string: "查看飞哥信息>>",
                                                              font: UIConfig.generalFont(12),
                                                              textColor: UIColor(hexString: "#3977cb")!),
                                           for: .normal)
            rightButton.titleEdgeInsets = UIEdgeInsetsMake(5, 0, -5, 0)
            rightButton.addTarget(self,
                                  action: #selector(VendorOrdersView.showDetailPressed(sender:)),
                                  for: UIControlEvents.touchUpInside)
            
            headerView.contentView.addSubview(rightButton)
            
            rightButton.snp.makeConstraints { (maker) in
                maker.right.equalTo(headerView.contentView).offset(-15)
                maker.bottom.equalTo(headerView.contentView)
            }
        }
        
        /// bottom
        let bottomButton = UIButton(type: .custom)
        
        bottomButton.addTarget(self,
                              action: #selector(VendorOrdersView.switchClick(_:)),
                              for: UIControlEvents.touchUpInside)
        bottomButton.setImage(UIImage(named:"icon_pulldown_more"),
                              for: .normal)
        
        self.addSubview(bottomButton)
        
        bottomButton.adjustsImageWhenHighlighted = false
        bottomButton.backgroundColor = .clear
        bottomButton.alpha = 0.95
        bottomButton.snp.makeConstraints { (maker) in
            maker.left.right.bottom.equalTo(self)
            maker.height.equalTo(60)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let tableView:UITableView
    
    let headerView:UITableViewHeaderFooterView
    
    let footerView:UIView
}

extension VendorOrdersView {
    func showDetailPressed(sender:Any?) {
        delegate?.headerView(self,
                             showFlygoInfoFrom: flygoItems.map({ $0.orderformId }))
    }
}

extension VendorOrdersView {
    func switchClick(_ sender:Any?) {
        if items.count < 2 {
            UIApplication.shared.keyWindow?.rootViewController?.showToast(text: "暂无更多订单")
            
            return
        }
        
        isDown = !isDown
        
        delegate?.headerView(self, slideChange: isDown)
        
        tableView.isScrollEnabled = isDown
        
        UIView.animate(withDuration: 0.25,
                       animations: {
                        self.sizeToFit()
                        
                        if !self.isDown {
                            self.tableView.contentOffset = .zero
                        }
        }) { (_) in
            
        }
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        
        if !isDown {
            self.h = 179
            
            return
        }
        
        if let max = maxHeight {
            self.h = min(max, tableView.contentSize.height + 60)
        } else {
            self.h = tableView.contentSize.height + 60
        }
    }
}

extension VendorOrdersView {
    class VendorTableViewCell: UITableViewCell {
        
        let coverView:UIImageView
        
        let titleLabel:UILabel
        
        let subTitleLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            coverView = UIImageView()
            
            coverView.backgroundColor = UIConfig.generalColor.white
            coverView.contentMode = .scaleAspectFit
            coverView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
            coverView.layer.borderWidth = 1
            coverView.layer.masksToBounds = true
            
            titleLabel = UILabel()
            
            titleLabel.backgroundColor = UIConfig.generalColor.backgroudWhite
            titleLabel.numberOfLines = 2
            
            subTitleLabel = ViewFactory.label(font: UIConfig.generalFont(11),
                                              textColor: UIConfig.generalColor.labelGray,
                                              backgroudColor: titleLabel.backgroundColor)
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            let blockView = ViewFactory.view(color: titleLabel.backgroundColor!)
            
            self.selectionStyle = .none
            self.backgroundColor = .clear
            self.backgroundView = ViewFactory.view(color: .clear)
            self.contentView.backgroundColor = .clear
            self.contentView.addSubviews(blockView, coverView, titleLabel, subTitleLabel)
            
            blockView.snp.makeConstraints { (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(10, 15, 0, 15))
            }
            
            coverView.snp.makeConstraints({ (maker) in
                maker.left.top.equalTo(blockView).offset(10)
                maker.bottom.equalTo(blockView).offset(-10)
                maker.width.equalTo(75)
            })
            
            titleLabel.snp.makeConstraints({ (maker) in
                maker.left.equalTo(coverView.snp.right).offset(10)
                maker.right.equalTo(blockView).offset(-10)
                maker.top.equalTo(coverView)
            })
            
            subTitleLabel.snp.makeConstraints({ (maker) in
                maker.left.right.equalTo(titleLabel)
                maker.top.equalTo(titleLabel.snp.bottom).offset(1)
            })
        }
        
        func set(item:VendorOrderItem) {
            coverView.kf.setImage(with: item.pic)
            
            titleLabel.attributedText = NSAttributedString(string: item.productName,
                                                           font: UIConfig.generalFont(13),
                                                           textColor: UIConfig.generalColor.unselected,
                                                           lineSpace: 0)
            
            subTitleLabel.text = item.productRecStandard
        }
        
        override func prepareForReuse() {
            super.prepareForReuse()
            
            coverView.kf.cancelDownloadTask()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension VendorOrdersView: UITableViewDelegate, UITableViewDataSource {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        
        let alpha = (y / 34)
        
        headerView.backgroundView?.alpha = min(1.0, max(0, alpha))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:VendorTableViewCell = tableView.dequeueReusableCell(at: indexPath)
        
        cell.set(item: items[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.headerView(self, didOrderClick: items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        if let headerView = view as? UITableViewHeaderFooterView {
//            headerView.backgroundView = ViewFactory.view(color: .clear)
//        }
    }
}

