//
//  CommentVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/6/1.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


struct CommentItem {
    
    var avatar:URL?
    
    var nickName:String
    
    var star:Int
    
    var time:String
    
    var standar:String
    
    var price:Currency
    
    var content:String
    
    var images:[URL]
    
    init(model:API.Comment) {
        avatar                 = model.userAvatar
        nickName               = model.nickname
        star                   = model.buyScore
        time                   = model.registerTime?.generalFormartString ?? ""
        standar                = model.productRecStandard
        price                  = Currency(unit: "￥",
                                          value: model.buyPrice)
        content                = model.commentsDesc
        images                 = model
            .commentsPic
            .filter({ $0.pic != nil })
            .sorted(by: { (left, right) -> Bool in
                return left.picSeq > right.picSeq
            })
            .map({ $0.pic! })
    }
}

class CommentVM: ViewModel {
    
    let productId:String
    
    init(productId:String) {
        self.productId = productId
    }
    
    func bind(to view:CommentVC) -> DisposeBag? {
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view, finishStyle: .gray)
        
        dataSet.emptyDescriptionHandler = { "此宝贝暂时没有评价..." }
        dataSet.emptyImageHandler = { UIImage(named:"icon_no_reviews")! }
        
        let productId = self.productId
        
        let request = {
            (page:Int) -> Observable<APIItem<API.CommentCollection>> in
            
            return API.CommentCollection.getList(of: productId, page: page)
        }
        
        let bag = dataSet.refrence(getRequestWithPage: request,
                                   listCallBack: {
                                    [weak view] (items) in
                                    view?.items = items.map({ CommentItem(model:$0) })
        })
        
        return bag
    }
    
}
