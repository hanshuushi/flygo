//
//  CommentVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/6/1.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SnapKit
import ImageViewer
import Kingfisher

class CommentVC: UIViewController, PresenterType {
    
    let viewModel:CommentVM
    
    init(productId:String) {
        viewModel = CommentVM(productId: productId)
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var bindDisposeBag: DisposeBag?
    
    let tableView = UITableView(frame: .zero,
                                style: .plain)
    
    var items:[CommentItem] = []
}


// MARK: - TableView Delegate
extension CommentVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CommentTableViewCell = tableView.dequeueReusableCell(at: indexPath)
        
        cell.set(item: items[indexPath.row])
        cell.viewController = self
        
        return cell
    }
    
}

// MARK: - ViewController Override Refer
extension CommentVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "宝贝评价"
        
        self.view.addSubview(tableView)
        
        tableView.snp.fillToSuperView()
        
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        tableView.separatorColor = UIConfig.generalColor.lineColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 450
        tableView.registerCell(cellClass: CommentTableViewCell.self)
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        
        bind()
    }
}


extension CommentVC {
    class CommentTableViewCell: UITableViewCell, GalleryDisplacedViewsDataSource, GalleryItemsDataSource {
        
        let stackView = PicsView()
        
        let avatarImaegView:UIImageView
        
        let nickNameLabel:UILabel
        
        let dateLabel:UILabel
        
        let starView:UIView
        
        let contentLabel:UILabel
        
        let subTitleLabel:UILabel
        
        var constraint:Constraint!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            avatarImaegView = UIImageView()
            
            nickNameLabel = ViewFactory.generalLabel()
            nickNameLabel.text = "昵称"
            
            dateLabel = ViewFactory.generalLabel(generalSize: 12,
                                                 textColor: UIConfig.generalColor.labelGray)
            dateLabel.text = "日期"
            
            starView = ViewFactory.view(color: UIConfig.generalColor.white)
            
            let starImage = UIImage(named: "icon_score")!
            
            for i in 0..<5 {
                let starImageView = UIImageView(image: starImage)
                
                let x = (starImage.size.width + 3) * CGFloat(i)
                
                starImageView.frame = CGRect(x: x,
                                             y: 0,
                                             width: starImage.size.width,
                                             height: starImage.size.height)
                
                starView.insertSubview(starImageView, at: 0)
            }
            
            contentLabel = ViewFactory.generalLabel()
            
            contentLabel.numberOfLines = 0
            contentLabel.text = "评价"
            
            subTitleLabel = ViewFactory.generalLabel(generalSize: 12,
                                                     textColor: UIConfig.generalColor.unselected)
            subTitleLabel.numberOfLines = 0
            
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubviews(avatarImaegView, nickNameLabel, dateLabel, starView, contentLabel, stackView, subTitleLabel)
            
            avatarImaegView.snp.makeConstraints { (maker) in
                maker.width.height.equalTo(30)
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(30)
            }
            
            nickNameLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView.snp.right).offset(10)
                maker.top.equalTo(avatarImaegView).offset(-3)
                maker.right.equalTo(starView.snp.left).offset(-5)
            }
            
            dateLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView.snp.right).offset(10)
                maker.bottom.equalTo(avatarImaegView).offset(3)
                maker.right.equalTo(starView.snp.left).offset(-5)
            }
            
            starView.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(nickNameLabel)
                maker.width.equalTo(starImage.size.width * 5 + 12)
                maker.height.equalTo(starImage.size.height)
            }
            
            contentLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView)
                maker.top.equalTo(avatarImaegView.snp.bottom).offset(10)
                maker.right.equalTo(starView)
            }
            
            stackView.imageTap = {
                [weak self]
                (index) in
                
                self?.imagePreview(of: index)
                
            }
            stackView.snp.makeConstraints { (maker) in
                maker.left.equalTo(avatarImaegView)
                maker.top.equalTo(contentLabel.snp.bottom).offset(15)
                maker.right.equalTo(starView)
            }
            
            subTitleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(contentLabel)
                maker.right.equalTo(contentLabel)
                maker.top.equalTo(stackView.snp.bottom).offset(12).priority(998)
                constraint = maker.top.equalTo(contentLabel.snp.bottom).offset(12).priority(999).constraint
                maker.bottom.equalTo(self.contentView).offset(-28)
            }
        }
        
        var picUrls:[URL] = []
        
        func set(item:CommentItem) {
            avatarImaegView.setAvatar(url: item.avatar)
            
            nickNameLabel.text = item.nickName
            
            dateLabel.text = item.time
            
            for (index, starImageView) in starView.subviews.enumerated() {
                starImageView.isHidden = (index + 1) > item.star
            }
            
            contentLabel.attributedText = NSAttributedString(string: item.content,
                                                             font: UIConfig.generalFont(15),
                                                             textColor: UIConfig.generalColor.selected,
                                                             lineSpace: 2)
            
            subTitleLabel.text = "购入规格：\(item.standar) 入手价格：\(item.price.description)"
            
            picUrls = item.images
            
            stackView.set(imageUrls: picUrls, width: UIScreen.main.bounds.width - 30)
            
            if picUrls.count > 0 {
                constraint.deactivate()
            } else {
                constraint.activate()
            }
        }
        
        func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
            return (stackView.subviews[index] as? UIImageView).flatMap({ $0 })
        }
        
        func itemCount() -> Int {
            return picUrls.count
        }
        
        func provideGalleryItem(_ index: Int) -> GalleryItem {
            let url = picUrls[index]
            
            return .image(fetchImageBlock: { (fetchImageBlock) in
                KingfisherManager.shared.retrieveImage(with: url,
                                                       options: nil,
                                                       progressBlock: nil,
                                                       completionHandler: { (image, _, _, _) in
                                                        fetchImageBlock(image)
                })
                
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        weak var viewController:UIViewController?
        
        func imagePreview(of index:UInt) {
            guard let viewController = self.viewController else {
                return
            }
            
            let galleryVC = GalleryViewController(startIndex: Int(index),
                                                  itemsDataSource: self,
                                                  displacedViewsDataSource: self,
                                                  configuration: ProductDetailVC.galleryConfiguration())
            
            galleryVC.headerView = nil
            
            viewController.present(galleryVC,
                                   animated: false,
                                   completion: nil)
        }
    }
}


extension CommentVC {
    class PicsView: UIView {
        
        var contentSize:CGSize = .zero
        
        var imageTap:((UInt) -> Void)?
        
        func tapGesture(_ tapGesture:UITapGestureRecognizer) {
            guard let view = tapGesture.view, let index = self.subviews.index(of: view) else {
                return
            }
            
            imageTap?(index.toUInt)
        }
        
        override var intrinsicContentSize: CGSize {
            return contentSize
        }
        
        func set(imageUrls:[URL], width:CGFloat) {
            self.removeSubviews()
            
            let imageViews = imageUrls.map { (url) -> UIImageView in
                let imageView = UIImageView()
                
                imageView.backgroundColor = UIConfig.generalColor.backgroudWhite
                imageView.layer.cornerRadius = 5
                imageView.layer.masksToBounds = true
                imageView.kf.setImage(with: url)
                imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                      action: #selector(PicsView.tapGesture(_:))))
                
                self.addSubview(imageView)
                
                return imageView
            }
            
            if imageUrls.count <= 0 {
                contentSize = .zero
            } else if imageUrls.count == 1 {
                let imageView = imageViews.first!
                
                let boardLength = (width - 15) / 2.0
                
                imageView.frame = CGRect(x: 0, y: 0, width: boardLength, height: boardLength)
                
                contentSize = CGSize(width: width, height: boardLength + 15)
            } else if imageUrls.count == 2 || imageUrls.count == 4 {
                let boardLength = (width - 15) / 2.0
                
                for (index, imageView) in imageViews.enumerated() {
                    let x:CGFloat = index % 2 == 0 ? 0 : 15 + boardLength
                    
                    let y:CGFloat = CGFloat(Int(index / 2)) * (boardLength + 15)
                    
                    imageView.frame = CGRect(x: x, y: y, width: boardLength, height: boardLength)
                }
                
                contentSize = CGSize(width: width, height: imageViews.last!.frame.maxY)
            } else {
                let boardLength = (width - 30) / 3.0
                
                for (index, imageView) in imageViews.enumerated() {
                    let x:CGFloat = CGFloat(index % 3) * (boardLength + 15)
                    
                    let y:CGFloat = CGFloat(Int(index / 3)) * (boardLength + 15)
                    
                    imageView.frame = CGRect(x: x, y: y, width: boardLength, height: boardLength)
                }
                
                contentSize = CGSize(width: width, height: imageViews.last!.frame.maxY)
            }
            
            invalidateIntrinsicContentSize()
        }
    }
}
