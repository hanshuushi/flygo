//
//  CommentSubmitVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/26.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SnapKit
import EZSwiftExtensions
import ImageViewer

class CommentSubmitVC: UITableViewController, PresenterType {
    
    static var orderFormidCache:String = ""
    
    let viewModel:CommentSubmitVM
    
    var bindDisposeBag: DisposeBag?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        viewModel = CommentSubmitVM(orderFormid: CommentSubmitVC.orderFormidCache)
        
        super.init(nibName: nil,
                   bundle: nibBundleOrNil)
    }
    
    init(orderFormid:String) {
        CommentSubmitVC.orderFormidCache = orderFormid
        
        viewModel = CommentSubmitVM(orderFormid: CommentSubmitVC.orderFormidCache)
        
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var cells:[UITableViewCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "评价"
        
//        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.sectionHeaderHeight = CGFloat.leastNormalMagnitude
        self.tableView.sectionFooterHeight = 10
        self.tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        self.tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        
        bind()
    }
    
    var uploadedPaths:[String] = []
    
    var uploadFinishCallBack:(() -> Void)?
}

extension CommentSubmitVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return cells.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cells[indexPath.section]
        
        if let commentCell = cell as? ProductCommentCell {
            commentCell.viewController = self
            
            if commentCell.submitBtn.allTargets.count == 0 {
                commentCell.submitBtn.addTarget(self,
                                                action: #selector(CommentSubmitVC.uploadImagePressed(sender:)),
                                                for: UIControlEvents.touchUpInside)
            }
        } else if let flymanCell = cell as? FlymanCommentCell {
            if flymanCell.submitBtn.allTargets.count == 0 {
                flymanCell.submitBtn.addTarget(self,
                                               action: #selector(CommentSubmitVC.uploadImagePressed(sender:)),
                                               for: UIControlEvents.touchUpInside)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 388
    }
}

extension CommentSubmitVC {
    class TagsView: UIView {
        
        fileprivate let bag = DisposeBag()
        
        fileprivate let tags:[UIButton]
        
        let tagsVariable:Variable<[Int]> = Variable([])
        
        init(tags:[String]) {
            self.tags = tags.mapEnumerated({ (index, tag) -> UIButton in
                
                let button = UIButton(type: .custom)
                
                button.setAttributedTitle(NSAttributedString.init(string: tag,
                                                                  font: UIConfig.generalFont(11),
                                                                  textColor: UIConfig.generalColor.selected),
                                          for: .normal)
                button.setBackgroundColor(UIConfig.generalColor.backgroudGray,
                                          forState: .normal)
                button.setAttributedTitle(NSAttributedString.init(string: tag,
                                                                  font: UIConfig.generalFont(11),
                                                                  textColor: UIConfig.generalColor.white),
                                          for: .selected)
                button.setBackgroundColor(UIConfig.generalColor.red,
                                          forState: .selected)
                
                button.setAttributedTitle(NSAttributedString.init(string: tag,
                                                                  font: UIConfig.generalFont(11),
                                                                  textColor: UIConfig.generalColor.white),
                                          for: .highlighted)
                button.setBackgroundColor(UIConfig.generalColor.red,
                                          forState: .highlighted)
                button.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                button.sizeToFit()
                button.tag = index
                button.layer.cornerRadius = 5
                button.layer.masksToBounds = true
                
                return button
            })
            
            super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 30, height: CGFloat.leastNormalMagnitude))
            
            for button in self.tags {
                button.addTarget(self,
                                 action: #selector(TagsView.buttonClick(sender:)),
                                 for: UIControlEvents.touchUpInside)
            }
            
            tagsVariable
                .asObservable()
                .observeOn(MainScheduler.instance)
                .bind { (selectedIndexs) in
                    for button in self.tags {
                        button.isSelected = selectedIndexs.contains(button.tag)
                    }
            }
                .addDisposableTo(bag)
            
            self.addSubviews(self.tags)
            
            layoutSubviews()
        }
        
        func buttonClick(sender:UIButton) {
            if sender.isSelected {
                tagsVariable.value = tagsVariable.value.filter({ $0 != sender.tag })
            } else {
                tagsVariable.value.append(sender.tag)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private var contentSize:CGSize = .zero
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            let width = self.w
            
            if width == 0 {
                contentSize = .zero
                
                return
            }
            
            
            var alexX:CGFloat = 0
            
            var buttonCollection = [[UIButton]]([[]])
            
            let gap:CGFloat = 20
            
            for tagButton in tags {
                let right = alexX + tagButton.w
                
                if right > width {
                    alexX = tagButton.w + gap
                    
                    buttonCollection.append([tagButton])
                } else {
                    let lastIndex = buttonCollection.count - 1
                    
                    var currentButtons = buttonCollection[lastIndex]
                    
                    currentButtons.append(tagButton)
                    
                    buttonCollection[lastIndex] = currentButtons
                    
                    alexX += (tagButton.w + gap)
                }
            }
            
            var alexY:CGFloat = 0
            
            for buttonRow in buttonCollection {
                let gaps:CGFloat = CGFloat(buttonRow.count - 1) * gap
                
                let rowWidth = buttonRow.reduce(gaps, { (result, button) -> CGFloat in
                    return result +  button.w
                })
                
                alexX = (width - rowWidth) / 2.0
                
                var maxAlexY = alexY
                
                for button in buttonRow {
                    button.origin = CGPoint(x: alexX, y: alexY)
                    
                    alexX = button.right + gap
                    maxAlexY = max(maxAlexY, button.bottom + gap)
                }
                
                alexY = maxAlexY
            }
            
            contentSize = CGSize(width: width, height: alexY)
            
            invalidateIntrinsicContentSize()
        }
        
        override var intrinsicContentSize: CGSize {
            return contentSize
        }
    }
}

extension CommentSubmitVC {
    class FlymanCommentCell: UITableViewCell {
        let tagView:TagsView
        
        let tags:Driver<[String]>
        
        var tagList:[String] {
            return tagView.tagsVariable.value.map({ (index) -> String in
                return currentLabel.tags[index]
            })
        }
        
        let currentLabel:FlymanCommentLabel
        
        init(label:FlymanCommentLabel) {
            
            currentLabel = label
            
            let tagLabels = label.tags
            
            tagView = TagsView(tags: tagLabels)
            
            tags = tagView.tagsVariable.asDriver().map({ (indexs) -> [String] in
                return indexs.map({ tagLabels[$0] })
            })
            
            super.init(style: .default,
                       reuseIdentifier: "FlymanCommentCell")
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            /// avatar
            let avatarImageView = UIImageView()
            
            avatarImageView.backgroundColor = UIConfig.generalColor.white
            avatarImageView.layer.masksToBounds = true
            avatarImageView.size = CGSize(width:60, height:60)
            avatarImageView.layer.cornerRadius = 30
            avatarImageView.layer.borderWidth = 1
            avatarImageView.layer.borderColor = UIConfig.generalColor.white.cgColor
            avatarImageView.setAvatar(url: label.avatar)
            
            let avatarParentView = ViewFactory.view(color: .clear)
            
            avatarParentView.addSubview(avatarImageView)
            
            self.contentView.addSubview(avatarParentView)
            
            avatarImageView.snp.fillToSuperView()
            
            avatarParentView.layer.shadowColor = UIColor.black.cgColor
            avatarParentView.layer.shadowOffset = .zero
            avatarParentView.layer.shadowRadius = 5
            avatarParentView.layer.shadowOpacity = 0.2
            avatarParentView.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(30)
                maker.centerX.equalTo(self.contentView)
                maker.width.height.equalTo(60)
            }
            
            /// nickname
            let nicknameLabel = ViewFactory.generalLabel()
            
            nicknameLabel.text = label.nickName
            
            self.contentView.addSubview(nicknameLabel)
            
            nicknameLabel.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(self.contentView)
                maker.top.equalTo(avatarParentView.snp.bottom).offset(10)
            }
            
            /// tip
            let tipLabel = ViewFactory.generalLabel(generalSize: 13)
            
            tipLabel.text = "飞哥服务指数"
            
            self.contentView.addSubview(tipLabel)
            
            tipLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(nicknameLabel.snp.bottom).offset(30)
                maker.centerX.equalTo(self.contentView)
                maker.height.equalTo(18.5)
            }
            
            /// star
            self.contentView.addSubview(starView)
            
            starView.snp.makeConstraints { (maker) in
                maker.top.equalTo(tipLabel.snp.bottom).offset(20)
                maker.centerX.equalTo(self.contentView)
                maker.height.equalTo(23)
            }
            
            self.contentView.addSubview(starLabel)
            
            starLabel.text = "极好"
            starLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(starView.snp.bottom).offset(17)
                maker.centerX.equalTo(self.contentView)
                maker.height.equalTo(30)
            }
            
            /// tags
            let line = ViewFactory.line()
            
            self.contentView.addSubview(line)
            
            line.snp.makeConstraints { (maker) in
                maker.width.equalTo(50)
                maker.height.equalTo(0.5)
                maker.top.equalTo(starLabel.snp.bottom).offset(30)
                maker.centerX.equalTo(self.contentView)
            }
            
            self.contentView.addSubview(tagView)
            
            tagView.snp.makeConstraints { (maker) in
                maker.top.equalTo(line.snp.bottom).offset(30)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            /// button
            
            self.contentView.addSubview(submitBtn)
            
            submitBtn.layer.cornerRadius = 22
            submitBtn.layer.masksToBounds = true
            submitBtn.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(35)
                maker.right.equalTo(self.contentView).offset(-35)
                maker.bottom.equalTo(self.contentView).offset(-20)
                maker.height.equalTo(44)
                maker.top.equalTo(tagView.snp.bottom).offset(30)
            }
        }
        
        let submitBtn = ViewFactory.redButton(title: "提交")
        
        let starView = CommentStarView(5)
        
        let starLabel = ViewFactory.generalLabel(generalSize: 15,
                                                 textColor: UIConfig.generalColor.yellow)
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension CommentSubmitVC {
    class ProductCommentCell: UITableViewCell, GalleryDisplacedViewsDataSource, GalleryItemsDataSource, TZImagePickerControllerDelegate, UITextViewDelegate {
        
        typealias PicStackView = VerifyTicketVC.PicStackView
        
        let starView = CommentStarView(5)
        
        let starLabel = ViewFactory.generalLabel(generalSize: 15,
                                                 textColor: UIConfig.generalColor.yellow)
        
        let textView = KMPlaceholderTextView()
        
        var textViewHeight:Constraint!
        
        var defaultHeight:CGFloat = 0
        
        let countLabel = ViewFactory.generalLabel(generalSize: 12,
                                                  textColor: UIConfig.generalColor.labelGray)
        
        let imagesVariable = Variable<[UIImage]>([])
        
        let disposeBag = DisposeBag()
        
        let stackView = PicStackView()
        
        
        let submitBtn = ViewFactory.redButton(title: "提交")
        
        init(label:ProductCommentLabel) {
            super.init(style: .default,
                       reuseIdentifier: "ProductCommentCell")
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            /// product zone
            let productZone = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            self.contentView.addSubview(productZone)
            
            productZone.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(10)
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            let coverImageView = UIImageView()
            
            coverImageView.backgroundColor = UIConfig.generalColor.white
            coverImageView.layer.borderWidth = 0.5
            coverImageView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
            coverImageView.layer.masksToBounds = true
            coverImageView.contentMode = .scaleAspectFit
            coverImageView.kf.setImage(with: label.coverImage)
            
            productZone.addSubview(coverImageView)
            
            coverImageView.snp.makeConstraints { (maker) in
                maker.width.height.equalTo(75)
                maker.top.left.equalTo(productZone).offset(10)
                maker.bottom.equalTo(productZone).offset(-10)
            }
            
            let titleLabel = ViewFactory.generalLabel(generalSize: 13,
                                                      backgroudColor: productZone.backgroundColor)
            
            productZone.addSubview(titleLabel)
            
            titleLabel.text = label.title
            titleLabel.numberOfLines = 2
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(coverImageView.snp.right).offset(15)
                maker.right.equalTo(productZone).offset(-15)
                maker.centerY.equalTo(productZone)
            }
            
            /// star
            let introduceLabel = ViewFactory.generalLabel(generalSize: 13)
            
            self.contentView.addSubview(introduceLabel)
            
            introduceLabel.text = "值得买指数"
            introduceLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(productZone.snp.bottom).offset(30)
                maker.centerX.equalTo(self.contentView)
                maker.height.equalTo(18.5)
            }
            
            self.contentView.addSubview(starView)
            
            starView.snp.makeConstraints { (maker) in
                maker.top.equalTo(introduceLabel.snp.bottom).offset(20)
                maker.centerX.equalTo(self.contentView)
                maker.height.equalTo(23)
            }
            
            self.contentView.addSubview(starLabel)
            
            starLabel.text = "极好"
            starLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(starView.snp.bottom).offset(17)
                maker.centerX.equalTo(self.contentView)
                maker.height.equalTo(30)
            }
            
            /// comment
            let line = ViewFactory.line()
            
            self.contentView.addSubview(line)
            
            line.snp.makeConstraints { (maker) in
                maker.width.equalTo(50)
                maker.height.equalTo(0.5)
                maker.top.equalTo(starLabel.snp.bottom).offset(30)
                maker.centerX.equalTo(self.contentView)
            }
            
            self.contentView.addSubview(textView)
            
            textView.text = ""
            textView.placeholder = "写下您的心得吧~"
            textView.placeholderFont = UIConfig.generalFont(13)
            textView.placeholderColor = UIConfig.generalColor.whiteGray
            textView.font = UIConfig.generalFont(13)
            textView.textColor = UIConfig.generalColor.selected
            textView.isScrollEnabled = false
            textView.returnKeyType = .done
            textView.delegate = self
            
            let inset = textView.textContainerInset
            
            defaultHeight = NSAttributedString(string: textView.placeholder,
                                               font: textView.placeholderFont!,
                                               textColor: textView.placeholderColor)
                .boundingRect(with: CGSize.init(width: UIScreen.main.bounds.width - 30 - inset.left - inset.right,
                                                height: .greatestFiniteMagnitude),
                              options: .usesLineFragmentOrigin,
                              context: nil)
                .height + inset.bottom + inset.top
            
            textView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(line.snp.bottom).offset(30)
//                textViewHeight = maker.height.equalTo(defaultHeight).constraint
            }
            
            self.contentView.addSubview(countLabel)
            
            countLabel.text = "0/1000"
            countLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(textView)
                maker.top.equalTo(textView.snp.bottom).offset(5)
            }
            
            /// pics
            let stackView = self.stackView
            
            self.contentView.addSubview(stackView)
            
            stackView.childSideLength = (UIScreen.main.bounds.width - 60) / 3.0
            stackView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(textView.snp.bottom).offset(50)
                maker.bottom.equalTo(self.contentView).offset(-20).priority(998)
            }
            
            imagesVariable
                .asObservable()
                .observeOn(MainScheduler.instance)
                .bind { (selectedImages) in
                    var stackChildViews = selectedImages.mapEnumerated{ (index, image) -> UIView in
                        let imageView = UIImageView(image: image)
                        
                        imageView.contentMode = .scaleAspectFill
                        imageView.backgroundColor = UIConfig.generalColor.white
                        imageView.layer.cornerRadius = 5
                        imageView.layer.masksToBounds = true
                        imageView.clipsToBounds = true
                        imageView.isUserInteractionEnabled = true
                        imageView.addTapGesture(target: self,
                                                action: #selector(ProductCommentCell.imagePreviewGesture(sender:)))
                        
                        let button = ViewFactory.button(imageNamed: "icon_delete_images")
                        
                        imageView.addSubview(button)
                        
                        button.tag = index
                        button.snp.makeConstraints({ (maker) in
                            maker.width.height.equalTo(35)
                            maker.right.top.equalTo(imageView)
                        })
                        button.addTarget(self,
                                         action: #selector(ProductCommentCell.deleteImagePressed(sender:)),
                                         for: UIControlEvents.touchUpInside)
                        
                        return imageView
                    }
                    
                    if stackChildViews.count < 6 {
                        let button = UIButton(type: .custom)
                        
                        button
                            .setImage(ProductCommentCell.addImage(text: "\(stackChildViews.count)/6"),
                                        for: .normal)
                        button.addTarget(self,
                                         action: #selector(ProductCommentCell.addImagePressed(sender:)),
                                         for: UIControlEvents.touchUpInside)
                        
                        stackChildViews.append(button)
                    }
                    
                    stackChildViews.forEachEnumerated { (tag, subView) in
                        subView.tag = tag
                    }
                    
                    stackView.childViews = stackChildViews
            }
                .addDisposableTo(disposeBag)
            
            /// button
            
            self.contentView.addSubview(submitBtn)
            
            submitBtn.layer.cornerRadius = 22
            submitBtn.layer.masksToBounds = true
            submitBtn.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(35)
                maker.right.equalTo(self.contentView).offset(-35)
                constraint = maker.bottom.equalTo(self.contentView).offset(-20).priority(999).constraint
                maker.height.equalTo(44)
                maker.top.equalTo(stackView.snp.bottom).offset(30)
            }
        }
        
        var constraint:Constraint!
        
        func setButtonHidden(_ hidden:Bool) {
            submitBtn.isHidden = hidden
            
            if hidden {
                constraint.deactivate()
            } else {
                constraint.activate()
            }
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
                
                return false
            }
            
            return true
        }
        
        static func addImage(text:String) -> UIImage? {
            let length = (UIScreen.main.bounds.width - 60) / 3.0
            
            UIGraphicsBeginImageContextWithOptions(CGSize(width: length, height: length),
                                                   false,
                                                   UIScreen.main.scale)
            
            guard let ct:CGContext = UIGraphicsGetCurrentContext() else {
                UIGraphicsEndImageContext()
                
                return nil
            }
            
            let path = CGPath(roundedRect: CGRect(x: 2, y: 2, width: length - 4, height: length - 4),
                              cornerWidth: 5,
                              cornerHeight: 5,
                              transform: nil)
            
            ct.addPath(path)
            ct.setLineCap(.round)
            ct.setLineJoin(.round)
            ct.setLineWidth(2)
            ct.setStrokeColor(UIColor(hexString: "#eeeeee")!.cgColor)
            ct.setLineDash(phase: 0, lengths: [8,7])
            ct.strokePath()
            
            let image = UIImage(named: "icon_add_photo-1")!
            
            let imageFrame = CGRect(x: (length - image.size.width) / 2.0,
                                    y: length / 2.0 - image.size.height - 5,
                                    width: image.size.width,
                                    height: image.size.height)
            
            image.draw(in: imageFrame)
            
            let attributed = NSAttributedString(string: text,
                font: UIConfig.generalFont(13),
                textColor: UIConfig.generalColor.whiteGray)
            
            let labelSize = attributed
                .boundingRect(with: CGSize.init(width: length,
                                                height: CGFloat.greatestFiniteMagnitude),
                              options: .usesLineFragmentOrigin,
                              context: nil)
                .size
            
            attributed.draw(at: CGPoint(x: length / 2.0 - labelSize.width / 2.0,
                                        y: imageFrame.maxY + 10))
            
            
            
            guard let outputImage = UIGraphicsGetImageFromCurrentImageContext() else {
                UIGraphicsEndImageContext()
                
                return nil
            }
            
            UIGraphicsEndImageContext()
            
            return outputImage
        }
        
        weak var viewController:UIViewController?
        
        func imagePreviewGesture(sender:UITapGestureRecognizer) {
            guard let index = sender.view?.tag, let viewController = self.viewController else {
                return
            }
            
            let galleryVC = GalleryViewController(startIndex: index,
                                                  itemsDataSource: self,
                                                  displacedViewsDataSource: self,
                                                  configuration: ProductDetailVC.galleryConfiguration())
            
            galleryVC.headerView = nil
            
            viewController.present(galleryVC,
                                   animated: false,
                                   completion: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
            return (stackView.childViews[index] as? UIImageView).flatMap({ $0 })
        }
        
        func itemCount() -> Int {
            return imagesVariable.value.count
        }
        
        func provideGalleryItem(_ index: Int) -> GalleryItem {
            return .image(fetchImageBlock: { (fetchImageBlock) in
                fetchImageBlock(self.imagesVariable.value[index])
            })
        }
        
        func deleteImagePressed(sender:UIButton) {
            let index = sender.tag
            
            let alert = ViewFactory.deleteActionSheet(from: sender,
                                                      content: "是否要删除这张照片") {
                                                        self.imagesVariable.value.remove(at: index)
            }
            
            self.viewController?.navigationController?.present(alert,
                                                               animated: true,
                                                               completion: nil)
        }
        
        func addImagePressed(sender:Any?) {
            guard let imagePickerController = TZImagePickerController(maxImagesCount: 6 - self.imagesVariable.value.count,
                                                                      delegate: self) else {
                                                                        return
            }
            
            imagePickerController.allowCrop = false
            imagePickerController.allowPickingVideo = false
            imagePickerController.allowPickingGif = false
            imagePickerController.allowPickingOriginalPhoto = false
            
            self.viewController?.navigationController?.presentVC(imagePickerController)
        }
        
        func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
            self.imagesVariable.value += photos ?? []
        }
    }
}

extension CommentSubmitVC {
    func uploadImagePressed(sender:Any?) {
        
        guard let commentCell = cells.first as? ProductCommentCell,
            commentCell.textView.text.length <= 1000 else {
            self.showToast(text: "字数不能超过1000")
            return
        }
        
        uploadedPaths = []
        
        uploadImage(at: 0, toast: nil)
    }
    
    func uploadImage(at index:Int, toast:ToastViewController?) {
        
        guard let commentCell = cells.first as? ProductCommentCell else {
            return
        }
        
        let flymanCell = cells.last as? FlymanCommentCell
        
        let selectedImages = commentCell.imagesVariable.value
        
        /// upload ticket
        if index >= selectedImages.count {
            
            var _toast = toast
            
            if let aToast = _toast {
                aToast.displayLoadingAndLabel(text: "上传评论中")
            } else {
                _toast = self.showToast(text: "上传评论中")
            }
            
            /// upload pic url into ticket
            API
                .Comment
                .submitCommentWith(orderformId: viewModel.orderFormid,
                                   buyScore: commentCell.starView.starVariable.value,
                                   serviceScore: flymanCell?.starView.starVariable.value ?? 0,
                                   commentsDesc: commentCell.textView.text ?? "",
                                   serviceSelectedLabel: flymanCell?.tagList ?? [],
                                   commentsPic: uploadedPaths)
                .responseNoModel({
                    ez.runThisInMainThread {
                        NotificationCenter
                            .default
                            .post(name: .TicketAddSuccessNotification,
                                  object: nil)
                        
                        _toast?.dismiss(animated: true,
                                       completion: {
                                        
                                        let _ = self.navigationController?.popViewController(animated: true)
                                        
                                        self.navigationController?.showToast(text: "评价成功")
                                        
                                        self.uploadFinishCallBack?()
                                        
                                        NotificationCenter
                                            .default
                                            .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                  object: nil)
                        })
                    }
                }, error: { (error) in
                    ez.runThisInMainThread {
                        _toast?.displayLabel(text:error.description)
                    }
                })
            
            return
        }
        
        /// upload image
        let image = selectedImages[index]
        
        guard let imageData = UIImageJPEGRepresentation(image, 0.7) else {
            
            toast?.displayLabel(text: "")
            
            return
        }
        
        let toastLabel = "正在上传第\(index + 1)张图片，请稍等"
        
        toast?.displayLoadingAndLabel(text: toastLabel)
        
        let `toast` = toast ?? self.showLoadingAndToast(toastLabel)
        
        ImagePicker
            .httpManager
            .post(URLConfig.imageUploader + "files/uploadProcesser",
                  parameters: ["type":"x99",
                               "module":"member"],
                  constructingBodyWith: { (formData) in
                    formData.appendPart(withFileData: imageData,
                                        name: "file",
                                        fileName: NSUUID().uuidString + ".jpg",
                                        mimeType: "image/jpeg")
            },
                  progress: nil,
                  success: {
                    (task, result) in
                    
                    do {
                        let model:API.UploaderImage = try HttpSession.model(from: result ?? [String:Any]())
                        
                        self.uploadedPaths.append(model.urlString)
                        
                        ez.runThisInMainThread {
                            self.uploadImage(at: index + 1, toast: toast)
                        }
                        
                    } catch HttpError.businessError(_, let description) {
                        ez.runThisInMainThread {
                            toast.displayLabel(text: "第\(index + 1)张图片上传失败，原因为\(description)")
                        }
                    } catch {
                        ez.runThisInMainThread {
                            toast.displayLabel(text: "第\(index + 1)张图片上传失败")
                        }
                    }
                    
            }) { (task, error) in
                ez.runThisInMainThread {
                    toast.displayLabel(text: "第\(index + 1)张图片上传失败，原因为\(error.localizedDescription)")
                }
        }
    }
}
