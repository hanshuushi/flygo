//
//  CommentSubmitVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/26.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ProductCommentLabel {
    let coverImage:URL?
    
    let title:String
    
    let labels:[String]
    
    init(model:API.CommentOrder) {
        coverImage = model.pic
        
        title = model.productName
        
        labels = model.buyScoreLabels
    }
    
    func label(of star:Int) -> String? {
        if star > labels.count {
            return nil
        }
        
        return labels[star - 1]
    }
}

struct FlymanCommentLabel {
    let avatar:URL?
    
    let nickName:String
    
    let labels:[String]
    
    let tags:[String]
    
    init(model:API.CommentOrder) {
        avatar = model.userAvatar
        
        nickName = model.nickname
        
        labels = model.serviceScoreLabels
        
        tags = model.serviceLabels
    }
    
    func label(of star:Int) -> String? {
        if star > labels.count {
            return nil
        }
        
        return labels[star - 1]
    }
}

class CommentSubmitVM: ViewModel {
    
    let orderFormid:String
    
    init(orderFormid:String) {
        self.orderFormid = orderFormid
    }
    
    func bind(to view: CommentSubmitVC) -> DisposeBag? {
        
        let orderFormid = self.orderFormid
        
        let disposeBag = DisposeBag()
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .none,
                                       refreshEnable: false)
        
        
        
        let request = dataSet
            .refrence(getRequest: { API
                .CommentOrder
                .getCommentLabel(of: orderFormid) },
                      isEmpty: { _ in false },
                      in: disposeBag)
            .shareReplay(1)
        
        request
            .flatMapLatest { (model) -> Observable<[UITableViewCell]> in
                
                let productLabel = ProductCommentLabel(model: model)
                
                let productCell = CommentSubmitVC.ProductCommentCell(label: productLabel)
                
                var flymanLabel:FlymanCommentLabel?
                
                var flymanCell:CommentSubmitVC.FlymanCommentCell?
                
                if model.selfSupport != 1 {
                    flymanLabel = FlymanCommentLabel(model: model)
                    
                    flymanCell = CommentSubmitVC.FlymanCommentCell(label: flymanLabel!)
                    
                    productCell.setButtonHidden(true)
                } 
                
                return Observable.create({
                    [weak view] (obs) -> Disposable in
                    
                    guard let `view` = view else {
                        return Disposables.create()
                    }
                    
                    let tableView = view.tableView
                    
                    if let `flymanCell` = flymanCell {
                        obs.onNext([productCell, flymanCell])
                    } else {
                        obs.onNext([productCell])
                    }
                    
                    let productNumber = productCell.starView.starVariable.asDriver()
                    
                    let productStarLabelString = productNumber.map({ productLabel.label(of: $0) })
                    
                    let p1 = productStarLabelString.filter({ $0 != nil })
                        .drive(onNext: { (text) in
                            productCell.starLabel.text = text
                        },
                               onCompleted: nil,
                               onDisposed: nil)
                    
                    let content = productCell
                        .textView
                        .rx
                        .text
                        .asDriver().map({ (text) -> String in
                            guard let `text` = text, text.length > 0 else {
                                return productCell.textView.placeholder
                            }
                            
                            return text
                        })
                        .distinctUntilChanged()
                    
                    let inset = productCell.textView.textContainerInset
                    
                    let screenWidth = UIScreen.main.bounds.width
                    
                    let textViewFont = productCell.textView.font!
                    
                    let textViewColor = productCell.textView.textColor!
                    
                    let textViewHeight = content
//                        .map({ (text) -> CGFloat in
//                        let attributedString = NSAttributedString(string: text,
//                                                                  font: textViewFont,
//                                                                  textColor: textViewColor,
//                                                                  lineSpace: 3)
//                        
//                        return attributedString
//                            .boundingRect(with: CGSize.init(width: screenWidth - 30 - inset.left - inset.right,
//                                                            height: .greatestFiniteMagnitude),
//                                          options: .usesLineFragmentOrigin,
//                                          context: nil)
//                            .height + inset.top + inset.bottom
//                    })
//                        .startWith(productCell.defaultHeight)
                        .throttle(0.3)
//                        .distinctUntilChanged()
                        .map({ (_) -> Void in
                            return ()
                        })
                        .skip(1)
                    
                    let picsHeightChange = productCell
                        .imagesVariable
                        .asDriver()
                        .distinctUntilChanged({ (ori, new) -> Bool in
                            func getLine(count:Int) -> Int {
                                let allCount =  count < 6 ? count + 1 : 6
                                
                                return (allCount - 1) / 3
                            }
                            
                            return getLine(count: ori.count) == getLine(count: new.count)
                        })
                        .map({ (_) -> Void in
                            return ()
                        })
                        .skip(1)
                    
                    let changeHeight = Driver.of(textViewHeight, picsHeightChange)
                        .merge()
                    
                    let p2 = changeHeight
                        .drive(onNext: { () in
                            tableView?.beginUpdates()
                            
                            tableView?.endUpdates()
                        },
                               onCompleted: nil,
                               onDisposed: nil)
                    
                    let count = productCell
                        .textView
                        .rx
                        .text
                        .asDriver()
                        .map({ $0?.length ?? 0 })
                    
                    let countAttributed = count
                        .map({ (number) -> NSAttributedString in
                            
                            let text = "\(number)/1000"
                            
                            if number > 1000 {
                                return NSAttributedString(string: text,
                                                          font: UIConfig.generalFont(12),
                                                          textColor: UIConfig.generalColor.red)
                            }
                            
                            return NSAttributedString(string: text,
                                                      font: UIConfig.generalFont(12),
                                                      textColor: UIConfig.generalColor.labelGray)
                        })
                    
                    let p3 = countAttributed
                        .drive(productCell.countLabel.rx.attributedText)
                    
                    if let `flymanCell` = flymanCell, let `flymanLabel` = flymanLabel {
                        let flymanNumber = flymanCell.starView.starVariable.asDriver()
                        
                        let flymanStarLabelString = flymanNumber.map({ flymanLabel.label(of: $0) })
                        
                        let p4 = flymanStarLabelString.filter({ $0 != nil })
                            .drive(onNext: { (text) in
                                flymanCell.starLabel.text = text
                            },
                                   onCompleted: nil,
                                   onDisposed: nil)
                        
                        
                        return Disposables.create(p1, p2, p3, p4)
                    } else {
                        return Disposables.create(p1, p2, p3)
                    }
                })
                    .shareReplay(1)
        }.bind {
            [weak view] (cells) in
            
            view?.cells = cells
        }
            .addDisposableTo(disposeBag)
        
        return disposeBag
    }
    
}
