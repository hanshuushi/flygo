//
//  CommentStarView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/26.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CommentStarView: UIView {
    
    let starVariable:Variable<Int>
    
    let disposeBag:DisposeBag
    
    let contentSize:CGSize
    
    init(_ defaultSelectedStarNumber:Int) {
        
        disposeBag = DisposeBag()
        
        let selectedStarImage = UIImage(named: "icon_score_selected")!
        
        let unselectedStarImage = UIImage(named: "icon_score_normal")
        
        let width = selectedStarImage.size.width * 5 + 60
        
        let height = selectedStarImage.size.height
        
        contentSize = CGSize(width: width, height: height)
        
        starVariable = Variable(defaultSelectedStarNumber)
        
        super.init(frame: CGRect.init(x: 0, y: 0, width: width, height: height))
        
        var buttons = [UIButton]()
        
        for i in 0..<5 {
            let button = UIButton(type: .custom)
            
            self.addSubview(button)
            
            buttons.append(button)
            
            let floatI = CGFloat(i)
            
            let left = floatI * (15 + selectedStarImage.size.width)
            
            button.tag = i + 1
            button.frame = CGRect(x: left,
                                  y: 0,
                                  width: selectedStarImage.size.width,
                                  height: selectedStarImage.size.height)
            button.addTarget(self,
                             action: #selector(CommentStarView.startButtonClick(sender:)),
                             for: UIControlEvents.touchUpInside)
        }
        
        starVariable
            .asDriver()
            .drive(onNext: { (starNumber) in
                for (index, button) in buttons.enumerated() {
                    if index < starNumber {
                        button.setImage(selectedStarImage,
                                        for: .normal)
                        button.setImage(unselectedStarImage,
                                        for: .highlighted)
                    } else {
                        button.setImage(unselectedStarImage,
                                        for: .normal)
                        button.setImage(selectedStarImage,
                                        for: .highlighted)
                    }
                }
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
    
    func startButtonClick(sender:UIButton) {
        let star = sender.tag
        
        self.starVariable.value = star
    }
}
