//
//  CategoryVCBrandViewModel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/8.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RxDataSources

class CategoryVCBrandViewModel: ViewModel {

    let allBrands:Driver<[ProductBrandItem]>
    
    let hots:Driver<[ProductBrandItem]>
    
    init() {
        allBrands = DictionaryManager
            .shareInstance
            .brandList
            .map({ $0.map({ ProductBrandItem(model: $0) }) })
        
        hots = API
            .Brand
            .getHots()
            .filter{$0.isSuccess}
            .map { (item) in
            return (item.model?.list ?? []).map{ ProductBrandItem(model:$0) }
        }
            .asDriver(onErrorJustReturn: [])
    }
    
    func bind(to view: CategoryVCBrandView) -> DisposeBag? {
        
        let bag = DisposeBag()
        
        hots
            .drive(view.hotBrands)
            .addDisposableTo(bag)
        
        allBrands.map({ (items) -> [SectionModel<String, ProductBrandItem>] in
            
            var dict:[String: [ProductBrandItem]] = [:]
            
            for item in items {
                if dict[item.indexLetter] == nil {
                    dict[item.indexLetter] = [item]
                } else {
                    dict[item.indexLetter]!.append(item)
                }
            }
            
            var array = [SectionModel<String, ProductBrandItem>]()
            
            for (k, v) in dict {
                array.append(SectionModel(model: k,
                                          items: v))
            }
            
            return array.sorted(by: { (model, otherModel) -> Bool in
                return model.model < otherModel.model
            })
            
        }).drive(view.allBrands).addDisposableTo(bag)
        
        return bag
        
    }
}
