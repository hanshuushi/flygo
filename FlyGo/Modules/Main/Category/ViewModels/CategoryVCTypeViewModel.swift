//
//  CategoryVCTypeViewModel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/15.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class CategoryVCTypeViewModel: ViewModel {
    
    let parentType:Driver<[ProductTypeItem]>

    let childType:Driver<[ProductTypeItem]>
    
    let selectedItem:PublishSubject<String>
    
    init() {
        selectedItem = PublishSubject()
        
        parentType = DictionaryManager
            .shareInstance
            .typeList
            .map{ $0.map({ ProductTypeItem(model:$0) }) }
        
        let defaultChildType =  DictionaryManager
            .shareInstance
            .typeList
            .asObservable()
            .map({ $0.count < 1 ? [] : $0[0]
                .subTypeList
                .map{ProductTypeItem(model:$0)} })
        
        let selectedChildType = selectedItem
            .withLatestFrom(DictionaryManager
                .shareInstance
                .typeList
                .asObservable()) { (selectedId, items) -> [ProductTypeItem] in
                let selectedItems = items
                    .filter({ $0.productTypeId == selectedId })
                
                if selectedItems.count <= 0 {
                    return []
                }
                
                let selectedItem = selectedItems[0]
                
                return selectedItem.subTypeList.map{ProductTypeItem(model:$0)}
        }
        
        childType = Observable.of(defaultChildType, selectedChildType).merge().asDriver(onErrorJustReturn: [])
    }
    
    func bind(to view: CategoryVCTypeView) -> DisposeBag? {
        let bag = DisposeBag()
        
        parentType
            .drive(view.leftValue)
            .addDisposableTo(bag)
        
        childType
            .drive(view.rightValue)
            .addDisposableTo(bag)
        
        view
            .leftSelected
            .bindNext(selectedItem)
            .addDisposableTo(bag)
        
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        view.leftTableView.selectRow(at: indexPath,
                                     animated: false,
                                     scrollPosition: .none)
        
        return bag
    }
    
}
