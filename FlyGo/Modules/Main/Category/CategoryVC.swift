//
//  CategoryVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/18.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CategoryVC: UIViewController {

    let brandPresent:Presenter<CategoryVCBrandViewModel> = {
        let p = Presenter(viewModel: CategoryVCBrandViewModel(),
                         view: CategoryVCBrandView())

        p.bind()

        return p
    }()
    
    let typePresent:Presenter<CategoryVCTypeViewModel> = {
        let p = Presenter(viewModel: CategoryVCTypeViewModel(),
                          view: CategoryVCTypeView())
        
        p.bind()
        
        return p
    }()
    
    let tempView = { Void -> UIView in
        let view = UIView()
        
        view.backgroundColor = UIConfig.generalColor.backgroudGray
        
        return view
    }()
    
    var navTabView:NavTabView!
    
    var style = CommonTabViewStyle()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        navTabView = NavTabView()
        
        self.view.addSubview(navTabView)
        
        navTabView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0))
        }
        navTabView.navTabStyle = style
        navTabView.dataSource = self
        
        brandPresent.containView.brandSelected.bind { [weak self](item) in
            let vc = ProductListVC(brandId: item.brandId,
                                   brandName: item.brandTitle)
            
            self?.pushVC(vc)
        }
            .addDisposableTo(disposeBag)
        
        typePresent.containView.rightSelected.bind { [weak self](type) in
            self?.pushVC(ProductListVC(type: type))
            }
            .addDisposableTo(disposeBag)
    }
    
    private let _onceToken = NSUUID().uuidString
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.once(token: _onceToken) {
            self.typePresent.containView.leftTableView.selectRow(at: IndexPath(row: 0, section: 0),
                                                                 animated: false,
                                                                 scrollPosition: .none)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        brandPresent.containView.frame = self.view.bounds
        
        let leftWith = ("分类" as NSString).size(attributes: [
            NSFontAttributeName: style.font
            ]).width
        
        let rightWith = ("品牌" as NSString).size(attributes: [
            NSFontAttributeName: style.font
            ]).width
        
        
        let padding = (self.view.bounds.width - (leftWith + rightWith)) / 4.0
        
        style.titlePadding = padding
        style.titleInset = padding * 2.0
        
        navTabView.navTabStyle = style
        navTabView.reloadData()
    }
}

extension CategoryVC: NavTabViewDataSource {
    func titleArrayForNavTabView(_ navTabView:NavTabView) -> [String] {
        return ["分类", "品牌"]
    }
    
    func navTabView(_ navTabView:NavTabView, viewAtIndex index:Int) -> UIView {
        if index == 1 {
            return brandPresent.containView.view
        }
        
        return typePresent.containView.view
    }
}
