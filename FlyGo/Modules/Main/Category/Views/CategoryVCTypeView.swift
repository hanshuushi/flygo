//
//  CategoryVCTypeView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/12.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CategoryVCTypeView: UIView {
    
    let leftTableView:UITableView
    
    let rightTableView:UITableView
    
    let leftValue:Variable<[ProductTypeItem]>
    
    let rightValue:Variable<[ProductTypeItem]>
    
    let leftSelected:Observable<String>
    
    let rightSelected:Observable<ProductTypeItem>
    
    let disposeBag:DisposeBag
    
    convenience init() {
        self.init(frame:.zero)
    }
    
    override init(frame: CGRect) {
        disposeBag = DisposeBag()
        
        leftValue = Variable([])
        
        rightValue = Variable([])
        
        leftTableView = UITableView(frame: CGRect(x:frame.origin.x,
                                                  y:frame.origin.y,
                                                  width:frame.size.width / 2.0,
                                                  height:frame.size.height)
            , style: .plain)
        leftTableView.allowsSelection = true
        leftTableView.allowsMultipleSelection = false
        leftTableView.registerCell( cellClass: LeftCell.self)
        leftTableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        leftTableView.contentInset = UIEdgeInsetsMake(110, 0, 49, 0)
        leftTableView.scrollIndicatorInsets = leftTableView.contentInset
        leftTableView.contentOffset = CGPoint(x: 0, y: -110)
        leftTableView.rowHeight = 55
        leftTableView.showsVerticalScrollIndicator = false
        leftSelected = leftTableView
            .rx
            .itemSelected
            .withLatestFrom(leftValue.asObservable(),
                            resultSelector: { (indexPath, items) -> String in
                                return items[indexPath.row].id
            })
        leftTableView.tableFooterView = UIView()
        leftValue
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: leftTableView
                .rx
                .items(cellIdentifier: "LeftCell",
                       cellType: LeftCell.self)) { (row, element, cell) in
                        cell.set(item: element)
        }.addDisposableTo(disposeBag)
        
        
        rightTableView = UITableView(frame: CGRect(x:frame.size.width / 2.0,
                                               y:frame.origin.y,
                                               width:frame.size.width / 2.0,
                                               height:frame.size.height)
            , style: .plain)
        rightTableView.allowsSelection = true
        rightTableView.allowsMultipleSelection = false
        rightTableView.registerCell(cellClass: RightCell.self)
        rightTableView.backgroundColor =  UIColor(r: 249, g: 249, b: 249)
        rightTableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        rightTableView.contentInset = UIEdgeInsetsMake(110, 0, 49, 0)
        rightTableView.scrollIndicatorInsets = rightTableView.contentInset
        rightTableView.contentOffset = CGPoint(x: 0, y: -110)
        rightTableView.backgroundView = UIView()
        rightTableView.rowHeight = 43
        rightTableView.backgroundView?.backgroundColor = rightTableView.backgroundColor
        rightTableView.tableFooterView = UIView()
        
        rightSelected = rightTableView
            .rx
            .itemSelected
            .withLatestFrom(rightValue.asObservable(),
                            resultSelector: { (indexPath, items) -> ProductTypeItem in
                                
                                print("row = \(indexPath.row)")
                                
                                return items[indexPath.row]
            })
        
        rightValue
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: rightTableView
                .rx
                .items(cellIdentifier: "RightCell",
                       cellType: RightCell.self)) { (row, element, cell) in
                        cell.label.text = element.name
            }.addDisposableTo(disposeBag)
        
        super.init(frame: frame)
        
        self.addSubview(leftTableView)
        self.addSubview(rightTableView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let bounds = self.bounds
        
        let width = bounds.width / 2.0
        
        leftTableView.frame = CGRect(x: 0,
                                     y: 0,
                                     w: width,
                                     h: bounds.height)
        
        rightTableView.frame = CGRect(x: width,
                                      y: 0,
                                      w: width,
                                      h: bounds.height)
        
        rightTableView.backgroundView?.frame = CGRect(x: 0,
                                                      y: 0,
                                                      w: width,
                                                      h: bounds.height)
        
        leftTableView.separatorColor = UIConfig.generalColor.lineColor
        rightTableView.separatorColor = UIConfig.generalColor.lineColor
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CategoryVCTypeView {
    
    
    class RightCell: UITableViewCell {
        let label:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            label = UILabel()
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubview(label)
            
            self.selectionStyle = .none
            
            self.label.font = UIConfig.generalFont(13)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.snp.makeConstraints({ (maker) in
                maker.left.equalTo(self.contentView).offset(12.5)
                maker.centerY.equalTo(self.contentView)
            })
            self.contentView.backgroundColor = UIColor(r: 249, g: 249, b: 249)
            self.backgroundColor = UIColor(r: 249, g: 249, b: 249)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class LeftCell: UITableViewCell {
        
        let label:UILabel
        
        static let path:CGPath = {
            let bezierPath = UIBezierPath()
            
            let rect = CGRect(x: 0,
                              y: 0,
                              w: 10,
                              h: 20)
            
            bezierPath.move(to: CGPoint(x: 10, y: 0))
            bezierPath.addLine(to: CGPoint(x: 0, y: 10))
            bezierPath.addLine(to: CGPoint(x: 10, y: 20))
            bezierPath.close()
            
            return bezierPath.cgPath
        }()
        
        let selectionView:UIView
        
        let tagImageView:UIImageView
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            let layer = CAShapeLayer()
            
            layer.path = LeftCell.path
            layer.fillColor = UIColor(r: 249, g: 249, b: 249).cgColor
            layer.frame = CGRect(x: 0, y: 0, w: 10, h: 20)
            
            selectionView = UIView(x: 0, y: 0, w: 10, h: 20)
            selectionView.backgroundColor = UIConfig.generalColor.white
            selectionView.layer.addSublayer(layer)
            
            tagImageView = UIImageView()
            
            label = UILabel()
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(tagImageView)
            
            tagImageView.backgroundColor = UIColor.clear
            tagImageView.contentMode = .scaleAspectFit
            tagImageView.snp.makeConstraints({ (maker) in
                maker.left.equalTo(self.contentView).offset(25)
                maker.top.bottom.equalTo(self.contentView)
                maker.width.equalTo(20)
            })
            
            self.contentView.addSubview(label)
            
            self.label.font = UIConfig.generalSemiboldFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.snp.makeConstraints({ (maker) in
                maker.left.equalTo(self.tagImageView.snp.right).offset(25)
                maker.centerY.equalTo(self.contentView)
            })
            
            self.contentView.addSubview(selectionView)
            
            selectionView.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView)
                maker.size.equalTo(layer.frame.size)
                maker.centerY.equalTo(self.contentView)
            }
        }
        
        override var isSelected: Bool {
            get {
                return super.isSelected
            }
            set {
                super.isSelected = newValue
                
                selectionView.isHidden = !newValue
            }
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            selectionView.isHidden = !selected
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func set(item:ProductTypeItem) {
            tagImageView.kf.setImage(with:item.imageUrl)
         
            self.label.text = item.name
        }
    }
}
