//
//  CategoryVCBrandView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/8.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class CategoryVCBrandView: UITableView {
    
    let hotBrands: Variable<[ProductBrandItem]>
    
    let allBrands: Variable<[SectionModel<String, ProductBrandItem>]>
    
    let disposeBag = DisposeBag()
    
    var brandSelected: Observable<(brandId:String, brandTitle:String)>! = nil
    
    init() {
        hotBrands = Variable([])
        
        allBrands = Variable([])
        
        let headView = hotBrands
            .asObservable()
            .map({ HeaderView(items:$0) })
            .shareReplay(1)
            .observeOn(MainScheduler.instance)
        
        let hotBrandSelected = headView.flatMapLatest({ $0.itemSelected }).shareReplay(1)
        
        super.init(frame: .zero,
                   style: .plain)
        
        
        self.contentInset = UIEdgeInsetsMake(110, 0, 49, 0)
        self.scrollIndicatorInsets = self.contentInset
        self.contentOffset = CGPoint(x: 0, y: -110)
        self.rowHeight = 43
        self.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        self.separatorColor = UIConfig.generalColor.lineColor
        self.sectionHeaderHeight = 20
        self.delegate = self
        self.registerCell(cellClass: TableViewCell.self)
        self.tableFooterView = UIView()
        
        weak var weakSelf = self
        
        headView.bind { (view) in
            if let strongSelf = weakSelf {
                strongSelf.tableHeaderView = view
                strongSelf.reloadData()
            }
        }.addDisposableTo(disposeBag)
        
        let itemSelected = self.rx
            .modelSelected(ProductBrandItem.self).map({ (item) -> (brandId:String, brandTitle:String) in
                return (brandId:item.id, brandTitle:item.name)
            })
        
        brandSelected = Observable.of(hotBrandSelected, itemSelected).merge()
        
        allBrands.asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.items(dataSource: CategoryVCBrandView.configureDataSource()))
            .addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CategoryVCBrandView {
    class HeaderView : UIView {
        let itemSelected:Driver<(brandId:String, brandTitle:String)>
        
        let firstLabel = UILabel()
        
        let finalLabel = UILabel()
        
        let buttons:[UIButton]
        
        init (items:[ProductBrandItem]) {
            
            firstLabel.text = "热门品牌"
            firstLabel.font = UIConfig.generalSemiboldFont(15)
            firstLabel.textColor = UIConfig.generalColor.selected
            firstLabel.backgroundColor = UIConfig.generalColor.white
            firstLabel.sizeToFit()
            
            buttons = items.map { (item) -> UIButton in
                let button = UIButton(type: .custom)
                
                button.backgroundColor = UIConfig.generalColor.white
                button.kf.setImage(with:item.imageUrl,
                                   for: .normal)
                button.layer.setValue(item.id,
                                      forKey: "itemId")
                button.layer.setValue(item.name,
                                      forKey: "itemName")
                button.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
                
                
                button.layer.borderWidth = 0.5
                button.layer.borderColor = UIConfig.generalColor.backgroudWhite.cgColor
                button.layer.masksToBounds = true
                
                return button
            }
            
            itemSelected = Observable.from(
                buttons.map({
                    btn in
                    return btn.rx
                        .tap
                        .map({
                            _ in
                            return (brandId:btn
                                .layer
                                .value(forKey: "itemId") as! String,
                                    brandTitle:btn
                                        .layer
                                        .value(forKey: "itemName") as! String)
                        })
                })).merge().asDriver(onErrorJustReturn: (brandId:"", brandTitle:""))
            
            finalLabel.text = "所有品牌"
            finalLabel.font = UIConfig.generalSemiboldFont(15)
            finalLabel.textColor = UIConfig.generalColor.selected
            finalLabel.backgroundColor = UIConfig.generalColor.white
            finalLabel.sizeToFit()
            
            let _finalLabel = finalLabel
            
            let _buttons = buttons
            
            super.init(frame: CGRect(x:0,
                                     y:0,
                                     width:UIScreen.main.bounds.width,
                                     height:HeaderView.getHeight(finalLabel: _finalLabel,
                                                                 buttons: _buttons)))
            
            self.view.addSubview(firstLabel)
            
            self.view.addSubviews(buttons)
            
            self.view.addSubview(finalLabel)
            
            if buttons.count > 0 {
                firstLabel.isHidden = false
                firstLabel.origin = CGPoint(x: 15, y: 15)
                
                let top:CGFloat = 50, left:CGFloat = 15
                
                let itemWidth:CGFloat = (self.bounds.width - 30) / 4.0
                
                for (index, button) in buttons.enumerated() {
                    let x = left + CGFloat(index % 4) * itemWidth
                    
                    let y = top + CGFloat(index / 4) * itemWidth
                    
                    button.frame = CGRect(x: x,
                                          y: y,
                                          width:itemWidth,
                                          height:itemWidth)
                }
                
                finalLabel.left = firstLabel.left
                finalLabel.bottom = self.bounds.height - 15
            } else {
                firstLabel.isHidden = true
                finalLabel.left = 15
                finalLabel.centerYInSuperView()
            }
        }
        
        static func getHeight(finalLabel:UILabel, buttons:[UIButton]) -> CGFloat {
            let width = UIScreen.main.bounds.width
            
            if buttons.count <= 0 {
                return 30 + finalLabel.h
            }
            
            let inset:CGFloat = 105.0
            
            let lineCount = CGFloat(buttons.count / 4 + (buttons.count % 4 == 0 ? 0 : 1))
            
            let itemWidth = (width - 20) / 4.0
            
            return itemWidth * lineCount + inset
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension CategoryVCBrandView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView, let label = header.textLabel {
            label.font = UIConfig.arialFont(13)
            label.textColor = UIColor(r: 175, g: 177, b: 179)
            label.backgroundColor = UIConfig.generalColor.backgroudGray
        }
        
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
        } else {
            view.backgroundColor = UIConfig.generalColor.backgroudGray
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == allBrands.value.count - 1 {
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let headerFooterView = view as? UITableViewHeaderFooterView {
            headerFooterView.contentView.backgroundColor = UIConfig.generalColor.white
        } else {
            view.backgroundColor = UIConfig.generalColor.white
        }
    }
}

extension CategoryVCBrandView {
    
    class TableViewCell: UITableViewCell {
        let title = TableViewCell.createLabel()
        
        static func createLabel() -> UILabel {
            let label = UILabel()
            
            label.textColor = UIConfig.generalColor.selected
            label.font = UIConfig.generalFont(15)
            label.backgroundColor = UIConfig.generalColor.white
            
            return label
        }
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(title)
            
            title.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.centerY.equalTo(self.contentView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    static func configureDataSource() -> RxTableViewSectionedReloadDataSource<SectionModel<String, ProductBrandItem>> {
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ProductBrandItem>>()
        
        dataSource.configureCell = { (_, tv, ip, item: ProductBrandItem) in
            let cell:TableViewCell = tv.dequeueReusableCell(at: ip)
            
            cell.title.text = item.name
            
            return cell
        }
        
        dataSource.titleForHeaderInSection = { return $0[$1].model }
        dataSource.sectionForSectionIndexTitle = { return $2 }
        dataSource.sectionIndexTitles = { return $0.sectionModels.map({ $0.model }) }
        
        return dataSource
    }
}
