//
//  RootVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class RootVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addChildViewController(contentController)
        
        self.view.addSubview(contentController.view)
        
        contentController.view.snp.fillToSuperView()
    }
    
//    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
//        super.init(nibName: nil, bundle: nil)
//    }
    
    let contentController:UIViewController
    
    init(contentController:UIViewController) {
//        super.init(contentViewController: contentController,
//                   leftMenuViewController: nil,
//                   rightMenuViewController: nil)
        
//        self.delegate = self
        self.contentController = contentController
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}




