//
//  OrderDetailTimeLineView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import SnapKit
import RxCocoa
import RxSwift

class OrderDetailTimeLineView: UITableView {
    
    var items:[OrderTimeLineItem] = []
    
    init() {
        super.init(frame: .zero,
                   style: .plain)
        
        self.tableHeaderView = UIView(x: 0, y: 0, w: 1, h: 35)
        self.separatorStyle = .none
        self.rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = 67
        self.allowsSelection = false
        self.allowsMultipleSelection = false
        self.sectionFooterHeight = 0
        self.sectionHeaderHeight = 0
        self.delegate = self
        self.dataSource = self
        self.registerCell(cellClass: TimeLineTableViewCell.self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OrderDetailTimeLineView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(at: indexPath) as TimeLineTableViewCell
        
        cell.set(item: items[indexPath.row],
                 row: indexPath.row,
                 rowCount: items.count)
        
        return cell
    }
}

extension OrderDetailTimeLineView {
    class TimeLineTableViewCell: UITableViewCell {
        
        let circle:UIView
        
        let line:UIView
        
        var labelConstraint:Constraint!
        
        let iconImageView:UIImageView
        
        let label:UILabel
        
        let subLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            circle = ViewFactory.view(color: UIConfig.generalColor.selected)
            circle.layer.cornerRadius = 5
            circle.layer.masksToBounds = true
            
            line = ViewFactory.view(color: UIConfig.generalColor.backgroudGray)
            
            iconImageView = UIImageView()
            
            label = UILabel()
            
            subLabel = UILabel()
            
            super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubviews(label, subLabel)
            
            iconImageView.backgroundColor = UIConfig.generalColor.white
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            self.label.font = UIConfig.generalFont(15)
            self.label.textColor = UIConfig.generalColor.selected
            self.label.highlightedTextColor = UIConfig.generalColor.red
            self.label.backgroundColor = UIConfig.generalColor.white
            self.label.numberOfLines = 0
            
            self.subLabel.textColor = UIConfig.generalColor.selected
            self.subLabel.highlightedTextColor = UIConfig.generalColor.red
            self.subLabel.backgroundColor = UIConfig.generalColor.white
            self.subLabel.numberOfLines = 0
            
            self.contentView.insertSubview(line, at: 0)
            self.contentView.addSubview(circle)
            self.contentView.addSubview(iconImageView)
            
            circle.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(25)
                maker.size.equalTo(CGSize(width:10, height:10))
            }
            
            iconImageView.snp.remakeConstraints { (maker) in
                maker.center.equalTo(circle)
            }
            
            self.label.snp.makeConstraints({ (maker) in
                maker.top.equalTo(self.contentView)
                maker.left.equalTo(circle.snp.right).offset(25)
                maker.centerY.equalTo(circle)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-10)
            })
            
            self.subLabel.snp.makeConstraints({ (maker) in
                maker.left.equalTo(circle.snp.right).offset(25)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-10)
                labelConstraint = maker.top.equalTo(self.label.snp.bottom).offset(10).constraint
                maker.bottom.equalTo(self.contentView).offset(-35)
            })
        }
        
        func set(item:OrderTimeLineItem, row:Int, rowCount:Int) {
            
            let highlighted = row == 0
            
            self.label.isHighlighted = highlighted
            iconImageView.isHidden = !highlighted
            self.circle.isHidden = highlighted
            
            self.label.text = item.description
            iconImageView.image = item.icon
            
            
            if let logistics = item.logistics, logistics.count > 0 {
                
                let attributedString = NSMutableAttributedString()
                
                for (index, one) in logistics.enumerated() {
                    let info = NSAttributedString(string: one.0 + "\n",
                                                  font: UIConfig.generalFont(12),
                                                  textColor: UIConfig.generalColor.unselected,
                                                  lineSpace: 10)
                    
                    attributedString.append(info)
                    
                    var timeString = one.1?.generalFormartString ?? ""
                    
                    if index < (logistics.count - 1) {
                        timeString += "\n"
                    }
                    
                    let time = NSAttributedString(string: timeString,
                                                  font: UIConfig.arialFont(11),
                                                  textColor: UIConfig.generalColor.whiteGray,
                                                  lineSpace: 15)
                    
                    attributedString.append(time)
                }
                
                labelConstraint.update(offset: 20)
                
                self.subLabel.attributedText = attributedString
            } else if let date = item.date?.generalFormartString {
                self.subLabel.attributedText = NSAttributedString(string: date,
                                                                          font: UIConfig.arialFont(12),
                                                                          textColor: highlighted ? UIConfig.generalColor.red : UIConfig.generalColor.unselected)
                labelConstraint.update(offset: 10)
            } else {
                self.subLabel.text = ""
            }
            
            if row == 0 && rowCount == 1 {
                self.line.isHidden = true
            } else if row == 0 {
                self.line.isHidden = false
                self.line.snp.remakeConstraints({ (maker) in
                    maker.width.equalTo(2)
                    maker.centerX.equalTo(circle)
                    maker.top.equalTo(circle.snp.centerY)
                    maker.bottom.equalTo(self.contentView)
                })
            } else if row >= (rowCount - 1) {
                self.line.isHidden = false
                self.line.snp.remakeConstraints({ (maker) in
                    maker.width.equalTo(2)
                    maker.centerX.equalTo(circle)
                    maker.bottom.equalTo(circle.snp.centerY)
                    maker.top.equalTo(self.contentView)
                })
            } else {
                self.line.isHidden = false
                self.line.snp.remakeConstraints({ (maker) in
                    maker.width.equalTo(2)
                    maker.centerX.equalTo(circle)
                    maker.bottom.equalTo(self.contentView)
                    maker.top.equalTo(self.contentView)
                })
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}




