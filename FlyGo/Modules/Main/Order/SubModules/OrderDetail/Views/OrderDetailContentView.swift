//
//  OrderDetailContentView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import EZSwiftExtensions

class OrderDetailContentView: UIView {
    
    let tableView:UITableView
    
    var cellList:[UITableViewCell] = []
    
//    let rightButton = ViewFactory.redButton(title: "去付款")
//    
//    let leftButton = ViewFactory.grayButton(title: "取消订单")
    
    let clickEvent:PublishSubject<(OrderItem, ActionType)>
    
    weak var parentViewController:OrderDetailVC? = nil
    
    init() {
        clickEvent = PublishSubject()
        
        tableView = UITableView(frame: .zero,
                                style: .grouped)
        tableView.separatorStyle = .none
        tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        tableView.allowsSelection = false
        tableView.allowsMultipleSelection = false
        tableView.sectionFooterHeight = 10
        tableView.estimatedSectionFooterHeight = 10
        tableView.sectionHeaderHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
        tableView.tableFooterView = ViewFactory.groupedTableViewEmptyView()
        tableView.setInset(top: 0, bottom: 0)
        
        super.init(frame: .zero)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.addSubview(tableView)
        
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self).inset(UIEdgeInsets.zero)
        }
        
//        rightButton.addTarget(self,
//                              action: #selector(OrderDetailContentView.buttonClick(sender:)),
//                              for: UIControlEvents.touchUpInside)
//        
//        self.addSubview(rightButton)
//        
//        leftButton.tag = 1
//        leftButton.addTarget(self,
//                             action: #selector(OrderDetailContentView.buttonClick(sender:)),
//                             for: UIControlEvents.touchUpInside)
//        
//        self.addSubview(leftButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buttonClick(sender:UIButton) {
        let index = sender.tag
        
        let currentType = actionType[index]
        
        var messageString:String? = nil
        
        switch currentType {
        case .cancel:
            messageString = "确认取消订单"
        case .confirm:
            messageString = "是否要确认收货"
        case .reorder:
            messageString = "确认重新下单，试试其他飞哥？"
        default:
            break
        }
        
        if let alertMessageString = messageString {
            let alertVC = UIAlertController(title: nil,
                                            message: alertMessageString,
                                            preferredStyle: .alert)
            
            alertVC.addAction(UIAlertAction(title: "否",
                                            style: .cancel,
                                            handler: nil))
            
            alertVC.addAction(UIAlertAction(title: "是",
                                            style: .default,
                                            handler: { [weak self](_) in
                                                self?.clickEvent.onNext((self!.orderItem!, currentType))
            }))
            
            parentViewController?.presentVC(alertVC)
        } else {
            clickEvent.onNext((self.orderItem!, currentType))
        }
    }
    
    /// Item
    var bottomButtons = [UIButton]()
    
    var actionType:[ActionType] = [] {
        didSet {
            for button in bottomButtons {
                button.removeFromSuperview()
            }
            
            if actionType.count > 0 {
                tableView.setInset(top: 110, bottom: 49)
                
                bottomButtons = actionType.map({ $0.button })
                
                self.view.addSubviews(bottomButtons)
                
                var leftButton:UIButton? = nil
                
                for button in bottomButtons {
                    
                    button.addTarget(self,
                                     action: #selector(OrderDetailContentView.buttonClick(sender:)),
                                     for: UIControlEvents.touchUpInside)
                    
                    if let `leftButton` = leftButton {
                        button.snp.makeConstraints({ (maker) in
                            maker.bottom.equalTo(self)
                            maker.height.equalTo(49)
                            maker.left.equalTo(leftButton.snp.right)
                            maker.width.equalTo(leftButton)
                        })
                    } else {
                        button.snp.makeConstraints({ (maker) in
                            maker.left.bottom.equalTo(self)
                            maker.height.equalTo(49)
                        })
                    }
                    
                    leftButton = button
                }
                
                leftButton?.snp.makeConstraints({ (maker) in
                    maker.right.equalTo(self)
                })
//                self.addSubview(rightButton)
//                
//                rightButton.snp.remakeConstraints({ (maker) in
//                    maker.left.bottom.right.equalTo(self).offset(0).priority(999)
//                    maker.height.equalTo(49)
//                })
//                
//                let currentType = actionType[0]
//                
//                rightButton.setTitle(currentType.title,
//                                     for: .normal)
//                rightButton.isSelected = currentType.isSelected
////                rightButton.setBackgroundColor(currentType.tintColor,
////                                               forState: .normal)
//                
//                if actionType.count > 1 {
//                    self.addSubview(leftButton)
//                    
//                    leftButton.snp.remakeConstraints({ (maker) in
//                        maker.left.bottom.equalTo(self).offset(0).priority(1000)
//                        maker.right.equalTo(rightButton.snp.left).offset(0).priority(1000)
//                        maker.height.equalTo(49)
//                        maker.width.equalTo(rightButton)
//                    })
//                    
//                    let currentType = actionType[1]
//                    
//                    leftButton.setTitle(currentType.title,
//                                        for: .normal)
//                    leftButton.isSelected = currentType.isSelected
////                    leftButton.setBackgroundColor(currentType.tintColor,
////                                                   forState: .normal)
//                }
            } else {
                tableView.setInset(top: 106.5, bottom: 0)
            }
        }
    }
    
    /// Address
    var contaicter:Contaicter? {
        didSet {
            addressCell = contaicter.flatMap({ AddressTableViewCell(item: $0) })
            
            self.reloadData()
        }
    }
    
    func reloadData() {
        
//        let lock = NSRecursiveLock()
//        
//        lock.lock()
        
        cellList = []
        
        if let cell = addressCell {
            cellList.append(cell)
        }
        
        if let cell = orderCell {
            cellList.append(cell)
        }
        
        if let cell = flygoCell {
            cellList.append(cell)
        }
        
        if let cell = paymentCell {
            cellList.append(cell)
        }
        
//        tableView.reloadData()
        
//        lock.unlock()
    }
    
    var addressCell:AddressTableViewCell? = nil

    /// Order Info
    var orderItem:OrderItem? {
        didSet {
            orderCell = orderItem
                .flatMap({
                    OrderTableViewCell(item:$0,
                                       productPressed:{
                                        [weak self] (item) in
                                 self?.parentViewController?.pushDetailVC(item: item)
                    })
                })
            
            self.reloadData()
        }
    }
    
    var orderCell:OrderTableViewCell? = nil
    
    
    /// Flygo
    var flygoStatus:FlygoStatus? {
        didSet {
            if let status = flygoStatus {
                switch status {
                case .own(let item):
                    let _flygoCell = SelfFlygoTableViewCell(item: item)
                    
                    flygoCell = _flygoCell
                case .normal(let item):
                    let _flygoCell = FlygoTableViewCell(item: item)
                    
                    _flygoCell.imButton.addTarget(self,
                                                  action: #selector(OrderDetailContentView.toChatVCClick(sender:)),
                                                  for: .touchUpInside)
                    _flygoCell.flymanInfoButton.addTarget(self,
                                                          action: #selector(OrderDetailContentView.toFlaymanInfoVCClick(sender:)),
                                                          for: .touchUpInside)
                    
                    flygoCell = _flygoCell
                case .waiting:
                    flygoCell = FlygoWaitingTableViewCell()
                }
            }
            
            self.reloadData()
        }
    }
    
    var flygoCell:UITableViewCell? = nil
    
    func toFlaymanInfoVCClick(sender:Any?) {
        guard let item = flygoStatus?.item else {
            return
        }
        
        let toast = parentViewController?.showLoading()
        
        API
            .VendorOrder
            .getOrder(to: item.userId)
            .responseModel({ (collection:API.VendorOrderCollection) in
                
                let list = collection.orderList
                
                let orderList = list.filter({ $0.customerId == item.userId }).map({ $0.orderformId ?? "" })
                
                ez.runThisInMainThread {
                    if orderList.count < 1 {
                        toast?.displayLabel(text: "这是我自己的订单^_^")
                    } else {
                        let vc = FlygoInfoVC(userId: item.userId,
                                             nickName: item.nickName,
                                             avatar: item.avatar,
                                             orders: orderList)
                        
                        toast?.dismiss(animated: true, completion: {
                            self
                                .parentViewController?
                                .navigationController?
                                .pushViewController(vc,
                                                    animated: true)
                        })
                        
                        
                    }
                }
            }) { (error) in
                ez.runThisInMainThread {
                    toast?.dismiss(animated: true, completion: nil)
                }
        }
    }
    
    func toChatVCClick(sender:Any?) {
        guard let item = flygoStatus?.item,
            let session = ChatManager.currentSession else {
                return
        }
        
        if item.userId == UserManager.shareInstance.currentId {
            
//            parentViewController?.showToast(text: "这是我自己的订单^_^")
            
            return
        }
        
        if let conversation = session
            .fetchConversationInCache(for: item.userId,
                                      nickName: item.nickName,
                                      avatarPath: item.avatar) {
            let vc = ChatRoomVC(conversation: conversation)
            
            parentViewController?.navigationController?.pushViewController(vc,
                                                                           animated: true)
            
            return
        }
        
        let toast = parentViewController?.showLoading()
        
        session
            .fetchConversationFromServer(for: item.userId,
                                         nickName: item.nickName,
                                         avatarPath: item.avatar) { (result) in
                                            switch result {
                                            case .failture(let error):
                                                toast?.displayLabel(text: error.localizedDescription)
                                            case .success(let conversation):
                                                toast?.dismiss(animated: true,
                                                               completion: {
                                                                let vc = ChatRoomVC(conversation: conversation)
                                                                
                                                                self.parentViewController?.navigationController?.pushViewController(vc,
                                                                                                                                    animated: true)
                                                })
                                            }
        }
    }
    
    /// Payment
    var paymentItem:OrderPaymentItem? {
        didSet {
            if let item = paymentItem {
                paymentCell = PaymentTableViewCell(item: item)
            } else{
                paymentCell = nil
            }
            
            self.reloadData()
        }
    }
    
    var paymentCell:PaymentTableViewCell? = nil
}


extension OrderDetailContentView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellList[indexPath.section]
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
    }
}

extension OrderDetailContentView {
    class PaymentTableViewCell: UITableViewCell {
        
        let label:UILabel
        
        init(item:OrderPaymentItem) {
            label = UILabel()
            
            super.init(style: .default,
                       reuseIdentifier: "PaymentTableViewCell")
            
            var text = "支付方式: " + item.payment
            
            if let orderTime = item.orderTime {
                text += "\n下单时间: " + orderTime.generalFormartString
            }
            
            if let receiveTime = item.receiveTime {
                text += "\n接单时间: " + receiveTime.generalFormartString
            }
            
            text += "\n订单编号: " + item.orderNumber
            
            self.label.attributedText = NSAttributedString(string: text,
                                                                font: UIConfig.arialFont(11),
                                                                textColor: UIConfig.generalColor.labelGray,
                                                                lineSpace: 3)
            self.label.numberOfLines = 0
            
            self.contentView.addSubviews(label)
            
            self.label.snp.makeConstraints({ (maker) in
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(15, 15, 15, 15))
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension OrderDetailContentView {
    class FlygoWaitingTableViewCell: UITableViewCell {
        init() {
            super.init(style: .default,
                       reuseIdentifier: "FlygoWaitingTableViewCell")
            
            let avatar = UIView()
            
            avatar.backgroundColor = UIConfig.generalColor.labelGray
            avatar.layer.cornerRadius = 25
            avatar.layer.masksToBounds = true
            
            let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                          textColor: UIConfig.generalColor.labelGray)
            label.text = "等待飞哥中..."
            
            self.contentView.addSubviews(avatar, label)
            
            avatar.snp.makeConstraints { (maker) in
                maker.size.equalTo(CGSize(width:50, height:50))
                maker.top.left.equalTo(self.contentView).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
            
            label.snp.makeConstraints({ (maker) in
                maker.centerY.equalTo(avatar)
                maker.left.equalTo(avatar.snp.right).offset(15)
            })
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension OrderDetailContentView {
    class SelfFlygoTableViewCell: UITableViewCell {
        
        init(item:SelfflymanItem) {
            
            let imageView = UIImageView()
            
            imageView.backgroundColor = UIConfig.generalColor.white
            imageView.setAvatar(url: item.avatar)
            
            
            let nameLabel = ViewFactory.label(font: UIConfig.generalFont(12),
                                              textColor: UIConfig.generalColor.labelGray)
            
            nameLabel.text = item.nickName
            
            super.init(style: .default,
                       reuseIdentifier: "FlygoTableViewCell")
            
            self.contentView.addSubviews(imageView, nameLabel)
            
            imageView.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(self.contentView).offset(15)
                maker.size.equalTo(CGSize(width:40, height:40))
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
            
            nameLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(imageView.snp.right).offset(15)
                maker.centerY.equalTo(imageView)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension OrderDetailContentView {
    class FlygoTableViewCell: UITableViewCell {
        let imButton = UIButton(type: .custom)
        
        let flymanInfoButton = UIButton(type: .custom)
        
        init(item:FlygoItem) {
            
            let imageView = UIImageView()
            
            imageView.backgroundColor = UIConfig.generalColor.white
            imageView.setAvatar(url: item.avatar)
            
            
            let nameLabel = ViewFactory.label(font: UIConfig.generalFont(12),
                                              textColor: UIConfig.generalColor.labelGray)
            
            nameLabel.text = item.nickName
            
            
            imButton.setImage(UIImage(named:"icon_contact"),
                              for: .normal)
            
            let flyLabel = ViewFactory.label(font: UIConfig.arialFont(13),
                                             textColor: UIConfig.generalColor.selected)
            
            flyLabel.lineBreakMode = .byWordWrapping
            flyLabel.adjustsFontSizeToFitWidth = true
            flyLabel.text = "航班号:  \(item.FLTNO)"
            
            let airportLabel = ViewFactory.label(font: UIConfig.arialFont(13),
                                             textColor: UIConfig.generalColor.selected)
            
            var attrString = NSMutableAttributedString(string: "起降机场:  \(item.takeOffAirport)  ",
                font: UIConfig.arialFont(13),
                textColor: UIConfig.generalColor.selected)
            
            let icon = UIImage(named:"icon_until")
            
            let attach = NSTextAttachment()
            
            attach.image = icon
            
            let arrowAttrString = NSAttributedString(attachment: attach)
            
            attrString.append(arrowAttrString)
            attrString.append(NSAttributedString(string: "  \(item.landAirport)",
                                                 font: UIConfig.arialFont(13),
                                                 textColor: UIConfig.generalColor.selected))
            
            airportLabel.attributedText = attrString
            airportLabel.lineBreakMode = .byWordWrapping
            airportLabel.adjustsFontSizeToFitWidth = true
            
            let timeLabel = ViewFactory.label(font: UIConfig.arialFont(13),
                                              textColor: UIConfig.generalColor.selected)
            
            attrString = NSMutableAttributedString(string: "起降时间:  \((item.takeOffTime?.generalFormartString ?? ""))  ",
                font: UIConfig.arialFont(13),
                textColor: UIConfig.generalColor.selected)
            attrString.append(arrowAttrString)
            attrString.append(NSAttributedString(string: "  \((item.landTime?.generalFormartString ?? ""))",
                font: UIConfig.arialFont(13),
                textColor: UIConfig.generalColor.selected))
            
            timeLabel.attributedText = attrString
            timeLabel.lineBreakMode = .byWordWrapping
            timeLabel.adjustsFontSizeToFitWidth = true
            
            super.init(style: .default,
                       reuseIdentifier: "FlygoTableViewCell")
            
            self.contentView.addSubviews(imageView, nameLabel, imButton, flyLabel, airportLabel, timeLabel, flymanInfoButton)
            
            imageView.snp.makeConstraints { (maker) in
                maker.left.top.equalTo(self.contentView).offset(15)
                maker.size.equalTo(CGSize(width:40, height:40))
            }
            
            nameLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(imageView.snp.right).offset(15)
                maker.centerY.equalTo(imageView)
            }
            
            imButton.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.centerY.equalTo(nameLabel)
            }
            
            flyLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(imageView)
                maker.top.equalTo(imageView.snp.bottom).offset(15)
            }
            
            airportLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(flyLabel)
                maker.top.equalTo(flyLabel.snp.bottom).offset(15)
            }
            
            timeLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(airportLabel)
                maker.top.equalTo(airportLabel.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
            
            flymanInfoButton.snp.makeConstraints { (maker) in
                maker.edges.equalTo(imageView).inset(UIEdgeInsetsMake(-10, -10, -10, -10))
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension OrderDetailContentView {
    class OrderTableViewCell: UITableViewCell {
        
        init(item:OrderItem, productPressed:@escaping (OrderProduct) -> Void) {
            let titleLabel = ViewFactory.label(font: UIConfig.generalSemiboldFont(14),
                                           textColor: UIConfig.generalColor.selected)
            
            titleLabel.text = item.type.description
            
            let timeLabel = ViewFactory.label(font: UIConfig.arialFont(12),
                                          textColor: UIConfig.generalColor.labelGray)
            
            timeLabel.text = item.time?.generalFormartString
            
            let productZone = UIView()
            
            let chargeLabel = UILabel()
            
            chargeLabel.numberOfLines = 4
            
            /// cast
            var attributedString = NSMutableAttributedString(string: "运费：\(item.deliver <= 0 ? "免费" : "￥" + item.deliver.description)\n",
                font: UIConfig.generalFont(12),
                textColor: UIConfig.generalColor.titleColor,
                lineSpace: 5,
                alignment: .right)
            
            attributedString.append(NSAttributedString(string: "服务费：\(item.tip <= 0 ? "免费" : "￥" + item.tip.priceString)\n",
                font: UIConfig.generalFont(12),
                textColor: UIConfig.generalColor.titleColor,
                lineSpace: 5,
                alignment: .right))
            
            attributedString.append(NSAttributedString(string: "优惠折扣：",
                                                       font: UIConfig.generalFont(12),
                                                       textColor: UIConfig.generalColor.titleColor,
                                                       lineSpace: 5,
                                                       alignment: .right))
            
            
            attributedString
                .append(NSAttributedString(string: "￥\(item.coupon.priceString)\n",
                    font: UIConfig.generalFont(12),
                    textColor: UIConfig.generalColor.red,
                    lineSpace: 5,
                    alignment: .right))
            
            chargeLabel.attributedText = attributedString
            
            /// total
            let totalLabel = UILabel()
            
            let totalPrice = item.total
            
            attributedString = NSMutableAttributedString(string: "共计 \(item.number) 件商品 小计:",
                font: UIConfig.generalFont(12),
                textColor: UIConfig.generalColor.titleColor,
                lineSpace: 5,
                alignment: .right)
            
            let currency = Currency(unit: "￥",
                                    value: totalPrice)
            
            attributedString.append(currency.attributedString(color: UIConfig.generalColor.red,
                                                              intSize: 15,
                                                              floatSize: 12))
            
            totalLabel.attributedText = attributedString
            
            /// line
            let bottomLine = ViewFactory.line()
            
            super.init(style: .default,
                       reuseIdentifier: "OrderTableViewCell")
            
            self.contentView.addSubview(titleLabel)
            self.contentView.addSubview(productZone)
            self.contentView.addSubview(timeLabel)
            self.contentView.addSubview(chargeLabel)
            self.contentView.addSubview(totalLabel)
            self.contentView.addSubview(bottomLine)
            
            titleLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.top.equalTo(self.contentView).offset(15)
            }
            
            timeLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.centerY.equalTo(titleLabel)
            }
            
            productZone.snp.makeConstraints { (maker) in
                maker.left.equalTo(titleLabel)
                maker.right.equalTo(timeLabel)
                maker.top.equalTo(titleLabel.snp.bottom).offset(15)
            }
            
            chargeLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(productZone.snp.bottom).offset(10)
//                maker.bottom.equalTo(self.contentView).offset(-15)
                maker.right.equalTo(productZone)
            }
            
            bottomLine.snp.makeConstraints { (maker) in
                maker.top.equalTo(chargeLabel.snp.bottom).offset(15)
                maker.left.equalTo(chargeLabel)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.height.equalTo(0.5)
            }
            
            totalLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(bottomLine.snp.right)
                maker.top.equalTo(bottomLine.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-15)
            }
            
            /// add product list
            for product in item.products {
                let prevView = productZone.subviews.last
                
                let productView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
                
                productView.addTapGesture(action: { (_) in
                    productPressed(product)
                })
                
                productZone.addSubview(productView)
                
                let coverView = UIImageView()
                
                coverView.kf.setImage(with: product.imageUrl)
                coverView.backgroundColor = UIConfig.generalColor.white
                coverView.contentMode = .scaleAspectFit
                coverView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
                coverView.layer.borderWidth = 1
                coverView.layer.masksToBounds = true
                
                let titleLabel = UILabel()
                
                titleLabel.backgroundColor = productView.backgroundColor
                titleLabel.numberOfLines = 2
                titleLabel.attributedText = NSAttributedString(string: product.name,
                                                               font: UIConfig.generalFont(13),
                                                               textColor: UIConfig.generalColor.unselected,
                                                               lineSpace: 0)
                titleLabel.lineBreakMode = .byTruncatingTail
                
                let subTitleLabel = ViewFactory.label(font: UIConfig.generalFont(11),
                                                      textColor: UIConfig.generalColor.labelGray,
                                                      backgroudColor: productView.backgroundColor)
                subTitleLabel.text = product.subTitle
                
                let priceLabel = ViewFactory.label(font: UIConfig.arialBoldFont(12),
                                                   textColor: UIConfig.generalColor.red,
                                                   backgroudColor: productView.backgroundColor)
                priceLabel.text = "￥\(product.price.priceString)"
                
                let numLabel = ViewFactory.label(font: UIConfig.generalFont(11),
                                                 textColor: UIConfig.generalColor.labelGray,
                                                 backgroudColor: productView.backgroundColor)
                
                numLabel.attributedText = FlygoUtil.attributedString(number: product.number)
                
                productView.addSubviews(coverView, titleLabel, subTitleLabel, priceLabel, numLabel)
                
                coverView.snp.makeConstraints({ (maker) in
                    maker.left.top.equalTo(productView).offset(10)
                    maker.bottom.equalTo(productView).offset(-10)
                    maker.width.equalTo(75)
                })

                titleLabel.snp.makeConstraints({ (maker) in
                    maker.left.equalTo(coverView.snp.right).offset(10)
                    maker.right.equalTo(productView).offset(-10)
                    maker.top.equalTo(coverView)
                })

                subTitleLabel.snp.makeConstraints({ (maker) in
                    maker.left.right.equalTo(titleLabel)
                    maker.top.equalTo(titleLabel.snp.bottom).offset(1)
                })
                
                priceLabel.snp.makeConstraints({ (maker) in
                    maker.bottom.equalTo(coverView)
                    maker.left.equalTo(subTitleLabel)
                })
                
                numLabel.snp.makeConstraints({ (maker) in
                    maker.bottom.equalTo(coverView)
                    maker.right.equalTo(subTitleLabel)
                })
                
                productView.snp.makeConstraints({ (maker) in
                    maker.left.right.equalTo(productZone)
                    maker.height.equalTo(95)
                })
                
                if let _prevView = prevView {
                    productView.snp.makeConstraints({ (maker) in
                        maker.top.equalTo(_prevView.snp.bottom).offset(5)
                    })
                } else {
                    productView.snp.makeConstraints({ (maker) in
                        maker.top.equalTo(productZone)
                    })
                }
            }
            
            if let last = productZone.subviews.last {
                last.snp.makeConstraints({ (maker) in
                    maker.bottom.equalTo(productZone)
                })
            } else {
                productZone.snp.makeConstraints({ (maker) in
                    maker.height.equalTo(0)
                })
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

    }
}

extension OrderDetailContentView {
    class AddressTableViewCell: UITableViewCell {
        
        static func label() -> UILabel {
            let label                        = UILabel()
            
            label.textColor                  = UIConfig.generalColor.unselected
            label.font                       = UIConfig.generalFont(13)
            label.backgroundColor            = UIConfig.generalColor.white
            
            return label
        }
        
        let nameLabel                        = AddressTableViewCell.label()
        
        let telLabel                         = AddressTableViewCell.label()
        
        let detailLabel                      = AddressTableViewCell.label()
        
        let tagImageView                     = UIImageView(named: "icon_address")
        
        let lineImageView                    = UIImageView(named: "background_rules")
        
        init(item:Contaicter) {
            super.init(style: .default,
                       reuseIdentifier: "AddressTableViewCell")
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            
            self.selectionStyle = .none
            
            tagImageView.backgroundColor     = UIConfig.generalColor.white
            
            detailLabel.numberOfLines = 0
            
            self
                .contentView
                .addSubviews([tagImageView, detailLabel, nameLabel, telLabel, lineImageView])
            
            
            nameLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(tagImageView.snp.right).offset(15)
                maker.top.equalTo(self.contentView).offset(20)
            }
            
            telLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(self.contentView).offset(-15)
                maker.bottom.equalTo(nameLabel)
            }
            
            detailLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(nameLabel.snp.bottom).offset(15)
                maker.left.equalTo(nameLabel)
                maker.right.equalTo(telLabel)
            }
            
            lineImageView.snp.makeConstraints { (maker) in
                maker.left.right.equalTo(self.contentView)
                maker.height.equalTo(lineImageView.image?.size.height ?? 0)
                maker.top.equalTo(detailLabel.snp.bottom).offset(20)
                maker.bottom.equalTo(self.contentView.bottom).offset(0)
            }
            
            tagImageView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.size.equalTo(tagImageView.size)
                maker.centerY.equalTo(detailLabel.snp.top)
            }
            
            telLabel.text = item.phone
            nameLabel.text = item.name
            detailLabel.text = item.address
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
