//
//  OrderDetailContentViewModel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct Contaicter {
    var name:String
    
    var phone:String
    
    var address:String
    
    init(model:API.Order) {
        name = model.contactName
        
        phone = model.contactTel
        
        address = model.contactAddress
    }
}

struct FlygoItem {
    var userId:String
    
    var avatar:URL?
    
    var nickName:String
    
    var FLTNO:String
    
    var takeOffAirport:String
    
    var landAirport:String
    
    var takeOffTime:Date?
    
    var landTime:Date?
    
    init(model:API.Order) {
        userId = model.flymanId
        
        avatar = model.userAvatar
        
        nickName = model.flymanNickName
        
        FLTNO = model.flightNo
        
        takeOffAirport = model.departureAirport
        
        landAirport = model.landingAirport
        
        takeOffTime = model.takeoffTime
        
        landTime = model.fallTime
    }
}

struct SelfflymanItem {
    var avatar:URL?
    
    var nickName:String
    
    init(model:API.Order) {
        avatar = model.userAvatar
        
        nickName = model.flymanNickName
    }
}

enum FlygoStatus {
    case waiting
    case normal(FlygoItem)
    case own(SelfflymanItem)
    
    var item:FlygoItem? {
        switch self {
        case .normal(let item):
            return item
        default:
            return nil
        }
    }
    
    var selfItem:SelfflymanItem? {
        switch self {
        case .own(let item):
            return item
        default:
            return nil
        }
    }
}

struct OrderPaymentItem {
    var payment:String
    
    var orderTime:Date?
    
    var receiveTime:Date?
    
    var orderNumber:String
    
    init(model:API.Order) {
        payment = "支付宝"
        
        orderTime = model.createTime
        
        receiveTime = model.takeTime
        
        orderNumber = model.orderSn
    }
}

class OrderDetailContentViewModel: ViewModel {
    
    let orderId:String
    
    let refreshEvent:PublishSubject<Void>
    
    init(orderId:String) {
        refreshEvent = PublishSubject()
        
        self.orderId = orderId
    }
    
    func bind(to view: OrderDetailContentView) -> DisposeBag? {
        
        let bag = DisposeBag()
        
        let orderId = self.orderId
        
        let dataSet = TableViewDataSet(view.tableView,
                                       delegate:view,
                                       dataSource:view,
                                       finishStyle: .none)
        
        let result = dataSet.refrence(getRequest: {
            return API.OrderCollection.detail(orderId: orderId)
        },
                                      otherRequest:refreshEvent,
                         isEmpty: { $0.orderList.count <= 0 },
                         in: bag)
        
        let contaicter = result.map { (collection) -> Contaicter? in
            if collection.orderList.count < 1 {
                return nil
            }
            
            let order = collection.orderList[0]
            
            return Contaicter(model: order)
        }
        
        let order = result.map { (collection) -> OrderItem? in
            if collection.orderList.count < 1 {
                return nil
            }
            
            let order = collection.orderList[0]
            
            return OrderItem(model: order)
        }
        
        let flygoStatus = result.map { (collection) -> FlygoStatus? in
            if collection.orderList.count < 1 {
                return nil
            }
            
            let order = collection.orderList[0]
            
            switch order.detailsState {
            case .pending:
                return FlygoStatus.waiting
//            case .flygoback, .delivering, .finish, .ordered, .finishAndCommented, .pickupfailed, .flygoexception, .refunded:
//                
//                if order.selfState == 1 {
//                    let item = SelfflymanItem(model: order)
//                    
//                    return FlygoStatus.own(item)
//                }
//                
//                let item = FlygoItem(model: order)
//                
//                return FlygoStatus.normal(item)
            default:
                if order.selfState == 1 {
                    let item = SelfflymanItem(model: order)
                    
                    return FlygoStatus.own(item)
                }
                
                if order.flymanId.length > 0 {
                    let item = FlygoItem(model: order)
                    
                    return FlygoStatus.normal(item)
                }
                
                return nil
            }
        }
        
        let payment = result.map { (collection) -> OrderPaymentItem? in
            if collection.orderList.count < 1 {
                return nil
            }
            
            let order = collection.orderList[0]
            
            return OrderPaymentItem(model: order)
        }
        
        let actionType = result.map { (collection) -> [ActionType] in
            if collection.orderList.count < 1 {
                return []
            }
            
            let orderState = collection.orderList[0].detailsState
            
            let state = {
                (apiOrderState:API.OrderState) -> OrderItemState in
                switch apiOrderState {
                case .obligation:
                    return .obligation
                case .unpayed:
                    return .unpayed
                case .pending:
                    return .pending
                case .refunding:
                    return .refunding
                case .refunded:
                    return .refunded
                case .noorder:
                    return .noorder
                case .ordered:
                    return .ordered
                case .pickupfailed:
                    return .pickupfailed
                case .flygoback:
                    return .flygoback
                case .flygoexception:
                    return .flygoexception
                case .delivering:
                    return .delivering
                case .finish:
                    return .finish
                case .canceling:
                    return .canceling
                case .canceled:
                    return .canceled
                case .finishAndCommented:
                    return .canceled
                }
            }(orderState)
            
            return state.actionType
        }
        
        contaicter.bind { (item) in
            view.contaicter = item
            }.addDisposableTo(bag)
        
        order.bind { (item) in
            view.orderItem = item
            }.addDisposableTo(bag)
        
        payment.bind { (item) in
            view.paymentItem = item
            }.addDisposableTo(bag)
        
        actionType.bind { (item) in
            view.actionType = item
            }.addDisposableTo(bag)
        
        flygoStatus.bind { (status) in
            view.flygoStatus = status
        }.addDisposableTo(bag)
        
        /// pay
        let paymentInfo = PublishSubject<API.OrderPaymentInfo>()
        
        let requestItem = SubmitItem(rootController: view.parentViewController) {
            (model:API.OrderPaymentInfo) in
            paymentInfo.onNext(model)
        }
        
        bag.insert(requestItem)
        
        let payRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.pay })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.repay(orderformId: $0.id) })
            .shareReplay(1)
        
        payRequest
            .bindNext(requestItem)
            .addDisposableTo(bag)
        
        let alipayRequest = paymentInfo
            .flatMapLatest{AlipayManager.payEvent(order: $0.applyPayContent, userInfo:$0.outTradeNo )}
        
        let successItem = NoResSubmitItem(rootController: view.parentViewController) {
            [weak view] in
            NotificationCenter
                .default
                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                      object: [OrderTitleType.all,
                               OrderTitleType.obligation,
                               OrderTitleType.pending])
            
            
            view?.parentViewController?.refresh()
        }
        
        bag.insert(successItem)
        
        let paySuccess = alipayRequest.filter({ $0.isSuccess }).flatMapLatest { (result) -> Observable<PostItem> in
            return API.OrderPaymentInfo.paySuccess(outTradeNo: result.userInfo ?? "")
            }.shareReplay(1)
        
        paySuccess.bindNext(successItem).addDisposableTo(bag)
        
        /// cancel
        let cancelRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.cancel })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.cancel(orderformId: $0.id) })
            .shareReplay(1)
        
        let cancelControl = RequestItem(rootController: view.parentViewController,
                                        successTip: "取消订单成功") {
                                            [weak view] in
                                            NotificationCenter
                                                .default
                                                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                      object: [OrderTitleType.all,
                                                               OrderTitleType.pending,
                                                               OrderTitleType.cancel])
                                            
                                            view?.parentViewController?.refresh()
        }
        
        cancelRequest.bindNext(cancelControl).addDisposableTo(bag)
        
        /// confirm
        let confirmRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.confirm })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.confirm(orderformId: $0.id) })
            .shareReplay(1)
        
        let confirmControl = RequestItem(rootController: view.parentViewController,
                                         successTip: "确认收货成功") {
                                            [weak view] in
                                            NotificationCenter
                                                .default
                                                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                      object: [OrderTitleType.all,
                                                               OrderTitleType.delivering,
                                                               OrderTitleType.finish])
                                            
                                            view?.parentViewController?.refresh()
        }
        
        confirmRequest.bindNext(confirmControl).addDisposableTo(bag)
        
        /// comment
        view
            .clickEvent
            .filter({ $0.1 == ActionType.comment })
            .map({ CommentSubmitVC(orderFormid: $0.0.id) })
            .bind {
                [weak view] (vc) in
                
                vc.uploadFinishCallBack = {
                    view?.parentViewController?.refresh()
                }
                
                view?
                    .parentViewController?
                    .navigationController?
                    .pushViewController(vc,
                                        animated: true)
            }
            .addDisposableTo(bag)
        
        /// reorder
        let reorderRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.reorder })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.reorder(orderformId: $0.id) })
            .shareReplay(1)
        
        let reorderControl = RequestItem(rootController: view.parentViewController,
                                         successTip: "重新下单成功") {
                                            [weak view] in
                                            NotificationCenter
                                                .default
                                                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                      object: [OrderTitleType.all,
                                                               OrderTitleType.pending])
                                            
                                            view?.parentViewController?.refresh()
        }
        
        reorderRequest.bindNext(reorderControl).addDisposableTo(bag)
//
        return bag
    }
}


