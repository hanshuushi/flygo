//
//  OrderDetailTimeLineViewModel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

//enum OrderTimeLineItem {
//    case obligation (Date)
//    case pending(Date)
//    case backing(Date)
//    case waitingForDeliver(Date)
//    case delivering([(String, Date)])
//    case finish(Date)
//    case cancel(Date)
//    

//}

enum OrderTimeLineItem {
    case normal(String, UIImage?, Date?, Bool)
    case deliver(String, UIImage?, [(String, Date?)])
    
    var date:Date? {
        switch self {
        case .normal(_,_,let date,_):
            return date
        default:
            return nil
        }
    }
    
    var description:String {
        switch self {
        case .normal(let title,_,_,_):
            return title
        case .deliver(let title,_,_):
            return title
        }
    }
    
    var icon:UIImage? {
        switch self {
        case .normal(_,let image,_,_):
            return image
        case .deliver(_,let image,_):
            return image
        }
    }
    
    var logistics:[(String, Date?)]? {
        switch self {
        case .deliver(_, _, let logistics):
            return logistics
        default:
            return nil
        }
    }
    
    var isFinish:Bool {
        switch self {
        case .normal(_,_,_,let isFinish):
            return isFinish
        default:
            return false
        }
    }
    
    static func items(with model:API.OrderDetail) -> [OrderTimeLineItem] {
        var items = model.orderStateInfo.map { (info) -> OrderTimeLineItem in
            let title = info.desc.length <= 0 ? "无消息" : info.desc
            
            let image:UIImage? = {
                switch info.detailState! {
                case .obligation:
                    return UIImage(named: "icon_state_waiting_for_payment")
                case .pending:
                    return UIImage(named: "icon_state_waiting_for_order_receiving")
                case .ordered:
                    return UIImage(named: "icon_state_waiting_for_back")
                case .flygoback:
                    return UIImage(named: "icon_state_waiting_for_delivery")
                case .delivering:
                    return UIImage(named: "icon_state_logistics")
                case .finish:
                    return UIImage(named: "icon_state_complete")
                case .finishAndCommented:
                    return UIImage(named: "icon_state_complete")
                default:
                    return UIImage(named: "icon_state_waiting_for_payment")
                }
            }()
            
            let date = info.actionTime
            
            let isFinish = (info.detailState == .finish || info.detailState == .finishAndCommented)
            
            return OrderTimeLineItem.normal(title,
                                            image,
                                            date,
                                            isFinish)
        }
        
        if model.shippingInfo.count > 0 {
            let deliver = OrderTimeLineItem.deliver("物流信息",
                                                    UIImage(named: "icon_state_logistics"),
                                                    model.shippingInfo.sorted(by: { (left, right) -> Bool in
                                                        guard let `left` = left.acceptTime else {
                                                            return false
                                                        }
                                                        
                                                        guard let `right` = right.acceptTime else {
                                                            return true
                                                        }
                                                        
                                                        return left > right
                                                    }).map({ (body) in
                                                        return (body.remark, body.acceptTime)
                                                    }))
            for (index, value) in items.enumerated() {
                if !value.isFinish {
                    items.insert(deliver, at: index)
                    
                    break
                }
            }
        }
        
        return items
    }
    
    static func itemCount(with model:API.OrderDetail) -> Int {
        var count = model.orderStateInfo.count
        
        if model.shippingInfo.count > 0 {
            for (_, value) in model.orderStateInfo.enumerated() {
                if value.detailState != .finish{
                    
                    count += 1
                    
                    break
                }
            }
        }
        
        return count
    }
}

class OrderDetailTimeLineViewModel: ViewModel {
    
    let refreshEvent:PublishSubject<Void>
    
    let orderId:String
    
    init(orderId:String) {
        refreshEvent = PublishSubject()
        
        self.orderId = orderId
    }
    
    typealias TimeLineTableViewCell = OrderDetailTimeLineView.TimeLineTableViewCell
    
    func bind(to view: OrderDetailTimeLineView) -> DisposeBag? {
        
        let bag = DisposeBag()
        
        let orderId = self.orderId
        
        let dataSet = TableViewDataSet(view, delegate:view, dataSource:view, finishStyle: .none)
        
        dataSet.refrence(getRequest: {
            return API.OrderDetail.orderStateInfo(orderformId: orderId)
        },
                         otherRequest:refreshEvent,
                         isEmpty: { OrderTimeLineItem.itemCount(with: $0) <= 0 },
                         in: bag)
            .bind { (detail) in
                view.items = OrderTimeLineItem.items(with: detail)
        }
            .addDisposableTo(bag)
        
        return bag
    }
    
}
