//
//  OrderDetailVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class OrderDetailVC: UIViewController {
    
    let timeLinePresent:Presenter<OrderDetailTimeLineViewModel>
    
    let contentPresent:Presenter<OrderDetailContentViewModel>
    
    var navTabView:NavTabView!
    
    var style = CommonTabViewStyle()
    
    let disposeBag = DisposeBag()
    
    init (orderId:String) {
        let timeLineView = OrderDetailTimeLineView()
        
        timeLineView.setInset(top: 106.5, bottom: 0)
        
        timeLinePresent = Presenter(viewModel: OrderDetailTimeLineViewModel(orderId:orderId),
                                    view: timeLineView)
        
        let contentView = OrderDetailContentView()
        
        contentPresent = Presenter(viewModel: OrderDetailContentViewModel(orderId:orderId),
                                    view: contentView)
        super.init(nibName: nil,
                   bundle: nil)
        
        contentView.parentViewController = self
    }
    
    func refresh() {
        timeLinePresent.viewModel.refreshEvent.onNext()
        contentPresent.viewModel.refreshEvent.onNext()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func pushDetailVC(item:OrderProduct) {
        let detailVM = ProductDetailVM(productId: item.recId)
        
        let detailVC = ProductDetailVC(viewModel: detailVM,
                                       showCart: false)
        
        self.navigationController?.pushViewController(detailVC,
                                                      animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        navTabView = NavTabView()
        
        self.view.addSubview(navTabView)
        
        self.title = "订单详情"
        
        let leftWith = ("详情" as NSString).size(attributes: [
            NSFontAttributeName: style.font
            ]).width
        
        let rightWith = ("状态" as NSString).size(attributes: [
            NSFontAttributeName: style.font
            ]).width
        
        
        let padding = (UIScreen.main.bounds.width - (leftWith + rightWith)) / 4.0
        
        style.titlePadding = padding
        style.titleInset = padding * 2.0
        
        navTabView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0))
        }
        navTabView.navTabStyle = style
        navTabView.dataSource = self
        navTabView.reloadData()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_news"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(OrderDetailVC.showMessage(sender:)))
    }
    
    private let _oneceTokenInLayoutSubviews = NSUUID().uuidString
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.once(token: _oneceTokenInLayoutSubviews) {
            if let navigationBar = self.navigationController?.navigationBar {
                if let rightButton = navigationBar
                    .subviews.map({ $0 as? UIButton })
                    .filter({ $0 != nil })
                    .map({ $0! })
                    .sorted(by: { $0.left >= $1.left }).first {
                    
                    if let imageView = rightButton.imageView {
                        rightButton.bageOffset = CGSize(width: (rightButton.w - imageView.w) / 2.0,
                                                        height: (rightButton.h - imageView.h) / 2.0)
                    }
                    
                    NotificationManager
                        .shareInstance
                        .notificationUnread
                        .drive(onNext: { (bage) in
                            rightButton.bage = bage
                        },
                               onCompleted: nil,
                               onDisposed: nil).addDisposableTo(disposeBag)
                }
            }
        }
    }
    
    func showMessage(sender:Any?) {
        print("showMessage In Order")
        LoginVC.doActionIfNeedLogin {
            let vc = MessageEntryVC()
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        }
    }
}

extension OrderDetailVC: NavTabViewDataSource {
    func titleArrayForNavTabView(_ navTabView:NavTabView) -> [String] {
        return ["详情", "状态"]
    }
    
    func navTabView(_ navTabView:NavTabView, viewAtIndex index:Int) -> UIView {
        if index == 0 {
            return contentPresent.containView.view
        }
        
        return timeLinePresent.containView.view
    }
}
