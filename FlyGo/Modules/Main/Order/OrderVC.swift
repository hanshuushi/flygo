//
//  OrderVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/18.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources


class OrderVC: UIViewController {
    
    let navTabView:NavTabView
    
    let titleArray:[String]
    
    let disposeBag:DisposeBag
    
    let viewModels:[OrderVM]
    
    let defaultPage:Int
    
    init(defaultPage:Int) {
        disposeBag = DisposeBag()
        
        let allType = OrderTitleType.allTypes
        
        titleArray = allType.map{ $0.title }
        
        viewModels = allType.map{ OrderVM(type:$0) }
        
        navTabView = NavTabView()
        navTabView.navTabStyle = CommonTabViewStyle()
        
        self.defaultPage = defaultPage
        
        super.init(nibName: nil,
                   bundle: nil)
        
        navTabView.dataSource = self
        
        let selected = Observable.from(viewModels.map({ $0.orderSelected })).merge()
        
        selected.bind { [weak self](orderId) in
            self?.pushVC(OrderDetailVC(orderId: orderId))
            }
            .addDisposableTo(disposeBag)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        disposeBag = DisposeBag()
        
        let allType = OrderTitleType.allTypes
        
        titleArray = allType.map{ $0.title }
        
        viewModels = allType.map{ OrderVM(type:$0) }
        
        navTabView = NavTabView()
        navTabView.navTabStyle = CommonTabViewStyle()
        
        defaultPage = 0
        
        super.init(nibName: nibNameOrNil,
                   bundle: nibBundleOrNil)
        
        navTabView.dataSource = self
        
        let selected = Observable.from(viewModels.map({ $0.orderSelected })).merge()
        
        selected.bind { [weak self](orderId) in
            self?.pushVC(OrderDetailVC(orderId: orderId))
            }.addDisposableTo(disposeBag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "我的订单"
        
        super.viewDidLoad()
        
        self.view.addSubview(navTabView)
        
        navTabView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0))
        }
        
        navTabView.reloadData()
        
        navTabView.select(titleIndex: defaultPage,
                          animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let vcs = self.navigationController?.viewControllers, vcs.count > 0 {
            let array = vcs.filter({ type(of:$0) != CartConfirmVC.classForCoder() })
            
            if array.count != vcs.count {
                self.navigationController?.setViewControllers(array,
                                                              animated: false)
            }
        }
    }
}

extension OrderVC {
    class OrderItemTableView : UITableView, UITableViewDataSource, UITableViewDelegate, PresenterType {
        
        weak var parentViewController:OrderVC?
        
        let orderSelected:PublishSubject<String>
        
        var items:[OrderItem] 
        
        let disposeBag:DisposeBag
        
        let clickEvent:PublishSubject<(OrderItem, ActionType)>
        
        let viewModel:OrderVM
        
        var bindDisposeBag: DisposeBag?
        
        init(viewModel:OrderVM) {
            self.viewModel = viewModel
            
            disposeBag = DisposeBag()
            
            clickEvent = PublishSubject()
            
            orderSelected = PublishSubject()
            
            items = []
            
            super.init(frame: .zero,
                       style: .grouped)
            
            self.registerCell(cellClass: OrderProductTableViewCell.self)
            self.registerView(viewClass: OrderHeaderView.self)
            self.registerView(viewClass: OrderFooterView.self)
            self.backgroundView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            self.tableHeaderView = ViewFactory.groupedTableViewEmptyView()
            self.tableFooterView = ViewFactory.groupedTableViewEmptyView()
            
            self.separatorStyle = .none
        }
        
        override func removeFromSuperview() {
            bindDisposeBag = nil
            
            super.removeFromSuperview()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return items.count
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            orderSelected.onNext(items[indexPath.section].id)
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if items.count == 0 {
                return 0
            }
            
            return items[section].products.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:OrderProductTableViewCell = tableView.dequeueReusableCell(at: indexPath)
            
            let item = items[indexPath.section].products[indexPath.row]
            
            cell.set(item: item)
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let view:OrderHeaderView = tableView.dequeueReusableView()
            
            view.set(item: items[section])
            
            return view
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let view:OrderFooterView = tableView.dequeueReusableView()
            
            view.set(item: items[section])
            view.tableView = self
            
            return view
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return OrderProductTableViewCell.cellHeight
        }
        
//        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//            return OrderProductTableViewCell.cellHeight
//        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return OrderFooterView.cellHeight(from: items[section].type)
        }
        
//        func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
//            if items.count == 0 {
//                return 0
//            }
//            
//            return OrderFooterView.cellHeight(from: items[section].type)
//        }
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return OrderHeaderView.cellHeight
        }
        
//        func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//            if items.count == 0 {
//                return 0
//            }
//            
//            return OrderHeaderView.cellHeight
//        }
    }
}

extension OrderVC {
    class OrderHeaderView: UITableViewHeaderFooterView {
        
        let paymentLabel:UILabel
        
        let timeLabel:UILabel
        
        static var cellHeight:CGFloat = 44
        
        override init(reuseIdentifier: String?) {
            paymentLabel = UILabel()
            paymentLabel.font = UIConfig.generalSemiboldFont(14)
            paymentLabel.textColor = UIConfig.generalColor.selected
            paymentLabel.backgroundColor = UIConfig.generalColor.white
            
            timeLabel = UILabel()
            timeLabel.font = UIConfig.arialFont(12)
            timeLabel.textColor = UIConfig.generalColor.labelGray
            timeLabel.backgroundColor = UIConfig.generalColor.white
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            self.contentView.addSubview(paymentLabel)
            self.contentView.addSubview(timeLabel)
            
            paymentLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.centerY.equalTo(self.contentView)
            }
            
            timeLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView)
                maker.right.equalTo(self.contentView).offset(-15)
            }
        }
        
        func set(item:OrderItem) {
            paymentLabel.text = item.type.description
            
            timeLabel.text = item.time?.generalFormartString
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class OrderFooterView: UITableViewHeaderFooterView {
        
        static func cellHeight(from type:OrderItemState) -> CGFloat {
            return type.actionType.count == 0 ? 50 : 105
        }
        
        let totalLabel:UILabel
        
        let separator:UIView
        
        let rightButton:UIButton
        
        let leftButton:UIButton
        
        weak var tableView:OrderItemTableView?
        
        override init(reuseIdentifier: String?) {
            totalLabel = UILabel()
            totalLabel.backgroundColor = UIConfig.generalColor.white
            
            separator = UIView()
            separator.backgroundColor = UIConfig.generalColor.lineColor
            
            rightButton = UIButton()
            rightButton.backgroundColor = UIConfig.generalColor.white
            rightButton.layer.borderColor = UIConfig.generalColor.red.cgColor
            rightButton.layer.borderWidth = 1
            rightButton.layer.cornerRadius = 10
            rightButton.layer.masksToBounds = true
            
            leftButton = UIButton()
            leftButton.backgroundColor = UIConfig.generalColor.white
            leftButton.layer.borderColor = UIConfig.generalColor.red.cgColor
            leftButton.layer.borderWidth = 1
            leftButton.layer.cornerRadius = 10
            leftButton.layer.masksToBounds = true
            leftButton.tag = 1
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            rightButton.addTarget(self,
                                    action: #selector(OrderFooterView.buttonClick(sender:)),
                                    for: UIControlEvents.touchUpInside)
            leftButton.addTarget(self,
                                  action: #selector(OrderFooterView.buttonClick(sender:)),
                                  for: UIControlEvents.touchUpInside)
            
            self.contentView.backgroundColor = UIConfig.generalColor.white
            self.contentView.addSubview(totalLabel)
            self.contentView.addSubview(separator)
            self.contentView.addSubview(rightButton)
            self.contentView.addSubview(leftButton)
            
            totalLabel.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(self.contentView.snp.top).offset(17.5)
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            separator.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(45)
                maker.left.right.equalTo(totalLabel)
                maker.height.equalTo(0.5)
            }
            
            rightButton.snp.makeConstraints { (maker) in
                maker.top.equalTo(separator).offset(15)
                maker.size.equalTo(CGSize(width:80, height:25))
                maker.right.equalTo(self.contentView).offset(-15)
            }
            
            leftButton.snp.makeConstraints { (maker) in
                maker.centerY.equalTo(rightButton)
                maker.right.equalTo(rightButton.snp.left).offset(-25)
                maker.size.equalTo(CGSize(width:80, height:25))
            }
            
            let lineView = ViewFactory.view(color: UIConfig.generalColor.backgroudWhite)
            
            self.contentView.addSubview(lineView)
            
            lineView.snp.makeConstraints { (maker) in
                maker.left.bottom.right.equalTo(self.contentView)
                maker.height.equalTo(5)
            }
        }
        
        func buttonClick(sender:UIButton) {
            let index = sender.tag
            
            guard let count = self.orderItem?.type.actionType.count, count > index else {
                return
            }
            
            let actionType = self.orderItem!.type.actionType[index]
            
            var messageString:String? = nil
            
            switch actionType {
            case .cancel:
                messageString = "确认取消订单"
            case .confirm:
                messageString = "是否要确认收货"
            case .reorder:
                messageString = "确认重新下单，试试其他飞哥？"
            default:
                break
            }
            
            if let alertMessageString = messageString {
                let alertVC = UIAlertController(title: nil,
                                                message: alertMessageString,
                    preferredStyle: .alert)
                
                alertVC.addAction(UIAlertAction(title: "否",
                                                style: .cancel,
                                                handler: nil))
                
                alertVC.addAction(UIAlertAction(title: "是",
                                                style: .default,
                                                handler: { [weak self](_) in
                                                    self?.tableView?.clickEvent.onNext((self!.orderItem!, actionType))
                }))
                
                tableView?.parentViewController?.presentVC(alertVC)
            } else {
                tableView?.clickEvent.onNext((self.orderItem!, actionType))
            }
        }
        
        var orderItem:OrderItem?
        
        func set(item:OrderItem) {
            orderItem = item
            
            let attrString = NSMutableAttributedString(string: "总计: ",
                                                       font: UIConfig.generalFont(11),
                                                       textColor: UIConfig.generalColor.titleColor)
            
            attrString.append(NSAttributedString(string: "￥",
                                                 font: UIConfig.arialFont(11),
                                                 textColor: UIConfig.generalColor.red))
            attrString.append(NSAttributedString(string: "\(Int(item.total))",
                                                 font: UIConfig.arialFont(15),
                                                 textColor: UIConfig.generalColor.red))
            
            let tailString = ((Float(item.total) - Float(Int(item.total))).priceString as NSString)
            
            attrString.append(NSAttributedString(string: tailString.substring(from: 1),
                                                 font: UIConfig.arialFont(11),
                                                 textColor: UIConfig.generalColor.red))
            
            totalLabel.attributedText = attrString
            
            if item.type.actionType.count > 0 {
                 let actionType = item.type.actionType[0]
                
                separator.isHidden = false
                
                rightButton.isHidden = false
                rightButton.setAttributedTitle(NSAttributedString(string: actionType.title,
                                                                    font: UIConfig.generalFont(13),
                                                                    textColor: actionType.tintColor),
                                                 for: .normal)
                rightButton.layer.borderColor = actionType.tintColor.cgColor
                
                if item.type.actionType.count > 1 {
                    
                    let actionType = item.type.actionType[1]
                    
                    leftButton.isHidden = false
                    leftButton.setAttributedTitle(NSAttributedString(string: actionType.title,
                                                                      font: UIConfig.generalFont(13),
                                                                      textColor: actionType.tintColor),
                                                   for: .normal)
                    leftButton.layer.borderColor = actionType.tintColor.cgColor
                } else {
                    leftButton.isHidden = true
                }
            } else {
                rightButton.isHidden = true
                leftButton.isHidden = true
                separator.isHidden = true
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}


extension OrderVC {
    class OrderProductTableViewCell : UITableViewCell {
        static let cellHeight:CGFloat = 100
        
        let picCoverImageView:UIImageView
        
        let titleLabel:UILabel
        
        let subTitleLabel:UILabel
        
        let priceLabel:UILabel
        
        let numberLabel:UILabel
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            
            // add borad
            let boardView = UIView()
            
            boardView.backgroundColor = UIConfig.generalColor.backgroudWhite
            
            // add cover
            let picCoverImageView = UIImageView()
            
            self.picCoverImageView = picCoverImageView
            
            boardView.addSubview(picCoverImageView)
            
            picCoverImageView.backgroundColor = UIConfig.generalColor.white
            picCoverImageView.contentMode = .scaleAspectFit
            picCoverImageView.layer.borderColor = UIConfig.generalColor.lineColor.cgColor
            picCoverImageView.layer.borderWidth = 1
            picCoverImageView.snp.makeConstraints { (maker) in
                maker.size.equalTo(CGSize(width:75, height:75))
                maker.left.top.equalTo(boardView).offset(10)
                maker.bottom.equalTo(boardView).offset(-10)
            }
            
            // add title
            let titleLabel = UILabel()
            
            self.titleLabel = titleLabel
            
            boardView.addSubview(titleLabel)
            
            titleLabel.backgroundColor = boardView.backgroundColor
            titleLabel.lineBreakMode = .byTruncatingTail
            titleLabel.numberOfLines = 2
            titleLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(picCoverImageView)
                maker.left.equalTo(picCoverImageView.snp.right).offset(10)
                maker.right.equalTo(boardView).offset(-10)
            }
            
            // add sub title
            let subTitleLabel = UILabel()
            
            self.subTitleLabel = subTitleLabel
            
            boardView.addSubview(subTitleLabel)
            
            subTitleLabel.backgroundColor = boardView.backgroundColor
            subTitleLabel.lineBreakMode = .byTruncatingTail
            subTitleLabel.numberOfLines = 1
            subTitleLabel.font = UIConfig.generalFont(11)
            subTitleLabel.textColor = UIConfig.generalColor.labelGray
            subTitleLabel.snp.makeConstraints { (maker) in
                maker.top.equalTo(titleLabel.snp.bottom).offset(1)
                maker.left.equalTo(picCoverImageView.snp.right).offset(10)
                maker.right.equalTo(boardView).offset(-10)
            }
            
            // add price label
            let priceLabel = UILabel()
            
            self.priceLabel = priceLabel
            
            boardView.addSubview(priceLabel)
            
            priceLabel.backgroundColor = boardView.backgroundColor
            priceLabel.numberOfLines = 1
            priceLabel.font = UIConfig.arialBoldFont(12)
            priceLabel.textColor = UIConfig.generalColor.red
            priceLabel.snp.makeConstraints { (maker) in
                maker.left.equalTo(picCoverImageView.snp.right).offset(10)
                maker.bottom.equalTo(picCoverImageView)
            }
            
            
            // add number label
            let numberLabel = UILabel()
            
            self.numberLabel = numberLabel
            
            boardView.addSubview(numberLabel)
            
            numberLabel.backgroundColor = boardView.backgroundColor
            numberLabel.numberOfLines = 1
            numberLabel.snp.makeConstraints { (maker) in
                maker.right.equalTo(boardView).offset(-10)
                maker.bottom.equalTo(picCoverImageView)
            }
            
            super.init(style: style,
                       reuseIdentifier: reuseIdentifier)
            
            self.selectionStyle = .none
            
            self.contentView.addSubview(boardView)
            
            boardView.snp.makeConstraints { (maker) in
                maker.left.equalTo(self.contentView).offset(15)
                maker.right.equalTo(self.contentView).offset(-15)
                maker.top.equalTo(self.contentView)
            }
        }
        
        func set(item:OrderProduct) {
            picCoverImageView.kf.setImage(with: item.imageUrl)
            
            titleLabel.attributedText = NSAttributedString(string: item.name,
                                                           font: UIConfig.generalFont(13),
                                                           textColor: UIConfig.generalColor.unselected,
                                                           lineSpace: 0)
            titleLabel.lineBreakMode = .byTruncatingTail
            
            subTitleLabel.text = item.subTitle
            
            priceLabel.text = "￥\((item.price).priceString)"
            
            let numberAttrString = NSMutableAttributedString (string: "x",
                                                              font: UIConfig.arialFont(12),
                                                              textColor: UIConfig.generalColor.labelGray)
            
            numberAttrString.append(NSAttributedString(string: "\(item.number)", font: UIConfig.arialFont(12), textColor: UIConfig.generalColor.labelGray))
            
            numberLabel.attributedText = numberAttrString
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension OrderVC: NavTabViewDataSource {
    func navTabView(_ navTabView: NavTabView, viewAtIndex index: Int) -> UIView {
        let tableView =  OrderItemTableView(viewModel: viewModels[index])
//
        tableView.parentViewController = self
        tableView.setInset(top: 106.5, bottom: 0)
        tableView.bind()
//
        return tableView
    }
    
    func titleArrayForNavTabView(_ navTabView: NavTabView) -> [String] {
        return titleArray
    }
}
