//
//  OrderVM.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/22.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


enum OrderTitleType: Int {
    case all = 0
    case obligation = 1
    case pending = 2
    case delivering = 3
    case finish = 4
    case cancel = 5
    
    var title:String {
        switch self {
        case .all:
            return "全部"
        case .obligation:
            return "待付款"
        case .pending:
            return "待接单"
        case .delivering:
            return "待收货"
        case .finish:
            return "待评价"
        case .cancel:
            return "退款"
        }
    }
    
    static var allTypes:[OrderTitleType] {
        return [.all, .obligation, .pending, .delivering, .finish, .cancel]
    }
}

enum ActionType: Int {
    case pay = 0
    case cancel = 1
    case confirm = 2
    case reorder = 3
    case comment = 4
    
    var title:String {
        switch self {
        case .pay:
            return "去支付"
        case .cancel:
            return "取消订单"
        case .confirm:
            return "确认收货"
        case .reorder:
            return "重新下单"
        case .comment:
            return "去评价"
        }
    }
    
    var tintColor:UIColor {
        switch self {
        case .cancel:
            return UIConfig.generalColor.labelGray
        default:
            return UIConfig.generalColor.red
        }
    }
    
    var button:UIButton {
        switch self {
        case .cancel:
            return ViewFactory.grayButton(title: title)
        default:
            return ViewFactory.redButton(title: title)
        }
    }
}

enum OrderItemState : Int {
    /// 订单已生成,待付款
    case obligation = 1
    /// 付款超时,订单已取消
    case unpayed = 2
    /// 付款成功,等待飞哥接单
    case pending = 3
    /// 申请退款,待审核
    case refunding = 4
    /// 已退款
    case refunded = 5
    /// 接单超时,待退款
    case noorder = 6
    /// 飞哥已接单
    case ordered = 7
    /// 飞哥提货失败,待退款
    case pickupfailed = 8
    /// 飞哥已到达
    case flygoback = 9
    /// 飞哥出现异常,待退款
    case flygoexception = 10
    /// 已发货
    case delivering = 11
    /// 交易完成，未评价
    case finish = 12
    /// 订单已取消,待退款
    case canceling = 13
    /// 订单已取消,交易关闭
    case canceled = 14
    /// 已评价
    case commented = 15
    
    
    
    static func state(from apiOrderState:API.OrderState) -> OrderItemState {
        switch apiOrderState {
        case .obligation:
            return .obligation
        case .unpayed:
            return .unpayed
        case .pending:
            return .pending
        case .refunding:
            return .refunding
        case .refunded:
            return .refunded
        case .noorder:
            return .noorder
        case .ordered:
            return .ordered
        case .pickupfailed:
            return .pickupfailed
        case .flygoback:
            return .flygoback
        case .flygoexception:
            return .flygoexception
        case .delivering:
            return .delivering
        case .finish:
            return .finish
        case .canceling:
            return .canceling
        case .canceled:
            return .canceled
        case .finishAndCommented:
            return .commented
        }
    }
    
    var description:String {
        switch self {
        case .obligation:
            return "订单已生成,待付款"
        case .unpayed:
            return "付款超时,订单已取消"
        case .pending:
            return "付款成功,等待飞哥接单"
        case .refunding:
            return "申请退款,待审核"
        case .refunded:
            return "已退款"
        case .noorder:
            return "接单超时,待退款"
        case .ordered:
            return "飞哥已接单"
        case .pickupfailed:
            return "飞哥提货失败,待退款"
        case .flygoback:
            return "飞哥已到达"
        case .flygoexception:
            return "飞哥出现异常,待退款"
        case .delivering:
            return "已发货"
        case .finish:
            return "待评价"
        case .canceling:
            return "订单已取消,待操作"
        case .canceled:
            return "订单已取消"
        case .commented:
            return "已评价"
        }
    }
    
    var actionType:[ActionType] {
        let rawValue = self.rawValue
        
        if rawValue == 1 {
            return [ActionType.pay]
        } else if rawValue == 3 || rawValue == 6 || rawValue == 8 || rawValue == 10 || rawValue == 13 {
            
            var array = [ActionType.cancel]
            
            
            if rawValue == 8 || rawValue == 10 || rawValue == 13 {
                array.append(ActionType.reorder)
            }
            
            return array
        } else if rawValue == 11 {
            return [ActionType.confirm]
        } else if rawValue == 12 {
            return [ActionType.comment]
        }
        
        return []
    }
}

struct OrderProduct {
    
    var name:String
    
    var imageUrl:URL?
    
    var price:Float
    
    var subTitle:String
    
    var number:Int = 0
    
    var recId:String
    
    var tipPrice:Float
    
    var deliverPrice:Float
    
    var coupouValue:Float
    
    init(model:API.OrderSubproduct) {
        name = model.name
        
        imageUrl = model.img
        
        price = model.cnyPrice
        
        subTitle = model.productRecStandard
        
        number = model.num
        
        recId = model.productRecId
        
        tipPrice = model.serviceCharge
        
        coupouValue = model.deductionAmt
        
        deliverPrice = 0.0
    }
}

struct OrderItem {
    
    var id:String
    
    var type:OrderItemState
    
    var time:Date?
    
    var tip:Float
    
    var deliver:Float
    
    var coupon:Float
    
    var total:Float
    
    var products:[OrderProduct]
    
    var number:Int
    
    init(model:API.Order) {
        self.type = OrderItemState.state(from: model.detailsState)
        
        id = model.orderformId

        time = model.stateTime
        
        products = model.subOrders.map({ OrderProduct(model:$0) })
        
        tip = products.reduce(0, { $0 + $1.tipPrice })
        
        deliver = products.reduce(0, { $0 + $1.deliverPrice })
        
        coupon = products.reduce(0, { $0 + $1.coupouValue })
        
        total = model.sumPrice
        
        number = products.reduce(0, { $0 + $1.number })
    }
}

let OrderVMRefreshNotification = "OrderVMRefreshNotification"

class OrderVM: ViewModel {
    
    let orderSelected:PublishSubject<String>
    
    let refreshEvent:PublishSubject<Void>
    
    let type:OrderTitleType
    
    init(type:OrderTitleType = .all) {
        self.type = type
        
        orderSelected = PublishSubject()
        
        refreshEvent = PublishSubject()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(OrderVM.refresh(notification:)),
                                               name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                               object: nil)
    }
    
    @objc
    func refresh(notification:NSNotification) {
//        if let val = notification.object as? OrderTitleType, val == self.type {
//            refreshEvent.onNext()
//        } else if let array = notification.object as? [OrderTitleType],  array.contains(self.type) {
//            refreshEvent.onNext()
//        }
        refreshEvent.onNext()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func bind(to view: OrderVC.OrderItemTableView) -> DisposeBag? {
        let type = self.type
        
        let dataSet = TableViewDataSet(view,
                                       delegate: view,
                                       dataSource: view,
                                       finishStyle: .gray)
        let bag =  dataSet.refrence(getRequestWithPage: { (page) in
            return API.OrderCollection.orderList(state: type.rawValue,
                                                 page: page)
        },
                                    otherRequest:refreshEvent,
                                    listCallBack:{ [weak view] (items) in
                                        view?.items = items.map({ OrderItem(model: $0) })
        })
        
        /// selected
        view.orderSelected.bindNext(orderSelected).addDisposableTo(bag)
        
        /// pay
        let paymentInfo = PublishSubject<API.OrderPaymentInfo>()
        
        let requestItem = SubmitItem(rootController: view.parentViewController) {
            (model:API.OrderPaymentInfo) in
            paymentInfo.onNext(model)
        }
        
        bag.insert(requestItem)
        
        let payRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.pay })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.repay(orderformId: $0.id) })
            .shareReplay(1)
        
        payRequest
            .bindNext(requestItem)
            .addDisposableTo(bag)
        
        let alipayRequest = paymentInfo
            .flatMapLatest{AlipayManager.payEvent(order: $0.applyPayContent, userInfo:$0.outTradeNo )}
        
        let successItem = NoResSubmitItem(rootController: view.parentViewController) {
                                        NotificationCenter
                                            .default
                                            .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                  object: [OrderTitleType.all,
                                                           OrderTitleType.obligation,
                                                           OrderTitleType.pending])
                                        
        }
        
        bag.insert(successItem)
        
        let paySuccess = alipayRequest.filter({ $0.isSuccess }).flatMapLatest { (result) -> Observable<PostItem> in
            return API.OrderPaymentInfo.paySuccess(outTradeNo: result.userInfo ?? "")
            }.shareReplay(1)
        
        paySuccess.bindNext(successItem).addDisposableTo(bag)
        
        
        /// cancel
        let cancelRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.cancel })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.cancel(orderformId: $0.id) })
            .shareReplay(1)
        
        let myType = self.type
        
        let cancelControl = RequestItem(rootController: view.parentViewController,
                                        successTip: "取消订单成功") {
                                            NotificationCenter
                                                .default
                                                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                      object: [OrderTitleType.all,
                                                               myType,
                                                               OrderTitleType.cancel])
                                            
        }
        
        cancelRequest.bindNext(cancelControl).addDisposableTo(bag)
        
        /// confirm
        let confirmRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.confirm })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.confirm(orderformId: $0.id) })
            .shareReplay(1)
        
        let confirmControl = RequestItem(rootController: view.parentViewController,
                                        successTip: "确认收货成功") {
                                            NotificationCenter
                                                .default
                                                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                      object: [OrderTitleType.all,
                                                               myType,
                                                               OrderTitleType.finish])
        }
        
        confirmRequest.bindNext(confirmControl).addDisposableTo(bag)
        
        /// comment
        view
            .clickEvent
            .filter({ $0.1 == ActionType.comment })
            .map({ CommentSubmitVC(orderFormid: $0.0.id) })
            .bind {
                [weak view] (vc) in
                view?
                    .parentViewController?
                    .navigationController?
                    .pushViewController(vc,
                                        animated: true)
        }
            .addDisposableTo(bag)
        
        /// reorder
        let reorderRequest = view
            .clickEvent
            .filter({ $0.1 == ActionType.reorder })
            .map({ $0.0 })
            .flatMapLatest({ API.OrderPaymentInfo.reorder(orderformId: $0.id) })
            .shareReplay(1)
        
        let reorderControl = RequestItem(rootController: view.parentViewController,
                                         successTip: "重新下单成功") {
                                            NotificationCenter
                                                .default
                                                .post(name: NSNotification.Name(rawValue: OrderVMRefreshNotification),
                                                      object: [OrderTitleType.all,
                                                               myType,
                                                               OrderTitleType.pending])
        }
        
        reorderRequest.bindNext(reorderControl).addDisposableTo(bag)
        
        return bag
    }
    
}
