//
//  ChooseVCImageFilter.c
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

#import "ChooseVCImageFilter.h"


void ergodicPixels (const uint8_t* bytes, size_t row, size_t width, size_t height, size_t col, size_t bpr, size_t bytes_per_pixel, PointFrame *frame, NSMutableArray *pointArray) {
    
    const uint8_t* pixel = &bytes[row * bpr + col * bytes_per_pixel];
    
    const int alpha = pixel[3];
    
    if (alpha == 0) {
//        printf("a0");
        return;
    }
    
    CGPoint currentPoint = CGPointMake(row, col);
    
    for (NSValue *value in pointArray) {
        CGPoint p = [value CGPointValue];
        
        if (CGPointEqualToPoint(p, currentPoint)) {
//            printf("is in");
            return;
        }
    }
    
    NSValue *val = [NSValue valueWithCGPoint:currentPoint];
    
    [pointArray addObject:val];
    
    if (row < frame.top) {
        frame.top = row;
    }
    
    if (row > frame.bottom) {
        frame.bottom = row;
    }
    
    if (col < frame.left) {
        frame.left = col;
    }
    
    if (col > frame.right) {
        frame.right = col;
    }
    
    // up
    if (row > 0) {
//        printf("up(x:%ld,y%ld):(", col,row);
        ergodicPixels(bytes, row - 1, width, height, col, bpr, bytes_per_pixel, frame, pointArray);
//        printf(")");
    }
    
    // bottom
    if (row < (height - 1)) {
//        printf("bottom(x:%ld,y%ld):(", col,row);
        ergodicPixels(bytes, row + 1, width, height, col, bpr, bytes_per_pixel, frame, pointArray);
//        printf(")");
    }
    
    // left
    if (col > 0) {
//        printf("left(x:%ld,y%ld):(", col,row);
        ergodicPixels(bytes, row, width, height, col - 1, bpr, bytes_per_pixel, frame, pointArray);
//        printf(")");
    }
    
    // right
    if (col < (width - 1)) {
//        printf("right(x:%ld,y%ld):(", col,row);
        ergodicPixels(bytes, row, width, height, col + 1, bpr, bytes_per_pixel, frame, pointArray);
//        printf(")");
    }
}

@implementation PointFrame
@synthesize left, right, top, bottom;
- (CGRect)frame {
    CGFloat scale = [UIScreen mainScreen].scale;
    
    return CGRectMake(left / scale, top / scale, (right - left + 1) / scale, (bottom - top + 1) / scale);
}

- (BOOL)containPoint:(CGPoint)p {
    
    CGFloat scale = [UIScreen mainScreen].scale;
    
    CGPoint pt = p;
    
    pt.x /= scale;
    pt.y /= scale;
    
    return CGRectContainsPoint(self.frame, pt);
}

@end

@implementation ChooseVCImageFilter

+ (NSArray<NSValue *> *)imageFilter:(NSString *)imageName startPoint:(CGPoint)point{
    UIImage* image = [UIImage imageNamed:imageName];
    
    CGImageRef cgimage = image.CGImage;
    
    size_t width  = CGImageGetWidth(cgimage);
    size_t height = CGImageGetHeight(cgimage);
    
    size_t bpr = CGImageGetBytesPerRow(cgimage);
    size_t bpp = CGImageGetBitsPerPixel(cgimage);
    size_t bpc = CGImageGetBitsPerComponent(cgimage);
    size_t bytes_per_pixel = bpp / bpc;
    
    CGBitmapInfo info = CGImageGetBitmapInfo(cgimage);
    
    NSLog(
          @"\n"
          "===== %@ =====\n"
          "CGImageGetHeight: %d\n"
          "CGImageGetWidth:  %d\n"
          "CGImageGetColorSpace: %@\n"
          "CGImageGetBitsPerPixel:     %d\n"
          "CGImageGetBitsPerComponent: %d\n"
          "CGImageGetBytesPerRow:      %d\n"
          "CGImageGetBitmapInfo: 0x%.8X\n"
          "  kCGBitmapAlphaInfoMask     = %s\n"
          "  kCGBitmapFloatComponents   = %s\n"
          "  kCGBitmapByteOrderMask     = %s\n"
          "  kCGBitmapByteOrderDefault  = %s\n"
          "  kCGBitmapByteOrder16Little = %s\n"
          "  kCGBitmapByteOrder32Little = %s\n"
          "  kCGBitmapByteOrder16Big    = %s\n"
          "  kCGBitmapByteOrder32Big    = %s\n",
          imageName,
          (int)width,
          (int)height,
          CGImageGetColorSpace(cgimage),
          (int)bpp,
          (int)bpc,
          (int)bpr,
          (unsigned)info,
          (info & kCGBitmapAlphaInfoMask)     ? "YES" : "NO",
          (info & kCGBitmapFloatComponents)   ? "YES" : "NO",
          (info & kCGBitmapByteOrderMask)     ? "YES" : "NO",
          (info & kCGBitmapByteOrderDefault)  ? "YES" : "NO",
          (info & kCGBitmapByteOrder16Little) ? "YES" : "NO",
          (info & kCGBitmapByteOrder32Little) ? "YES" : "NO",
          (info & kCGBitmapByteOrder16Big)    ? "YES" : "NO",
          (info & kCGBitmapByteOrder32Big)    ? "YES" : "NO"
          );
    
    CGDataProviderRef provider = CGImageGetDataProvider(cgimage);
    NSData* data = (__bridge id)CGDataProviderCopyData(provider);
    
    const uint8_t* bytes = [data bytes];
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    
    for(size_t row = 0; row < height; row++)
    {
        for(size_t col = 0; col < width; col++)
        {
            const uint8_t* pixel = &bytes[row * bpr + col * bytes_per_pixel];
            
            const int alpha = pixel[3];
            
            if (alpha >= 255) {
                
                BOOL isContain = false;
                
                for (PointFrame *frame in arr) {
                    if ([frame containPoint:CGPointMake(col, row)]) {
                        isContain = true;
                        break;
                    }
                }
                
                if (isContain) {
                    continue;
                }
                
                PointFrame *frame = [[PointFrame alloc] init];
                
                frame.bottom = 0;
                frame.right = 0;
                frame.left = width;
                frame.top = height;
//                printf("Start:\n");
                ergodicPixels(bytes, row, width, height, col, bpr, bytes_per_pixel, frame, [[NSMutableArray alloc] init]);
//                printf("\nFinish");
                [arr addObject:frame];
                
                NSLog(@"frame is %@", NSStringFromCGRect(frame.frame));
            }
        }
        
    }
    
    CGPoint startPoint = CGPointMake(point.x * width, point.y * height);
    
    NSMutableArray *pointArr =  [NSMutableArray array];
    
    NSMutableArray *storeArr = [NSMutableArray array];
    
    while (arr.count > 0) {
        
        PointFrame *closedFrame = nil;
        
        CGFloat distance = CGFLOAT_MAX;
        
        for (PointFrame *one in arr) {
            CGRect frame = one.frame;
            
            CGPoint center = CGPointMake(frame.origin.x + frame.size.width / 2.0, frame.origin.y + frame.size.height / 2.0);
            
            CGFloat currentDistance = pow((center.x - startPoint.x), 2.0) + pow((center.y - startPoint.y), 2.0);
            
            if (currentDistance < distance) {
                distance = currentDistance;
                
                closedFrame = one;
            }
        }
        
        [arr removeObject:closedFrame];
        
        CGRect frame = closedFrame.frame;
        
        [pointArr addObject:[NSValue valueWithCGRect:frame]];
        
        [storeArr addObject:@{@"x":@(frame.origin.x),
                              @"y":@(frame.origin.y),
                              @"width":@(frame.size.width),
                              @"height":@(frame.size.height)}];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fileName =[NSString stringWithFormat:@"%@.plist",imageName];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    [storeArr writeToFile:filePath atomically:YES];
    
    return pointArr;
}

@end


