//
//  ChooseVCImageFilter.h
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

#ifndef ChooseVCImageFilter_h
#define ChooseVCImageFilter_h


#endif /* ChooseVCImageFilter_h */
@import UIKit;

@interface PointFrame : NSObject
@property (nonatomic, assign) size_t top;
@property (nonatomic, assign) size_t right;
@property (nonatomic, assign) size_t bottom;
@property (nonatomic, assign) size_t left;
@property (nonatomic, readonly) CGRect frame;

@end

@interface ChooseVCImageFilter : NSObject

+ (NSArray<NSValue *> *)imageFilter:(NSString *)imageName startPoint:(CGPoint)point;

@end


