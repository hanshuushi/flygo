//
//  ChooseVCLine.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class ChooseVCLine: UIImageView {
    
    var imageName:String! = ""
    
    var pathCount:Int {
        get {
            return self.layer.mask?.sublayers?.count ?? 0
        }
    }
    
    func showPath(_ pathIndex:Int) {
        if let subLayers = self.layer.mask?.sublayers, subLayers.count > 0 {
            for (index, subLayer) in subLayers.enumerated() {
                subLayer.isHidden = (index >= pathIndex)
            }
            
//            print("hidden is [")
            for (index, subLayer) in subLayers.enumerated() {
                subLayer.isHidden = (index >= pathIndex)
//                print("\(subLayer.isHidden),")
            }
//            print("]")
        }
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let path = Bundle.main.bundlePath + "/" + imageName + ".plist"
        
        guard let array = NSArray(contentsOfFile: path) as? [[String:Any]] else {
            NSLog("Error:No Plist Named \(imageName) In Bundle")
            return
        }
        
        let adjustFrame:(CGRect) -> CGRect = {
            (frame:CGRect) in
            
            var _frame = frame
            
            _frame.origin.x -= 1
            _frame.origin.y -= 1
            _frame.size.width += 1
            _frame.size.height += 1
            
            return _frame
        }
        
        let layerArray = array.map { (dict) -> CGRect in
            let x:CGFloat = dict["x"] as? CGFloat ?? 0
            
            let y:CGFloat = dict["y"] as? CGFloat ?? 0
            
            let width:CGFloat = dict["width"] as? CGFloat ?? 0
            
            let height:CGFloat = dict["height"] as? CGFloat ?? 0
            
            return CGRect(x: x,
                          y: y,
                          w: width,
                          h: height)
        }.map { (frame) -> CALayer in
            let subLayer = CALayer()
            
            subLayer.frame = adjustFrame(frame)
            subLayer.backgroundColor = UIColor.white.cgColor
            
            return subLayer
        }
        
        let maskLayer = CALayer()
        
        maskLayer.frame = self.bounds
        
        for subLayer in layerArray {
            
            maskLayer.addSublayer(subLayer)
        }
        
        self.layer.mask = maskLayer
        
//        showPath(34)
    }
    
}
