//
//  ChooseVCTab.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/29.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

enum ChooseVCTabStatus: Int {
    case flygo = 0
    case user = 1
    
    mutating func switchStatus (){
        switch self {
        case .flygo:
            self = .user
        case .user:
            self = .flygo
        }
    }
    
    func getProgressing() -> CGFloat {
        switch self {
        case .flygo:
            return 0
        case .user:
            return 1
        }
    }
}

class ChooseVCTab: UIView {
    
    @IBOutlet weak var markView:UIView! = nil
    
    @IBOutlet weak var sliderImageView:UIView! = nil
    
    @IBOutlet weak var leftLabel:UILabel! = nil
    
    @IBOutlet weak var rightLabel:UILabel! = nil
    
    @IBOutlet weak var gestureZone:UIView! = nil
    
    @IBOutlet weak var button:UIButton! = nil
    
    var moveDistance:CGFloat = 0
    
    var moveLeftPoint:CGFloat = 0, moveRightPoint:CGFloat = 0
    
    private var _moveProgress:CGFloat = 0.0
    
    var status = ChooseVCTabStatus.flygo {
        didSet {
            button.tag = status.rawValue
            
            switch status {
            case .flygo:
                leftLabel.isHighlighted = true
                rightLabel.isHighlighted = false
                
                button.setTitle("开始探索",
                                for: .normal)
            case .user:
                rightLabel.isHighlighted = true
                leftLabel.isHighlighted = false
                
                button.setTitle("全球好物",
                                for: .normal)
            }
        }
    }
    
    var moveProgress:CGFloat {
        get {
            return _moveProgress
        }
        set(newValue) {
            _moveProgress = newValue
            
            let moveDistance = rightLabel.centerX - leftLabel.centerX
            
            markView.center = CGPoint(x: leftLabel.centerX + _moveProgress * moveDistance,
                                      y: sliderImageView.centerY)
            
            status = _moveProgress >= 0.5 ? .user : .flygo
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.status = .user
        
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(ChooseVCTab.sliderTap(sender:)))
        
        gestureZone.addGestureRecognizer(tapGesture)
        
        
        let rightSwipe = UISwipeGestureRecognizer(target: self,
                                             action: #selector(ChooseVCTab.sliderSwipe(sender:)))
        
        rightSwipe.direction = .right
        
        gestureZone.addGestureRecognizer(rightSwipe)
        
        let leftSwipe = UISwipeGestureRecognizer(target: self,
                                                  action: #selector(ChooseVCTab.sliderSwipe(sender:)))
        
        leftSwipe.direction = .left
        
        gestureZone.addGestureRecognizer(leftSwipe)
        
        /// setup button
        let buttonSize = button.size
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: buttonSize.width, height: buttonSize.height),
                                               false,
                                               UIScreen.main.scale)
        
        guard let ctx:CGContext = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            
            return
        }
        
        let path = CGMutablePath()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let array = [UIColor(r: 247, g: 107, b: 98).withAlphaComponent(0.95).cgColor, UIColor(r: 253, g: 60, b: 83).withAlphaComponent(0.95).cgColor] as CFArray
        
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: array,
                                  locations: [0.0, 1.0])
        
        let startPoint = CGPoint(x: buttonSize.width / 2.0, y: 0)
        
        let endPoint = CGPoint(x: buttonSize.width / 2.0 + buttonSize.height * 0.176, y: buttonSize.height)
        
        path.addRect(button.bounds)
        
        ctx.addPath(path)
        ctx.clip()
        ctx.drawLinearGradient(gradient!,
                               start: startPoint,
                               end: endPoint,
                               options: [.drawsBeforeStartLocation, .drawsAfterEndLocation])
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        button.setBackgroundImage(image,
                                  for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        moveProgress = status.getProgressing()
    }
}




// MARK: - 手势事件
extension ChooseVCTab {
    
    /// 点击
    ///
    /// - Parameter sender: 手势
    func sliderTap(sender:Any?) {
        self.status.switchStatus()
        
        UIView.animate(withDuration: 0.125,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: { self.moveProgress = self.status.getProgressing() },
                       completion: nil)
    }
    
    
    /// 手势滑动
    ///
    /// - Parameter sender: 手势
    func sliderSwipe(sender:UISwipeGestureRecognizer) {
        if sender.direction.contains(UISwipeGestureRecognizerDirection.left) && status != .flygo {
            self.status = .flygo
            UIView.animate(withDuration: 0.125,
                           delay: 0,
                           options: .curveEaseInOut,
                           animations: { self.moveProgress = self.status.getProgressing() },
                           completion: nil)
        } else if sender.direction.contains(UISwipeGestureRecognizerDirection.right) && status != .user {
            status = .user
            UIView.animate(withDuration: 0.125,
                           delay: 0,
                           options: .curveEaseInOut,
                           animations: { self.moveProgress = self.status.getProgressing() },
                           completion: nil)
        }
    }
}
