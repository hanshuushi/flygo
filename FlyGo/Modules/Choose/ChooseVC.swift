//
//  ChooseVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import SpriteKit
import CoreGraphics
import RxSwift

fileprivate let config = (
    mapZoomIn: 2.0,
    fallTag: 0.375,
    compressionTag: 0.125,
    reserveTag: 0.625,
    reserveSpring: 0.35,
    sliderSpring: 0.9,
    mapStep: 0.25,
    fly: 0.375,
    flyGap: 0.125
)



class ChooseVC: UIViewController {
    
    @IBOutlet weak var locationTag:UIImageView! = nil
    
    @IBOutlet weak var maskImageView:UIImageView! = nil
    
    @IBOutlet var mapNavigationView:UIView! = nil
    
    @IBOutlet var sliderView:UIView! = nil
    
    @IBOutlet var lineArray: [ChooseVCLine]!
    
    @IBOutlet var flyArray: [UIView]!
    
    var pathTimer:Timer? = nil
    
    let disposeBag = DisposeBag()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setupNotification()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupNotification()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backgroudView = UIView(frame: self.view.bounds)
        
        backgroudView.addSubview(mapNavigationView)
        
        self.view.addSubview(backgroudView)
        
        self.view.addSubview(sliderView)
        
        sliderView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view).offset(10)
            maker.right.bottom.equalTo(self.view).offset(-10)
            maker.height.equalTo(188)
        }
        
        setupRouteObserver(disposeBag: disposeBag)
    }
//    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        DispatchQueue.once(token: _oneceTokenInAppear) {
            /// setting map
            settingMapBeforeAnimation()
            
            /// seting slider
            settingSliderViewBeforeAnimation()
            
            /// setting path
            settingPathViewBeforeAnimation()
        }
    }
    
    private let _oneceTokenInAppear = NSUUID().uuidString
    
    private let _oneceTokenInDidAppear = NSUUID().uuidString
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.once(token: _oneceTokenInDidAppear) {
            
            if WelcomeVC.shouldShowWelcome {
                self.buildMapAction()
            } else {
                let vc = WelcomeVC()
                
                vc.welcomeDismissed = {
                    self.buildMapAction()
                }
                
                self.present(vc,
                             animated: true,
                             completion: nil)
            }
        }
    }
}


// MARK: - Notification
extension ChooseVC {
    func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChooseVC.applicationDidEnterBackground(_:)),
                                               name: NSNotification.Name.UIApplicationDidEnterBackground,
                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(ChooseVC.applicationWillResignActive(_:)),
//                                               name: NSNotification.Name.UIApplicationWillResignActive,
//                                               object: nil)
    }
    
    func applicationDidEnterBackground(_ notification:Notification) {
        /// remove temp view
        while let tempView = self.view.viewWithTag(666) {
            tempView.layer.removeAllAnimations()
            tempView.removeFromSuperview()
        }
        
        /// remove timer and path animation
        pathTimer?.invalidate()
        pathTimer = nil
        
        for fly in flyArray {
            fly.layer.removeAllAnimations()
            fly.isHidden = false
            fly.transform = CGAffineTransform.identity
            fly.alpha = 1
        }
        
        for one in lineArray {
            one.showPath(1000)
        }
        
        /// slide view
        sliderView.layer.removeAllAnimations()
        sliderView.transform = CGAffineTransform.identity
        
        /// map 
        let center = CGPoint(x: self.view.bounds.midX,
                             y: self.view.bounds.midY)
        
        let locationCenter = locationTag.center
        
        let locationSize = locationTag.size
        
        let targetScale:CGFloat = 1.1
        
        maskImageView.layer.removeAllAnimations()
        maskImageView.isHidden = true
        
        mapNavigationView.layer.removeAllAnimations()
        mapNavigationView.isHidden = false
        mapNavigationView.origin = CGPoint(x: (center.x - locationCenter.x + 50) * targetScale,
                                                      y: (center.y - locationCenter.y - locationSize.height / 2.0) * targetScale - 50)
        mapNavigationView.transform = CGAffineTransform.init(scaleX: targetScale, y: targetScale)
        
        /// location tag
        locationTag.layer.removeAllAnimations()
        locationTag.transform = CGAffineTransform.identity
        locationTag.isHidden = false
    }
    
    func applicationWillResignActive(_ notification:Notification) {
        
    }
}

// MARK: - Render Path
extension ChooseVC {
    
    fileprivate func settingPathViewBeforeAnimation() {
        for one in lineArray {
            one.showPath(0)
        }
        
        for one in flyArray {
            one.isHidden = true
            one.transform = CGAffineTransform.init(scaleX: 2.5, y: 2.5)
            one.alpha = 0
        }
        
        if let timer = pathTimer {
            timer.invalidate()
        }
    }
    
    fileprivate func showPath() {
        weak var weakSelf = self
        
        var pathIndex = 5
        
        var maxCount = Int.min, minCount = Int.max
        
        for one in lineArray {
            maxCount = max(maxCount, one.pathCount)
            minCount = min(minCount, one.pathCount)
        }
        
        let offsets = lineArray.map({ $0.pathCount - minCount })
        
        pathTimer = Timer.runThisEvery(
            seconds: 0.05,
            handler: {
                _ in
                
                if let strongSelf = weakSelf {
                    pathIndex += 1
                    
                    for (index, one) in strongSelf.lineArray.enumerated() {
                        one.showPath(pathIndex + offsets[index])
                    }
                    
                    if pathIndex >= maxCount {
                        strongSelf.pathTimer?.invalidate()
                        strongSelf.pathTimer = nil
                        
                        strongSelf.showAirplain()
                    }
                }
        })
    }
    
    fileprivate func showAirplain() {
        for (index, fly) in flyArray.enumerated() {
            UIView.animate(withDuration: config.fly,
                           delay: config.flyGap * TimeInterval(index),
                           options: .curveEaseOut,
                           animations: {
                            fly.isHidden = false
                            fly.transform = CGAffineTransform.identity
                            fly.alpha = 1
            }, completion: nil)
        }
    }
}

// MARK: - Slider
fileprivate extension ChooseVC {
    func settingSliderViewBeforeAnimation() {
        self.sliderView.transform = CGAffineTransform(translationX: 0, y: 200)
    }
    
    func popSliderView() {
        
        weak var weakSelf = self
        
        var animation:((Void) -> Void)! = nil
        
        var completion:((Bool) -> Void)? = nil

        if let image = sliderView.getScreenShot() {
            
            let imageView = UIImageView(image: image)
            
            self.view.addSubview(imageView)
            
            imageView.frame = CGRect(x: 10,
                                     y: self.view.bounds.height - 10 - image.size.height,
                                     width: image.size.width,
                                     height: image.size.height)
            imageView.layer.changeAnchorPoint(CGPoint(x: 0.05, y: 0.05))
            imageView.transform = CGAffineTransform.init(rotationAngle: CGFloat(CGFloat.pi / 4.0 / 4.0))
            imageView.transform.ty = 200
            imageView.tag = 666
            
            animation = {
                imageView.transform = CGAffineTransform.identity
            }
            
            completion = {
                success in
                
                if let strongSelf = weakSelf, success {
                    imageView.removeFromSuperview()
                    strongSelf.sliderView.transform = CGAffineTransform.identity
                }
            }
        } else {
            
            animation = {
                if let strongSelf = weakSelf {
                    strongSelf.sliderView.transform = CGAffineTransform.identity
                }
            }
        }
        
        UIView.animate(withDuration: config.sliderSpring,
                       delay: 0.0,
                       usingSpringWithDamping: CGFloat(config.reserveSpring),
                       initialSpringVelocity: 0.0,
                       options: .curveEaseOut,
                       animations: animation,
                       completion: completion)
        
    }
}

// MARK: - Render Map By SpriteKit
fileprivate extension ChooseVC {
    func settingMapBeforeAnimation() {
        locationTag.isHidden = true
        locationTag.layer.changeAnchorPoint(CGPoint(x: 0.5, y: 1.0))
        
        maskImageView.alpha = 1.5
        maskImageView.isHidden = false
        
        mapNavigationView.center = self.view.bounds.center
        
        let scale = self.view.w / mapNavigationView.w
        
        mapNavigationView.setScale(x: scale, y: scale)
    }
    
    func buildMapAction() {
        // zoom in
        let center = CGPoint(x: self.view.bounds.midX,
                             y: self.view.bounds.midY)
        
        let locationCenter = locationTag.center
        
        let locationSize = locationTag.size
        
        weak var weakSelf = self
        
        let targetScale:CGFloat = 1.1
        
        UIView.animate(withDuration: config.mapZoomIn,
                       delay: 1.0,
                       options: .curveEaseInOut,
                       animations: {
                        if let strongSelf = weakSelf {
                            strongSelf.maskImageView.alpha = 0.0
                            
                            strongSelf.mapNavigationView.origin = CGPoint(x: (center.x - locationCenter.x + 50) * targetScale,
                                                                          y: (center.y - locationCenter.y - locationSize.height / 2.0) * targetScale - 50)
                            strongSelf.mapNavigationView.transform = CGAffineTransform.init(scaleX: targetScale, y: targetScale)
                        }
                        
                        
        },
                       completion:  {
                        success in
                        if let strongSelf = weakSelf, success {
                            strongSelf.fallLocationTag()
                        }
                        
        })
    }
    
    
    func fallLocationTag() {
        locationTag.transform = CGAffineTransform.init(translationX: 0, y: -UIScreen.main.bounds.height)
        
        weak var weakSelf = self
        
        UIView.animate(withDuration: config.fallTag,
                       delay: 0.0,
                       options: .curveEaseIn,
                       animations: {
                        
                        if let strongSelf = weakSelf {
                            strongSelf.locationTag.transform = CGAffineTransform.identity
                            strongSelf.locationTag.isHidden = false
                        }
        },
                       completion: {
            success in
                        if let strongSelf = weakSelf, success {
                            strongSelf.compressionLocationTag()
                        }
        })
    }
    
    func compressionLocationTag() {
        weak var weakSelf = self
        
        UIView.animate(withDuration: config.compressionTag,
                       delay: 0.0,
                       options: .curveEaseOut,
                       animations: {
                        
                        if let strongSelf = weakSelf {
                            strongSelf.locationTag.transform = CGAffineTransform.init(scaleX: 1.0, y: 0.5)
                        }
        },
                       completion: {
                        success in
                        if let strongSelf = weakSelf, success {
                            strongSelf.resumeLocationTag()
                        }
        })
    }
    
    func resumeLocationTag() {
        weak var weakSelf = self
        
        UIView.animate(withDuration: config.reserveTag,
                       delay: 0.0,
                       usingSpringWithDamping:CGFloat(config.reserveSpring),
                       initialSpringVelocity:0,
                       options: .curveEaseInOut,
                       animations: {
                        if let strongSelf = weakSelf {
                            
                            strongSelf.locationTag.transform = CGAffineTransform.identity
                        }
        },
                       completion: nil)
        
        popSliderView()
        
        showPath()
    }
}


// MARK: - Button点击事件
extension ChooseVC {
    @IBAction func enterButtonClick(sender:Any?) {
        
        guard let button = sender as? UIButton, let status = ChooseVCTabStatus(rawValue: button.tag) else {
            return
        }
        
        let switchRootControllerHandler = {
            (viewController:UIViewController) in
            let complete:(Bool) -> Void = {
                success in
                let tration = CATransition()
                
                tration.duration = 0.275
                tration.type = "kCATransitionFade"
                
                UIApplication.shared.keyWindow?.layer.add(tration, forKey: "tration")
                UIApplication.shared.keyWindow?.rootViewController = viewController
            }
            
            guard let captureImage = self.mapNavigationView.superview?.getScreenShot(), let sliderContentImage = self.sliderView.getScreenShot()
                else {
                    
                    complete(true)
                    
                    return
            }
            
            let fakeImageView = UIImageView(image: captureImage)
            
            fakeImageView.frame = self.view.bounds
            fakeImageView.isUserInteractionEnabled = true
            
            self.view.addSubview(fakeImageView)
            
            let sliderImageView = UIImageView(named: "background_action-Box")
            
            sliderImageView.frame = self.sliderView.frame
            
            fakeImageView.addSubview(sliderImageView)
            
            let sliderContentImageView = UIImageView(image: sliderContentImage)
            
            sliderContentImageView.contentMode = .center
            sliderContentImageView.frame = self.sliderView.frame - 10
            sliderContentImageView.clipsToBounds = true
            
            fakeImageView.addSubview(sliderContentImageView)
            
            /// 转场动画
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           options: .curveEaseOut,
                           animations: {
                            sliderImageView.frame = self.view.bounds + 10.0
                            sliderContentImageView.alpha = 0.0
            },
                           completion: complete)
        }
        
        
        switch status {
        case .user:
            switchRootControllerHandler(MainVC.rootController())
        case .flygo:
            print("In Choose VC")
            
            LoginVC.doActionIfNeedLogin {
                
                switchRootControllerHandler(FlygoVC())
            }
        }
    }
}

