//
//  AppDelegate.swift
//  FlyGo
//
//  Created by Latte on 2016/11/9.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIViewController.methodSwizzling()
        
        DictionaryManager.finishLaunching(at: application,
                                          with: launchOptions)
        VersionManager.finishLaunching(at: application,
                                       with: launchOptions)
        WechatManager.finishLaunching(at: application,
                                      with: launchOptions)
        RemoteManager.finishLaunching(at: application,
                                      with: launchOptions)
        AnalyzeManager.finishLaunching(at: application,
                                     with: launchOptions)
        ChatManager.finishLaunching(at: application,
                                    with: launchOptions)
        NotificationManager.finishLaunching(at: application,
                                            with: launchOptions)
        UserManager.finishLaunching(at: application,
                                    with: launchOptions)
        self.window = RouteManager.finishLaunching(at: application,
                                                   with: launchOptions)
//         API.AirportItem.saveDB()
        // Override point for customization after application launch.
        
//        AirportItem.setupAirportList()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        AlipayManager.urlHandler(url: url)
        
        AnalyzeManager.urlHandler(url: url)
        
        return WechatManager.urlHandler(url: url) || RouteManager.shareInstance.urlHandler(url: url)
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        AlipayManager.urlHandler(url: url)
        
        AnalyzeManager.urlHandler(url: url)
        
        return WechatManager.urlHandler(url: url) || RouteManager.shareInstance.urlHandler(url: url)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        AlipayManager.urlHandler(url: url)
        
        AnalyzeManager.urlHandler(url: url)
        
        return WechatManager.urlHandler(url: url) || RouteManager.shareInstance.urlHandler(url: url)
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        RouteManager.application(application,
                                 performActionFor: shortcutItem,
                                 completionHandler: completionHandler)
    }
}


// MARK: -  添加处理 APNs 通知回调方法
extension AppDelegate {
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        NotificationManager.shareInstance.requestNotificationEvent.onNext()
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        RemoteManager.shareInstance.application(application,
                                                didFailToRegisterForRemoteNotificationsWithError: error)
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        NotificationManager.shareInstance.requestNotificationEvent.onNext()
    }
    
    func application(_ application: UIApplication,
                     didRegister notificationSettings: UIUserNotificationSettings) {
        RemoteManager.shareInstance.application(application,
                                                didRegister: notificationSettings)
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        RemoteManager.shareInstance.application(application,
                                                didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
}

