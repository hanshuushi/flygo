//
//  DictionaryManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/6.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper
import EZSwiftExtensions

extension Operation: Disposable {
    public func dispose() {
        self.cancel()
    }
}

fileprivate extension String {
    func contain (pinyin:String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Za-z]+$",
                                                options: .caseInsensitive)
            
            let match = regex.matches(in: pinyin,
                                      options: [],
                                      range: NSMakeRange(0, pinyin.length))
            
            if match.count <= 0 {
                return false
            }
            
            let myPinyin = (self as NSString).pinyin().lowercased()
            
            let lowPinyin = pinyin.lowercased()
            
            return myPinyin.contains(lowPinyin)
        } catch {
            return false
        }
    }
}

class DictionaryManager: Manager {
    
    static let shareInstance = DictionaryManager()
    
    static func finishLaunching (at application:UIApplication,
                                 with launchOptions: [UIApplicationLaunchOptionsKey: Any]?){
        let _ = shareInstance
    }
    
    init() {
        menuList = DictionaryManager.requestHomeMenu().asDriver(onErrorJustReturn: [])
        
        brandList = DictionaryManager.requestBrandList().asDriver(onErrorJustReturn: [])
        
        typeList = DictionaryManager.requestTypeList().asDriver(onErrorJustReturn: [])
        
        searchList = DictionaryManager.requestSearchList().asDriver(onErrorJustReturn: [])
        
        advertisementList = DictionaryManager.requestAdvertisementList().asDriver(onErrorJustReturn: [])
        
        Driver
            .combineLatest(menuList,
                           brandList,
                           typeList,
                           searchList) {
                            (_, _, _, _) -> Void in
                            return Void()
        }
            .drive(onNext: { (_) in
                #if DEBUG
                print("subcribe")
                #endif
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        tipCollection = Observable<(a0:Float, a1:Float, b:Float, k:Float, Y0:Float, P0:Float)>
            .create({ (obs) -> Disposable in
            
                ez.runThisAfterDelay(seconds: 0.1,
                                     after: {
                                        obs.onNext((a0: 100, a1: 367879.44, b: 0.3, k: 0.05, Y0: 20, P0: 0.022))
                })
                
                return Disposables.create()
        })
            .asDriver(onErrorJustReturn: (a0: 100, a1: 367879.44, b: 0.3, k: 0.05, Y0: 20, P0: 0.022))
    }
    
    let menuList:Driver<[HomeMenu]>
    
    let brandList:Driver<[BrandItem]>
    
    let typeList:Driver<[TypeItem]>
    
    let searchList:Driver<[HomeSearchItem]>
    
    let advertisementList:Driver<[AdvertisementItem]>
    
    let disposeBag = DisposeBag()
    
    let tipCollection:Driver<(a0:Float, a1:Float, b:Float, k:Float, Y0:Float, P0:Float)>!
    
    static let queue:OperationQueue = {
        () -> OperationQueue in
        
        let queue = OperationQueue()
        
        queue.maxConcurrentOperationCount = 5
        queue.name = "com.zhiyou.manager.dictionary"
        
        return queue
    }()
    
    static var scheduler:SerialDispatchQueueScheduler {
        return SerialDispatchQueueScheduler(queue: queue.underlyingQueue!,
                                            internalSerialQueueName: queue.name!)
    }
}

extension DictionaryManager {
    func getTipPrice(of sumPrice:Float) -> Driver<Float> {
        return tipCollection.map({ (collection) -> Float in
            if sumPrice < collection.a0 {
                return collection.Y0
            }
            
            if sumPrice > collection.a1 {
                return collection.P0 * sumPrice
            }
            
            return (collection.b - collection.k * log10(sumPrice)) * sumPrice
        })
    }
}

// MARK: - Search List
extension DictionaryManager {
    typealias HomeSearchItem = API.SearchItem
    
    private static let HomeSearchStoreKey = "HomeSearchStoreKey"
    
    private static func store(searchItems:[HomeSearchItem]) {
        queue.addOperation {
            guard let jsonData = searchItems.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData, forKey: HomeSearchStoreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private static func getStoreSerchItems(response:@escaping (([HomeSearchItem]) -> Void)) -> Operation {
        let operation = BlockOperation {
            if let jsonData = UserDefaults.standard.value(forKey: HomeSearchStoreKey) as? Data,
                let jsonString = String(data: jsonData, encoding: .utf8),
                let searchItems = [HomeSearchItem](JSONString: jsonString) {
                response(searchItems)
            } else {
                response([])
            }
        }
        
        queue.addOperation(operation)
        
        return operation
    }
    
    fileprivate static func requestSearchList() -> Observable<[HomeSearchItem]> {
        /// requet cache
        let cache = Observable<[HomeSearchItem]>
            .create { (obs) -> Disposable in
                return getStoreSerchItems(response: { (searchItems) in
                    obs.onNext(searchItems)
                    obs.onCompleted()
                })
            }
            .shareReplay(1)
        
        /// request from network
        let network = Observable<[HomeSearchItem]>
            .create { (obs) -> Disposable in
                let session = API
                    .SearchItem
                    .getSearchList()
                
                session
                    .responseModelList({ (searchItems:[HomeSearchItem]) in
                        store(searchItems: searchItems)
                        
                        obs.onNext(searchItems)
                        obs.onCompleted()
                    })
                    .error({ (error) in
                        obs.onError(error)
                    })
                
                return session
            }
            .retry()
            .shareReplay(1)
        
        return Observable
            .of(cache, network)
            .startWith(Observable.just([]))
            .concat()
            .observeOn(MainScheduler.instance)
            .shareReplay(1)
    }
}

// MARK: - Menu List
extension DictionaryManager {
    typealias HomeMenu = API.HomeMenu
    
    private static let HomeMenuStoreKey = "HomeMenuStoreKey"
    
    private static func store(menus:[HomeMenu]) {
        queue.addOperation {
            guard let jsonData = menus.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData, forKey: HomeMenuStoreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private static func getStoreMenus(response:@escaping (([HomeMenu]) -> Void)) -> Operation {
        
        let operation = BlockOperation {
            if let jsonData = UserDefaults.standard.value(forKey: HomeMenuStoreKey) as? Data,
                let jsonString = String(data: jsonData, encoding: .utf8),
                let menus = [HomeMenu](JSONString: jsonString) {
                response(menus)
            } else {
                response([])
            }
        }
        
        queue.addOperation(operation)
        
        return operation
    }
    
    fileprivate static func requestHomeMenu() -> Observable<[HomeMenu]> {
        
        /// requet cache
        let cache = Observable<[HomeMenu]>
            .create { (obs) -> Disposable in
                return getStoreMenus(response: { (menus) in
                    obs.onNext(menus)
                    obs.onCompleted()
                })
        }
            .shareReplay(1)
        
        /// request from network
        let network = Observable<[HomeMenu]>
            .create { (obs) -> Disposable in
                let session = API
                    .HomeMenu
                    .getMenuList()
                
                session
                    .responseModelList({ (menus:[HomeMenu]) in
                        store(menus: menus)
                        
                        obs.onNext(menus)
                        obs.onCompleted()
                    })
                    .error({ (error) in
                        obs.onError(error)
                    })
                
                return session
        }
            .retry()
            .shareReplay(1)
        
        return Observable
            .of(cache, network)
            .startWith(Observable.just([]))
            .concat()
            .observeOn(MainScheduler.instance)
            .shareReplay(1)
    }
}

// MARK: - Advertisement
extension DictionaryManager {
    typealias AdvertisementItem = API.Advertisement
    
    private static let AdvertisementItemStoreKey = "AdvertisementItemStoreKey"
    
    private static func store(advertisements:[AdvertisementItem]) {
        queue.addOperation {
            guard let jsonData = advertisements.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData, forKey: AdvertisementItemStoreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private static func getStoreAdvertisements(response:@escaping (([AdvertisementItem]) -> Void)) -> Operation {
        let operation = BlockOperation {
            if let jsonData = UserDefaults.standard.value(forKey: AdvertisementItemStoreKey) as? Data,
                let jsonString = String(data: jsonData, encoding: .utf8),
                let advertisements = [AdvertisementItem](JSONString: jsonString) {
                response(advertisements)
            } else {
                response([])
            }
        }
        
        queue.addOperation(operation)
        
        return operation
    }
    
    fileprivate static func requestAdvertisementList() -> Observable<[AdvertisementItem]> {
        /// requet cache
        let cache = Observable<[AdvertisementItem]>
            .create { (obs) -> Disposable in
                return getStoreAdvertisements(response: { (advertisements) in
                    obs.onNext(advertisements)
                    obs.onCompleted()
                })
            }
            .shareReplay(1)
        
        /// request from network
        let network = Observable<[AdvertisementItem]>
            .create { (obs) -> Disposable in
                let session = API
                    .Advertisement
                    .getAdvertisements()
                
                session
                    .responseModelList({ (advertisements:[AdvertisementItem]) in
                        obs.onNext(advertisements)
                        obs.onCompleted()
                    })
                    .error({ (error) in
                        obs.onError(error)
                    })
                
                return session
            }
            .retry()
            .shareReplay(1)
        
        return Observable
            .of(cache, network)
            .startWith(Observable.just([]))
            .concat()
            .observeOn(MainScheduler.instance)
            .shareReplay(1)
    }
}


// MARK: - Brand
extension DictionaryManager {
    typealias BrandItem = API.ProductBrand
    
    private static let BrandItemStoreKey = "BrandItemStoreKey"
    
    private static func store(brands:[BrandItem]) {
        queue.addOperation {
            guard let jsonData = brands.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData, forKey: BrandItemStoreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private static func getStoreBrands(response:@escaping (([BrandItem]) -> Void)) -> Operation {
        let operation = BlockOperation {
            if let jsonData = UserDefaults.standard.value(forKey: BrandItemStoreKey) as? Data,
                let jsonString = String(data: jsonData, encoding: .utf8),
                let brands = [BrandItem](JSONString: jsonString) {
                response(brands)
            } else {
                response([])
            }
        }
        
        queue.addOperation(operation)
        
        return operation
    }
    
    fileprivate static func requestBrandList() -> Observable<[BrandItem]> {
        /// requet cache
        let cache = Observable<[BrandItem]>
            .create { (obs) -> Disposable in
                return getStoreBrands(response: { (brands) in
                    obs.onNext(brands)
                    obs.onCompleted()
                })
            }
            .shareReplay(1)
        
        /// request from network
        let network = Observable<[BrandItem]>
            .create { (obs) -> Disposable in
                let session = API
                    .ProductBrand
                    .getBrandList()
                
                session
                    .responseModelList({ (brands:[BrandItem]) in
                        obs.onNext(brands)
                        obs.onCompleted()
                    })
                    .error({ (error) in
                        obs.onError(error)
                    })
                
                return session
            }
            .retry()
            .shareReplay(1)
        
        return Observable
            .of(cache, network)
            .startWith(Observable.just([]))
            .concat()
            .observeOn(MainScheduler.instance)
            .shareReplay(1)
    }
    
    func searchBrand(by text:String) -> Observable<[BrandItem]> {
        return brandList
            .asObservable()
            .flatMapLatest({ (list) -> Observable<[BrandItem]> in
                if list.count <= 0 {
                    return Observable.just([])
                }
                
                return Observable.create { (observer) -> Disposable in
                    observer.onNext([])
                    
                    let blockOperation = BlockOperation(block: {
                        let searchList = list.filter({ (brand) -> Bool in
                            
                            return brand.displayName.lowercased().contains(text.lowercased())
                        })
                        
                        observer.onNext(searchList)
                        observer.onCompleted()
                    })
                    
                    DictionaryManager.queue.addOperation(blockOperation)
                    
                    return blockOperation
                }
            })
    }
}

// MARK: - Type
extension DictionaryManager {
    typealias TypeItem = API.ProductType
    
    private static let TypeItemStoreKey = "TypeItemStoreKey"
    
    private static func store(types:[TypeItem]) {
        queue.addOperation {
            guard let jsonData = types.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData, forKey: TypeItemStoreKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    private static func getStoreTypes(response:@escaping (([TypeItem]) -> Void)) -> Operation {
        let operation = BlockOperation {
            if let jsonData = UserDefaults.standard.value(forKey: TypeItemStoreKey) as? Data,
                let jsonString = String(data: jsonData, encoding: .utf8),
                let types = [TypeItem](JSONString: jsonString) {
                response(types)
            } else {
                response([])
            }
        }
        
        queue.addOperation(operation)
        
        return operation
    }
    
    fileprivate static func requestTypeList() -> Observable<[TypeItem]> {
        /// requet cache
        let cache = Observable<[TypeItem]>
            .create { (obs) -> Disposable in
                return getStoreTypes(response: { (types) in
                    obs.onNext(types)
                    obs.onCompleted()
                })
            }
            .shareReplay(1)
        
        /// request from network
        let network = Observable<[TypeItem]>
            .create { (obs) -> Disposable in
                let session = API
                    .ProductType
                    .getTypeList()
                
                session
                    .responseModelList({ (types:[TypeItem]) in
                        obs.onNext(types)
                        obs.onCompleted()
                    })
                    .error({ (error) in
                        obs.onError(error)
                    })
                
                return session
            }
            .retry()
            .shareReplay(1)
        
        return Observable
            .of(cache, network)
            .startWith(Observable.just([]))
            .concat()
            .observeOn(MainScheduler.instance)
            .shareReplay(1)
    }
    
    func searchType(by text:String) -> Observable<[TypeItem]> {
        return typeList
            .asObservable()
            .flatMapLatest({ (list) -> Observable<[TypeItem]> in
                if list.count <= 0 {
                    return Observable.just([])
                }
                
                return Observable.create { (observer) -> Disposable in
                    
                    let blockOperation = BlockOperation(block: {
                        let searchList = DictionaryManager.search(typeList: list,
                                                                  text: text)
                        
                        observer.onNext(searchList)
                        observer.onCompleted()
                    })
                    
                    DictionaryManager.queue.addOperation(blockOperation)
                    
                    return blockOperation
                }
            })
    }
    
    static func search(typeList:[TypeItem], text:String) -> [TypeItem] {
        var list = [TypeItem]()
        
        for type in typeList {
            if type.layers != 1 && (type.productTypeName.contains(text) || type.productTypeName.contain(pinyin: text)) {
                list.append(type)
            }
            
            if type.subTypeList.count > 0 {
                let subList = search(typeList: type.subTypeList,
                                     text: text)
                
                if subList.count > 0 {
                    list += subList
                }
            }
        }
        
        return list
    }
    
//    func getAllTypes() -> Observable<[TypeItem]> {
//        func addTypes (type:TypeItem) -> [TypeItem] {
//            
//            var types = [type]
//            
//            for sub in type.subTypeList {
//                types.append(contentsOf: addTypes(type: sub))
//            }
//            
//            return types
//        }
//        
//        return typeList
//            .asObservable()
//            .flatMapLatest({ (list) -> Observable<[TypeItem]> in
//            return Observable.create({ (observer) -> Disposable in
//                observer.onNext([])
//                
//                let blockOperation = BlockOperation(block: {
//                    observer.onNext(list.flatMap({ addTypes(type: $0) }))
//                    observer.onCompleted()
//                })
//                
//                DictionaryManager.queue.addOperation(blockOperation)
//                
//                return blockOperation
//            })
//        })
//    }
//    
//    func getTypesByLayers() -> [[TypeItem]] {
//        var array = Array<[TypeItem]>(repeating: [], count: 3)
//        
//        let types = typeList.value
//        
//        func pickerTypes (types:[TypeItem]) {
//            for one in types {
//                let layer = one.layers - 1
//                
//                array[layer - 1] += [one]
//                
//                if one.subTypeList.count > 0 {
//                    pickerTypes(types: one.subTypeList)
//                }
//            }
//        }
//        
//        pickerTypes(types: types)
//        
//        return array
//    }
    
    func getType (by productTypeId:String) -> Observable<TypeItem?> {
        func get (id productTypeId:String, in array:[TypeItem]) -> TypeItem? {
            for one in array {
                if one.productTypeId == productTypeId {
                    return one
                }
                
                if one.subTypeList.count > 0, let target = get(id: productTypeId,
                                                               in: one.subTypeList) {
                    return target
                }
            }
            
            return nil
        }
        
        return typeList
            .asObservable()
            .flatMapLatest({ (list) -> Observable<TypeItem?> in
                if list.count <= 0 {
                    return Observable.just(nil)
                }
                
                return Observable.create { (observer) -> Disposable in
                    let blockOperation = BlockOperation(block: {
                        observer.onNext(get(id: productTypeId, in: list))
                        observer.onCompleted()
                    })
                    
                    DictionaryManager.queue.addOperation(blockOperation)
                    
                    return blockOperation
                }
            })
    }
    
    func getParent (by productTypeId:String) -> Observable<TypeItem?> {
        func search (id:String, parent:TypeItem?, list:[TypeItem]) -> TypeItem? {
            for one in list {
                if one.productTypeId == id {
                    return parent
                }
                
                if one.subTypeList.count > 0, let target = search(id: id,
                                                                  parent: one,
                                                                  list: one.subTypeList) {
                    return target
                }
                
            }
            
            return nil
        }
        
        return typeList
            .asObservable()
            .flatMapLatest({ (list) -> Observable<TypeItem?> in
                if list.count <= 0 {
                    return Observable.just(nil)
                }
                
                return Observable.create { (observer) -> Disposable in
                    let blockOperation = BlockOperation(block: {
                        observer.onNext(search(id: productTypeId,
                                               parent: nil,
                                               list: list))
                        observer.onCompleted()
                    })
                    
                    DictionaryManager.queue.addOperation(blockOperation)
                    
                    return blockOperation
                }
            })
    }
    
//    func getSubtypes (productTypeId:String) -> [TypeItem] {
//        
//        return getType(by: productTypeId)?.subTypeList ?? []
//    }
    
//    func getBrothers (productTypeId:String) -> [TypeItem] {
//        let list = typeList.value
//        
//        func getType(in list:[TypeItem], by searchId:String) -> [TypeItem] {
//            for one in list {
//                if one.subTypeList.count <= 0 {
//                    continue
//                }
//                
//                if one.subTypeList.filter({ $0.productTypeId == searchId }).count > 0 {
//                    return one.subTypeList
//                }
//                
//                let resp = getType(in: one.subTypeList, by: searchId)
//                
//                if resp.count > 0 {
//                    return resp
//                }
//            }
//            
//            return []
//        }
//        
//        return getType(in: list,
//                       by: productTypeId)
//    }
}

