//
//  SubmitItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift

class NoResSubmitItem: ObserverType, Disposable {
    
    weak var rootController:UIViewController?
    
    let successHandler:(() -> Void)?
    
    init(rootController:UIViewController?,
         successHandler:(() -> Void)? = nil) {
        self.rootController = rootController ?? UIApplication.shared.keyWindow!.rootViewController!
        
        self.successHandler = successHandler
    }
    
    weak var toastController:ToastViewController?
    
    func dispose() {
        toastController?.dismiss(animated: false, completion: nil)
    }
    
    func on(_ event: Event<PostItem>) {
        switch event {
        case .next(let item):
            switch item {
            case .failed( _):
                toastController?.dismiss(animated: true,
                                         completion: successHandler)
            case .requesting:
                if let vc = self.toastController {
                    vc.displayLoading()
                } else {
                    let toastController = ToastViewController()
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .success:
                toastController?.dismiss(animated: true,
                                         completion: successHandler)
            }
        default:
            return
        }
    }
}
