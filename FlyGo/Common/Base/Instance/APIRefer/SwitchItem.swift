//
//  SwitchItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/6/15.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

//
//  SwitchItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift

class SwitchItem: ObserverType, Disposable {
    
    weak var rootController:UIViewController?
    
    init(rootController:UIViewController?) {
        self.rootController = rootController ?? UIApplication.shared.keyWindow!.rootViewController!
    }
    
    weak var toastController:ToastViewController?
    
    func dispose() {
        toastController?.dismiss(animated: false, completion: nil)
    }
    
    func on(_ event: Event<PostItem>) {
        switch event {
        case .next(let item):
            switch item {
            case .failed(let error):
                if let vc = toastController {
                    vc.displayLabel(text: error)
                } else {
                    let toastController = ToastViewController(text: error)
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .requesting:
                if let vc = self.toastController {
                    vc.displayLoading()
                } else {
                    let toastController = ToastViewController()
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .success:
                self.toastController?.dismiss(animated: true, completion: nil)
            }
        default:
            return
        }
    }
}
