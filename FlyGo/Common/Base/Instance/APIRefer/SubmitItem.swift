//
//  SubmitItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift

class SubmitItem<M:ModelType>: ObserverType, Disposable {
    
    weak var rootController:UIViewController?
    
    let successHandler:((M) -> Void)
    
    init(rootController:UIViewController?,
         successHandler:@escaping ((M) -> Void)) {
        self.rootController = rootController ?? UIApplication.shared.keyWindow!.rootViewController!
        
        self.successHandler = successHandler
    }
    
    weak var toastController:ToastViewController?
    
    func dispose() {
        toastController?.dismiss(animated: false, completion: nil)
    }
    
    func on(_ event: Event<APIItem<M>>) {
        switch event {
        case .next(let item):
            switch item {
            case .failed(let error):
                if let vc = toastController {
                    vc.displayLabel(text: error)
                } else {
                    let toastController = ToastViewController(text: error)
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .validating:
                if let vc = self.toastController {
                    vc.displayLoading()
                } else {
                    let toastController = ToastViewController()
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .success(let model):
                toastController?.dismiss(animated: true,
                                         completion: {
                                            [weak self] in
                                     self?.successHandler(model)
                })
            }
        default:
            return
        }
    }
}
