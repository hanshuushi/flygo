//
//  RequestItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift

class RequestItem: ObserverType, Disposable {
    
    weak var rootController:UIViewController?
    
    let successTip:String
    
    let successHandler:(() -> Void)?
    
    init(rootController:UIViewController?,
         successTip:String,
         successHandler:(() -> Void)? = nil) {
        self.rootController = rootController ?? UIApplication.shared.keyWindow!.rootViewController!
        
        self.successTip = successTip
        
        self.successHandler = successHandler
    }
    
    weak var toastController:ToastViewController?
    
    func dispose() {
        toastController?.dismiss(animated: false, completion: nil)
    }
    
    func on(_ event: Event<PostItem>) {
        switch event {
        case .next(let item):
            switch item {
            case .failed(let error):
                if let vc = toastController {
                    vc.displayLabel(text: error)
                } else {
                    let toastController = ToastViewController(text: error)
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .requesting:
                if let vc = self.toastController {
                    vc.displayLoading()
                } else {
                    let toastController = ToastViewController()
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
            case .success:
                if let vc = toastController {
                    vc.displayLabel(text: successTip)
                } else {
                    let toastController = ToastViewController(text: successTip)
                    
                    rootController?.presentVC(toastController)
                    
                    self.toastController = toastController
                }
                
                successHandler?()
            }
        default:
            return
        }
    }
}
