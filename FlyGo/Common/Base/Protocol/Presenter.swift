//
//  Presenter.swift
//  FlyGo
//
//  Created by Latte on 2016/11/9.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol PresenterType : class {
    
    associatedtype VM: ViewModel
    
    var viewModel:VM { get }
    
    var bindDisposeBag:DisposeBag? { get set }
    
    typealias V = VM.V
    
    var containView:V { get }
}

extension PresenterType {
    func bind() {
        bindDisposeBag = viewModel.bind(to: containView)
    }
}

extension PresenterType where Self: View, Self == Self.VM.V {
    var containView:V {
        get {
            return self
        }
    }
}

class Presenter<VM:ViewModel>: PresenterType {
    var bindDisposeBag: DisposeBag? = nil
    
    let viewModel:VM
    
    let containView:VM.V
    
    init (viewModel aViewModel:VM, view aContainView:VM.V) {
        viewModel = aViewModel
        
        containView = aContainView
        
        bind()
    }
}
