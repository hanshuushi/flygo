//
//  AsyncRequest.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/15.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

enum AsyncItem<T> {
    case success(model: T)
    
    case validating
    
    case failed(error: String)
    
    func map<R:ModelType>(_ transform: (T) -> R) -> APIItem<R> {
        switch self {
        case .validating:
            return .validating
        case .failed(let error):
            return .failed(error: error)
        case .success(let model):
            let result = transform(model)
            
            return APIItem<R>.success(model: result)
        }
    }
    
    var model:T? {
        switch self {
        case .success(let model):
            return model
        default:
            return nil
        }
    }
    
    var isSuccess:Bool {
        switch self {
        case .success(_):
            return true
        default:
            return false
        }
    }
    
    var isFinish:Bool {
        switch self {
        case .success(_):
            return true
        case .failed(_):
            return true
        default:
            return false
        }
    }
}
