//
//  ViewModel.swift
//  FlyGo
//
//  Created by Latte on 2016/11/9.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol GeneralSectionRow {
    var name:String { get }
    
    var id:String { get }
}

protocol GeneralSectionItem {
    var title:String { get }
    
    var items:[GeneralSectionRow] { get }
}

struct SelectableItem<Item> {
    
    var item:Item
    
    var isSelected:Bool = false
    
    init (item:Item) {
        self.item = item
    }
}

protocol ViewModel : class {
    associatedtype V: View
    
    func bind(to view:V) -> DisposeBag?
}

extension Date {
    var generalFormartString:String {
        
        let formart = DateFormatter()
        
        formart.dateFormat = "YYYY-MM-dd HH:mm"
        
        return formart.string(from: self)
    }
}
