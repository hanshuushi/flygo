//
//  ViewController.swift
//  FlyGo
//
//  Created by Latte on 2016/11/9.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol View: class {
    var view:UIView! { get }
}

extension UIView: View {
    var view: UIView! {
        return self
    }
}

extension UIViewController: View { }

