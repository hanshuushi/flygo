//
//  CategoryFilterViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/6.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift

class CategoryFilterViewController: UIViewController, PresenterType {
    
    weak var collectionView:UICollectionView!
    
    weak var resetButton:UIButton!
    
    weak var confirmButton:UIButton!
    
    weak var flowLayout:UICollectionViewFlowLayout!
    
    let sections:Variable<[CategorySectionType]> = Variable([])
    
    var values:Variable<[String:String]> = Variable([:])
    
    let disposeBag = DisposeBag()
    
    let viewModel:CategoryFilterViewModel
    
    var bindDisposeBag:DisposeBag? = nil
    
    var allowMutipleSectionSelected:Bool = true
    
    init(viewModel:CategoryFilterViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CategoryFilterViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        // set layout
        let flowLayout = UICollectionViewFlowLayout()
        
        flowLayout.minimumLineSpacing = 5
        flowLayout.minimumInteritemSpacing = 5
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        self.flowLayout = flowLayout
        
        flowLayout.scrollDirection = .vertical
        
        // add collection view
        let collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: flowLayout)
        collectionView.backgroundColor = UIConfig.generalColor.white
        collectionView.registerCell(cellClass: CollectionViewCell.self)
        collectionView.registerView(viewClass: HeaderView.self,
                                    kind: UICollectionElementKindSectionHeader)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 30,
                                                   left: 0,
                                                   bottom: 0,
                                                   right: 0)
        collectionView.contentOffset = CGPoint(x: 0,
                                               y: -30)
        
        sections.asObservable().bind { (_) in
            collectionView.reloadData()
        }.addDisposableTo(disposeBag)
        
        values.asObservable().bind { (_) in
            collectionView.reloadData()
            }.addDisposableTo(disposeBag)
        
        self.collectionView = collectionView
        self.view.addSubview(collectionView)
        
        // add buttons
        let resetButton = UIButton(type: .custom)
        
        resetButton.backgroundColor = UIConfig.generalColor.unselected
        resetButton.setTitle("重置", for: .normal)
        resetButton.setTitleColor(UIConfig.generalColor.white, for: .normal)
        resetButton.titleLabel?.font = UIConfig.generalFont(15)
        resetButton.addTarget(self,
                              action: #selector(CategoryFilterViewController.resetButtonClick(_:)),
                              for: .touchUpInside)
        
        self.resetButton = resetButton
        self.view.addSubview(resetButton)
        
        let confirmButton = UIButton(type: .custom)
        
        confirmButton.backgroundColor = UIConfig.generalColor.red
        confirmButton.setTitle("确认", for: .normal)
        confirmButton.setTitleColor(UIConfig.generalColor.white, for: .normal)
        confirmButton.titleLabel?.font = UIConfig.generalFont(15)
        confirmButton.addTarget(self,
                                action: #selector(CategoryFilterViewController.confirmButtonClick(_:)),
                                for: .touchUpInside)
        
        self.confirmButton = confirmButton
        self.view.addSubview(confirmButton)
        
        bind()
        
    }
    
    func confirmButtonClick(_ sender:Any?) {
        self.navigationController?.dismiss(animated: true,
                                           completion: nil)
    }
    
    func resetButtonClick(_ sender:Any?) {
        values.value = [:]
        
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let width = self.view.bounds.w
        
        let contentWidth = width - 20
        
        collectionView.frame = CGRect(x: 0,
                                      y: 0,
                                      w: width,
                                      h: self.view.bounds.height - 44)
        
        resetButton.frame = CGRect(x: 0,
                                   y: collectionView.bottom,
                                   w: width / 2.0,
                                   h: 44)
        
        confirmButton.frame = CGRect(x: width / 2.0,
                                     y: collectionView.bottom,
                                     w: width / 2.0,
                                     h: 44)
        
        flowLayout.itemSize = CGSize(width: (contentWidth - 10) / 3.0,
                                     height: 44)
        flowLayout.footerReferenceSize = CGSize(width: width, height: 20)
        flowLayout.headerReferenceSize = CGSize(width: width, height: 21)
    }
}

extension CategoryFilterViewController {
    class HeaderView: UICollectionReusableView {
        let label = UILabel()
        
        let button = UIButton(type: .custom)
        
        var enterMoreHandler:((Void) -> Void)? = nil
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.backgroundColor = UIConfig.generalColor.white
            self.addSubview(label)
            self.addSubview(button)
            
            label.textColor = UIConfig.generalColor.unselected
            label.font = UIConfig.generalSemiboldFont(11)
            label.backgroundColor = self.backgroundColor
            
            button.setImage(UIImage(named:"icon_more-brand"), for: .normal)
            button.sizeToFit()
            button.addTarget(self,
                             action: #selector(HeaderView.enterMore),
                             for: UIControlEvents.touchUpInside)
        }
        
        var item:CategorySectionType? {
            didSet {
                label.text = item?.title
                
                label.sizeToFit()
                
                guard let _item = item else {
                    button.isHidden = true
                    
                    return
                }
                
                button.isHidden = !(_item.displayLimitCount > 0 && _item.items.count > _item.displayLimitCount)
            
                adjustLayout()
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            adjustLayout()
        }
        
        func enterMore () {
            enterMoreHandler?()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func adjustLayout () {
            label.sizeToFit()
            
            label.origin = CGPoint(x: 10, y: 0)
            
            button.centerY = label.centerY
            button.right = self.bounds.width - 10
        }
    }
    
    class CollectionViewCell: UICollectionViewCell {
        let label = UILabel()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.contentView.backgroundColor = UIConfig.generalColor.backgroudGray
            self.contentView.addSubview(label)
            
            label.textColor = UIConfig.generalColor.selected
            label.highlightedTextColor = UIConfig.generalColor.white
            label.numberOfLines = 2
            label.lineBreakMode = .byWordWrapping
            label.font = UIConfig.generalSemiboldFont(11)
            label.backgroundColor = self.contentView.backgroundColor
            label.textAlignment = .center
        }
        
        override var isSelected: Bool {
            didSet {
                label.isHighlighted = isSelected
                
                self.contentView.backgroundColor = isSelected ? UIConfig.generalColor.red : UIConfig.generalColor.backgroudGray
                
                label.backgroundColor = self.contentView.backgroundColor
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var item:GeneralSectionRow? {
            didSet {
                label.text = item?.name
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            label.frame = CGRect(x: 7,
                                 y: 7,
                                 w: self.contentView.bounds.width - 14,
                                 h: self.contentView.bounds.height - 14)
        }
    }
}

extension CategoryFilterViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections.value[section].itemCount
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view:HeaderView = collectionView.dequeueReusableView(kind: UICollectionElementKindSectionHeader,
                                                                 indexPath: indexPath)
        
        view.item = sections.value[indexPath.section]
        
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CollectionViewCell = collectionView.dequeueReusableCell(at: indexPath)
        
        cell.item = sections.value[indexPath.section].items[indexPath.row]
        
        if let val = values.value[sections.value[indexPath.section].key], val == cell.item?.id {
            cell.isSelected = true
        } else {
            cell.isSelected = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let section = sections.value[indexPath.section]
        
        let item = sections.value[indexPath.section].items[indexPath.row]
        
        if values.value[section.key] != item.id {
            if !allowMutipleSectionSelected {
                values.value = [section.key:item.id]
            } else {
                values.value[section.key] = item.id
            }
        } else {
            values.value[section.key] = nil
        }
        
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let section = sections.value[indexPath.section]
        
        values.value[section.key] = nil
        
        self.collectionView.reloadData()
    }
}
