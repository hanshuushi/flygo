//
//  CategoryFilterDropDownItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/6.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class CategoryKeywordType : ProductFilterItemType {
    
    var viewModel:CategoryFilterType.ViewModel!
    
    var key = "filter"
    
    var dataValue:Any? = nil
    
    var title:String = "筛选"
    
    var image:UIImage? = UIImage(named: "icon_pulldown_normal")
    
    var highlightedImage = UIImage(named: "icon_pulldown_selected")
    
    var allowMultiple:Bool = true
    
    var itemNames:[String] = []
    
    var values:[Int] = [0]
    
    var syncHandlers:[(Void) -> Void] = []
    
    let disposeBag = DisposeBag()
    
    var itemStyle:DropDownMenuItemPopStyle {
        
        weak var weakSelf = self
        
        return .custom { () in
            if let strongSelf = weakSelf {
                
                let vc = CategoryFilterViewController(viewModel: strongSelf.viewModel)
                
                let nc = CommonNavigationVC(rootViewController: vc)
                
                SliderPopViewController(nc).present()
            }
        }
    }

    init(keyword:String) {
        self.viewModel = CategoryFilterType.ViewModel(keyword: keyword)
        
        viewModel.confirmValue.asObservable().bind(onNext: {
            [weak self] (val) in
            
            if let ss = self {
                var dict = [String:String]()
                
                if let brand = val["brandId"] {
                    dict["brandId"] = brand
                }
                
                ss.dataValue = dict
                
                for one in ss.syncHandlers {
                    one ()
                }
            }
            
        })
            .addDisposableTo(disposeBag)
    }
}

class CategoryFilterType : ProductFilterItemType {
    var currentType:ProductTypeItem {
        didSet {
            if oldValue.id != currentType.id {
                self.dataValue = nil
                
                self.values = []
                
                setupViewModel()
            }
        }
    }
    
    var viewModel:ViewModel!
    
    func setupViewModel() {
        
        self.viewModel = ViewModel(currentType: currentType)
        
        viewModel.confirmValue.asObservable().bind(onNext: {
            [weak self] (val) in
            
            if let ss = self {
                var dict = [String:String]()
                
                if let type = val["type"] {
                    dict["subPductTypeId"] = type
                }
                
                if let brand = val["brandId"] {
                    dict["brandId"] = brand
                }
                
                ss.dataValue = dict
                
                for one in ss.syncHandlers {
                    one ()
                }
            }
            
        }).addDisposableTo(disposeBag)
    }
    
    init(currentType:ProductTypeItem) {
        self.currentType = currentType
        
        setupViewModel()
    }
    
    var key = "filter"
    
    var dataValue:Any? = nil
    
    var title:String = "筛选"
    
    var image:UIImage? = UIImage(named: "icon_pulldown_normal")
    
    var highlightedImage = UIImage(named: "icon_pulldown_selected")
    
    var allowMultiple:Bool = true
    
    var itemNames:[String] = []
    
    var values:[Int] = [0]
    
    var syncHandlers:[(Void) -> Void] = []
    
    let disposeBag = DisposeBag()
    
    var itemStyle:DropDownMenuItemPopStyle {
        
        weak var weakSelf = self
        
        return .custom { () in
            if let strongSelf = weakSelf {
                
                let vc = CategoryFilterViewController(viewModel: strongSelf.viewModel)
                
                let nc = CommonNavigationVC(rootViewController: vc)
                
                SliderPopViewController(nc).present()
            }
        }
    }
    
    class ViewModel: CategoryFilterViewModel {
        
        struct LoadingCategorySectionType: CategorySectionType {
            
            var items:[GeneralSectionRow] = []
            
            let key:String = "brandId"
            
            let title:String = "载入中"
            
            var displayLimitCount:Int = -1
        }
        
        struct TypeItem: GeneralSectionRow  {
            var name:String
            
            var id:String
            
            init(item:API.ProductType) {
                name = item.productTypeName
                
                id = item.productTypeId
            }
            
            init(item:API.Brand.SubtypeItem) {
                name = item.productTypeName
                
                id = item.productTypeId
            }
        }
        
        struct TypeSection : CategorySectionType {
            init (array:[API.ProductType], title:String) {

                items = array.map({ TypeItem(item:$0) })
                
                self.title = title
            }
            
            init (array:[API.Brand.SubtypeItem], title:String) {
                
                items = array.map({ TypeItem(item:$0) })
                
                self.title = title
            }
            
            var items:[GeneralSectionRow]
            
            let key:String = "type"
            
            let title:String
            
            var displayLimitCount:Int = -1
        }
        
        struct BrandItem: GeneralSectionRow  {
            var name:String
            
            var id:String
            
            init(item:API.Brand) {
                name = item.chineseName
                
                id = item.brandId
            }
        }
        
        struct BrandSection: CategorySectionType {
            
            init (brands:[API.Brand], title:String) {
                self.title = title
                
                items = brands.map({ BrandItem(item:$0) })
            }
            
            var items:[GeneralSectionRow]
            
            let key:String = "brandId"
            
            let title:String
            
            var displayLimitCount:Int = -1
        }
        
        let disposeBag = DisposeBag()
        
        init(keyword:String) {
            super.init(sectionTypes: [LoadingCategorySectionType()])
            
            API.SearchIndexCollection.getBrandList(from: keyword).map {
                (item) -> [CategorySectionType] in
                
                switch item {
                case .success(let model):
                    return [BrandSection(brands: model.brandList,
                                         title: "品牌")]
                default:
                    return [LoadingCategorySectionType()]
                }
                }
                .bind {
                    [weak self] (types) in
                    
                    guard let strongSelf = self else {
                        return
                    }
                    
                    strongSelf.sectionCollection.value = types
                }.addDisposableTo(disposeBag)
        }
    
        init(currentType:ProductTypeItem) {
            if currentType.level == 3 {
                super.init(sectionTypes: [LoadingCategorySectionType()])
                
                API.Brand.getBrandList(typeId: currentType.id).map {
                    (item) -> CategorySectionType in
                    switch item {
                    case .success(let model):
                        return BrandSection(brands: model.list,
                                            title: "品牌")
                    default:
                        return LoadingCategorySectionType()
                    }
                    }.bind {
                        [weak self] (type) in
                        
                        guard let strongSelf = self else {
                            return
                        }
                        
                        if strongSelf.sectionCollection.value.count >= 1 {
                            strongSelf.sectionCollection.value[0] = type
                        } else {
                            strongSelf.sectionCollection.value.append(type)
                        }
                    }.addDisposableTo(disposeBag)
            
            } else {
                let array = Array<API.ProductType>()
                
                let typeSession = TypeSection(array: array,
                                              title: currentType.name)
                
                super.init(sectionTypes: [typeSession])
                
                DictionaryManager
                    .shareInstance
                    .getType(by: currentType.id)
                    .flatMapLatest({
                        [weak self] (item) -> Observable<[CategorySectionType]> in
                        guard let `item` = item, let `self` = self else {
                            return Observable.empty()
                        }
                        
                        return Observable.create({
                            [weak self] (observer) -> Disposable in
                            
                            guard let `self` = self else {
                                return Disposables.create()
                            }
                            
                            let sessions:Variable<[CategorySectionType]> = Variable([TypeSection(array: item.subTypeList,
                                                                                                 title: item.productTypeName)])
                            
                            let typeId = self
                                .value
                                .asObservable()
                                .distinctUntilChanged({ (dict1, dict2) -> Bool in
                                    return dict1.count == dict2.count && dict1["type"] == dict2["type"] && dict1["brandId"] == dict2["brandId"]
                                })
                                .map({ $0["type"] })
                                .distinctUntilChanged({ $0 == $1 })
                                .shareReplay(1)
                            
                            let p1 = typeId.map({_ in "brandId"}).bind(to: self.deleteKey)
                            
                            let p2 = typeId
                                .filter({ $0 == nil })
                                .bind {
                                    _ in
                                
                                if sessions.value.count >= 2 {
                                    sessions.value.removeLast()
                                }
                            }
                            
                            let p3 = typeId
                                .filter({ $0 != nil })
                                .flatMapLatest({ API.Brand.getBrandList(typeId: $0!) }).map {
                                    (item) -> CategorySectionType in
                                    switch item {
                                    case .success(let model):
                                        return BrandSection(brands: model.list,
                                                            title: "品牌")
                                    default:
                                        return LoadingCategorySectionType()
                                    }
                                }.bind {
                                    (type) in
                                    
                                    if sessions.value.count >= 2 {
                                        sessions.value[1] = type
                                    } else {
                                        sessions.value.append(type)
                                    }
                            }
                            
                            let p4 = sessions.asObservable().bind(to: observer)
                            
                            return Disposables.create(p1, p2, p3, p4)
                        })
                            .shareReplay(1)
                    })
                    .bind(to: sectionCollection)
                    .addDisposableTo(disposeBag)
            }
            
            
        }
    }
}

class BrandFilterType: ProductFilterItemType {
    
    let viewModel:ViewModel
    
    var key = "pductTypeId"
    
    var dataValue:Any? = nil
    
    var title:String = "筛选"
    
    var image:UIImage? = UIImage(named: "icon_pulldown_normal")
    
    var highlightedImage = UIImage(named: "icon_pulldown_selected")
    
    var allowMultiple:Bool = true
    
    var itemNames:[String] = []
    
    var values:[Int] = [0]
    
    var syncHandlers:[(Void) -> Void] = []
    
    let disposeBag = DisposeBag()
    
    init(brandId:String) {
        viewModel = ViewModel(brandId:brandId)
        
        viewModel.confirmValue.bind(onNext: { [weak self] (val) in
            if let ss = self {
                
                if val.keys.count > 0 {
                    ss.dataValue = ["layer": val.keys.first!,
                                    "pductTypeId": val.values.first!
                    ]
                } else {
                    ss.dataValue = nil
                }
                
                for one in ss.syncHandlers {
                    one ()
                }
            }
        }).addDisposableTo(disposeBag)
    }
    
    var itemStyle:DropDownMenuItemPopStyle {
        return .custom { [weak self]() in
            if let strongSelf = self {
                let vc = CategoryFilterViewController(viewModel: strongSelf.viewModel)
                
                vc.allowMutipleSectionSelected = false
                
                let nc = CommonNavigationVC(rootViewController: vc)
                
                SliderPopViewController(nc).present()
            }
        }
    }
    
    class ViewModel: CategoryFilterViewModel {
        struct LoadingCategorySectionType: CategorySectionType {
            
            var items:[GeneralSectionRow] = []
            
            let key:String = "type"
            
            let title:String = "载入中"
            
            var displayLimitCount:Int = -1
        }
        
        struct EmptyCategorySectionType: CategorySectionType {
            
            var items:[GeneralSectionRow] = []
            
            let key:String = "type"
            
            let title:String = "该品牌下无分类"
            
            var displayLimitCount:Int = -1
        }
        
        struct TypeItem: GeneralSectionRow  {
            var name:String
            
            var id:String
            
            init(item:API.ProductType) {
                name = item.productTypeName
                
                id = item.productTypeId
            }
        }
        
        struct TypeSection : CategorySectionType {
            init (level:Int, typeItems:[ProductTypeItem]) {
                title = "\(level)级分类"
                
                key = "\(level)"
                
                items = typeItems
            }
            
            var items:[GeneralSectionRow]
            
            let key:String
            let title:String
            
            var displayLimitCount:Int = -1
        }
        
        let disposeBag = DisposeBag()
        
        init (brandId:String) {
            super.init(sectionTypes: [LoadingCategorySectionType()])
            
            API.ProductType.getTypeList(brandId: brandId).responseModelList({
                [weak self] (list:[API.ProductType]) in
                
                let typeItems = list.map({ ProductTypeItem(model:$0) })
                
                var dict:[Int:[ProductTypeItem]] = [:]
                
                for one in typeItems {
                    let level = one.level
                    
                    if let v = dict[level] {
                        dict[level] = v + [one]
                    } else {
                        dict[level] = [one]
                    }
                }
                
                let levels = [Int](dict.keys).sorted(by: <)
                
                let sections = levels.map{TypeSection(level:$0, typeItems:dict[$0]!)}
                
                if sections.count > 0 {
                    self?.sectionCollection.value = sections
                } else {
                    self?.sectionCollection.value = [EmptyCategorySectionType()]
                }
            }, error: nil)
        }
    }
}
