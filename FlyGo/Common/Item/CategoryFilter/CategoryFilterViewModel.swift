//
//  CategoryFilterViewModel.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/6.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol CategorySectionType : GeneralSectionItem {
    var key:String { get }
    
    var displayLimitCount:Int { get }
}

extension CategorySectionType {
    var itemCount:Int {
        return displayLimitCount <= 0 ? items.count : min(displayLimitCount, items.count)
    }
}

class CategoryFilterViewModel: ViewModel {
    
    let sectionCollection:Variable<[CategorySectionType]>
    
    let value:Variable<[String:String]>
    
    let confirmValue:PublishSubject<[String:String]> = PublishSubject()
    
    let deleteKey:PublishSubject<String> = PublishSubject()
    
    init (sectionTypes:[CategorySectionType]) {
        sectionCollection = Variable(sectionTypes)
        
        value = Variable([:])
    }
    
    func bind(to view: CategoryFilterViewController) -> DisposeBag? {
        let bindBag = DisposeBag()
        
        view.values.value = value.value
        
        view.values.asObservable().bind(to: value).addDisposableTo(bindBag)
        
        deleteKey.bind {
            [weak view] (key) in
            if let strongView = view {
                strongView.values.value[key] = nil
            }
        }
            .addDisposableTo(bindBag)
        
        sectionCollection
            .asDriver()
            .drive(view
                .sections)
            .addDisposableTo(bindBag)
        
        view
            .confirmButton
            .rx.tap
            .withLatestFrom(view
                .values
                .asObservable())
            .bindNext(confirmValue)
            .addDisposableTo(bindBag)
        
        return bindBag
    }
}
