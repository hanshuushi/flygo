//
//  OpenURLWebVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/10.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OpenURLWebVC: WKWebViewController {
    init(title:String, url:URL) {
        super.init(url: url)
        
        self.title = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var showNavigationBar: Bool {
        if let `title` = self.title, title.length > 0 {
            return true
        }
        
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let `title` = self.title, title.length > 0 {
            return
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        webView.scrollView.setInset(top: 20, bottom: 0)
        webView.scrollView.bounces = false
        webView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        
        progressView.snp.remakeConstraints { (maker) in
            maker.left.right.equalTo(self.view)
            maker.top.equalTo(self.view).offset(20)
            maker.height.equalTo(3)
        }
        
        let backButton = UIButton(type: .custom)
        
        backButton.setImage(UIImage(named:"icon_return_white"),
                            for: .normal)
        
        backButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2)
        backButton.layer.cornerRadius = 15
        backButton.layer.masksToBounds = true
        backButton.addTarget(target,
                             action: #selector(UIViewController.popVC),
                             for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(backButton)
        
        backButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.view).offset(27)
            maker.left.equalTo(self.view).offset(14)
            maker.size.equalTo(CGSize(width:30, height:30))
        }
    }
    
    
    override func verticalOffset() -> CGFloat{
        let inset = self.webView.scrollView.contentInset
        
        return inset.top - inset.bottom
    }
    
    override func switchLoadingView() {
        coverView?.removeFromSuperview()
    }
}

