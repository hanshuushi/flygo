//
//  AdvertisementItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

struct AdvertisementItem {
    let id:String
    
    let width:CGFloat
    
    let height:CGFloat
    
    let imageUrl:URL?
    
    let addressUrl:URL?
    
    var radio:CGFloat {
        return height / width
    }
    
    init(model:API.Advertisement) {
        id = model.advertisementId
        
        width = CGFloat(model.width)
        
        height = CGFloat(model.height)
        
        imageUrl = model.pic
        
        addressUrl = model.url
    }
    
}

class AdvertisementTableViewCell: UITableViewCell {
    
    let coverImageView: UIImageView
    
    var radio:CGFloat = 0.01
    
    var url:URL?
    
    var id:String?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        coverImageView = UIImageView()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(coverImageView)
        
        coverImageView.snp.fillToSuperView()
        
        coverImageView.backgroundColor = UIConfig.generalColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        coverImageView.kf.cancelDownloadTask()
    }
    
    func set(item:AdvertisementItem) {
        coverImageView.kf.setImage(with: item.imageUrl,
                                   placeholder: UIImage(named: "bk_loading"))
        
        radio = item.radio
        
        url = item.addressUrl
        
        id = item.id
    }
}
