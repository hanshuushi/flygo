//
//  ProductView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/2.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

extension Currency {
    func unitFont(size:CGFloat) -> UIFont {
        if unit == "￥" || unit == "円" {
            return UIConfig.generalFont(size)
        }
        
        return UIConfig.arialFont(size)
    }
    
    func attributedString(color:UIColor = UIConfig.generalColor.red,
                          intSize:CGFloat,
                          floatSize:CGFloat = 13) -> NSAttributedString {
        let attr = NSMutableAttributedString(string: "\(unit) ",
                                             font: unitFont(size: floatSize),
                                             textColor: color)
        
        let priceString = self.priceString
        
        var array = priceString.split(".")
        
        if array.count == 1 {
            array.append("00")
        } else if array.count == 0 {
            array = ["0","00"]
        }
        
        attr.append(NSAttributedString(string: "\(array[0]).",
            font: UIConfig.arialFont(intSize),
            textColor: color))
        attr.append(NSAttributedString(string: array[1],
            font: UIConfig.arialFont(floatSize),
            textColor: color))
        
        return NSAttributedString(attributedString: attr)
    }
    
    func attributedString(with color:UIColor = UIConfig.generalColor.red, size:CGFloat = 13) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: "\(unit) ",
            font: unitFont(size:size), textColor: color)
        
        attributedString.append(NSAttributedString(string: value.priceString,
                                                   font: UIConfig.arialFont(size),
                                                   textColor: color))
        
        return NSAttributedString(attributedString: attributedString)
    }
}

func getPriceAttributedString(rmb:Currency, ori:Currency) -> NSAttributedString {
    let attributedString = NSMutableAttributedString()
    
    attributedString.append(ori.attributedString(with: UIConfig.generalColor.red))
    attributedString.append(NSAttributedString(string: " 约 ",
                                               font: UIConfig.generalFont(11),
                                               textColor: UIConfig.generalColor.titleColor))
    attributedString.append(rmb.attributedString(with: UIConfig.generalColor.titleColor, size:11))
    
    return NSAttributedString(attributedString: attributedString)
}

/// Product
class ProductCollectionViewCell: UICollectionViewCell {
    
    static func cellHeight(with productWidth:CGFloat, and isShowRank:Bool) -> CGFloat {
        return productWidth + (isShowRank ? 100 : 80)
    }
    
    let imageView = UIImageView()
    
    let rankLabel = UILabel()
    
    let titleLabel = TopAlignLabel()
    
    let priceLabel = UILabel()
    
    let blockView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // add image
        let coverZone = UIView()
        
        coverZone.backgroundColor = UIConfig.generalColor.white
        
        self.contentView.addSubview(coverZone)
        coverZone.snp.makeConstraints { (maker) in
            maker.top.left.right.equalTo(self.contentView)
            maker.height.equalTo(coverZone.snp.width)
        }
        
        coverZone.addSubview(imageView)
        
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.clear
        imageView.snp.makeConstraints { (maker) in
            maker.center.equalTo(coverZone)
            maker.size.equalTo(CGSize(width:100, height:100))
        }
        
        // add block
        self.contentView.addSubview(blockView)
        
        blockView.backgroundColor = UIConfig.generalColor.selected
        blockView.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.contentView)
            maker.top.equalTo(coverZone.snp.bottom).offset(5)
            maker.size.equalTo(CGSize(width:5, height:10))
        }
        
        // add top title
        self.contentView.addSubview(rankLabel)
        
        // add rank
        rankLabel.textColor = UIConfig.generalColor.selected
        rankLabel.font = UIConfig.arialBoldFont(12)
        rankLabel.backgroundColor = UIConfig.generalColor.white
        rankLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(blockView.snp.right).offset(5)
            maker.centerY.equalTo(blockView)
        }
        
        // add title
        self.contentView.addSubview(titleLabel)
        
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.numberOfLines = 2
        titleLabel.textColor = UIConfig.generalColor.selected
        titleLabel.backgroundColor = UIConfig.generalColor.white
        titleLabel.font = UIConfig.generalFont(12)
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(rankLabel.snp.bottom).offset(6.5)
            maker.left.right.equalTo(self.contentView)
            maker.height.equalTo(34)
        }
        titleLabel.contentMode = .top
        
        // add price label
        self.contentView.addSubview(priceLabel)
        
        priceLabel.font = UIConfig.arialFont(13)
        priceLabel.textColor = UIConfig.generalColor.red
        priceLabel.backgroundColor = UIConfig.generalColor.white
        priceLabel.snp.makeConstraints { (maker) in
            maker.right.equalTo(self.contentView)
            maker.left.equalTo(self.contentView)
            maker.top.equalTo(titleLabel.snp.bottom).offset(6.5)
        }
    }
    
    var item:ProductItem? {
        didSet {
            
            imageView.kf.setImage(with:item?.imageUrl, options:[.scaleFactor(UIScreen.main.scale)])
            
            let rank = item?.rank ?? ""
            
            if rank.length > 0 {
                titleLabel.snp.remakeConstraints { (maker) in
                    maker.top.equalTo(rankLabel.snp.bottom).offset(6.5)
                    maker.left.right.equalTo(self.contentView)
                    maker.height.equalTo(34)
                }
                
                blockView.isHidden = false
                rankLabel.isHidden = false
                
                rankLabel.text = rank
            } else {
                titleLabel.snp.remakeConstraints { (maker) in
                    maker.top.equalTo(imageView.superview!.snp.bottom).offset(6.5)
                    maker.left.right.equalTo(self.contentView)
                    maker.height.equalTo(34)
                }
                
                blockView.isHidden = true
                rankLabel.isHidden = true
            }
            
            titleLabel.text = item?.name
            
            priceLabel.attributedText = getPriceAttributedString(rmb: item?.rmb ?? Currency(unit: "￥", value: 0),
                                                                 ori: item?.price ?? Currency(unit: "$", value: 0))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
