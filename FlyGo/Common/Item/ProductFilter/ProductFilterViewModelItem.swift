//
//  ProductFilterViewModelItem.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/10.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift

protocol ProductFilterItemType: DropDownMenuItemType {
    var key:String { get }
    
    var dataValue:Any? { get }
}

class ProductFilterItemSort: ProductFilterItemType {
    var key = "sort"
    
    var dataValue:Any? {
        if let v = values.first {
            if v == 2 {
                return 0
            }
            
            if v == 1 {
                return 1
            }
            
            return 2
        }
        
        return 2
    }
    
    var title = "排序"
    
    var image = UIImage(named: "icon_pulldown_normal")
    
    var highlightedImage = UIImage(named: "icon_pulldown_selected")
    
    var allowMultiple:Bool = false
    
    var itemNames:[String] = ["智能排序","价格升序","价格降序"]
    
    var values:[Int] = [0]
    
    var syncHandlers:[(Void) -> Void] = []
    
    let itemStyle = DropDownMenuItemPopStyle.pop
}

extension ProductFilterItemType {
    static var sort:ProductFilterItemType {
        return ProductFilterItemSort()
    }
}

class ProductFilterCategorySort: ProductFilterItemType {
    var key = "pductTypeId"
    
    let disposeBag:DisposeBag
    
    var dataValue:Any? {
        return values.count > 0 ? items[values[0]].id : nil
    }
    
    var dict:[String:ProductTypeItem]
    
    var sync:((Void) -> Void)?
    
    var title:String {
        if values.count > 0 {
            let index = values[0]
            
            return itemNames[index]
        }
        
        return "类型"
    }
    
    init?(currentType:ProductTypeItem) {
        guard let parentId = currentType.parentId, currentType.level > 1 else {
            return nil
        }
        
        disposeBag = DisposeBag()
        
        items = [currentType]
        
        dict = [currentType.id: currentType]
        
        values = [0]
        
        DictionaryManager
            .shareInstance
            .getType(by: parentId)
            .filter({ $0 != nil })
            .take(1)
            .bind { [weak self] (item) in
                guard let `item` = item, let `self` = self else {
                    return
                }
                
                self.dict = [:]
                
                self.items = item.subTypeList.map({ ProductTypeItem(model:$0) })
                self.items.forEachEnumerated({ (_, item) in
                    self.dict[item.id] = item
                })
                
                if let index = self.items.map({ $0.id }).index(of: currentType.id) {
                    self.values = [index]
                }
                
                self.sync?()
        }
            .addDisposableTo(disposeBag)
        
    }
    
    var items:[ProductTypeItem]
    
    var image:UIImage? = UIImage(named: "icon_pulldown_normal")
    
    var highlightedImage = UIImage(named: "icon_pulldown_selected")
    
    var allowMultiple:Bool = false
    
    var itemNames:[String] {
        return items.map({ $0.name })
    }
    
    var values:[Int] = [0]
    
    var syncHandlers:[(Void) -> Void] = []
    
    let itemStyle = DropDownMenuItemPopStyle.pop
}

extension ProductFilterItemType {
    static func category(_ currentType:ProductTypeItem) -> ProductFilterCategorySort? {
        return ProductFilterCategorySort(currentType:currentType)
    }
}
