//
//  ImagePicker.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/9.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import EZSwiftExtensions

class ImagePicker: NSObject {
    
    fileprivate enum PickerType {
        case uploader
        case avatar
        case picker
    }
    
    fileprivate let type:PickerType
    
    /// AFNetworking 管理单列
    static let httpManager:AFHTTPSessionManager = {
        guard let baseUrl = URL(string: URLConfig.api) else {
            fatalError("Base Url Error")
        }
        
        let manager = AFHTTPSessionManager(baseURL: baseUrl,
                                           sessionConfiguration: URLSessionConfiguration.default)
        
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.completionQueue = DispatchQueue(label: "com.zhiyou.flygo.uploader")
        
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        /// https
        let securityPolicy = AFSecurityPolicy.default()
        
        securityPolicy.allowInvalidCertificates = true
        securityPolicy.validatesDomainName = false
        
        manager.securityPolicy = securityPolicy
        
        /// response
        manager.responseSerializer.acceptableContentTypes = ["text/plain", "text/json", "text/html","text/xml", "application/json"]
        
        return manager
    }()
    
    let rootViewController:UIViewController
    
    let imagePickerController:TZImagePickerController
    
    let response:CropImageResponse
    
    typealias CropImageResponse = (UIImage, URL?, String?) -> Void
    
    var storeImagePicker:ImagePicker? = nil
    
    fileprivate init?(rootViewController:UIViewController, response:@escaping CropImageResponse, type:PickerType) {
        self.rootViewController = rootViewController
        
        guard let imagePickerController = TZImagePickerController(maxImagesCount: 1,
                                                                  delegate: nil) else {
                                                                    return nil
        }
        
        let boardLength = UIScreen.main.bounds.width - 30
        
        imagePickerController.allowCrop = type == .avatar
        imagePickerController.needCircleCrop = false
        imagePickerController.cropRect = CGRect(x: 15,
                                                y: (UIScreen.main.bounds.height - boardLength) / 2.0,
                                                width: boardLength,
                                                height: boardLength)
        imagePickerController.allowPickingVideo = false
        imagePickerController.allowPickingGif = false
        imagePickerController.allowPickingOriginalPhoto = false
        
//        imagePickerController.naviBgColor = UIConfig.generalColor.barColor
//        imagePickerController.naviTitleFont = UIConfig.generalFont(20.0)
//        imagePickerController.naviTitleColor = UIConfig.generalColor.selected
//        imagePickerController.barItemTextFont = UIConfig.generalFont(20.0)
//        imagePickerController.barItemTextColor = UIConfig.generalColor.selected
//        imagePickerController.doneBtnTitleStr = "完成"
//        imagePickerController.cancelBtnTitleStr = "取消"
//        imagePickerController.previewBtnTitleStr = "预览"
        
        self.imagePickerController = imagePickerController
        
        self.response = response
        
        self.type = type
        
        super.init()
        
        imagePickerController.pickerDelegate = self
    }
    
    func hide() {
        imagePickerController.dismiss(animated: true,
                                      completion: nil)
    }
    
    func show() {
        storeImagePicker = self
        
        rootViewController.present(imagePickerController,
                                   animated: true,
                                   completion: nil)
    }
    
    @discardableResult
    static func uploader(show rootViewController:UIViewController,
                         response:@escaping CropImageResponse) -> ImagePicker? {
        let imagePicker = ImagePicker(rootViewController: rootViewController,
                                      response: response,
                                      type: .uploader)
        
        imagePicker?.show()
        
        return imagePicker
    }
    
    @discardableResult
    static func picker(show rootViewController:UIViewController,
                             response:@escaping CropImageResponse) -> ImagePicker? {
        let imagePicker = ImagePicker(rootViewController: rootViewController,
                                      response: response,
                                      type: .picker)
        
        imagePicker?.show()
        
        return imagePicker
    }
    
    @discardableResult
    static func updateAvatar(show rootViewController:UIViewController,
                             response:@escaping CropImageResponse) -> ImagePicker? {
        let imagePicker = ImagePicker(rootViewController: rootViewController,
                                      response: response,
                                      type: .avatar)
        
        imagePicker?.show()
        
        return imagePicker
    }
}

extension ImagePicker: TZImagePickerControllerDelegate {
    func imagePickerController(_ picker: TZImagePickerController!,
                               didFinishPickingPhotos photos: [UIImage]!,
                               sourceAssets assets: [Any]!,
                               isSelectOriginalPhoto: Bool) {
        let type = self.type
        
        if type == .picker {
            guard let image = photos.first else {
                return
            }
            
            self.response(image, nil, nil)
            
            return
        }
        
        guard let image = photos.first,
            let imageData = UIImageJPEGRepresentation(image, 0.8) else {
            return
        }
        
        let toast = rootViewController.showLoading()
        
        let response = self.response
        
        
        ImagePicker
            .httpManager
            .post(URLConfig.imageUploader + "files/uploadProcesser",
                  parameters: ["type":"x99",
                               "module":"member"],
                  constructingBodyWith: { (formData) in
                    formData.appendPart(withFileData: imageData,
                                        name: "file",
                                        fileName: NSUUID().uuidString + ".jpg",
                                        mimeType: "image/jpeg")
            },
                  progress: nil,
                  success: {
                    [weak self] (task, result) in
                    do {
                        let model:API.UploaderImage = try HttpSession.model(from: result ?? [String:Any]())
                        
                        if type == .avatar {
                            
                            UserManager.shareInstance.update(avatar: model,
                                                             response: { (error) in
                                                                if let httperror = error {
                                                                    ez.runThisInMainThread {
                                                                        toast.displayLabel(text: httperror.localizedDescription)
                                                                    }
                                                                } else {
                                                                    ez.runThisInMainThread {
                                                                        response(image, model.picurl, model.urlString)
                                                                        
                                                                        toast.dismiss(animated: true,
                                                                                      completion: nil)
                                                                    }
                                                                }
                                                                
                                                                self?.storeImagePicker = nil
                            })
                        } else {
                            ez.runThisInMainThread {
                                response(image, model.picurl, model.urlString)
                                
                                toast.dismiss(animated: true,
                                              completion: nil)
                            }
                            
                        }
                    } catch HttpError.businessError(_, let description) {
                        ez.runThisInMainThread {
                            toast.displayLabel(text: description)
                        }
                        
                        self?.storeImagePicker = nil
                    } catch {
                        ez.runThisInMainThread {
                            toast.displayLabel(text: "上传失败")
                        }
                        
                        self?.storeImagePicker = nil
                    }
            }) { (task, error) in
                ez.runThisInMainThread {
                    toast.displayLabel(text: error.localizedDescription)
                }
        }
    }
}

extension API {
    struct UploaderImage: Model {
        var picurl:URL!
        
        var type:String = ""
        
        var filesize:String = ""
        
        var originalName:String = ""
        
        var errmsg:String = ""
        
        var urlString:String = ""
        
        init?(map: Map) {
            picurl                          <-      (map["picurl"], API.PicTransform())
            
            if picurl == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            picurl                          <-      (map["picurl"], API.PicTransform())
            urlString                       <-      (map["picurl"], API.StringTransform())
            type                            <-      (map["type"], API.StringTransform())
            filesize                        <-      (map["filesize"], API.StringTransform())
            originalName                    <-      (map["originalName"], API.StringTransform())
            errmsg                          <-      (map["errmsg"], API.StringTransform())
        }
    }
}
