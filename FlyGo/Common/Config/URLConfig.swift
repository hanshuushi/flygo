//
//  URLConfig.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/3.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

typealias URLConfigItem = (api:String, prefix:String, image:String)

enum URLSetting {
    case develop
    case test
    case appstore
    
    var api:String {
        switch self {
        case .develop:
            return "http://118.178.189.22:8080/yaodai-api/api/"
        case .test:
            return "https://118.178.192.187/yaodai-api/api/"
        case .appstore:
            return "https://api.iflygoo.com/yaodai-api/api/"
        }
    }
    
    var image:String {
        switch self {
        case .develop:
            return "http://118.178.189.22:8000/repos/"
        case .test:
            return "http://118.178.191.144/repos/"
        case .appstore:
            return "http://img.iflygoo.com/repos/"
        }
    }
    
    var imageUploader:String {
        switch self {
        case .develop:
            return "http://118.178.189.22:8000/"
        case .test:
            return "http://118.178.191.144/"
        case .appstore:
            return "http://img.iflygoo.com/"
        }
    }
    
    var prefix:String {
        switch self {
        case .develop:
            return "http://118.178.189.22:8080/"
        case .test:
            return "http://118.178.192.187/"
        case .appstore:
            return "http://api.iflygoo.com/"
        }
    }
    
    var leanCloudAPPId:String {
        switch self {
        case .appstore:
            return "gWPJjoRcO2fN0d31KDyY5J2j-gzGzoHsz"
        default:
            return "l31p33C5vt8GS5vRmf03EDfW-gzGzoHsz"
        }
    }
    
    var leanCloudAPPKey:String {
        switch self {
        case .appstore:
            return "v8rVw0jRu2fF0hVOV8qy8lXY"
        default:
            return "1DdQxAHMqsuWurpHjnxjaIAV"
        }
    }
}

#if AdHoc || DEBUG
let URLConfig = {
    () -> URLSetting in
    if let value = UserDefaults.standard.value(forKey: "AdHoc-URLConfig") as? String, value == "test" {
        return .test
    }
    
    if let value = UserDefaults.standard.value(forKey: "AdHoc-URLConfig") as? String, value == "release" {
        return .appstore
    }
    
    return .develop
}()
#else
let URLConfig = URLSetting.appstore
#endif
