//
//  UIConfig.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

let UIConfig = (
    generalColor: (
        red : UIColor(r: 255, g: 39, b: 65),
        highlyRed: UIColor(r: 255, g: 89, b: 109),
        white : UIColor.white,
        selected : UIColor(r: 65, g: 66, b: 67),
        unselected : UIColor(r: 142, g: 145, b: 147),
        backgroudGray : UIColor(r: 245, g: 245, b: 245),
        backgroudWhite : UIColor(r: 249, g: 249, b: 249),
        barColor : UIColor.white.withAlphaComponent(0.95),
        whiteGray : UIColor(r: 209, g: 212, b: 214),
        lineColor : UIColor(r: 238, g: 238, b: 238),
        titleColor : UIColor(r: 107, g: 109, b: 110),
        labelGray : UIColor(r: 175, g: 177, b: 179),
        yellow : UIColor(r: 255, g: 190, b: 78)
    ),
    generalFont: {
        (fontSize:CGFloat) -> UIFont in
        if #available(iOS 9.0, *) {
            return UIFont(name: "PingFangSC-Light", size:fontSize)!
        } else {
            return UIFont.systemFont(ofSize: fontSize)
        }
    },
    generalSemiboldFont: {
        (fontSize:CGFloat) -> UIFont in
        if #available(iOS 9.0, *) {
            return UIFont(name: "PingFang SC", size:fontSize)!
        } else {
            return UIFont.boldSystemFont(ofSize: fontSize)
        }
    },
    arialBoldFont: {
        (fontSize:CGFloat) in
        return UIFont(name: "Arial Rounded MT Bold", size:fontSize)!
    },
    arialFont: {
        (fontSize:CGFloat) in
        return UIFont(name: "Arial", size:fontSize)!
    }
)

struct CommonTabViewStyle: NavTabStyle {
    var shadowColor = UIConfig.generalColor.lineColor
    
    var shadowHeigh: CGFloat = 0.5

    var font:UIFont = UIConfig.generalSemiboldFont(16)
    
    var tabItemColor:UIColor = UIConfig.generalColor.whiteGray
    
    var tabHighlyColor:UIColor =  UIConfig.generalColor.selected
    
    var titleBackgroundColor:UIColor = UIConfig.generalColor.barColor
    
    var contentBackgroundColor:UIColor = UIConfig.generalColor.white
    
    var contentMarginTop: CGFloat = -106.5
    
    var barColor:UIColor = UIConfig.generalColor.selected
    
    var titleHeight:CGFloat = 42.5
    
    var titlePadding:CGFloat = 15
    
    var barHeight:CGFloat = 2

    var titleInset:CGFloat = 25
}

struct CommonDropDownMenuStyle: DropDownMenuStyle {
    var shadowColor = UIConfig.generalColor.lineColor
    
    var shadowHeight: CGFloat = 0.5
    
    var backgroudColor:UIColor = UIConfig.generalColor.barColor
    
    var barHeight:CGFloat = 43.5
    
    var titleFont:UIFont = UIConfig.generalSemiboldFont(16)
    
    var titleColor:UIColor = UIConfig.generalColor.unselected
    
    var titleHighlyColor: UIColor = UIConfig.generalColor.selected
    
    var contentMarginTop:CGFloat = -107.5
    
    var tagOffset:CGFloat = 15
    
    var itemHeight:CGFloat = 43.5
    
    var itemFont:UIFont = UIFont.systemFont(ofSize: 15)
    
    var itemFontColor:UIColor = UIConfig.generalColor.selected
    
    var itemSelectColor:UIColor = UIConfig.generalColor.red
    
    var lineColor:UIColor = UIConfig.generalColor.lineColor
    
    var lineEdge:CGFloat = 5
    
    var maskColor = UIColor.black.withAlphaComponent(0.3)
}

//extension UIViewController {
//    func addFakeShadow () {
//        
////        let shadowView = UIView()
////        
////        shadowView.backgroundColor = UIConfig.generalColor.lineColor
////        
////        self.view.addSubview(shadowView)
////        
////        shadowView.snp.makeConstraints { (maker) in
////            maker.left.right.equalTo(self.view)
////            maker.top.equalTo(self.view).offset(64)
////            maker.height.equalTo(0.5)
////        }
//    }
//}
