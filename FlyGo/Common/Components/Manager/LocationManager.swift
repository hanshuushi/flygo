//
//  LocationManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/21.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa

enum LocationManagerAuthorizationStatus {
    case notDetermined
    case denied
    case authorized
    
    static func from(status:CLAuthorizationStatus) -> LocationManagerAuthorizationStatus {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return .authorized
        case .denied, .restricted:
            return .denied
        case .notDetermined:
            return .notDetermined
        }
    }
    
    var enable:Bool {
        switch self {
        case .authorized:
            return true
        default:
            return false
        }
    }
}

struct LocationManagerItem {
    var latitude:Double
    
    var longitude:Double
    
    var locationName:String
    
    var countryName:String
    
    var provinceName:String
    
    var cityName:String
    
    var districtName:String
    
    var streetName:String
    
    init?(placemark:CLPlacemark) {
        guard let location = placemark.location else {
            return nil
        }
        
        latitude = location.coordinate.latitude
        
        longitude = location.coordinate.longitude
        
        locationName = placemark.name ?? ""
        
        countryName = placemark.country ?? ""
        
        provinceName = placemark.administrativeArea ?? ""
        
        var city:String?
        
        if let locality = placemark.locality {
            city = locality
        } else if let administrativeArea = placemark.administrativeArea {
            city = administrativeArea
        }
        
        cityName = city ?? ""
        
        districtName = placemark.subLocality ?? ""
        
        streetName = placemark.thoroughfare ?? ""
    }
}

class LocationManager: NSObject {
    
    fileprivate let locationManager:CLLocationManager
    
    fileprivate let authorizationVariable:Variable<LocationManagerAuthorizationStatus>
    
    fileprivate let locationSubject:PublishSubject<CLLocation>
    
    let authorization:Driver<LocationManagerAuthorizationStatus>
    
    let locationItem:Driver<LocationManagerItem>
    
    static func playcemarkObserver(from location:CLLocation) -> Observable<CLPlacemark> {
        return Observable.create({ (observer) -> Disposable in
            let geocode = CLGeocoder()
            
            geocode.reverseGeocodeLocation(location,
                                           completionHandler: { (marks, error) in
                                            if let err = error {
                                                observer.onError(err)
                                                observer.onCompleted()
                                                
                                                return
                                            }
                                            
                                            if let maskArray = marks {
                                                for one in maskArray {
                                                    observer.onNext(one)
                                                }
                                            }
                                            
                                            observer.onCompleted()
            })
            
            return Disposables.create {
                geocode.cancelGeocode()
            }
        })
    }
    
    override init() {
        
        locationManager = CLLocationManager()
        
        authorizationVariable = Variable(.from(status: CLLocationManager.authorizationStatus()))
        
        let locationSubject = PublishSubject<CLLocation>()
        
        self.locationSubject = locationSubject
        
        authorization = authorizationVariable.asDriver()
        
        locationItem = authorization.map({ (status) -> Driver<LocationManagerItem> in
            if !status.enable {
                return Driver.empty()
            }
            
            return locationSubject
                .observeOn(MainScheduler.asyncInstance)
                .flatMap { (location) -> Observable<CLPlacemark> in
                    return LocationManager.playcemarkObserver(from: location)
                }
                .map({ LocationManagerItem(placemark:$0) })
                .asDriver(onErrorJustReturn: nil)
                .filter({ $0 != nil })
                .map({ $0! })
        })
            .switchLatest()
        
        super.init()
        
        locationManager.distanceFilter = 3000.0
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = true
    }
    
    static let shareInstance:LocationManager = LocationManager()
    
    var locationEnable:Bool {
        return authorizationVariable.value.enable
    }
    
    func startLocation() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        if let location = locationManager.location {
            locationSubject.onNext(location)
        }
    }
    
    func stopLocation() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            locationSubject.onNext(location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        authorizationVariable.value = .from(status: status)
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        
    }
}
