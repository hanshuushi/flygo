//
//  IMManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import EZSwiftExtensions

extension Notification.Name {
    public static var ChatManagerUnreadCountChangeNotification:Notification.Name {
        return Notification.Name(rawValue: "ChatManagerUnreadCountChangeNotification")
    }
    
    public static var ChatManagerReceiveMessageNotification:Notification.Name {
        return Notification.Name(rawValue: "ChatManagerReceiveMessageNotification")
    }
    
    public static var ChatManagerUpdateConversationNotification:Notification.Name {
        return Notification.Name(rawValue: "ChatManagerUpdateConversationNotification")
    }
}

class ChatManager: Manager {
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        UserManager
            .shareInstance
            .isLogin
            .asObservable()
            .distinctUntilChanged()
            .bind { (isLogined) in
                self.currentSession = isLogined ? Session(clientId: UserManager.shareInstance.currentId) : nil
                
                NotificationCenter.default.post(name: .ChatManagerUnreadCountChangeNotification,
                                                object: nil,
                                                userInfo: nil)
        }
            .addDisposableTo(disposeBag)
    }
    
    static let disposeBag = DisposeBag()
    
    static var currentSession:Session? = nil
}

extension AVIMConversation {
    func contain(target:String) -> Bool {
        guard let members = self
            .members?
            .map({ $0 as? String })
            .filter({ $0 != nil })
            .map({ $0! }) else {
            return false
        }
        
        return members.contains(target)
    }
}

extension ChatManager {
    enum ClientStatus {
        case openning
        case success
        case failture(error:Error)
    }
    
    enum ConversationResult {
        case success(conversation:AVIMConversation)
        case failture(error:Error)
    }
    
    class Session: NSObject, AVIMClientDelegate {
        let clientId:String
        
        var clientStatus:ClientStatus
        
        let client:AVIMClient
        
        let chatDB:ChatDB
        
        init(clientId:String) {
            self.clientId = clientId
            
            clientStatus = .openning
            
            client = AVIMClient(clientId: clientId)
            
            chatDB = ChatDB(userId: UserManager.shareInstance.currentId,
                            client: client,
                            cacheFinishedSetup:{
                                (dict) in
                                
                                NotificationCenter.default.post(name: .ChatManagerUnreadCountChangeNotification,
                                                                object: nil,
                                                                userInfo: nil)
            })
            
            super.init()
            
            client.delegate = self
            
            let callBack:AVIMBooleanResultBlock = {
                [weak self] (success, error) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if success {
                    strongSelf.clientStatus = .success
                    strongSelf.clientOpenedHandler?()
                } else {
                    let _error = error ?? NSError(domain: "com.zhiyou.flygo.chart.unknow",
                                                  code: 0,
                                                  userInfo: nil)
                    
                    strongSelf.clientStatus = .failture(error: _error)
                    strongSelf.clientOpenFailedHandler?(_error)
                }
                
                
                strongSelf.clientOpenedHandler = nil
                strongSelf.clientOpenFailedHandler = nil
            }
            
            let option = AVIMClientOpenOption()
            
            option.force = false
            
            client.open(with:option, callback: callBack)
        }
        
        deinit {
            client.close { (_, _) in
                
            }
        }
        
        var clientOpenedHandler:((Void) -> Void)?
        
        var clientOpenFailedHandler:((Error) -> Void)?
        
        func closeConversation(_ conversation:Conversation) {
            currentConversations.removeFirst(conversation)
        }
        
        func findAllConversation() -> [AVIMConversation] {
            return self
                .chatDB.conversationsDictionary.values.elements.sorted(by: { (left, right) -> Bool in
                    guard let leftDate = left.latestMessageDate else {
                        return true
                    }
                    
                    guard let rightDate = right.latestMessageDate else {
                        return false
                    }
                    
                    return leftDate > rightDate
                })
        }
        
        func fetchConversationInCache(for userId:String,
                               nickName:String,
                               avatarPath:URL?) -> AVIMConversation? {
            if let conversation = self
                .chatDB.conversationsDictionary.values.elements.filter({ $0.userId == userId }).first {
                
                return conversation
            }
            
            return nil
        }
        
        func fetchConversationFromServer(for userId:String,
                                         nickName:String,
                                         avatarPath:URL?,
                                         conversationCallback:@escaping (ConversationResult) -> Void) {
            clientOpenedHandler = {
                [weak self] in
                
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf
                    .client
                    .createConversation(withName: "Conversation",
                                        clientIds: [userId],
                                        attributes: [userId:nickName,
                                                     UserManager.shareInstance.currentId:UserManager.shareInstance.currentNickName],
                                        options: .unique,
                                        callback: { (conversation, error) in
                                            guard let currentConversation = conversation else {
                                                let _error = error ?? NSError(domain: "com.zhiyou.flygo.chart.unknow",
                                                                              code: 0,
                                                                              userInfo: nil)
                                                
                                                conversationCallback(.failture(error: _error))
                                                
                                                return
                                            }
                                            
                                            currentConversation.userId = userId
                                            currentConversation.nickName = nickName
                                            currentConversation.avatarPath = avatarPath
                                            currentConversation.syncLatest()
                                            
                                            strongSelf.chatDB.update(conversation: currentConversation)
                                            
                                            conversationCallback(.success(conversation: currentConversation))
                                            
                                            NotificationCenter
                                                .default
                                                .post(name: .ChatManagerUpdateConversationNotification,
                                                      object: currentConversation.conversationId ?? "")
                    })
            }
            
            clientOpenFailedHandler = {
                (error) in
                
                conversationCallback(.failture(error: error))
            }
            
            switch clientStatus {
            case .success:
                clientOpenedHandler!()
                
                clientOpenFailedHandler = nil
                clientOpenedHandler = nil
            case .failture(let error):
                conversationCallback(.failture(error:error))
                
                clientOpenFailedHandler = nil
                clientOpenedHandler = nil
            default:
                break
            }
        }
        
        func conversation(for avconversation:AVIMConversation) -> Conversation {
            let conversation = Conversation(avconversation: avconversation, chatdb: self.chatDB)
            
            currentConversations.append(conversation)
            
            chatDB.update(unread: false, of: conversation.avconversation.conversationId ?? "")
            
            NotificationCenter.default.post(name: .ChatManagerUnreadCountChangeNotification,
                                            object: nil,
                                            userInfo: nil)
            
            return conversation
        }
        
        var currentConversations:[Conversation] = []
//        
//        func imClientPaused(_ imClient: AVIMClient) {
//            clientStatusChanged?(.paused)
//        }
//        
//        func imClientResumed(_ imClient: AVIMClient) {
//            clientStatusChanged?(.normal)
//        }
//        
//        func imClientResuming(_ imClient: AVIMClient) {
//            clientStatusChanged?(.resuming)
//        }
        
        func conversation(_ conversation: AVIMConversation,
                          messageDelivered message: AVIMMessage) {
            
        }
        
        func conversation(_ conversation: AVIMConversation, didReceive message: AVIMTypedMessage) {
            self.conversation(conversation,
                              didReceiveCommonMessage: message)
        }
        
        func conversation(_ conversation: AVIMConversation,
                          didReceiveCommonMessage message: AVIMMessage) {
            
            print("你有新短消息")
            
            guard let conversationId = conversation.conversationId,
                  let userId = conversation.members?.map({ $0 as? String }).filter({ $0 != nil && $0 != UserManager.shareInstance.currentId }).map({ $0! }).first
                else {
                return
            }
            
            conversation.userId = userId
            
            var isCurrent = false
            
            for one in currentConversations {
                if one.avconversation.conversationId == conversationId {
                    one.conversation(didReceiveCommonMessage: message)
                    
                    isCurrent = true
                }
            }
            
            if chatDB.conversationsDictionary[conversationId] == nil {
                API
                    .Avatar
                    .getAvatar(from: userId)
                    .responseModel({ (avatar:API.Avatar) in
                        self.chatDB.update(nickName: avatar.nickName,
                                           avatarPath: avatar.userAvatar,
                                           of: conversationId)
                        
                        ez.runThisInMainThread {
                            NotificationCenter
                                .default
                                .post(name: .ChatManagerUpdateConversationNotification,
                                      object: conversationId)
                        }
                    })
            }
            
            self.chatDB.update(conversation: conversation)
            self.chatDB.update(messageContent: message.contentType.text ?? "",
                               messageDate: message.time.date,
                               of: conversationId)
            
            NotificationCenter.default.post(name: .ChatManagerReceiveMessageNotification,
                                            object: nil,
                                            userInfo: ["conversationId": conversationId,
                                                       "message":message])
            
            if !isCurrent {
                self.chatDB.update(unread: true, of: conversationId)
                
                NotificationCenter.default.post(name: .ChatManagerUnreadCountChangeNotification,
                                                object: nil,
                                                userInfo: nil)
            }
        }
    }
}

extension AVIMMessage {
    
    var isMine:Bool {
        return self.ioType == .out
    }
    
    struct Time {
        let year:Int
        
        let month:Int
        
        let day:Int
        
        let weekOfYear:Int
        
        let hour:Int
        
        let min:Int
        
        let sec:Int
        
        let timeInterval:TimeInterval
        
        let date:Date
        
        static let secOffset = TimeInterval(TimeZone(identifier: "Asia/Shanghai")!.secondsFromGMT() - TimeZone.current.secondsFromGMT())
        
        func isCloseTo(other time:Time) -> Bool {
            return fabs(timeInterval - time.timeInterval) < 60
        }
        
        init(timeInterval:TimeInterval) {
            let currentTimeInterval = timeInterval - Time.secOffset
            
            self.timeInterval = currentTimeInterval
            
            date = Date(timeIntervalSince1970: currentTimeInterval)
            
            let components = Calendar.current.dateComponents(in: TimeZone.current,
                                                             from:date)
            
            year = components.year ?? 1970
            
            month = components.month ?? 1
            
            day = components.day ?? 1
            
            weekOfYear = components.weekOfYear ?? 1
            
            hour = components.hour ?? 0
            
            min = components.minute ?? 0
            
            sec = components.second ?? 0
        }
        
        fileprivate init() {
            date = Date()
            
            timeInterval = date.timeIntervalSince1970
            
            let components = Calendar.current.dateComponents(in: TimeZone.current,
                                                             from: date)
            
            year = components.year ?? 1970
            
            month = components.month ?? 1
            
            day = components.day ?? 1
            
            weekOfYear = components.weekOfYear ?? 1
            
            hour = components.hour ?? 0
            
            min = components.minute ?? 0
            
            sec = components.second ?? 0
        }
        
        static let today:Time = Time()
        
        static func getTitle(from date:Date) -> String {
            let currentTime = Time(timeInterval: date.timeIntervalSince1970)
            
            return currentTime.title
        }
        
        var title:String {
            //// 同年隐藏年份
            if Time.today.year != self.year {
                return date.generalFormartString
            }
            
            if (Time.today.month != self.month || Time.today.day != self.day) && Time.today.weekOfYear != self.weekOfYear {
                let formart = DateFormatter()
                
                formart.dateFormat = "MM-dd HH:mm"
                
                return formart.string(from: self.date)
            } else if Time.today.month != self.month || Time.today.day != self.day {
                let formart = DateFormatter()
                
                formart.dateFormat = "E HH:mm"
                
                return formart.string(from: self.date)
            }
            
            let formart = DateFormatter()
            
            formart.dateFormat = "HH:mm"
            
            return formart.string(from: self.date)
        }
    }
    
    var time:Time {
        
        if self.status == .none {
            return Time()
        }
        
        return Time(timeInterval: TimeInterval(sendTimestamp / 1000))
    }
    
    static let localTimeKey = "localTime"
    
    var localTimeInterval:TimeInterval? {
        if let typeMessage = self as? AVIMTypedMessage {
            return typeMessage.attributes?[AVIMMessage.localTimeKey] as? TimeInterval
        }
        
        return nil
    }
    
    var localTimeString:String? {
        guard let _localTimeInterval = localTimeInterval, _localTimeInterval > 0 else {
            return nil
        }
        
        let date = Date(timeIntervalSince1970: _localTimeInterval)
        
        return date.toString(format: "HH:mm")
    }
    
    
    func isCloseTo(other message:AVIMMessage) -> Bool {
        return message.time.isCloseTo(other: time)
    }
    
    enum ContentType {
        case text(String)
        case image(AVFile)
        case unknow
        
        var isImage:Bool {
            switch self {
            case .image(_):
                return true
            default:
                return false
            }
        }
        
        var imageFile:AVFile {
            switch self {
            case .image(let file):
                return file
            default:
                fatalError("error")
            }
        }
        
        var imageSize:CGSize {
            switch self {
            case .image(let file):
                guard let dict = file.metaData else {
                    return CGSize.zero
                }
                
                let width = (dict["width"] as? CGFloat) ??  0
                let height = (dict["height"] as? CGFloat) ??  0
                
                return CGSize(width: width, height: height)
            default:
                return CGSize.zero
            }
        }
        
        var text:String? {
            switch self {
            case .text(let string):
                return string
            case .image(_):
                return "[图片]"
            default:
                return "[未知类型]"
            }
        }
    }
    
    var contentType:ContentType {
        if let message = self as? AVIMImageMessage {
            
            if let file = message.file {
                return .image(file)
            } else {
                return .unknow
            }
        } else if let message = self as? AVIMTextMessage {
            return .text(message.text ?? "")
        } else if let _ = self as? AVIMTypedMessage {
            return .unknow
        }
        
        return .text(self.content ?? "")
    }
}

protocol ChatManagerConversationDelegate: NSObjectProtocol {
    func conversation(_ conversation:ChatManager.Conversation, receiveNewMessage:AVIMMessage)
    
    func conversation(_ conversation:ChatManager.Conversation, messageDidDeliverd:AVIMMessage)
    
    func conversation(_ conversation:ChatManager.Conversation, message:AVIMMessage, deliveFailed:NSError)
}

extension ChatManager {
    
    class Conversation: NSObject, AVIMClientDelegate {
        
        let avconversation: AVIMConversation
        
        let chatdb:ChatDB
        
        var messages:[AVIMMessage]
        
        weak var delegate:ChatManagerConversationDelegate?
        
        init(avconversation: AVIMConversation, chatdb:ChatDB) {
            self.avconversation = avconversation
            
            self.chatdb = chatdb
            
            self.messages = []
            
            super.init()
            
            API
                .Avatar
                .getAvatar(from: avconversation.userId)
                .responseModel({ (avatar:API.Avatar) in
                    chatdb.update(nickName: avatar.nickName,
                                       avatarPath: avatar.userAvatar,
                                       of: avconversation.conversationId ?? "")
                    
                    ez.runThisInMainThread {
                        NotificationCenter
                            .default
                            .post(name: .ChatManagerUpdateConversationNotification,
                                  object: nil)
                    }
                })
        }
        
        func sendText(text:String) -> AVIMMessage {
            let localTime = Date().timeIntervalSince1970
            
            let avmessage = AVIMTextMessage(text: text,
                                            attributes: [AVIMMessage.localTimeKey: localTime])
            
            sendMessage(avmessage: avmessage)
            
            return avmessage
        }
        
        func sendImage(image:UIImage) -> AVIMMessage {
            let data = UIImageJPEGRepresentation(image,
                                                 0.7)!
            
            let avfile = AVFile(data: data)
            
            avfile.metaData = NSMutableDictionary(dictionary: ["width": image.size.width, "height": image.size.height])
            
            let localTime = Date().timeIntervalSince1970
            
            let avmessage = AVIMImageMessage(text: nil,
                                             file: avfile,
                                             attributes: [AVIMMessage.localTimeKey: localTime])
            
            sendMessage(avmessage: avmessage,
                        userInfo: ["size":image.size])
            
            return avmessage
        }
        
        private func sendMessage(avmessage:AVIMTypedMessage,
                                 userInfo:[String:Any] = [:]) {
//            avmessage.sendTimestamp = Int64(Date().timeIntervalSince1970 * 1000)
            
            messages.append(avmessage)
            
            chatdb.update(messageContent: avmessage.contentType.text ?? "",
                          messageDate: avmessage.time.date,
                          of: avconversation.conversationId ?? "")
            
            NotificationCenter.default.post(name: .ChatManagerReceiveMessageNotification,
                                            object: nil,
                                            userInfo: ["conversationId": avconversation.conversationId ?? "",
                                                       "message":avmessage])
            
            DispatchQueue.main.async {
                self.avconversation.send(avmessage,
                                         option: nil,
                                         callback: {[weak self] (succeeded, error) in
                                            
                                            guard let strongSelf = self else {
                                                return
                                            }
                                            
                                            if succeeded {
                                                strongSelf.delegate?.conversation(strongSelf, messageDidDeliverd: avmessage)
                                            } else {
                                                if let _error = error {
                                                    strongSelf.delegate?.conversation(strongSelf,
                                                                                      message: avmessage,
                                                                                      deliveFailed: _error as NSError)
                                                }
                                            }
                })
            }
        }
        
        func conversation(didReceiveCommonMessage message: AVIMMessage) {
            messages.append(message)
            
            self.delegate?.conversation(self,
                                        receiveNewMessage: message)
        }
        
        enum MessageQueryResult {
            case success([AVIMMessage])
            case failed(NSError)
        }
        
        typealias MessageQueryCallback = (MessageQueryResult) -> Void
        
        var isRequesting:Bool = false
        
        func beginQueryMessage(callback:@escaping MessageQueryCallback) {
            print("开始请求数据")
            
            if isRequesting {
                return
            }
            
            isRequesting = true
            
            self.avconversation.queryMessages(withLimit: 20) {
                [weak self] (messages, error) in
                
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.isRequesting = false
                
                if let _error = error {
                    callback(.failed(_error as NSError))
                } else {
                    
                    strongSelf.messages = messages?.map({ $0 as? AVIMMessage }).filter({ $0 != nil }).map({ $0! }) ?? []
                    
                    if let last = strongSelf.messages.last {
                        strongSelf.chatdb.update(messageContent: last.contentType.text ?? "",
                                                 messageDate: last.time.date,
                                                 of: strongSelf.avconversation.conversationId ?? "")
                    }
                    
                    callback(.success(strongSelf.messages))
                }
            }
        }
        
        func update(nickName:String, avatarPath:URL?) {
            API
                .Avatar
                .getAvatar(from: avconversation.userId)
                .responseModel({ (model:API.Avatar) in
                    self
                        .chatdb
                        .update(nickName: nickName,
                                avatarPath: avatarPath,
                                of: self.avconversation.conversationId ?? "")
                }, error: nil)
            
            var attributes = [String: String]()
            
            attributes[avconversation.userId] = nickName
            attributes[UserManager.shareInstance.currentId] = UserManager.shareInstance.currentNickName
            
            avconversation.setObject(attributes,
                                     forKey: "attributes")
            
            NotificationCenter
                .default
                .post(name: .ChatManagerUpdateConversationNotification,
                      object: avconversation.conversationId ?? "")
        }
        
        
        func queryMoreMessages(callback:@escaping MessageQueryCallback) {
            print("请求更多数据")
            
            if isRequesting {
                return
            }
            
            guard let first = messages.first,
                let messageId = first.messageId else {
                print("没有数据")
                return
            }
            
            isRequesting = true
            
            print("请求在\(messageId)之前，时间在\(first.sendTimestamp)之前")
            
            self.avconversation.queryMessages(beforeId: messageId,
                                              timestamp: first.sendTimestamp,
                                              limit: 20) {
                                                [weak self] (messages, error) in
                                                
                                                guard let strongSelf = self else {
                                                    return
                                                }
                                                
                                                strongSelf.isRequesting = false
                                                
                                                if let _error = error {
                                                    
                                                    print("请求出错原因为\(_error)")
                                                    
                                                    callback(.failed(_error as NSError))
                                                    
                                                    return
                                                }
                                                
                                                
                                                let newMessages = messages?.map({ $0 as? AVIMMessage }).filter({ $0 != nil }).map({ $0! }) ?? []
                                                
                                                if newMessages.count == 0 {
                                                    print("没有更多数据了")
                                                } else {
                                                    print("返回\(newMessages.count)条数据")
                                                }
                                                
                                                strongSelf.messages = newMessages + strongSelf.messages
                                                
                                                callback(.success(newMessages))
            }
        }
    }
}

