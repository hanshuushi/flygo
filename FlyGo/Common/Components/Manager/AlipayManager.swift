//
//  AlipayManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/29.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

fileprivate let appSchemes = "zhiyouflygo"

fileprivate let processOrderNotification = "processOrderNotification"

enum AlipayResult<U> {
    case success(userInfo:U)
    case cancel
    case failed(error:String)
    
    var isSuccess:Bool {
        switch self {
        case .success(_):
            return true
        default:
            return false
        }
    }
    
    var userInfo:U {
        switch self {
        case .success(let userInfo):
            return userInfo
        default:
            fatalError()
        }
    }
}

class AlipayManager {
    static func urlHandler(url:URL) {
        /// 支付宝
        if let host = url.host, host == "safepay" {
            AlipaySDK
                .defaultService()
                .processOrder(withPaymentResult: url,
                              standbyCallback: { (resultDict) in
                                
                                NotificationCenter
                                    .default
                                    .post(name: Notification.Name(rawValue:processOrderNotification),
                                          object: resultDict)
                })
            
            var authCode = ""
            
            AlipaySDK
                .defaultService()
                .processAuthResult(url,
                                      standbyCallback: { (resultDict) in
                                        if let result = resultDict?["result"] as? String,
                                            result.length > 0 {
                                            let resultArr = result.components(separatedBy: "&")
                                            
                                            for subResult in resultArr {
                                                if subResult.length > 10 && subResult.hasPrefix("auth_code=") {
                                                    authCode = (subResult as NSString).substring(from: 10)
                                                }
                                            }
                                        }
                                        
                                        print("授权结果 authCode = \(authCode)")
                                        
                })
        }
    }
}

extension AlipayManager {
    
    
    struct PaymentTradeResponse: Model {
        var code:Int?
        
        var msg = ""
        
        var appId = ""
        
        var authAppId = ""
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            code      <-  (map)
        }
    }
    
    enum PaymentStatus {
        case success
        case error(NSError)
    }
    
    struct PaymentResult: Model {
        var resultStatus:Int = 0
        
        var trade:PaymentTradeResponse?
        
        var status:PaymentStatus
        
        init?(map: Map) {
            resultStatus                <-  (map["resultStatus"], API.IntTransform())
            
            trade                       <-  map["alipay_trade_app_pay_response"]
            
            status = resultStatus == 9000 ? .success : .error(NSError(domain: "com.alipay.failed",
                code: trade?.code ?? 0,
                userInfo: nil))
        }
        
        mutating func mapping(map: Map) {
            resultStatus                <-  (map["resultStatus"], API.IntTransform())
            
            trade                       <-  map["alipay_trade_app_pay_response"]
            
            status = resultStatus == 9000 ? .success : .error(NSError(domain: "com.alipay.failed",
                                                                      code: trade?.code ?? 0,
                                                                      userInfo: nil))
        }
    }
    
    class AlipayRequest: NSObject {
        
        static var shareRequests:[AlipayRequest] = []
        
        let callback:(PaymentResult?) -> Void
        
        init(order:String,
             callback:@escaping ((PaymentResult?) -> Void)) {
            
            self.callback = callback
            
            super.init()
            
            NotificationCenter
                .default.addObserver(self,
                                     selector: #selector(AlipayRequest.processOrder(notification:)),
                                     name: Notification.Name(rawValue:processOrderNotification),
                                     object: nil)
            
            AlipaySDK.defaultService().payOrder(order,
                                                fromScheme: appSchemes) {
                                                    [weak self] (resultDic) in
                                                    
                                                    if let strongSelf = self {
                                                        strongSelf.set(result: resultDic)
                                                    }
            }
        }
        
        deinit {
            NotificationCenter
                .default.removeObserver(self)
            
            print("alipay kill")
        }
        
        func set(result:Any?) {
            guard let dict = result as? [String: Any],
                let result = PaymentResult(JSON:dict) else {
                    self.callback(nil)
                    
                    AlipayRequest.shareRequests.removeFirst(self)
                    
                    return
            }
            
            if result.resultStatus != 4000 {
                callback(result)
                
                AlipayRequest.shareRequests.removeFirst(self)
            }
        }
        
        @objc
        func processOrder(notification:Notification) {
            set(result: notification.object)
        }
    }
    
    static func pay(order:String, callback:@escaping ((PaymentResult?) -> Void)) {
        AlipayRequest.shareRequests.append(AlipayRequest(order: order,
                                                         callback: callback))
    }
}

extension AlipayManager {
    static func payEvent<U>(order:String, userInfo:U) -> Observable<AlipayResult<U>> {
        return Observable.create({ (observer) -> Disposable in
            pay(order: order, callback: { (result) in
                if let _result = result {
                    switch _result.status {
                    case .success:
                        observer.onNext(AlipayResult.success(userInfo: userInfo))
                        observer.onCompleted()
                    case .error(let error):
                        observer.onNext(AlipayResult.failed(error: error.localizedDescription))
                        observer.onCompleted()
                    }
                } else {
                    observer.onError(NSError(domain: "com.alipay.failed",
                                             code: 0,
                                             userInfo: nil))
                }
            })
            
            return Disposables.create()
        })
    }
}
