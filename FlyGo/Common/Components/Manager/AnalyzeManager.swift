//
//  UMMobManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/4.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AnalyzeManager: NSObject, Manager {
    
    static func register(accountId:String) {
        TalkingDataAppCpa.onRegister(accountId)
    }
    
    static func login(accountId:String) {
        TalkingDataAppCpa.onLogin(accountId)
    }
    
    static func urlHandler(url:URL) {
        TalkingDataAppCpa.onReceiveDeepLink(url)
    }
    
    static func view(product:ProductDetailItem) {
        TalkingDataAppCpa.onViewItem(withCategory: product.typeName,
                                     itemId: product.standars.first?.productId ?? product.recId,
                                     name: product.title,
                                     unitPrice: Int32(product.rmb.value))
    }
    
    static func addToCartOf(product:ProductDetailItem, number:Int) {
        TalkingDataAppCpa.onAddItemToShoppingCart(withCategory: product.typeName,
                                                  itemId: product.standars.first?.productId ?? product.recId,
                                                  name: product.title,
                                                  unitPrice: Int32(product.rmb.value),
                                                  amount: Int32(number))
    }
    
    func viewCart() {
        guard let cart = shoppingCart.value else {
            return
        }
        
        TalkingDataAppCpa.onViewShoppingCart(cart)
    }
    
    let shoppingCart:Variable<TDShoppingCart?>
    
    let disposeBag:DisposeBag = DisposeBag()
    
    static let shareInstance:AnalyzeManager = AnalyzeManager()
    
    override init() {
        let cart = UserManager.shareInstance.isLogin.map({ (isLogined) -> TDShoppingCart? in
            if !isLogined {
                return nil
            }
            
            return TDShoppingCart.create()
        })
        
        shoppingCart = Variable(nil)
        
        cart
            .drive(shoppingCart)
            .addDisposableTo(disposeBag)
        
        super.init()
    }
    
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        
//        AFDE436C10194BA6A7520154CF4247AB
//        if let config = UMAnalyticsConfig.sharedInstance() {
//            config.appKey = "58772fa5bbea8352b000174e"
#if Release
    TalkingDataAppCpa.init("AFDE436C10194BA6A7520154CF4247AB",
                           withChannelId: "App Store")
//            config.channelId = "App Store"
#else
    TalkingDataAppCpa.init("AFDE436C10194BA6A7520154CF4247AB",
                           withChannelId: "Developer")
//            config.channelId = "Developer"
#endif
        
        let _ = AnalyzeManager.shareInstance
//            MobClick.start(withConfigure: config)
//            MobClick.setAppVersion(VersionManager.version)
//            
//            UserManager
//                .shareInstance
//                .isLogin
//                .asObservable()
//                .bind(onNext: { (isLogined) in
//                    if isLogined {
//                        MobClick.profileSignIn(withPUID: UserManager.shareInstance.currentTelephone)
//                    } else {
//                        MobClick.profileSignOff()
//                    }
//                }).addDisposableTo(disposeBag)
//        }
    }
}
