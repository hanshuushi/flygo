//
//  UserManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import KeychainAccess
import RxSwift
import RxCocoa
import EZSwiftExtensions
import Kingfisher

enum FlymanAuthState {
    case authing
    case authed
    case normal
    
    static func state(by userType:Int, and authState:Int) -> FlymanAuthState {
        
        if userType == 2 && authState == 3 {
            return .authed
        } else if authState == 2 {
            return .authing
        }
    
        return .normal
    }
}

class UserManager: Manager {
    
    static let shareInstance = UserManager()
    
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        let _ = shareInstance
    }
    
    init() {
        
        if let storeUserData = UserManager.getStoreData() {
            token                   = storeUserData.token
            customerId              = storeUserData.user.customerId
            
            nickNameVariable        = Variable(storeUserData.user.nickname)
            avatarVariable          = Variable(storeUserData.user.userAvatar)
            telephoneVariable       = Variable(storeUserData.user.mobile)
            areaCodeVariable        = Variable(storeUserData.user.areaCode)
            isLoginVariable         = Variable(true)
            flymanAuthStateVariable = Variable(.state(by: storeUserData.user.userType,
                                                      and: storeUserData.user.authState))
        } else {
            token                   = ""
            customerId              = ""
            
            nickNameVariable        = Variable("")
            avatarVariable          = Variable(nil)
            telephoneVariable       = Variable("")
            areaCodeVariable        = Variable(0)
            isLoginVariable         = Variable(false)
            flymanAuthStateVariable = Variable(.normal)
        }
        
        nickName  = nickNameVariable.asDriver()
        avatar    = avatarVariable.asDriver()
        telephone = telephoneVariable.asDriver()
        isLogin   = isLoginVariable.asDriver()
        
        HttpSession.getHeaderHandler = {
            
            var dict = ["Version": VersionManager.version]
            
            if !self.isLogined() {
                return dict
            }
            
            dict["Authorization"] = self.currentToken
            
            return dict
        }
        
        HttpSession.tokenInValidHandler = {
            
            self.tokenLock.lock()
            
            if !self.isLogined() {
                return
            }
            
            self.logout()
            
            ez.runThisInMainThread {
                let mainVC = MainVC.rootController()
                
                let tration = CATransition()
                
                tration.duration = 0.35
                tration.type = kCATransitionMoveIn
                tration.subtype = kCATransitionFromTop
                tration.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
                
                UIApplication.shared.keyWindow?.layer.add(tration, forKey: "tration")
                UIApplication.shared.keyWindow?.rootViewController = mainVC
                
                let vc = LoginVC.showLoginSession(on: mainVC)
                
                vc.showToast(text: "登录信息失效，请重新登录")
            }
            
            self.tokenLock.unlock()
        }
        
        if areaCodeVariable.value > 0 {
            self.verifyToken(tel: telephoneVariable.value,
                             code: areaCodeVariable.value)
        }
    }
    
    fileprivate let tokenLock = NSRecursiveLock()
    
    fileprivate let nickNameVariable:Variable<String>
    
    fileprivate let avatarVariable:Variable<URL?>
    
    fileprivate let telephoneVariable:Variable<String>
    
    fileprivate let areaCodeVariable:Variable<Int>
    
    fileprivate var token:String
    
    fileprivate var customerId:String
    
    fileprivate let isLoginVariable:Variable<Bool>
    
    fileprivate let flymanAuthStateVariable:Variable<FlymanAuthState>
    
    let nickName:Driver<String>
    
    let avatar:Driver<URL?>
    
    let telephone:Driver<String>
    
    var currentToken:String {
        return token
    }
    
    func changeToken(_ token:String) {
        self.token = token
    }
    
    var currentId:String {
        return customerId
    }
    
    var currentTelephone:String {
        return telephoneVariable.value
    }
    
    var flymanAuthState:FlymanAuthState {
        return flymanAuthStateVariable.value
    }
    
    let isLogin:Driver<Bool>
    
    func isLogined() -> Bool {
        return isLoginVariable.value
    }
    
    var currentNickName:String {
        return nickNameVariable.value
    }
    
    var avatarURL:URL? {
        return avatarVariable.value
    }
    
    var areaCode:Int {
        return areaCodeVariable.value
    }
}

extension UserManager {
    static let UserDefaultKey = "UserManagerDefaultStoreDataKey"
    
    static func store(userData:UserInfo) {
        DispatchQueue.global().async {
            guard let json = userData.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(json,
                                           forKey: UserDefaultKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    static func removeUserData() {
        UserDefaults.standard.removeObject(forKey: UserDefaultKey)
        UserDefaults.standard.synchronize()
    }
    
    static func getStoreData() -> UserInfo? {
        guard let json = UserDefaults.standard.value(forKey: UserDefaultKey) as? Data,
            let jsonString = String(data: json, encoding: .utf8) else {
                return nil
        }
        
        return UserInfo(JSONString: jsonString)
    }
    
    static func update(nickName:String) {
        DispatchQueue.global().async {
            guard let json = UserDefaults.standard.value(forKey: UserDefaultKey) as? Data,
                let jsonString = String(data: json, encoding: .utf8),
                var userData = UserInfo(JSONString: jsonString) else {
                    return
            }
            
            userData.user.nickname = nickName
            
            guard let jsonData = userData.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData,
                                           forKey: UserDefaultKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    static func update(avatarURL:URL) {
        DispatchQueue.global().async {
            guard let json = UserDefaults.standard.value(forKey: UserDefaultKey) as? Data,
                let jsonString = String(data: json, encoding: .utf8),
                var userData = UserInfo(JSONString: jsonString) else {
                    return
            }
            
            userData.user.userAvatar = avatarURL
            
            guard let jsonData = userData.toJSONString()?.data(using: .utf8) else {
                return
            }
            
            UserDefaults.standard.setValue(jsonData,
                                           forKey: UserDefaultKey)
            UserDefaults.standard.synchronize()
        }
    }
}

extension UserManager {
    typealias UserInfo = API.UserInfo
    
    func login(with userInfo:UserInfo) {
        UserManager.store(userData:userInfo)
        
        AnalyzeManager.login(accountId: userInfo.user.customerId)
        
        token = userInfo.token
        
        customerId = userInfo.user.customerId
        
        nickNameVariable.value = userInfo.user.nickname
        
        avatarVariable.value = userInfo.user.userAvatar
        
        telephoneVariable.value = userInfo.user.mobile
        
        flymanAuthStateVariable.value = .state(by: userInfo.user.userType,
                                               and: userInfo.user.authState)
        
        areaCodeVariable.value = userInfo.user.areaCode
        
        isLoginVariable.value = true
    }
    
    func validate(code:String, response:@escaping ((Error?) -> Void)) {
        let session = UserInfo.validateCode(code: code)
        
        session.responseNoModel({
            response(nil)
        }) { (error) in
            response(error)
        }
    }
    
    func getUserInfo(handler:@escaping ((Bool) -> Void)) {
        let session = UserInfo.getUserInfo()
        
        let token = currentToken
        
        session.responseModel({ (model:API.UserData) in
            UserManager.store(userData:UserInfo(userData: model, userToken: token))
            
            self.customerId = model.user.customerId
            
            self.nickNameVariable.value = model.user.nickname
            
            self.avatarVariable.value = model.user.userAvatar
            
            self.telephoneVariable.value = model.user.mobile
            
            self.flymanAuthStateVariable.value = .state(by: model.user.userType,
                                                        and: model.user.authState)
            
            self.areaCodeVariable.value = model.user.areaCode
            
            self.isLoginVariable.value = true
            
            handler(true)
        }) { (error) in
            handler(false)
        }
    }
    
    fileprivate func verifyToken(tel:String,
                             code:Int) {
        let session = UserInfo.checkLogin(areaCode: code,
                                          mobile: tel,
                                          imei: OpenUDID.value())
        
        session.responseModel({ (model:UserInfo) in
            UserManager.store(userData: model)
            
            AnalyzeManager.login(accountId: model.user.customerId)
            
            self.token = model.token
            
            self.customerId = model.user.customerId
            
            self.nickNameVariable.value = model.user.nickname
            
            self.avatarVariable.value = model.user.userAvatar
            
            self.telephoneVariable.value = model.user.mobile
            
            self.flymanAuthStateVariable.value = .state(by: model.user.userType,
                                                        and: model.user.authState)
            
            self.isLoginVariable.value = true
            
            
            self.areaCodeVariable.value = model.user.areaCode
            
            if let installationId = RemoteManager.shareInstance.installationId {
                API
                    .UserInfo
                    .save(installationId: installationId,
                          imei:OpenUDID.value())
                    .responseNoModel({
                        print("installationId 更新成功")
                    },
                                     error: { (error) in
                                        print("installationId 更新失败")
                    })
            }
        }) { (error) in
            
            switch error {
            case .requestFail(_):
                return
            default:
                UserManager.removeUserData()
                
                self.nickNameVariable.value = ""
                
                self.telephoneVariable.value = ""
                
                self.isLoginVariable.value = false
                
                self.avatarVariable.value = nil
                
                self.areaCodeVariable.value = 0
            }
        }
    }
}

extension UserManager {
    func logout() {
        UserManager.removeUserData()
        
        nickNameVariable.value = ""
        
        telephoneVariable.value = ""
        
        isLoginVariable.value = false
        
        avatarVariable.value = nil
        
        areaCodeVariable.value = 0
    }
}

extension UserManager {
    func update (nickName:String, response:@escaping (HttpError?) -> Void) {
        UserInfo.update(nickName: nickName,
                        userId: currentId)
            .responseNoModel({
                UserManager.update(nickName: nickName)
                
                self.nickNameVariable.value = nickName
                
                response(nil)
            }) { (error) in
                response(error)
        }
    }
    
    func update (avatar:API.UploaderImage, response:@escaping (HttpError?) -> Void) {
        UserInfo.update(avatarURL: avatar.urlString)
            .responseNoModel({
                UserManager.update(avatarURL: avatar.picurl)
                
                self.avatarVariable.value = avatar.picurl
                
                response(nil)
            }) { (error) in
                response(error)
        }
    }
}

fileprivate let imageViewKFTaskAssociatedKey = NSData(data: "imageViewKFTaskAssociatedKey".data(using: .utf8)!).bytes

fileprivate let getURLKFTaskAssociatedKey = NSData(data: "getURLKFTaskAssociatedKey".data(using: .utf8)!).bytes


extension UIImageView {
    private var kf_task:RetrieveImageTask? {
        get {
            guard let val = objc_getAssociatedObject(self, imageViewKFTaskAssociatedKey) as? RetrieveImageTask else {
                return nil
            }
            
            return val
        }
        set {
            objc_setAssociatedObject(self,
                                     imageViewKFTaskAssociatedKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    private var kf_session:HttpSession? {
        get {
            guard let val = objc_getAssociatedObject(self, imageViewKFTaskAssociatedKey) as? HttpSession else {
                return nil
            }
            
            return val
        }
        set {
            objc_setAssociatedObject(self,
                                     imageViewKFTaskAssociatedKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func stopAllRequest() {
        if let task = self.kf_task {
            task.cancel()
        }
        
        if let session = self.kf_session {
            session.dispose()
        }
    }
    
//    static var avatarURLShareDictionary:[String:URL?] = [:]
    
//    func setAvatar(from userId:String) {
//        stopAllRequest()
//        
//        if let url = UIImageView.avtarURL(from: userId) {
//            
//            setAvatar(url: url)
//            
//            return
//        }
//        
//        self.image = UIImage(named: "icon_default_avatar")
//        
//        self.kf_session = API.Avatar.getAvatar(from: userId)
//        self.kf_session!.responseModel({
//            [weak self] (avatar:API.Avatar) in
//            
//            UIImageView.avatarURLShareDictionary[userId] = avatar.userAvatar
//            
//            self?.setAvatar(url: avatar.userAvatar)
//        })
//    }
    
//    private static func avtarURL(from userId:String) -> URL? {
//        return avatarURLShareDictionary[userId].flatMap({ $0 })
//    }
    
    func setAvatar(url:URL?) {
        stopAllRequest()
        
        self.image = UIImage(named: "icon_default_avatar")
        
        guard let avatarUrl = url else {
            self.kf_task = nil
            
            return
        }
        
        self.kf_task = KingfisherManager
            .shared
            .retrieveImage(with: avatarUrl,
                           options: [KingfisherOptionsInfoItem.callbackDispatchQueue(DispatchQueue.global())],
                           progressBlock: nil,
                           completionHandler: {
                            [weak self] (image, _, _, _) in
                            
                            guard let avatarImage = image else {
                                self?.kf_task = nil
                                
                                return
                            }
                            
                            let rect:CGRect = {
                               () -> CGRect in
                                if avatarImage.size.width == avatarImage.size.height {
                                    return CGRect(x: 0, y: 0, width: avatarImage.size.width, height: avatarImage.size.width)
                                } else if avatarImage.size.width > avatarImage.size.height {
                                    return CGRect(x: (avatarImage.size.width - avatarImage.size.height) / 2.0,
                                                  y: 0,
                                                  width: avatarImage.size.height,
                                                  height: avatarImage.size.height)
                                } else {
                                    return CGRect(x: 0,
                                                  y: (avatarImage.size.height - avatarImage.size.width) / 2.0,
                                                  width: avatarImage.size.width,
                                                  height: avatarImage.size.width)
                                }
                            }()
                            
                            UIGraphicsBeginImageContextWithOptions(avatarImage.size,
                                                                   false,
                                                                   avatarImage.scale)
                            
                            UIBezierPath(roundedRect: rect, cornerRadius: rect.width / 2.0).addClip()
                            
                            avatarImage.draw(in: rect)
                            
                            guard let circleImage = UIGraphicsGetImageFromCurrentImageContext() else {
                                
                                UIGraphicsEndImageContext()
                                
                                self?.kf_task = nil
                                
                                return
                            }
                            
                            UIGraphicsEndImageContext()
                            
                            ez.runThisInMainThread {
                                self?.image = circleImage
                            }
            })
    }
}
