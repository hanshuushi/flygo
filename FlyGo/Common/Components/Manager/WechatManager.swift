//
//  WechatManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/19.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import Kingfisher
import EZSwiftExtensions

class WechatManager: NSObject, Manager {
    static let shareInstance = WechatManager()
    
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        WXApi.registerApp("wx6a2cf0c8770022ad")
    }
    
    static func urlHandler(url:URL) -> Bool {
        return WXApi.handleOpen(url, delegate: shareInstance)
    }
}

extension WechatManager {
    
    typealias ShareWebsiteItem = (title:String, imageUrl:URL?, url:String, content:String)
    
    func showShareProductActionSheet(on viewController:UIViewController,
                                     website:ShareWebsiteItem,
                                     sender:UIView) {
        #if IOS_SIMULATOR
            API.Coupon.shareCoupon()
        #else
            if !WXApi.isWXAppInstalled() || !WXApi.isWXAppSupport() {
                viewController.showToast(text: "请安装或更新最新版本的微信")
                
                return
            }
            
            let actionSheet = UIAlertController(title: "微信分享",
                                                message: "是否希望将\(website.title)分享至您的",
                preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "好友",
                                                style: .default,
                                                handler: {
                                                    [weak self] (_) in
                                                    self?.share(on:viewController,
                                                                website: website,
                                                                to: WXSceneSession)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "朋友圈",
                                                style: .default,
                                                handler: {
                                                    [weak self] (_) in
                                                    self?.share(on:viewController,
                                                                website: website,
                                                                to: WXSceneTimeline)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "取消",
                                                style: .cancel,
                                                handler: nil))
            
            if let popover = actionSheet.popoverPresentationController {
                popover.sourceView = sender
                popover.sourceRect = sender.bounds
            }
            
            viewController.present(actionSheet,
                                   animated: true,
                                   completion: nil)
        #endif
    }
    
    typealias ShareProductItem = (recId:String, picURL:URL?, productName:String)
    
    func showShareProductActionSheet(on viewController:UIViewController,
                                     productItem:ShareProductItem,
                                     sender:UIView) {
        
        if !WXApi.isWXAppInstalled() || !WXApi.isWXAppSupport() {
            viewController.showToast(text: "请安装或更新最新版本的微信")
            
            return
        }
        
        let actionSheet = UIAlertController(title: "微信分享",
                                            message: "是否希望将\(productItem.productName)分享至您的",
            preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "好友",
                                            style: .default,
                                            handler: {
                                                [weak self] (_) in
                                                self?.share(on:viewController,
                                                            product: productItem,
                                                            to: WXSceneSession)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "朋友圈",
                                            style: .default,
                                            handler: {
                                                [weak self] (_) in
                                                self?.share(on:viewController,
                                                            product: productItem,
                                                            to: WXSceneTimeline)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "取消",
                                            style: .cancel,
                                            handler: nil))
        
        if let popover = actionSheet.popoverPresentationController {
            popover.sourceView = sender
            popover.sourceRect = sender.bounds
        }
        
        viewController.present(actionSheet,
                               animated: true,
                               completion: nil)
    }
    
    typealias ShareModuleItem = (mdlId:String, picURL:URL?, mdlName:String)
    
    func showShareProductActionSheet(on viewController:UIViewController,
                                     moduleItem:ShareModuleItem,
                                     sender:UIView) {
        
        if !WXApi.isWXAppInstalled() || !WXApi.isWXAppSupport() {
            viewController.showToast(text: "请安装或更新最新版本的微信")
            
            return
        }
        
        let actionSheet = UIAlertController(title: "微信分享",
                                            message: "是否希望将\(moduleItem.mdlName)分享至您的",
            preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "好友",
                                            style: .default,
                                            handler: {
                                                [weak self] (_) in
                                                self?.share(on:viewController,
                                                            module: moduleItem,
                                                            to: WXSceneSession)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "朋友圈",
                                            style: .default,
                                            handler: {
                                                [weak self] (_) in
                                                self?.share(on:viewController,
                                                            module: moduleItem,
                                                            to: WXSceneTimeline)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "取消",
                                            style: .cancel,
                                            handler: nil))
        
        if let popover = actionSheet.popoverPresentationController {
            popover.sourceView = sender
            popover.sourceRect = sender.bounds
        }
        
        viewController.present(actionSheet,
                               animated: true,
                               completion: nil)
    }
    
    func share(on viewController:UIViewController,
               website:ShareWebsiteItem,
               to scene:WXScene) {
        
        let toast = viewController.showLoading()
        
        ez.runThisInBackground {
            let image =
                WechatManager.getThumbImage(with: website.imageUrl) ??
                    UIImage(named: "flygo_icon")!
            
            ez.runThisInMainThread {
                
                toast.dismiss(animated: true,
                              completion: nil)
                
                let message = WXMediaMessage()
                
                message.title = website.title
                message.description = website.content
                message.setThumbImage(image)
                
                let webpageObject = WXWebpageObject()
                
                webpageObject.webpageUrl = website.url
                
                message.mediaObject = webpageObject
                
                let req = SendMessageToWXReq()
                
                req.bText = false
                req.message = message
                req.scene = Int32(scene.rawValue)
                
                if WXApi.send(req) {
                    API.Coupon.shareCoupon()
                }
            }
        }
    }
    
    func share(on viewController:UIViewController,
               module:ShareModuleItem,
               to scene:WXScene) {
        
        let toast = viewController.showLoading()
        
        ez.runThisInBackground {
            let image =
                WechatManager.getThumbImage(with: module.picURL) ??
                    UIImage(named: "flygo_icon")!
            
            ez.runThisInMainThread {
                
                toast.dismiss(animated: true,
                              completion: nil)
                
                let message = WXMediaMessage()
                
                message.title = module.mdlName
                message.description = "免税店直供，小飞哥专送，又真又快又便宜，飞购帮你来拔草！"
                message.setThumbImage(image)
                
                let webpageObject = WXWebpageObject()
                
                webpageObject.webpageUrl = URLConfig.prefix + "activity/special/index.html?modularId=\(module.mdlId)"
                
                message.mediaObject = webpageObject
                
                let req = SendMessageToWXReq()
                
                req.bText = false
                req.message = message
                req.scene = Int32(scene.rawValue)
                
                WXApi.send(req)
            }
        }
    }
    
    func share(on viewController:UIViewController,
               product:ShareProductItem,
               to scene:WXScene) {
        
        let toast = viewController.showLoading()
        
        ez.runThisInBackground {
            let image =
                WechatManager.getThumbImage(with: product.picURL) ??
                    UIImage(named: "flygo_icon")!
            
            ez.runThisInMainThread {
                
                toast.dismiss(animated: true,
                              completion: nil)
                
                let message = WXMediaMessage()
                
                message.title = product.productName
                message.description = "免税店直供，小飞哥专送，快来飞购看看吧"
                message.setThumbImage(image)
                
                let webpageObject = WXWebpageObject()
                
                webpageObject.webpageUrl = URLConfig.prefix + "flyapp/detail.html?productRecId=\(product.recId)"
                
                message.mediaObject = webpageObject
                
                let req = SendMessageToWXReq()
                
                req.bText = false
                req.message = message
                req.scene = Int32(scene.rawValue)
                
                WXApi.send(req)
            }
        }
    }
    
    static func getThumbImage(with url:URL?) -> Image? {
        guard let imageURL = url else {
            return nil
        }
        
        var compression:CGFloat = 0.9
        
        guard let imageData = NSData(contentsOf: imageURL),
            let image = UIImage(data: imageData as Data),
            let pngData = UIImagePNGRepresentation(image),
            let pngImage = UIImage(data: pngData),
            let jpgData = UIImageJPEGRepresentation(pngImage,
                                                    compression) else {
            return nil
        }
        
        let maxFileSize:CGFloat = 32 * 1024
        
        let maxCompression:CGFloat = 0.1
        
        var compressedData = jpgData as NSData
        
        while CGFloat(compressedData.length) > maxFileSize && compression > maxCompression {
            compression -= 0.1
            
            guard let compressedJPGData = UIImageJPEGRepresentation(pngImage,
                                                                    compression) else {
                                        return nil
            }
            
            compressedData = compressedJPGData as NSData
        }
        
        if CGFloat(compressedData.length) > maxFileSize {
            return nil
        }
        
        return UIImage(data: compressedData as Data)
    }
}

extension WechatManager: WXApiDelegate {
    func onReq(_ req: BaseReq!) {
        print("req.type is \(req.type)")
    }
    
    func onResp(_ resp: BaseResp!) {
        print("resp.code is \(resp.errCode) resp.message is \(resp.errStr) resp.type is \(resp.type)")
    }
    
}
