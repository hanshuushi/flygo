//
//  NetworkManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/17.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import RxSwift

class NetworkManager {
    
    /// 网络请求状态
    static let networkStatus:Observable<AFNetworkReachabilityStatus> = {
        
        let subject = PublishSubject<AFNetworkReachabilityStatus>()
        
        AFNetworkReachabilityManager.shared().startMonitoring()
        
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange({
            subject.onNext($0)
        })
        
        return subject
    }()
}
