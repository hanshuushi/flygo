//
//  RemoteManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import UserNotifications
import RxSwift
import RxCocoa

class RemoteManager: NSObject, Manager {
    
    static let shareInstance = RemoteManager()
    
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        
        AVOSCloud.setApplicationId(URLConfig.leanCloudAPPId,
                                   clientKey: URLConfig.leanCloudAPPKey)
        
#if DEBUG
            AVOSCloud.setAllLogsEnabled(true)
#else
            AVOSCloud.setAllLogsEnabled(false)
#endif
        
        application.registerForRemoteNotifications()
        
        shareInstance.registerForRemoteNotification()
    }
    
    override init() {
        remoteEnable = Variable(false)
        
        super.init()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(RemoteManager.applicationWillEnterForeground(notification:)),
                                               name: .UIApplicationWillEnterForeground,
                                               object: nil)
        
        remoteEnable.asObservable().bind { (enable) in
            print("推送功能 \(enable ? "打开" : "关闭")")
        }.addDisposableTo(bag)
    }
    
    var installationId:String?
    
    let remoteEnable:Variable<Bool>
    
    let bag = DisposeBag()
}

extension RemoteManager {
    func applicationWillEnterForeground(notification:Notification) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                self.remoteEnable.value = settings.authorizationStatus == .authorized
            })
        } else {
            self.remoteEnable.value =
                (UIApplication.shared.currentUserNotificationSettings?.types.rawValue ?? 0) != 0
            
            print("value is \(self.remoteEnable.value)")
        }
    }
}

extension RemoteManager: UNUserNotificationCenterDelegate {
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            // iOS 10 With UserNotification
            let uncenter = UNUserNotificationCenter.current()
            
            uncenter.delegate = self
            uncenter.requestAuthorization(options: [.alert, .badge, .sound],
                                          completionHandler: { (granted, error) in
                                            self.remoteEnable.value = granted
                                            print(granted ? "授权成功" : "授权失败")
            })
            uncenter.getNotificationSettings(completionHandler: { (settings) in
                switch settings.authorizationStatus {
                case .authorized:
                    self.remoteEnable.value = true
                    print("已授权")
                case .denied:
                    self.remoteEnable.value = false
                    print("未授权")
                case .notDetermined:
                    self.remoteEnable.value = false
                    print("未选择")
                }
            })
        } else {
            // iOS 9 Or Prev With Old
            let setting = UIUserNotificationSettings.init(types: [.alert, .badge, .sound],
                                                          categories: nil)
            
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
        
        completionHandler(.alert)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func application(_ application: UIApplication,
                     didRegister notificationSettings: UIUserNotificationSettings) {
        self.remoteEnable.value = notificationSettings.types.rawValue != 0
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        AVOSCloud.handleRemoteNotifications(withDeviceToken: deviceToken) { installation in
            self.installationId = installation.objectId
            
            if let installationId = self.installationId, UserManager.shareInstance.isLogined() {
                API
                    .UserInfo
                    .save(installationId: installationId,
                          imei:OpenUDID.value())
                    .responseNoModel({
                        print("installationId 更新成功")
                    },
                                     error: { (error) in
                                        print("installationId 更新失败")
                    })
            }
        }
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        self.remoteEnable.value = false
    }
}
