//
//  SocialManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit


/// 分享类型
///
/// - weibo: 新浪微博
/// - qq: QQ
/// - wechat: 微信
enum SocialPlatform : UInt  {
    case weibo                                    = 1
    case wechatSession                            = 22
    case wechatTimeLine                           = 23
    
    
    /// 桥接为ShareSDK类型
    ///
    /// - Returns: 转化之后的SSDKPlatformType类型
    fileprivate func toShareSDKPlatformType () -> SSDKPlatformType {
        return SSDKPlatformType(rawValue: self.rawValue)!
    }

    
    /// 分享结果回调闭包
    fileprivate var shareStateChangedHandler:SSDKShareStateChangedHandler {
        get {
            return {
                (state : SSDKResponseState, _, entity : SSDKContentEntity?, error :Error?) in
                
                switch state{
                    
                case SSDKResponseState.success: print("分享成功")
                case SSDKResponseState.fail:    print("授权失败,错误描述:\(error)")
                case SSDKResponseState.cancel:  print("操作取消")
                    
                default:
                    break
                }
            }
        }
    }
    
    
    /// 图片类型的枚举
    ///
    /// - image: 图片，包含UIImage
    /// - url: URL路径，指向图片的路径
    /// - string: 路径的字符串，指向图片路径的字符串
    /// - array: 图片合集
    enum ImageType {
        case image (UIImage)
        case url (URL)
        case string (String)
        case array ([ImageType])
        
        
        /// 转为具体类型
        ///
        /// - Returns: 转化成ShareSDK Params所需要的Image类型
        func val() -> Any {
            switch self {
            case .image(let image):
                return image
            case .url(let url):
                return url
            case .string(let string):
                return string
            case .array(let array):
                return array.map({ $0.val() })
            }
        }
    }
    
    func shareEnable() -> NSError? {
        switch self {
        case .wechatSession, .wechatTimeLine:
            if !ShareSDK.isClientInstalled(.typeWechat) {
                return NSError(domain: "com.zhiyou.flygo.share",
                               code: 2,
                               userInfo: [NSLocalizedDescriptionKey:"微信还未安装，请安装微信^_^"])
            }
        default:
            return nil
        }
        
        return nil
    }
    
    func share(product:ShareProductItem) -> NSError? {
        guard let picUrl = product.picURL else {
            return NSError(domain: "com.zhiyou.flygo.share",
                           code: 1,
                           userInfo: [NSLocalizedDescriptionKey:"这个商品的信息有误T_T"])
        }
        
        if let error = shareEnable() {
            return error
        }
        
        let params = shareParamsFrom(text: "免税店直供，小飞哥专送，快来飞购看看吧",
                                                    images: .url(picUrl),
                                                    url: URL(string: URLConfig.prefix + "flyapp/detail.html?productRecId=\(product.recId)"),
                                                    title: product.productName)
        
        ShareSDK.share(SSDKPlatformType(rawValue: self.rawValue)!,
                       parameters: params) { (state, _, _, error) in
                        switch state {
                        case .fail:
                            print("error is \(error)")
                        case .success:
                            print("分享成功")
                        default:
                            break
                        }
        }
        
        return nil
    }
    
    /// 分享URL参数
    ///
    /// - Parameters:
    ///   - text: 分享内容
    ///   - images: 分享图片
    ///   - url: 分享网址
    ///   - title: 分享标题
    func shareParamsFrom(text:String?                  = nil,
                         images:ImageType,
                         url:URL?                    = nil,
                         title:String?               = nil
        ) -> NSMutableDictionary {
        
        let shareParames                          = NSMutableDictionary()
        
        shareParames.ssdkSetupShareParams(byText: text,
                                          images: images.val(),
                                          url: url,
                                          title: title,
                                          type: SSDKContentType.auto)
        
        
        return shareParames
    }

    
    typealias ShareProductItem = (recId:String, picURL:URL?, productName:String)
}

/// ShareSDK 管理模块
class SocialManager : Manager {
    
    static func urlHandler(url:URL) -> Bool {
        return WXApi.handleOpen(url, delegate: nil)
    }
    
    /// ShareSDK AppInfo 配置
    static let socialInfo                         = (
        shareSDK: (
            appKey:"1612c4710c1e0"
        ),
        weibo: (
            appKey:"618028075",
            appSecret:"d3f9640399b8f202e21f96627590b9ba",
            redirectUri:"http://www.iflygoo.com/"
        ),
        wechat: (
            appId:"wx6a2cf0c8770022ad",
            appSecret:"f649e929aaa3cd055fd2b2d156d1cff0"
        ),
//        qq: (
//            appId:"",
//            appSecret:""
//        ),
        authType:SSDKAuthTypeBoth
    )
    
    /// ShareSDK App启动配置，在AppDelegate的“application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)”函数中启动
    ///
    /// - Parameters:
    ///   - application:由AppDelegate传入
    ///   - launchOptions: 由AppDelegate传入
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        // 启动ShareSDK的配置
        ShareSDK.registerApp(socialInfo.shareSDK.appKey,
                             activePlatforms: [1,997],
                             onImport: { (platformType) in
                                switch (platformType) {
                                case .typeWechat:
                                    ShareSDKConnector.connectWeChat(WXApi.classForCoder())
//                                case .typeQQ:
//                                    ShareSDKConnector.connectQQ(QQApiInterface.classForCoder(), tencentOAuthClass: TencentOAuth.classForCoder())
                                case .typeSinaWeibo:
                                    ShareSDKConnector.connectWeibo(WeiboSDK.classForCoder())
                                default:
                                    break;
                                }
                                
        },
                             onConfiguration: { (platformType, appInfo) in
                                switch (platformType) {
                                case .typeWechat:
                                    let wechat    = socialInfo.wechat
                                    
                                    appInfo?.ssdkSetupWeChat(byAppId: wechat.appId,
                                                             appSecret: wechat.appSecret)
//                                case .typeQQ:
//                                    let qqInfo    = socialInfo.qq
//                                    
//                                    appInfo?.ssdkSetupWeChat(byAppId: qqInfo.appId,
//                                                             appSecret: qqInfo.appSecret)
                                case .typeSinaWeibo:
                                    let weiboInfo = socialInfo.weibo
                                    
                                    appInfo?.ssdkSetupSinaWeibo(byAppKey: weiboInfo.appKey,
                                                                appSecret: weiboInfo.appSecret,
                                                                redirectUri: weiboInfo.redirectUri, authType: socialInfo.authType)
                                default:
                                    break;
                                }
                                
        })
    }

}
