//
//  VersionManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/14.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import EZSwiftExtensions

class VersionManager: Manager {
    static func finishLaunching(at application: UIApplication, with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        API.Version.getVersionInfo().responseModel({ (version:API.VersionInfo) in
            if compare(version: version.appVersion.versionNumber) {
                ez.runThisAfterDelay(seconds: 1.0,
                                     after: {
                                        ez.runThisInMainThread {
                                            let alertVC = UIAlertController(title: nil,
                                                                            message: "有新版本\(version.appVersion.versionNumber!)更新，请前往AppStore更新",
                                                                            preferredStyle: .alert)
                                            
                                            alertVC.addAction(UIAlertAction(title: "暂不更新",
                                                                            style: .cancel,
                                                                            handler: nil))
                                            alertVC.addAction(UIAlertAction(title: "更新"
                                                , style: .default,
                                                  handler: { (_) in
                                                        UIApplication.shared.openURL(appStoreUrl)
                                            }))
                                            
                                            UIApplication.shared.keyWindow?.rootViewController?.presentVC(alertVC)
                                        }
                })
            }
        },
                                                   error: nil)
    }
    
    static var version:String {
        return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String).flatMap({ $0 }) ?? "1.0.0"
    }
    
    static var build:String {
        return (Bundle.main.infoDictionary?["CFBundleVersion"] as? String).flatMap({ $0 }) ?? "1"
    }
    
    static let appStoreUrl = URL(string:"https://itunes.apple.com/us/app/fei-gouflygo/id1191740163?l=zh&ls=1&mt=8")!

    static func compare(version:String) -> Bool {
        let tempArray = version.split(".")
        
        if tempArray.count != 3 {
            return false
        }
        
        let versionArray = self.version.split(".")
        
        if versionArray.count != 3 {
            return false
        }
        
        for index in 0..<3 {
            let versionValue = tempArray[index].intFormat().toInt() ?? 0
            
            let currentVersion = versionArray[index].intFormat().toInt() ?? 0
            
            if versionValue > currentVersion {
                return true
            } else if versionValue < currentVersion {
                return false
            }
        }
        
        return false
    }
}
