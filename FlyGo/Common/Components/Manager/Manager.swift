//
//  Manager.swift
//  FlyGo
//
//  Created by Latte on 2016/11/10.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

protocol Manager {
    static func finishLaunching (at application:UIApplication,
                          with launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
}
