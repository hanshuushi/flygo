//
//  NotificationManager.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/16.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class NotificationManager: Manager {
    static func finishLaunching(at application: UIApplication,
                                with launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        let _ = NotificationManager.shareInstance
    }
    
    static let shareInstance = NotificationManager()
    
    fileprivate let systemNotificationVarliable:Variable<Int>
    
    let systomUnread:Driver<Bool>
    
    fileprivate let orderNotificationVarliable:Variable<Int>
    
    let orderUnread:Driver<Bool>
    
    fileprivate let flygoNotificationVarliable:Variable<Int>
    
    let flygoUnread:Driver<Bool>
    
    fileprivate let messageNotificationVarliable:Variable<Int>
    
    let notificationUnread:Driver<Bool>
    
    init() {
        systemNotificationVarliable = Variable(0)
        
        systomUnread = systemNotificationVarliable.asDriver().map({ $0 > 0 }).distinctUntilChanged()
        
        orderNotificationVarliable = Variable(0)
        
        orderUnread = orderNotificationVarliable.asDriver().map({ $0 > 0 }).distinctUntilChanged()
        
        flygoNotificationVarliable = Variable(0)
        
        flygoUnread = flygoNotificationVarliable.asDriver().map({ $0 > 0 }).distinctUntilChanged()
        
        messageNotificationVarliable = Variable(0)
        
        let allCount = Driver.combineLatest(systemNotificationVarliable.asDriver(),
                                            orderNotificationVarliable.asDriver(),
                                            flygoNotificationVarliable.asDriver(),
                                            messageNotificationVarliable.asDriver(),
                                            resultSelector: { (system, order, recive, message) -> Int in
                                                return system + order + recive + message
        }).distinctUntilChanged()
        
        notificationUnread = allCount.map({ $0 > 0 }).distinctUntilChanged()
        
        disposeBag = DisposeBag()
        
        allCount
            .drive(onNext: { (notificationCount) in
                UIApplication.shared.applicationIconBadgeNumber = notificationCount
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        requestNotificationEvent = PublishSubject()
        
        addChatNotification()
        
        setupNotificationRefer()
        
        getNewMessage()
    }
    
    func clearNotification(type:MessageRequestType) {
        if !UserManager.shareInstance.isLogined() {
            return
        }
        
        let uid = UserManager.shareInstance.currentId
        
        switch type {
        case .systom:
            NotificationManager.setSystomUnread(count: 0, of: uid)
            
            systemNotificationVarliable.value = 0
        case .order:
            NotificationManager.setOrderUnread(count: 0, of: uid)
            
            orderNotificationVarliable.value = 0
        case .flygo:
            NotificationManager.setflygoUnread(count: 0, of: uid)
            
            flygoNotificationVarliable.value = 0
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    let disposeBag:DisposeBag
    
    let requestNotificationEvent:PublishSubject<Void>
}

extension NotificationManager {
    
    func addChatNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(NotificationManager.chatManagerUnreadCountChangeNotification(_:)),
                                               name: Notification.Name.ChatManagerUnreadCountChangeNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(NotificationManager.applicationWillEnterForeground(_:)),
                                               name: Notification.Name.UIApplicationWillEnterForeground,
                                               object: nil)
    }
    
    @objc
    func applicationWillEnterForeground(_ notification:Notification) {
        requestNotificationEvent.onNext()
    }
    
    @objc
    func chatManagerUnreadCountChangeNotification(_ notification:Notification) {
        if let db = ChatManager.currentSession?.chatDB {
            let count = db.queryUnreadCount()
            
            self.messageNotificationVarliable.value = count
        } else {
            self.messageNotificationVarliable.value = 0
        }
    }
}

// MARK: - 计时器相关
extension NotificationManager {
    func getNewMessage() {
        let requestNotificationEvent = self.requestNotificationEvent
        
        let timerAction = UserManager
            .shareInstance
            .isLogin
            .asObservable()
            .map { (isLogined) -> Observable<[API.UnreadMessage]> in
                if !isLogined {
                    return Observable.empty()
                }
                
                let timer = Observable<Int>
                    .timer(0,
                           period: 300,
                           scheduler: MainScheduler.asyncInstance)
                    .map({ _ in ()})
                
                return Observable
                    .of(timer, requestNotificationEvent)
                    .merge()
                    .flatMapLatest({ _ in
                        API
                            .UnreadMessage
                            .getNewMessage()
                            .asModelsObservable()
                            .shareReplay(1)
                    })
        }
        
        timerAction
            .switchLatest()
            .bind { (list) in
                
                let uid = UserManager.shareInstance.currentId
                
                for one in list {
                    switch one.messageType! {
                    case .systom:
                        self.systemNotificationVarliable.value = one.messageCount
                        
                        NotificationManager.setSystomUnread(count: one.messageCount,
                                                            of: uid)
                    case .order:
                        self.orderNotificationVarliable.value = one.messageCount
                        
                        NotificationManager.setOrderUnread(count: one.messageCount,
                                                           of: uid)
                    case .flygo:
                        self.flygoNotificationVarliable.value = one.messageCount
                        
                        NotificationManager.setflygoUnread(count: one.messageCount,
                                                            of: uid)
                    }
                }
            }.addDisposableTo(disposeBag)
    }
}


// MARK: - UserDefault相关
extension NotificationManager {
    
    func setupNotificationRefer() {
        UserManager
            .shareInstance
            .isLogin
            .drive(onNext: { (isLogined) in
                if isLogined {
                    let userId = UserManager.shareInstance.currentId
                    
                    self.systemNotificationVarliable.value = NotificationManager.systomUnreadCount(of: userId)
                    self.orderNotificationVarliable.value = NotificationManager.orderUnreadCount(of: userId)
                    self.flygoNotificationVarliable.value = NotificationManager.flygoUnreadCount(of: userId)
                    
                    
                } else {
                    self.systemNotificationVarliable.value = 0
                    self.orderNotificationVarliable.value = 0
                    self.flygoNotificationVarliable.value = 0
                }
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
    
    static func systomUnreadCount(of userId:String) -> Int {
        return valueForKey(userDefaults: UserDefaults.standard,
                           key: "systom",
                           of: userId)
    }
    
    static func valueForKey(userDefaults:UserDefaults, key:String, of userId:String) -> Int {
        guard let dict = userDefaults.value(forKey: "notifcation_count") as? [String:Any] else {
            return 0
        }
        
        guard let dictOfMine = dict["user_\(userId)"] as? [String:Int] else {
            return 0
        }
        
        guard let value = dictOfMine[key] else {
            return 0
        }
        
        return value
    }
    
    static func setValue(userDefaults:UserDefaults, of userId:String, key:String, value:Int) {
        if var dict = userDefaults.value(forKey: "notifcation_count") as? [String:Any] {
            
            guard var dictOfMy = dict["user_\(userId)"] as? [String:Int] else {
                
                var dictOfMy = ["systom":0, "order":0, "recive":0]
                
                dictOfMy[key] = value
                
                dict["user_\(userId)"] = dictOfMy
                
                userDefaults.setValue(NSDictionary(dictionary: dict),
                                      forKeyPath: "notifcation_count")
                userDefaults.synchronize()
                return
            }
            
            if dictOfMy["systom"] == nil {
                dictOfMy["systom"] = 0
            }
            
            if dictOfMy["order"] == nil {
                dictOfMy["order"] = 0
            }
            
            if dictOfMy["flygo"] == nil {
                dictOfMy["flygo"] = 0
            }
            
            dictOfMy[key] = value
            
            dict["user_\(userId)"] = dictOfMy
            
            userDefaults.setValue(NSDictionary(dictionary: dict),
                                  forKey: "notifcation_count")
            userDefaults.synchronize()
        } else {
            
            var dictOfMy = ["systom":0, "order":0, "recive":0]
            
            dictOfMy[key] = value
            
            let dict = ["user_\(userId)":dictOfMy]
            
            userDefaults.setValue(NSDictionary(dictionary: dict),
                                  forKey: "notifcation_count")
            userDefaults.synchronize()
        }
    }
    
    static func setSystomUnread(count:Int, of userId:String) {
        let userDefaults = UserDefaults.standard
        
        setValue(userDefaults: userDefaults,
                   of: userId,
                   key: "systom",
                   value: count)
    }
    
    static func orderUnreadCount(of userId:String) -> Int {
        return valueForKey(userDefaults: UserDefaults.standard,
                           key: "order",
                           of: userId)
    }
    
    static func setOrderUnread(count:Int, of userId:String) {
        let userDefaults = UserDefaults.standard
        
        setValue(userDefaults: userDefaults,
                 of: userId,
                 key: "order",
                 value: count)
    }
    
    static func flygoUnreadCount(of userId:String) -> Int {
        return valueForKey(userDefaults: UserDefaults.standard,
                           key: "flygo",
                           of: userId)
    }
    
    static func setflygoUnread(count:Int, of userId:String) {
        let userDefaults = UserDefaults.standard
        
        setValue(userDefaults: userDefaults,
                 of: userId,
                 key: "flygo",
                 value: count)
    }
    
    
}

