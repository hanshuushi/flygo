//
//  TableViewDataSet.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/6.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum DataSetRequestStatus: Equatable {
    case normal
    case error(String)
    case loading
    
    var rawValue:Int {
        switch self {
        case .normal:
            return 1
        case .error(_):
            return -1
        case .loading:
            return 0
        }
    }
    
    var errorString:String? {
        switch self {
        case .error(let errorString):
            return errorString
        default:
            return nil
        }
    }
    
    static func ==(lhs: DataSetRequestStatus, rhs: DataSetRequestStatus) -> Bool {
        if lhs.rawValue == -1 && rhs.rawValue == -1 {
            return lhs.errorString == lhs.errorString
        }
        
        return lhs.rawValue == rhs.rawValue
    }
}

enum DataSetLoadFinishStyle {
    case none
    case white
    case gray
}

extension Array where Element == Observable<DataSetRequestStatus> {
    func merge() -> Observable<DataSetRequestStatus> {
        return Observable<DataSetRequestStatus>
            .combineLatest(self) { (collection) -> DataSetRequestStatus in
            
                var error:String? = nil
                
                for one in collection {
                    if one.rawValue == 0 {
                        return .loading
                    }
                    
                    if let errorString = one.errorString {
                        error = errorString
                    }
                }
                
                if let errorString = error {
                    return .error(errorString)
                }
                
                return .normal
        }
    }
}

protocol DataSetPageCollection: ModelType {
    associatedtype DataSetPageItem: ModelType
    
    var list:[DataSetPageItem] { get }
    
    var hasMore:Bool { get }
    
    var otherInfo:Any? { get }
}

protocol DataSetRequestItem {
    var requestStatus:DataSetRequestStatus { get }
}

extension APIItem: DataSetRequestItem {
    var requestStatus:DataSetRequestStatus {
        switch self {
        case .validating:
            return .loading
        case .failed(let error):
            return .error(error)
        case .success(_):
            return .normal
        }
    }
}

extension ObservableType where E: DataSetRequestItem {
    func asRequestStatusObservable() -> Observable<DataSetRequestStatus> {
        return self.map({ $0.requestStatus })
    }
}

class TableViewDataSet: NSObject, Disposable {
    
    func dispose() {
        
    }
    
    let tableView:UITableView
    
    let status:Variable<DataSetRequestStatus>
    
    let isEmpty:Variable<Bool>
    
    let canLoadMore:Variable<Bool>
    
    let pageIndex:Variable<Int>
    
//    unowned let delegate:UITableViewDelegate
//    
//    unowned let dataSource:UITableViewDataSource
    
    let refreshAction:PublishSubject<Void>
    
    let disposeBag:DisposeBag
    
    var refreshEnable:Bool = false {
        didSet {
            if refreshEnable {
                guard let header = tableView.mj_header else {
                                                    
                                                    tableView.mj_header = RefreshHeader(refreshingTarget: self,
                                                                                        refreshingAction: #selector(TableViewDataSet.refreshingAction))
                                                    
                                                    return
                }
                
                header.endRefreshing()
            } else {
                if let header = tableView.mj_header {
                    header.endRefreshing()
                }
                
                tableView.mj_header = nil
            }
        }
    }
    
    func refreshingAction() {
        refreshAction.onNext()
        
        if let footer = tableView.mj_footer {
            footer.endRefreshing()
            
            tableView.mj_footer = nil
        }
    }
    
    func loadingMoreAction() {
        loadMoreAction.onNext(pageIndex.value + 1)
    }
    
    let loadMoreAction:PublishSubject<Int>
    
    func refrence<M:ModelType>(getRequest:@escaping (Void) -> Observable<APIItem<M>>,
                  otherRequest:Observable<Void> = Observable.empty(),
                  isEmpty:@escaping (M) -> Bool,
                  in disposeBag:DisposeBag) -> Observable<M> {
        self.refreshEnable = true
        
        self.canLoadMore.value = false
        
        /// FistRequest
        let request = getRequest().shareReplay(1)
        
        /// Other Request
        let other = otherRequest
            .map({ getRequest().shareReplay(1) })
            .shareReplay(1)
        
        Observable
            .of(request, other.switchLatest())
            .merge()
            .asRequestStatusObservable()
            .bind(to: self.status)
            .addDisposableTo(disposeBag)
        
        /// Refresh
        let refreshRequest = Observable.of(self
            .refreshAction
            .map({ getRequest()
                .asItemObservable()
                .retry()
                .shareReplay(1)
            }),
                                           other.map({ $0.asModelObservable() }))
            .merge()
            .startWith(request.asModelObservable())
            .switchLatest()
            .shareReplay(1)
        
        refreshRequest
            .map({ isEmpty($0) })
            .bind(to: self.isEmpty)
            .addDisposableTo(disposeBag)
        
        disposeBag.insert(self)
        
        return refreshRequest
    }
    
    func refrence<M:Model>(getRequest:@escaping (Void) -> Observable<APIItem<ModelList<M>>>,
                  otherRequest:Observable<Void> = Observable.empty(),
                  listCallBack:@escaping ([M]) -> Void) -> DisposeBag {
        
        self.refreshEnable = true
        
        self.canLoadMore.value = false
        
        let bag = DisposeBag()
        
        /// FistRequest
        let request = getRequest().shareReplay(1)
        
        /// Other Request
        let other = otherRequest
            .map({ getRequest().shareReplay(1) })
            .shareReplay(1)
        
        Observable
            .of(request, other.switchLatest())
            .merge()
            .asRequestStatusObservable()
            .bind(to: self.status)
            .addDisposableTo(bag)
        
        /// Refresh
        let refreshRequest = Observable.of(self
            .refreshAction
            .map({ getRequest()
                .asItemObservable()
                .retry()
                .shareReplay(1)
            }),
                                           other.map({ $0.asModelObservable() }))
            .merge()
            .startWith(request.asModelObservable())
            .switchLatest()
            .shareReplay(1)
        
        let list = refreshRequest
            .map({ $0.list })
            .shareReplay(1)
        
        list
            .bind { (productList) in
                listCallBack(productList)
            }
            .addDisposableTo(bag)
        
        list
            .map({ $0.count <= 0 })
            .bind(to: self.isEmpty)
            .addDisposableTo(bag)
        
        bag.insert(self)
        
        return bag
    }
    
    func refrence<C:DataSetPageCollection>(getRequestWithPage:@escaping (Int) -> Observable<APIItem<C>>,
                  otherRequest:Observable<Void> = Observable.empty(),
                  listCallBack:@escaping ([C.DataSetPageItem]) -> Void,
                  otherInfoCallBack: ((Any?) -> Void)? = nil) -> DisposeBag {
        
        self.refreshEnable = true
        
        let bag = DisposeBag()
        
        /// FistRequest
        let request = getRequestWithPage(1).shareReplay(1)
        
        /// Other Request
        let other = otherRequest
            .map({ getRequestWithPage(1).shareReplay(1) })
            .shareReplay(1)
        
//        Observable
//            .of(request, other.switchLatest())
//            .merge()
//            .asRequestStatusObservable()
//            .bind(to: self.status)
//            .addDisposableTo(bag)
//        
        
        other
            .startWith(request)
            .switchLatest()
            .asRequestStatusObservable()
            .bind(to: self.status)
            .addDisposableTo(bag)
        
        /// Refresh
        let refreshRequest = Observable.of(self
            .refreshAction
            .map({ getRequestWithPage(1)
                .asItemObservable()
                .retry()
                .shareReplay(1)
            }),
                                           other.map({ $0.asModelObservable() }))
            .merge()
            .startWith(request.asModelObservable())
            .switchLatest()
            .shareReplay(1)
        
        let allCollection = refreshRequest
            .flatMapLatest {
                (collection) -> Observable<(list:[C.DataSetPageItem], page:Int, canLoadMore:Bool, otherInfo:Any?)> in
                
                let model = self
                    .loadMoreAction
                    .map({ (page) -> Observable<C> in
                        
                        return getRequestWithPage(page)
                            .asItemObservable()
                            .retry()
                            .shareReplay(1)
                    })
                    .concat()
                    .startWith(collection)
                    .shareReplay(1)
                
                return model.map({ (list:$0.list,
                                    page:1,
                                    canLoadMore:$0.hasMore,
                                    otherInfo:$0.otherInfo) })
                    .scan((list:[], page:0, canLoadMore:false, otherInfo:nil),
                          accumulator: { (ori, new)  in
                            return (list:ori.list + new.list,
                                    page:ori.page + new.page,
                                    canLoadMore:new.canLoadMore,
                                    otherInfo:new.otherInfo)
                            })
                    .shareReplay(1)
            }
            .shareReplay(1)
        
        
        let list = allCollection
            .map({ $0.list })
            .shareReplay(1)
        
        list
            .bind { (productList) in
                listCallBack(productList)
            }
            .addDisposableTo(bag)
        
        allCollection
            .map({ $0.otherInfo })
            .bind { (otherInfo) in
                otherInfoCallBack?(otherInfo)
            }
            .addDisposableTo(bag)
        
        list
            .map({
                $0.count <= 0
            })
            .bind(to: self.isEmpty)
            .addDisposableTo(bag)
        
        allCollection
            .map({ $0.canLoadMore })
            .bind(to: self.canLoadMore)
            .addDisposableTo(bag)
        
        allCollection
            .map({ $0.page })
            .bind(to: self.pageIndex)
            .addDisposableTo(bag)
        
        bag.insert(self)
        
        return bag
    }
    
    init(_ tableView:UITableView,
         delegate:UITableViewDelegate,
         dataSource:UITableViewDataSource,
         finishStyle:DataSetLoadFinishStyle,
         refreshEnable:Bool = true) {
        self.tableView = tableView
        
        self.status = Variable(.loading)
        
        self.isEmpty = Variable(true)
        
        self.refreshAction = PublishSubject()
        
        self.loadMoreAction = PublishSubject()
        
        self.canLoadMore = Variable(false)
        
        self.pageIndex = Variable(1)
        
        self.disposeBag = DisposeBag()
        
        super.init()
        
        unowned let _delegate = delegate
        
        unowned let _dataSource = dataSource
        
        let storeBackgroudView = tableView.backgroundView
        
        self.tableView.registerView(viewClass: LoadingHeaderView.self)
        self.tableView.registerView(viewClass: PicHeaderView.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        
        Observable
            .combineLatest(self.status.asObservable().distinctUntilChanged(),
                           self.isEmpty.asObservable(),
                           self.canLoadMore.asObservable().distinctUntilChanged())
//            .throttle(0.1, latest: true, scheduler: MainScheduler.instance)
            .debounce(0.1,
                      scheduler: MainScheduler.instance)
            .bind {
                [weak self]
                (status, isEmpty, canLoadMore) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if !isEmpty && status.rawValue == 1 {
                    strongSelf.refreshEnable = refreshEnable
                    strongSelf.tableView.delegate = _delegate
                    strongSelf.tableView.dataSource = _dataSource
                    strongSelf.tableView.backgroundView = storeBackgroudView
                } else {
                    strongSelf.refreshEnable = false
                    strongSelf.tableView.delegate = self
                    strongSelf.tableView.dataSource = self
                    strongSelf.tableView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
                }
                
                
                let loadMore = status.rawValue == 1 && !isEmpty && canLoadMore
                
                if status.rawValue == 1 && !isEmpty {
                    if loadMore {
                        guard let footer = tableView.mj_footer else {
                            tableView.mj_footer = LoadmoreFooter(refreshingTarget: strongSelf,
                                                                 refreshingAction: #selector(TableViewDataSet.loadingMoreAction))
                            
                            strongSelf.tableView.reloadData()
                            
                            return
                        }
                        
                        footer.endRefreshing()
                    } else {
                        if let footer = tableView.mj_footer {
                            footer.endRefreshing()
                        }
                        
                        tableView.mj_footer = LoadfinishFooter(style: finishStyle)
                    }
                } else {
                    tableView.mj_footer = nil
                }
                
                strongSelf.tableView.reloadData()
        }
            .addDisposableTo(self.disposeBag)
    }
    
    
    var loadingViewHandler:((Void) -> (UIView))?
    var emptyImageHandler:((Void) -> UIImage)?
    var emptyDescriptionHandler:((Void) -> String)?
    var emptyViewHandler:((Void) -> (UIView))?
}

extension TableViewDataSet {
    class LoadingHeaderView: UITableViewHeaderFooterView {
        override init(reuseIdentifier: String?) {
            super.init(reuseIdentifier: reuseIdentifier)
            
            let loadingView = ActivityIndicatorView()
            
            self.contentView.addSubview(loadingView)
            
            loadingView.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(self.contentView)
                maker.top.equalTo(self.contentView).offset(20)
                maker.bottom.equalTo(self.contentView).offset(-20)
            }
            
            loadingView.play()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension TableViewDataSet {
    
    class RefreshHeader: MJRefreshHeader {
        
        let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                      textColor: UIConfig.generalColor.labelGray,
                                      backgroudColor:.clear)
        
        let loading = ActivityIndicatorView()
        
        override func prepare() {
            super.prepare()
        
            // 设置控件高度
            self.mj_h = 70
            
            // 增加控件
            self.addSubviews(label, loading)
            
            self.isAutomaticallyChangeAlpha = true
        }
        
        override func placeSubviews() {
            super.placeSubviews()
            
            //
            self.label.sizeToFit()
            self.label.centerX = self.bounds.midX
            self.label.bottom = self.bounds.maxY - 10
            
            //
            self.loading.centerX = self.bounds.midX
            self.loading.bottom = self.label.top - 5
        }
        
        override var state: MJRefreshState {
            get {
                return super.state
            }
            set {
                super.state = newValue
                
                switch newValue {
                case .idle:
                    self.loading.pause()
                    self.label.text = "赶紧下拉吖"
                case .pulling:
                    self.loading.play()
                    self.label.text = "赶紧放开我吧"
                case .refreshing:
                    self.loading.play()
                    self.label.text = "加载数据中"
                default:
                    break
                }
            }
        }
    }
}

extension TableViewDataSet {
    class LoadmoreFooter: MJRefreshAutoFooter {
        let loading = ViewFactory.loadingView()
        
        override func prepare() {
            super.prepare()
            
            self.mj_h = 50.0
            
            self.addSubview(loading)
            
            self.isAutomaticallyChangeAlpha = true
        }
        
        override func placeSubviews() {
            super.placeSubviews()
            
            loading.centerInSuperView()
        }
    }
}

extension TableViewDataSet {
    class LoadfinishFooter: MJRefreshAutoFooter {
        init?(style:DataSetLoadFinishStyle) {
            super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width,
                                     height: 40))
            
            var color = UIConfig.generalColor.unselected
            
            switch style {
            case .white:
                color = UIConfig.generalColor.whiteGray
                
                self.backgroundColor = UIConfig.generalColor.white
            case .gray:
                self.backgroundColor = UIConfig.generalColor.backgroudWhite
            default:
                return nil
            }
            
            let label = ViewFactory.generalLabel(generalSize: 12,
                                                 textColor: color,
                                                 backgroudColor: UIConfig.generalColor.backgroudWhite)
            
            self.addSubview(label)
            
            label.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(self)
                maker.centerY.equalTo(self)
            }
            
            label.text = "已经到底了"
            
            let leftLine = ViewFactory.line()
            
            self.addSubview(leftLine)
            
            leftLine.snp.makeConstraints { (maker) in
                maker.height.equalTo(0.5)
                maker.left.equalTo(self).offset(15)
                maker.right.equalTo(label.snp.left).offset(-15)
                maker.centerY.equalTo(label)
            }
            
            let rightLine = ViewFactory.line()
            
            self.addSubview(rightLine)
            
            rightLine.snp.makeConstraints { (maker) in
                maker.height.equalTo(0.5)
                maker.right.equalTo(self).offset(-15)
                maker.left.equalTo(label.snp.right).offset(15)
                maker.centerY.equalTo(label)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension TableViewDataSet {
    class PicHeaderView: UITableViewHeaderFooterView {
        
        let imageView:UIImageView
        
        let label:UILabel
        
        override init(reuseIdentifier: String?) {
            
            imageView = UIImageView()
            
            label = ViewFactory.label(font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.whiteGray)
            label.numberOfLines = 0
            
            super.init(reuseIdentifier: reuseIdentifier)
            
            self.contentView.addSubviews(imageView, label)
            
            imageView.snp.makeConstraints { (maker) in
                maker.top.equalTo(self.contentView).offset(100)
//                maker.bottom.equalTo(self.contentView.snp.centerY)
                maker.centerX.equalTo(self.contentView)
                maker.left.greaterThanOrEqualTo(self.contentView).offset(15)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-15)
            }
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(imageView.snp.bottom).offset(30)
                maker.centerX.equalTo(self.contentView)
                maker.left.greaterThanOrEqualTo(self.contentView).offset(15)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-15)
                maker.bottom.bottom.equalTo(self.contentView).offset(0)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension TableViewDataSet: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch status.value {
        case .normal:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension TableViewDataSet: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        switch status.value {
        case .normal:
            if isEmpty.value {
                return 350
            }
            return 50
        case .loading:
            return 50
        case .error(_):
            return 350
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch status.value {
        case .normal:
            if isEmpty.value {
                if let emptyView = emptyViewHandler?() {
                    return emptyView
                }
                
                let picHeaderView:PicHeaderView = tableView.dequeueReusableView()
                
                picHeaderView.imageView.image = emptyImageHandler?() ?? UIImage(named: "icon_no_data")
                picHeaderView.label.text = emptyDescriptionHandler?() ?? "数据还在生产中..."
                
                return picHeaderView
            }
            
            return loadingViewHandler?() ?? tableView.dequeueReusableView() as LoadingHeaderView
        case .loading:
            return loadingViewHandler?() ?? tableView.dequeueReusableView() as LoadingHeaderView
        case .error(let errorString):
            let picHeaderView:PicHeaderView = tableView.dequeueReusableView()
            
            picHeaderView.imageView.image = UIImage(named: "icon_interface_error")
            picHeaderView.label.text = errorString
            
            return picHeaderView
        }
    }
}

