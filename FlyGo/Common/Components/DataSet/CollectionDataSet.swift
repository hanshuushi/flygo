//
//  CollectionDataSet.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/6.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CollectionDataSet: NSObject, Disposable {
    
    func dispose() {
        
    }
    
    let collectionView:UICollectionView
    
    let status:Variable<DataSetRequestStatus>
    
    let isEmpty:Variable<Bool>
    
    let canLoadMore:Variable<Bool>
    
    let pageIndex:Variable<Int>
    
//    unowned let delegate:UIcollectionViewDelegate
//    
//    unowned let dataSource:UIcollectionViewDataSource
    
    let refreshAction:PublishSubject<Void>
    
    let disposeBag:DisposeBag
    
    var refreshEnable:Bool = false {
        didSet {
            if refreshEnable {
                guard let header = collectionView.mj_header else {
                    
                                                    collectionView.mj_header = TableViewDataSet.RefreshHeader(refreshingTarget: self,
                                                                                        refreshingAction: #selector(CollectionDataSet.refreshingAction))
                                                    
                                                    return
                }
                
                header.endRefreshing()
            } else {
                if let header = collectionView.mj_header {
                    header.endRefreshing()
                }
                
                collectionView.mj_header = nil
            }
        }
    }
    
    func refreshingAction() {
        refreshAction.onNext()
        
        if let footer = collectionView.mj_footer {
            footer.endRefreshing()
            
            collectionView.mj_footer = nil
        }
    }
    
    func loadingMoreAction() {
        loadMoreAction.onNext(pageIndex.value + 1)
    }
    
    let loadMoreAction:PublishSubject<Int>
    
    func refrence<M:ModelType>(getRequest:@escaping (Void) -> Observable<APIItem<M>>,
                  isEmpty:@escaping (M) -> Bool,
                  in disposeBag:DisposeBag) -> Observable<M> {
        self.refreshEnable = true
        
        self.canLoadMore.value = false
        
        /// FistRequest
        let request = getRequest().shareReplay(1)
        
        request
            .asRequestStatusObservable()
            .bind(to: self.status)
            .addDisposableTo(disposeBag)
        
        /// Refresh
        let refreshRequest = self
            .refreshAction
            .map({ getRequest()
                .asItemObservable()
                .retry()
                .shareReplay(1)
            })
            .startWith(request.asModelObservable())
            .switchLatest()
            .shareReplay(1)
        
        refreshRequest
            .map({ isEmpty($0) })
            .bind(to: self.isEmpty)
            .addDisposableTo(disposeBag)
        
        disposeBag.insert(self)
        
        return refreshRequest
    }
    
    func refrence<M:Model>(getRequest:@escaping (Void) -> Observable<APIItem<ModelList<M>>>,
                  listCallBack:@escaping ([M]) -> Void) -> DisposeBag {
        
        self.refreshEnable = true
        
        self.canLoadMore.value = false
        
        let bag = DisposeBag()
        
        /// FistRequest
        let request = getRequest().shareReplay(1)
        
        request
            .asRequestStatusObservable()
            .bind(to: self.status)
            .addDisposableTo(bag)
        
        /// Refresh
        let refreshRequest = self
            .refreshAction
            .map({ getRequest()
                .asItemObservable()
                .retry()
                .shareReplay(1)
            })
            .startWith(request.asModelObservable())
            .switchLatest()
            .shareReplay(1)
        
        let list = refreshRequest
            .map({ $0.list })
            .shareReplay(1)
        
        list
            .bind { (productList) in
                listCallBack(productList)
            }
            .addDisposableTo(bag)
        
        list
            .map({ $0.count <= 0 })
            .bind(to: self.isEmpty)
            .addDisposableTo(bag)
        
        bag.insert(self)
        
        return bag
    }
    
    func refrence<C:DataSetPageCollection>(getRequestWithPage:@escaping (Int) -> Observable<APIItem<C>>,
                  otherRequest:Observable<Void> = Observable.empty(),
                  listCallBack:@escaping ([C.DataSetPageItem]) -> Void,
                  otherInfoCallBack: ((Any?) -> Void)? = nil) -> DisposeBag {
        
        self.refreshEnable = true
        
        let bag = DisposeBag()
        
        /// FistRequest
        let request = getRequestWithPage(1).shareReplay(1)
        
        /// Other Request
        let other = otherRequest
            .map({ getRequestWithPage(1).shareReplay(1) })
            .shareReplay(1)
        
        Observable
            .of(request, other.switchLatest())
            .merge()
            .asRequestStatusObservable()
            .bind(to: self.status)
            .addDisposableTo(bag)
        
        /// Refresh
        let refreshRequest = Observable.of(self
            .refreshAction
            .map({ getRequestWithPage(1)
                .asItemObservable()
                .retry()
                .shareReplay(1)
            }),
                                           other.map({ $0.asModelObservable() }))
            .merge()
            .startWith(request.asModelObservable())
            .switchLatest()
            .shareReplay(1)
        
        let allCollection = refreshRequest
            .flatMapLatest {
                (collection) -> Observable<(list:[C.DataSetPageItem], page:Int, canLoadMore:Bool, otherInfo:Any?)> in
                
                let model = self
                    .loadMoreAction
                    .map({ (page) -> Observable<C> in
                        return getRequestWithPage(page)
                            .asItemObservable()
                            .retry()
                            .shareReplay(1)
                    })
                    .concat()
                    .startWith(collection)
                    .shareReplay(1)
                
                return model.map({ (list:$0.list,
                                    page:1,
                                    canLoadMore:$0.hasMore,
                                    otherInfo:$0.otherInfo) })
                    .scan((list:[], page:0, canLoadMore:false, otherInfo:nil),
                          accumulator: { (ori, new)  in
                            return (list:ori.list + new.list,
                                    page:ori.page + new.page,
                                    canLoadMore:new.canLoadMore,
                                    otherInfo:new.otherInfo)
                            })
                    .shareReplay(1)
            }
            .shareReplay(1)
        
        
        let list = allCollection
            .map({ $0.list })
            .shareReplay(1)
        
        list
            .bind { (productList) in
                listCallBack(productList)
            }
            .addDisposableTo(bag)
        
        allCollection
            .map({ $0.otherInfo })
            .bind { (otherInfo) in
                otherInfoCallBack?(otherInfo)
            }
            .addDisposableTo(bag)
        
        list
            .map({ $0.count <= 0 })
            .bind(to: self.isEmpty)
            .addDisposableTo(bag)
        
        allCollection
            .map({ $0.canLoadMore })
            .bind(to: self.canLoadMore)
            .addDisposableTo(bag)
        
        allCollection
            .map({ $0.page })
            .bind(to: self.pageIndex)
            .addDisposableTo(bag)
        
        bag.insert(self)
        
        return bag
    }
    
    init(_ collectionView:UICollectionView,
         delegate:UICollectionViewDelegate,
         dataSource:UICollectionViewDataSource,
         refreshEnable:Bool = true) {
        self.collectionView = collectionView
        
        self.status = Variable(.loading)
        
        self.isEmpty = Variable(true)
        
        self.refreshAction = PublishSubject()
        
        self.loadMoreAction = PublishSubject()
        
        self.canLoadMore = Variable(false)
        
        self.pageIndex = Variable(1)
        
        self.disposeBag = DisposeBag()
        
        super.init()
        
        unowned let _delegate = delegate
        
        unowned let _dataSource = dataSource
        
        let layout = collectionView.collectionViewLayout
        
        let currentLayout = CollectionLayout()
        
        let storeBackgroudView = collectionView.backgroundView
        
        self.collectionView.registerView(viewClass: LoadingHeaderView.self, kind: "Loading")
        self.collectionView.registerView(viewClass: PicHeaderView.self, kind: "PicAndLabel")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
        self.collectionView.collectionViewLayout = currentLayout
        
        Observable
            .combineLatest(self.status.asObservable().distinctUntilChanged(),
                           self.isEmpty.asObservable()) { status, isEmpty in
                            return (status:status, isEmpty:isEmpty)
            }
            .debounce(0.1,
                      scheduler: MainScheduler.instance)
            .bind {
                [weak self]
                info in
                
                guard let strongSelf = self else {
                    return
                }
                
                if !info.isEmpty && info.status.rawValue == 1 {
                    strongSelf.refreshEnable = refreshEnable
                    strongSelf.collectionView.collectionViewLayout = layout
                    strongSelf.collectionView.delegate = _delegate
                    strongSelf.collectionView.dataSource = _dataSource
                    strongSelf.collectionView.backgroundView = storeBackgroudView
                } else {
                    
                    switch info.status {
                    case .error(_):
                        currentLayout.currentKind = "PicAndLabel"
                    case .loading:
                        currentLayout.currentKind = "Loading"
                    case .normal:
                        if info.isEmpty {
                            currentLayout.currentKind = "PicAndLabel"
                        } else {
                            currentLayout.currentKind = "Loading"
                        }
                    }
                    
                    strongSelf.refreshEnable = false
                    strongSelf.collectionView.collectionViewLayout = currentLayout
                    strongSelf.collectionView.delegate = self
                    strongSelf.collectionView.dataSource = self
                    strongSelf.collectionView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
                }
                
                strongSelf.collectionView.reloadData()
                
        }
            .addDisposableTo(self.disposeBag)
        
        Observable
            .combineLatest(self.status.asObservable().distinctUntilChanged(),
                           self.isEmpty.asObservable(),
                           self.canLoadMore.asObservable()) { (status, isEmpty, canLoadMore) -> Bool in
                            return status.rawValue == 1 && !isEmpty && canLoadMore
        }
            .bind { [weak self] (canLoadMore) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if canLoadMore {
                    guard let footer = collectionView.mj_footer else {
                        collectionView.mj_footer = TableViewDataSet.LoadmoreFooter(refreshingTarget: strongSelf,
                                                                                   refreshingAction: #selector(CollectionDataSet.loadingMoreAction))
                        
                        return
                    }
                    
                    footer.endRefreshing()
                } else {
                    if let footer = collectionView.mj_footer {
                        footer.endRefreshing()
                        
                        collectionView.mj_footer = nil
                    }
                }
                
        }
            .addDisposableTo(self.disposeBag)
    }
    
    
    var loadingViewHandler:((Void) -> (UICollectionReusableView))?
    var emptyImageHandler:((Void) -> UIImage)?
    var emptyDescriptionHandler:((Void) -> String)?
    var emptyViewHandler:((Void) -> (UICollectionReusableView))?
}

extension CollectionDataSet {
    class LoadingHeaderView: UICollectionReusableView {
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            let loadingView = ActivityIndicatorView()
            
            self.addSubview(loadingView)
            
            loadingView.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(self)
                maker.top.equalTo(self).offset(20)
//                maker.bottom.equalTo(self).offset(-20)
            }
            
            loadingView.play()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension CollectionDataSet {
    class PicHeaderView: UICollectionReusableView {
        
        let imageView:UIImageView
        
        let label:UILabel
        
        override init(frame: CGRect){
            
            imageView = UIImageView()
            
            label = ViewFactory.label(font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.whiteGray)
            label.numberOfLines = 0
            
            super.init(frame: frame)
            
            self.addSubviews(imageView, label)
            
            imageView.snp.makeConstraints { (maker) in
                maker.top.equalTo(self).offset(100)
//                maker.bottom.equalTo(self.snp.centerY)
                maker.centerX.equalTo(self)
                maker.left.greaterThanOrEqualTo(self).offset(15)
                maker.right.lessThanOrEqualTo(self).offset(-15)
            }
            
            label.snp.makeConstraints { (maker) in
                maker.top.equalTo(imageView.snp.bottom).offset(30)
                maker.centerX.equalTo(self)
                maker.left.greaterThanOrEqualTo(self).offset(15)
                maker.right.lessThanOrEqualTo(self).offset(-15)
//                maker.bottom.bottom.equalTo(self).offset(0)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension CollectionDataSet {
    class CollectionLayout: UICollectionViewLayout {
        
        var currentKind:String = "Loading"
        
        override var collectionViewContentSize: CGSize {
            
            guard let collectionView = self.collectionView else {
                return .zero
            }
            
            var size = collectionView.bounds.size
            
            switch currentKind {
            case "PicAndLabel":
                size.height = 351
            case "Loading":
                size.height = 50
            default:break
            }
            
            return size
        }
        
        override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
            return [layoutAttributesForSupplementaryView(ofKind: currentKind, at: IndexPath(row: 0, section: 0))!]
        }
        
        override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
            return nil
        }
        
        override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
            var bounds = collectionView?.bounds ?? .zero
            
            bounds.origin = .zero
            
            var size = bounds.size
            
            switch currentKind {
            case "PicAndLabel":
                size.height = 351
            case "Loading":
                size.height = 50
            default:break
            }
            
            bounds.size = size
            
            let layoutAttribute = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
            
            layoutAttribute.frame = bounds
            
            return layoutAttribute
        }
    }
}

extension CollectionDataSet: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case "Loading":
            return loadingViewHandler?() ?? collectionView.dequeueReusableView(kind: kind, indexPath: indexPath) as LoadingHeaderView
        case "PicAndLabel":
            
            switch status.value {
            case .error(let errorString):
                let picHeaderView:PicHeaderView = collectionView.dequeueReusableView(kind: kind, indexPath: indexPath)
                
                picHeaderView.imageView.image = UIImage(named: "icon_interface_error")
                picHeaderView.label.text = errorString
                
                return picHeaderView
            default:
                if isEmpty.value {
                    let picHeaderView:PicHeaderView = collectionView.dequeueReusableView(kind: kind, indexPath: indexPath)
                    
                    picHeaderView.imageView.image = emptyImageHandler?() ?? UIImage(named: "icon_no_data")
                    picHeaderView.label.text = emptyDescriptionHandler?() ?? "数据还在生产中..."
                    
                    return picHeaderView
                }
            }
            
            return loadingViewHandler?() ?? collectionView.dequeueReusableView(kind: kind, indexPath: indexPath) as PicHeaderView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    
    
//    func collectionView(_ collectionView: UIcollectionView, numberOfRowsInSection section: Int) -> Int {
//        switch status.value {
//        case .normal:
//            return 0
//        default:
//            return 0
//        }
//    }
//    
//    func collectionView(_ collectionView: UIcollectionView, cellForRowAt indexPath: IndexPath) -> UIcollectionViewCell {
//        return UIcollectionViewCell()
//    }
//    
//    func numberOfSections(in collectionView: UIcollectionView) -> Int {
//        return 1
//    }
}

extension CollectionDataSet: UICollectionViewDelegate {
//    func collectionView(_ collectionView: UIcollectionView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        if let headerView = view as? UICollectionReusableView {
//            headerView.backgroundView = ViewFactory.view(color: UIConfig.generalColor.white)
//        }
//    }
//    
//    
//    func collectionView(_ collectionView: UIcollectionView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 0.0
//    }
//    
//    func collectionView(_ collectionView: UIcollectionView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UIcollectionViewAutomaticDimension
//    }
//    
//    func collectionView(_ collectionView: UIcollectionView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//        switch status.value {
//        case .normal:
//            if isEmpty.value {
//                return 350
//            }
//            return 50
//        case .loading:
//            return 50
//        case .error(_):
//            return 350
//        }
//    }
//    
//    func collectionView(_ collectionView: UIcollectionView, viewForHeaderInSection section: Int) -> UIView? {
//        switch status.value {
//        case .normal:
//            if isEmpty.value {
//                if let emptyView = emptyViewHandler?() {
//                    return emptyView
//                }
//                
//                let picHeaderView:PicHeaderView = collectionView.dequeueReusableView()
//
//
//                return picHeaderView
//            }
//            
//            return loadingViewHandler?() ?? collectionView.dequeueReusableView() as LoadingHeaderView
//        case .loading:
//            return loadingViewHandler?() ?? collectionView.dequeueReusableView() as LoadingHeaderView
//        case .error(let errorString):
//            let picHeaderView:PicHeaderView = collectionView.dequeueReusableView()
//            
    
//
//            return picHeaderView
//        }
//    }
}

