//
//  ToastViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class ToastViewController: UIViewController {
    
    fileprivate enum State {
        case loading
        case label(String)
        case loadingAndLabel(String)
    }
    
    fileprivate func changeState (state:State) {
        
        timer?.invalidate()
        timer = nil
        
        switch state {
        case .loadingAndLabel(let text):
            tapGesture.isEnabled = false
            
            if loading.superview != self.contentView {
                self.contentView.addSubview(loading)
            }
            
            if label.superview != self.contentView {
                self.contentView.addSubview(label)
            }
            
            self.contentView.addSubviews(loading, label)
            
            loading.snp.remakeConstraints({ (maker) in
                maker.top.equalTo(self.contentView).offset(25)
                maker.centerX.equalTo(self.contentView)
                maker.left.greaterThanOrEqualTo(self.contentView).offset(25)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-25)
            })
            
            label.text = text
            label.snp.remakeConstraints({ (maker) in
                maker.centerX.equalTo(self.contentView)
                maker.left.greaterThanOrEqualTo(self.contentView).offset(25)
                maker.right.lessThanOrEqualTo(self.contentView).offset(-25)
                maker.top.equalTo(loading.snp.bottom).offset(15)
                maker.bottom.equalTo(self.contentView).offset(-25)
            })
            
            contentView.snp.remakeConstraints { (maker) in
                maker.center.equalTo(self.view)
                maker.height.greaterThanOrEqualTo(52)
            }
            
        case .loading:
            tapGesture.isEnabled = false
            
            label.removeFromSuperview()
            
            if loading.superview != self.contentView {
                self.contentView.addSubview(loading)
            }
            
            loading.snp.remakeConstraints({ (maker) in
                maker.center.equalTo(self.contentView)
                maker.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(25, 25, 25, 25))
            })
            
            contentView.snp.remakeConstraints { (maker) in
                maker.center.equalTo(self.view)
            }
        case .label(let text):
            tapGesture.isEnabled = true
            
            loading.removeFromSuperview()
            
            if label.superview != self.contentView {
                self.contentView.addSubview(label)
            }
            
            label.text = text
            label.snp.remakeConstraints({ (maker) in
                maker.center.equalTo(self.contentView)
                maker.edges.greaterThanOrEqualTo(self.contentView).inset(UIEdgeInsetsMake(25, 25, 25, 25))
            })
            
            contentView.snp.remakeConstraints { (maker) in
                maker.center.equalTo(self.view)
                maker.width.equalTo(230)
                maker.height.greaterThanOrEqualTo(52)
            }
            
            timer = Timer.scheduledTimer(timeInterval: 0.333 * TimeInterval(text.length),
                                         target: self,
                                         selector: #selector(ToastViewController.autoDimiss(sender:)),
                                         userInfo: nil,
                                         repeats: false)
        }
    }
    
    let loading:LoadingView = LoadingView(type: .white)
    
    var timer:Timer? = nil
    
    let contentView:UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.isUserInteractionEnabled = false
        
        return view
    }()
    
    let tapGesture = UITapGestureRecognizer()
    
    let label:UILabel = {
        let label = ViewFactory.label(font: UIConfig.generalFont(13),
                                      textColor: UIConfig.generalColor.white,
                                      backgroudColor: UIColor.clear)
        
        label.textAlignment = .center
        label.numberOfLines = 0
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapGesture.addTarget(self,
                             action: #selector(ToastViewController.contentTap(sender:)))
        
        self.view.addGestureRecognizer(tapGesture)
        self.view.backgroundColor = UIColor.clear
        self.view.addSubview(contentView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func contentTap(sender:UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func autoDimiss(sender:Timer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayLoading() {
        UIView.animate(withDuration: 0.125) { 
            self.loading.alpha = 1.0
            self.label.alpha = 0.0
            self.changeState(state: .loading)
            self.contentView.layoutIfNeeded()
        }
    }
    
    func displayLabel(text:String) {
        UIView.animate(withDuration: 0.125) {
            self.label.alpha = 1.0
            self.loading.alpha = 0.0
            self.changeState(state: .label(text))
            self.contentView.layoutIfNeeded()
        }
    }
    
    func displayLoadingAndLabel(text:String) {
        
        UIView.animate(withDuration: 0.125) {
            self.label.alpha = 1.0
            self.loading.alpha = 1.0
            self.changeState(state: .loadingAndLabel(text))
            self.contentView.layoutIfNeeded()
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        changeState(state: .loading)
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    init(text:String) {
        super.init(nibName: nil, bundle: nil)
        
        changeState(state: .label(text))
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    init(loadingAndText:String) {
        super.init(nibName: nil, bundle: nil)
        
        changeState(state: .loadingAndLabel(loadingAndText))
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var isPresenting:Bool = false
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer?.invalidate()
        timer = nil
    }
}


extension ToastViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        
        return PresentAnimationItem()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        
        return DismissAnimationItem()
    }
}

extension ToastViewController {
    class DismissAnimationItem: NSObject, UIViewControllerAnimatedTransitioning {
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return 0.0625
        }
        
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            
            let fromVC = transitionContext.viewController(forKey: .from)!
            
            let fromView = fromVC.view!
            
            UIView.animate(withDuration: transitionDuration(using: transitionContext),
                           delay: 0,
                           options: UIViewAnimationOptions.curveLinear,
                           animations: {
                            fromView.alpha = 0.0
                            fromView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            }) { (complete) in
                transitionContext.completeTransition(complete)
            }
        }
    }
}

extension ToastViewController {
    class PresentAnimationItem: NSObject, UIViewControllerAnimatedTransitioning {
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return 0.125
        }
        
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
            let container = transitionContext.containerView
            
            let fromVC = transitionContext.viewController(forKey: .from)!
            
            let toVC = transitionContext.viewController(forKey: .to)!
            
            let toView = toVC.view!
            
            container.insertSubview(toView, belowSubview: fromVC.view)
            
            toView.transform = CGAffineTransform(scaleX: 0.8,
                                                 y: 0.8)
            toView.alpha = 0.0
            
            UIView.animate(withDuration: transitionDuration(using: transitionContext),
                           delay: 0,
                           options: UIViewAnimationOptions.curveLinear,
                           animations: {
                            toView.alpha = 1.0
                            toView.transform = CGAffineTransform.identity
            }) { (complete) in
                transitionContext.completeTransition(complete)
            }
        }
    }
}
