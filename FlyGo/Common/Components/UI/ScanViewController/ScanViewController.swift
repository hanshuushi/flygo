//
//  ScanViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/21.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class ScanViewController: ZBarReaderViewController, UINavigationControllerDelegate {
    
    var resultCallBack:((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.readerDelegate = self
        self.showsZBarControls = false
        self.tracksSymbols = false
        self.cameraFlashMode = .off
        
        /// mask
        let maskView = MaskView(frame: UIScreen.main.bounds)
        
        self.cameraOverlayView = maskView
        
        self.readerView.frame = UIScreen.main.bounds
        /// add tip
        let tipView = ViewFactory.view(color: .clear)
        
        self.view.addSubview(tipView)
        
        let tipImageView = UIImageView(named: "icon_bar_code_paradigm")
        
        let tipLabel = ViewFactory.generalLabel(generalSize: 13,
                                                textColor: UIConfig.generalColor.white,
                                                backgroudColor: .clear)
        tipLabel.text = "请将条形码放入下方扫描区域内"
        
        tipView.addSubviews(tipImageView, tipLabel)
        
        tipImageView.snp.makeConstraints { (maker) in
            maker.left.equalTo(tipView)
            maker.top.greaterThanOrEqualTo(tipView)
            maker.bottom.lessThanOrEqualTo(tipView)
            maker.centerY.equalTo(tipView)
        }
        
        tipLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(tipImageView.snp.right).offset(10)
            maker.right.equalTo(tipView)
            maker.top.greaterThanOrEqualTo(tipView)
            maker.bottom.lessThanOrEqualTo(tipView)
            maker.centerY.equalTo(tipView)
        }
        
        tipView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.bottom.equalTo(self.view.snp.centerY).offset(-145)
        }
        
        //// buttons
        let flashButton = UIButton(type: .custom)
        
        self.view.addSubview(flashButton)
        
        flashButton.setImage(UIImage(named: "icon_flash_open"),
                             for: .normal)
        flashButton.setImage(UIImage(named: "icon_flash_close"),
                             for: .selected)
        flashButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        flashButton.layer.cornerRadius = 25
        flashButton.layer.masksToBounds = true
        flashButton.addTarget(self,
                              action: #selector(ScanViewController.flashButtonPressed(sender:)),
                              for: UIControlEvents.touchUpInside)
        flashButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(self.view.snp.centerX).offset(-37.5)
            maker.width.height.equalTo(50)
            maker.top.equalTo(self.view.snp.centerY).offset(175)
        }
        
        let flashLabel = ViewFactory.generalLabel(generalSize: 13,
                                                  textColor: UIConfig.generalColor.white,
                                                  backgroudColor: .clear)
        
        flashLabel.text = "闪光灯"
        
        self.view.addSubview(flashLabel)
        
        flashLabel.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(flashButton)
            maker.top.equalTo(flashButton.snp.bottom).offset(8)
        }
        
        let albumButton = UIButton(type: .custom)
        
        self.view.addSubview(albumButton)
        
        albumButton.setImage(UIImage(named: "icon_photo_album"),
                             for: .normal)
        albumButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        albumButton.layer.cornerRadius = 25
        albumButton.layer.masksToBounds = true
        albumButton.addTarget(self,
                              action: #selector(ScanViewController.showAlbumPressed(sender:)),
                              for: UIControlEvents.touchUpInside)
        albumButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.view.snp.centerX).offset(37.5)
            maker.width.height.equalTo(50)
            maker.top.equalTo(self.view.snp.centerY).offset(175)
        }
        
        let albumLabel = ViewFactory.generalLabel(generalSize: 13,
                                                  textColor: UIConfig.generalColor.white,
                                                  backgroudColor: .clear)
        
        albumLabel.text = "相册"
        
        self.view.addSubview(albumLabel)
        
        albumLabel.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(albumButton)
            maker.top.equalTo(albumButton.snp.bottom).offset(8)
        }
        
        //// add scan board
        self.scanner.setSymbology(ZBAR_I25, config: ZBAR_CFG_ENABLE, to: 0)
        
        
        /// add back item
        let backBarButton = ViewFactory.button(imageNamed: "icon_return_white")
        
        self.view.addSubview(backBarButton)
        
        backBarButton.addTarget(self,
                                action: #selector(UIViewController.popVC),
                                for: UIControlEvents.touchUpInside)
        backBarButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.view).offset(26.5)
            maker.left.equalTo(self.view).offset(4)
            maker.size.equalTo(CGSize(width:30, height:30))
        }
    }
    
    func flashButtonPressed(sender:UIButton) {
        sender.isSelected = !sender.isSelected
        
        self.cameraFlashMode = sender.isSelected ? .on : .off
    }
    
    override var showNavigationBar: Bool {
        return false
    }
    
    func showAlbumPressed(sender:UIButton) {
        let imagePicker = ImagePickerViewController()
        
        imagePicker.parentController = self
        
        self.present(imagePicker,
                     animated: true,
                     completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.readerView.start()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.readerView.stop()
    }
}

extension ScanViewController {
    class MaskView: UIView {
        
        let cropRect:CGRect
        
        override init(frame: CGRect) {
            cropRect = CGRect(x: (frame.width - 250) / 2.0,
                              y: (frame.height - 250) / 2.0,
                              width: 250,
                              height: 250)
            
            super.init(frame: frame)
            
            /// ad bg
            let path = UIBezierPath(rect: self.bounds)
            
            let shapeLayer = CAShapeLayer()
            
            path.append(UIBezierPath(roundedRect: cropRect, cornerRadius: 5))
            
            shapeLayer.path = path.cgPath
            shapeLayer.fillRule = kCAFillRuleEvenOdd;
            shapeLayer.fillColor = UIColor.black.cgColor //[[UIColor blackColor] CGColor];
            shapeLayer.opacity = 0.5;
            
            self.layer.addSublayer(shapeLayer)
            
            /// add board
            let left = cropRect.minX
            let top = cropRect.minY
            let right = cropRect.maxX
            let bottom = cropRect.maxY
            
            //// left top
            let leftTopLayer = CAShapeLayer()
            
            let leftTopPath = CGMutablePath()
            
            leftTopPath.move(to: CGPoint(x: left, y: top + 20))
            leftTopPath.addArc(tangent1End: CGPoint(x: left, y:top), tangent2End: CGPoint(x: left + 20, y: top), radius: 5)
            leftTopPath.addLine(to: CGPoint(x: left + 20, y: top))
            leftTopPath.addLine(to: CGPoint(x: left + 20, y: top + 4))
            leftTopPath.addLine(to: CGPoint(x: left + 4, y: top + 4))
            leftTopPath.addLine(to: CGPoint(x: left + 4, y: top + 20))
            leftTopPath.addLine(to: CGPoint(x: left, y: top + 20))
            
            leftTopLayer.fillColor = UIConfig.generalColor.red.cgColor
            leftTopLayer.path = leftTopPath
            
            self.layer.addSublayer(leftTopLayer)
            
            //// right top
            let rightopLayer = CAShapeLayer()
            
            let rightopPath = CGMutablePath()
            
            rightopPath.move(to: CGPoint(x: right, y: top + 20))
            rightopPath.addArc(tangent1End: CGPoint(x: right, y:top), tangent2End: CGPoint(x: right - 20, y: top), radius: 5)
            rightopPath.addLine(to: CGPoint(x: right - 20, y: top))
            rightopPath.addLine(to: CGPoint(x: right - 20, y: top + 4))
            rightopPath.addLine(to: CGPoint(x: right - 4, y: top + 4))
            rightopPath.addLine(to: CGPoint(x: right - 4, y: top + 20))
            rightopPath.addLine(to: CGPoint(x: right, y: top + 20))
            
            rightopLayer.fillColor = UIConfig.generalColor.red.cgColor
            rightopLayer.path = rightopPath
            
            self.layer.addSublayer(rightopLayer)
            
            //// left bottom
            let leftbottomLayer = CAShapeLayer()
            
            let leftbottompPath = CGMutablePath()
            
            leftbottompPath.move(to: CGPoint(x: left, y: bottom - 20))
            leftbottompPath.addArc(tangent1End: CGPoint(x: left, y:bottom), tangent2End: CGPoint(x: left + 20, y: bottom), radius: 5)
            leftbottompPath.addLine(to: CGPoint(x: left + 20, y: bottom))
            leftbottompPath.addLine(to: CGPoint(x: left + 20, y: bottom - 4))
            leftbottompPath.addLine(to: CGPoint(x: left + 4, y: bottom - 4))
            leftbottompPath.addLine(to: CGPoint(x: left + 4, y: bottom - 20))
            leftbottompPath.addLine(to: CGPoint(x: left, y: bottom - 20))
            
            leftbottomLayer.fillColor = UIConfig.generalColor.red.cgColor
            leftbottomLayer.path = leftbottompPath
            
            self.layer.addSublayer(leftbottomLayer)
            
            //// right bottom
            let rightbottomLayer = CAShapeLayer()
            
            let righbottompPath = CGMutablePath()
            
            righbottompPath.move(to: CGPoint(x: right, y: bottom - 20))
            righbottompPath.addArc(tangent1End: CGPoint(x: right, y:bottom), tangent2End: CGPoint(x: right - 20, y: bottom), radius: 5)
            righbottompPath.addLine(to: CGPoint(x: right - 20, y: bottom))
            righbottompPath.addLine(to: CGPoint(x: right - 20, y: bottom - 4))
            righbottompPath.addLine(to: CGPoint(x: right - 4, y: bottom - 4))
            righbottompPath.addLine(to: CGPoint(x: right - 4, y: bottom - 20))
            righbottompPath.addLine(to: CGPoint(x: right, y: bottom - 20))
            
            rightbottomLayer.fillColor = UIConfig.generalColor.red.cgColor
            rightbottomLayer.path = righbottompPath
            
            self.layer.addSublayer(rightbottomLayer)
            
            //// add line
            lineImageView.frame = CGRect(x: (frame.width - lineImageView.size.width) / 2.0,
                                     y: top,
                                     width: lineImageView.size.width,
                                     height: lineImageView.size.height)
            
            currentDistance = 0
            
            self.addSubview(lineImageView)
            
            displayLink = CADisplayLink(target: self,
                                        selector: #selector(MaskView.moveLine(displayLink:)))
            displayLink.isPaused = false
            displayLink.add(to: .main,
                            forMode: .commonModes)
        }
        
        deinit {
            displayLink?.invalidate()
        }
        
        var displayLink:CADisplayLink!
        
        let lineImageView = UIImageView(named: "icon_scan_line")
        
        var currentDistance:CGFloat = 0
        
        func moveLine(displayLink:CADisplayLink) {
            let timeRate = displayLink.duration / 3.0
            
            let moveRate:CGFloat = CGFloat(238.0 * timeRate)
            
            currentDistance += moveRate
            
            if currentDistance > 238 {
                currentDistance = currentDistance - 238
            }
            
            lineImageView.transform = CGAffineTransform(translationX: 0, y: currentDistance)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension ScanViewController {
    class ImagePickerViewController: ZBarReaderController, ZBarReaderDelegate {
        var callBack:((UIImage) -> Void)!
        
        weak var parentController:ScanViewController?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.allowsEditing = false
            self.sourceType = .photoLibrary
            self.delegate = self
            self.readerDelegate = self
            self.allowsEditing = false
            
            self.scanner.setSymbology(ZBAR_I25, config: ZBAR_CFG_ENABLE, to: 0)
        }
        
        override func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker.dismiss(animated: true,
                           completion: nil)
        }
        
        override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
                return
            }
            
            picker.dismiss(animated: true) {
                let result = self.scanUIImage(image)
                
                self.parentController?.scanResult(result)
            }
        }
//
//        func readerControllerDidFail(toRead reader: ZBarReaderController!, withRetry retry: Bool) {
//            print("retry is \(retry)")
//        }
        
//        init(callBack:@escaping (UIImage) -> Void) {
//            self.callBack = callBack
//            
//            super.init(navigationBarClass: PresentNavigationVC.self,
//                       toolbarClass: UIToolbar.self)
//            
//            self.allowsEditing = false
//            self.sourceType = .photoLibrary
//            self.delegate = self
//        }
        
//        override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
//            super.init(nibName: nil, bundle: nil)
//            
        
//        }
        
//        required init?(coder aDecoder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
        
        
    }
}

extension ZBarSymbolSet: Sequence {
    public func makeIterator() -> NSFastEnumerationIterator {
        return NSFastEnumerationIterator(self)
    }
}

extension ZBarSymbol {
    var isValid:Bool {
        let format = "^[0-9]{12}$"
        
        return NSPredicate(format: "SELF MATCHES %@", format).evaluate(with: (self.data ?? ""))
    }
}

extension ScanViewController: ZBarReaderDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func scanResult(_ result:Any?) {
        
        if let _result = result, let symbol = _result as? ZBarSymbol {
            if symbol.isValid {
                let content = symbol.data ?? ""
                
                resultCallBack?(content)
                
                return
            }
        } else {
            guard let results = result as? ZBarSymbolSet,
                results.count > 0 else {
                    self.showToast(text: "该图片不包含一个条形码")
                    return
            }
            
            for one in results {
                if let result = one as? ZBarSymbol, result.isValid {
                    let content = result.data ?? ""
                    
                    self.readerView.stop()
                    
                    resultCallBack?(content)
                    
                    return
                }
            }
        }
        
        self.showToast(text: "请扫描顺丰的快递单条形码")
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        scanResult(info[ZBarReaderControllerResults])
    }
}
