//
//  CommonViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/6/15.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class CommonViewController: WKWebViewController {
    
    override var showNavigationBar: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIButton(type: .custom)
        
        backButton.setImage(UIImage(named:"icon_return_white"),
                            for: .normal)
        
        backButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2)
        backButton.layer.cornerRadius = 15
        backButton.layer.masksToBounds = true
        backButton.addTarget(target,
                             action: #selector(UIViewController.popVC),
                             for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(backButton)
        
        backButton.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(self.view).offset(30)
            maker.left.equalTo(self.view).offset(14)
            maker.size.equalTo(CGSize(width:30, height:30))
        }
    }
}
