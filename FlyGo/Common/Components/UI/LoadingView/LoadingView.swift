//
//  LoadingView.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/11.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

enum LoadingViewType:String {
    case white
    case red
}

class LoadingView: UIView {
    
    private let imageView:UIImageView
    
    init(type:LoadingViewType) {
        
        let imageName = "loading-\(type.rawValue)"
        
        imageView = UIImageView(named:imageName)
        
        super.init(frame:imageView.bounds)
        
        self.backgroundColor = UIColor.clear
        self.view.addSubview(imageView)
        
        imageView.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(LoadingView.applicationWillResignActive),
                                               name: NSNotification.Name.UIApplicationWillResignActive,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(LoadingView.applicationDidBecomeActive),
                                               name: NSNotification.Name.UIApplicationDidBecomeActive,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func applicationWillResignActive() {
        displayLinker?.isPaused = true
    }
    
    @objc
    func applicationDidBecomeActive() {
        displayLinker?.isPaused = false
    }
    
    var displayLinker:CADisplayLink?
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if self.superview == nil {
            if let _displayLinker = displayLinker {
                _displayLinker.invalidate()
                
                displayLinker = nil
            }
        } else {
            ratation()
        }
    }
    
    func ratation() {
        
        if let _displayLinker = displayLinker {
            
            _displayLinker.isPaused = false
            
            return
        }
        
        displayLinker = CADisplayLink(target: self,
                                          selector: #selector(LoadingView.displayLink))
        
        displayLinker!.isPaused = false
        
        displayLinker?.add(to: RunLoop.current,
                           forMode: .commonModes)
    }
    
    static let offset:CGFloat = CGFloat(-CGFloat.pi * 2 / 60.0)
    
    func displayLink() {
        imageView.layer.transform = CATransform3DRotate(imageView.layer.transform,
                                                        LoadingView.offset,
                                                        0,
                                                        0,
                                                        1)
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return imageView.size
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
