//
//  SociaShareViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/27.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
/*
class SocialShareViewController: UIViewController {
    
    typealias SocialItemClickHandler = (SocialPlatform) -> Bool
    
    var handler:SocialItemClickHandler?
    
    static var height:CGFloat = 206
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.barColor
        
        /// add title
        let titleLabel = ViewFactory.label(font: UIConfig.generalFont(15),
                                           textColor: UIConfig.generalColor.selected,
                                           backgroudColor: .clear)
        
        titleLabel.text = "分享"
        
        let leftTag = UIImageView(named: "icon_orn_left")
        
        let rightTag = UIImageView(named: "icon_orn_right")
        
        let buttonWithImage = {
            (named:String, title:String, target:Any, tag:Int, imageInsets:CGFloat, titleInsets:CGFloat) -> UIButton in
            
            let button = UIButton(type: .custom)
            
            button.tag = tag
            button.setImage(UIImage(named:named),
                            for: .normal)
            button.setAttributedTitle(NSAttributedString(string: title,
                                                         font: UIConfig.generalFont(13),
                                                         textColor: UIConfig.generalColor.selected),
                                      for: .normal)
            button.addTarget(self,
                             action: #selector(SocialShareViewController.shareItemClick(sender:)),
                             for: .touchUpInside)
            button.size = CGSize(width: 80, height: 120)
            button.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, -20)
            button.imageEdgeInsets = UIEdgeInsetsMake(-10, imageInsets, 10, -imageInsets)
            button.titleEdgeInsets = UIEdgeInsetsMake(44, -titleInsets, -44, titleInsets)
            
            return button
        }
        
        let wcButton = buttonWithImage("icon_wechat",
                                       "微信好友",
                                       self,
                                       22, 26.5, 25.25)
        
        let tlButton = buttonWithImage("icon_circle_of_friends",
                                       "朋友圈",
                                       self,
                                       23, 20, 25.25)
        
        let wbButton = buttonWithImage("icon_weibo",
                                       "微博",
                                       self,
                                       1, 13, 25.25)
        
        let line = ViewFactory.line()
        
        let cancelButton = UIButton(type: .custom)
        
        
        cancelButton.setAttributedTitle(NSAttributedString(string: "取消",
                                                           font: UIConfig.generalFont(15),
                                                           textColor: UIConfig.generalColor.selected),
                                        for: .normal)
        cancelButton.addTarget(self,
                               action: #selector(SocialShareViewController.dismissClick(sender:)),
                               for: .touchUpInside)
        cancelButton.backgroundColor = .clear
        
        self.view.addSubviews(titleLabel, leftTag, rightTag, wcButton, tlButton, wbButton, cancelButton, line)
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.top.equalTo(self.view).offset(20)
        }
        
        leftTag.snp.makeConstraints { (maker) in
            maker.right.equalTo(titleLabel.snp.left).offset(-15)
            maker.centerY.equalTo(titleLabel)
        }
        
        rightTag.snp.makeConstraints { (maker) in
            maker.left.equalTo(titleLabel.snp.right).offset(15)
            maker.centerY.equalTo(titleLabel)
        }
        
        let margin = (UIScreen.main.bounds.width - 80 * 4) / 5.0
        
        wcButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.left.equalTo(self.view).offset(margin)
            maker.size.equalTo(wcButton.size)
        }
        
        tlButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.left.equalTo(wcButton.snp.right).offset(margin)
            maker.size.equalTo(wcButton.size)
        }
        
        wbButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.left.equalTo(tlButton.snp.right).offset(margin)
            maker.size.equalTo(wcButton.size)
        }
        
        line.snp.makeConstraints { (maker) in
            maker.height.equalTo(0.5)
            maker.left.right.equalTo(self.view)
        }
        
        cancelButton.snp.makeConstraints { (maker) in
            maker.left.right.bottom.equalTo(self.view)
            maker.height.equalTo(49)
            maker.top.equalTo(line)
        }
    }
    
    func shareItemClick(sender:Any?) {
        guard let button = sender as? UIButton,
            let platform = SocialPlatform(rawValue: UInt(button.tag)) else {
            return
        }
        
        if let dismiss = handler?(platform), dismiss {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func dismissClick(sender:Any?) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
*/
