//
//  WKWebViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/21.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import WebKit
import RxSwift
import EZSwiftExtensions

class WKWebViewController: UIViewController {
    
    let webView:WKWebView
    
    let hybridContainer:HybridContainer
    
    init(url:URL) {
        requestEvent = PublishSubject()
        
        asyncDisposeBag = DisposeBag()
        
        hybridContainer = HybridContainer()
        
#if DEBUG
        let urlRequest = URLRequest(url: url,
                                    cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                    timeoutInterval: 100000)
#else
        let urlRequest = URLRequest(url: url)
#endif
        
        let contentController = WKUserContentController()
        
        
        contentController.add(hybridContainer, name: "h5ToNative")
        contentController.add(hybridContainer, name: "openLocationUrl")
        contentController.add(hybridContainer, name: "hrefHtml")
        contentController.add(hybridContainer, name: "contact")
        contentController.add(hybridContainer, name: "share")
        contentController.add(hybridContainer, name: "login")
        
        var userObjectJS = "{"
        
        if UserManager.shareInstance.isLogined() {
            /// add custom id
            userObjectJS += "\"customerId\":\"\(UserManager.shareInstance.currentId)\","
            
            /// nickname
            userObjectJS += "\"nickname\":\"\(UserManager.shareInstance.currentNickName)\","
            
            /// avatar
            userObjectJS += "\"avatar\":\"\(UserManager.shareInstance.avatarURL?.absoluteString ?? "")\""
        }
        
        userObjectJS += "}"
        
        let userScript = WKUserScript(source: "var ios_userinfo = \(userObjectJS); function ios_setuserinfo(userinfo) { ios_userinfo = userinfo; }; function ios_getuserinfo() { return ios_userinfo; };",
                                      injectionTime: .atDocumentStart,
                                      forMainFrameOnly: false)
        
        contentController.addUserScript(userScript)
        
        /// notification callback
        let notificationJS = "var getNotificationEnableCallBack = null; var enableRemote = \(RemoteManager.shareInstance.remoteEnable.value ? "true" : "false"); function setNotificationEnable(enable) { enableRemote = enable; if (typeof getNotificationEnableCallBack != 'function') { return; } getNotificationEnableCallBack(enableRemote) }; function getNotificationEnable(callBack) { if (typeof callBack != 'function') { return; } getNotificationEnableCallBack = callBack; callBack(enableRemote); };"
        
        let notificationScript = WKUserScript(source: notificationJS,
            injectionTime: .atDocumentStart,
            forMainFrameOnly: false)
        
        contentController.addUserScript(notificationScript)
        
        let preferences = WKPreferences()
        
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        
        config.preferences = preferences
        config.userContentController = contentController
        
        
        webView = WKWebView(frame: .zero,
                            configuration: config)
        
        webView.load(urlRequest)
        
        progressView = ProgressView(frame: .zero)
        
        super.init(nibName: nil, bundle: nil)
        
        hybridContainer.parentController = self
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.addObserver(self,
                            forKeyPath: "estimatedProgress",
                            options: .new,
                            context: nil)
        
        let _webView = webView
        
        RemoteManager
            .shareInstance
            .remoteEnable
            .asDriver()
            .drive(onNext: { (enable) in
                
                if _webView.isLoading {
                    return
                }
                
                _webView.evaluateJavaScript("setNotificationEnable(\(enable ? "true" : "false"))",
                    completionHandler: {
                        (_, error) in
                        
                        print("error is \(error.debugDescription)")
                })
            },
                   onCompleted: nil,
                   onDisposed: nil)
            .addDisposableTo(asyncDisposeBag)
        
//        let _webView = webView
//        
//        ez.runThisAfterDelay(seconds: 5.0,
//                             after: {
//                                _webView.evaluateJavaScript("window.webkit.messageHandlers.login.postMessage({ \"js\":\"alert(ios_getuserinfo().customerId)\" })",
//                                                            completionHandler: {
//                                                                (_, error) in
//                                                                print("error is \(error)")
//                                })
//        })
    }
    
    deinit {
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let key = keyPath, key == "estimatedProgress" {
            self.progressView.progress = Float(webView.estimatedProgress)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(webView)
        
        webView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view).inset(UIEdgeInsets.zero)
        }
        
        //addFakeShadow()
        
        self.view.addSubview(progressView)
        
        progressView.snp.makeConstraints { (maker) in
            maker.left.right.equalTo(self.view)
            maker.top.equalTo(self.view).offset(64)
            maker.height.equalTo(3)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.webView.endEditing(true) 
    }
    
    var coverView:UIView? {
        willSet {
            if let cv = coverView {
                cv.removeFromSuperview()
            }
        }
        didSet {
            if let cv = coverView {
                self.view.addSubviews(cv)
                
                cv.snp.makeConstraints({ (maker) in
                    maker.centerX.width.equalTo(self.view)
                    maker.height.equalTo(self.view.snp.height).offset(-64)
                    maker.centerY.equalTo(self.view).offset(32)
                })
            }
        }
    }
    
    let progressView:ProgressView
    
    let requestEvent:PublishSubject<Void>
    
    let asyncDisposeBag:DisposeBag
}

extension WKWebViewController {
    
    class ProgressView: UIView {
        
        let bar:UIView
        
        override init(frame: CGRect) {
            bar = UIView(frame: .zero)
            bar.backgroundColor = UIConfig.generalColor.red
            
            
            super.init(frame: frame)
            
            self.backgroundColor = UIConfig.generalColor.backgroudGray
            
            self.addSubview(bar)
            
            self.progress = 0.0
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            let progress = self.progress
            
            self.progress = progress
        }
        
        var progress:Float = 0.0 {
            didSet {
                bar.frame = CGRect(x: 0,
                                   y: 0,
                                   width: self.bounds.width * CGFloat(progress),
                                   height: self.bounds.height)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}

extension WKWebViewController {
    func verticalOffset() -> CGFloat{
        let inset = self.webView.scrollView.contentInset
        
        return inset.top - inset.bottom
    }
    
    func switchLoadingView() {
        self.coverView?.removeFromSuperview()
        
        /// add cover
        let coverView = UIView()
        
        self.coverView = coverView
        
        coverView.backgroundColor = UIConfig.generalColor.white
        
        /// add loading
        let loading = ActivityIndicatorView()
        
        coverView.addSubview(loading)
        
        loading.play()
        
        loading.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(coverView)
            maker.centerY.equalTo(coverView)
            maker.size.equalTo(loading.size)
        }
    }
    
    func switchFailedView(error:String) {
        self.coverView?.removeFromSuperview()
        
        /// add cover
        let coverView = UIView()
        
        self.coverView = coverView
        
        coverView.backgroundColor = UIConfig.generalColor.white
        
        /// add label
        let label = ViewFactory.label(font: UIConfig.generalFont(15),
                                      textColor: UIConfig.generalColor.whiteGray)
        
        label.text = error
        
        coverView.addSubview(label)
        
        let imageView = UIImageView(named: "icon_interface_error")
        
        coverView.addSubview(imageView)
        
        imageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(coverView.snp.centerY)
            maker.centerX.equalTo(coverView)
        }
        
        label.snp.makeConstraints { (maker) in
            maker.top.equalTo(imageView.snp.bottom).offset(30)
            maker.centerX.equalTo(coverView)
        }
        
        let button = UIButton(type: .custom)
        
        button.setTitle("刷新", for: .normal)
        button.setBackgroundColor(UIConfig.generalColor.red,
                                  forState: .normal)
        button.setBackgroundColor(UIConfig.generalColor.highlyRed,
                                  forState: .highlighted)
        button.titleLabel?.font = UIConfig.generalSemiboldFont(15)
        button.setTitleColor(UIConfig.generalColor.white,
                             for: .normal)
        button.layer.cornerRadius = 22
        button.layer.masksToBounds = true
        button.rx
            .tap
            .throttle(1.0, scheduler: MainScheduler.instance)
            .bindNext(requestEvent).addDisposableTo(asyncDisposeBag)
        
        coverView.addSubview(button)
        
        button.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(coverView)
            maker.top.equalTo(label.snp.bottom).offset(80)
            maker.size.equalTo(CGSize(width:200, height:44))
        }
    }
    
    func switchNormal() {
        self.coverView?.removeFromSuperview()
        self.coverView = nil
    }
}

extension WKWebViewController: WKUIDelegate {
    func webView(_ webView: WKWebView,
                 runJavaScriptAlertPanelWithMessage message: String,
                 initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        self.showToast(text: message)
        
        completionHandler()
    }
    
    func webView(_ webView: WKWebView,
                 runJavaScriptConfirmPanelWithMessage message: String,
                 initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: nil,
                                                message: message,
                                                preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "确定",
                               style: .default) { (_) in
                                completionHandler(true)
        }
        
        alertController.addAction(ok)
        
        let no = UIAlertAction(title: "取消",
                               style: .cancel) { (_) in
                                completionHandler(false)
        }
        
        alertController.addAction(no)
        
        self.present(alertController,
                     animated: true,
                     completion: nil)
    }
}

extension WKWebViewController: WKScriptMessageHandler {
    
    class HybridContainer: NSObject, WKScriptMessageHandler {
        
        weak var parentController:WKWebViewController?
        
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            parentController?.userContentController(userContentController,
                                                    didReceive: message)
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "h5ToNative" {
            
            var page = ""
            
            var number:String?
            
            var orderformId:String?
            
            var vendorOrderInfoId:String?
            
            var vendorFlightInfoId:String?
            
            var arrivedTime:TimeInterval?
            
            var shippingSn:String?
            
            var shippingCode:String?
            
            if let jsonString = message.body as? String,
                let data = jsonString.replacingOccurrences(of: "\'", with: "\"").data(using: .utf8),
                let body = (try? JSONSerialization.jsonObject(with: data,
                                                              options: .allowFragments) as? [String:Any])?.flatMap({ $0 }),
                let pageValue = body["page"] as? String{
                
                page = pageValue
                
                number = body["number"] as? String
                
                orderformId = body["orderformId"] as? String
                
                vendorOrderInfoId = body["vendorOrderInfoId"] as? String
                
                vendorFlightInfoId = body["vendorFlightInfoId"] as? String
                
                shippingSn = body["shippingSn"] as? String
                
                shippingCode = body["shippingCode"] as? String
                
                arrivedTime = (body["arrivedTime"] as? String)
                    .flatMap({ $0.toDouble() })
                    .flatMap({ TimeInterval($0) })
            } else if let body = message.body as? [String:Any], let pageValue = body["page"] as? String{
                page = pageValue
                
                number = body["number"] as? String
                
                orderformId = body["orderformId"] as? String
                
                vendorOrderInfoId = body["vendorOrderInfoId"] as? String
                
                vendorFlightInfoId = body["vendorFlightInfoId"] as? String
                
                shippingSn = body["shippingSn"] as? String
                
                shippingCode = body["shippingCode"] as? String
                
                arrivedTime = (body["arrivedTime"] as? String)
                    .flatMap({ $0.toDouble() })
                    .flatMap({ TimeInterval($0) })
            }
            
            switch page {
            case "center":
                let vm = UserAccountVM()
                
                self
                    .navigationController?
                    .pushViewController(UserAccountVC(viewModel: vm),
                                        animated: true)
            case "news":
                let vc = MessageEntryVC()
                
                self.navigationController?.pushViewController(vc, animated: true)
            case "setting":
                let vc = SettingVC()
                
                self.navigationController?.pushViewController(vc, animated: true)
            case "feedback":
                let vc = FeedbackVC()
                
                self.navigationController?.pushViewController(vc, animated: true)
            case "back":
                FlygoVC.switchMain()
            case "pop":
                let tration = CATransition()
                
                tration.duration = 0.35
                tration.type = kCATransitionMoveIn
                tration.subtype = kCATransitionFromLeft
                tration.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
                
                UIApplication.shared.keyWindow?.layer.add(tration, forKey: "tration")
                
                self.navigationController?.setViewControllers([GetUserInfoVC()],
                                                              animated: false)
            case "callCenter":
                guard let tel = number else {
                    break
                }
                FlygoUtil.callNumber(tel: tel)
            case "scanningCode":
                let scanVC = ScanViewController()
                
                self.navigationController?.present(scanVC,
                                                   animated: true,
                                                   completion: nil)
            case "pickupAddress":
                guard let `orderformId` = orderformId, let `vendorOrderInfoId` = vendorOrderInfoId else {
                    return
                }
                
                let vc = OrderDeliverVC(vendorOrderInfoId: vendorOrderInfoId, orderformId: orderformId)
                
                vc.successHandler = {
                    [weak self] (_, _) in
                    guard let `self` = self else {
                        return
                    }
                    
                    let openudid = OpenUDID.value() ?? ""
                    
                    let urlString = URLConfig.prefix + "flyapp/fly/orderindex.html?customerId=\(UserManager.shareInstance.currentId.urlEncode())&token=\(UserManager.shareInstance.currentToken.urlEncode())&nickname=\(UserManager.shareInstance.currentNickName.urlEncode())&imei=\(openudid.urlEncode())"
                    
                    let webURL = URL(string: urlString)!
                    
                    let request = URLRequest(url: webURL)
                    
                    self.webView.load(request)
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            case "deliver":
                guard let `orderformId` = orderformId,
                    let `vendorOrderInfoId` = vendorOrderInfoId,
                    let `arrivedTime` = arrivedTime
                    else {
                        return
                }
                
                let vc = ScanDeliverVC(vendorOrderInfoId: vendorOrderInfoId,
                                       orderFormId: orderformId,
                                       arriveDate: arrivedTime)
                
                self.navigationController?.pushViewController(vc,
                                                              animated: true)
            case "addFlight":
                AddFlightVC.getFlightmentItem(showOn: self.navigationController!,
                                              callBack: { (item) in
                                                let listVC = FlightListVC(item: item)
                                                
                                                self.navigationController?.pushViewController(listVC,
                                                                                              animated: true)
                })
            case "viewLogistics":
                guard let `shippingSn` = shippingSn, let `shippingCode` = shippingCode else {
                    return
                }
                
                let vc = DeliverDetailVC(shipperCode: shippingCode,
                                         shippingSn: shippingSn)
                
                self.navigationController?.pushViewController(vc,
                                                              animated: true)
            case "pictureUpload":
                guard let `vendorFlightInfoId` = vendorFlightInfoId else {
                    return
                }
                
                self.navigationController?.pushViewController(VerifyTicketVC(vendorFlightInfoId:vendorFlightInfoId),
                                                              animated: true)
            default:
                return
            }
        } else if message.name == "openLocationUrl" {
            var dict:[String:Any]!
            
            if let jsonString = message.body as? String,
                let data = jsonString.replacingOccurrences(of: "\'", with: "\"").data(using: .utf8),
                let body = (try? JSONSerialization.jsonObject(with: data,
                                                              options: .allowFragments) as? [String:Any])?.flatMap({ $0 })
            {
                dict = body
            } else if let body = message.body as? [String:Any] {
                dict = body
            } else {
                return
            }
            
            if let storeName = dict["freeStoreName"] as? String,
                storeName == "日上免税店" {
                self.showToast(text: "亲爱的飞哥，由于日上免税店尚未开放官网购买渠道，请您在首都国际机场和上海国际机场的日上免税店，或下载”日上会员”app进行选购，给您带来的不便请谅解，谢谢！")
                
                return
            }
            
            var url = dict["url"] as? String
            
            if let openUrlString = url, (!openUrlString.hasPrefix("http://") && !openUrlString.hasPrefix("https://") && !openUrlString.hasPrefix("prefs:root=")) {
                url = "http://" + openUrlString
            }
            
            if #available(iOS 10.0, *) {
                if let openUrlString = url, openUrlString.hasPrefix("prefs:root=") {
                    url = "App-Prefs:root=" + (openUrlString as NSString).substring(from: 11)
                }
            }
            
            guard let openUrlString = url, let openUrl = URL(string: "\(openUrlString)") else {
                return
            }
            
            UIApplication.shared.openURL(openUrl)
        } else if message.name == "hrefHtml" {
            guard let dict = message.body as? [String:Any],
                let recId = dict["productRecId"] as? String else {
                    return
            }
            
            let showCart = (dict["enableCart"] as? Bool) ?? false
            
            let showOrder = (dict["enableOrder"] as? Bool) ?? false
            
            let vm = ProductDetailVM(productId: recId)
            
            let vc = ProductDetailVC(viewModel: vm,
                                     showCart: showCart,
                                     canBuy: showOrder)
            
            self.navigationController?.pushViewController(vc,
                                                          animated: true)
        } else if message.name == "contact" {
            guard let dict = message.body as? [String:Any],
                let customerId = dict["customerId"] as? String,
                let nickName = dict["nickName"] as? String,
                let session = ChatManager.currentSession else {
                    
                    return
            }
            
            let avatar = (dict["avatar"] as? String).flatMap({ $0.length > 0 ? URL(string: URLConfig.image + $0) : nil })
            
            if customerId == UserManager.shareInstance.currentId {
                
                self.showToast(text: "这是我自己的订单^_^")
                
                return
            }
            
            if let conversation = session
                .fetchConversationInCache(for: customerId,
                                          nickName: nickName,
                                          avatarPath: avatar) {
                let vc = ChatRoomVC(conversation: conversation)
                
                self.navigationController?.pushViewController(vc,
                                                              animated: true)
                
                return
            }
            
            let toast = self.showLoading()
            
            session
                .fetchConversationFromServer(for: customerId,
                                             nickName: nickName,
                                             avatarPath: avatar) { (result) in
                                                switch result {
                                                case .failture(let error):
                                                    toast.displayLabel(text: error.localizedDescription)
                                                case .success(let conversation):
                                                    toast.dismiss(animated: true,
                                                                  completion: {
                                                                    let vc = ChatRoomVC(conversation: conversation)
                                                                    
                                                                    self.navigationController?.pushViewController(vc,
                                                                                                                  animated: true)
                                                    })
                                                }
            }
        } else if message.name == "share" {
            guard let dict = message.body as? [String:Any],
                let title = dict["title"] as? String,
                let imgUrlString = dict["imgUrl"] as? String,
                let url = dict["url"] as? String,
                let content = dict["content"] as? String else {
                    return
            }
            
            WechatManager.shareInstance.showShareProductActionSheet(on: self,
                                                                    website: (title:title, imageUrl:URL(string: imgUrlString), url:url, content:content),
                                                                    sender: self.view)
        } else if message.name == "login" {
            let jsString = (message.body as? [String:Any]).flatMap({ $0["js"] as? String })
            
            LoginVC.doActionIfNeedLogin {
                
                var userObjectJS = "{"
                
                if UserManager.shareInstance.isLogined() {
                    /// add custom id
                    userObjectJS += "\"customerId\":\"\(UserManager.shareInstance.currentId)\","
                    
                    /// nickname
                    userObjectJS += "\"nickname\":\"\(UserManager.shareInstance.currentNickName)\","
                    
                    /// avatar
                    userObjectJS += "\"avatar\":\"\(UserManager.shareInstance.avatarURL?.absoluteString ?? "")\""
                }
                
                userObjectJS += "}"
                
                self.webView.evaluateJavaScript("ios_setuserinfo(\(userObjectJS))",
                                                completionHandler: nil)
                
                if let _jsString = jsString {
                    self.webView.evaluateJavaScript(_jsString,
                                                    completionHandler: nil)
                }
            }
        }
    }
}

extension WKWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView,
                 didReceive challenge: URLAuthenticationChallenge,
                 completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let credential = challenge.protectionSpace.serverTrust.map{ URLCredential.init(trust: $0) }
        
        completionHandler(.useCredential,
                          credential)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.switchLoadingView()
        
        self.view.bringSubview(toFront: progressView)
        
        progressView.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        let nserror = error as NSError
        
        self.switchFailedView(error: nserror.localizedDescription)
        
        progressView.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.switchNormal()
        
        progressView.isHidden = true
        
        webView.evaluateJavaScript("setNotificationEnable(\(RemoteManager.shareInstance.remoteEnable.value ? "true" : "false"))",
            completionHandler: {
                (_, error) in
                
                print("error is \(error.debugDescription)")
        })
    }
}

