//
//  CityPickerViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit
import ObjectMapper

class CityPickerViewController: UIViewController {
    
    let cityCollection:[Province]
    
    var currentProvince:Province
    
    var currentCity:City
    
    var currentArea:String
    
    var confirmHandler:(String, String, String) -> Void
    
    init(default value:(String,String,String), confirmHandler:@escaping (String, String, String) -> Void) {
        
        self.confirmHandler = confirmHandler
        
        /// load city info
                let array = NSArray(contentsOfFile: Bundle.main.bundlePath + "/CityPickerViewController.plist")
        
        cityCollection = [Province](JSONArray:array as! [[String : Any]])!
        
        let provinces = cityCollection
        
        currentProvince = {
            (name) -> Province in
            
            for one in provinces {
                if one.name == name {
                    return one
                }
            }
            
            return provinces[0]
        }(value.0)
        
        let citys = currentProvince.city
        
        currentCity = {
            (name) -> City in
            
            for one in citys {
                if one.name == name {
                    return one
                }
            }
            
            return citys[0]
        }(value.1)
        
        let areas = currentCity.area
        
        currentArea = {
            (name) -> String in
            
            for one in areas {
                if one == name {
                    return one
                }
            }
            
            return areas[0]
        }(value.2)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        let pIndex = cityCollection.index(of: currentProvince) ?? 0
        
        let cIndex = currentProvince.city.index(of: currentCity) ?? 0
        
        let aIndex = currentCity.area.index(of: currentArea) ?? 0
        
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIConfig.generalColor.white
        
        self.view.addSubview(pickerView)
        
        pickerView.snp.makeConstraints { (maker) in
            maker.left.bottom.right.equalTo(self.view)
        }
        
        let toolbar = UIView()
        
        self.view.addSubview(toolbar)
        
        toolbar.backgroundColor = UIConfig.generalColor.white
        toolbar.snp.makeConstraints { (maker) in
            maker.left.right.equalTo(pickerView)
            maker.bottom.equalTo(pickerView.snp.top)
            maker.height.equalTo(50)
            maker.top.equalTo(self.view)
        }
        
        let cancelButton = UIButton(type: .custom)
        
        toolbar.addSubview(cancelButton)
        
        cancelButton.setTitle("取消", for: .normal)
        cancelButton.setTitleColor(UIConfig.generalColor.titleColor,
                                   for: .normal)
        cancelButton.addTarget(self,
                               action: #selector(CityPickerViewController.dismissViewController),
                               for: .touchUpInside)
        cancelButton.backgroundColor = UIConfig.generalColor.white
        cancelButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(toolbar).offset(15)
            maker.top.bottom.equalTo(toolbar)
        }
        
        let confirmButton = UIButton(type: .custom)
        
        toolbar.addSubview(confirmButton)
        
        confirmButton.setTitle("确定", for: .normal)
        confirmButton.setTitleColor(UIConfig.generalColor.titleColor,
                                   for: .normal)
        confirmButton.addTarget(self,
                               action: #selector(CityPickerViewController.confirmAndDismiss),
                               for: .touchUpInside)
        confirmButton.backgroundColor = UIConfig.generalColor.white
        confirmButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(toolbar).offset(-15)
            maker.top.bottom.equalTo(toolbar)
        }
        
        pickerView.reloadAllComponents()
        
        pickerView.selectRow(pIndex,
                             inComponent: 0,
                             animated: false)
        pickerView.selectRow(cIndex,
                             inComponent: 1,
                             animated: false)
        pickerView.selectRow(aIndex,
                             inComponent: 2,
                             animated: false)
    }
    
    func dismissViewController() {
        self.dismissVC(completion: nil)
    }
    
    func confirmAndDismiss() {
        confirmHandler(currentProvince.name, currentCity.name, currentArea)
        
        dismissViewController()
    }
    
}

extension CityPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    struct City : Model, Equatable {
        var name:String = ""
        
        var area:[String] = []
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            name <- map["name"]
            area <- map["area"]
        }
        
        static func ==(lhs: City, rhs: City) -> Bool {
            return lhs.name == rhs.name
        }
    }
    
    struct Province : Model, Equatable {
        var name:String = ""
        
        var city:[City] = []
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            name <- map["name"]
            city <- map["city"]
        }
        
        static func ==(lhs: Province, rhs: Province) -> Bool {
            return lhs.name == rhs.name
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return cityCollection.count
        case 1:
            return currentProvince.city.count
        case 2:
            return currentCity.area.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let title = {
            () -> String in
            switch component {
            case 0:
                return cityCollection[row].name
            case 1:
                return currentProvince.city[row].name
            case 2:
                return currentCity.area[row]
            default:
                return ""
            }
        }()
        
        return NSAttributedString(string: title,
                                  font: UIConfig.generalFont(15),
                                  textColor: UIConfig.generalColor.selected)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            currentProvince = cityCollection[row]
            
            currentCity = currentProvince.city[0]
            currentArea = currentCity.area[0]
            
            pickerView.reloadComponent(1)
            pickerView.reloadComponent(2)
        case 1:
            currentCity = currentProvince.city[row]
            currentArea = currentCity.area[0]
            
            pickerView.reloadComponent(2)
        case 2:
            currentArea = currentCity.area[row]
        default:
            return
        }
    }
}

