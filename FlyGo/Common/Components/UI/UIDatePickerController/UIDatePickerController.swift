//
//  UIDatePickerController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/30.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class UIDatePickerController: UIViewController {
    
    let datePicker:UIDatePicker
    
    let callBack:(Date) -> Void
    
    var currentDate:Date
    
    init(mode:UIDatePickerMode, miniDate:Date = Date(timeIntervalSince1970: Date().timeIntervalSince1970 + 24 * 60 * 60), callBack:@escaping (Date) -> Void) {
        self.datePicker = UIDatePicker()
        self.datePicker.datePickerMode = mode
        
        self.currentDate = miniDate
        
        self.datePicker.minimumDate = self.currentDate
        self.datePicker.maximumDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 + 24 * 60 * 60 * 365)
        self.datePicker.date = self.currentDate
        
        self.callBack = callBack
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        datePicker.backgroundColor = UIConfig.generalColor.white
        
        self.view.addSubview(datePicker)
        
        datePicker.addTarget(self,
                             action: #selector(UIDatePickerController.changeDate),
                             for: UIControlEvents.valueChanged)
        datePicker.snp.makeConstraints { (maker) in
            maker.left.bottom.right.equalTo(self.view)
        }
        
        let toolbar = UIView()
        
        self.view.addSubview(toolbar)
        
        toolbar.backgroundColor = UIConfig.generalColor.white
        toolbar.snp.makeConstraints { (maker) in
            maker.left.right.equalTo(datePicker)
            maker.bottom.equalTo(datePicker.snp.top)
            maker.height.equalTo(50)
            maker.top.equalTo(self.view)
        }
        
        let cancelButton = UIButton(type: .custom)
        
        toolbar.addSubview(cancelButton)
        
        cancelButton.setTitle("取消", for: .normal)
        cancelButton.setTitleColor(UIConfig.generalColor.titleColor,
                                   for: .normal)
        cancelButton.addTarget(self,
                               action: #selector(CityPickerViewController.dismissViewController),
                               for: .touchUpInside)
        cancelButton.backgroundColor = UIConfig.generalColor.white
        cancelButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(toolbar).offset(15)
            maker.top.bottom.equalTo(toolbar)
        }
        
        let confirmButton = UIButton(type: .custom)
        
        toolbar.addSubview(confirmButton)
        
        confirmButton.setTitle("确定", for: .normal)
        confirmButton.setTitleColor(UIConfig.generalColor.titleColor,
                                    for: .normal)
        confirmButton.addTarget(self,
                                action: #selector(CityPickerViewController.confirmAndDismiss),
                                for: .touchUpInside)
        confirmButton.backgroundColor = UIConfig.generalColor.white
        confirmButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(toolbar).offset(-15)
            maker.top.bottom.equalTo(toolbar)
        }
    }
    
    func changeDate() {
        currentDate = self.datePicker.date
    }
    
    func dismissViewController() {
        self.dismissVC(completion: nil)
    }
    
    func confirmAndDismiss() {
        
        callBack(currentDate)
        
        dismissViewController()
    }

}
