//
//  CoverTableViewCell.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/17.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation


/// CoverCell
class CoverTableViewCell: UITableViewCell, UIScrollViewDelegate {
    
    func scrollInTableView(_ sv:UIScrollView) {
        let offsetX = sv.contentOffset.y + sv.contentInset.top
        
        let scale:CGFloat = {
            if (offsetX < 0) {
                return (self.h - offsetX) / self.h
            } else {
                return 1.0
            }
        }()
        
        scrollView.transform = CGAffineTransform(scaleX: scale,
                                                 y: scale)
    }
    
//    static var cellHeight:CGFloat = 8 * UIScreen.main.bounds.w / 15
    
    static let squareSize = CGSize(width: 20, height: 2)
    
    static let gap:CGFloat = 10
    
    let scrollView:UIScrollView
    
    let pageControl:PageControl
    
    class PageControl : UIPageControl {
        override var currentPage: Int {
            //willSet {
            didSet { //so updates will take place after page changed
                self.updateDots()
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            self.updateDots()
        }
        
        func updateDots() {
            
            if subviews.count <= 0 { return }
            
            self.w = (CGFloat(subviews.count - 1) * CoverTableViewCell.gap + CGFloat(subviews.count) * CoverTableViewCell.squareSize.width)
            
            var left:CGFloat = 0
            
            for (i, view) in subviews.enumerated() {
                view.frame = CGRect(x: left,
                                    y: -CoverTableViewCell.squareSize.height / 2.0,
                                    w: CoverTableViewCell.squareSize.width,
                                    h: CoverTableViewCell.squareSize.height)
                
                view.backgroundColor = i == currentPage ? UIConfig.generalColor.selected : UIConfig.generalColor.selected.withAlphaComponent(0.3)
                
                left = view.right + CoverTableViewCell.gap
            }
        }
    }
    
    var itemViews:[UIView] = [] {
        didSet (oldVal) {
            for one in oldVal {
                one.removeFromSuperview()
            }
            
            for one in itemViews {
                scrollView.addSubview(one)
            }
            
            adjustItemViewFrmes()
            
            pageControl.isHidden = itemViews.count < 2
        }
    }
    
    func adjustItemViewFrmes() {
        
        scrollView.frame = self.contentView.bounds
        
        if itemViews.count < 1 {
            
            pageControl.numberOfPages = 0
            pageControl.currentPage = 0
            
            return
        }
        
        let width = self.w
        
        let height = self.h
        
        for (index, view) in itemViews.enumerated() {
            view.frame = CGRect(x: CGFloat(index) * width,
                                y: 0,
                                w: width,
                                h: height)
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(itemViews.count) * width, height: 0)
        
        pageControl.currentPage = 0
        pageControl.numberOfPages = itemViews.count
        pageControl.centerXInSuperView()
        pageControl.h = 10
        pageControl.bottom = self.contentView.h - 0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustItemViewFrmes()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.w + 0.5)
    }
    
    var urlList:[URL?] = [] {
        didSet {
            var viewCollection = [UIView]()
            
            for (index, url) in urlList.enumerated() {
                let imageView = UIImageView()
                
                imageView.clipsToBounds = true
                imageView.contentMode = imageViewContentMode
                imageView.kf.setImage(with:url)
                imageView.isUserInteractionEnabled = true
                imageView.addTapGesture(target: self,
                                        action: #selector(CoverTableViewCell.coverTap(sender:)))
                imageView.tag = index
                
                viewCollection.append(imageView)
            }
            
            itemViews = viewCollection
        }
    }
    
    final func coverTap(sender:UITapGestureRecognizer) {
        let tag = sender.view?.tag ?? 0
        
        coverTapHandler(with: tag)
    }
    
    func coverTapHandler(with tag:Int) {
        
    }
    
    let imageViewContentMode:UIViewContentMode
    
    init(contentMode:UIViewContentMode = .scaleAspectFill) {
        
        imageViewContentMode = contentMode
        
        scrollView = UIScrollView()
        
        pageControl = PageControl()
        
        super.init(style: .default, reuseIdentifier: "CoverTableViewCell")
        
        self.contentView.addSubview(scrollView)
        
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        scrollView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
        
        self.contentView.addSubview(pageControl)
        
        pageControl.isUserInteractionEnabled = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var autoScroll:Bool = false {
        didSet {
            startScrollTimer()
        }
    }
    
    var scrollTimer:Timer?
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollTimer?.invalidate()
        scrollTimer = nil
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        startScrollTimer()
    }
    
    func startScrollTimer() {
        scrollTimer?.invalidate()
        
        if autoScroll {
            scrollTimer = Timer.scheduledTimer(timeInterval: 4.0,
                                               target: self,
                                               selector: #selector(CoverTableViewCell.scrollToNextPage),
                                               userInfo: nil,
                                               repeats: true)
        } else {
            scrollTimer = nil
        }
    }
    
    func scrollToNextPage() {
        var page = pageControl.currentPage
        
        let pageCount = pageControl.numberOfPages
        
        if page < (pageCount - 1) {
            page += 1
        } else {
            page = 0
        }
        
        scrollView.setContentOffset(CGPoint(x: CGFloat(page) * scrollView.bounds.size.width, y: 0),
                                    animated: true)
    }
}
