//
//  CommonNavigationVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/11/18.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import UIKit

class CommonNavigationVC: CustomNavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = UIConfig.generalColor.selected
        self.navigationBar.backIndicatorImage = UIImage(named:"icon_return")?.withRenderingMode(.alwaysOriginal)
        self.navigationBar.backIndicatorTransitionMaskImage = UIImage(named:"icon_return")?.withRenderingMode(.alwaysOriginal)
        
        // add navigation backgroud
        let backgroudView = ViewFactory.view(color: UIConfig.generalColor.barColor)
        
        backgroudView.frame = CGRect(x: 0,
                                     y: 0,
                                     w: self.navigationBar.w,
                                     h: 64)
        
        self.navigationBar.subviews[0].addSubview(backgroudView)
        
        // Title
        var attr = [String : Any]()
        
        attr[NSForegroundColorAttributeName] = UIConfig.generalColor.selected
        attr[NSFontAttributeName] = UIConfig.generalSemiboldFont(20.0)
        
        self.navigationBar.titleTextAttributes = attr
    }
    
    static func item(with title:String, target:Any, action:Selector) -> UIBarButtonItem {
        let item = UIBarButtonItem(title: title,
                                   style: .done,
                                   target: target,
                                   action: action)
        
        var attr = [String : Any]()
        
        attr[NSForegroundColorAttributeName] = UIConfig.generalColor.selected
        attr[NSFontAttributeName] = UIConfig.generalFont(15.0)
        
        item.setTitleTextAttributes(attr,
                                    for: .normal)
        
        attr[NSForegroundColorAttributeName] = UIConfig.generalColor.unselected
        
        item.setTitleTextAttributes(attr,
                                    for: .disabled)
        
        return item
    }
}


extension UIViewController {
    @objc
    var showNavigationBar:Bool {
        return true
    }
    
    func ms_viewWillAppear(_ animated: Bool) {
        ms_viewWillAppear(animated)
        
        if let navigationController = self.navigationController,
            navigationController.isNavigationBarHidden == showNavigationBar {
            navigationController.setNavigationBarHidden(!showNavigationBar,
                                                        animated: animated)
        }
    }
    
    static func methodSwizzling() {
        let oriSelector = #selector(UIViewController.viewWillAppear(_:))
        
        let swizzledSelector = #selector(UIViewController.ms_viewWillAppear(_:))
        
        let oriMethod = class_getInstanceMethod(UIViewController.classForCoder(),
                                                oriSelector)
        
        let swizzledMethod = class_getInstanceMethod(UIViewController.classForCoder(),
                                                     swizzledSelector)
        
        method_exchangeImplementations(oriMethod, swizzledMethod)
    }
}
