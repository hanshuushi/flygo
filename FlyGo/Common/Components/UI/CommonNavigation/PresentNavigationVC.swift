//
//  PresentNavigationVC.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/14.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation

class PresentNavigationVC: CommonNavigationVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()    }
    
    func dismissViewController () {
        self.dismiss(animated: true,
                     completion: nil)
    }
    
    override func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        super.navigationController(navigationController,
                                   willShow: viewController,
                                   animated: animated)
        
        if viewControllers.count > 0 &&
            viewControllers[0] == viewController {
            viewController
                .navigationItem
                .leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon_close"),
                                                     style: .plain,
                                                     target: self,
                                                     action: #selector(PresentNavigationVC.dismissViewController))
        }
    }
}
