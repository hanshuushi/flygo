//
//  TimePickerViewController.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/3/28.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import UIKit
import ObjectMapper

class TimePickerViewController: UIViewController {
    
    var confirmHandler:(Date) -> Void
    
    let hasToday:Bool
    
    let currentHour:Int
    
    let todayCount:Int
    
    var daySelected:Int = 0
    
    var hourSelected:Int = 0
    
    static var chinaDate:Date {
        return Date(timeIntervalSince1970: Date().timeIntervalSince1970 + TimeInterval(timeOffset))
    }
    
    static var timeOffset:Int {
        return chinaTimeZone.secondsFromGMT() - TimeZone.current.secondsFromGMT()
    }
    
    static var isChinaTimeZone:Bool {
        return abs(TimeZone.current.secondsFromGMT() - chinaTimeZone.secondsFromGMT()) <= 0
    }
    
    static let chinaTimeZone = TimeZone(identifier: "Asia/Shanghai")!
    
    let currentChinadate:Date = Date()
    
    var calendar = Calendar(identifier: .gregorian)
    
    init(confirmHandler:@escaping (Date) -> Void) {
        
        self.confirmHandler = confirmHandler
        
        /// 强制转换为中国时区
        calendar.timeZone = TimePickerViewController.chinaTimeZone
        
        currentHour = calendar.component(.hour,
                                         from: currentChinadate)
        
        todayCount = max(0, min(12, 19 - currentHour))
        
        hasToday = todayCount > 0
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIConfig.generalColor.white
        
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIConfig.generalColor.white
        
        self.view.addSubview(pickerView)
        
        pickerView.snp.makeConstraints { (maker) in
            maker.left.bottom.right.equalTo(self.view)
        }
        
        let toolbar = UIView()
        
        self.view.addSubview(toolbar)
        
        toolbar.backgroundColor = UIConfig.generalColor.white
        toolbar.snp.makeConstraints { (maker) in
            maker.left.right.equalTo(pickerView)
            maker.bottom.equalTo(pickerView.snp.top)
            maker.height.equalTo(50)
            maker.top.equalTo(self.view)
        }
        
        let cancelButton = UIButton(type: .custom)
        
        toolbar.addSubview(cancelButton)
        
        cancelButton.setTitle("取消", for: .normal)
        cancelButton.setTitleColor(UIConfig.generalColor.titleColor,
                                   for: .normal)
        cancelButton.addTarget(self,
                               action: #selector(CityPickerViewController.dismissViewController),
                               for: .touchUpInside)
        cancelButton.backgroundColor = UIConfig.generalColor.white
        cancelButton.snp.makeConstraints { (maker) in
            maker.left.equalTo(toolbar).offset(15)
            maker.top.bottom.equalTo(toolbar)
        }
        
        let confirmButton = UIButton(type: .custom)
        
        toolbar.addSubview(confirmButton)
        
        confirmButton.setTitle("确定", for: .normal)
        confirmButton.setTitleColor(UIConfig.generalColor.titleColor,
                                    for: .normal)
        confirmButton.addTarget(self,
                                action: #selector(CityPickerViewController.confirmAndDismiss),
                                for: .touchUpInside)
        confirmButton.backgroundColor = UIConfig.generalColor.white
        confirmButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(toolbar).offset(-15)
            maker.top.bottom.equalTo(toolbar)
        }
        
        if !TimePickerViewController.isChinaTimeZone {
            let label = ViewFactory.generalLabel(generalSize: 13,
                                                 textColor: UIConfig.generalColor.labelGray,
                                                 backgroudColor: UIConfig.generalColor.white)
            
            toolbar.addSubview(label)
            
            label.text = "中国时间为\(TimePickerViewController.chinaDate.generalFormartString)"
            label.snp.centerInSuperView()
        }
        
        pickerView.reloadAllComponents()
    }
    
    func dismissViewController() {
        self.dismissVC(completion: nil)
    }
    
    func confirmAndDismiss() {
        
        var cmps = calendar.dateComponents(in: TimePickerViewController.chinaTimeZone,
                                           from: currentChinadate)
        
        cmps.hour = 0
        cmps.minute = 0
        cmps.second = 0
        
        guard let startDate = calendar.date(from: cmps) else {
            dismissViewController()
            
            return
        }
        
        let offsetDayTimeZone = TimeInterval((hasToday ? 0 : 1) + daySelected) * 24 * 60.0 * 60.0
        
        let hourTimeZone = TimeInterval((hasToday && daySelected == 0) ? 20 - todayCount + hourSelected : hourSelected + 8) * 60 * 60
        
        let selectedDate = Date(timeIntervalSince1970: startDate.timeIntervalSince1970 + offsetDayTimeZone + hourTimeZone + TimeInterval(TimePickerViewController.timeOffset))
        
        confirmHandler(selectedDate)
        
        dismissViewController()
    }
}

extension TimePickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return hasToday ? 3 : 2
        case 1:
            let isToday = hasToday && daySelected == 0
            
            return isToday ? todayCount : 12
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let title = {
            () -> String in
            switch component {
            case 0:
                if hasToday {
                    if row == 0 {
                        return "今天"
                    } else if row == 1 {
                        return "明天"
                    } else if row == 2 {
                        return "后天"
                    }
                } else {
                    if row == 0 {
                        return "明天"
                    } else if row == 1 {
                        return "后天"
                    }
                }
                return ""
            case 1:
                let isToday = hasToday && daySelected == 0
                
                let hour = isToday ? 20 - todayCount + row : row + 8
                
                return "\(hour):00~\(hour + 1):00"
            default:
                return ""
            }
        }()
        
        return NSAttributedString(string: title,
                                  font: UIConfig.generalFont(15),
                                  textColor: UIConfig.generalColor.selected)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            daySelected = row + (hasToday ? 0 : 1)
            
            pickerView.reloadComponent(1)
            pickerView.selectRow(0, inComponent: 1, animated: false)
        case 1:
            hourSelected = row
        default:
            return
        }
    }
}

