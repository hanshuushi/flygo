//
//  FlygoUtil.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/9.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation

class FlygoUtil: NSObject {
    
    @objc
    static func callServer() {
        let vc = CallServerVC()
        
        if let nc = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            nc.pushViewController(vc,
                                  animated: true)
        }
    }
    
    @objc
    static func callNumber(tel:String = "13022108361") {
        let telStr = "tel://\(tel)"
        
        let webView = UIWebView()
        
        let url = URL(string: telStr)
        
        webView.loadRequest(URLRequest(url: url!))
        
        UIApplication.shared.keyWindow?.addSubview(webView)
    }
    
    static func attributedString(number:Int) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: "x",
                                                         font: UIConfig.arialFont(10),
                                                         textColor: UIConfig.generalColor.labelGray)
        
        attributedString
            .append(NSAttributedString(string: "\(number)",
                font: UIConfig.arialFont(13),
                textColor: UIConfig.generalColor.labelGray))
        
        return attributedString
    }
}
