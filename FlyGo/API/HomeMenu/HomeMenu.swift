//
//  HomeMenu.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/6.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension API {
    
    struct SearchItem: Model {
        var hotSearchId = ""
        var keyword = ""		 		//热门搜索关键字
        var type = 1				//热门类型		1商品       2分类          3品牌
        var clickRate = 0			//点击率
        var clickNum = 0
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            hotSearchId             <- map["hotSearchId"]
            keyword                 <- map["keyword"]
            type                    <- map["type"]
            clickRate               <- map["clickRate"]
            clickNum                <- map["clickNum"]
        }
        
        static func getSearchList() -> HttpSession {
            return HttpSession.get(urlString: "index/getHotSearch")
        }
    }
    
    struct SubjectInfo: Model {
        struct ModularInfo: Model {
            var modularId:String   = ""
            var modularName:String = ""
            var homeMenuId:String  = ""
            var pic:URL?
            var title:String       = ""
            var productList:[HomeMenuItem.Promotion.Item] = []
            
            init?(map: Map) {
            }
            
            mutating func mapping(map: Map) {
                modularId       <-  map["modularId"]
                modularName     <-  map["modularName"]
                homeMenuId      <-  map["homeMenuId"]
                pic             <-  (map["pic"], API.PicTransform())
                title           <-  map["useId"]
                productList     <-  map["productList"]
            }
        }
        
        var modularInfo:ModularInfo!
        
        init?(map: Map) {
            modularInfo     <-  map["modularInfo"]
            
            if modularInfo == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            modularInfo     <-  map["modularInfo"]
        }
        
        static func getSubjectInfo(modularId:String) -> Observable<APIItem<SubjectInfo>> {
            return HttpSession.get(urlString: "index/getModularInfo",
                                   params: ["modularId": modularId])
        }
    }
    
    struct HomeMenuItem : Model {
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            modularList         <- map["modularList"]
            menuTypeList        <- map["menuTypeList"]
            promotion           <- map["promotion"]
            menuBrand           <- map["menuBrand"]
            active              <- map["active"]
            banner              <- map["banner"]
            top                 <- map["top"]
        }
        
        /// Modular
        struct Modular: Model {
            var modularId:String!
            
            var modularName:String = ""
            
            var pic:URL?
            
            var productList:[Promotion.Item] = []
            
            init?(map: Map) {
                modularId <- map["modularId"]
                if modularId == nil { return nil }
            }
            
            mutating func mapping(map: Map) {
                modularId           <- map["modularId"]
                modularName         <- map["modularName"]
                productList         <- map["productList"]
                pic                 <- (map["pic"], API.PicTransform())
                
            }
        }
        
        var modularList:[Modular] = []
        
        /// MenuType
        var menuTypeList:[ProductType] = []
        
        /// Promotion
        struct Promotion : Model {
            struct Item : Model {
                var productId:String!
                
                var productRecId:String = ""
                
                var productName:String = ""
                
                var miniPrice:Currency = Currency(unit: "$", value: 0)
                
                var miniDomestic:Currency = Currency(unit: "￥", value: 0)
                
                var cnyPrice:Currency = Currency(unit: "￥", value: 0)
                
                var pic:URL?
                
                var useId:String = ""
                
                var currencySymbols:String?
                
                var sellPrice:Currency = Currency(unit: "$", value: 0)
                
                init?(map: Map) {
                    productId <- map["productId"]
                    
                    if productId == nil { return nil }
                }
                
                mutating func mapping(map: Map) {
                    productId       <- map["productId"]
                    productRecId    <- map["productRecId"]
                    productName     <- map["productName"]
                    miniPrice       <- (map["miniPrice"], API.CurrencyTransform(defaultUnit:"$"))
                    miniDomestic    <- (map["miniDomestic"], API.CurrencyTransform(defaultUnit:"￥"))
                    cnyPrice        <- (map["cnyPrice"], API.CurrencyTransform(defaultUnit:"￥"))
                    pic             <- (map["pic"], API.PicTransform())
                    useId           <- map["useId"]
                    currencySymbols <- map["currencySymbols"]
                    sellPrice       <- (map["sellPrice"], API.CurrencyTransform(defaultUnit:"$"))
                    
                    if let _currencySymbols = currencySymbols {
                        sellPrice.unit = _currencySymbols
                        
                        miniPrice.unit = _currencySymbols
                    }
                }
            }
            
            var promotionName:String = ""
            
            var type:Int = 0
            
            var promotionList:[Item] = []
            
            init?(map: Map) {
                
            }
            
            init () {
                promotionName = ""
                promotionList = []
                type = 0
            }
            
            mutating func mapping(map: Map) {
                promotionName       <- map["promotionName"]
                promotionList       <- map["promotionList"]
                type                <- (map["type"], API.IntTransform())
            }
        }
        
        var promotion:Promotion?
        
        /// MenuBrand
        struct MenuBrand: Model {
            struct Item: Model {
                var menuBrandId:String!
                
                var menuBrandName:String = ""
                
                var brandId:String = ""
                
                var pic:URL? = nil
                
                init?(map: Map) {
                    menuBrandId <- map["menuBrandId"]
                    
                    if menuBrandId == nil { return nil }
                }
                
                mutating func mapping(map: Map) {
                    menuBrandId         <- map["menuBrandId"]
                    menuBrandName       <- map["menuBrandName"]
                    brandId             <- map["brandId"]
                    pic                 <- (map["pic"], API.PicTransform())
                }
            }
            
            var menuBrandName:String = ""
            
            var type:Int = 0
            
            var menuBrandList:[Item] = []
            
            init?(map: Map) {
                
            }
            
            init () {
                menuBrandName = ""
                menuBrandList = []
                type = 0
            }
            
            mutating func mapping(map: Map) {
                menuBrandName       <- map["menuBrandName"]
                menuBrandList       <- map["menuBrandList"]
                type                <- (map["type"], API.IntTransform())
            }
        }
        
        var menuBrand:MenuBrand?
        
        /// Active
        struct Active: Model {
            struct Item: Model {
                var activityId:String!
                
                var title:String = ""
                
                var pic:URL? = nil
                
                var linkAddress:URL? = nil
                
                var useId:String = ""
                
                var pageViews:Int = 0
                
                var basePageViews:Int = 0
                
                init?(map: Map) {
                    activityId <- map["activityId"]
                    
                    if activityId == nil { return nil }
                }
                
                mutating func mapping(map: Map) {
                    activityId          <- map["activityId"]
                    title               <- map["title"]
                    useId               <- map["useId"]
                    pic                 <- (map["pic"], API.PicTransform())
                    linkAddress         <- (map["linkAddress"], API.URLTransform())
                    pageViews           <- (map["pageViews"], API.IntTransform())
                    basePageViews       <- (map["basePageViews"], API.IntTransform())
                }
            }
            
            var activeName:String = ""
            
            var type:Int = 0
            
            var activeList:[Item] = []
            
            init?(map: Map) {
                
            }
            
            init () {
                activeName = ""
                activeList = []
                type = 0
            }
            
            mutating func mapping(map: Map) {
                activeName          <- map["activeName"]
                activeList          <- map["activeList"]
                type                <- (map["type"], API.IntTransform())
            }
        }
        
        var active:Active?
        
        /// Banner
        struct Banner: Model {
            struct Item: Model {
                var carouselId:String!
                
                var title:String = ""
                
                var linkAddress:URL? = nil
                
                var pic:URL? = nil
                
                init?(map: Map) {
                    carouselId <- map["carouselId"]
                    if carouselId == nil { return nil }
                }
                
                mutating func mapping(map: Map) {
                    carouselId          <- map["carouselId"]
                    title               <- map["title"]
                    linkAddress         <- (map["linkAddress"], API.URLTransform())
                    pic                 <- (map["pic"], API.PicTransform())
                }
            }
            
            var type:Int = 0
            
            var bannerList:[Item] = []
            
            init?(map: Map) {
                
            }
            
            init () {
                bannerList = []
                type = 0
            }
            
            mutating func mapping(map: Map) {
                bannerList          <- map["bannerList"]
                type                <- (map["type"], API.IntTransform())
            }
        }
        
        var banner:Banner?
        
        
        /// Top
        struct Top : Model {
            struct Item : Model {
                var productId:String!
                
                var productRecId:String = ""
                
                var productName:String = ""
                
                var miniPrice:Currency = Currency(unit: "$", value: 0)
                
                var miniDomestic:Currency = Currency(unit: "￥", value: 0)
                
                var cnyPrice:Currency = Currency(unit: "￥", value: 0)
                
                var pic:URL?
                
                var useId:String = ""
                
                var currencySymbols:String?
                
                var sellPrice:Currency = Currency(unit: "$", value: 0)
                
                init?(map: Map) {
                    productId <- map["productId"]
                    
                    if productId == nil { return nil }
                }
                
                mutating func mapping(map: Map) {
                    productId       <- map["productId"]
                    productRecId    <- map["productRecId"]
                    productName     <- map["productName"]
                    miniPrice       <- (map["miniPrice"], API.CurrencyTransform(defaultUnit:"$"))
                    miniDomestic    <- (map["miniDomestic"], API.CurrencyTransform(defaultUnit:"￥"))
                    cnyPrice        <- (map["cnyPrice"], API.CurrencyTransform(defaultUnit:"￥"))
                    pic             <- (map["pic"], API.PicTransform())
                    useId           <- map["useId"]
                    currencySymbols <- map["currencySymbols"]
                    sellPrice       <- (map["sellPrice"], API.CurrencyTransform(defaultUnit:"$"))
                    
                    if let _currencySymbols = currencySymbols {
                        sellPrice.unit = _currencySymbols
                        
                        miniPrice.unit = _currencySymbols
                    }
                }
            }
            
            var topName:String = ""
            
            var type:Int = 0
            
            var topList:[Item] = []
            
            init?(map: Map) { }
            
            init() {
                topName = ""
                topList = []
                type    = 0
            }
            
            mutating func mapping(map: Map) {
                topName             <- map["topName"]
                topList             <- map["topList"]
                type                <- (map["type"], API.IntTransform())
            }
        }
        
        var top:Top?
        
        static func getMenuModular (homeMenuId:String = "1",
                                    state:Int = 0) -> Observable<APIItem<HomeMenuItem>>{
            return HttpSession.get(urlString: "index/getMenuModular",
                                   params: ["homeMenuId":homeMenuId,
                                            "state":state
                ])
        }
    }
}
