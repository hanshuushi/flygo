//
//  Airport.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/6.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import RxCocoa

extension FMDatabaseQueue: Disposable {
    public func dispose() {
        self.close()
    }
}

extension API {
    struct AirportItem: Model {
        var fs:String                 = ""
        var iata:String               = ""
        var icao:String               = ""
        var name:String               = ""
        var city:String               = ""
        var cityCode:String           = ""
        var countryCode:String        = ""
        var countryName:String        = ""
        var regionName:String         = ""
        var timeZoneRegionName:String = ""
        var localTime:String          = ""
        var utcOffsetHours:Int        = 0
        var latitude:Double           = 0
        var longitude:Double          = 0
        var elevationFeet:Int         = 0
        var classification:Int        = 0
        var active:Bool               = false
        var weatherUrl:URL?           = nil
        var delayIndexUrl:URL?        = nil
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            fs                  <-      map["fs"]
            iata                <-      map["iata"]
            icao                <-      map["icao"]
            name                <-      map["name"]
            city                <-      map["city"]
            cityCode            <-      map["cityCode"]
            countryCode         <-      map["countryCode"]
            countryName         <-      map["countryName"]
            regionName          <-      map["regionName"]
            timeZoneRegionName  <-      map["timeZoneRegionName"]
            localTime           <-      map["localTime"]
            utcOffsetHours      <-      (map["utcOffsetHours"], API.IntTransform())
            latitude            <-      (map["latitude"], API.DoubleTransform())
            longitude           <-      (map["longitude"], API.DoubleTransform())
            elevationFeet       <-      (map["elevationFeet"], API.IntTransform())
            classification      <-      (map["classification"], API.IntTransform())
            active              <-      (map["active"], API.BoolTransform())
            weatherUrl          <-      (map["weatherUrl"], API.URLTransform())
            delayIndexUrl       <-      (map["delayIndexUrl"], API.URLTransform())
        }
        
        private static let dbPath = Bundle.main.bundlePath + "/Airport.db"
        
        static func getAllItems() -> Observable<APIItem<ModelList<AirportItem>>> {
            return Observable.create({ (obs) -> Disposable in
                
                guard let dbQueue = FMDatabaseQueue(path: dbPath) else {
                    
                    obs.onNext(.failed(error: "机场查询失败，数据库发生错误T_T"))
                    
                    return Disposables.create()
                }
                
                obs.onNext(.validating)
                
                dbQueue.inDatabase({ (db) in
                    guard let `db` = db,
                        let resultSet = db.executeQuery("SELECT fs, name, city, utcOffsetHours, countryCode, countryName FROM Airport WHERE active = '1' ORDER BY name ASC",
                                      withParameterDictionary: nil) else {
                        
                        obs.onNext(.failed(error: "机场查询失败，数据库发生错误T_T"))
                        
                        return
                    }
                    
                    var items:[AirportItem] = []
                    
                    while resultSet.next() {
                        if let dict = resultSet.resultDictionary() as? [String:Any],
                            let item = AirportItem(JSON: dict) {
                            items.append(item)
                        }
                    }
                    
                    resultSet.close()
                    
                    obs.onNext(.success(model:ModelList(items)))
                    obs.onCompleted()
                })
                
                return dbQueue
            })
        }
        
        static func getItem(from name:String) -> API.AirportItem? {
            let db = FMDatabase(path: dbPath)!
            
            db.open()
            
            let resultSet = db.executeQuery("SELECT fs, name, city, utcOffsetHours, countryCode, countryName FROM Airport WHERE active = '1' AND instr(name,:name)<>0",
                                            withParameterDictionary: ["name":name])!
            
            while resultSet.next() {
                if let dict = resultSet.resultDictionary() as? [String:Any],
                    let item = AirportItem(JSON: dict) {
                    
                    resultSet.close()
                    
                    return item
                }
            }
            
            resultSet.close()
            
            return nil
        }
        
        static func getItems(by searchText:String) -> Observable<APIItem<ModelList<AirportItem>>> {
            return Observable.create({ (obs) -> Disposable in
                
                guard let dbQueue = FMDatabaseQueue(path: dbPath) else {
                    
                    obs.onNext(.failed(error: "机场查询失败，数据库发生错误T_T"))
                    
                    return Disposables.create()
                }
                
                obs.onNext(.validating)
                //
                dbQueue.inDatabase({ (db) in
                    guard let `db` = db,
                        let resultSet = db.executeQuery("SELECT fs, name, city, utcOffsetHours, countryCode, countryName FROM Airport WHERE active = '1' AND (instr(name,:searchText)<>0 OR instr(city,:searchText)<>0) ORDER BY name ASC",
                                                        withParameterDictionary: ["searchText": searchText]) else {
                                                            
                                                            obs.onNext(.failed(error: "机场查询失败，数据库发生错误T_T"))
                                                            
                                                            return
                    }
                    
                    var items:[AirportItem] = []
                    
                    
                    while resultSet.next() {
                        if let dict = resultSet.resultDictionary() as? [String:Any],
                            let item = AirportItem(JSON: dict) {
                            items.append(item)
                        }
                    }
                    
                    resultSet.close()
                    
                    obs.onNext(.success(model:ModelList(items)))
                    obs.onCompleted()
                })
                
                return dbQueue
            })
        }
        
//        static func getAllItems() -> [AirportItem] {
//            let bundlePath = Bundle.main.bundlePath
//            
//            let assetPath = bundlePath + "/ai.json"
//            
//            let assetData = NSData(contentsOfFile: assetPath)! as Data
//            
//            let json = try! JSONSerialization.jsonObject(with: assetData,
//                                                         options: .allowFragments) as! [String:Any]
//            
//            let array = json["airports"] as! [[String:Any]]
//            
//            return array.map({ (dict) -> AirportItem in
//                return AirportItem(JSON: dict)!
//            })
//        }
//        
//        static func saveDB() {
//            let dbDocPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
//                                                                .userDomainMask,
//                                                                true)[0] + "/aidb.db"
//            
//            
//            let q = FMDatabaseQueue(path: dbDocPath)
//            
//            q?.inDatabase({ (db) in
//                if let `db` = db {
//                    db.executeUpdate("CREATE TABLE IF NOT EXISTS Airport(fs CHARACTER(20), iata CHARACTER(20), icao CHARACTER(20), name VARCHAR(255), city VARCHAR(255), cityCode CHARACTER(20), countryCode CHARACTER(20), countryName VARCHAR(255), regionName VARCHAR(255), timeZoneRegionName VARCHAR(255), localTime VARCHAR(55), utcOffsetHours SMALLINT, latitude DOUBLE, longitude DOUBLE, elevationFeet INT, classification SMALLINT, active BOOLEAN, weatherUrl TEXT, delayIndexUrl TEXT)", withParameterDictionary: [:])
//                    
//                    let items = getAllItems()
//                    
//                    for one in items {
//                        db.executeUpdate("INSERT INTO Airport (fs, iata, icao, name, city, cityCode, countryCode, countryName, regionName, timeZoneRegionName, localTime, utcOffsetHours, latitude, longitude, elevationFeet, classification, active, weatherUrl, delayIndexUrl) VALUES(:fs, :iata, :icao, :name, :city, :cityCode, :countryCode, :countryName, :regionName, :timeZoneRegionName, :localTime, :utcOffsetHours, :latitude, :longitude, :elevationFeet, :classification, :active, :weatherUrl, :delayIndexUrl)",
//                                         withParameterDictionary: ["fs": one.fs,
//                                                                   "iata": one.iata,
//                                                                   "icao": one.icao,
//                                                                   "name": one.name,
//                                                                   "city": one.city,
//                                                                   "cityCode": one.cityCode,
//                                                                   "countryCode": one.countryCode,
//                                                                   "countryName": one.countryName,
//                                                                   "regionName": one.regionName,
//                                                                   "timeZoneRegionName": one.timeZoneRegionName,
//                                                                   "localTime": one.localTime,
//                                                                   "utcOffsetHours": one.utcOffsetHours,
//                                                                   "latitude": one.latitude,
//                                                                   "longitude": one.longitude,
//                                                                   "elevationFeet": one.elevationFeet,
//                                                                   "classification": one.classification,
//                                                                   "active": one.active,
//                                                                   "weatherUrl": one.weatherUrl?.absoluteString ?? "",
//                                                                   "delayIndexUrl": one.delayIndexUrl?.absoluteString ?? ""])
//                    }
//                }
//            })
//            
//            print("dbpath is \(dbDocPath)")
//            
//        }
    }
}
