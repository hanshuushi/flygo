//
//  Chat.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/17.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import EZSwiftExtensions


extension AVIMConversation {
    private static var unreadKey = "AVIMConversation_EX_UNREAD"
    
    var unread:Bool {
        set {
            objc_setAssociatedObject(self, &AVIMConversation.unreadKey, newValue, .OBJC_ASSOCIATION_ASSIGN)
        }
        
        get {
            return objc_getAssociatedObject(self, &AVIMConversation.unreadKey) as? Bool ?? false
        }
    }
    
    private static var nickNameKey = "AVIMConversation_EX_NICKNAME"
    
    var nickName:String {
        set {
            objc_setAssociatedObject(self, &AVIMConversation.nickNameKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            return objc_getAssociatedObject(self, &AVIMConversation.nickNameKey) as? String ?? "未知用户"
        }
    }
    
    private static var userIdKey = "AVIMConversation_EX_USERID"
    
    var userId:String {
        set {
            objc_setAssociatedObject(self, &AVIMConversation.userIdKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            return objc_getAssociatedObject(self, &AVIMConversation.userIdKey) as? String ?? ""
        }
    }
    
    private static var avatarPathKey = "AVIMConversation_EX_AVATARPATH"
    
    var avatarPath:URL? {
        set {
            objc_setAssociatedObject(self, &AVIMConversation.avatarPathKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            return objc_getAssociatedObject(self, &AVIMConversation.avatarPathKey) as? URL
        }
    }
    
    func syncLatest() {
        if let lastMessage = self.lastMessage {
            latestMessage = lastMessage.contentType.text ?? ""
        } else {
            latestMessage = ""
        }
        
        latestMessageDate = self.lastMessageAt
    }
    
    private static var latestMessageKey = "AVIMConversation_EX_LATESTMESSAGE"
    
    var latestMessage:String {
        set {
            objc_setAssociatedObject(self, &AVIMConversation.latestMessageKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            return objc_getAssociatedObject(self, &AVIMConversation.latestMessageKey) as? String ?? ""
        }
    }
    
    private static var latestMessageDate = "AVIMConversation_EX_LATESTMESSAGEDATE"
    
    var latestMessageDate:Date? {
        set {
            objc_setAssociatedObject(self, &AVIMConversation.latestMessageDate, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            return objc_getAssociatedObject(self, &AVIMConversation.latestMessageDate) as? Date
        }
    }
    
    static func conversationFrom(client:AVIMClient, resultSet:FMResultSet) -> AVIMConversation? {
        guard let data = resultSet.data(forColumn: "data"),
            let keyedConversation = NSKeyedUnarchiver.unarchiveObject(with: data) as? AVIMKeyedConversation
            else {
                return nil
        }
        
        let conversation = client.conversation(with: keyedConversation)
        
        let unread = resultSet.bool(forColumn: "unread")
        
        conversation.unread = unread
        
        if let nickName = resultSet.string(forColumn: "nickName") {
            conversation.nickName = nickName
        }
        
        if let userId = resultSet.string(forColumn: "userId") {
            conversation.userId = userId
        }
        
        if let avatarPath = resultSet.string(forColumn: "avatarPath"),
            let url = URL(string:avatarPath) {
            conversation.avatarPath = url
        }
        
        if let lastMessage = resultSet.string(forColumn: "latestMessage") {
            conversation.latestMessage = lastMessage
        }
        
        let lastDate = resultSet.double(forColumn: "latestMessageTime")
        
        if lastDate > 0 {
            conversation.latestMessageDate = Date(timeIntervalSince1970: lastDate)
        }
        
        return conversation
    }
    
    var storeData:Data {
        let keydConversation = self.keyedConversation()
        
        let data = NSKeyedArchiver.archivedData(withRootObject: keydConversation)
        
        return data
    }
}

class ChatDB {
    let userId:String
    
    let client:AVIMClient
    
    var dataBaseQueue:FMDatabaseQueue
    
    var conversationsDictionary:[String:AVIMConversation]
    
    deinit {
        dataBaseQueue.close()
    }
    
    init(userId:String, client:AVIMClient, cacheFinishedSetup response:@escaping ([String:AVIMConversation]) -> Void) {
        self.userId = userId
        
        self.client = client
        
        self.dataBaseQueue = FMDatabaseQueue(path: ChatDB.DBPath(of: userId))
        
        conversationsDictionary = [:]
        
        createTables()
        
        self.dataBaseQueue.inDatabase { (db) in
            if let database = db {
                guard let resultSet =  database
                    .executeQuery("SELECT * FROM Conversations",
                                  withParameterDictionary: nil) else {
                                    return
                }
                
                while resultSet.next() {
                    if let conversation = AVIMConversation.conversationFrom(client: client, resultSet: resultSet),
                        let conversationId = conversation.conversationId {
                        self.conversationsDictionary[conversationId] = conversation
                    }
                }
                
                resultSet.close()
                
                response(self.conversationsDictionary)
            }
        }
    }
    
    func reset() {
        self.dataBaseQueue = FMDatabaseQueue(path: ChatDB.DBPath(of: userId))
        
        createTables()
        
        conversationsDictionary = [:]
    }
    
    private func createTables() {
        self.dataBaseQueue.inDatabase { (db) in
            db?.executeUpdate("CREATE TABLE IF NOT EXISTS Conversations(id VARCHAR(63) PRIMARY KEY, nickName VARCHAR(50), userId VARCHAR(32), avatarPath VARCHAR(32), unread BOOLEAN, latestMessage TEXT, latestMessageTime DOUBLE, data BLOB NOT NULL)", withParameterDictionary: [:])
        }
    }
    
    static func DBPath(of userId:String) -> String {
        let fileManager = FileManager.default
        
        let dbDocPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                            .userDomainMask,
                                                            true)[0] + "/IMDB/"
        
        if !fileManager.fileExists(atPath: dbDocPath) {
            try! fileManager.createDirectory(atPath: dbDocPath,
                                             withIntermediateDirectories: true,
                                             attributes: nil)
        }
        
        return "\(dbDocPath)\(userId).db"
    }
    
    static func isDBExists(of userId:String) -> Bool {
        return FileManager.default.fileExists(atPath: DBPath(of:userId))
    }
    
    var dbexists:Bool {
        return ChatDB.isDBExists(of: userId)
    }
    
    func queryUnreadCount() -> Int {
        return conversationsDictionary.values.enumerated().filter({ $1.unread }).count
    }
    
    func queryConversation(from conversationId:String,
                           client:AVIMClient) -> AVIMConversation? {
        return conversationsDictionary[conversationId]
    }
    
    func queryConversation(of targetId:String,
                           client:AVIMClient) -> AVIMConversation? {
        return conversationsDictionary.values.enumerated().filter({ $1.userId == targetId }).map({ $1 }).first
    }
    
    @discardableResult
    static func executeInsert(conversation:AVIMConversation,
                              from dataBase:FMDatabase) -> Bool {
        return dataBase.executeUpdate("INSERT INTO Conversations (id, nickName, userId, avatarPath, unread, latestMessage, latestMessageTime, data) VALUES(:conversationId, :nickName, :userId, :avatarPath, :unread, :latestMessage, :latestMessageTime, :data)",
                                      withParameterDictionary: ["conversationId":conversation.conversationId!,
                                                                "nickName":conversation.nickName,
                                                                "userId":conversation.userId,
                                                                "unread":conversation.unread,
                                                                "latestMessage":conversation.latestMessage,
                                                                "latestMessageTime":conversation.latestMessageDate?.timeIntervalSince1970 ?? 0,
                                                                "avatarPath":conversation.avatarPath.flatMap({ $0.absoluteString }) ?? "",
                                                                "data":conversation.storeData])
    }
    
    @discardableResult
    static func executeUpdate(conversation:AVIMConversation,
                              from dataBase:FMDatabase) -> Bool {
        return dataBase.executeUpdate("UPDATE Conversations SET id = :conversationId, nickName = :nickName, userId = :userId, unread = :unread, latestMessage = :latestMessage, latestMessageTime = :latestMessageTime, data = :data WHERE id = :conversationId",
                                      withParameterDictionary: ["conversationId":conversation.conversationId!,
                                                                "nickName":conversation.nickName,
                                                                "userId":conversation.userId,
                                                                "unread":conversation.unread,
                                                                "latestMessage":conversation.latestMessage,
                                                                "latestMessageTime":conversation.latestMessageDate?.timeIntervalSince1970 ?? 0,
                                                                "data":conversation.storeData])
    }
    
    func update(conversation:AVIMConversation) {
        guard let conversationId = conversation.conversationId else {
            return
        }
        
        if let _ = conversationsDictionary[conversationId] {
            return
        }
        
        conversationsDictionary[conversationId] = conversation
        
        dataBaseQueue.inDatabase { (db) in
            guard let database = db else {
                return
            }
            
            ChatDB.executeInsert(conversation: conversation, from: database)
        }
    }
    
    func update(nickName:String, avatarPath:URL?, of conversationId:String) {
        conversationsDictionary[conversationId]?.nickName = nickName
        conversationsDictionary[conversationId]?.avatarPath = avatarPath
        
        dataBaseQueue.inDatabase { (db) in
            guard let database = db else {
                return
            }
            
            database.executeUpdate("UPDATE Conversations SET nickName = :nickName, avatarPath = :avatarPath WHERE id = :conversationId", withParameterDictionary: ["nickName":nickName,
                                                                                                                                         "avatarPath":avatarPath.flatMap({ $0.absoluteString }) ?? "",
                                                                                                                                         "conversationId":conversationId])
        }
    }
    
    func update(messageContent:String, messageDate:Date, of conversationId:String) {
        conversationsDictionary[conversationId]?.latestMessage = messageContent
        conversationsDictionary[conversationId]?.latestMessageDate = messageDate
        
        dataBaseQueue.inDatabase { (db) in
            guard let database = db else {
                return
            }
            
            database.executeUpdate("UPDATE Conversations SET latestMessage = :latestMessage, latestMessageTime = :latestMessageTime WHERE id = :conversationId",
                                   withParameterDictionary: ["latestMessage":messageContent,
                                                             "latestMessageTime":messageDate.timeIntervalSince1970,
                                                             "conversationId":conversationId])
        }
    }
    
    func update(unread:Bool, of conversationId:String) {
        conversationsDictionary[conversationId]?.unread = unread
        
        dataBaseQueue.inDatabase { (db) in
            guard let database = db else {
                return
            }
            
            database.executeUpdate("UPDATE Conversations SET unread = :unread WHERE id = :conversationId", withParameterDictionary: ["unread":unread, "conversationId":conversationId])
        }
    }
}
