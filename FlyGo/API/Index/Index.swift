//
//  Index.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/13.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension API {
    
    struct IndexCollection: Model {
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            
        }
        
    }
    
    struct HomeMenu: Model {
        var homeMenuName: String = ""
        
        var homeMenuId: String!
        
        init?(map: Map) {
            homeMenuId <- map["homeMenuId"]
            
            if homeMenuId == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            homeMenuId          <- map["homeMenuId"]
            
            homeMenuName        <- map["homeMenuName"]
        }
        
        static func getMenuList () -> HttpSession {
            return HttpSession.get(urlString: "index/getCommonTags")
        }
    }
    
    struct SearchIndexCollection: Model {
        
        var brandList:[Brand] = []
        
        var productTypeList:[Brand.SubtypeItem] = []
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            brandList               <- map["brandList"]
            productTypeList         <- map["productTypeList"]
        }
        
        static func getBrandList(from keyword:String) -> Observable<APIItem<SearchIndexCollection>> {
            return HttpSession.get(urlString: "product/getProductBrand",
                                   params: ["productName":keyword])
        }
    }
    
    struct Brand : Model {
        var brandId:String!
        
        var chineseName = ""
        
        var englishName = ""
        
        var firstLetter = ""
        
        var pic:URL?
        
        var displayName = ""
        
        struct SubtypeItem: Model {
            var productTypeId: String!
            
            var productTypeName: String    = ""
            
            var fatherId: String?
            
            var layers: Int                = 0
            
            init?(map: Map) {
                productTypeId            <- map["productTypeId"]
                
                if productTypeId == nil { return nil }
                
                layers                   <- map["layers"]
                
                if layers <= 0 || layers > 3 { return nil }
            }
            
            mutating func mapping(map: Map) {
                productTypeId            <- map["productTypeId"]
                productTypeName          <- map["productTypeName"]
                layers                   <- map["layers"]
                fatherId                 <- map["fatherId"]
            }
        }
        
        var typeList:[SubtypeItem] = []
        
        init?(map: Map) {
            brandId                      <- map["brandId"]
            
            if brandId == nil { return nil }
        }
        
        mutating func mapping(map: Map) {
            brandId                      <- map["brandId"]
            chineseName                  <- map["chineseName"]
            englishName                  <- map["englishName"]
            firstLetter                  <- map["firstLetter"]
            pic                          <- (map["pic"], API.PicTransform())
            typeList                     <- map["typeList"]
            displayName                  <- map["displayName"]
        }
        
        static func getBrandList() -> HttpSession {
            return HttpSession.get(urlString: "product/findBrand")
        }
        
        static func getBrandList(typeId:String) -> Observable<APIItem<ModelList<Brand>>> {
            return HttpSession.get(urlString: "product/getBrandType",
                                   params:["productTypeId":typeId])
        }
        
        static func getHots() -> Observable<APIItem<ModelList<Brand>>> {
            return HttpSession.get(urlString: "product/getHotBrand")
        }
        
        
        
    }
    
    struct ProductType : Model  {
        var productTypeId: String!
        
        var state: String              = ""
        
        var homeMenuId: String         = ""
        
        var layers: Int                = 0
        
        var tax: String                = ""
        
        var ifHot: String              = ""
        
        var modifyMan: String          = ""
        
        var useId: String              = ""
        
        var pic: URL?
        
        var registerMan: String        = ""
        
        var remark: String             = ""
        
        var subTypeList: [ProductType] = []
        
        var fatherId: String?
        
        var registerTime: String       = ""
        
        var modifyTime: String         = ""
        
        var productTypeName: String    = ""
        
        var brandList: [Brand]         = []
        
        init?(map: Map) {
            productTypeId            <- map["productTypeId"]
            layers                   <- map["layers"]
            
            if productTypeId == nil { return nil }
            
            if layers <= 0 || layers > 3 { return nil }
        }
        
        mutating func mapping(map: Map) {
            productTypeId            <- map["productTypeId"]
            
            state                    <- map["state"]
            homeMenuId               <- map["homeMenuId"]
            layers                   <- map["layers"]
            
            tax                      <- map["tax"]
            ifHot                    <- map["ifHot"]
            modifyMan                <- map["modifyMan"]
            useId                    <- map["useId"]
            pic                      <- (map["pic"], API.PicTransform())
            registerMan              <- map["registerMan"]
            remark                   <- map["remark"]
            subTypeList              <- map["subTypeList"]
            fatherId                 <- map["fatherId"]
            registerTime             <- map["registerTime"]
            modifyTime               <- map["modifyTime"]
            productTypeName          <- map["productTypeName"]
            brandList                <- map["brandList"]
        }
        
        static func getTypeList (brandId:String) -> HttpSession {
            return HttpSession.get(urlString: "product/getBrandType",
                                   params:["brandId":brandId])
        }
        
        static func getTypeList() -> HttpSession {
            return HttpSession.get(urlString: "product/getTypes",
                                   params: ["fatherId":0,"layers":"1,2,3"])
            
        }
    }
    
}
