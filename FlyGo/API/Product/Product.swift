//
//  product.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/5.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension Float {
    var priceString:String {
        return String(format:"%.2f", self)
    }
}

extension Currency {
    var priceString:String {
        return self.value.priceString
    }
}

struct Currency {
    var unit:String                              = "$"
    
    var value:Float                              = 0.0
    
    var description:String {
        return "\(unit)\(value.priceString)"
    }
    
    init() {
        
    }
    
    init(unit:String, value:Float) {
        self.unit = unit
        
        self.value = value
    }
    
    static func unit(with string:String) -> String {
        let unitDefined = ["$","￥","円","₩","€","￡","£"]
        
        for one in unitDefined {
            if string.contains(one) {
                return one
            }
        }
        
        return "$"
    }
    
    init?(string:String) {
        let unitDefined = ["$","￥","円","₩","€","￡","£"]
    
        var currentUnit:String? = nil
        
        for one in unitDefined {
            if string.contains(one) {
                currentUnit = one
                
                break
            }
        }
        
        guard let _currentUnit = currentUnit else {
            return nil
        }
        
        unit = _currentUnit
        
        value = string.floatFormat().toFloat() ?? 0
    }
}

extension API {
    
    struct ProductBrandIndex : Model {
        var letter:String                        = ""
        
        var brandList:[ProductBrand]             = []
        
        init?(map: Map) {
            letter                      <- map["letter"]
            
            brandList                   <- map["subBrandList"]
        }
        
        mutating func mapping(map: Map) {
            letter                      <- map["letter"]
            brandList                   <- map["brandList"]
        }
    }
    
    typealias ProductBrand                       = Brand
    
    struct Product : Model {
        var productRecId:String                  = ""
        
        var productId:String!
        
        var productColorId:String                = ""
        
        var productSizeId:String                 = ""
        
        var productPics:URL?
        
        var productPicsArray:[URL]               = []
        
        var productName:String                   = ""
        
        var currencyId:String                    = ""
        
        var sellPrice:Currency                   = Currency(unit: "$", value: 0)
        
        var miniPrice:Currency                   = Currency(unit: "$", value: 0)
        
        var miniDomestic:Currency                = Currency(unit: "￥", value: 0)
        
        var cnyPrice:Currency                    = Currency(unit: "￥", value: 0)
        
        var productNumber:String                 = ""
        
        var ifHot:String                         = ""
        
        var linkAddress:String                   = ""
        
        var state:String                         = ""
        
        var belongStoreId:String                 = ""
        
        var currencySymbols:String?
        
        init?(map: Map) {
            productId            <- map["productId"]
            
            if productId == nil { return nil }
        }
        
        mutating func mapping(map: Map) {
            productRecId         <- map["productRecId"]
            productColorId       <- map["productColorId"]
            productSizeId        <- map["productSizeId"]
            productPics          <- (map["productPics"], API.PicTransform())
            productPicsArray     <- (map["productPics"], API.PicsTransform())
            productName          <- map["productName"]
            currencyId           <- map["currencyId"]
            sellPrice            <- (map["sellPrice"], API.CurrencyTransform(defaultUnit:"$"))
            miniPrice            <- (map["miniPrice"], API.CurrencyTransform(defaultUnit:"$"))
            miniDomestic         <- (map["miniDomestic"], API.CurrencyTransform(defaultUnit:"￥"))
            cnyPrice             <- (map["cnyPrice"], API.CurrencyTransform(defaultUnit:"￥"))
            ifHot                <- map["ifHot"]
            linkAddress          <- map["linkAddress"]
            state                <- map["state"]
            belongStoreId        <- map["belongStoreId"]
            currencySymbols      <- map["currencySymbols"]
            
            if let _currencySymbols = currencySymbols {
                sellPrice.unit = _currencySymbols
                
                miniPrice.unit = _currencySymbols
            }
        }
    }
    
    struct ProductDetail: Model {
        
        struct BestComment: Model {
            var comment:Comment?
            
            var buyIndex:Float = 0.0
            
            var commentsNumber:Int = 0
            
            var productId:String!
            
            mutating func mapping(map: Map) {
                comment             <-  map["comment"]
                productId           <-  map["productId"]
                buyIndex            <-  (map["buyIndex"], API.FloatTransform())
                commentsNumber      <-  (map["commentsNumber"], API.IntTransform())
            }
            
            init?(map: Map) {
                comment             <-  map["comment"]
                productId           <-  map["productId"]
                
                if productId == nil {
                    return nil
                }
            }
        }
        
        struct Pic: Model {
            var productRecid    = ""
            var productRecpicid = ""
            var pic:URL?
            
            init?(map: Map) {
                
            }
            
            mutating func mapping(map: Map) {
                productRecid                <- map["product_rec_id"]
                productRecpicid             <- map["product_rec_pic_id"]
                pic                         <- (map["pic"], API.PicTransform())
            }
        }
        
        struct Detail: Model {
            var displayName     = ""
            var productRecid    = ""
            var productName     = ""
            var brandId         = ""
            var englishName     = ""
            var chineseName     = ""
            var productTypeName = ""
            
            init?(map: Map) {
                
            }
            
            mutating func mapping(map: Map) {
                displayName             <- map["display_name"]
                productRecid            <- map["product_rec_id"]
                productName             <- map["product_name"]
                brandId                 <- map["brand_id"]
                englishName             <- map["english_name"]
                chineseName             <- map["chinese_name"]
                productTypeName         <- map["product_type_name"]
            }
        }
        
        struct Standard: Model {
            var productRecid = ""
            var productId = ""
            var standard = 0
            var productRecStandard = ""
            var productName = ""
            var linkAddress: URL?
            
            init?(map: Map) {
                
            }
            
            mutating func mapping(map: Map) {
                productRecid                <- map["product_rec_id"]
                productId                   <- map["product_id"]
                standard                    <- (map["standard"], API.IntTransform())
                productRecStandard          <- map["product_rec_standard"]
                productName                 <- map["product_name"]
                linkAddress                 <- (map["link_address"], API.URLTransform())
            }
        }
        
        struct Stores: Model {
            var rmb:Currency = Currency(unit: "￥", value: 0)
            var sellPrice:Currency = Currency(unit: "$", value: 0)
            var currencySymbols:String?
            var belongStoreId = ""
            var state = 1
            var freeStoreName = ""
            var miniDomestic = Currency(unit: "￥", value: 0)
            var miniPrice = Currency(unit: "$", value: 0)
            
            init?(map: Map) {
            }
            
            mutating func mapping(map: Map) {
                rmb                         <- (map["cny_price"], API.CurrencyTransform(defaultUnit:"￥"))
                sellPrice                   <- (map["sell_price"], API.CurrencyTransform(defaultUnit:"$"))
                currencySymbols             <- map["currency_symbols"]
                belongStoreId               <- map["belong_store_id"]
                state                       <- (map["state"], API.IntTransform())
                freeStoreName               <- map["free_store_name"]
                miniDomestic                <- (map["mini_domestic"], API.CurrencyTransform(defaultUnit:"￥"))
                miniPrice                   <- (map["mini_price"], API.CurrencyTransform(defaultUnit:"$"))
            }
        }
        
        struct Paras: Model {
            var content:String!
            
            var productParaName:String!
            
            var productParaId:String = ""
            
            init?(map: Map) {
                content                     <-  (map["content"], API.StringTransform())
                productParaName             <-  (map["product_para_name"], API.StringTransform())
                
                if content == nil || content.length <= 0 {
                    return nil
                }
                
                if productParaName == nil || productParaName.length <= 0 {
                    return nil
                }
            }
            
            mutating func mapping(map: Map) {
                content                     <-  (map["content"], API.StringTransform())
                productParaName             <-  (map["product_para_name"], API.StringTransform())
                productParaId               <-  (map["product_para_id"], API.StringTransform())
            }
        }
        
        struct ProductPic: Model {
            var picSeq:Int = 0
            
            var pic:URL?
            
            init?(map: Map) {
                
            }
            
            mutating func mapping(map: Map) {
                picSeq                          <-  (map["pic_seq"], API.IntTransform())
                pic                             <-  (map["pic"], API.PicTransform())
            }
        }
        
        var pics:[Pic]                  = []
        var detail:[Detail]             = []
        var serviceRatio:Float          = 0.15
        var standard:[Standard]         = []
        var stores:[Stores]             = []
        var paras:[Paras]               = []
        var productPics:[ProductPic]    = []
        var bestComment:BestComment?    = nil
        var postage:Float               = 0
        var cnyPrice:Float              = 0
        var sellPrice:Float             = 0
        var productStock:Int            = 0
        var estimatedArrivalTime:String = ""
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            pics                        <-      map["pics"]
            detail                      <-      map["detail"]
            serviceRatio                <-      map["serviceRatio"]
            standard                    <-      map["standard"]
            stores                      <-      map["stores"]
            paras                       <-      map["paras"]
            productPics                 <-      map["productPics"]
            bestComment                 <-      map["bestComment"]
            postage                     <-      (map["postage"], API.FloatTransform())
            cnyPrice                    <-      (map["cnyPrice"], API.FloatTransform())
            sellPrice                   <-      (map["sellPrice"], API.FloatTransform())
            productStock                <-      (map["productStock"], API.IntTransform())
            estimatedArrivalTime        <-      (map["estimatedArrivalTime"], API.StringTransform())
        }
        
        static func detail(recId:String) -> Observable<APIItem<ProductDetail>> {
            return HttpSession.get(urlString: "product/productDetail",
                                   params:["productRecId": recId])
        }
    }
    
    struct ProductCollection: Model, DataSetPageCollection {
        
        var list:[Product] {
            return self.productList
        }
        
        var otherInfo:Any? {
            return self.brand
        }
        
        var productList:[Product]                = []
        
        var typeList:[ProductType]               = []
        
        var brand:ProductBrand?                  = nil
        
        var hasMore:Bool                         = false
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            productList             <- map["pList"]
            typeList                <- map["proTypeList"]
            brand                   <- map["brand"]
            hasMore                 <- (map["hasMore"], API.BoolTransform())
        }
        
        static func search (brandId:String?      = nil,
                            pductTypeId:String?  = nil,
                            level:Int?           = nil,
                            keyWord:String?      = nil,
                            sort:String          = "2",
                            useProType:Bool      = false,
                            page:Int             = 1,
                            pageSize:Int         = 10) -> Observable<APIItem<ProductCollection>> {
            
            var params                           = [String:Any]()
            
            if let _brandId                      = brandId {
                params["brandId"]                = _brandId
            }
            
            if let _pductTypeId                  = pductTypeId {
                params["productTypeId"]          = _pductTypeId
            }
            
            if let _level                        = level {
                params["level"]                  = _level
            }
            
            if let _keyWord                      = keyWord {
                params["keyWord"]                = _keyWord
            }
            
            params["sort"]                       = sort
            params["useProType"]                 = useProType
            params["page"]                       = page
            params["pageSize"]                   = pageSize
            
            return HttpSession
                .get(urlString: "product/search",
                     params: params)
        }
    }
}
