//
//  Message.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/29.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

extension API {
    
    enum MessageType: Int {
        case systom = 1
        case order = 2
        case flygo = 3
    }
    
    fileprivate struct TypeTransform : TransformType {
        func transformFromJSON(_ value: Any?) -> MessageType? {
            
            var intValue:Int?
            
            if let string = value as? String {
                intValue = Int(string)
            } else if let float = value as? Float {
                intValue = Int(float)
            } else if let int = value as? Int {
                intValue = int
            }
            
            return intValue.flatMap{MessageType(rawValue: $0)}
        }
        
        func transformToJSON(_ value: MessageType?) -> Int? {
            return value.map({ $0.rawValue })
        }
    }
    
    struct UnreadMessage: Model {
        var messageType:MessageType!
        var messageCount:Int        = 0
        
        init?(map: Map) {
            messageType     <-  (map["messageType"], API.TypeTransform())
            
            if messageType == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            messageType     <-  (map["messageType"], API.TypeTransform())
            messageCount    <-  (map["newMessageCount"], API.IntTransform())
        }
        
        static func getNewMessage() -> Observable<APIItem<ModelList<UnreadMessage>>> {
            NSLog("###################看看有没有新短消息###################")
            
            return HttpSession.get(urlString: "message/getNewMessage",
                                   params: ["customerId" : UserManager.shareInstance.currentId])
        }
    }
    
    struct Message: Model {
        var receiveMessageId:String?
        var userType = 0
        var messageId:String?
        var isRead = false		//是否已读
        var messageType:MessageType = .systom //消息类型	1 一般通知    2 用户订单通知    3飞哥订单通知
        var content = "" //内容
        var sendTime:Date?	//			发送时间
        var vendorOrderInfoId = ""
        var orderformId = ""
        var customerId = ""
        var orderformSubId = ""
        var vendorFlightInfoId = ""
        var productRecId = ""
        var productName	= ""			//商品名称
        var orderNumber	= 0 //			订单商品数量
        var flightNo = "" //	航班号
        var takeoffTime:Date? //			起飞时间
        var fallTime:Date? //			降落时间
        var productRecStandard = "" //	规格
        var pic:URL?    //			商品图片
        var sumPrice:Float? //				订单金额
        var orderSn:String = "" //		订单号
        
        init?(map: Map) {
            receiveMessageId                        <-  map["receiveMessageId"]
            messageId                               <-  map["messageId"]
            
            if receiveMessageId == nil || messageId == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            receiveMessageId                        <-  map["receiveMessageId"]
            userType                                <-  (map["userType"], API.IntTransform())
            messageId                               <-  map["messageId"]
            isRead                                  <-  (map["isRead"], API.BoolTransform())
            messageType                             <-  (map["messageType"], API.TypeTransform())
            content                                 <-  (map["content"], API.StringTransform())
            sendTime                                <-  (map["sendTime"], API.DateTransform())
            vendorOrderInfoId                       <-  (map["vendorOrderInfoId"], API.StringTransform())
            orderformId                             <-  (map["orderformId"], API.StringTransform())
            customerId                              <-  (map["customerId"], API.StringTransform())
            orderformSubId                          <-  (map["orderformSubId"], API.StringTransform())
            vendorFlightInfoId                      <-  (map["vendorFlightInfoId"], API.StringTransform())
            productRecId                            <-  (map["productRecId"], API.StringTransform())
            productName                             <-  (map["productName"], API.StringTransform())
            orderNumber                             <-  (map["orderNumber"], API.IntTransform())
            flightNo                                <-  (map["flightNo"], API.StringTransform())
            takeoffTime                             <-  (map["takeoffTime"], API.DateTransform())
            fallTime                                <-  (map["fallTime"], API.DateTransform())
            productRecStandard                      <-  (map["productRecStandard"], API.StringTransform())
            pic                                     <-  (map["pic"], API.PicTransform())
            sumPrice                                <-  (map["sumPrice"], API.FloatTransform())
            orderSn                                 <-  (map["orderSn"], API.StringTransform())
        }
        
        
    }
    
    struct MessageCollection: Model, DataSetPageCollection {
        
        var list:[Message] {
            return self.receiveMessageList
        }
        
        var otherInfo: Any? {
            return nil
        }
        
        var hasMore:Bool = false
        
        var receiveMessageList:[Message] = []
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            hasMore                <-  (map["hasMore"], API.BoolTransform())
            receiveMessageList     <-  map["receiveMessageList"]
        }
        
        static func getList(
            from type:MessageType,
            page:Int = 1,
            pageSize:Int = 10) -> Observable<APIItem<MessageCollection>> {
            
            var params = [String:Any]()
            
            params["messageType"] = type.rawValue
            params["pageNo"] = page
            params["pageSize"] = pageSize
            params["customerId"] = UserManager.shareInstance.currentId
            
            return HttpSession.post(urlString: "message/findMessageList",
                                    params: params)
            
        }
    }
}
