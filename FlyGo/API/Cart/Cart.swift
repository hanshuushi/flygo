//
//  Cart.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/16.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension API {
    struct CartCollection: Model {
        var cartList:[Cart] = []
        
        var rate:Float = 0
        
        init?(map: Map) {
            
        }
        
        init () {
            
        }
        
        mutating func mapping(map: Map) {
            rate        <-  (map["rate"], API.FloatTransform())
            cartList    <-  map["cartList"]
        }
        
        static func findCarts() -> Observable<APIItem<CartCollection>> {
            if !UserManager.shareInstance.isLogined() {
                let emptyCollection = CartCollection()
                
                return Observable.just(APIItem.success(model: emptyCollection))
            }
            
            return HttpSession.get(urlString: "cart/findCart",
                                   params: ["customerId": UserManager.shareInstance.currentId])
        }
    }
    
    struct CartPrice: Model {
        var serviceCharge: Float = 0
        var shippingCharge: Float = 0
        var sumPrice: Float = 0
        var serviceRatio: Float = 0
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            serviceCharge               <-      (map["serviceCharge"], API.FloatTransform())
            shippingCharge              <-      (map["shippingCharge"], API.FloatTransform())
            sumPrice                    <-      (map["sumPrice"], API.FloatTransform())
            serviceRatio                <-      (map["serviceRatio"], API.FloatTransform())
        }
        
        typealias SubmitCartItem = (cartSubId:String,
            belongStoreId:String,
            productRecId:String,
            selectedBelongStore:String,
            productName:String,
            num:Int,
            sellPrice:Float)
        
        static func getPrice(of items:[SubmitCartItem]) -> HttpSession {
            var dict = API.defaultDict
            
            dict["data"] = items.map({ (one) -> [String:Any] in
                var currentDictionary = [String:Any]()
                
                currentDictionary["cartSubId"]           = one.cartSubId
                currentDictionary["belongStoreId"]       = one.belongStoreId
                currentDictionary["productRecId"]        = one.productRecId
                currentDictionary["selectedBelongStore"] = one.selectedBelongStore
                currentDictionary["productName"]         = one.productName
                currentDictionary["num"]                 = one.num
                currentDictionary["cnyPrice"]           = one.sellPrice
                
                return currentDictionary
            })
            
            return HttpSession.post(urlString: "cart/updateCart?customerId=\(UserManager.shareInstance.currentId)",
                                    isJson: true,
                                    params: dict)
        }
        
        static func getPrice(of totalPrice:Float) -> HttpSession {
            return HttpSession.post(urlString: "cart/getCostInfo?totalPrice=\(totalPrice)",
                                    params: nil)
        }
    }
    
    struct Cart: Model {
        enum Status: Int {
            case normal        = 1
            case outStock      = 2
            case shelved       = 3
        }
        
        var cartId:String!        =  nil
        var customerId            = ""
        var belongStoreId         = ""
        var selectedBelongStore   = ""
        var cartSubId             = ""
        var productId             = ""
        var productRecId          = ""
        var productName           = ""
        var productRecStandard    = ""
        var pic:URL?
        var orderNumber           = 0
        var sellPrice:Currency    = Currency(unit: "$", value: 0)
        var miniPrice:Currency    = Currency(unit: "$", value: 0)
        var miniDomestic:Currency = Currency(unit: "￥", value: 0)
        var cnyPrice:Currency     = Currency(unit: "￥", value: 0)
        var state:Status          = .shelved
        var currencySymbols:String?
        var serviceCharge: Float  = 0
        var shippingCharge: Float = 0
        var sumPrice: Float       = 0
        var serviceRatio: Float   = 0
        
        static func addToCart (belongStoreId:String,
                               productRecId:String,
                               productName:String,
                               orderNumber:Int) -> Observable<PostItem> {
            return HttpSession.post(urlString: "cart/saveCart",
                                    params: ["customerId"       : UserManager.shareInstance.currentId,
                                             "productName"      : productName,
                                             "belongStoreId"    : belongStoreId,
                                             "productRecId"     : productRecId,
                                             "orderNumber"      : orderNumber])
        }
        
        init?(map: Map) {
            cartId      <- map["cartId"]
            
            if cartId == nil {
                return nil
            }
        }
        
        struct StatusTransform: TransformType {
            func transformFromJSON(_ value: Any?) -> Status? {
                return value
                    .flatMap{String(describing: $0)}
                    .flatMap{Int($0)}
                    .flatMap{Status(rawValue: $0)}
            }
            
            func transformToJSON(_ value: Status?) -> String? {
                return value.flatMap({ "\($0.rawValue)" })
            }
        }
        
        mutating func mapping(map: Map) {
            cartId              <- map["cartId"]
            customerId          <- map["customerId"]
            belongStoreId       <- map["belongStoreId"]
            selectedBelongStore <- map["selectedBelongStore"]
            cartSubId           <- map["cartSubId"]
            productId           <- map["productId"]
            productRecId        <- map["productRecId"]
            productName         <- map["productName"]
            productRecStandard  <- map["productRecStandard"]
            pic                 <- (map["pic"], API.PicTransform())
            orderNumber         <- (map["orderNumber"], API.IntTransform())
            sellPrice           <- (map["sellPrice"], API.CurrencyTransform(defaultUnit:"$"))
            miniPrice           <- (map["miniPrice"], API.CurrencyTransform(defaultUnit:"$"))
            miniDomestic        <- (map["miniDomestic"], API.CurrencyTransform(defaultUnit:"￥"))
            cnyPrice            <- (map["cnyPrice"], API.CurrencyTransform(defaultUnit:"￥"))
            state               <- (map["state"], StatusTransform())
            currencySymbols     <- map["currencySymbols"]
            
            if let _currencySymbols = currencySymbols {
                sellPrice.unit = _currencySymbols
                
                miniPrice.unit = _currencySymbols
            }
            
            serviceCharge       <- (map["serviceCharge"], API.FloatTransform())
            shippingCharge      <- (map["shippingCharge"], API.FloatTransform())
            sumPrice            <- (map["sumPrice"], API.FloatTransform())
            serviceRatio        <- (map["serviceRatio"], API.FloatTransform())
        }
        
        static func delete(cartId:String) -> HttpSession {
            return HttpSession.post(urlString: "cart/deleteCart",
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "cartSubId":cartId])
        }
        
        static func delete(cardIds:[String]) -> HttpSession {
            return HttpSession.post(urlString: "cart/batchDeleteCart?customerId=\(UserManager.shareInstance.currentId)",
                                    isJson: true,
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "cartSubIds":cardIds])
        }
    }
}
    
