//
//  Coupon.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/25.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import RxCocoa

extension API {
    
    enum CouponType : Int {
        case discount = 1
        case cash = 2
    }
    
    fileprivate struct CouponTypeTransform : TransformType {
        func transformFromJSON(_ value: Any?) -> CouponType? {
            
            var intValue:Int?
            
            if let string = value as? String {
                intValue = Int(string)
            } else if let float = value as? Float {
                intValue = Int(float)
            } else if let int = value as? Int {
                intValue = int
            }
            
            return intValue.flatMap{CouponType(rawValue: $0)}
        }
        
        func transformToJSON(_ value: CouponType?) -> Int? {
            return value.map({ $0.rawValue })
        }
    }
    
    fileprivate struct PercentTransform : TransformType {
        func transformFromJSON(_ value: Any?) -> Float? {
            
            if let float = value as? Float {
                return float / 100.0
            } else if let int = value as? Int {
                return Float(int) / 100.0
            } else if let string = value as? String {
                return string.replacingOccurrences(of: "%", with: "").toFloat()
            }
            
            return nil
        }
        
        func transformToJSON(_ value: Float?) -> String? {
            return value.map({ "\($0 * 100)%" })
        }
    }
    
    enum CouponRequestState: Int {
        case all = 0
        case valid = 1
        case invalid = 2
    }
    
    fileprivate struct CouponUserTypeTransform : TransformType {
        func transformFromJSON(_ value: Any?) -> CouponUserType? {
            
            var intValue:Int?
            
            if let string = value as? String {
                intValue = Int(string)
            } else if let float = value as? Float {
                intValue = Int(float)
            } else if let int = value as? Int {
                intValue = int
            }
            
            return intValue.flatMap{CouponUserType(rawValue: $0)}
        }
        
        func transformToJSON(_ value: CouponUserType?) -> Int? {
            return value.map({ $0.rawValue })
        }
    }
    
    enum CouponUserType: Int {
        case all = 1
        case exclusive = 2
        case special = 3
        case vip = 4
    }
    
    struct CoupeCollection: Model {
        var validList:[Coupon]      =   []
        var invalidList:[Coupon]    =   []
        var serverTime:Date!
        
        init?(map: Map) {
            serverTime              <-      (map["serverTime"], API.DateTransform())
            
            if serverTime == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            validList               <-      map["validList"]
            invalidList             <-      map["invalidList"]
            serverTime              <-      (map["serverTime"], API.DateTransform())
        }
    }
    
    struct ProductsCoupes: Model {
        var productCoupons:[[Coupon]] = []
        
        var globalCoupons:[Coupon] = []
        
        var serverTime:Date!
        
        init?(map: Map) {
            serverTime              <-      (map["serverTime"], API.DateTransform())
            
            if serverTime == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            productCoupons      <-      map["productCoupons"]
            globalCoupons       <-      map["globalCoupons"]
        }
        
        static func getCoupons(from productRecIds:[String]) -> Observable<APIItem<ProductsCoupes>> {
            var dict = API.defaultDict
            
            dict["productIds"] = productRecIds
            
            return HttpSession.post(urlString: "coupon/getCouponByProductIds?customerId=\(UserManager.shareInstance.currentId)",
                isJson: true,
                params: dict)
        }
    }
    
    struct Coupon: Model {
        var couponId:String!       //优惠券ID  (券1)
        var couponDetailsId:String = ""
        var couponName:String      = ""  //标题名称
        var couponDesc:String      = ""  //内容
        var couponCode:String      = ""   //优惠券代码
        var brandPic:URL?   //优惠券品牌宣传图
        var couponNo:String        = ""  //优惠券编号
        var recevieType:Int        = 1 //领取方式:1 自动派发  2 手动领取
        var useType:CouponUserType = .special  //使用类型:1 通用型  2 专用型  3 特别券  4 VIP券
        var couponType:CouponType  = .cash  //优惠券类型: 1 折扣券, 2 现金券
        var discountAmt:Float      = 0.0 // 抵扣金额 :类型为现金券时有效
        var discountPercent:Float  = 1.0//抵扣比例:类型为折扣券时有效
        var useLimitAmt:Float      = 0.0//满多少才能用
        var startTime:Date! //优惠券生效时间
        var endTime:Date! //优惠券结束时间
        
        init?(map: Map) {
            couponId                <-      (map["couponId"], API.StringTransform())
            
            guard let `couponId` = self.couponId, couponId.length > 0 else {
                return nil
            }
            
            startTime               <-      (map["startTime"], API.DateTransform())
            endTime                 <-      (map["endTime"], API.DateTransform())
            
            if startTime == nil || endTime == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            couponId                <-      (map["couponId"], API.StringTransform())
            couponDetailsId         <-      (map["couponDetailsId"], API.StringTransform())
            couponName              <-      (map["couponName"], API.StringTransform())
            couponDesc              <-      (map["couponDesc"], API.StringTransform())
            couponCode              <-      (map["couponCode"], API.StringTransform())
            brandPic                <-      (map["brandPic"], API.PicTransform())
            couponNo                <-      (map["couponNo"], API.StringTransform())
            recevieType             <-      (map["recevieType"], API.IntTransform())
            useType                 <-      (map["useType"], API.CouponUserTypeTransform())
            couponType              <-      (map["couponType"], API.CouponTypeTransform())
            discountAmt             <-      (map["discountAmt"], API.FloatTransform())
            discountPercent         <-      (map["discountPercent"], API.FloatTransform())
            useLimitAmt             <-      (map["useLimitAmt"], API.FloatTransform())
            startTime               <-      (map["startTime"], API.DateTransform())
            endTime                 <-      (map["endTime"], API.DateTransform())
        }
        
        static func getCoupon(of state:CouponRequestState = .all) -> Observable<APIItem<CoupeCollection>> {
            var dict = API.defaultDict
            
            dict["state"] = state.rawValue
            
            return HttpSession.post(urlString: "coupon/getCouponList?customerId=\(UserManager.shareInstance.currentId)",
                                    isJson: true,
                                    params: dict)
        }
        
        static func shareCoupon() {
            let dict = API.defaultDict
            
            HttpSession
                .post(urlString: "coupon/generateShareCoupon",
                      isJson: false,
                      params: dict)
                .responseNoModel({
                    print("分享成功")
                }) { (error) in
                    print("分享失败 \(error)")
            }
        }
    }
    
    
}
