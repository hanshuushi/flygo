//
//  Other.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/1/14.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper


extension API {
    
    struct VersionInfo: Model {
        var appVersion:Version!
        
        init?(map: Map) {
            appVersion <- map["appVersion"]
            
            if appVersion == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            appVersion <- map["appVersion"]
        }
    }
    
    struct Version: Model {
        var versionNumber:String!
        
        init?(map: Map) {
            versionNumber <- map["versionNumber"]
            
            if versionNumber == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            versionNumber <- map["versionNumber"]
            
        }
        
        static func getVersionInfo() -> HttpSession {
            return HttpSession.get(urlString: "common/getNewVersion",
                                   params: ["appType":"2"])
        }
    }
}
