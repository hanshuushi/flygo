//
//  User.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension API {
    
    struct AccessToken: Model {
        var token:String!
        
        init?(map: Map) {
            token       <-  map["token"]
            
            guard let `token` = token, token.length > 0 else {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            token       <-  map["token"]
        }
        
        static func getAccessToken() -> HttpSession {
            return HttpSession.post(urlString: "member/getAccessToken",
                                    isJson: true,
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "token": UserManager.shareInstance.currentToken])
        }
    }
    
    struct UserData: Model {
        var user:User!
        
        init?(map: Map) {
            user        <- map["customerInfo"]
            
            if user == nil { return nil }
        }
        
        mutating func mapping(map: Map) {
            user        <- map["customerInfo"]
        }
    }
    
    struct Avatar: Model {
        var userAvatar:URL?
        
        var nickName:String = ""
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            userAvatar  <-  (map["userAvatar"], API.PicTransform())
            nickName    <-  (map["nickName"], API.StringTransform())
        }
        
        static func getAvatar(from userId:String) -> HttpSession {
            return HttpSession.get(urlString: "member/getUserAvatar",
                                   params: ["customerId":userId])
        }
    }
    
    struct UserInfo: Model {
        var user:User!
        
        var infoFlag:Int = 0
        
        var token:String!
        
        init(userData:UserData, userToken:String) {
            user = userData.user
            
            token = userToken
        }
        
        init?(map: Map) {
            token       <- (map["token"], API.StringTransform())
            infoFlag    <- (map["infoFlag"], API.IntTransform())
            user        <- map["customerInfo"]
            
            if token == nil || user == nil { return nil }
        }
        
        mutating func mapping(map: Map) {
            token       <- (map["token"], API.StringTransform())
            user        <- map["customerInfo"]
            infoFlag    <- (map["infoFlag"], API.IntTransform())
        }
        
        static func login(areaCode:Int, mobile:String, passWord:String, imei:String, loginWay:Int = 0) -> HttpSession {
            var params = ["areaCode":areaCode,
                          "mobile":mobile,
                          "imei":imei,
                          "appType":2,
                          "passWord":passWord,
                          "loginWay": loginWay] as [String : Any]
            
            if let installationId = RemoteManager.shareInstance.installationId {
                params["installationId"] = installationId
            }
            
            return HttpSession.post(urlString: "member/customerLogin",
                                    params: params)
        }
        
        static func checkLogin(areaCode:Int, mobile:String, imei:String) -> HttpSession {
            return HttpSession.post(urlString: "member/checkLogin",
                                    params: ["areaCode":areaCode,
                                             "mobile":mobile,
                                             "imei":imei,
                                             "isCust":1,
                                             "isToken":1])
        }
        
        static func update(nickName:String, userId:String) -> HttpSession {
            return HttpSession.post(urlString: "member/updateUserInfo?customerId=\(UserManager.shareInstance.currentId)",
                                    params: ["id":userId,
                                             "nickname":nickName])
        }
        
        static func feedBack(addressId:String, content:String) -> Observable<PostItem> {
            return HttpSession.post(urlString: "common/saveReply",
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "content":content,
                                             "customerAddressId":addressId])
        }
        
        static func getUserInfo() -> HttpSession {
            return HttpSession.post(urlString: "member/getCustomerInfo",
                params: ["customerId":UserManager.shareInstance.currentId])
        }
        
        static func validateCode(code:String) -> HttpSession {
            return HttpSession.post(urlString: "member/validateCode", params: ["customerId":UserManager.shareInstance.currentId,
                                                                               "flygoInvitationCode":code])
        }
        
        static func settingPassword(areaCode:String, mobile:String, passWord:String) -> HttpSession {
            return HttpSession.post(urlString: "member/ressetPwssWord",
                                    params: ["areaCode":areaCode,
                                             "mobile":mobile,
                                             "passWord":passWord,
                                             "confirmPwd":passWord])
        }
        
        static func update(avatarURL:String) -> HttpSession  {
            return HttpSession.post(urlString: "member/updateCustomerInfo",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "userAvatar": avatarURL])
        }
        
        static func save(installationId:String, imei:String) -> HttpSession {
            return HttpSession.post(urlString: "member/saveInstallation",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "imei":imei,
                                             "installationId": installationId])
        }
        
        static func loginOut(imei:String) -> HttpSession {
            return HttpSession.post(urlString: "member/outLogin",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "imei":imei])
        }
        
        static func verification(name:String, cardNo:String, type:Int, imgPath:String, flygoInvitationCode:String) -> HttpSession {
            return HttpSession.post(urlString: "member/customerRealName",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "customerName": name,
                                             "IDCardNo": cardNo,
                                             "type": type,
                                             "imgPath": imgPath,
                                             "flygoInvitationCode": flygoInvitationCode])
        }
    }
    
    enum SendSMSType: String {
        case register = "T100"
        case resetPassword = "T102"
        
        func sendSMS(mobile:String, areaCode:String) -> Observable<PostItem> {
            return HttpSession.post(urlString: "member/sendSmsValidate",
                                    params: ["mobile":mobile,
                                             "areaCode":areaCode,
                                             "smsCode": self.rawValue])
        }
        
        func validate(mobile:String, areaCode:String, smsCode:String) -> HttpSession {
            return HttpSession.post(urlString: "member/smsValidate",
                                    params: ["mobile":mobile,
                                             "areaCode":areaCode,
                                             "validateCode":smsCode,
                                             "smsCode": self.rawValue])
        }
    }
    
    struct User: Model {
        var age              = 0
        var alipayAccount    = ""
        var alipayName       = ""
        var areaCode         = 0
        var authPic1:URL?
        var authPic2:URL?
        var authPic3:URL?
        var authState        = 0
        var customerId:String!
        var drivingLicenseNo = ""
        var email            = ""
        var gender           = ""
        var idCardNo         = ""
        var isAuthReal       = 0
        var lastLoginTime:Date?
        var loginCounts      = 0
        var loginErrorCounts = 0
        var mobile           = ""
        var modifyMan        = ""
        var modifyTime:Date?
        var name             = ""
        var nickname         = ""
        var passportNo       = ""
        var password         = ""
        var realName         = ""
        var registerMan      = ""
        var registerTime:Date?
        var remark           = ""
        var state            = 0
        var userAvatar:URL?
        var userType         = 0
        
        init?(map: Map) {
            customerId <- (map["customerId"], API.StringTransform())
            
            if customerId == nil { return nil }
        }
        
        mutating func mapping(map: Map) {
            age                 <- (map["age"], API.IntTransform())
            alipayAccount       <- (map["alipayAccount"], API.StringTransform())
            alipayName          <- (map["alipayName"], API.StringTransform())
            areaCode            <- (map["areaCode"], API.IntTransform())
            authPic1            <- (map["authPic1"], API.PicTransform())
            authPic2            <- (map["authPic2"], API.PicTransform())
            authPic3            <- (map["authPic3"], API.PicTransform())
            authState           <- (map["authState"], API.IntTransform())
            customerId          <- (map["customerId"], API.StringTransform())
            drivingLicenseNo    <- (map["drivingLicenseNo"], API.StringTransform())
            email               <- (map["email"], API.StringTransform())
            gender              <- (map["gender"], API.StringTransform())
            idCardNo            <- (map["idCardNo"], API.StringTransform())
            isAuthReal          <- (map["isAuthReal"], API.IntTransform())
            lastLoginTime       <- (map["lastLoginTime"], API.DateTransform())
            loginCounts         <- (map["loginCounts"], API.IntTransform())
            loginErrorCounts    <- (map["loginErrorCounts"], API.IntTransform())
            mobile              <- (map["mobile"], API.StringTransform())
            modifyMan           <- (map["modifyMan"], API.StringTransform())
            modifyTime          <- (map["modifyTime"], API.DateTransform())
            name                <- (map["name"], API.StringTransform())
            nickname            <- (map["nickname"], API.StringTransform())
            passportNo          <- (map["passportNo"], API.StringTransform())
            password            <- (map["password"], API.StringTransform())
            realName            <- (map["realName"], API.StringTransform())
            nickname            <- (map["nickname"], API.StringTransform())
            registerMan         <- (map["registerMan"], API.StringTransform())
            registerTime        <- (map["registerTime"], API.DateTransform())
            remark              <- (map["remark"], API.StringTransform())
            state               <- (map["state"], API.IntTransform())
            userAvatar          <- (map["userAvatar"], API.PicTransform())
            userType            <- (map["userType"], API.IntTransform())
        }
    }
}
