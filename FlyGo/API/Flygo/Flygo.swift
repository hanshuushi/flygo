//
//  Flygo.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/22.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

extension API {
    
    struct Deliver {
        static func onlineOrder(vendorOrderInfoId:String,
                                customerAddressId:String,
                                orderformId:String,
                                startDate:String,
                                endDate:String) -> HttpSession {
            return HttpSession.post(urlString: "order/onlineOrder",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "vendorOrderInfoId":vendorOrderInfoId,
                                             "customerAddressId":customerAddressId,
                                             "orderformId":orderformId,
                                             "startDate":startDate,
                                             "endDate":endDate])
        }
    }
    
    struct FlymentItemInArray: Model {
        var startAirport:String = ""
        var flightDeptimePlanDate:Date?
        var startAirportCode:String = ""
        var orgTimezone:String = ""
        var flightArrtimePlanDate:Date?
        var endAirport:String = ""
        var endCountryCode:String = ""
        var endCountryName:String = ""va
        "FlightArrtimePlanDate": "2017-03-31 14:25",
        "endAirport": "浦东机场",
        "endCountryCode": "CN",
        "endCountryName": "中国",
        "startCountryName": "韩国",
        "dstTimezone": "GMT+8",
        "startCountryCode": "KR",
        "flightNumber": "MU5060",
        "endAirportCode": "PVG"
    }
    
    struct Flyment: Model {
        var flightNo:String!
        var startAirport = ""
        var startAirportTimezone = ""
        var endAirport = ""
        var endAirportTimezone = ""
        var faillTimeString = ""
        var takeoffTimeString = ""
        
        init?(map: Map) {
            flightNo                                <-      map["flightNo"]
            
            if flightNo == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            flightNo                                <-      map["flightNo"]
            startAirport                            <-      (map["startAirport"], API.StringTransform())
            startAirportTimezone                    <-      (map["startAirportTimezone"], API.StringTransform())
            endAirport                              <-      (map["endAirport"], API.StringTransform())
            endAirportTimezone                      <-      (map["endAirportTimezone"], API.StringTransform())
            faillTimeString                         <-      (map["fallTime"], API.StringTransform())
            takeoffTimeString                       <-      (map["takeoffTime"], API.StringTransform())
        }
        
        static func get(orders:[String]) -> Observable<APIItem<ModelList<Flyment>>> {
            return HttpSession.get(urlString: "member/getOrderFlightList",
                                   params: ["customerId":UserManager.shareInstance.currentId,
                                            "orderformIds":orders.joined(separator: ",")])
        }
        
        static func getFlyghtArray(flightNo:String, startTime:Date) -> HttpSession {
            return HttpSession.get(urlString: "member/getFlightArrInfo",
                                   params: ["flightNo":flightNo,
                                            "startTime":startTime.toString(format: "YYYY-mm-dd"),
                                            "customerId":UserManager.shareInstance.currentId])
        }
    }
    
    struct FlygoInfo: Model {
        var locationTrack:LocationTrack? = nil
        
        var userAvatar:URL?
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            locationTrack           <-      map["locationTrack"]
            userAvatar              <-      (map["userAvatar"], API.PicTransform())
        }
        
        static func get(from flymanId:String) -> Observable<APIItem<FlygoInfo>> {
            return HttpSession.get(urlString: "member/getAvatarLocation",
                                   params: ["customerId": flymanId])
        }
    }
    
    struct LocationTrack: Model {
        
        var locationTrackId:String?
        var customerId:String!
        var longitude:Double!
        var latitude:Double!
        var locationName:String = ""
        var countryName:String  = ""
        var provinceName:String = ""
        var cityName:String     = ""
        var districtName:String = ""
        var streetName:String   = ""
        var createTime:Date?
        
        init?(map: Map) {
            customerId                          <-      map["customerId"]
            longitude                           <-      (map["longitude"], API.DoubleTransform())
            latitude                            <-      (map["latitude"], API.DoubleTransform())
            
            if customerId == nil || longitude == nil || latitude == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            locationTrackId                     <-      map["locationTrackId"]
            customerId                          <-      map["customerId"]
            longitude                           <-      (map["longitude"], API.DoubleTransform())
            latitude                            <-      (map["latitude"], API.DoubleTransform())
            locationName                        <-      (map["locationName"], API.StringTransform())
            countryName                         <-      (map["countryName"], API.StringTransform())
            provinceName                        <-      (map["provinceName"], API.StringTransform())
            cityName                            <-      (map["cityName"], API.StringTransform())
            districtName                        <-      (map["districtName"], API.StringTransform())
            streetName                          <-      (map["streetName"], API.StringTransform())
            createTime                          <-      (map["createTime"], API.DateTransform())
        }
        
        static func save(longitude:Double,
                         latitude:Double,
                         locationName:String,
                         countryName:String,
                         provinceName:String,
                         cityName:String,
                         districtName:String,
                         streetName:String) -> HttpSession {
            
            print("上传坐标 \(locationName)")
            
            return HttpSession.post(urlString: "member/saveLocationTrack",
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "longitude":longitude,
                                             "latitude":latitude,
                                             "locationName":locationName,
                                             "countryName":countryName,
                                             "provinceName":provinceName,
                                             "cityName":cityName,
                                             "districtName":districtName,
                                             "streetName":streetName])
        }
    }
}
