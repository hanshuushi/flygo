//
//  Address.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/20.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension API {
    struct Address: Model {
        var customerAddressId:String!
        var address:String     = ""
        var contactName:String = ""
        var contactTel:String  = ""
        var isDefault          = false
        var province:String    = ""
        var city:String        = ""
        var district:String    = ""
        
        init?(map: Map) {
            customerAddressId           <- map["customerAddressId"]
            
            if customerAddressId == nil { return nil }
        }
        
        mutating func mapping(map: Map) {
            customerAddressId           <- map["customerAddressId"]
            address                     <- map["address"]
            contactName                 <- map["contactName"]
            contactTel                  <- map["contactTel"]
            isDefault                   <- (map["isDefault"], API.BoolTransform())
            province                    <- map["province"]
            city                        <- map["city"]
            district                    <- map["district"]
        }
        
        
        static func getList() -> Observable<APIItem<ModelList<Address>>> {
            return HttpSession.get(urlString: "memberAddress/getMemberAddressInfo",
                                   params: ["id":UserManager.shareInstance.currentId,
                                            "customerId":UserManager.shareInstance.currentId])
        }
        
        static func selectDefault(id:String) -> HttpSession {
            return HttpSession.post(urlString: "memberAddress/updateMemberAddressDefalut",
                                    params: ["id":UserManager.shareInstance.currentId,
                                             "customerId":UserManager.shareInstance.currentId,
                                             "customerAddressId":id])
        }
        
        static func delete(id:String) -> HttpSession {
            return HttpSession.post(urlString: "memberAddress/deleteMemberAddress",
                                    params: ["id":id,
                                             "customerId":UserManager.shareInstance.currentId])
        }
        
        static func update (id:String,
                            province:String,
                            city:String,
                            district:String,
                            address:String,
                            contactName:String,
                            contactTel:String,
                            isDefault:Bool) -> HttpSession {
            return HttpSession.post(urlString: "memberAddress/updateMemberAddress",
                                    params: ["id":id,
                                             "customerId":UserManager.shareInstance.currentId,
                                             "address":address,
                                             "contactName":contactName,
                                             "contactTel":contactTel,
                                             "areaName":[province, city, district]
                                                .filter{$0.length > 0}
                                                .joined(separator: ",")])
        }
        
        static func create (province:String,
                            city:String,
                            district:String,
                            address:String,
                            contactName:String,
                            contactTel:String) -> HttpSession {
            return HttpSession.post(urlString: "memberAddress/insertMemberAddress",
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "address":address,
                                             "contactName":contactName,
                                             "contactTel":contactTel,
                                             "areaName":[province, city, district]
                                                .filter{$0.length > 0}
                                                .joined(separator: ",")])
        }
    }
}
