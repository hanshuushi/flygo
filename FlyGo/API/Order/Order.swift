//
//  Order.swift
//  FlyGo
//
//  Created by 范舟弛 on 2016/12/28.
//  Copyright © 2016年 舟弛 范. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift

extension API {
    
    struct VendorOrderCollection: Model {
        var orderList:[VendorOrder] = []
        
        var selfSupport:Int = 0
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            orderList       <-      map["orderList"]
            selfSupport     <-      (map["selfSupport"], API.IntTransform())
        }
    }
    
    struct VendorOrder: Model {
        var vendorOrderInfoId:String!
        var orderformId:String!
        var customerId              =       ""
        var userId                  =       ""
        var productName             =       ""
        var productRecStandard      =       ""
        var pic:URL?
        
        init?(map: Map) {
            vendorOrderInfoId           <-      map["vendorOrderInfoId"]
            orderformId                 <-      map["orderformId"]
            
            if vendorOrderInfoId == nil || orderformId == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            vendorOrderInfoId           <-      map["vendorOrderInfoId"]
            orderformId                 <-      map["orderformId"]
            customerId                  <-      (map["customerId"], API.StringTransform())
            userId                      <-      (map["userId"], API.StringTransform())
            productName                 <-      (map["productName"], API.StringTransform())
            productRecStandard          <-      (map["productRecStandard"], API.StringTransform())
            pic                         <-      (map["pic"], API.PicTransform())
        }
        
        static func getOrder(to userId:String) -> HttpSession {
            return HttpSession.get(urlString: "order/getUserVendorOrder",
                                   params: ["customerId":UserManager.shareInstance.currentId,
                                            "userId":userId])
        }
    }
    
    
    enum OrderState : Int {
        /// 订单已生成,待付款
        case obligation         = 1
        /// 付款超时,订单已取消
        case unpayed            = 2
        /// 付款成功,等待飞哥接单
        case pending            = 3
        /// 申请退款,待审核
        case refunding          = 4
        /// 已退款
        case refunded           = 5
        /// 接单超时,待退款
        case noorder            = 6
        /// 飞哥已接单  
        case ordered            = 7
        /// 飞哥提货失败,待退款
        case pickupfailed       = 8
        /// 飞哥已到达
        case flygoback          = 9
        /// 飞哥出现异常,待退款 
        case flygoexception     = 10
        /// 已发货
        case delivering         = 11
        /// 交易完成 
        case finish             = 12
        /// 订单已取消,待退款
        case canceling          = 13
        /// 订单已取消,交易关闭
        case canceled           = 14
        /// 已评价
        case finishAndCommented = 15
    }
    
    struct OrderCouponInfo: Model {
        var couponDeductionMoney:Float = 0.0
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            couponDeductionMoney            <-  (map["couponDeductionMoney"], API.FloatTransform())
        }
    }
    
    struct OrderSubproduct: Model {
        var img:URL?
        var name                = ""
        var num                 = 0
        var sumPrice:Float      = 0.0
        var serviceCharge:Float = 0.0
        var serviceRatio        = ""
        var productRecStandard  = ""
        var cnyPrice:Float      = 0.0
        var productRecId:String = ""
        var deductionAmt:Float  = 0.0
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            img                             <-  (map["img"], API.PicTransform())
            name                            <-  (map["name"], API.StringTransform())
            num                             <-  (map["num"], API.IntTransform())
            sumPrice                        <-  (map["sumPrice"], API.FloatTransform())
            serviceCharge                   <-  (map["serviceCharge"], API.FloatTransform())
            serviceRatio                    <-  (map["serviceRatio"], API.StringTransform())
            productRecStandard              <-  (map["productRecStandard"], API.StringTransform())
            cnyPrice                        <-  (map["cnyPrice"], API.FloatTransform())
            productRecId                    <-  (map["productRecId"], API.StringTransform())
            deductionAmt                    <-  (map["deductionAmt"], API.FloatTransform())
        }
    }
    
    fileprivate struct StateTransform : TransformType {
        func transformFromJSON(_ value: Any?) -> OrderState? {
            
            var intValue:Int?
            
            if let string = value as? String {
                intValue = Int(string)
            } else if let float = value as? Float {
                intValue = Int(float)
            } else if let int = value as? Int {
                intValue = int
            }
            
            return intValue.flatMap{OrderState(rawValue: $0)}
        }
        
        func transformToJSON(_ value: OrderState?) -> Int? {
            return value.map({ $0.rawValue })
        }
    }
    
    struct OrderCollection: Model, DataSetPageCollection {
        
        var list:[API.Order] {
            return self.orderList
        }
        
        var otherInfo:Any? {
            return nil
        }
        
        var orderList:[Order] = []
        
        var hasMore:Bool = false
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            orderList                   <- map["orderList"]
            hasMore                     <- (map["hasMore"], API.BoolTransform())
        }
        
        static func orderList(state:Int? = nil,
                              page:Int = 1,
                              pageSize:Int = 10) -> Observable<APIItem<OrderCollection>> {
            var params:[String:Any] = ["customerId" :UserManager.shareInstance.currentId,
                                       "page"       :page,
                                       "pageSize"   :pageSize]
            
            if let _state = state {
                params["state"] = _state
            }
            
            return HttpSession.post(urlString: "order/orderList?customerId=\(UserManager.shareInstance.currentId)",
                                    isJson: true,
                                    params: params)
        }
        
        static func detail(orderId:String) -> Observable<APIItem<OrderCollection>> {
            return HttpSession.post(urlString: "order/orderList?customerId=\(UserManager.shareInstance.currentId)",
                isJson: true,
                params: ["orderformId":orderId,
                         "state":0,
                         "page":1,
                         "pageSize":1,
                         "customerId":UserManager.shareInstance.currentId])
        }
    }
    
    struct Order: Model {
        var orderformId:String!
        var state = OrderState.canceled
        var detailsState = OrderState.canceled
        var stateZN = ""
        var selfState:Int = 0
        var subOrders:[OrderSubproduct] = []
        var shippingId = ""
        var invTitle = ""
        var invContent = ""
        var buyerNote = ""
        var payTime:Date?
        var createTime:Date?
        var refundTime:Date?
        var cancelTime:Date?
        var deliveryTime:Date?
        var finishTime:Date?
        var serviceCharge:Float = 0.0
        var serviceRatio = ""
        var orderSn = ""
        var contactName = ""
        var contactAddress = ""
        var contactTel = ""
        var flymanId = ""
        var flymanNickName = ""
        var takeTime:Date?
        var takeoffTime:Date?
        var fallTime:Date?
        var flightNo = ""
        var departureAirport = ""
        var landingAirport = ""
        var userAvatar:URL?
        var stateTime:Date?
        var couponInfo:OrderCouponInfo?
        var sumPrice:Float = 0
        
        init?(map: Map) {
            orderformId                                 <-  map["orderformId"]
            orderSn                                     <-  (map["orderSn"], API.StringTransform())
            
            if orderformId == nil || orderSn.length <= 0 {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            orderformId                                 <-  map["orderformId"]
            state                                       <-  (map["state"], API.StateTransform())
            detailsState                                <-  (map["detailsState"], API.StateTransform())
            stateZN                                     <-  (map["stateZN"], API.StringTransform())
            selfState                                   <-  (map["selfState"], API.IntTransform())
            subOrders                                   <-  map["subOrders"]
            shippingId                                  <-  (map["shippingId"], API.StringTransform())
            invTitle                                    <-  (map["invTitle"], API.StringTransform())
            invContent                                  <-  (map["invContent"], API.StringTransform())
            buyerNote                                   <-  (map["buyerNote"], API.StringTransform())
            payTime                                     <-  (map["payTime"], API.DateTransform())
            createTime                                  <-  (map["createTime"], API.DateTransform())
            refundTime                                  <-  (map["refundTime"], API.DateTransform())
            cancelTime                                  <-  (map["cancelTime"], API.DateTransform())
            deliveryTime                                <-  (map["deliveryTime"], API.DateTransform())
            finishTime                                  <-  (map["finishTime"], API.DateTransform())
            serviceCharge                               <-  (map["serviceCharge"], API.FloatTransform())
            serviceRatio                                <-  (map["serviceRatio"], API.StringTransform())
            orderSn                                     <-  (map["orderSn"], API.StringTransform())
            contactName                                 <-  (map["contactName"], API.StringTransform())
            contactAddress                              <-  (map["contactAddress"], API.StringTransform())
            contactTel                                  <-  (map["contactTel"], API.StringTransform())
            flymanId                                    <-  (map["flymanId"], API.StringTransform())
            flymanNickName                              <-  (map["flymanNickName"], API.StringTransform())
            takeTime                                    <-  (map["takeTime"], API.DateTransform())
            takeoffTime                                 <-  (map["takeoffTime"], API.DateTransform())
            fallTime                                    <-  (map["fallTime"], API.DateTransform())
            flightNo                                    <-  (map["flightNo"], API.StringTransform())
            departureAirport                            <-  (map["departureAirport"], API.StringTransform())
            landingAirport                              <-  (map["landingAirport"], API.StringTransform())
            userAvatar                                  <-  (map["userAvatar"], API.PicTransform())
            stateTime                                   <-  (map["stateTime"], API.DateTransform())
            couponInfo                                  <-  map["couponInfo"]
            sumPrice                                    <-  (map["sumPrice"], API.FloatTransform())
            
            #if DEBUG
                if var first = subOrders.first {
                    first.name += (" \\ " + Date().timeIntervalSince1970.toString)
                    
                    subOrders[0] = first
                }
            #endif
        }
    }
    
    struct OrderDetail: Model {
        
        static func orderStateInfo(orderformId:String) -> Observable<APIItem<OrderDetail>> {
            return HttpSession.post(urlString: "order/orderStateInfo?customerId=\(UserManager.shareInstance.currentId)",
                isJson: true,
                params: ["orderformId":orderformId,
                         "customerId":UserManager.shareInstance.currentId])
        }
        
        struct ShippingInfo: Model {
            struct Head: Model {
                var code = ""
                var message = ""
                var transType = 0
                var transMessageId = "201501061626507397"
                
                init?(map: Map) {
                    
                }
                
                mutating func mapping(map: Map) {
                    code                        <-      map["code"]
                    message                     <-      map["message"]
                    transType                   <-      (map["transType"], API.IntTransform())
                    transMessageId              <-      map["transMessageId"]
                    
                }
            }
            
            struct Body: Model {
                var orderId = ""
                var mailNo = ""
                var acceptTime:Date?
                var acceptAddress = ""
                var opcode = 0
                var remark = ""
                
                init?(map: Map) {
                    
                }
                
                mutating func mapping(map: Map) {
                    orderId                         <-      map["orderId"]
                    mailNo                          <-      map["mailNo"]
                    acceptTime                      <-      (map["AcceptTime"], API.DateTransform())
                    acceptAddress                   <-      map["acceptAddress"]
                    opcode                          <-      (map["opcode"], API.IntTransform())
                    remark                          <-      (map["AcceptStation"], API.StringTransform())
                }
            }
            
            var head:Head?
            
            var body:[Body] = []
            
            init?(map: Map) {
            }
            
            mutating func mapping(map: Map) {
                head                            <-      map["head"]
                body                            <-      map["body"]
            }
        }
        
        var shippingInfo:[ShippingInfo.Body] = []
        
        struct OrderStateInfo: Model {
            var desc = "" //状态小状态详细说明
            var detailState:OrderState! //小状态状态码
            var money = 18400 //订单金额
            var state:OrderState! //大状态状态码
            var actionTime:Date?  //状态时间
            var orderformId = ""
            
            init?(map: Map) {
                state                           <-      (map["state"], API.StateTransform())
                detailState                     <-      (map["detailState"], API.StateTransform())
                
                if state == nil || detailState == nil {
                    return nil
                }
            }
            
            mutating func mapping(map: Map) {
                desc                            <-      map["desc"]
                detailState                     <-      (map["detailState"], API.StateTransform())
                money                           <-      (map["money"], API.IntTransform())
                state                           <-      (map["state"], API.StateTransform())
                actionTime                      <-      (map["actionTime"], API.DateTransform())
                orderformId                     <-      map["orderformId"]
            }
        }
        
        var orderStateInfo:[OrderStateInfo] = []
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            shippingInfo                                <-      map["shippingInfo"]
            orderStateInfo                              <-      map["orderStateInfo"]
        }
    }
    
    struct OrderPaymentInfo: Model {
        private struct Datas: Model {
            var paymentInfo:OrderPaymentInfo!
            
            init?(map: Map) {
                paymentInfo <- map["paymentInfo"]
                
                if paymentInfo == nil {
                    return nil
                }
            }
            
            mutating func mapping(map: Map) {
                paymentInfo <- map["paymentInfo"]
            }
        }
        
        var money:Float     = 0.0
        var applyPayContent:String!
        var outTradeNo:String!
        
        init?(map: Map) {
            applyPayContent     <- map["applyPayContent"]
            outTradeNo          <- map["outTradeNo"]
            
            guard let content = applyPayContent, content.length > 0 else {
                return nil
            }
            
            guard let no = outTradeNo, no.length > 0 else {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            applyPayContent     <- map["applyPayContent"]
            outTradeNo          <- map["outTradeNo"]
            money               <- (map["money"], API.FloatTransform())
        }
        
        typealias OrderSubmit = (
            productRecId:String,
            num:Int,
            belongStoreIds:String,
            freeStores:String,
            cartSubId:String,
            couponDetailsId:String
        )
        
        static func repay(orderformId:String) -> Observable<APIItem<OrderPaymentInfo>> {
            return HttpSession.post(urlString: "order/goPay?customerId=\(UserManager.shareInstance.currentId)",
                                    isJson: true,
                                    params: ["orderformId":orderformId,
                                             "customerId":UserManager.shareInstance.currentId])
        }
        
        static func paySuccess(outTradeNo:String) -> Observable<PostItem> {
            return HttpSession.post(urlString: "pay/paySuccess?customerId=\(UserManager.shareInstance.currentId)",
                                    isJson: true,
                                    params: ["outTradeNo":outTradeNo,
                                             "customerId":UserManager.shareInstance.currentId,
                                             "respCode":1])
        }
        
        static func cancel(orderformId:String) -> Observable<PostItem> {
            return HttpSession.post(urlString: "order/cancelOrder?customerId=\(UserManager.shareInstance.currentId)",
                isJson: true,
                params: ["customerId":UserManager.shareInstance.currentId,
                         "orderformId":orderformId
                ])
        }
        
        static func confirm(orderformId:String) -> Observable<PostItem> {
            return HttpSession.post(urlString: "order/confirmReceive?customerId=\(UserManager.shareInstance.currentId)",
                isJson: true,
                params: ["customerId":UserManager.shareInstance.currentId,
                         "orderformId":orderformId
                ])
        }
        
        static func reorder(orderformId:String) -> Observable<PostItem> {
            return HttpSession.post(urlString: "order/reOrder?customerId=\(UserManager.shareInstance.currentId)",
                isJson: true,
                params: ["customerId":UserManager.shareInstance.currentId,
                         "orderformId":orderformId
                ])
        }
        
        static func submit(orders:[OrderSubmit],
                           addressId:String,
                           buyerNote:String = "",
                           payTotalMoney:Float) -> Observable<APIItem<OrderPaymentInfo>> {
            
            var params = [String:Any]()
            
            params["orders"] = orders.map({ (order) -> [String:Any] in
                
                var dict:[String:Any] = ["productRecId":order.productRecId,
                                         "num":order.num,
                                         "belongStoreIds":order.belongStoreIds]
                
//                if order.freeStores.length > 0 {
//                    dict["freeStores"] = order.freeStores
//                }
                
                if order.cartSubId.length > 0 {
                    dict["cartSubId"] = order.cartSubId
                }
                
                if order.couponDetailsId.length > 0 {
                    dict["couponDetailsId"] = order.couponDetailsId
                }
                
                return dict
            })
            
            params["addressId"] = addressId
            params["customerId"] = UserManager.shareInstance.currentId
            params["payTotalMoney"] = payTotalMoney
            params["ver"] = 3
            
            let datas = (HttpSession.post(urlString: "order/submitOrder?customerId=\(UserManager.shareInstance.currentId)",
                                          isJson: true,
                                          params: params) as Observable<APIItem<OrderPaymentInfo>>)
            
            return datas
        }
    }
}
