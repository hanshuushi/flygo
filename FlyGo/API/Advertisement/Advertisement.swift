//
//  Advertisement.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/5/8.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

extension API {
    struct Advertisement: Model {
        var advertisementId: String!
        var url: URL?
        var state: Int = 1
        var registerTime: Date?
        var registerMan: String = ""
        var modifyTime: Date?
        var modifyMan: String = ""
        var remark: String = ""
        var pic: URL?
        var type: Int!
        var clicks: Int = 0
        var width: Int = 0
        var height: Int = 0
        
        init?(map: Map) {
            advertisementId                     <-      map["advertisementId"]
            
            if advertisementId == nil || advertisementId.length < 1 {
                return nil
            }
            
            type                                <-      (map["type"], API.IntTransform())
            
            if type == nil || type < 1 || type > 3 {
                return nil
            }
        }
        
        static func getAdvertisements() -> HttpSession {
            return HttpSession.get(urlString: "member/getAdsInfo",
                                   params: API.defaultDict)
        }
        
        static func clickBy(advertisementId:String) {
            var dict = API.defaultDict
            
            dict["advertisementId"] = advertisementId
            
            HttpSession
                .post(urlString: "member/clickAds",
                      params: dict)
                .responseNoModel({
                    print("advertisementId(\(advertisementId)) clicl success")
                }) { (error) in
                    print("advertisementId(\(advertisementId)) clicl failed error is \(error)")
            }
        }
        
        static func clickBy(modularId:String) {
            var dict = API.defaultDict
            
            dict["modularId"] = modularId
            
            HttpSession
                .post(urlString: "index/clickModular",
                      params: dict)
                .responseNoModel({
                    print("modularId(\(modularId)) clicl success")
                }) { (error) in
                    print("modularId(\(modularId)) clicl failed error is \(error)")
            }
        }
        
        mutating func mapping(map: Map) {
            advertisementId                     <-      map["advertisementId"]
            url                                 <-      (map["url"], API.URLTransform())
            state                               <-      (map["state"], API.IntTransform())
            registerTime                        <-      (map["registerTime"], API.DateTransform())
            registerMan                         <-      (map["registerMan"], API.StringTransform())
            modifyTime                          <-      (map["modifyTime"], API.DateTransform())
            modifyMan                           <-      (map["modifyMan"], API.StringTransform())
            remark                              <-      (map["remark"], API.StringTransform())
            pic                                 <-      (map["pic"], API.PicTransform())
            type                                <-      (map["type"], API.IntTransform())
            clicks                              <-      (map["clicks"], API.IntTransform())
            width                               <-      (map["width"], API.IntTransform())
            height                              <-      (map["height"], API.IntTransform())
        }
    }
}
