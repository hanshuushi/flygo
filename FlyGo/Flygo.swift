//
//  Flygo.swift
//  FlyGo
//
//  Created by 范舟弛 on 2017/2/22.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

struct AirportBrage {
    typealias Item = AirportItem
}

extension API {
    
    struct DeliverItem: Model {
        var acceptTime:String = ""
        
        var acceptStation:String = ""
        
        var remark:String = ""
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            acceptTime      <-  map["AcceptTime"]
            acceptStation   <-  map["AcceptStation"]
            remark          <-  map["Remark"]
        }
        
        static func getDetail(shipperCode:String, shippingSn:String) -> Observable<APIItem<ModelList<DeliverItem>>> {
            return HttpSession.post(urlString: "order/getOrderTraces",
                                    params: ["shipperCode": shipperCode,
                                             "shippingSn":  shippingSn,
                                             "customerId":  UserManager.shareInstance.currentId])
        }
    }
    
    struct DeliverOrder: Model {
        var startDate:Date?
        var address:String = ""
        var contactName:String = ""
        var endDate:Date?
        var pic:URL?
        var contactTel:String = ""
        var productName:String = ""
        var vendorOrderInfoId:String = ""
        var orderformId:String = ""
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            startDate               <-  (map["startDate"], API.DateTransform())
            address                 <-  (map["address"], API.StringTransform())
            contactName             <-  (map["contactName"], API.StringTransform())
            endDate                 <-  (map["endDate"], API.DateTransform())
            pic                     <-  (map["pic"], API.PicTransform())
            contactTel              <-  (map["contactTel"], API.StringTransform())
            productName             <-  (map["productName"], API.StringTransform())
            vendorOrderInfoId       <-  (map["vendorOrderInfoId"], API.StringTransform())
            orderformId             <-  (map["orderformId"], API.StringTransform())
        }
        
        static func getOrderInfo(from orderformId:String) -> Observable<APIItem<DeliverOrder>> {
            return HttpSession.post(urlString: "order/getOnlineOrderInfo",
                                    params: ["orderformId":orderformId,
                                             "customerId": UserManager.shareInstance.currentId])
        }
        
        static func bindLogisticsOrder(from shippingSn:String, vendorOrderInfoId:String, orderformId:String, arrivedTime:String) -> HttpSession {
            return HttpSession.post(urlString: "order/bindLogisticsOrder",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "vendorOrderInfoId":vendorOrderInfoId,
                                             "shippingSn":shippingSn,
                                             "orderformId":orderformId,
                                             "arrivedTime":arrivedTime])
        }
    }
    
    struct FlightmentTicket: Model {
        
        var vendorFlightInfoId:String!
        
        init?(map: Map) {
            vendorFlightInfoId <- map["vendorFlightInfoId"]
            
            if vendorFlightInfoId == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            vendorFlightInfoId <- map["vendorFlightInfoId"]
        }
        
        static func onlineOrder(vendorOrderInfoId:String,
                                customerAddressId:String,
                                orderformId:String,
                                startDate:String,
                                endDate:String) -> HttpSession {
            return HttpSession.post(urlString: "order/onlineOrder",
                                    params: ["customerId": UserManager.shareInstance.currentId,
                                             "vendorOrderInfoId":vendorOrderInfoId,
                                             "customerAddressId":customerAddressId,
                                             "orderformId":orderformId,
                                             "startDate":startDate,
                                             "endDate":endDate])
        }
        
        static func updateTicketPics(for vendorFlightInfoId:String, pics:[String]) -> HttpSession {
            var dict = [String:Any]()
            
            dict["customerId"] = UserManager.shareInstance.currentId
            dict["vendorFlightInfoId"] = vendorFlightInfoId
            dict["pic"] = pics.joined(separator: ",")
            
            return HttpSession.post(urlString: "member/updateVendorFlightPicById",
                                    params: dict)
        }
        
        static func addFlightment(for items:[FlymentItemInArray]) -> HttpSession {
            var dict = [String:Any]()
            
            dict["customerId"] = UserManager.shareInstance.currentId
            dict["isTransfer"] = items.count
            
            items.forEachEnumerated { (index, item) in
                let number = index + 1
                
                if index == 0 {
                    dict["flightNo"] = item.flightNumber
                    dict["departureAirportName"] = item.startAirport
                    dict["landingAirportName"] = item.endAirport
                    dict["takeoffTimeStr"] = item.flightDeptimePlanDate
                    dict["fallTimeStr"] = item.flightArrtimePlanDate
                } else {
                    dict["flightNo\(number)"] = item.flightNumber
                    dict["airportStartName\(number)"] = item.startAirport
                    dict["airportEndName\(number)"] = item.endAirport
                    dict["startTimeStr\(number)"] = item.flightDeptimePlanDate
                    dict["endTimeStr\(number)"] = item.flightArrtimePlanDate
                }
                
                dict["startAirportTimezone\(number)"] = item.orgTimezone
                dict["endAirportTimezone\(number)"] = item.dstTimezone
                dict["startAirportCode\(number)"] = item.startAirportCode
                dict["endAirportCode\(number)"] = item.endAirportCode
            }
            
            dict["startCountryCode"] = items.first!.startCountryCode
            dict["endCountryCode"] = items.last!.endCountryCode
            dict["startCityName"] = items.first!.startCityName
            dict["endCityName"] = items.last!.endCityName
            
            return HttpSession.post(urlString: "member/saveNewVendorFlight",
                                    params: dict)
        }
    }
    
    struct FlymentItemInArray: Model {
        var startAirport:String          = ""
        var flightDeptimePlanDate:String = ""
        var startAirportCode:String      = ""
        var orgTimezone:String           = ""
        var flightArrtimePlanDate:String = ""
        var endAirport:String            = ""
        var endCountryCode:String        = ""
        var endCountryName:String        = ""
        var startCountryName:String      = ""
        var dstTimezone:String           = ""
        var startCountryCode:String      = ""
        var flightNumber:String          = ""
        var endAirportCode:String        = ""
        var startCityName:String         = ""
        var endCityName:String           = ""
        
        init?(map: Map) {
            
        }
        
        init(flightNo:String, startAirportItem:AirportBrage.Item, endAirportItem:AirportBrage.Item, startDate:Date, endDate:Date) {
            
            let gmt = {
                (utc:Int) -> String in
                
                if utc >= 0 {
                    return "GMT+\(utc)"
                } else {
                    return "GMT\(utc)"
                }
            }
            
            startAirport          = startAirportItem.name
            flightDeptimePlanDate = startDate.toString(format: "YYYY-MM-dd HH:mm")
            startAirportCode      = startAirportItem.code
            orgTimezone           = gmt(startAirportItem.utc)
            flightArrtimePlanDate = endDate.toString(format: "YYYY-MM-dd HH:mm")
            endAirport            = endAirportItem.name
            endCountryCode        = endAirportItem.countryCode
            endCountryName        = endAirportItem.countryName
            startCountryName      = startAirportItem.countryName
            dstTimezone           = gmt(endAirportItem.utc)
            startCountryCode      = startAirportItem.countryCode
            flightNumber          = flightNo
            endAirportCode        = endAirportItem.code
            startCityName         = startAirportItem.city
            endCityName           = endAirportItem.city
        }
        
        mutating func mapping(map: Map) {
            startAirport                            <-      (map["startAirport"], API.StringTransform())
            flightDeptimePlanDate                   <-      (map["FlightDeptimePlanDate"], API.StringTransform())
            startAirportCode                        <-      (map["startAirportCode"], API.StringTransform())
            orgTimezone                             <-      (map["orgTimezone"], API.StringTransform())
            flightArrtimePlanDate                   <-      (map["FlightArrtimePlanDate"], API.StringTransform())
            endAirport                              <-      (map["endAirport"], API.StringTransform())
            endCountryCode                          <-      (map["endCountryCode"], API.StringTransform())
            endCountryName                          <-      (map["endCountryName"], API.StringTransform())
            startCountryName                        <-      (map["startCountryName"], API.StringTransform())
            dstTimezone                             <-      (map["dstTimezone"], API.StringTransform())
            startCountryCode                        <-      (map["startCountryCode"], API.StringTransform())
            flightNumber                            <-      (map["flightNumber"], API.StringTransform())
            endAirportCode                          <-      (map["endAirportCode"], API.StringTransform())
            startCityName                           <-      (map["startCityName"], API.StringTransform())
            endCityName                             <-      (map["endCityName"], API.StringTransform())
        }
        
        static func getFlyghtArray(flightNo:String, startTime:Date) -> HttpSession {
            return HttpSession.get(urlString: "member/getFlightArrInfo",
                                   params: ["flightNo":flightNo,
                                            "startTime":startTime.toString(format: "YYYY-MM-dd"),
                                            "customerId":UserManager.shareInstance.currentId])
        }
    }
    
    struct Flyment: Model {
        var flightNo:String!
        var startAirport = ""
        var startAirportTimezone = ""
        var endAirport = ""
        var endAirportTimezone = ""
        var faillTimeString = ""
        var takeoffTimeString = ""
        
        init?(map: Map) {
            flightNo                                <-      map["flightNo"]
            
            if flightNo == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            flightNo                                <-      map["flightNo"]
            startAirport                            <-      (map["startAirport"], API.StringTransform())
            startAirportTimezone                    <-      (map["startAirportTimezone"], API.StringTransform())
            endAirport                              <-      (map["endAirport"], API.StringTransform())
            endAirportTimezone                      <-      (map["endAirportTimezone"], API.StringTransform())
            faillTimeString                         <-      (map["fallTime"], API.StringTransform())
            takeoffTimeString                       <-      (map["takeoffTime"], API.StringTransform())
        }
        
        static func get(orders:[String]) -> Observable<APIItem<ModelList<Flyment>>> {
            return HttpSession.get(urlString: "member/getOrderFlightList",
                                   params: ["customerId":UserManager.shareInstance.currentId,
                                            "orderformIds":orders.joined(separator: ",")])
        }
    }
    
    struct FlygoInfo: Model {
        var locationTrack:LocationTrack? = nil
        
        var userAvatar:URL?
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            locationTrack           <-      map["locationTrack"]
            userAvatar              <-      (map["userAvatar"], API.PicTransform())
        }
        
        static func get(from flymanId:String) -> Observable<APIItem<FlygoInfo>> {
            return HttpSession.get(urlString: "member/getAvatarLocation",
                                   params: ["customerId": flymanId])
        }
    }
    
    struct LocationTrack: Model {
        
        var locationTrackId:String?
        var customerId:String!
        var longitude:Double!
        var latitude:Double!
        var locationName:String = ""
        var countryName:String  = ""
        var provinceName:String = ""
        var cityName:String     = ""
        var districtName:String = ""
        var streetName:String   = ""
        var createTime:Date?
        
        init?(map: Map) {
            customerId                          <-      map["customerId"]
            longitude                           <-      (map["longitude"], API.DoubleTransform())
            latitude                            <-      (map["latitude"], API.DoubleTransform())
            
            if customerId == nil || longitude == nil || latitude == nil {
                return nil
            }
        }
        
        mutating func mapping(map: Map) {
            locationTrackId                     <-      map["locationTrackId"]
            customerId                          <-      map["customerId"]
            longitude                           <-      (map["longitude"], API.DoubleTransform())
            latitude                            <-      (map["latitude"], API.DoubleTransform())
            locationName                        <-      (map["locationName"], API.StringTransform())
            countryName                         <-      (map["countryName"], API.StringTransform())
            provinceName                        <-      (map["provinceName"], API.StringTransform())
            cityName                            <-      (map["cityName"], API.StringTransform())
            districtName                        <-      (map["districtName"], API.StringTransform())
            streetName                          <-      (map["streetName"], API.StringTransform())
            createTime                          <-      (map["createTime"], API.DateTransform())
        }
        
        static func save(longitude:Double,
                         latitude:Double,
                         locationName:String,
                         countryName:String,
                         provinceName:String,
                         cityName:String,
                         districtName:String,
                         streetName:String) -> HttpSession {
            
            print("上传坐标 \(locationName)")
            
            return HttpSession.post(urlString: "member/saveLocationTrack",
                                    params: ["customerId":UserManager.shareInstance.currentId,
                                             "longitude":longitude,
                                             "latitude":latitude,
                                             "locationName":locationName,
                                             "countryName":countryName,
                                             "provinceName":provinceName,
                                             "cityName":cityName,
                                             "districtName":districtName,
                                             "streetName":streetName])
        }
    }
}
