//
//  ZBarReaderController_ZBarReaderController_Extension.h
//  Fl/Users/Latte/项目/Zhiyou/FlyGo/TesttttttVC.swiftyGo
//
//  Created by 范舟弛 on 2017/4/10.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

#import <ZBarSDK/ZBarSDK.h>

@interface ZBarReaderController (Brage)

- (nullable ZBarSymbol *)scanUIImage:(nonnull UIImage *)image;

@end
