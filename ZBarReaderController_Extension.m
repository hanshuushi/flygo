//
//  ZBarReaderController_ZBarReaderController_Extension.h
//  FlyGo
//
//  Created by 范舟弛 on 2017/4/10.
//  Copyright © 2017年 舟弛 范. All rights reserved.
//

#import "ZBarReaderController_Extension.h"

@implementation ZBarReaderController (Brage)

- (nullable ZBarSymbol *)scanUIImage:(nonnull UIImage *)image {
    
    id <NSFastEnumeration> results = [self scanImage:image.CGImage];
    
    ZBarSymbol *sym = nil;
    for(sym in results) {
        break;
    }
    
    // Get only last symbol
    
    return sym;
}

@end
